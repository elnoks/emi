<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Tips & tricks");
?> 
<div class="bx_page"> 
<div class="h1-top">
		<h1>Tips & tricks</h1>
    </div>
 <br>
  <p> <b>Come si diventa un rappresentante ufficiale del marchio E.Mi e della Scuola di nail design di Ekaterina Miroshnichenko?</b> 
    <br />
E.Mi offre diversi pacchetti di soluzioni aziendali per iniziare subito un’attività di rapida crescita e dai grandi riscontri finanziari. Puoi diventare un rappresentante ufficiale della Scuola di nail design di Ekaterina Miroshnichenko nel tuo paese, diventare un distributore ufficiale del marchio E.Mi o un rivenditore all’ingrosso. Informazioni dettagliate sono disponibili alla pagina <a href="/social/open_school.php" >“Come diventare concessionario della scuola E.Mi”</a></p>
 
  <p> <b>Nel mio paese è già presente una Scuola E.Mi? </b> 
    <br />
L’elenco delle Scuole di nail design di Ekaterina Miroshnichenko si trova nella pagina<a href="/contacts/" >“Contatti”</a>. </p>
 
  <p> <b>Dove posso acquistare i prodotti E.Mi? </b> 
    <br />
Puoi comprare i prodotti E.Mi sul nostro sito nella sezione “Prodotti” (vedere la sezione “Come effettuare un ordine” per istruzioni dettagliate) o in un punto vendita. Per conoscere tutti i punti vendita visita la pagina <a href="/contacts/" >“Contatti”</a>. </p>
 
  <p> <b>Frequentando i corsi della Scuola di nail design di Ekaterina Miroshnichenko ricevo una qualifica di base come tecnica di manicure, pedicure o onicotecnica specializzata in ricostruzione unghie? </b> 
    <br />
La Scuola di nail design di Ekaterina Miroshnichenko è specializzata solo in corsi di nail art.</p>
 
  <p> <b>Posso sostenere i corsi della Scuola di nail design di Ekaterina Miroshnichenko senza esperienza di lavoro come onicotecnica? </b> 
    <br />
I corsi sono di livello avanzato, raccomandiamo quindi di sviluppare delle capacità di base in corsi per principianti. Questo renderà più semplice imparare questo tipo di nail art.</p>
 
  <p> <b>Quali sono i corsi con cui è meglio iniziare? </b> 
    <br />
Raccomandiamo di iniziare con i corsi base: Art Painting e E.Mi Technologies – dove imparerai a lavorare con i gel colorati e svilupperai molta manualità. Dopo questi corsi sarà semplice partecipare ai corsi di secondo e terzo livello di difficoltà come Pittura Zhostovo, Micropittura, Ornamenti Floreali Sofisticati, Complessità astratte, ecc.</p>
 
  <p> <b>Dove posso trovare il calendario del corsi? </b> 
    <br />
Puoi trovare maggiori dettagli sui corsi organizzati alla pagina <a href="/courses/#schedule" >“Calendario”</a>. </p>
 
  <p> <b>Qual è l’età minima delle studentesse della Scuola di nail design di Ekaterina Miroshnichenko? </b> 
    <br />
Si accettano partecipanti a partire dai 15 anni.</p>
 
  <p> <b>Posso applicare i gel paint di E.Mi sullo smalto semi permanente di altri marchi? </b> 
    <br />
Si, i gel paint E.Mi possono essere applicati su altri smalti semi permanenti, ricorda però che la rimozione dello smalto semi permanente è effettuata con un solvete apposito, mentre i gel paint E.Mi devono essere rimossi con la fresa o la lima. Pertanto raccomandiamo di realizzare il decoro su una piccola parte dell’unghia, coprendola poi con lo smalto. Esempio: Sabbia di velluto e Pietre liquide o 3D vintage.</p>
 
  <p> <b>Posso applicare i gel paint di E.Mi sull’unghia naturale? </b> 
    <br />
Prima dell’applicazione dei gel paint E.Mi, l’unghia deve essere coperta con gel, acrilico o smalto semi permanente. L’applicazione diretta sull’unghia naturale è vietata.</p>
 
  <p> <b>Qual è la differenza tra i gel paint E.Mi e EMPASTA? </b> 
    <br />
Le principali differenze sono la consistenza e la dispersione. E.Mi EMPASTA è più denso, le pennellate risultano quindi più precise, la texture di EMPASTA permette di realizzare disegni 3D. EMPASTA non rilascia dispersione, può quindi essere applicato sia sopra che sotto il Finish Gel. Inoltre la polimerizzazione di EMPASTA è di solo 2-5 secondi, questo permette di realizzare disegni complessi e molto ampi in breve tempo. I gel paint E.Mi sono più liquidi, ideali per gli ornamenti.</p>
 
  <p> <b>Qual è la differenza tra il Liquid stones polimero e Liquid stones gel? </b> 
    <br />
Il Liquid stones gel è un prodotto per disegnare “pietre liquide”. Permette di imitare le pietre preziose sulle unghie in diverse dimensioni e altezze senza richiedere l’applicazione di un successivo strato protettivo di top coat permettendo di risparmiare tempo. Il Liquid stones gel è trasparente e non può essere miscelato con i pigmenti. Il Liquid stones polimero imita le pietre colorate grazie alla possibilità di essere miscelato con i pigmenti, necessita inoltre la copertura con un top gel con strato di dispersione.</p>
 
  <p> <b>Come utilizzare EMPASTA Openwork Silver per incorniciare i decori? </b> 
    <br />
EMPASTA Openwork Silver contiene particelle trasparenti, questo rende necessario di mettere poco prodotto nella palette e lavorarlo bene; dopo questo far riposare il colore per alcuni giorni per ottenere una texture densa, adatta alla realizzazione di cornici. Lavorare con EMPASTA fresco è impossibile. Più si lascia riposare EMPASTA, meglio è. EMPASTA non si secca, è sempre utilizzabile e diventa perfetto per lavorare.</p>
 
  <p> <b>Di quali pennelli ho bisogno per lavorare i materiali E.Mi? </b> 
    <br />
Raccomandiamo 3 set di pennelli per lavorare con i prodotti E.Mi: Set di pennelli per Art Painting, Set di pennelli per tecnica One Stroke and set di pennelli Universali.</p>
 
  <p> <b>Come posso mantenere i pennelli belli per lungo tempo? </b> 
    <br />
Non utilizzare liquidi che contengono ethyl acetate per la pulizia, un fazzolettino bagnato è sufficiente. I pennelli possono essere danneggiati dalla luce UV della lampada per unghie.</p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>