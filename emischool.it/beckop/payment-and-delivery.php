<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Pagamenti e spedizioni");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Pagamenti e spedizioni</h1>
   </div>
 
  <br />

  <p class="MsoListParagraphCxSpFirst" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.<span style="font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">      </span></span></b><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Prezzi.<o:p></o:p></span></b></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.1<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Tutti i prezzi segnalati nel sito e-commerce includono l’IVA.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.2<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">I prodotti sono forniti senza la maggiorazione dell’IVA per i prodotti con destinati a paesi fuori dall’area Schengen.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.3<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Per tutti gli ordini superiori a 500 € sarà applicato uno sconto del 5% (sconto non rimborsabile e valido solo su un ordine). <o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.4<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">I prezzi non comprendono i costi di spedizione.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.5<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Tutte le tasse e altri possibili costi dovuti alle leggi del paese di destinazione della merce sono a carico del cliente.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpLast" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;"><o:p> </o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><b><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">2. Modalità di pagamento.<o:p></o:p></span></b></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">2.1 Tutti i pagamenti devono fare riferimento alla fattura inviata.<o:p></o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">2.2 Prima di procedere con il pagamento chiediamo gentilmente di controllare che i prodotti e il totale della fattura corrispondano con l’ordine effettuato.<o:p></o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">2.3 Sono accettati i pagamenti tramite:<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpFirst" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; font-family: Symbol;">·<span style="font-stretch: normal; font-size: 7pt; font-family: 'Times New Roman';">         </span></span><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">Bonifico bancario.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; font-family: Symbol;">·<span style="font-stretch: normal; font-size: 7pt; font-family: 'Times New Roman';">         </span></span><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">Western Union.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpLast" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;"> </span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3. Modalità di spedizione.<o:p></o:p></span></b></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.1 Gli ordini sono spediti con corriere TNT ransporting Company.<o:p></o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.2 La quota delle spese di spedizione non è fissa, ma varia a seconda del peso e dell’indirizzo di destinazione. <o:p></o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">Il totale di costi di trasporto sarà inserito nella fattura.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpFirst" style="margin-left: 18pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.3.</span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">La preparazione e la spedizione dell’ordine saranno effettuati entro 3 giorni lavorativi dal ricevimento del pagamento.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpLast" style="margin-left: 18pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.4.</span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">I tempi di consegna variano a seconda dell’indirizzo di destinazione.</span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
 <span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">Al momento della spedizione il corriere provvederà a inviare all’indirizzo email fornito il codice di tracciabilità dell’ordine. </span> </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>