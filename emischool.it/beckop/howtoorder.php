<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как заказать");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>How to make an order.</h1>
   </div>
 </div>
 
<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

 
<div class="bx_page"> 
  <p>1.	Sono accettati solo gli ordini inviati tramite il sito e-commerce. Gli ordini inoltrati telefonicamente o via e-mail non saranno presi in carico. 
    <br />
   </p>
 
  <p>2.	Per evitare ogni tipo di problema nella spedizione chiediamo gentilmente di controllare i tuoi dati personali e di contatto prima di procedere con l’ordine. Dati richiesti: nome e cognome, numero di telefono comprensivo di prefisso internazionale (Italia +39), indirizzo email e di consegna della merce (via/piazza, numero civico, città, CAP, nazione). 
    <br />
   </p>
 
  <p>3.	In caso di errori l’azienda non è responsabile per ogni spesa aggiuntiva derivante da consegne extra dovute alla fornitura di un indirizzo errato.
    <br />
   </p>
 
  <p>4.	Accettiamo e processiamo ordini da tutti i paesi eccetto Russia, Ucraina, Kazakhistan, Bielorussia, Armenia e Azerbaijan. Gli ordini destinati ai paesi sopracitati saranno inoltrati al sito www.emi-school.ru.</p>
 
  <p>5.	Alla conclusione dell’ordine, il cliente riceverà in modo automatico una notifica via email con un riepilogo dell’ordine senza includere le spese di trasporto. <b>Attenzione! Vi preghiamo di non procedere al pagamento sulla base dell’importo segnalato nell’email automatica!</b> La fattura con l’importo totale, che include i costi di trasporto, sarà inviata entro 2 giorni lavorativi. I dettagli con cui procedere al pagamento saranno inviati allegati alla email con la fattura, la quale sarà in Euro o in Dollari a seconda delle necessità indicate. Vi chiediamo gentilmente di informare anticipatamente i gestori del sito e-commerce se avete necessità di pagare in dollari. 
    <br />
   </p>
 
  <p>6.	È possibile procedere con il pagamento dell’ordine entro 7 giorni lavorativi a partire dalla data di emissione della fattura. In caso di mancato pagamento nei termini stabiliti l’ordine viene automaticamente annullato.
    <br />
   </p>
  <p>7.	Se non riesci a completare il pagamento entro i 7 giorni lavorativi stabiliti ti chiediamo gentilmente di contattare i gestori del sito e-commerce via email post@emischool.com or phone +420 773 208 276 in advance. 
    <br />
   </p>
  <p>8.	Prima di procedere con il pagamento ti chiediamo di controllare se i prodotti e il totale della fattura sono corretti.
    <br />
   </p>
  <p>9.	La preparazione dell’ordine e la spedizione saranno completati entro 3 giorni lavorativi dal ricevimento del pagamento.
    <br />
   </p>

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>