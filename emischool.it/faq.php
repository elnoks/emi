<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Tips & tricks");
?> 
<div class="bx_page"> 
<div class="h1-top">
		<h1>Suggerimenti</h1>
    </div>
 <br>
  <p> <b>Vuoi diventare un rappresentante o un rivenditore?</b> 
    <br />
E.Mi Italia ricerca rappresentanti e rivenditori nelle zone ancora scoperte. Per ricevere informazioni a riguardo contatta il direttore commerciale E.Mi Italia <a href="mailto:franceschelli.alessandro@gmail.com" >Alessandro Franceschelli</a></p>
 
  <p> <b>Nel mio paese è già presente una Scuola E.Mi? </b> 
    <br />
Si, scrivi a <a href="mailto:italy-school@emischool.com">Scuola E.mi Italia</a>. </p>
 
  <p> <b>Dove posso acquistare i prodotti E.Mi? </b> 
    <br />
Puoi comprare i prodotti E.Mi sul nostro sito nella sezione “Prodotti” (vedere la sezione “Come effettuare un ordine” per istruzioni dettagliate) o da uno dei nostri rivenditori. Per conoscere tutti i rivenditori visita la pagina <a href="/contacts/" >“Contatti”</a>. </p>
 
  <p> <b>Frequentando i corsi della Scuola di nail design di Ekaterina Miroshnichenko ricevo una qualifica di base come tecnica di manicure o onicotecnica specializzata in ricostruzione unghie? </b> 
    <br />
La Scuola di nail design di Ekaterina Miroshnichenko è specializzata in corsi di nail art e di ricostruzione avanzati.</p>
 
  <p> <b>Posso sostenere i corsi della Scuola di nail design di Ekaterina Miroshnichenko senza esperienza di lavoro come onicotecnica? </b> 
    <br />
I corsi sono di livello avanzato, raccomandiamo quindi di sviluppare delle capacità di base in corsi per principianti. Questo renderà più semplice imparare questo tipo di nail art.</p>
 
  <p> <b>Quali sono i corsi con cui è meglio iniziare? </b> 
    <br />
Raccomandiamo di iniziare con i corsi base, dove imparerai a lavorare con i gel colorati e svilupperai molta manualità, dopo questi corsi sarà semplice partecipare ai corsi di secondo e terzo livello di difficoltà.</p>
 
  <p> <b>Dove posso trovare il calendario del corsi? </b> 
    <br />
Puoi trovare maggiori dettagli sui corsi organizzati alla pagina <a href="/courses/#schedule" >“Calendario”</a>. </p>
 
  <p> <b>Quali competenze bisogna avere per aderire ai corsi? </b> 
    <br />
Accettiamo partecipanti specializzate con qualifica di estetista o onicotecnica.</p>
 
  <p> <b>Posso applicare i gel paint di E.Mi sullo smalto semi permanente di altri marchi? </b> 
    <br />
Si, i gel paint E.Mi possono essere applicati su altri smalti semi permanenti, ricorda però che la rimozione dello smalto semi permanente è effettuata con un solvete apposito, mentre i gel paint E.Mi devono essere rimossi con la fresa o la lima. Inoltre non possiamo garantire la corretta durata come quando utilizzano tutti i prodotti E.Mi</p>
 
  <p> <b>Posso applicare i gel paint di E.Mi sull’unghia naturale? </b> 
    <br />
Prima dell’applicazione dei gel paint E.Mi, l’unghia deve essere coperta con gel o smalto semi permanente. L’applicazione diretta sull’unghia <b>naturale è vietata</b>.</p>
 
  <p> <b>Qual è la differenza tra i gel paint E.Mi e EMPASTA? </b> 
    <br />
Le principali differenze sono la consistenza e la dispersione. E.Mi EMPASTA è più denso, le pennellate risultano quindi più precise, la texture di EMPASTA permette di realizzare disegni 3D. EMPASTA non rilascia dispersione, può quindi essere applicato sia sopra che sotto il Finish Gel. Inoltre la polimerizzazione di EMPASTA è di solo 2-5 secondi, questo permette di realizzare disegni complessi e molto ampi in breve tempo. I gel paint E.Mi sono più liquidi, ideali per gli ornamenti.</p>
 
  <p> <b>Qual è la differenza tra il Liquid stones polimero e Liquid stones gel? </b> 
    <br />
Il Liquid stones gel è un prodotto per disegnare “pietre liquide”. Permette di imitare le pietre preziose sulle unghie in diverse dimensioni e altezze senza richiedere l’applicazione di un successivo strato protettivo di top coat permettendo di risparmiare tempo. Il Liquid stones gel è trasparente e non può essere miscelato con i pigmenti. Il Liquid stones polimero imita le pietre colorate grazie alla possibilità di essere miscelato con i pigmenti, necessita inoltre la copertura con un top gel con strato di dispersione.</p>
 
  <p> <b>Come utilizzare EMPASTA Openwork Silver per incorniciare i decori? </b> 
    <br />
EMPASTA Openwork Silver contiene particelle trasparenti, questo rende necessario di mettere poco prodotto nella palette e lavorarlo bene; dopo questo far riposare il colore per alcuni giorni per ottenere una texture densa, adatta alla realizzazione di cornici. Lavorare con EMPASTA fresco è impossibile. Più si lascia riposare EMPASTA, meglio è. EMPASTA non si secca, è sempre utilizzabile e diventa perfetto per lavorare.</p>
 
  <p> <b>Di quali pennelli ho bisogno per lavorare i materiali E.Mi? </b> 
    <br />
Raccomandiamo 3 set di pennelli per lavorare con i prodotti E.Mi: Set di pennelli per Art Painting, Set di pennelli per tecnica One Stroke and set di pennelli Universali.</p>
 
  <p> <b>Come posso mantenere i pennelli belli per lungo tempo? </b> 
    <br />
Non utilizzare liquidi che contengono ethyl acetate per la pulizia, un fazzolettino bagnato è sufficiente.<b>I pennelli possono essere danneggiati dalla luce UV della lampada per unghie.</b></p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>