<?//if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog.php");

$ORDER = substr($_REQUEST["orderid"], 3);
$rcode = $_REQUEST["rcode"];

if ($rcode == 'OK')
{
	if (CModule::IncludeModule('sale')) {
		CSaleOrder::PayOrder($ORDER, "Y", True, True, 0, array("PAY_VOUCHER_NUM" => "555"));
	}
    ?>
    <h1>Successo!</h1>
    <h3>L'ordine #<?=$ORDER?> è stato pagato</h3>
    <?
}
else{
    ?>
    <h1>Errore!</h1>
    <h3>Il pagamento dell'ordine #<?=$ORDER?> non è andato a buon fine, se il problema persiste contattare l'assistenza</h3>
    <?
}

header("refresh: 6; url=https://".$_SERVER['HTTP_HOST']."/personal/order/detail/161".$ORDER."/");
?>