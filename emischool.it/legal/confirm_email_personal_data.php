<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Consent to personal data processing");

?>

    <div id="text_en">
        <p>
            In the case that you grant us your consent to the processing of your personal data, we will be able to contact you in the future with an offer of our products and services which correspond better to your needs. Provision of our products and services is, however, not conditioned by this consent. Of course, you can change your decision at any time and to withdraw your consent. Here we would like to state some information on the processing of your personal data.
        </p>
        <h4>1. TO WHOM DO YOU GRANT YOUR CONSENT TO THE PROCESSING OF YOUR PERSONAL DATA, OR WHO ARE WE?</h4>
        <p>You grant your consent to the processing of your personal data to the company:</p>
        <p>E.Mi – International s.r.o.</p>
        <p>Company Reg. No. 24214647</p>
        <p>Registered office: U božích bojovníků 89/1, Žižkov, 130 00 Prague 3</p>
        <p>Registered in the Commercial Register administered by the Municipal Court in Prague under the file ref. no. C 189332</p>

        <h4>2. FOR WHAT PURPOSE DO WE PROCESS YOUR PERSONAL DATA?</h4>
        <p>We process your personal data for marketing purposes which include the following activities:</p>
        <ul>
            <li>Participation in the loyalty programme,</li>
            <li>Informing about new products and special offers, as well as sending invitations to corporate events (exhibitions, E.MiDay); we can send you offers on the basis of your consent electronically, especially by way of e-mail messages or messages sent to a mobile device through a phone number, through the web client zone, in a written form or by way of phone calls,</li>
            <li>Automated processing of personal data with a view to adapting a commercial offer to your individual needs,</li>
            <li>Organising various surveys of satisfaction with our products and services.</li>
        </ul>

        <h4>3. WHAT PERSONAL DATA WILL WE PROCESS ON THE BASIS OF THIS CONSENT PROVIDED BY YOU?</h4>
        <p>On the basis of this consent we will process the following personal data:</p>
        <ul>
            <li>First name(s), surname, title,</li>
            <li>Place of residence/business,</li>
            <li>Date of birth,</li>
            <li>Data from the documents submitted,</li>
            <li>Payment data and payment behaviour data,</li>
            <li>Telephone and e-mail connection data,</li>
            <li>Information identifying the equipment or the person connected with the use of the equipment, namely the IP address.</li>
        </ul>


        <h4>4. WHY DO PROFILING AND AUTOMATED DECISION MAKING TAKE PLACE?</h4>
        <p>We try to provide you with the most suitable personalised offers of products and services and not to bother you with the offer of all our products. For this reason we profile your personal data on the basis of the consent granted by you, to which we use automatic information systems or web applications. In practice this means that still before offering a product or service to you, we at first assess, according to your personal data, what needs you could have and according to the results we will send you personalised offers of products and services.</p>
        <p>The automated assessment (profiling) of personal data helps us to get better knowledge of you and your needs and according to this fact also to adapt our products and services so that they can be tailored to you.</p>

        <h4>5. FOR HOW LONG WILL WE PROCESS YOUR PERSONAL DATA?</h4>
        <p>We will process your personal data for up to 10 years from the day when you provide this consent to the processing of your data to us.</p>

        <h4>6. CAN ANYBODY ELSE PROCESS YOUR PERSONAL DATA FOR US?</h4>
        <p>Your personal data are processed by us, however, in order to be able to fulfil the above mentioned purpose of the processing of your personal data, your personal data are processed for us also by the following processors – providers of web sites, e-mail, SMS correspondence, on-line chat, statistical and monitoring systems.</p>

        <h4>7. CAN YOU WITHDRAW YOUR CONSENT TO THE PROCESSING OF PERSONAL DATA?</h4>
        <p>Yes, you can withdraw your consent to the processing of your personal data at any time, by clicking on the corresponding field in your account.</p>
        <p>At the same time we state that the non-granting of this content or its withdrawal does not have any impact on provision of our products or services.</p>

        <h4>8. WHAT ARE YOUR RIGHTS IN RELATION TO THE PERSONAL DATA PROCESSED?</h4>
        <p>You have the right to:</p>
        <ul>
            <li>Access to your personal data – or you have the right to ask us for the information whether we process your personal data or not, and if yes, what of your personal data we process and for what purpose, to what other entities we make your personal data accessible, for how long your personal data will be stored at our company, whether automated decision making, including profiling, is carried out or not</li>
            <li>Explanation, correction or completion of your personal data – if you think that your personal data which we process are inexact, you have the right to require explanation and removal of the state which has arisen, especially through blocking, correction, completion or erasure of personal data</li>
            <li>Erasure of your personal data,</li>
            <li>Limitation of the processing of your personal data,</li>
            <li>Portability of your personal data – you have the right to obtain the personal data concerning you and which you have provided to us, in a structured, commonly used and machine-readable format, for the purpose of the transferring thereof to another data controller</li>
            <li>Contact the Office for Personal Data Protection – in the case that you have doubts about compliance with the obligations associated with the processing of your personal data, you have the right to contact the Office for Personal Data Protection (www.uoou.cz)</li>
        </ul>
        <p>If you think that your personal data are processed by us or by a processor authorised by us in contradiction with the purpose for which the data were provided to us, you have the right to raise an objection against the processing of personal data for the purpose of direct marketing, including profiling, as far as the direct marketing is concerned.</p>


        <h4>9. DECLARATION</h4>
        <p>You declare that if you are younger than 16, you have asked your legal guardian for the consent to the processing of your personal data.</p>
        <p>You declare that the personal data provided are true, were provided freely, knowingly, voluntarily and on the basis of your own decision.</p>

        <p>Additional information concerning the processing of personal data is provided for in the document <strong>Information Note on Processing of Personal Data</strong>. The current information concerning the processing of personal data, including the information about personal data recipients, is always stated <a target="_blank" href="https://emischool.com/legal/processing_personal_data.php">here</a></p>

        <form action="" method="post">
            <input type="submit" value="Confirm" class="btn btn-pink">

        </form>

    </div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>