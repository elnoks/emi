<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("CONDIZIONI GENERALI DI VENDITA");
?>

<p>CONDIZIONI GENERALI DI VENDITA
</p>


<p>INFORMAZIONI GENERALI</p>

<p>Le presenti Condizioni Generali di Vendita hanno per oggetto l'acquisto di prodotti effettuato a distanza tramite rete telematica sul sito www.emischool.it, appartenente a Calcagni S.r.l., con sede a Gallarate, Via Magenta, 29.<br>
Ogni operazione di acquisto sarà regolata dalle disposizioni di cui al DLgs. 185/99, DLgs. 206/05; le informazioni dirette alla conclusione del contratto saranno sottoposte all'art. 12 del DLgs. 70/03 e, per quanto concerne la tutela della ritezza, sarà sottoposta alla normativa di cui al DLgs. 196/03.</p>

<p>CONCLUSIONE DEL CONTRATTO E ACCETTAZIONE DELLE CONDIZIONI GENERALI DI VENDITA</p>

<p>I contratti di vendita dei prodotti sul sito www.emiscool.it , si considerano conclusi al momento in cui perviene l'ordine di acquisto effettuato dal cliente a Calcagni S.r.l. e quest’ultimo lo accetta. Calcagni S.r.l. invierà tempestivamente al cliente ricevuta dell'ordine di acquisto effettuato dal cliente.<br>
Il cliente, con l'invio telematico del proprio ordine d'acquisto, dichiara di aver preso visione e di aver accettato le presenti condizioni generali di contratto e si obbliga ad osservarle e rispettarle nei suoi rapporti con Calcagni S.r.l..</p>

<p>TRATTAMENTO DEI DATI PERSONALI

<p>Calcagni S.r.l. ai sensi dell’art. 13 del DLgs 196/2003 informa che i dati personali anagrafici e fiscali acquisiti in riferimento ai rapporti commerciali instaurati, forniti direttamente dagli interessati, ovvero diversamente acquisiti nell’ambito dell’attività della società, formeranno oggetto di trattamento nel rispetto della normativa richiamata, compresi gli obblighi di riservatezza previsti.<br>
In relazione ai predetti potranno essere esercitati i diritti di cui all’articolo 7 DLgs. 196/2003.</p>


<p>OBBLIGHI DEL CLIENTE</p>

<p>Il Cliente è tenuto, prima di inoltrare il proprio ordine d'acquisto, a leggere accuratamente le presenti condizioni generali di vendita. L'inoltro dell'ordine di acquisto implica la loro integrale conoscenza e la loro accettazione.<br>
Il Cliente è tenuto, infine, una volta conclusa la procedura d'acquisto on-line, a stampare e conservare le presenti condizioni generali di vendita, già visionate ed accettate durante la fase di conclusione del contratto.</p>

<p>DEFINIZIONE DELL'ORDINE</p>

<p>Con l'invio dell'ordine on-line, il Cliente trasmette a Calcagni S.r.l. un ordine del prodotto e/o dei prodotti inseriti nel carrello. Quando il Cliente effettua un ordine on-line per i prodotti che ha inserito nel carrello, accetta di acquistarli al prezzo e ai termini indicati nelle presenti Condizioni Generali di Vendita.<br>
Calcagni S.r.l. comunicherà al Cliente la conferma dell'ordine.</p>
<p>In particolare Calcagni S.r.l. non accetterà ordini:<br>

    se viene inviato con mezzi diversi dal sito e-commerce e/o mail<br>
    se il Cliente non può o non vuole pagare utilizzando carte di credito e bonifico bancario</p>
<p>MODALITA' D' ACQUISTO</p>

<p>Il cliente acquista il prodotto, le cui caratteristiche sono illustrate on-line nelle relative schede descrittive e tecniche, al prezzo ivi indicato a cui si aggiungono le spese di consegna e/o di gestione precisate sul sito. <br>
Prima dell'inoltro dell'ordine di acquisto viene riepilogato il costo unitario di ogni prodotto prescelto, il costo complessivo in caso di acquisto di più prodotti e le relative spese di consegna e/o gestione.<br>
Una volta inoltrato l'ordine di acquisto, il cliente riceverà da Calcagni S.r.l. un messaggio di posta elettronica attestante conferma di avvenuta ricezione dell'ordine di acquisto </p>

<p>PAGAMENTO</p>

<p>Il cliente può effettuare il pagamento dovuto scegliendo una fra le seguenti modalità elencate.<br>
- Pagamento con carta di credito: Nel caso in cui il consumatore intenda effettuare il pagamento tramite carta di credito, egli può avvalersi della procedura di pagamento elettronico Gestpay, idonea ad assicurare la riservatezza dei dati forniti dai clienti.<br>

- Pagamento con bonifico bancario: Il pagamento con bonifico bancario è effettuabile utilizzando i seguenti estremi:<br>

<p> Calcagni s.r.l.<br>
    Via Magenta 12, Gallarate <br>
	BANCA CREDEM <br>
    CIN: H<br>
    ABI: 03032<br>
    CAB: 50240<br>
    Numero Conto Corrente: 010000002644<br>
    IBAN: IT60H0303250240010000002644<br>
</p>

<p>Nel caso in cui il pagamento venga effettuato tramite bonifico bancario , il bene acquistato sarà spedito con le modalità riportate al successivo paragrafo “Consegna dei Prodotti”, all'indirizzo indicato dal cliente al ricevimento dell’accredito, quindi mediamente entro due/cinque giorni dopo l'effettuazione del bonifico (le tempistiche variano a seconda dell’Istituto di Credito utilizzato).</p>


<p>CONSEGNA DEI PRODOTTI</p>

<p>Il bene acquistato, unitamente alla relativa fattura, è consegnato tramite corriere all’indirizzo specificato dal Cliente al momento dell’ordine on-line.<br>
Calcagni S.r.l. garantisce la consegna del bene entro 5 giorni lavorativi a partire dalla data di ricezione della conferma dell’avvenuta transazione.<br>
Nel caso di mancato recapito per assenza del destinatario, all'indirizzo da lui indicato nell'ordine, il corriere lascerà un avviso e riproverà una seconda volta; se il destinatario risultasse ancora assente, la merce verrà riconsegnata al mittente (Calcagni S.r.l.).</p>

<p>GARANZIA DI CONFORMITA' E PRODOTTI DIFETTOSI</p>

<p>I prodotti acquistati sul sito www.emischool.it sono soggetti alla disciplina sulla vendita dei beni di consumo. I prodotti consegnati sono conformi alle caratteristiche illustrate on-line nelle relative schede descrittive e tecniche.<br>
Calcagni S.r.l. è responsabile nei confronti del cliente per i difetti di conformità esistenti al momento della consegna del bene.</p>

<p>DIRITTO DI RECESSO</p>

<p>Il consumatore ha diritto di esercitare il diritto di recesso. In particolare, il consumatore ha diritto di recedere da qualunque contratto concluso con Calcagni S.r.l., senza alcuna penalità e senza specificarne il motivo, entro il termine di 14 (quattordici) giorni lavorativi decorrenti dal ricevimento del bene.<br>
Il bene deve essere restituito in uno stato integro e rivendibile<br>
Il diritto di recesso è riconosciuto in capo al consumatore in relazione a qualsiasi bene da lui acquistato sul sito www.emischool.it.</p>

<p>MODALITA' PER L'ERSERCIZIO DEL DIRITTO DI RECESSO</p>

<p>Il diritto di recesso si esercita con l'invio, entro il suddetto termine, di una comunicazione scritta all'indirizzo della Calcagni S.r.l.mediante lettera raccomandata con avviso di ricevimento.<br>
La comunicazione di recesso può essere inviata, entro il medesimo termine, anche mediante telegramma o fax, a condizione che sia confermata a mezzo raccomandata con ricevuta di ritorno entro le 48 ore successive.<br>
In caso di esercizio del diritto di recesso, la comunicazione va effettuata al seguente indirizzo "Via Magenta,29 - 21013 - Gallarate - VA" o via fax al numero "0331 - 774505"<br>
Qualora sia avvenuta la consegna del bene, il cliente è tenuto a restituirlo a Calcagni S.r.l. entro il termine di 15 (quindici) giorni lavorativi decorrenti dalla data di consegna del bene.<br>
Il bene va restituito a Calcagni S.r.l. completo di tutto quanto in origine consegnato al cliente, nonché imballato nei suoi involucri originali. Al prodotto restituito va unita una copia della ricevuta elettronica dell'ordine. Le spese di restituzione del bene a Calcagni S.r.l. sono a carico del cliente.<br>
Se il diritto di recesso è esercitato dal cliente conformemente alle disposizioni contenute nella presente clausola, Calcagni S.r.l. è tenuta al rimborso delle somme versate dal cliente.<br>
In particolare, Calcagni S.r.l. procederà gratuitamente alla trasmissione dell'ordine di riaccredito relativo al costo del bene spedito comprensivo delle spese di spedizione entro 30 (trenta) giorni dalla data in cui è venuta a conoscenza dell'esercizio del diritto di recesso da parte del cliente. Questa operazione verrà effettuata tramite l'istituto di credito di emissione della carta di credito utilizzata per il pagamento ovvero accreditando la somma sul conto corrente bancario indicato dal cliente.<br>
Calcagni S.r.l. ha il diritto di respingere qualsiasi prodotto restituito con modalità diverse da quelle sopra specificate, così come i prodotti per i quali non siano state integralmente pagate dal cliente le spese di restituzione, oppure non siano state rispettate le modalità e i tempi indicati per la comunicazione dell'esercizio del diritto di recesso.</p>

<p>CLAUSOLA RISOLUTIVA ESPRESSA</p>

<p>In caso di mancato pagamento totale o parziale del prezzo di acquisto del bene Calcagni S.r.l. si riserva il diritto di dichiarare ai sensi e per gli effetti dell'art. 1456 del codice civile risolto il presente contratto mediante l'invio di una comunicazione scritta all'indirizzo elettronico del cliente.</p>

<p>RECLAMI</p>

<p>Per ogni eventuale reclamo o chiarimento, il cliente dovrà contattare il numero 0331/701442 o l'indirizzo e-mail italysale@emischool.com. Il cliente verrà ricontattato per chiarimenti entro 3 (tre) giorni lavorativi dalla richiesta.</p>

<p>LEGGE APPLICABILE E FORO COMPETENTE</p>

<p>Il presente contratto è regolato dalla legge italiana. La competenza e il foro esclusivo per ogni eventuale azione legale intentata dall’Acquirente ai sensi della presente garanzia o di eventuali ulteriori garanzie legali sarà il Tribunale di Busto Arsizio.</p>

<p>RINVIO</p>

<p>Per quanto non espressamente previsto nel presente contratto si applicano le norme di legge vigenti in Italia.</p>

</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>