<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("About company");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>About company</h1>
   </div>
 
  <div class="bx_page" style="text-align: center;"><img src="/upload/medialibrary/it/Top.jpg" title="Top banner" border="0" alt="56.jpg" width="1140" height="300"  /> 
    <br />
   </div>
 
  <br />
 
  <p>E.Mi presenta migliaia di soluzioni pronte all’uso, esclusive e sempre alla moda, sviluppate da  Ekaterina Miroshnichenko, campionessa mondiale di nail design, materiali unici e accessori per  nail art creati con la sua collaborazione.</p>
 
  <p>La mission E.Mi è supportare le onicotecniche per aiutarle a esprimere il loro potenziale, farle sentire delle artiste e farle diventare delle vere esperte di tendenze! Imparare a creare i fantastici decori E.Mi è possibile presso la Scuola di nail design di Ekaterina Miroshnichenko.</p>
 
  <p>E.Mi si sta espandendo velocemente ampliando il proprio mercato: anche tu puoi farne parte! Attualmente, E.Mi comprende cinquanta rappresentanti ufficiali della Scuola di nail design di Ekaterina Miroshnichenko in Russia, Europa e Asia, distributori ufficiali E.Mi e centinaia punti vendita in tutto il mondo.</p>
 
  <h2>SOLUZIONI PRONTE ALL’USO&amp;NAIL&amp;MODA</h2>
 
  <p><img src="/upload/medialibrary/cd4/cd48dcf71294554d8839eab0a34a4986.jpg" title="98.jpg" border="0" alt="54.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>L’alta moda è vera arte. Abbaglia, incoraggia alla sperimentazione facendoti guardare a nuove stagioni sempre più sorprendenti e meravigliose.</p>
 
  <p>Le soluzioni pronte all’uso di E.Mi sono l’alta moda della nail art. Durante l’anno presentiamo varie collezioni esclusive create da Ekaterina Miroshnichenko, campionessa mondiale di nail design nella  categoria Fantasy presso MLA (Parigi, 2010), due volte campionessa europea (Atene, Parigi, 2009), giudice internazionale e fondatrice della propria Scuola di nail design.</p>
 
  <p>Ogni collezione di Ekaterina è un’interpretazione originale delle ultime tendenze, che vengono riproposte in nail art e calibrate per i saloni di bellezza: incredibilmente belle e uniche al primo sguardo, sono semplici da realizzare dopo il corso di formazione.</p>
 
  <p>Alcuni delle collezioni più vendute sono Pittura Zhostovo, Pittura One Stroke, Imitation of reptile skin, Crackled effect, Ethnic Prints, Velvet sand and liquid. Lasciati ispirare da tutti i decori di Ekaterina Miroshnichenko!</p>
 
  <h2>PRODOTTI PER NAIL DESIGN</h2>
 
  <p><img src="/upload/medialibrary/6e3/6e3b48a3e4405516b24de034f7ca5b60.jpg" title="6542.jpg" border="0" alt="6542.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Ogni onicotecnica, dopo aver provato i prodotti E.Mi, può affermare con sicurezza: “Questi prodotti sono fatti per me!”</p>
 
  <p>Il segreto di questo colpo di fulmine è semplice: Ekaterina Miroshnichenko prende parte personalmente nella creazione di ogni prodotto, ogni nuovo colore e accessorio. Una onicotecnica, una campionessa mondiale, una rispettata esperta di nail art che realizza nei prodotti E.Mi, i prodotti con cui lei stessa vorrebbe lavorare. Questo è il motivo per cui il prodotti E.Mi soddisfano tutte le esigenze di ogni nail designer, tengono in considerazione tutte le peculiarità di questo lavoro, premettono di risparmiare tempo e garantiscono grandi possibilità creative.</p>
 
  <p>Alcuni dei prodotti esclusivi sono EMPASTA, GLOSSEMI, TEXTONE, PRINCOT, in più ci sono le collezioni di gel paints, foil dei colori più di tendenza, una grande varietà di materiali per la decorazione e accessori. Trova subito i prodotti perfetti per te!</p>
 
  <h2>SCUOLA DI NAIL DESIGN DI EKATERINA MIROSHNICHENKO</h2>
 
  <p><img src="/upload/medialibrary/it/minibanner.jpg" title="minibanner.jpg" border="0" alt="765.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Se sogni una professione interessante e creativa, se vuoi che i tuoi clienti siano felici e che tornino nel tuo salone, partecipa ai corsi di formazione della Scuola di nail design di Ekaterina Miroshnichenko.</p>
 
  <p>La Scuola di nail design di Ekaterina Miroshnichenko offre programmi di formazione originali per nail designer, elaborati appositamente per onicotecniche che non hanno una base conoscitiva di nail art. I metodi unici di Ekaterina Miroshnichenko sono il fondamento di tutti i corsi di nail design.</p> 
 
  <p>Trova sul nostro sito la scuola a te più vicina e fai diventare il tuo sogno realtà!</p>
     
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>