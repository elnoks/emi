<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как заказать");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Come fare un ordine</h1>
   </div>
 </div>
 
<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

 
<div class="bx_page"> 
  <p>1.	Sono accettati solo gli ordini inviati tramite il sito e-commerce o via e-mail. Gli ordini inoltrati telefonicamente non saranno presi in carico. 
    <br />
   </p>
 
  <p>2.	Per evitare ogni tipo di problema nella spedizione chiediamo gentilmente di controllare i tuoi dati personali e di contatto prima di procedere con l’ordine. Dati richiesti: nome e cognome, numero di telefono comprensivo di prefisso internazionale (Italia +39), indirizzo email e di consegna della merce (via/piazza, numero civico, città, CAP, nazione). 
    <br />
   </p>
 
  <p>3.	In caso di errori l’azienda non è responsabile per ogni spesa aggiuntiva derivante da consegne extra dovute alla fornitura di un indirizzo errato.
    <br />
   </p>
  
  <p>4.	Alla conclusione dell’ordine, il cliente riceverà in modo automatico una notifica via email con un riepilogo dell’ordine incluse le spese di trasporto. Potete procedere al pagamento direttamente dal sito E.Mi nella sezione ordini dopo esservi autenticati.
    <br />
   </p>
 
  <p>5.	È possibile procedere con il pagamento dell’ordine entro 3 giorni lavorativi a partire dalla data di ricezione della mail. In caso di mancato pagamento nei termini stabiliti l’ordine viene automaticamente annullato.
    <br />
   </p>
  <p>6.	Se non riesci a completare il pagamento entro i 3 giorni lavorativi stabiliti ti chiediamo gentilmente di contattare i gestori del sito e-commerce via email italy-sale@emischool.com o telefonicamente 0331 701442 in anticipo. 
    <br />
   </p>
  <p>7.	Prima di procedere con il pagamento ti chiediamo di controllare se i prodotti e il totale dell'ordine sono corretti.
    <br />
   </p>
   </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>