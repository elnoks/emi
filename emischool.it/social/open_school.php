<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("How to open the E.Mi school representative office");
?> 
<div class="bx_page"> 
  <p style="text-align: center;"><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" border="0" alt="765.jpg" width="800" height="105"  /><b>
      <br />
    </b></p>

  <p><b>
      <br />
    </b></p>

  <p> <b>Collaborare con la Scuola di nail design di Ekaterina Miroshnichenko è un’opportunità per avviare un’attività commerciale pre composta: riceverai una gamma completa di strumenti che variano dai prodotti di vendita, a quelli per la gestione alle promozioni. La Scuola di nail design di Ekaterina Miroshnichenko presenta collezioni di tendenza, di grande richiesta e dallo stile inconfondibile.</b></p>
 
  <p> <b>Diventa nostro rappresentante e ti insegneremo a essere creativi e di successo come noi! </b>  La Scuola di nail design di Ekaterina Miroshnichenko è conosciuta in tutto il mondo: vittorie conseguite presso prestigiose competizioni internazionali, seminari in Germania e Italia, apertura di un ufficio internazionale per gestire il mercato europeo. 
    <br />
    La Scuola di nail design di Ekaterina Miroshnichenko è conosciuta in tutto il mondo: vittorie conseguite presso prestigiose competizioni internazionali, seminari in Germania e Italia, apertura di un ufficio internazionale per gestire il mercato europeo. </p>
 
  <p> <b>Unisciti a chi è di tendenza! </b> La Scuola di nail design di Ekaterina Miroshnichenko crea strumenti di decorazione inimitabili e corsi di formazione unici.
    <br />
   <b>Creatività e bellezza sono possibili con noi! </b>
    <br />
   <b>I vantaggi della Scuola di nail design di Ekaterina Miroshnichenko:</b> </p>

  <ul> 
    <li>Servizi di formazione di grande richiesta. Più di 500 insegnanti qualificati in tutta la Russia e nei paesi esteri partecipano a corsi di aggiornamento annualmente;</li>
   
    <li>I prodotti a marchio <a href="/catalog/" > E.Mi</a> sono utilizzati durante i corsi;</li>
   
    <li>L’azienda ha un ampio respiro internazionale, l’ufficio europeo ha sede a Praga;</li>
   
    <li>I corsi di formazione sono basati su metodi unici, sviluppati e brevettati da Ekaterina Miroshnochenko, campionessa mondiale di nail design (OMC Parigi, 2011);</li>
   </ul>
 
  <p></p>
 
  <p> <b>Formiamo i nostri rappresentanti con tutti i materiali necessari per raggiungere il successo  anche nell’ambito della formazione.
      <br />
     L’apertura di una sede di rappresentanza della Scuola di nail designs ti garantisce i seguenti vantaggi:</b> </p>

  <ol> 
    <li><b>Il diritto esclusivo </b>di presentarti con il nome di Scuola di nail design di Ekaterina Miroshnichenko e fornire corsi di formazione nel territorio di tua competenza. </li>
   
    <li><b>I corsi di formazione </b>sono sviluppati da Ekaterina Miroshnichenko. Grazie al centro di formazione risparmierai tempo, risorse economiche e materiali: potendoti concentrare esclusivamente sulla ricerca degli studenti riuscirai in breve tempo a compensare l’investimento iniziale.</li>
   
    <li><b>Un sistema unificato di certificazione. </b>L’ufficio di rappresentanza ha il diritto di rilasciare ai propri studenti il certificato di partecipazione con l’ologramma della Scuola di nail design di Ekaterina Miroshnichenko.</li>
   
    <li><b>Il diritto di avvalersi di uno sconto esclusivo sui prodotti <a href="/catalog/" >E.Mi</a>.</b></li>
   
    <li><b>Supporto promozionale. </b> La scuola fornisce tutte le informazioni riguardanti i suoi rappresentanti nei magazine professionali di settore, nel catalogo prodotti e sul sito ufficiale; fornisce supporto per la partecipazione a fiere locali e conferenze di settore, fornisce layout predefiniti per la pubblicità su magazine locali.</li>
   </ol>
 
  <p></p>
 
  <p> <b>La collaborazione in qualità di rappresentante ufficiale della Scuola di nail design di Ekaterina Miroshnichenko e distributore ufficiale dei prodotti Emi. </b>
    <br />
  I rappresentanti ufficiali della Scuola di nail design di Ekaterina Miroshnichenko vanta un diritto di prelazione sulla distribuzione dei prodotti a marchio E.Mi così come specificato nei termini di contratto. In questo caso, sarà necessario variare le condizioni di approvvigionamento della merce, con un aumento rilevante di sconto sui prodotti E.Mi. </p>
 
  <p> <b>I requisiti per diventare rappresentante ufficiale della Scuola di nail design di Ekaterina Miroshnichenko:</b> </p>

  <ol> 
    <li>Avere un insegnante talentuoso, capace di imitare perfettamente le tecniche di Ekaterina Miroshnichenko. Il candidato affronterà un periodo di apprendistato al temine del quale, se ritenuto idoneo agli standard qualitativi di Ekaterina Miroshnichenko, potrà partecipare a uno speciale corso per insegnanti.</li>
   
    <li>Avare la disponibilità di un locale di almeno 16 mq da dedicare esclusivamente ai corsi E.Mi. Sarò necessario esporre un’insegna marchiata E.Mi secondo lo stile peculiare del brand.</li>
   
    <li>Il rappresentante deve essere titolare di una società o di una partita Iva idonea all’apertura di un centro per la formazione.</li>
   
    <li>È necessario disporre del capitale per il primo acquisto di prodotti, per l’equipaggiamento del centro e per sovvenzionare la formazione di un insegnante.</li>
   
    <li>Il rappresentante deve avere l’obiettivo di attrarre nuovi apprendisti interessati ai corsi sulla base delle tecnologie sviluppate.</li>
   </ol>
 
  <p></p>
 
  <p> <b>Gentili insegnanti, se siete realmente interessati all’apertura di una sede di rappresentanza della Scuola nel vostro Paese, prima di procedere dovete:</b> </p>

  <ol> 
    <li>Sapere se nel vostro Paese è già presente una Scuola E.Mi. Per avere questa informazione potete contattarci attraverso il nostro sito web oppure chiamarci al numero(863) 255 73 67. Nel caso in cui fosse già presente una Scuola nel vostro Paese, saremmo tenuti a declinare la vostra proposta.</li>
   
    <li>Inviare le fotografie di nail tip decorate con disegni di Ekaterina Miroshnichenko all’indirizzp email sviridova@emi-school.ru (esempio, Sophisticated Ornamenti floreali, Fragole, Lamponi, Ciliege, ecc.). È necessario per la candidatura come insegnante, poiché lo stile dell’insegnante deve assolutamente corrispondere alle esigenze di Ekaterina Miroshnichenko, pertanto bisogna copiare accuratamente i suoi disegni e le sue tecniche.</li>
   
    <li><b>Per conoscere la retta per i corsi di formazione chiamare il numero +420 722935746. </b> La formazione degli insegnanti si svolge in due tempi: come apprendista e come istruttore. I corsi si svolgono esclusivamente presso il training centre di Rostov-on-Don. Il costo è direttamente proporzionale alla quantità dei corsi frequentati.</li>
   
    <li><b>Per conosce la quantità di prodotti da acquistare chiamare il numero +420 722935746. </b> Il volume di acquisto è determinato dal contratto.</li>
   
    <li>Essere pronti a frequentare regolarmente corsi avanzati. Il contratto di istruttore ha la durata di un anno. Alla scadenza è necessario riconfermare la qualificazione a Rostov-on-Don e rinnovare il contratto per l’anno successivo.</li>
   </ol>
 
  <p></p>
 
  <p> <b>Per ulteriori informazioni sull’apertura di un ufficio di rappresentanza della Scuola di nail design di Ekaterina Miroshnichenko, chiamare l’ufficio internazionale: 
      <br />
Korbut Maria, korbut@emischool.com, +420 722935746</b> </p>
 
  <br />
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>