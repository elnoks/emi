<?php
	require_once("../db_connect.php");
	//менять здесь
	$onlyCityId = '1011';
	$onlyCityName = 'Полтава';

	$cities = array();
	$sql = 'SELECT * FROM `cities` WHERE (id=' . $onlyCityId . ') ORDER BY name ASC';
	$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));
	while ($res = mysqli_fetch_assoc($result)) {
		$cities[$res["id"]] = $res["name"];
	}
	
	$courses = array();
	$sql = 'SELECT * FROM `courses` ORDER BY name ASC';
	$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));
	while ($res = mysqli_fetch_assoc($result)) {
		$courses[$res["id"]] = $res["name"];
	}
	
	//для замены номера Яндекс счетчика
	$yaIdent = '42555864';
?>
<!DOCTYPE html>
<html lang="ru" xmlns:og="http://ogp.me/ns#">
<head>
<meta charset="utf-8" >
<meta http-equiv="Content-Language" content="ru">
<title>Курсы маникюра</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="3715d3985edfd481c4a1b039d6e959d7" />
<link href="https://fonts.googleapis.com/css?family=Marmelad&amp;subset=cyrillic" rel="stylesheet">
<link href="../css/bxslider.css" rel="stylesheet" />
<link href="../css/s.css" rel="stylesheet" />
<link rel="icon" href="../i/favicon.png" type="image/x-icon">

<script src="../js/jquery.min.js"></script>
<script src="../js/maskedinput.min.js"></script>
<script src="http://api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script>
	var cities = [];
	<?php
	echo 'cities["'.$onlyCityName.'"] = '.$onlyCityId.';';
	/*foreach($cities as $key => $value){
		echo 'cities["'.$value.'"] = '.$key.';';
	}*/
	?>
</script>


</head> 
<body>

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KVKBZVN" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- navigation -->

	<nav role="navigation">
		<div class="c">
			<a class="menu"><span>МЕНЮ</span></a>
			<img src="../i/logo.png" alt="" class="logo" />
			<div class="links">
				<a href="#about">От создателей</a>
				<div class="separator">•</div>
				<a href="#kyrsi">Курсы</a>
				<div class="separator">•</div>
				<a href="#advantages">Преимущества</a>
				<div class="separator">•</div>
				<a href="#program">Программа курса</a>
				<div class="separator hidden">•</div>
				<br />
				<a href="#why">Почему идут учиться к нам</a>
				<div class="separator">•</div>
				<a href="#fast-start">Быстрый старт</a>
				<div class="separator">•</div>
				<a href="#reviews">Отзывы</a>
				<div class="separator">•</div>
				<a href="#order">Записаться на курсы</a>       
			</div>
		</div>
	</nav>

<!-- header -->
	<div id="top"></div>
	<header>
		<div class="c">
			<img src="../i/mobile_logo.png" alt="" class="mobile_logo" />
			<h1>Станьте мастером маникюра <br />всего за 10 дней!</h1>
			<div class="desc">Получите профессию, которая принесет вам стабильный <br />доход и постоянных клиентов</div>
			<a href="#order" class="btn">Записаться на курсы</a>
		</div>
		
	</header>
	
<!-- Блок  -->	
	<div id="about"></div>
	<section id="s1">
		<div class="c">
			<img src="../i/s1_img.jpg" alt="" class="img" />
			<div class="text">Вера и Екатерина Мирошниченко – создатели курсов, эксперты и лидеры нейл-индустрии, основатели бренда E.Mi и <span>60 Школ</span> в <span>23 странах</span> мира. 
<br />По их авторским методикам и учебным программам успешно работают 
<span class="big">20 000</span>
<span>мастеров ногтевого сервиса</span></div>
		</div>
	</section>	
	
<!-- Блок  -->	
<section id="s3" style="margin-top: -90px; padding-bottom: 20px">
	<div class="c" align="center" style="width: 100%">
		<video id="movie" poster="../video/video_preview.png" width="854px" height="540px" style="margin: 0 12%; height: calc((0.5vh + 0.5vw)*2*20); width: calc((0.5vh + 0.5vw)*2*34); max-width: 76%">
			<source src="../video/manicure_courses.mp4"></source>
		</video>
	</div>
    <script>
        var v = document.getElementById("movie");
        var status_play = false;

        v.onclick = function() {
            if (v.paused) {
                v.play();
            } else {
                v.pause();
                status_play = true;
            }
        };
        v.addEventListener('contextmenu', function(ev) {
            ev.preventDefault();
            return false;
        }, false);

        function scrollAppear(elem) {

            var docViewTop = $(window).scrollTop(),
                docViewBottom = docViewTop + $(window).height(),
                elemTop = $(elem).offset().top,
                elemBottom = elemTop + $(elem).height();

            return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
        }
        window.onscroll = function() {
            if (status_play == false){
                if(scrollAppear("#s3")){
                    v.play();
                }
            }
        }
    </script>
</section>
	
<!-- Блок  -->	

	<section id="s2">
		<div class="c">
			<form id="ordermaster" action="send.php" method="POST">
				<div class="small">курс</div>
				<h2>Мастер маникюра</h2>
				<div class="text">Оставьте заявку, чтобы записаться в группу <br />или задать вопрос администратору!</div>
				<input type="hidden" name="city" value="<?=$onlyCityId?>" />
				<input type="text" name="name" placeholder="Ваше имя" required />
				<input type="text" id="phone1" name="phone" placeholder="Телефон" required />
				<input type="text" name="email" placeholder="Email" />
				<input type="hidden" name="course" value="Мастер маникюра" />
				<input type="hidden" name="date" value="" />
				<input type="hidden" name="time" value="" />
				<button class="btn" id="sendorder1">Записаться на курс</button>
			</form>
		</div>
	</section>	
	
<!-- Блок  -->	
	<div id="advantages"></div>
	<section id="s4">
		<div class="c">
			<h2>Преимущества курса</h2>
			<div class="slider">
				<ul class="flex">
					<li class="block">
						<img src="../i/s4_icon1.png" alt="" class="icon" />
						<div class="title">Быстрое <br />и гарантированное <br />трудоустройство</div>
						<img src="../i/s4_line.png" alt="" class="line" />
						<div class="text">Наши ученики находят <br />работу и первых клиентов <br />через 10 дней</div>
					</li>
					<li class="block">
						<img src="../i/s4_icon2.png" alt="" class="icon" />
						<div class="title">80%<br />практики</div>
						<img src="../i/s4_line.png" alt="" class="line" />
						<div class="text">Практика на клиентах <br />на каждом занятии</div>
					</li>
					<li class="block">
						<img src="../i/s4_icon3.png" alt="" class="icon" />
						<div class="title">Обучение <br />на продукции E.Mi</div>
						<img src="../i/s4_line.png" alt="" class="line" />
						<div class="text">По авторским <br />методикам с опытными <br />преподавателями</div>
					</li>
					<li class="block">
						<img src="../i/s4_icon4.png" alt="" class="icon" />
						<div class="title">Престижный <br />диплом</div>
						<img src="../i/s4_line.png" alt="" class="line" />
						<div class="text">E.Mi и свидетельство <br />по стандартам министерства <br />образования Украины</div>
					</li>
				</ul>
			</div>
		</div>
	</section>	
	
<!-- Блок  -->	

	<section id="s5">
		<div class="c">
				<div class="block">
					<img src="../i/s5_img1.jpg" alt="" />
					<div class="inner">
						<div class="title">Вы сможете делать все <br />виды современного маникюра</div>
						<div class="text">Обрезной, аппаратный, комбинированный, мужской, детский</div>
					</div>
					<div class="clr"></div>
				</div>
				<div class="block right">
					<img src="../i/s5_img2.jpg" alt="" />
					<div class="inner">
						<div class="title">Вы сможете идеально <br />наносить цвет </div>
						<div class="text">Делать ровное однотонное покрытие <br />под кутикулу , френч, руффиан и омбре</div>
					</div>
					<div class="clr"></div>
				</div>
				<div class="block">
					<img src="../i/s5_img3.jpg" alt="" />
					<div class="inner">
						<div class="title">Вы сможете быстро <br />и эффективно работать</div>
						<div class="text">Делать комплексный маникюр <br />с покрытием всего за 1 час</div>
					</div>
					<div class="clr"></div>
				</div>
				<div class="block right">
					<img src="../i/s5_img4.jpg" alt="" />
					<div class="inner">
						<div class="title">Вы сможете <br />хорошо зарабатывать</div>
						<div class="text">Выпускники востребованы в ведущих салонах красоты и быстро нарабатывают клиентов</div>
					</div>
					<div class="clr"></div>
				</div>
		</div>
	</section>	
	
<!-- Блок  -->	
	<div id="program"></div>
	<section id="s6">
		<div class="c">
			<h2><div>Программа курса</div>«Мастер маникюра»</h2>
			<div class="flex menu" id="bx-pager">
				<a data-slide-index="0">Тема 1</a>
				<a data-slide-index="1">Тема 2</a>
				<a data-slide-index="2">Тема 3</a>
				<a data-slide-index="3">Тема 4</a>
				<a data-slide-index="4">Тема 5</a>
				<a data-slide-index="5">Тема 6</a>
			</div>
			<div class="slider">
				<ul>
					<li>
						<div class="inner">
							<h3>Как эффективно организовать свою работу?</h3>
							<div class="text">
								Маникюрные услуги от А до Я: история, современные тренды, классификация салонов красоты.<br />
								Оснащение рабочего места мастера маникюра. Идеальный набор оборудования, материалов и инструментов успешного мастера. Помощь в подборе оптимальных инструментов для вашей работы.<br /><br />
								Как рассчитать время на  услугу? Себестоимость процедур, ценообразование. Как составить грамотный прайс?<br /><br />
								Практика<br /><br />
								* Примерное содержание программы. На каждую тему отведено несколько дней.
							</div>
							<a href="#fast-start" class="btn">Расписание</a>
						</div>
					</li>
					<li>
						<div class="inner">
							<h3>Базовые знания анатомии и физиологии для успешной практики</h3>
							<div class="text">
								Строение верхних конечностей. Строение кисти. Строение кожи. Заболевания кожи. Строение ногтя. Формы свободного края. Заболевания ногтей. Дистрофия. Дисхромия.<br /><br />
								Практика<br /><br />
								* Примерное содержание программы. На каждую тему отведено несколько дней.
							</div>
							<a href="#fast-start" class="btn">Расписание</a>
						</div>
					</li>
					<li>
						<div class="inner">
							<h3>Как пройти СЭС?</h3>
							<div class="text">
								Основы дезинфекции и санитарии. Порядок дезинфекции и стерилизации в салоне. Требования СЭС.<br /><br />
								Практика на моделях<br /><br />
								* Примерное содержание программы. На каждую тему отведено несколько дней.
							</div>
							<a href="#fast-start" class="btn">Расписание</a>
						</div>
					</li>
					<li>
						<div class="inner">
							<h3>Технологии маникюрных услуг</h3>
							<div class="text">
								Основные этапы маникюра. Классический маникюр. Маникюр на проблемную кутикулу.  Европейский маникюр. Аппаратный маникюр. Смешанный (комбинированный) маникюр. Мужской маникюр. Детский маникюр.<br /><br />
								Практика на моделях<br /><br />
								* Примерное содержание программы. На каждую тему отведено несколько дней.
							</div>
							<a href="#fast-start" class="btn">Расписание</a>
						</div>
					</li>
					<li>
						<div class="inner">
							<h3>Идеальное покрытие под кутикулу</h3>
							<div class="text">
								Лаковые покрытия. Технология выполнения классического и французского покрытия лаком.<br />
								Гель-лаковые покрытия. Лунное, градиентное и Ruffian покрытие.<br /><br />
								Практика на моделях<br /><br />
								* Примерное содержание программы. На каждую тему отведено несколько дней.
							</div>
							<a href="#fast-start" class="btn">Расписание</a>
						</div>
					</li>
					<li>
						<div class="inner">
							<h3>Спа-маникюр и массаж</h3>
							<div class="text">
								Спа-маникюр. Парафинотерапия. Ремонт ногтя. Полировка. Технология выполнения массажа.<br /><br />
								Практика на моделях<br /><br />
								* Примерное содержание программы. На каждую тему отведено несколько дней.
							</div>
							<a href="#fast-start" class="btn">Расписание</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</section>	
	
<!-- Блок  -->	
	<div id="why"></div>
	<section id="s7">
		<div class="c">
			<h2>Почему идут учиться к нам?</h2>
			<div class="flex">
				<div class="block">
					<div class="title">20 000</div>
					<div class="text">мастеров уже<br />прошли обучение и стали <br />востребованными</div>
				</div>
				<div class="block">
					<div class="title">53</div>
					<div class="text">учебных центра Украины работают <br />по авторским методикам <br />и единым стандартам</div>
				</div>
				<div class="block">
					<div class="title">11 000</div>
					<div class="text">ведущих салонов <br />красоты уже сотрудничают <br />с нами</div>
				</div>
			</div>
		</div>
	</section>
	
<!-- Блок  -->	
	<div id="kyrsi"></div>
	<section id="s3">
		<div class="c">
			<h2>Другие курсы</h2>
			<div class="flex">
				<div class="block" style="background-image:url(../i/s3_img3.jpg);">
					<div class="inner">
						<div class="pretitle">курс</div>
						<div class="title"><span>День открытых дверей</span></div>
						<div class="text">Бесплатно познакомьтесь<br /> со Школой и преподавателями!</div>
						<a href="#order" class="btn">Записаться</a>
					</div>
				</div>
				<div class="block" style="background-image:url(../i/s3_img4.jpg);">
					<div class="inner">
						<div class="pretitle">курс</div>
						<div class="title long"><span>Бесплатное знакомство </span><br /><span>с маркой E.Mi</span></div>
						<div class="text">Тест-драйв материалов и технологий.<br />Попробуйте и решите!</div>
						<a href="#order" class="btn">Записаться</a>
					</div>
				</div>
				<div class="block" style="background-image:url(../i/s3_img1.jpg);">
					<div class="inner">
						<div class="pretitle">курс</div>
						<div class="title"><span>Маникюр+педикюр</span></div>
						<div class="text">Хотите освоить профессию комплексно <br />и зарабатывать еще больше?</div>
						<a href="#order" class="btn">Записаться</a>
					</div>
				</div>
				<div class="block" style="background-image:url(../i/s3_img2.jpg);">
					<div class="inner">
						<div class="pretitle">курс</div>
						<div class="title"><span>Мастер педикюра</span></div>
						<div class="text">Хотите, чтобы клиенты выстраивались к вам <br />в очередь на аппаратный педикюр?</div>
						<a href="#order" class="btn">Записаться</a>
					</div>
				</div>
				<div class="block" style="background-image:url(../i/s3_img3.jpg);">
					<div class="inner">
						<div class="pretitle">курс</div>
						<div class="title long"><span>Современные </span><br /><span>методики маникюра</span></div>
						<div class="text">Вы мастер маникюра, но обучались давно и хотите <br />освежить свои знания и быть в тренде?</div>
						<a href="#order" class="btn">Записаться</a>
					</div>
				</div>
				<div class="block" style="background-image:url(../i/s3_img4.jpg);">
					<div class="inner">
						<div class="pretitle">курс</div>
						<div class="title long"><span>Салонный </span><br /><span>маникюр с E.MiLac</span></div>
						<div class="text">Хотите делать маникюр с актуальным <br />покрытием быстро, легко и просто?</div>
						<a href="#order" class="btn">Записаться</a>
					</div>
				</div>
			</div>
		</div>
	</section>	
	
<!-- Блок  -->	
	<div id="fast-start"></div>
	<section id="s8">
		<div class="c">
			<h2>Быстрый старт группы</h2>
			<div class="inner">
				<div class="opener">
					<span class="js-choosen-course"></span>
					<div class="right"><span>Ещё курсы</span></div>
				</div>
				<div class="opening_window js-courses"></div>
				
				<div class="opener">
					<span class="js-choosen-city"></span>
					<div class="right"><span>Все города</span></div>
				</div>
				<div class="opening_windoww js-citiess"></div>
				<div class="info">
					<div class="course_info">
						<div class="block block1">
							Дата
							<div class="date">
								<span>9.09</span>
								<div>9 октября 2016</div>
							</div>
						</div>
						<div class="block block2">
							Время проведения курса
							<div class="time">
								<div class="digit digit0">1</div>
								<div class="digit digit1">3</div>
								<div class="digit dots">:</div>
								<div class="digit digit3">0</div>
								<div class="digit digit4">0</div>
							</div>
							<div class="clr"></div>
						</div>
						<div class="block block3">
							Выбрать другую дату
							<div class="days">
								<select name="other_date" id="all_dates">
									<option selected disabled>Выберите дату</option>
								</select>
								<!--
								<div class="day day1">ПН</div>
								<div class="day day2">ВТ</div>
								<div class="day day3">СР</div>
								<div class="day day4">ЧТ</div>
								<div class="day day5">ПТ</div>
								<div class="day day6">СБ</div>
								<div class="day day7">ВС</div>
								-->
							</div>
						</div>
						<div class="clr"></div>
					</div>
					<div class="course_no_info">
						Извините, на ближайшее время доступных курсов нет
					</div>
				</div>
				<a href="#order" class="btn">Записаться</a>
			</div>
			
			<div class="s8_end">
				<h3>Не нашли подходящего варианта?</h3>
				<div class="text">Закажите звонок, чтобы подобрать дату старта!</div>
				<a class="btn">Заказать звонок</a>
			</div>
		</div>
	</section>	
	
<!-- Блок  -->	
	<div id="reviews"></div>
	<section id="s9">
		<div class="c">
			<h2>Отзывы</h2>
			<div class="slider">
				<ul>
					<li>
						<div class="review">
							<img src="../i/foto1.jpg" alt="" />
							<div class="inner">
								<a>Читать полностью</a>
								<h3>Lenok Artlab</h3>
<!--								<div class="work">г.Москва</div>-->
								<div class="text">Школа EMI, вот я и получила свой первый сертификат, замечательная школа, замечательный колектив, огромнейшее спасибо лучшему преподавателю!! Школа@emi_moscow я к вам ещё вернусь и не раз! ЛУЧШАЯ ШКОЛА МАНИКЮРА!!!!</div>						</div>
						</div>
						<div class="review">
							<img src="../i/foto2.jpg" alt="" />
							<div class="inner">
								<a>Читать полностью</a>
								<h3>palina_leonidovna</h3>
<!--								<div class="work">г. Москва</div>-->
								<div class="text">Аааааеееееее!!))) Он мой!))) спасибо вам большое!!! Вы-замечательный преподаватель! Всё понятно, доступно и с душой-суперрр!!! Спасибо Вам большое!</div>
							</div>
						</div>
					</li>
					<li>
						<div class="review">
							<img src="../i/foto3.jpg" alt="" />
							<div class="inner">
								<a>Читать полностью</a>
								<h3>maria_tolmacheva55</h3>
<!--								<div class="work">г. Омск</div>-->
								<div class="text">Курс " Салонный маникюр " пройден!!! Наконечная Татьяна спасибо большое!!! И всему коллективу @emischool_omsk , Вы профессионалы своего дела!</div>
							</div>
						</div>
						<div class="review">
							<img src="../i/foto4.jpg" alt="" />
							<div class="inner">
								<a>Читать полностью</a>
								<h3>Натусик Иртуганова</h3>
<!--								<div class="work">г. Ростов-на-Дону</div>-->
								<div class="text">Забрала наконец свой диплом по педикюру, теперь я мастер-универсал, спасибо огромное моей любимой школе @emi_rostov за отличный старт, шикарную базу и возможность реализовать себя!! Вы лучшие!!! </div>
							</div>
						</div>
					</li>
					<li>
						<div class="review">
							<img src="../i/foto5.jpg" alt="" />
							<div class="inner">
								<a>Читать полностью</a>
								<h3>Ксения Кивенко</h3>
<!--								<div class="work">г. Ростов-на-Дону</div>-->
								<div class="text">Ну вот и все! <br />Экзамены сданы, дипломы получены. <br />Квалифицированный мастер маникюра и педикюра E.Mi school</div>
							</div>
						</div>
						<div class="review">
							<img src="../i/foto6.jpg" alt="" />
							<div class="inner">
								<a>Читать полностью</a>
								<h3>Васильева Екатерина</h3>
<!--								<div class="work">г. Новосибирск</div>-->
								<div class="text">Ну вот и сбылась мечта и...икс)))) +1 #emimaster </div>
							</div>
						</div>
					</li>
					<li>
						<div class="review" style="min-height:400px;">
							<img src="../i/foto7.jpg" alt="" />
							<div class="inner big">
								<a>Читать полностью</a>
								<h3>Мила Зырянова</h3>
<!--								<div class="work">г. Новосибирск</div>-->
								<div class="text">Получила свой диплом!!! А вместе с ним прекрасную базу в сфере маникюрных услуг, огромнейшую (☝) уверенность в своих дальнейших успехах, в своих работах, в обеспечении их качества и полной гигиенической безопасности своим клиентам! Спасибо за это вам@emischool_novosibirsk и моему инструктору Анне. Думаю, скоро я вернусь за новыми знаниями <br />Ну а в ближайшее время я в Кемерово</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</section>	
	
<!-- Блок  -->	
	<div id="order"></div>
	<section id="s10">
		<div class="c">
			<div class="holder">
				<form id="ordercourse" action="../send.php" method="POST">
					<div class="small">онлайн-запись</div>
					<h2>Записаться на курсы</h2>
					<div class="text">Оставьте заявку, чтобы записаться в группу <br />или задать вопрос администратору!</div>
					<input type="hidden" name="city" value="<?=$onlyCityId?>" />
					<select name="course" required >
						<option value="" disabled>Выберите курс</option>
						<?php 
							foreach($courses as $course){
								if($course == "Мастер маникюра"){
									$selected == ' selected';
								}else{
									$selected == '';
								}
								echo '<option value="'.$course.'"'.$selected.'>'.$course.'</option>';
							}
						?>
					</select>
					<input type="text" name="name" placeholder="Ваше имя" required />
					<input type="text" id="phone2" name="phone" placeholder="Телефон" required />
					<input type="text" name="email" placeholder="Email" />
					<input type="hidden" name="date" value="" />
					<input type="hidden" name="time" value="" />
					<button class="btn" id="sendorder2">Записаться на курс</button>
				</form>
			</div>
		</div>
	</section>	

	
<!-- footer -->

	<footer>
		<div class="c">
			<img src="../i/footer_logo.png" alt="" class="footer_logo" />
			<div class="left">© 2016 E.Mi<br />Школа ногтевого дизайна <br />Екатерины Мирошниченко</div>
			<div class="center">
				<a href="#top">
					<img src="../i/top.png" />
					Наверх
				</a>
			</div>
		</div>
	</footer>
	
	<div class="overlay"></div>
	
	<div class="popup callback">
		<a class="close"></a>
		<h3>Закажите обратный звонок</h3>
		<form id="ordercall" action="send.php" method="POST">
			<input type="text" name="phone" placeholder="Ваш Телефон" required />
			<select name="city" style="display:none;">
				<?php 
					foreach($cities as $key => $value){
						echo '<option value="'.$key.'">'.$value.'</option>';
					}
				?>
			</select>
			<button class="btn" id="callback">Заказать звонок</button>
		</form>
	</div>
	
	<div class="popup thanks">
		<a class="close"></a>
		<h3>Спасибо за Вашу заявку!</h3>
		Наши менеджеры свяжутся с вами в ближайшее время!<br />
        Больше информации о курсах на основном сайте <a href="https://emi.ua" target="_blank">emi.ua</a>
	</div>
	
	<div class="popup cities">
		<a class="close"></a>
		<h3>Выберите город</h3>
		<div class="inner">
			<?php 
				foreach($cities as $key => $value){
					echo '<a class="js-popup-city" data-city="'.$key.'">'.$value.'</a>';
				}
			?>
		</div>
	</div>
	

<script src="../js/bxslider.js"></script>
<script src="../js/script.js"></script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter<?=$yaIdent?> = new Ya.Metrika({
                    id:<?=$yaIdent?>,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");

    getSchedule(15,<?=$onlyCityId;?>); //id
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/<?=$yaIdent?>" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


</body>
</html>