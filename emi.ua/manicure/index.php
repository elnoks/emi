<!DOCTYPE html>
<html lang="ru">

<head>

    <meta charset="UTF-8">

    <title>Курсы маникюра</title>

    <link href="./map/css/jqvmap.css" rel="stylesheet">
    <link href="./css/s.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Marmelad&amp;subset=cyrillic" rel="stylesheet">

    <script src="https://yastatic.net/jquery/2.1.4/jquery.min.js"></script>
    <script src="./map/js/jqvmap.js"></script>
    <script src="./map/js/ukraine.ru.js"></script>

    <script>
        $(document).ready(function () {

            $('#ua').vectorMap(
                {
                    map: 'ukraine_ru',
                    backgroundColor: 'white',
                    borderColor: '#f54e6c',
                    borderOpacity: 0.60,
                    borderWidth: 2,
                    color: '#ff8585',
                    hoverColor: '#FFCAC8',
                    selectedColor: '#FFCAC8',
                    selectedRegions: null,
                    showTooltip: true,
                    onRegionClick: function(element, code, region)
                    {
                        $('#link').hide();
                        if (code==3){
                            $('#link').prop("href", "https://emi.ua/manicure/krivoj-rog/");
                            $('#link').text("Курсы маникюра в Кривом Роге");
                        }
                        else if (code==9){
                            $('#link').prop("href", "https://emi.ua/manicure/kiev/");
                            $('#link').text("Курсы маникюра в Киеве");
                        }
                        else if (code==12){
                            $('#link').prop("href", "https://emi.ua/manicure/lviv/");
                            $('#link').text("Курсы маникюра в Львове");
                        }
                        else if (code==14){
                            $('#link').prop("href", "https://emi.ua/manicure/odessa/");
                            $('#link').text("Курсы маникюра в Одессе");
                        }
                        else if (code==15){
                            $('#link').prop("href", "https://emi.ua/manicure/poltava/");
                            $('#link').text("Курсы маникюра в Полтаве");
                        }
                        else if (code==16 || code==2){
                            $('#link').prop("href", "https://emi.ua/manicure/rovno/");
                            $('#link').text("Курсы маникюра в Ровно");
                        }
                        else if (code==19){
                            $('#link').prop("href", "https://emi.ua/manicure/harkov/");
                            $('#link').text("Курсы маникюра в Харькове");
                        }
                        else if (code==24){
                            $('#link').prop("href", "https://emi.ua/manicure/chernovcy/");
                            $('#link').text("Курсы маникюра в Черновцах");
                        }
                        else
                        {
                            $('#link').prop("href", "#");
                            $('#link').text("В данной области курсы не проводятся");
                        }
                        $('#link').fadeIn(400);
                        var message = 'You clicked "'
                            + region
                            + '" which has the code: '
                            + code.toUpperCase();

                        console.log(message);

                    }
                }
            );
            $('#ua').vectorMap('set', 'colors', {
                3: '#ffb5b3',
                9: '#ffb5b3',
                12: '#ffb5b3',
                14: '#ffb5b3',
                15: '#ffb5b3',
                16: '#ffb5b3',
                2: '#ffb5b3',
                19: '#ffb5b3',
                24: '#ffb5b3'

            });


            console.log($('#ua'));
        });
    </script>


</head>

<body>

<div id="ua"></div>
<div style="text-align: center; font-size: 28px"><a id="link" href="#" target="_blank"></a></div>

</body>

</html>