// Scroll.js

$(document).ready(function(){
    $('a[href^="#"]').on('click',function (e) {
        e.preventDefault();

        var target = this.hash,
            $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 1000, 'swing', function () {
            window.location.hash = target;
        });
    });
});

// Определим город клиента


var user_city_in_list = true;



// ajax запрос к расписанию

function getSchedule(course,city){
    $.ajax({
        url: "../schedule_ua.php",
        method: "POST",
        data: { course: course, city: city }
    }).done(function(data) {
        var json = JSON.parse(data);
        var cities = courses = '';
        if(json.dates != undefined){
            var choosen_city = json.dates[0].city;
            var choosen_course = json.dates[0].course;
        }else{
            var choosen_city = city;
            var choosen_course = course;
        }
        for(var i=0; i<json.cities.length; i++){
            cities += '<a data-city="'+json.cities[i].id+'">'+json.cities[i].name+'</a>';
            if(json.cities[i].id == choosen_city){
                var choosen_city_name = json.cities[i].name;
            }
        }
        for(var i=0; i<json.courses.length; i++){
            courses += '<a data-course="'+json.courses[i].id+'">'+json.courses[i].name+'</a>';
            if(json.courses[i].id == choosen_course){
                var choosen_course_name = json.courses[i].name;

            }
        }
        $(".js-cities").html(cities);
        $(".js-courses").html(courses);
        $(".js-choosen-city").html(choosen_city_name).attr("data-city",choosen_city);
        $(".js-choosen-course").html(choosen_course_name).attr("data-course",choosen_course);

        if(json.dates != undefined){
            $(".course_info").css("display","block");
            $(".course_no_info").css("display","none");

            var date_number = json.dates[0].date_number;
            var date_string = json.dates[0].date_string;
            $(".course_info .date span").html(date_number);
            $(".course_info .date div").html(date_string);
            $("input[name=date]").val(date_string);

            var time = json.dates[0].time.split("");
            if (time[1]==""||time[1]==":"){
                time[1] = time[0];
                time[0] = 0;
            }

            $(".course_info .digit0").html(time[0]);
            $(".course_info .digit1").html(time[1]);
            $(".course_info .digit3").html(time[3]);
            $(".course_info .digit4").html(time[4]);
            $("input[name=time]").val(json.dates[0].time);

            var days_select = '<option selected disabled>Виберiть дату</option>';
            for(var i=0; i<json.dates.length; i++){
                days_select += '<option data-string="'+json.dates[i].date_string+'" data-number="'+json.dates[i].date_number+'" data-time="'+json.dates[i].time+'">'+json.dates[i].date_string+' - '+json.dates[i].time+'</option>';
            }
            $("#all_dates").html(days_select);
        }else{
            $(".course_info").css("display","none");
            $(".course_no_info").css("display","block");
            $("input[name=date]").val("");
            $("input[name=time]").val("");
        }
    });
}

$(document).on('change', "#all_dates", function(){
    var option = $(this).find("option:checked");
    var date_number = option.data("number");
    var date_string = option.data("string");
    var time = option.data("time").split("");
    $(".course_info .date span").html(date_number);
    $(".course_info .date div").html(date_string);
    $(".course_info .digit0").html(time[0]);
    $(".course_info .digit1").html(time[1]);
    $(".course_info .digit3").html(time[3]);
    $(".course_info .digit4").html(time[4]);
    $("input[name=date]").val(date_string);
    $("input[name=time]").val(option.data("time"));
});

$(document).on('click', ".js-cities a", function(){
    var course = $(".js-choosen-course").attr("data-course");
    var city = $(this).attr("data-city");
    getSchedule(course,city);
    $(".opener").removeClass("opened");
    $(".opening_window").animate({height:0},300);
});

$(document).on('click', ".js-courses a", function(){
    var course = $(this).attr("data-course");
    var city = $(".js-choosen-city").attr("data-city");
    var course_string = $(this).text();
    getSchedule(course,city);
    $(".opener").removeClass("opened");
    $(".opening_window").animate({height:0},300);
    $("input[name=course]").val(course_string);
    $("select[name=course] option").each(function(){
        if($(this).text() == course_string){
            $(this).attr("selected","selected");
        }else{
            $(this).attr("selected",false);
        }
    });
});

$(document).on('click', ".js-popup-city", function(){
    var course = $(".js-choosen-course").attr("data-course");
    var city = $(this).attr("data-city");
    getSchedule(course,city);
    var city_string = $(this).text();
    $("nav .right a span, footer .right a span").text(city_string);
    $("select[name=city] option").each(function(){
        if($(this).text() == city_string){
            $(this).attr("selected","selected");
        }else{
            $(this).attr("selected",false);
        }
    });
});

// bxSlider initialisation

$("#s6 .slider ul").bxSlider({pagerCustom: '#bx-pager', controls:true,});

// Открываем списки городов и курсов

$(".opener").click(function(){
    if($(this).hasClass("opened")){
        $(this).next().animate({height:0},300);
        $(this).removeClass("opened");
    }else{
        var link_height = $(this).next().find("a").outerHeight(true);
        var links_quantity = $(this).next().find("a").length;
        var height = link_height*links_quantity;
        $(this).next().animate({height:height},300);
        $(this).addClass("opened");
    }
});

// bxSlider initialisation

$("#s9 .slider ul").bxSlider({controls:false,pager:true,});

// popups

$(".close, .overlay, .js-popup-city").click(function(){
    $(".popup").fadeOut(300);
    $(".overlay").fadeOut(300);
});

$(".s8_end .btn").click(function(){
    $(".callback").fadeIn(300);
    $(".overlay").fadeIn(300);
});

$("nav .right a, footer .right a").click(function(){
    $(".popup.cities").fadeIn(300);
    $(".overlay").fadeIn(300);
});


// send form

$yaIdent = "42662474";

$("form#ordermaster").submit(function(event) {
    event.preventDefault();
    $(this).find("button").attr("disabled","disabled").prop("disabled","disabled");

    if (!event.target.checkValidity()) {
        event.preventDefault();
        var not_valid = true;
    }else{
        var not_valid = false;
    }

    if(not_valid===false){
        var $form = $(this),
            term = $form.serialize(),
            url = $form.attr("action");

        var posting = $.post( url, term );

        posting.done(function(data) {
            $(".popup").fadeOut(300);
            $(".overlay").fadeOut(300);
            $(".thanks").fadeIn(300);
            $(".overlay").fadeIn(300);
            $form.find("button").attr("disabled","disabled").prop("disabled","disabled");
        });
        $('input').val('');
        eval("yaCounter"+$yaIdent).reachGoal('ordermaster');
    }else{
        alert("Заполните все поля формы!");
    }
});

$("form#ordercourse").submit(function(event) {
    event.preventDefault();
    $(this).find("button").attr("disabled","disabled").prop("disabled","disabled");

    if (!event.target.checkValidity()) {
        event.preventDefault();
        var not_valid = true;
    }else{
        var not_valid = false;
    }

    if(not_valid===false){
        var $form = $(this),
            term = $form.serialize(),
            url = $form.attr("action");

        var posting = $.post( url, term );

        posting.done(function(data) {
            $(".popup").fadeOut(300);
            $(".overlay").fadeOut(300);
            $(".thanks").fadeIn(300);
            $(".overlay").fadeIn(300);
            $form.find("button").attr("disabled","disabled").prop("disabled","disabled");
        });
        $('input').val('');
        eval("yaCounter"+$yaIdent).reachGoal('ordercourse');
    }else{
        alert("Заполните все поля формы!");
    }
});

$("form#ordercall").submit(function(event) {
    event.preventDefault();
    $(this).find("button").attr("disabled","disabled").prop("disabled","disabled");

    if (!event.target.checkValidity()) {
        event.preventDefault();
        var not_valid = true;
    }else{
        var not_valid = false;
    }

    if(not_valid===false){
        var $form = $(this),
            term = $form.serialize(),
            url = $form.attr("action");

        var posting = $.post( url, term );

        posting.done(function(data) {
            $(".popup").fadeOut(300);
            $(".overlay").fadeOut(300);
            $(".thanks").fadeIn(300);
            $(".overlay").fadeIn(300);
            $form.find("button").attr("disabled","disabled").prop("disabled","disabled");
        });
        $('input').val('');
        eval("yaCounter"+$yaIdent).reachGoal('ordercall');
    }else{
        alert("Заполните все поля формы!");
    }
});

// open menu on mobile

$("nav .menu").click(function(){
    if($("nav .links").hasClass("opened")){
        $("nav .links").removeClass("opened");
    }else{
        $("nav .links").addClass("opened");
    }
});

var bx_advantages_slider;

function advantages_slider(){
    if($(window).width()<1100){
        if(bx_advantages_slider == undefined){
            bx_advantages_slider = $("#s4 .slider ul").bxSlider({controls:false,pager:true,});
        }
    }else{
        if(bx_advantages_slider != undefined){
            bx_advantages_slider.destroySlider();
            bx_advantages_slider = undefined;
        }
    }
}

advantages_slider();

$(window).resize(function(){
    advantages_slider();
});

$(document).ready(function(){
    $("input[name=phone]").mask("+380 99-999-9999");
});
