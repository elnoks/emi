<?php
////////////////////////////////////// Password protect ///////////////////////////////////////////
///
define('ADMIN_USERNAME','admin'); 	// Admin Username
define('ADMIN_PASSWORD','Klsg6gsFhd');  	// Admin Password
if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW']) ||
    $_SERVER['PHP_AUTH_USER'] != ADMIN_USERNAME ||$_SERVER['PHP_AUTH_PW'] != ADMIN_PASSWORD) {
    Header("WWW-Authenticate: Basic realm=\"Memcache Login\"");
    Header("HTTP/1.0 401 Unauthorized");

    echo <<<EOB
				<html><body>
				<h1>Rejected!</h1>
				<big>Wrong Username or Password!</big>
				</body></html>
EOB;
    exit;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
///


	ob_start();
	//ВОТ ЗДЕСЬ ЧИСТО ВСЕ МЕНЯЕМ
	//АЙДИШКИ В БАЗЕ СМОТРЕТЬ (МОЖНО ЧЕРЕЗ PhpMyAdmin, зайти с панели на хостинге)

		$CITY_ID = 1009; //айди города
		$CITY_NAME = "Киев"; //Имя города

	require_once("../../db_connect.php");
?>

<!DOCTYPE html>
<html lang="ru" xmlns:og="http://ogp.me/ns#">
<head>
<meta charset="utf-8" >
<meta http-equiv="Content-Language" content="ru">
<title>Панель управления</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="author" content="3715d3985edfd481c4a1b039d6e959d7" />
<link href="../../css/s.css" rel="stylesheet" />

<script src="../../js/jquery.min.js"></script>

</head> 
<body class="admin">
<div class="c">

	<h2>Панель управления</h2>
	
	<?php
		// Сохранение изменений
		if(!empty($_POST) && $_POST["action"]=="addCourse"){
			echo '<h2 class="green">Курс успешно добавлен</h2>';
			$sql = 'INSERT INTO `courses`(`id`, `active`, `name`) VALUES ("","1","'.$_POST['name'].'")';
			$update = mysqli_query($connection, $sql) or die(mysqli_error($connection));
		}
		
		if(!empty($_POST) && $_POST["action"]=="addCity"){
			echo '<h2 class="green">Город успешно добавлен</h2>';
			$sql = 'INSERT INTO `cities`(`id`, `name`, `city_email`) VALUES ("","'.$_POST['name'].'","'.$_POST['city_email'].'")';
			$update = mysqli_query($connection, $sql) or die(mysqli_error($connection));
		}
		
		if(!empty($_POST) && $_POST["action"]=="addDate"){
			echo '<h2 class="green">Дата успешно добавлена в расписание</h2>';
			$sql = 'INSERT INTO `dates`(`id`, `date`, `time`, `course`, `city`) VALUES ("","'.$_POST['date'].'","'.$_POST['time'].'","'.$_POST['course'].'","'.$_POST['city'].'")';
			$update = mysqli_query($connection, $sql) or die(mysqli_error($connection));
		}
		
		// Удаление
		if(!empty($_POST)){
			if($_POST["table"] == "course"){
				if($_POST["action"]=="Удалить"){
					$sql = 'DELETE FROM `courses` WHERE id = '.$_POST["id"];
					$delete = mysqli_query($connection, $sql) or die(mysqli_error($connection));
					$sql = 'DELETE FROM `dates` WHERE course = '.$_POST["id"];
					$delete = mysqli_query($connection, $sql) or die(mysqli_error($connection));
				}else{
					$sql = 'UPDATE courses SET name = "'.$_POST["name"].'" WHERE id = '.$_POST["id"];
					$update = mysqli_query($connection, $sql) or die(mysqli_error($connection));
				}
			}
			if($_POST["table"] == "city"){
				if($_POST["action"]=="Удалить"){
					$sql = 'DELETE FROM `cities` WHERE id = '.$_POST["id"];
					$delete = mysqli_query($connection, $sql) or die(mysqli_error($connection));
				}else{
					$sql = 'UPDATE cities SET name = "'.$_POST["name"].'", city_email = "'.$_POST['city_email'].'" WHERE id = '.$_POST["id"];
					$update = mysqli_query($connection, $sql) or die(mysqli_error($connection));
				}
			}
			if($_POST["table"] == "date"){
				if($_POST["action"]=="Удалить"){
					$sql = 'DELETE FROM `dates` WHERE id = '.$_POST["id"];
					$delete = mysqli_query($connection, $sql) or die(mysqli_error($connection));
				}else{
					$sql = 'UPDATE dates 
					SET date = "'.$_POST["date"].'", time = "'.$_POST["time"].'", course = "'.$_POST["course"].'", city = "'.$_POST["city"].'"
					WHERE id = '.$_POST["id"];
					$update = mysqli_query($connection, $sql) or die(mysqli_error($connection));
				}
			}
		}
	?>
		
		<?php 
			if(!empty($_GET) && $_GET['cat'] == "courses"){
		?>
			<h2>Все добавленные курсы</h2>
		<?php
				$sql = 'SELECT * FROM `courses` ORDER BY name ASC';
				$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));
				while ($res = mysqli_fetch_assoc($result)) {
		?>
				<form action="" method="POST" class="admin_list" style="width:100%;margin-bottom:10px;border-bottom:1px solid #ddd;">
					<input type="hidden" name="action" value="delete" />
					<input type="hidden" name="table" value="course" />
					<input type="hidden" name="id" value="<?php echo $res["id"]; ?>" />
					<div style="float:right;">
						<input type="submit" name="action" value="Сохранить" style="background:green;">
						<input type="submit" name="action" value="Удалить" style="background:red;">
					</div>
					<div style="margin-right:300px;">
						<input name="name" type="text" value="<?php echo $res["name"]; ?>" style="color:#000;width:100%;padding:0;text-align:left;" />
					</div>
					<div class="clr"></div>
				</form>
		<?php
				}
			}
		?>
		
		<?php 
			if(!empty($_GET) && $_GET['cat'] == "cities"){
		?>
			<h2>Все добавленные города</h2>
		<?php
				$sql = 'SELECT * FROM `cities` ORDER BY name ASC';
				$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));
				while ($res = mysqli_fetch_assoc($result)) {
		?>
				<form action="" method="POST" class="admin_list" style="width:100%;margin-bottom:10px;border-bottom:1px solid #ddd;">
					<input type="hidden" name="action" value="delete" />
					<input type="hidden" name="table" value="city" />
					<input type="hidden" name="id" value="<?php echo $res["id"]; ?>" />
					<div style="float:right;">
						<input type="submit" name="action" value="Сохранить" style="background:green;">
						<input type="submit" name="action" value="Удалить" style="background:red;">
					</div>
					<div style="float:left;">
						<input name="name" type="text" value="<?php echo $res["name"]; ?>" style="color:#000;width:100%;padding:0;text-align:left;" />
					</div>
					<div style="float:left;margin-left:50px;margin-right:300px;">
						<input name="city_email" type="text" value="<?php echo $res["city_email"]; ?>" style="color:#000;width:100%;padding:0;text-align:left;" placeholder="введите email" />
					</div>
					<div class="clr"></div>
				</form>
		<?php
				}
			}
		?>
		
		<?php 
			if(!empty($_GET) && $_GET['cat'] == "dates"){
		?>
			<h2>Расписание</h2>
		<?php			
				$cities = array();
				$cities[$CITY_ID] = $CITY_NAME;
				
				$courses = array();
				$sql = 'SELECT * FROM `courses` ORDER BY name ASC';
				$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));
				while ($res = mysqli_fetch_assoc($result)) {
					$courses[$res["id"]] = $res["name"];
				}
			
				$sql = 'SELECT * FROM `dates` WHERE(city='.$CITY_ID.') ORDER BY date ASC';
				$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));
				while ($res = mysqli_fetch_assoc($result)) {
		?>
				<form action="" method="POST" class="admin_list" style="width:100%;margin-bottom:10px;">
					<input type="hidden" name="table" value="date" />
					<input type="hidden" name="id" value="<?php echo $res["id"]; ?>" />
					<div style="float:left;width:110px;">
						<input type="date" name="date" value="<?php echo $res["date"]; ?>" style="width:100%;padding:0;color:#000;" />
					</div>
					<div style="float:left;width:100px;">
						<input type="text" name="time" value="<?php echo $res["time"]; ?>"  style="width:100%;padding:0;color:#000;"/>
					</div>
					<div style="float:left;width:310px;">
						<select name="course" style="width:100%;padding:0;color:#000;">
							<?php
								foreach($courses as $key => $value){
									if($key == $res["course"]){
										$selected = " selected";
									}else{
										$selected = "";
									}
									echo '<option value="'.$key.'"'.$selected.'>'.$value.'</option>';
								}
							?>
						</select>
					</div>
					<div style="float:left;width:255px;">
						<select name="city" style="width:100%;padding:0;color:#000;">
							<?php
								foreach($cities as $key => $value){
									if($key == $res["city"]){
										$selected = " selected";
									}else{
										$selected = "";
									}
									echo '<option value="'.$key.'"'.$selected.'>'.$value.'</option>';
								}
							?>
						</select>
					</div>
					<div style="float:left;width:260px;">
						<input type="submit" name="action" value="Сохранить" style="background:green;">
						<input type="submit" name="action" value="Удалить" style="background:red;">
					</div>
					<div class="clr"></div>
				</form>
		<?php
				}
			}
		?>
		
		<form action="" method="POST" class="admin_add">
			<h3>Добавить дату проведения курса</h3>
			<input type="hidden" name="action" value="addDate" />
			<div class="label">Дата (В формате: 2018-12-31):</div>
			<input type="date" name="date" placegolder="В формате: 2018-12-31" required /><br />
			<div class="label">Время:</div>
			<input type="text" name="time" placeholder="Время, например, 11:00" required /><br />
			<div class="label">Курс:</div>
			<select name="course" required>
				<option value="0" selected disabled>Выберите курс...</option>
				<?php
					$sql = 'SELECT id, name FROM `courses` ORDER BY name ASC';
					$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));
					while ($res = mysqli_fetch_assoc($result)) {
						echo '<option value="'.$res['id'].'">'.$res['name'].'</option>';
					}
				?>
			</select><br />
			<div class="label">Город/регион:</div>
			<select name="city" required>
				<option value="<?=$CITY_ID?>" selected><?=$CITY_NAME?></option>
			</select><br />
			<button>Добавить</button>
			<div><a href="../admin/?cat=dates">Список всех дат</a></div>
		</form>

</div>
</body>
</html>