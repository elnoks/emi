<?php
	require_once("../db_connect_ua.php");
	//менять здесь
	$onlyCityId = '1024';
	$onlyCityName = 'Львів';

	$cities = array();
	$sql = 'SELECT * FROM `cities` WHERE (id=' . $onlyCityId . ') ORDER BY name ASC';
	$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));
	while ($res = mysqli_fetch_assoc($result)) {
		$cities[$res["id"]] = $res["name"];
	}
	
	$courses = array();
	$sql = 'SELECT * FROM `courses` ORDER BY name ASC';
	$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));

    while ($res = mysqli_fetch_assoc($result)) {
        $courses[$res["id"]] = $res["name"];
    }


/*
Безкоштовне знайомство з маркою E.Mi
Манікюр + педикюр
Майстер манікюру
Майстер педикюру
Сombi-манікюр
Сучасні методики манікюру
 */
	//для замены номера Яндекс счетчика
	$yaIdent = '42555864';
?>
<!DOCTYPE html>
<html lang="ru" xmlns:og="http://ogp.me/ns#">
<head>
<meta charset="utf-8" >
<meta http-equiv="Content-Language" content="ru">
<title>Курси манікюру</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="3715d3985edfd481c4a1b039d6e959d7" />
<link href="https://fonts.googleapis.com/css?family=Marmelad&amp;subset=cyrillic" rel="stylesheet">
<link href="../css/bxslider.css" rel="stylesheet" />
<link href="../css/s.css" rel="stylesheet" />
<link rel="icon" href="../i/favicon.png" type="image/x-icon">

<script src="../js/jquery.min.js"></script>
<script src="../js/maskedinput.min.js"></script>

<script>
	var cities = [];
	<?php
	echo 'cities["'.$onlyCityName.'"] = '.$onlyCityId.';';
	/*foreach($cities as $key => $value){
		echo 'cities["'.$value.'"] = '.$key.';';
	}*/
	?>
</script>


</head> 
<body>

<!--<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KVKBZVN" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>-->


<!-- navigation -->

	<nav role="navigation">
		<div class="c">
			<a class="menu"><span>МЕНЮ</span></a>
			<img src="../i/logo.png" alt="" class="logo" />
			<div class="links">
				<a href="#about">Про нас</a>
				<div class="separator">•</div>
				<a href="#kyrsi">Курси</a>
				<div class="separator">•</div>
				<a href="#advantages">Переваги</a>
				<div class="separator">•</div>
				<a href="#program">Програма курсу</a>
				<div class="separator hidden">•</div>
				<br />
				<a href="#why">Чому обирають нас</a>
				<div class="separator">•</div>
				<a href="#fast-start">Швидкий старт</a>
				<div class="separator">•</div>
				<a href="#reviews">Відгуки</a>
				<div class="separator">•</div>
				<a href="#order">Записатися на курси</a>
			</div>
		</div>
	</nav>

<!-- header -->
	<div id="top"></div>
	<header>
		<div class="c">
			<img src="../i/mobile_logo.png" alt="" class="mobile_logo" />
			<h1>Стань майстром манікюру <br />лише за 10 днів!</h1>
			<div class="desc">Отримайте професію, яка принесе вам стабільний <br />дохід і постійних клієнтів</div>
			<a href="#order" class="btn">Записатися на курси</a>
		</div>
		
	</header>
	
<!-- Блок  -->	
	<div id="about"></div>
	<section id="s1">
		<div class="c">
            <div style="width: 100%; max-width: 200px; text-align: center; margin: 50px auto 0 auto;">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 108.2 67.8" enable-background="new 0 0 108.2 67.8" xml:space="preserve">
<path fill-rule="evenodd" clip-rule="evenodd" fill="#ff7e96" d="M108.2,7.8c0,1.5-1.2,2.7-2.8,2.7c-1.5,0-2.8-1.2-2.8-2.7
	c0-1.5,1.2-2.7,2.8-2.7C107,5.1,108.2,6.3,108.2,7.8L108.2,7.8z M65.6,36.9v30.8h7.1c0.1,0.1,0.1,0.1,0.2,0.1c0.1,0.1,0.3,0,0.3-0.1
	c0.2-0.3,0.3-0.5,0.5-0.7l0,0c0.2-0.2,0.5-0.4,0.7-0.6v0c0.2-0.3,0.7-0.6,1.3-0.9c0.6-0.3,1.4-0.5,2-0.7l0,0v0l0,0l0,0
	c0.7-0.2,1.3-0.4,1.8-0.7c0.7-0.4,1.4-0.8,1.9-1.4c0.6-0.7,1.1-1.5,1.2-2.4c0.3,0.2,0.6,0.4,1,0.6c0.6,0.3,1.2,0.5,1.9,0.6v0
	c1.9,0.3,4.9-1.7,5.4-2.1c0.3,0,1.5,0.1,3.1-0.3c2.1-0.5,3.5-2.1,4.8-3c0.5-0.3,0.7,0,1.2,0.3l0,0c0.2,0.1,0.5,0.3,0.8,0.4
	c0.2,0.1,0.4,0.2,0.7,0.4c0.2,0.1,0.3,0.2,0.5,0.2c0.2,0.1,0.3,0.2,0.5,0.2l0,0v0c0.1,0,0.1,0.1,0.2,0.1c0.2,0.1,0.5,0.2,0.7,0.2
	l0,0c0,0,0.1,0,0.2,0c0.1,0,0.1-0.1,0.2-0.1c0.1-0.1,0.1-0.1,0.1-0.2l0,0v0c0.1-0.1,0.1-0.3,0-0.4c0-0.1-0.1-0.2-0.2-0.2
	c-0.2-0.1-0.4-0.2-0.7-0.3c-0.1-0.1-0.2-0.1-0.4-0.2c-0.3-0.1-0.6-0.3-0.8-0.4c-0.5-0.2-1-0.5-1.4-0.7l0,0c-0.4-0.3-0.7-0.6-0.9-0.9
	c-0.2-0.3-0.3-0.7-0.3-1.2c-0.1-0.6-0.3-1.4-0.7-2.3c-0.4-0.7-0.9-1.5-1.6-2.3c-1.6-1.8-4.2-2.8-4.2-2.8c-0.1-0.1-0.3,0-0.3,0.1
	c0,0.1,0,0.2,0.1,0.3c0.2,0.1,2.6,1.6,4.3,4.1c1.6,3.3,0.9,5.8-2.6,7.2c-2.5,0.9-5-0.4-7.1-1.8c-2.8-1.9-5-5.5-5.2-5.7
	c-0.1-0.1-0.2-0.2-0.4-0.1c-0.1,0.1-0.2,0.2-0.1,0.3c1.3,3.6,4.2,7.5,7.4,8.6c-0.3,0.3-1.1,0.9-2.5,1.2c-0.7,0.1-1.3,0-1.9-0.2
	c-0.9-0.3-1.5-0.9-1.8-1.2c-0.4-7.5-4.1-12-8.2-17.7c-0.5-0.7-1-1.3-1.4-2v-1.4c0.1,0,0.2,0,0.3,0.1c1.4,0.5,2.2,2.2,3.3,3.1
	c0.3,0.3,0.7,0.7,1.1,0.8c0.1,0,0.2,0,0.3-0.1l0,0c0.5-0.9-0.8-2-1.6-2.8c-0.7-0.6-1.3-1.3-1.9-2c-0.1-0.1-0.3-0.4-0.2-0.4
	c0,0,0.3,0.1,0.5,0.2c0.4,0.2,2.2,0.9,2.6,1.1c0.3,0.1,0.5,0.2,0.8,0.3c1.1,0.5,2.2,1.1,2.7,0.6c0.3-0.3,0-0.7-0.2-0.9
	c-1.4-1.3-3.6-1.7-5.4-2.9c0,0-0.1-0.1-0.1-0.1c0.1-0.2,1-0.1,1.3-0.1c0.5,0,2.2-0.1,9.1-0.4c3.7-0.2,6.1,0.1,7.8,0.6
	c1.7,0.4,2.6,1.1,3.4,1.7c1.1,0.9,1.5,2.9,1.6,3.8c-3-1.5-6.4-2.7-9.8-2.5c-0.1,0-0.3,0.1-0.2,0.3c0,0.1,0.1,0.2,0.2,0.2
	c3.3,0.9,6.9,2,9.8,4.1c2.6,2,2.5,2.9,2.4,3.8c-0.1,0.4-0.1,0.8-0.5,1c-0.3,0.1-0.7,0.3-0.9,1c-0.2,0.8,0.3,1.3,0.8,1.8
	c0.3,0.3,0.6,0.7,0.7,1v0c0,0.1,0,0.2-0.1,0.3c-0.1,0.2-0.2,0.3-0.3,0.4c-0.3,0.3-0.6,0.7-0.5,1.6l0,0.1c0.1,1.4,2.5,2.9,3.2,3.4
	c0.1,0.1,0.2,0.1,0.2,0.1c0.2,0.1,0.3,0.1,0.4,0c0.1-0.1,0.1-0.3-0.1-0.4c-0.1-0.1-0.2-0.2-0.3-0.4c-0.3-0.4-0.7-0.8-1-1.2
	c-0.2-0.2-0.3-0.4-0.5-0.5c-0.6-0.6-1-0.9-0.4-1.9c1.3-1.8,0.6-2.4-0.3-3.9c0.2-0.1,0.3-0.2,0.4-0.4c0.3-0.4,0.6-1.3,0.6-2.3
	c0-0.8-0.2-1.6-0.6-2.3c-0.8-1.3-1.5-1.8-1.8-1.9c0-1.7-0.4-5.5-2.7-6.8c3.2-2.4,7.3-5.8,7.2-10.7c0-1.8-0.8-4.1-2.8-5.8
	c-0.3-0.2-0.8-0.5-0.9-0.9c-0.1-1.9-2.6-0.8-1.3-2.6c0.4-0.5,0.8-1.1-0.4-2.2c-0.4-0.7-0.7-1.3-0.5-1.9c0.2-0.7,0.8-1.7,1.9-3.3
	c0.5-0.7,1-1.5,1.5-2.2c0.1-0.1,0.2-0.3,0.3-0.5c0-0.1,0-0.2,0-0.3c-0.1-0.2-0.2-0.2-0.4-0.2c-0.1,0-0.2,0.1-0.3,0.1
	c-0.1,0.1-0.2,0.2-0.3,0.3c-0.2,0.3-0.4,0.5-0.6,0.7c-0.2,0.2-0.3,0.4-0.4,0.6c-0.7,0.9-1.3,1.6-1.8,2.2c-1.1,1.3-1.7,2-1.8,3.6
	c-0.1,1.3,0.5,1.7,1,1.9l0,0c0.2,0.1,0.4,0.2,0.3,0.6c-0.1,0.5-0.5,0.9-0.7,1.2c-0.4,0.4-0.8,0.8-0.4,1.3c0.3,0.5,0.7,0.6,1.1,0.6
	c1.2,0.2,1.4,0.6,1.2,1.8c-0.2,1-0.6,2.2-1.2,3.6c-1.1,2.4-8,6.4-8,6.4c-0.1,0.1-0.2,0.2-0.1,0.4c0,0.1,0.1,0.1,0.2,0.1v0
	c0,0,0.7,0.1,5.3-2.4c3.8-2.1,4.9-5.8,5.1-7.1c1.7,1.5,2.4,3.2,2.1,5.4c-0.5,3.8-3.9,6.5-6.9,8.3c-4.8-2.1-10.5-0.8-15.5-0.6
	c-0.4,0-0.8,0-1.1,0.1c-1,0-2,0.2-3,0.1c-0.1,0-0.2-0.1-0.1-0.3c0.3-0.5,3.6-1,4.8-1.2c0.4-0.1,2.8-0.2,2.6-0.9
	c-0.3-0.3-1.4-0.1-1.7-0.1c-2,0.3-2.7,0.7-4.7,0.7c-1,0-1.1-0.5,0.6-1.3c1-0.6,2-1.1,2.9-1.7c0.7-0.4,1.3-0.8,2-1.3
	c0.6-0.4,1.3-0.9,1.9-1.4l0,0c4.3-3.6,8-7.7,9.9-13.1c0.4-1.1,0.6-2,0.7-2.8l0,0c0.2-1,0.7-1.2,1.8-1.9c0.4-0.3,0.9-0.7,1.5-1.1
	c1.2-0.8,1.7-1.5,1.9-1.9l0,0c0.1-0.2,0.1-0.3,0-0.4c0-0.2-0.2-0.3-0.3-0.3c-0.2,0-0.4,0.1-0.7,0.2c-0.5,0.4-1.5,1.2-2.5,1.9
	c-0.6,0.4-1.1,0.7-1.6,0.9L95,7.1c-0.8,0.3-1,0.1-1.7-0.6l-0.2-0.2c-0.3-0.3-0.6-0.6-1-0.9c-1.4-1.2-3.6-1-4.2-0.9
	C87.4,3.9,86.4,2,82,2.2c-4.3,0.2-7.1,3.8-7.8,5c-1.4-2.4-3.5-5-6.3-5.9c-3.5-1.1-4.9,0-5.3,0.5c-0.6-0.3-2-1.3-4.5-1.3
	c-2.5-0.1-4.2,1.4-4.4,1.6c-0.1,0-0.4,0-0.9-0.1c-0.8,0-1.3,0.1-2,0.4c-1,0.4-1.5,0.2-2.3-0.3c-0.9-0.5-1.7-1.4-2-1.7
	c-0.1-0.1-0.1-0.1-0.2-0.2C46-0.1,45.1,0,45.4,0.6c0,0.1,0.1,0.2,0.2,0.3c0.4,0.5,1.2,1.8,2,2.4l0,0c0.6,0.4,1.3,0.8,1.9,1h0l0,0
	C50,4.4,50.5,4.4,51,4.2c0.4-0.2,0.7-0.4,1.1-0.4c0.4-0.1,0.9-0.1,1.6,0l0,0c0.7,0.1,1.5,0.6,2.7,1.6c1.8,0.7,12.4,12.8,11,19.7
	c-0.2,0.4-0.6,0.6-1,0.4c-1.1-0.7-1.9-2-2.8-3.1c-1.5-2-3.3-4-5.6-5.2c-0.1-0.1-0.3,0-0.4,0.1c-0.1,0.1,0,0.3,0.1,0.4
	c1.6,1.4,2.9,3.2,4.2,4.8c0.5,0.6,1,1.2,1.4,1.9c0.6,1,1.4,2.4,0.8,3.6c-0.1,0.2-0.3,0.4-0.4,0.5c-1.2,0.7-6.3-1.5-7.9-1.8
	c-0.7-0.2-1.4-0.4-2.1-0.6c-5.3-1.4-8.7-0.5-9.5-0.3c-0.5-0.6-2.5-3.1-2.8-5.6c-0.2-2.4,1.1-4.7,1.7-5.6c0.6,0.9,2.5,3.4,4.5,5.2
	c2.5,2.2,5.1,3.3,5.1,3.3c0.1,0.1,0.3,0,0.3-0.1c0-0.1,0-0.2-0.1-0.3c-0.1-0.1-1.3-1-3.5-2.6c-1.2-0.9-2.7-2.6-3.8-4.4
	c-0.8-1.4-1.2-2.5-1.2-3.9c0.1-2.6,0.8-4,2.4-5.6c1.6-1.1,3.7,0.8,4.9,1.6c2.3,1.7,5.3,6.7,5.3,6.8c0.1,0.1,0.2,0.2,0.4,0.1
	c0.1-0.1,0.2-0.2,0.1-0.3c-1.6-3.5-3.9-6.8-7.1-9c-2.4-1.7-3.2-1.7-3.6-1.7c-0.6-0.7-2.7-3.5-3.7-3.4c-0.1,0-0.2,0.1-0.2,0.2
	c-0.6,0.9,1.6,3.1,2.1,3.8c0.1,0.2,0.3,0.6,0.2,0.7c-2,1.9-2.8,4.9-2.7,7.7c-3.6,4.9-3.4,9.9,0.5,13.6c-1.1,0.3-4,1.4-6.7,4.7
	l-1-2.1h-7.5v38.9h7.6V44.9l7.5,14.8H48l7.4-14.8v22.7h7.6V38c0.1-0.1,0.2-0.3,0.2-0.4h0c0.2-0.4,0.4-0.7,0.6-0.9
	C64.4,36.3,65,36.5,65.6,36.9L65.6,36.9z M73.2,64.3V41.4c0.3,0.3,0.6,0.7,0.9,1c3.7,4,7.6,11.3,6.7,16.6l0,0c0,0.6-0.4,1.5-0.8,1.9
	c-0.5,0.7-1.2,1.2-2,1.4c-0.3,0.1-0.6,0.2-0.9,0.3l0,0C75.8,62.9,74.2,63.4,73.2,64.3L73.2,64.3z M14.8,63c0-0.8,0.1-1.5,0.4-2.2
	l-7.6,0v-9.5h15.4v-6.8H7.6v-9.1h18v-6.8H0v38.9h16.9C15.6,66.5,14.8,64.9,14.8,63L14.8,63z M36.8,31.8c0.3-0.4,0.6-0.7,1-1.1
	c1.7-1.7,3.4-2.7,5.3-3.3c1.9-0.6,4.2-0.8,6.9-0.8c3.1,0,6,1,8.8,2.1h-3.2l-1.7,3.5c-0.1,0-0.3,0.1-0.4,0.1c-5,1.1-8.4,2.6-9.9,3.3
	c-0.3,0.2-0.5,0.3-0.7,0.4c-0.5,0.2-0.6,0.7,0,0.6c0,0,0.1-0.1,0.3-0.1c1.1-0.5,5.6-2.4,10.2-3.2l-7.9,16.5L36.8,31.8L36.8,31.8z
	 M63.1,34.5c0.1,0.3,0.2,0.7,0,1.2V34.5L63.1,34.5z M21.5,67.8c-2.6,0-4.8-2.1-4.8-4.8c0-2.6,2.1-4.8,4.8-4.8c2.6,0,4.8,2.2,4.8,4.7
	C26.3,65.6,24.2,67.8,21.5,67.8L21.5,67.8z M69.4,35.7c-2.6,0-4.8-2.1-4.8-4.8c0-2.6,2.1-4.8,4.8-4.8c2.6,0,4.8,2.2,4.8,4.7
	C74.2,33.6,72.1,35.7,69.4,35.7L69.4,35.7z M74.9,8.4c0,0,0.4-0.9,1.4-1.8c1-1,2.2-2,4-2.5c1.7-0.5,2.4-0.4,3.4-0.2
	c1,0.1,1.9,0.9,2.1,1.1C86,5.1,86,5.1,86,5.1c0,0-2,1.4-3,2.6c-1,1.2-2.4,4-2.7,4.8c-0.3,0.8-0.2,0.8,0,0.9c0.2,0.1,0.3-0.1,0.4-0.2
	c0.1-0.1,0.5-0.9,1.3-2c0.8-1.2,1.8-2.4,2.8-3.4c1-1,1.8-1.4,2.2-1.5c0.4-0.1,1.6-0.6,3-0.1c1.4,0.5,2.3,1.4,2.6,2
	c0.4,0.6,0.8,3.4-1.5,7.3c-2.3,3.9-4,6.3-8,9.5c-3.9,3.2-6.6,4.1-7,4.2c-0.4,0-0.6-0.6,0-1.1c0.6-0.6,2.5-2.2,4.1-3.9
	c1.6-1.7,2-2.2,1.8-2.6c-0.3-0.4-1.1,0.2-3.3,2.6c-2.2,2.4-3.2,3.1-3.7,3.2c-0.5,0.1-0.5-0.4-0.2-1.5c1.3-4.1,2-7,1.4-12.1
	C76,11,74.9,8.4,74.9,8.4L74.9,8.4L74.9,8.4z M73.4,25.2c-0.3,0.3-0.5-0.1-0.6-1.2c-0.1-1.1-0.1-1.8-0.3-3.1
	c-0.2-1.4-0.6-2.7-0.9-3.7c-0.3-0.9-0.4-1.7-0.8-2c-0.4-0.3-0.6,0-0.7,0.5c-0.1,0.9,2.4,8.8,0,9c-0.4,0-0.7-1-1.3-2.3
	c-2.7-6.8-6.5-13.2-11.9-18.3c-0.8-0.7-0.9-0.9-0.9-1.1c0-0.1,0-0.3,0.9-0.8C58.4,1.5,61,2,62.5,3c0.6,0.4,0.8,0.5,1,0.5
	c0.2,0.1,0.3,0,0.8-0.3c3.6-1.8,7.7,2.5,9.1,5.5c0.4,0.9,1.7,3.9,1.9,8.1C75.4,19.6,74.8,23.6,73.4,25.2L73.4,25.2z M103.3,7.8
	c0,1.2,0.9,2.1,2.1,2.1c1.2,0,2-1,2-2.1c0-1.2-0.9-2.2-2.1-2.2C104.2,5.7,103.3,6.6,103.3,7.8L103.3,7.8z M105,9.2V8.1l0.3,0
	c0.3,0,0.5,0.1,0.6,0.4c0.1,0.3,0.1,0.6,0.2,0.7l0.7,0c0-0.1-0.1-0.2-0.2-0.7c-0.1-0.4-0.2-0.6-0.5-0.7l0,0c0.3-0.1,0.6-0.3,0.6-0.6
	c0-0.3-0.1-0.5-0.3-0.6c-0.2-0.1-0.4-0.2-0.9-0.2c-0.4,0-0.8,0-1,0.1v2.7H105L105,9.2z M105,7.7h0.3c0.3,0,0.6-0.1,0.6-0.4
	c0-0.2-0.2-0.4-0.6-0.4c-0.2,0-0.3,0-0.3,0V7.7z"/>
</svg>
            </div>
            <div class="text">Компанія E.Mi лідер світової нейл-індустрії, <span>60 Шкіл</span> E.Mi успішно працюють в <span>23 країнах</span> світу.
<br />За авторськими методиками і навчальними програмами пройшли навчання та працевлаштувались
<span class="big">20 000</span>
<span>майстрів нігтьового сервісу</span><br><br></div>
		</div>
	</section>	

<?/*
<!-- Блок  -->	
<section id="s3" style="margin-top: -90px; padding-bottom: 20px">
	<div class="c" align="center" style="width: 100%">
		<video id="movie" poster="../video/video_preview.png" width="854px" height="540px" style="margin: 0 12%; height: calc((0.5vh + 0.5vw)*2*20); width: calc((0.5vh + 0.5vw)*2*34); max-width: 76%">
			<source src="../video/manicure_courses.mp4"></source>
		</video>
	</div>
    <script>
        var v = document.getElementById("movie");
        var status_play = false;

        v.onclick = function() {
            if (v.paused) {
                v.play();
            } else {
                v.pause();
                status_play = true;
            }
        };
        v.addEventListener('contextmenu', function(ev) {
            ev.preventDefault();
            return false;
        }, false);

        function scrollAppear(elem) {

            var docViewTop = $(window).scrollTop(),
                docViewBottom = docViewTop + $(window).height(),
                elemTop = $(elem).offset().top,
                elemBottom = elemTop + $(elem).height();

            return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
        }
        window.onscroll = function() {
            if (status_play == false){
                if(scrollAppear("#s3")){
                    v.play();
                }
            }
        }
    </script>
</section>
	
<!-- Блок  -->	
*/?>
	<section id="s2">
		<div class="c">
			<form id="ordermaster" action="../send.php" method="POST">
				<div class="small">курс</div>
				<h2>Майстер манікюру</h2>
				<div class="text">Залиште заявку, щоб записатися в групу <br />або задати питання адміністратору!</div>
				<input type="hidden" name="city" value="<?=$onlyCityId?>" />
				<input type="text" name="name" placeholder="Ваше ім'я" required />
				<input type="text" id="phone1" name="phone" placeholder="Телефон" required />
				<input type="text" name="email" placeholder="Email" />
				<input type="hidden" name="course" value="Мастер маникюра" />
				<input type="hidden" name="date" value="" />
				<input type="hidden" name="time" value="" />
				<button class="btn" id="sendorder1">Записатися на курс</button>
			</form>
		</div>
	</section>	
	
<!-- Блок  -->	
	<div id="advantages"></div>
	<section id="s4">
		<div class="c">
			<h2>Переваги курсу</h2>
			<div class="slider">
				<ul class="flex">
					<li class="block">
						<img src="../i/s4_icon1.png" alt="" class="icon" />
						<div class="title">Швидке <br />і гарантоване <br />працевлаштування</div>
						<img src="../i/s4_line.png" alt="" class="line" />
						<div class="text">Наші учні знаходять <br />роботу та перших клієнтів <br />через 10 днів</div>
					</li>
					<li class="block">
						<img src="../i/s4_icon2.png" alt="" class="icon" />
						<div class="title">80%<br />практики</div>
						<img src="../i/s4_line.png" alt="" class="line" />
						<div class="text">Практика на клієнтах <br />на кожному занятті</div>
					</li>
					<li class="block">
						<img src="../i/s4_icon3.png" alt="" class="icon" />
						<div class="title">Навчання <br />на продукції E.Mi</div>
						<img src="../i/s4_line.png" alt="" class="line" />
						<div class="text">За авторським <br />методикам з досвідченими <br />викладачами</div>
					</li>
					<li class="block">
						<img src="../i/s4_icon4.png" alt="" class="icon" />
						<div class="title">Престижний <br />диплом E.Mi</div>
						<img src="../i/s4_line.png" alt="" class="line" />
						<div class="text">Міжнародного зразка зареєстрований <br />в єдиному міжнародному<br /> електронному реєстрі </div>
					</li>
				</ul>
			</div>
		</div>
	</section>	
	
<!-- Блок  -->	

	<section id="s5">
		<div class="c">
				<div class="block">
					<img src="../i/s5_img1.jpg" alt="" />
					<div class="inner">
						<div class="title">Ви зможете робити всі <br />види сучасного манікюру</div>
						<div class="text">Обрізний, комбінований, чоловічий, дитячий</div>
					</div>
					<div class="clr"></div>
				</div>
				<div class="block right">
					<img src="../i/s5_img2.jpg" alt="" />
					<div class="inner">
						<div class="title">Ви зможете ідеально <br />наносити колір </div>
						<div class="text">Робити рівне однотонне покриття <br />під кутикулу, френч, руффіан і омбре</div>
					</div>
					<div class="clr"></div>
				</div>
				<div class="block">
					<img src="../i/s5_img3.jpg" alt="" />
					<div class="inner">
						<div class="title">Ви зможете швидко <br />і ефективно працювати</div>
						<div class="text">Робити комплексний манікюр <br />з покриттям всього за 1 годину</div>
					</div>
					<div class="clr"></div>
				</div>
				<div class="block right">
					<img src="../i/s5_img4.jpg" alt="" />
					<div class="inner">
						<div class="title">Ви зможете <br />добре заробляти</div>
						<div class="text">Випускники користуються попитом в провідних салонах краси і швидко напрацьовують клієнтів</div>
					</div>
					<div class="clr"></div>
				</div>
		</div>
	</section>	
	
<!-- Блок  -->	
	<div id="program"></div>
	<section id="s6">
		<div class="c">
			<h2><div>Програма курсу</div>«Майстер манікюру»</h2>
			<div class="flex menu" id="bx-pager">
				<a data-slide-index="0">Тема 1</a>
				<a data-slide-index="1">Тема 2</a>
				<a data-slide-index="2">Тема 3</a>
				<a data-slide-index="3">Тема 4</a>
				<a data-slide-index="4">Тема 5</a>
				<a data-slide-index="5">Тема 6</a>
			</div>
			<div class="slider">
				<ul>
					<li>
						<div class="inner">
							<h3>Як ефективно організувати свою роботу?</h3>
							<div class="text">
                                Манікюрні послуги від А до Я: історія, сучасні тренди, класифікація салонів краси.<br />
                                Обладнання робочого місця майстра манікюру. Ідеальний набір обладнання, матеріалів та інструментів успішного майстра. Допомога в підборі оптимальних інструментів для вашої роботи.<br /><br />
                                Як розрахувати час на послугу? Собівартість процедур, ціноутворення. Як скласти грамотний прайс?<br /><br />
								Практика<br /><br />
								* Приблизний зміст програми. На кожну тему відведено кілька днів.
							</div>
							<a href="#fast-start" class="btn">Розклад</a>
						</div>
					</li>
					<li>
						<div class="inner">
							<h3>Базові знання анатомії і фізіології для успішної практики</h3>
							<div class="text">
                                Будова верхніх кінцівок. Будова кисті. Будова шкіри. Захворювання шкіри. Будова нігтя. Форми вільного краю. Захворювання нігтів. Дистрофія. Дисхромія.<br /><br />
								Практика<br /><br />
								* Приблизний зміст програми. На кожну тему відведено кілька днів.
							</div>
							<a href="#fast-start" class="btn">Розклад</a>
						</div>
					</li>
					<li>
						<div class="inner">
							<h3>Як пройти СЕС?</h3>
							<div class="text">
                                Основи дезінфекції та санітарії. Порядок дезінфекції та стерилізації в салоні. Вимоги СЕС.<br /><br />
								Практика на моделях<br /><br />
								* Приблизний зміст програми. На кожну тему відведено кілька днів.
                                * СЕС – санітарно-епідеміологічна служба
							</div>
							<a href="#fast-start" class="btn">Розклад</a>
						</div>
					</li>
					<li>
						<div class="inner">
							<h3>Технології манікюрних послуг</h3>
							<div class="text">
                                Основні етапи манікюру. Класичний манікюр. Манікюр на проблемну кутикулу. Європейський манікюр. Апаратний манікюр. Змішаний (комбінований) манікюр. Чоловічий манікюр. Дитячий манікюр.<br /><br />
								Практика на моделях<br /><br />
								* Приблизний зміст програми. На кожну тему відведено кілька днів.
							</div>
							<a href="#fast-start" class="btn">Розклад</a>
						</div>
					</li>
					<li>
						<div class="inner">
							<h3>Ідеальне покриття під кутикулу</h3>
							<div class="text">
                                Лакові покриття. Технологія виконання класичного і французького покриття лаком.<br />
                                Гель-лакові покриття. Лунне, градієнтне і Ruffian покриття.<br /><br />
								Практика на моделях<br /><br />
								* Приблизний зміст програми. На кожну тему відведено кілька днів.
							</div>
							<a href="#fast-start" class="btn">Розклад</a>
						</div>
					</li>
					<li>
						<div class="inner">
							<h3>Спа-манікюр і масаж</h3>
							<div class="text">
                                Спа-манікюр. Парафінотерапія. Ремонт нігтя. Полірування. Технологія виконання масажу.<br /><br />
								Практика на моделях<br /><br />
								* Приблизний зміст програми. На кожну тему відведено кілька днів.
							</div>
							<a href="#fast-start" class="btn">Розклад</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</section>	
	
<!-- Блок  -->	
	<div id="why"></div>
	<section id="s7">
		<div class="c">
			<h2>Чому обирають нас?</h2>
			<div class="flex">
				<div class="block">
					<div class="title">119 320</div>
                        <div class="text">Майстрів манікюру в світі вже<br />пройшли навчання і користуються <br />попитом на ринку праці</div>
				</div>
				<div class="block">
					<div class="title">83</div>
					<div class="text">Учбових центрів в cвiтi працюють <br />за авторськими методиками <br />і єдиними стандартами</div>
				</div>
				<div class="block">
					<div class="title">32 000</div>
					<div class="text">Провідних салонів <br />краси в світі вже співпрацюють <br />з нами</div>
				</div>
			</div>
		</div>
	</section>
	
<!-- Блок  -->	
	<div id="kyrsi"></div>
	<section id="s3">
		<div class="c">
			<h2>Iнші курси</h2>
			<div class="flex">
				<div class="block" style="background-image:url(../i/s3_img3.jpg);">
					<div class="inner">
						<div class="pretitle">курс</div>
						<div class="title"><span>День відкритих дверей</span></div>
						<div class="text">безкоштовно познайомтеся<br /> зі Школою та викладачами!</div>
						<a href="#order" class="btn">Записатися</a>
					</div>
				</div>
				<div class="block" style="background-image:url(../i/s3_img4.jpg);">
					<div class="inner">
						<div class="pretitle">курс</div>
						<div class="title long"><span>Безкоштовне знайомство </span><br /><span>з маркою E.Mi</span></div>
						<div class="text">Тест-драйв матеріалів і технологій.<br />Спробуйте і вирішите!</div>
						<a href="#order" class="btn">Записатися</a>
					</div>
				</div>
				<div class="block" style="background-image:url(../i/s3_img1.jpg);">
					<div class="inner">
						<div class="pretitle">курс</div>
						<div class="title"><span>Манікюр + педикюр</span></div>
						<div class="text">Хочете освоїти професію комплексно <br />і заробляти ще більше?</div>
						<a href="#order" class="btn">Записатися</a>
					</div>
				</div>
				<div class="block" style="background-image:url(../i/s3_img2.jpg);">
					<div class="inner">
						<div class="pretitle">курс</div>
						<div class="title"><span>Майстер педикюру</span></div>
						<div class="text">Хочете, щоб клієнти шикувалися до вас <br />в чергу на апаратний педикюр?</div>
						<a href="#order" class="btn">Записатися</a>
					</div>
				</div>
				<div class="block" style="background-image:url(../i/s3_img3.jpg);">
					<div class="inner">
						<div class="pretitle">курс</div>
						<div class="title long"><span>Вечірній курс манікюру </span></div>
						<div class="text">Хочете освоїти манікюрне мистецтво, але не можете відірватися від роботи чи навчання? <br />Ми створили зручний курс для вас</div>
						<a href="#order" class="btn">Записатися</a>
					</div>
				</div>
				<div class="block" style="background-image:url(../i/s3_img4.jpg);">
					<div class="inner">
						<div class="pretitle">курс</div>
						<div class="title long"><span>Курси </span><br /><span>підвищення кваліфікації</span></div>
						<div class="text">Ви майстер манікюру, але навчалися давно і хочете <br />освіжити свої знання і бути в тренді?</div>
						<a href="#order" class="btn">Записатися</a>
					</div>
				</div>
			</div>
		</div>
	</section>	
	
<!-- Блок  -->	
	<div id="fast-start"></div>
	<section id="s8">
		<div class="c">
			<h2>Швидкий старт групи</h2>
			<div class="inner">
				<div class="opener">
					<span class="js-choosen-course"></span>
					<div class="right"><span>Ще курси</span></div>
				</div>
				<div class="opening_window js-courses"></div>
				
				<div class="opener">
					<span class="js-choosen-city"></span>
					<div class="right"><span>Всi мiста</span></div>
				</div>
				<div class="opening_windoww js-citiess"></div>
				<div class="info">
					<div class="course_info">
						<div class="block block1">
							Дата
							<div class="date">
								<span>9.09</span>
								<div>9 жовтня 2018</div>
							</div>
						</div>
						<div class="block block2">
                            Час проведення курсу
							<div class="time">
								<div class="digit digit0">1</div>
								<div class="digit digit1">3</div>
								<div class="digit dots">:</div>
								<div class="digit digit3">0</div>
								<div class="digit digit4">0</div>
							</div>
							<div class="clr"></div>
						</div>
						<div class="block block3">
                            Вибрати іншу дату
							<div class="days">
								<select name="other_date" id="all_dates">
									<option selected disabled>Виберiть дату</option>
								</select>

							</div>
						</div>
						<div class="clr"></div>
					</div>
					<div class="course_no_info">
                        Вибачте, на найближчий час доступних курсів немає
					</div>
				</div>
				<a href="#order" class="btn">Записатися</a>
			</div>
			
			<div class="s8_end">
				<h3>Не знайшли відповідного варіанту?</h3>
				<div class="text">Замовте дзвінок, щоб підібрати дату старту!</div>
				<a class="btn">Замовити дзвінок</a>
			</div>
		</div>
	</section>	
	
<!-- Блок  -->	
	<div id="reviews"></div>
	<section id="s9">
		<div class="c">
			<h2>Відгуки</h2>
			<div class="slider">
				<ul>
					<li>
						<div class="review">
							<img src="../i/foto1.jpg" alt="" />
							<div class="inner">
								<a>Читати повнiстю</a>
								<h3>Lenok Artlab</h3>
								<div class="work">Київ</div>
								<div class="text">Школа EMI, ось я і отримала свій перший сертифікат, чудова школа, чудовий колектив, величезне спасибі краще викладачеві !! Школа @ emi_moscow я до вас ще повернуся і не раз! КРАЩА ШКОЛА МАНІКЮРУ !!!!</div>						</div>
						</div>
						<div class="review">
							<img src="../i/foto2.jpg" alt="" />
							<div class="inner">
								<a>Читати повнiстю</a>
								<h3>palina_leonidovna </h3>
								<div class="work">Чернівці</div>
								<div class="text">Аааааеееееее !!))) Він мій!))) Дякую вам Велике !!! Ви-чудовий викладач! Все зрозуміло, доступно і з душею-суперрр !!!</div>
							</div>
						</div>
					</li>
					<li>
						<div class="review">
							<img src="../i/foto3.jpg" alt="" />
							<div class="inner">
								<a>Читати повнiстю</a>
								<h3>maria_tolmacheva55</h3>
								<div class="work">Харків</div>
								<div class="text">Курс " Салонный маникюр " пройден!!! Наконечная Татьяна спасибо большое!!! И всему коллективу @emischool_omsk , Вы профессионалы своего дела!</div>
							</div>
						</div>
						<div class="review">
							<img src="../i/foto4.jpg" alt="" />
							<div class="inner">
								<a>Читати повнiстю</a>
								<h3>Натусик Иртуганова</h3>
								<div class="work">Київ</div>
								<div class="text">Забрала наконец свой диплом по педикюру, теперь я мастер-универсал, спасибо огромное моей любимой школе @emi_rostov за отличный старт, шикарную базу и возможность реализовать себя!! Вы лучшие!!! </div>
							</div>
						</div>
					</li>
					<li>
						<div class="review">
							<img src="../i/foto5.jpg" alt="" />
							<div class="inner">
								<a>Читати повнiстю</a>
								<h3>Ксения Кивенко</h3>
								<div class="work">Харків</div>
								<div class="text">Ну вот и все! <br />Экзамены сданы, дипломы получены. <br />Квалифицированный мастер маникюра и педикюра E.Mi school</div>
							</div>
						</div>
						<div class="review">
							<img src="../i/foto6.jpg" alt="" />
							<div class="inner">
								<a>Читати повнiстю</a>
								<h3>Васильева Екатерина</h3>
								<div class="work">Одеса</div>
								<div class="text">Ну вот и сбылась мечта и...икс)))) +1 #emimaster</div>
							</div>
						</div>
					</li>
					<li>
						<div class="review" style="min-height:400px;">
							<img src="../i/foto7.jpg" alt="" />
							<div class="inner big">
								<a>Читати повнiстю</a>
								<h3>Мила Зырянова</h3>
								<div class="work">Київ</div>
								<div class="text">Получила свой диплом!!! А вместе с ним прекрасную базу в сфере маникюрных услуг, огромнейшую (☝) уверенность в своих дальнейших успехах, в своих работах, в обеспечении их качества и полной гигиенической безопасности своим клиентам! Спасибо за это вам@emischool_novosibirsk и моему инструктору Анне. Думаю, скоро я вернусь за новыми знаниями</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</section>	
	
<!-- Блок  -->	
	<div id="order"></div>
	<section id="s10">
		<div class="c">
			<div class="holder">
				<form id="ordercourse" action="../send.php" method="POST">
					<div class="small">онлайн-запис</div>
					<h2>Записатися на курси</h2>
					<div class="text">Залиште заявку, щоб записатися в групу <br />або задати питання адміністратору!</div>
					<input type="hidden" name="city" value="<?=$onlyCityId?>" />
					<select name="course" required >
						<option value="" disabled>Выберите курс</option>
						<?php 
							foreach($courses as $course){
								if($course == "Майстер манікюру"){
									$selected == ' selected';
								}else{
									$selected == '';
								}
								echo '<option value="'.$course.'"'.$selected.'>'.$course.'</option>';
							}
						?>
					</select>
					<input type="text" name="name" placeholder="Ваше ім'я" required />
					<input type="text" id="phone2" name="phone" placeholder="Телефон" required />
					<input type="text" name="email" placeholder="Email" />
					<input type="hidden" name="date" value="" />
					<input type="hidden" name="time" value="" />
					<button class="btn" id="sendorder2">Записатися на курс</button>
				</form>
			</div>
		</div>
	</section>	

	
<!-- footer -->

	<footer style="background: #ff506f">
		<div class="c">
			<img src="../i/footer_logo_ua.png" alt="" class="footer_logo" />
			<div class="left">© <?=date("Y");?> E.Mi<br />Школа нігтьового дизайну <br />Катерини Мірошниченко</div>
			<div class="center">
				<a href="#top">
					<img src="../i/top.png" />
                    Вгору
				</a>
			</div>
		</div>
	</footer>
	
	<div class="overlay"></div>
	
	<div class="popup callback">
		<a class="close"></a>
		<h3>Залиште свій номер і ми вам передзвонимо</h3>
		<form id="ordercall" action="../send.php" method="POST">
			<input type="text" name="phone" placeholder="Ваш телефон" required />
			<select name="city" style="display:none;">
				<?php 
					foreach($cities as $key => $value){
						echo '<option value="'.$key.'">'.$value.'</option>';
					}
				?>
			</select>
			<button class="btn" id="callback">Замовити дзвінок</button>
		</form>

	</div>
	
	<div class="popup thanks">
		<a class="close"></a>
		<h3>Дякуємо за Вашу заявку!</h3>
        Наш адміністратор зв'яжеться з вами найближчим часом!<br />
        Більше інформації про курси на основному сайті <a href="https://emi.ua" target="_blank">emi.ua</a>
	</div>
	
	<div class="popup cities">
		<a class="close"></a>
		<h3>Выберите город</h3>
		<div class="inner">
			<?php 
				foreach($cities as $key => $value){
					echo '<a class="js-popup-city" data-city="'.$key.'">'.$value.'</a>';
				}
			?>
		</div>
	</div>
	

<script src="../js/bxslider.js"></script>
<script src="../js/script_ua.js"></script>
<script>
    getSchedule(15,<?=$onlyCityId;?>); //id
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-128552609-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-128552609-1');
</script>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '362923657786394');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=362923657786394&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->


</body>
</html>