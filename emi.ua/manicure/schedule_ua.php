<?php
	require_once("db_connect_ua.php");

	// set variables
	$today = date("Y-m-d");
	if(!empty($_POST["course"])){
		$choosen_course = $_POST["course"];
	}

	if(!empty($_POST["city"])){
		$choosen_city = $_POST["city"];
	}
	$courses = $cities = $dates = $json_array = array();

	$mounthes_str = array(
		"01" => "січня",
		"02" => "лютого",
		"03" => "березня",
		"04" => "квітня",
		"05" => "травня",
		"06" => "червня",
		"07" => "липня",
		"08" => "серпня",
		"09" => "вересня",
		"10" => "жовтнтя",
		"11" => "листопада",
		"12" => "грудня"
	);
	$week_days_str = array(
		1 => "ПН",
		2 => "ВТ",
		3 => "СР",
		4 => "ЧТ",
		5 => "ПТ",
		6 => "СБ",
		7 => "НД"
	);

	// заполним массивы с городами и курсами (всеми)

	$sql = 'SELECT * FROM `cities` ORDER BY name ASC';
	$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));
	while ($res = mysqli_fetch_assoc($result)) {
		$cities[] = array($res["id"], $res["name"]);
	}

	$sql = 'SELECT id, name FROM `courses` WHERE active = 1 ORDER BY name ASC';
	$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));
	while ($res = mysqli_fetch_assoc($result)) {
		$courses[] = array($res["id"], $res["name"]);
	}

	// Получим даты для выбранной пары курс-город

	if($choosen_city && $choosen_course){
		$sql = 'SELECT date, time FROM `dates` WHERE course = '.$choosen_course.' AND city = '.$choosen_city.' AND date >= "'.$today.'" ORDER BY date ASC';
	}else if($choosen_city && !$choosen_course){
		$sql = 'SELECT date, time, course FROM `dates` WHERE city = '.$choosen_city.' AND date >= "'.$today.'" ORDER BY date ASC';
	}else{
		$sql = 'SELECT date, time, city, course FROM `dates` WHERE date >= "'.$today.'" ORDER BY date ASC LIMIT 0,1';
	}
	$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));
	while ($res = mysqli_fetch_assoc($result)) {
		$dates[] = array($res["date"], $res["time"]);
		if(!$choosen_city){
			$choosen_city = $res["city"];
		}
		if(!$choosen_course){
			$choosen_course = $res["course"];
		}
	}

	// Формируем json

	foreach($cities as $city){
		$json_array["cities"][] = array("id" => $city[0], "name" => $city[1]);
	}
	foreach($courses as $course){
		$json_array["courses"][] = array("id" => $course[0], "name" => $course[1]);
	}
	foreach($dates as $date){
		$this_date = strtotime($date[0]);
		$date_number = date("j.m",$this_date);
		$date_string = date("j",$this_date);
		$date_string .= ' '.$mounthes_str[date("m",$this_date)].' '.date("Y",$this_date);
		$date_week_day = date("N",$this_date);
		$json_array["dates"][] = array(
			"date" => $date[0],
			"time" => $date[1],
			"city" => $choosen_city,
			"course" => $choosen_course,
			"date_number" => $date_number,
			"date_string" => $date_string,
			"date_week_day" => $date_week_day
		);
	}

	echo json_encode($json_array);

?>
