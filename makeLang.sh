#!/usr/bin/env bash
langDir=/var/www/admin/data/www/emischool.gr/
ID=gr

#Получаем список файлов переводов
ls langs/*.php | awk -F. '{print $1}' | sed 's|.*/||' > ohlol.txt

#Начинаем перебирать файлы в цикле
while read LINE
    do
    FILENAME=$LINE'.php'

#
    case $LINE in
        1)
        CURPATH=bitrix/templates/.default/components/ithive/offices.list/.default/lang/
        ;;

        2)
        CURPATH=bitrix/templates/.default/components/bitrix/main.profile/emi_personal/lang/
        ;;

        3)
        CURPATH=bitrix/templates/.default/components/bitrix/subscribe.edit/subscribe/lang/
        ;;

        3_1)
        CURPATH=bitrix/templates/.default/components/bitrix/sale.personal.order.list/.default/lang/
        ;;

        3_2)
        CURPATH=bitrix/templates/.default/components/bitrix/sale.personal.order.list/order_list_courses/lang/
        ;;

        4_1)
        CURPATH=bitrix/templates/.default/components/bitrix/sale.personal.order.detail/.default/lang/
        ;;

        4_2)
        CURPATH=bitrix/templates/.default/components/bitrix/sale.personal.order.detail/order_detail_courses/lang/
        ;;

        5)
        CURPATH=bitrix/templates/.default/components/bitrix/sale.personal.order.cancel/.default/lang/
        ;;

        6_1)
        CURPATH=bitrix/templates/.default/components/bitrix/sale.basket.basket/basket/lang/
        ;;

        6_2)
        CURPATH=bitrix/templates/.default/components/bitrix/sale.basket.basket/basketSchool/lang/
        ;;

        7_1)
        CURPATH=bitrix/templates/.default/components/bitrix/sale.order.ajax/order/lang/
        ;;

        7_2)
        CURPATH=bitrix/templates/.default/components/bitrix/sale.order.ajax/orderSchool/lang/
        ;;

        8_1)
        CURPATH=bitrix/templates/.default/components/bitrix/sale.basket.basket.line/products_basket/lang/
        ;;

        8_2)
        CURPATH=bitrix/templates/.default/components/bitrix/sale.basket.basket.line/courses_basket/lang/
        ;;

        9)
        CURPATH=bitrix/components/emi/news.sections/templates/emi/lang/
        ;;

        10)
        CURPATH=bitrix/components/emi/buttons/templates/.default/lang/
        ;;

        11)
        CURPATH=bitrix/templates/.default/components/bitrix/catalog/.default/bitrix/catalog.section/list/lang/
        ;;

        12_1)
        CURPATH=bitrix/templates/.default/components/bitrix/catalog/.default/bitrix/catalog.element/.default/lang/
        ;;

        12_1)
        CURPATH=local/components/oc/analog.product.slider/templates/.default/lang/
        ;;

        13_1)
        CURPATH=bitrix/templates/.default/components/bitrix/catalog/courses/bitrix/catalog.section/list/lang/
        ;;

        13_2)
        CURPATH=bitrix/components/emi/schedule.calendar/templates/.default/lang/
        ;;

        header)
        ;;

        esac

        NEWLANGDIR=$langDir$CURPATH$ID'/'
        BASELANGDIR=$langDir$CURPATH'en/.'
        FILEEDIT=$langDir$CURPATH$ID'/template.php'
      if [ ! -d $NEWLANGDIR ]; then
             # Создать папку, только если ее не было
         mkdir $NEWLANGDIR
        fi
      yes | cp -i $BASELANGDIR $NEWLANGDIR -r
      echo $?
      echo $FILENAME
      cat langs/$FILENAME > $FILEEDIT
      echo $?

done < ohlol.txt