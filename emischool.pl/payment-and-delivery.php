<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оплата и доставка");
?><div class="bx_page">
	<div class="h1-top">
		<h1>Płatność i dostawa</h1>
	</div>
	<p>
		<br>
	</p>
	<p>
 <b>Opłata</b>
	</p>
	<p>
		 <b>Uwaga! Minimalne zamówienie produktów E.Mi -20 PLN</b>
	</p>
	<p>
        Opłata jest realizowana za pośrednictwem PAYU.
	</p>
	<ul>
		<li>
		<p>
            Visa
		</p>
 </li>
		<li>
		<p>
            MasterCard
		</p>
 </li>
    </ul>
		<p>
            Realizacja zamówień odbywa się od poniedziałku do piątku ,w godz 9:00 - 18:00. Jeśli masz pytania możesz zadzwonić pod nr. + 48 534 689 832 lub napisać email poland-sale@emischool.com
		</p>


		<p>
            Zwróć uwagę na regulamin realizacji zamówień! Klikając «ОК»pod zamówieniem, zgadzasz się z obowiązującymi warunkami płatności i dostawy.
		</p>
	<div>
		<br>
	</div>
	<p>
	</p>
	<p>
	</p>
	<p>
 <b>Dodatkowe informacje</b>
	</p>
	<p>
        Minimalna kwota zamówienia wynosi 20zł. Przy realizacji zamówienia dajemy następujące rabaty:
	</p>
	<p>
        Od kwoty 1000zł. – rabat 5%
    </p>
	<p>
        Od kwoty 1500zł. — rabat 10%
    </p>
	<p>
        Realizacja i wysyłka zamówień jest realizowana w ciągu 3 dni roboczych od momentu zaksięgowania wpłaty na naszym koncie. Jeśli realizacja zamówienia następuje przez region w którym działa przedstawicielstwo «Szkoły stylizacji paznokci Ekateriny Miroshnichenko», przedstawiciel skontaktuje się z Tobą w ciągu 5 dni roboczych.
	</p>
	<p>
 <br>
	</p>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>