<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Юридическая информация");
?>
<div class="bx_page">
<div class="h1-top">
		<h1>Informacje prawne</h1>
    </div>
 <br>
<h2>
    Polityka konfidencjalności
</h2>
<p>
1. Warunki polityki konfidencyjności między Tobą a «E.Mi», związane z poufnością danych osobowych, regulowane są przepisami prawa Polskiego. «O ochronie danych osobowych».
</p>
<p>
2. Polityka konfidencyjności działa w stosunku danych osobowych , które «E.Mi» dostało lub może dostać od Ciebie przy rejestracji lub realizacji zamówienia na stronie i nie zbędne są  do spełnienia zobowiązań po stronie «E.Mi» w stosunku zakupionej przez Cebie produkcji lub Twojego dostępu do konta osobistego na stronie sklepu.
</p>
<p>
3. Tym samym wyrażasz zgodę, na wykorzystanie Twoich danych osobowych na potrzebę strony i zgadzasz się z polityką konfidencyjności  i warunkami przetwarzania danych osobowych. Jeśli nie wyrażasz zgody lub nie zgadzasz się z warunkami polityki konfidencyjności musisz opuścić tą stronę.
</p>
<p>
4. Potwierdzasz , że Twoja zgoda na przetwarzanie danych osobowych, tyczy również osoby powiązane z firma «E.Mi».
</p>
<p>
5. Dane osobowe które posiada i przetwarza «E.Mi»:
</p>
<p>
5.1. Obowiązkowe dane osobowe, które świadomie udostępniasz przy rejestracji lub złożeniu zamówienia na stronie, przede wszystkim potrzebne do spełnienia zobowiązań ze strony «E.Mi» w stosunku zakupionego przez Ciebie towaru,takie jak:
    imię i nazwisko użytkownika;
    Telefon kontaktowy użytkownika;
    Adres email użytkownika;
    Adres wysyłki.
</p>
<p>
5.2. Nie obowiązkowe dane osobowe, które świadomie udostępniasz o sobie według swojego uznania, na przykład, wiek,pleć, status społeczny, przy rejestracji na stronie lub dalszym użytkowaniu strony.
</p>
<p>
5.3. Zdepersonalizowane dane, automatycznie otrzymane pod czas Twojego przeglądania strony, za pomocą, należącego do Twojego urządzenia: IP-adres, pliki cookie, informacja o Twojej przeglądarce  (lub aplikacji, za pomocą której korzystasz z dostępu do strony, godzina dostępu, adres pytającej strony.
</p>
<p>
6. «E.Mi» nie sprawdza prawidłowości dostarczanych przez Ciebie danych osobowych i nie kontroluje ich aktualności.  «E.Mi» zakłada, że dostarczasz poprawnych i wystarczających danych pod czas rejestracji, i utrzymuje ta informacje jako aktualną. Całą odpowiedzialność za nieprawidłowe lub nie kompletne dane osobowe ponosi użytkownik.
</p>
<p>
7. «E.Mi» posiada i przetwarza tylko te dane osobowe, które niezbędne są do korzystania przez użytkownika z serwisów strony lub nabycia produktów z katalogu «E.Mi» na stronie, a mianowicie:
</p>
<p>
7.1. otrzymania, przygotowania i dostarczenia Twojego zamówienia;
</p>
<p>
7.2. otrzymania Twojej płatności ;
</p>
<p>
7.3. informowania Ciebie o statusie Twojego zamówienia za pośrednictwem poczty elektronicznej lub wiadomości sms;
</p>
<p>
7.4. ulepszania pracy strony i serwisu towarzyszącego;
</p>
<p>
7.5. efektywne wsparcie klienta;
</p>
<p>
7.6. umożliwienie korzystania ze swojego konta na stronie;
</p>
<p>
7.7. informowania związanego z działaniami na Twoim koncie;
</p>
<p>
7.8. polepszenia pracy konta, wygody w korzystaniu z konta, a tak że wprowadzenia korzystnych zmian w oparciu о Twoje opinie korzystania ze strony;
</p>
<p>
7.9. informowanie Ciebie o promocjach i wydarzeniach «E.Mi»;
</p>
<p>
7.10. prowadzenie statystyk i badań rynku na podstawie zdepersonalizowanych danych.
</p>
<p>
8. W stosunku do Twoich danych osobowych zachowana jest ich poufność, chyba ze świadomie udostępniłeś informacje o sobie do ogólnego dostępu.
</p>
<p>
9. «E.Mi» chroni Twoje dane osobowe zgodnie z wymaganiami, stosowanymi w tego typu informacjach i niesie odpowiedzialność za wykorzystanie bezpiecznych metod ochrony takiej informacji.
</p>
<p>
10. Do ochrony Twoich danych osobowych, zapewniając ich właściwe stosowanie i zapobiegając nieuprawnionemu i / lub nieumyślnemu dostępowi do nich, E.Mi stosuje niezbędne i wystarczające środki techniczne i administracyjne. Podane dane osobowe są przechowywane na serwerach z ograniczonym dostępem zlokalizowanych w chronionych pomieszczeniach.
</p>
<p>
11. «E.Mi» ma prawo udostępnić Twoje dane osobowe osobą trzecim w przypadku:
</p>
<p>
11.1. Wyraziłeś swoją zgodę na takie czyny;
</p>
<p>
11.2. przekazanie niezbędne jest do Twojego korzystania z usług strony сервиса lub w kwestii umowy, na którym jesteś beneficjentem. Jednocześnie E.Mi zapewnia poufność Twoich danych osobowych, a Ty otrzymasz powiadomienie o takim przeniesieniu;
</p>
<p>
11.3. Przekazanie przewiduje prawo Polskie zgodnie z procedurą ustanowioną przez ustawę.
</p>
<p>
12.  Masz prawo w każdej chwili (zmienić , uzupełnić) udostępnione przez Ciebie dane osobowe, korzystając z opcji redagowania swoich danych osobowych.
</p>
<p>
13. W każdej chwili możesz zażądać usunięcia udostępnionych przez Ciebie danych osobowych, zwracając się do «E.Mi».
</p>
<p>
14. Wyrażasz swoją zgodę na согласие przetwarzanie swoich danych osobowych klikając «Z warunkami polityki konfidencyjności zgadzam się» przy składaniu zamówienia na stronie.
</p>
<p>
15. Twoja zgoda na przetwarzanie danych osobowych jest aktywna przy korzystaniu z usług «E.Mi».
</p>
<p>
16. Wszystkie pytania tyczące przetwarzania danych osobowych możesz kierować do «E.Mi».
</p>
<h2>
    Informacje prawne
</h2>
<h3>
    Prawa autorskie
</h3>
<p>
    Znaki towarowe, znaki towarowe, logotypy oraz inne sposoby indywidualizacji umieszczone na tej stronie są własnością E.Mi (Ekaterina Miroshnichenko) i stron trzecich. Informacje publikowane na stronie nie przyznają żadnych praw licencyjnych do używania znaków towarowych bez uzyskania uprzedniej pisemnej zgody właściciela.
</p>
<h3>
    Znaki towarowe
</h3>
<p>
    Znaki towarowe, znaki towarowe, logotypy oraz inne sposoby indywidualizacji umieszczone na tej stronie są własnością E.Mi (Ekaterina Miroshnichenko) i stron trzecich. Informacje publikowane na stronie nie przyznają żadnych praw licencyjnych do używania znaków towarowych bez uzyskania uprzedniej pisemnej zgody właściciela.
</p>
<h3>
    Informacje o firmie
</h3>
<p>
<b>Pełna nazwa przedsiębiorstwa:</b> Spółka z ograniczoną odpowiedzialnością  «Е.Мi»<br>
<b>Adres siedziby firmy:</b> 344010, Rostów nad Donem, ul.Halturinskiy 160/102<br>
<b>NIP (ОГРН):</b> 1146195002700<br>
<b>ИНН:</b> 6163134280
</p>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>