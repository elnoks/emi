<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("How to open the E.Mi school representative office");
?><div class="bx_page">
	<p style="text-align: center;">
 <img width="800" alt="765.jpg" src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" height="105" title="765.jpg" border="0"><b> <br>
 </b>
	</p>
	<p>
 <b> <br>
 </b>
	</p>
	<p>
 <b>Współpraca ze Szkolą Stylizacji Paznokci Ekateriny Miroshnichenko–to możliwość otrzymać gotowy, działający i przynoszący stabilny dochód biznes.Współpracując z nami, otrzymujesz cały kompleks technologii:od stylizacji paznokci po wsparcie w prowadzeniu własnego biznesu. Stylizacje paznokci od autorskiej szkoły Ekateriny Miroshnichenko – zawsze aktualne, pożądane przez klientki, stylowe i rozpoznawalne stylizacje!</b>
	</p>
	<p>
 <b>Zostań naszym regionalnym przedstawicielem, a my nauczymy Cię jak osiągnąć sukces!</b>Na dzień dzisiejszy szkoła stylizacji paznokci Ekateriny Miroshnichenko znana jest nie tylko w Rosji, a tak ze na całym Świecie: prestiżowe nagrody w międzynarodowych konkursach , wykłady w Niemczech i Włoszech,otwarcie międzynarodowej siedziby w Pradze.
        Możesz otrzymać wyłączność na reprezentowanie marki w swoim regionie oraz wysokodochodowy, ciekawy biznes, prowadzenie szkoleń dla stylistek paznokci pod marką «Szkoła stylizacji paznokci Ekateriny Miroshnichenko».
    </p>
	<p>
 <b>Dołącz do tych, którzy zawsze trendy! </b> Szkoła stylizacji paznokci Ekateriny Miroshnichenko stworzyła nie tylko rozpoznawalne ,efektowne stylizacje, no i również wyjątkowe techniki szkoleniowe do ich tworzenia. <br>
 <b>Dajemy możliwość każdemu cieszyć się tworzeniem piękna!</b> <br>
 <b>Przewaga «Szkoły stylizacji paznokci Ekateriny Miroshnichenko»:</b>
	</p>
	<ul>
		<li>Zapotrzebowanie na szkolenia tego typu potwierdza to, że w naszym głównym centrum szkoleniowym (Rostów nad Donem) corocznie kończy naukę ponad 500 stylistów paznokci, korzystających z różnych programów szkoleniowych, stylistek z Rosji i innych krajów.</li>
		<li>Szkolenia prowadzone są  na ekskluzywnej marce produktów do stylizacji «<a href="/catalog/">E.Mi</a>»</li>
		<li>Międzynarodowy standard przedsiębiorstwa. Europejska siedziba (Praga).</li>
		<li>U podstaw programów szkoleniowych szkoły autorskiej— unikatowe techniki, opracowane i opatentowane , Ekateriną Miroshnichenko mistrzynią świata w nominacji OMC w zdobieniu paznokci  (Paryż 2011r.).</li>
	</ul>
	<p>
	</p>
	<p>
 <b>Zapewniamy naszym przedstawicielom wszystkie niezbędne narzędzia do pracy jednostki szkoleniowej «Szkoły stylizacji paznokci Ekateriny Miroshnichenko».
     Otwierając oficjalne przedstawicielstwo szkoły stylizacji paznokci dostajesz:
 </b>
	</p>
	<ol>
		<li><b>Ekskluzywne prawo pracować pod nazwą </b>szkoły stylizacji paznokci Ekateriny Miroshnichenko, świadcząc usługi edukacyjne,omówione w kontrakcie. W jednym regionie RF (rejon, kraj, okręg) może działać tylko jedno Przedstawicielstwo.</li>
		<li><b>Gotowe programy szkoleniowe, </b>opracowane przez Ekaterine Miroshnichenko. Regionalne centrum szkoleniowe  oszczędza czas i środki na opracowywanie patentów szkoleniowych, metod nauczania i literatury edukacyjnej. Ten fakt pozwala skierować wszystkie siły na rekrutacje kursantek, i w krótkim czasie zwrócić zainwestowane środki.</li>
		<li><b>Podłączenie pod jedyny system certyfikowania. </b>Przedstawicielstwo ma prawo wydawać swoim kursantom oryginalne certyfikaty z hologramem «Szkoły stylizacji paznokci Ekateriny  Miroshnichenko».</li>
		<li><b>Prawo ekskluzywnej ceny na <a href="/catalog/">produkty E.Mi </a></b></li>
		<li><b>Wsparcie reklamowo-informacyjne.</b> . Autorska szkoła umieszcza informacje o swoich przedstawicielstwach w reklamach wiodących profesjonalnych czasopism, a tak że w swoim katalogu reklamowym jak i na oficjalnej stronie. Wspiera uczestnictwo w targach i konferencjach, udostępnia gotowe makiety reklamowe do publikowania w regionalnej prasie.</li>
	</ol>
	<p>
	</p>
	<p>
 <b>Zasady łączenia współpracy oficjalnego Przedstawicielstwa szkoły Ekateriny Miroshniczenko i oficjalnego dystrybutora marki E.Mi.</b> <br>
        Oficjalne przedstawicielstwo szkoły stylizacji paznokci Ekateriny Miroshnichenko ma przewagę na prawo podpisania kontraktu dystrybucji marki  E.Mi na terytorium, obejmującym umowne terytorium przedstawicielstwa szkoły. W tym przypadku do wymagań stosunkiem oficjalnego przedstawicielstwa szkoły stylizacji paznokci Ekateriny Miroshniczenko dochodzą warunki współpracy z dystrybutorem , obowiązki oraz określone upusty na produkty E.Mi.
	</p>
	<p>
 <b>Wymagania do otwarcia oficjalnego przedstawicielstwa Szkoły Stylizacji Paznokci E.Miroshnichenko:</b>
	</p>
	<ol>
		<li>Utalentowany instruktor, który potrafi idealnie kopiować autorskie techniki E. Miroshnichenko. Kandydat na instruktora E.Mi najpierw odbywa podstawowe szkolenia i jeśli odpowiada wymaganiom Ekateriny Miroshnichenko odbywa specjalny kurs instruktorski.</li>
		<li>Posiadanie osobnego pomieszczenia na sale szkoleniową nie mniej 16m.kw. Umieszczenie firmowego szyldu zgodnego z stylem firmowym Szkoły stylizacji paznokci E. Miroshnichenko.</li>
		<li>Posiadanie uprawnień do świadczenia usług szkoleniowych.</li>
		<li>Środki finansowe na zakup startowego pakietu produktów i wyposażenie ośrodka szkoleniowego,jak i środki na szkolenia przyszłego instruktora.</li>
		<li>Gotowość przedstawiciela na współpracę przy rekrutacji kursantów w sposób opracowany przez menadżerów  E.Mi.</li>
	</ol>
	<p>
	</p>
	<p>
 <b>Szanowne stylistki! Jeśli postanowiłaś otworzyć przedstawicielstwo szkoły w swoim regionie, na początek potrzebujesz:</b>
	</p>
	<ol>
		<li>Dowiedzieć się czy już nie działa przedstawicielstwo w Twoim regionie. Sprawdzić to możesz w dziale «Kontakty» lub zadzwonić na nr.. +7 906 427-25-86. Jeśli  przedstawicielstwo szkoły już działa w Twoim regionie, niestety musimy odmówić.</li>
		<li>Przysłać zdjęcia tipsów z stylizacjami Ekateriny Miroshnichenko na adres mailowy <a href="mailto:sviridova@emi-school.ru">sviridova@emi-school.ru</a> (na przykład, «Autorski kwiat z obrysowaniem», «Plisa tkaniny», «Truskawka», «Malina», «Wiśnie» i td..) Jest to niezbędne do zaakceptowania Twojej osoby na instruktora marki, ponieważ Twój styl rysunku musi być zgodny z wymaganiami Ekateriny Miroshnichenko. Musisz w zupełności umieć kopiować techniki i stylizację.</li>
		<li><b>Informacje o koszcie szkolenia instruktorskiego dostępne pod numerem telefonu: +7-906-427-25-86.</b> Szkolenie to obejmuje dwa etapy: jako uczeń i jako instruktor. Szkolenia odbywają się tylko w ośrodku szkoleniowym w Rostowie Nad Donem! Koszt jest zależny od ilości szkoleń<b> Dowiedzieć się o ilości zakupów produktów na kwartał możesz pod nr. +7-906-427-25-86. </b> Ilości zakupów są regulowane kontraktem.</li>
		<li>Gotowość do podnoszenia kwalifikacji zawodowych. Kontrakt instruktorski odpisywany jest na okres jednego roku. Co roku instruktor przechodzi osobiście potwierdzenie kwalifikacji w Rostowie nad Donem i otrzymuje umowę na następny rok.</li>
	</ol>
	<p>
	</p>
	<p>
 <b>Dodatkową informacje w sprawie otwarcia przedstawicielstwa Szkoły stylizacji paznokci Ekateriny Miroshnichenko możesz uzyskać w siedzibie główniej:  <br>
     Rostów nad Donem, <a href="mailto:sviridova@emi-school.ru">sviridova@emi-school.ru</a>,&nbsp;.+7-906-427-25-86</b>
	</p>
 <br>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>