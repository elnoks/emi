<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Prague 2017");
?>
    <div class="top-wrapper">
        <div class="bredcrumbs">

        </div>
        <div class="h1-top">
            <h1>Prague 2017</h1>
        </div>
        <br>
    </div>
    <section class="headers text-center">
        <div class="row">
            <h1>WELCOME TO A BRIEF GUIDE TO PRAGUE</h1>
            <h3>ESPECIALLY FOR YOU WE HAVE PREPARED THE NECESSARY INFORMATION AND SELECTED THE INTERESTING PLACES.</h3>
            <p>Here we will tell you about the possible ways to move across the city of Prague. Our interactive map will help your to navigate easily and give you all needful information about the restaurants and sightseeing attractions.</p>
            <a class="button_prague_2017" href="#interact">SKIP TO THE MAP</a>
            <p class="bold">Please read carefully the important information about the tickets and moving across the city:</p>
        </div>
    </section>
    <script>
        $(document).ready(function(){
            $("body").on('click', '[href*="#"]', function(e){
                var fixed_offset = 100;
                $('html,body').stop().animate({ scrollTop: $(this.hash).offset().top - fixed_offset }, 1000);
                e.preventDefault();
            });
        });
    </script>
    <section class="info text-center">
        <h2>How to go?</h2>
        <div class="row text-left">
            <div class="col-md-6">
                <i class="fa fa-bus" aria-hidden="true"></i><h3>PUBLIC TRANSPORT</h3>
                <p class="top">All the tickets in the Czech Republic are valid for a certain period of time: 30 minutes, 90 minutes, 1 day and 3 days.</p>
                <p>They cost 24, 32, 110 and 310 Czech Koruna (Kč) respectively. The tickets are available for sale in the special automatic machines, and in any “Relay” stores, which is almost at every metro/tram station. After buying, you must validate the ticket using a special ticket-punching device: in metro you will find it at the entrance, near the escalators, and in buses and trams – inside. The ticket must be kept during the entire trip.</p>
                <p>You not only have to go into the transport with a valid ticket, but you also have to leave the transport with a valid ticket. In other words, for the 35-minute trip you will need to buy the 90-minute-trip ticket in order to avoid problems and fines.</p>
                <p class="top">Transport is divided for the day and night service.</p>
                <p>It is very easy to find the way around: the numbers for the night buses and trams always start with the digit “9”, moreover the trams have the two-digit numbers (for example, tram “98”), while the buses – three-digit numbers (for example, bus “908”).</p>

            </div>

            <div class="col-md-6">
                <div class="subway">
                    <img src="/bitrix/templates/.default/img/maps-cost-prague-2017.png" alt="Metro map in Prague">
                </div>
                <br><br>
                <div class="subway">
                    <img src="/bitrix/templates/.default/img/bus-stop-praha.jpg" alt="Bus stop in Prague">
                </div>
            </div>

            <div class="col-md-12">
                <p>Each stop- and tram stop is provided with the detailed schedule for each route showing all the stops, the distance between each of them, as well as the timetables for each route. The schedule is divided into 2 parts: the schedule for working days and the schedule for weekends. On weekends transport runs less frequently.</p>
                <p>The name of the stop, whereat you are at the moment, is put in bold type. The stops below yours – those toward which one or another route goes in the direction where you are right now. The stops above – those toward which one or another route goes in the opposite direction from you.</p>
                <p class="top">There will be holidays:</p>
                <ul>
                    <li><strong>July 5</strong> - Saints Cyril and Methodius*</li>
                    <li><strong>July 6</strong> - Jan Hus Day*</li>
                </ul>
                <p class="small-text">* These are the Public holidays, and the public transport will be working according to the Sunday schedule.</p>
            </div>

        </div>


        <div class="row margin-top"></div>

        <div class="row text-left background-row">
            <div class="padding-row">
                <div class="col-md-6">
                    <i class="fa fa-subway" aria-hidden="true"></i><h3>METRO</h3>
                    <p class="top">Moving across the city by the subway is probably the fastest way.</p>
                    <p class="top">Working hours:</p>
                    <p>The subway start running in an early morning, at 4:34. Precisely at this time the first train leaves the terminal station сalled Letňany. The subway stops running at 00:40 (at the same station). Travelling from one station to another takes 2 minutes as the maximum. An approximate time-interval for train movement during the peak hours is somewhere about 2-3 minutes; at another day-time you will have to wait the train you need for 5 to 10 minutes, and in the evening – 10-12 minutes.</p>
                    <p class="top">The stations of intersection of the Prague Metro’s lines:</p>
                    <p>The Prague Metro has three transfer stations, whereat you can change the trains.</p>
                    <ul>
                        <li>Mustek – transfer station between <span class="yellow">yellow</span> and <span class="green">green</span> line</li>
                        <li>Muzeum – transfer station between <span class="red">red</span> and <span class="green">green</span> line</li>
                        <li>Florenc - transfer station between <span class="yellow">yellow</span> and <span class="red">red</span> line</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="subway">
                        <img src="/bitrix/templates/.default/img/maps-subway-prague-2017.jpg" alt="Metro map in Prague">

                        <div class="text-center">
                            <p class="small-text">* Florenc St. is the nearest metro station to the Hotel</p>
                            <a href="/bitrix/templates/.default/img/maps-subway-prague-2017-big.jpg" download="Metro map Prague" class="button_prague_2017_white">DOWNLOAD THE METRO MAP</a>
                        </div>
                    </div>
                    <!--                    <div>-->
                    <!--                        <p class="top">Метро Праги на русском языке:</p>-->
                    <!--                        <p>Ознакомьтесь с основными терминами, которые вам пригодятся при использовании Пражского метро. Их вы услышите и увидите спустившись в подземное царство метрополитена.</p>-->
                    <!--                        <ul>-->
                    <!--                            <li>eskalátor - эскалатор</li>-->
                    <!--                            <li>linka (A, B, C) - линия (A, B, C)</li>-->
                    <!--                            <li>metro - метро</li>-->
                    <!--                            <li>přestup - переход</li>-->
                    <!--                            <li>stanice metra - станция метро</li>-->
                    <!--                            <li>vstup - вход</li>-->
                    <!--                            <li>výstup - выход</li>-->
                    <!--                            <li>pozor — внимание</li>-->
                    <!--                            <li>provoz přerušen — движение приостановлено</li>-->
                    <!--                            <li>Ukončete prosím výstup a nástup, dveře se zavírají - Пожалуйста, окончите высадку и посадку, двери закрываются.</li>-->
                    <!--                            <li>Příští stanice: [название станции] - Следующая станция: [название станции]</li>-->
                    <!--                            <li>Přestup na linku A/B/C - Переход на линию A/B/C</li>-->
                    <!--                            <li>[название станции], přestupní stanice - [название станции], пересадочная станция</li>-->
                    <!---->
                    <!--                        </ul>-->
                    <!--                    </div>-->

                </div>
                <div class="col-md-12">
                    <p class="top">Prague Metro in English:</p>
                    <p>Please read the main expressions which will be of service to you when using the Prague Metro. You will hear and see them on entering the Metro reign.</p>
                </div>
                <div class="col-md-6">
                    <ul>
                        <li>eskalátor - escalator</li>
                        <li>linka (A, B, C) - line (A, B, C)</li>
                        <li>metro - subway</li>
                        <li>přestup - transfer</li>
                        <li>stanice metra - subway station</li>
                        <li>vstup - entrance</li>
                        <li>výstup - exit</li>
                        <li>pozor - attention</li>

                    </ul>
                </div>
                <div class="col-md-6">
                    <ul>
                        <li>provoz přerušen - suspended movement</li>
                        <li>Ukončete prosím výstup a nástup, dveře se zavírají - Please finish exiting and boarding the train, the doors are closing</li>
                        <li>Příští stanice: [Station Name] - Next Station: [Station Name]</li>
                        <li>Přestup na linku A/B/C - Transfer to line A/B/C</li>
                        <li>[Station Name], přestupní stanice - [Station Name], transfer station</li>
                    </ul>
                </div>


            </div>
        </div>

        <div class="row margin-bottom"></div>

        <div class="row text-left">
            <div class="col-md-6">
                <i class="fa fa-taxi" aria-hidden="true"></i>
                <h3>Taxi</h3>
                <p> +420 257 257 257 (City TAXI) ENG<br>
                    (+420) 737 222 333 (Modrý Anděl) ENG
                </p>
                <p>UBER is very popular in Prague. Using this application will exclude the need to talk to a taxi operator.</p>
                <p>The tips amount approximately to 10% from the cost (as in all Europe), but the Czech Republic has its own rule: you do not leave a tip after paying the order before leaving, but call the amount, which immediately includes your reward to the taxi driver or the waiter.</p>
            </div>
            <div class="col-md-6">
                <div class="taxi">
                    <img src="/bitrix/templates/.default/img/taxi-prague-2017.jpg" alt="Taxi in Prague">
                </div>
            </div>


        </div>
    </section>

    <section class="cost text-center">
        <h2>APROXIMATE COST</h2>
        <div class="row text-center">
            <div class="table">
                <div class="table-row">
                    <div class="table-col">LUNCH</div>
                    <div class="table-col">OBĚD</div>
                    <div class="table-col">250-300 Kč (SOUP + MAIN MEAL)</div>
                </div>
                <div class="table-row">
                    <div class="table-col">DINNER</div>
                    <div class="table-col">VEČEŘE</div>
                    <div class="table-col">250-300 Kč (MAIN MEAL + DESSERTS)</div>
                </div>
                <div class="table-row">
                    <div class="table-col">SALADS</div>
                    <div class="table-col">Saláty</div>
                    <div class="table-col">150-180 Kč</div>
                </div>
                <div class="table-row">
                    <div class="table-col">BEER</div>
                    <div class="table-col">Pivo</div>
                    <div class="table-col">40 Kč</div>
                </div>
                <div class="table-row">
                    <div class="table-col">СOFFEE</div>
                    <div class="table-col">KÁVA</div>
                    <div class="table-col">60 Kč</div>
                </div>
                <div class="table-row">
                    <div class="table-col">HOMEMADE LEMONADE</div>
                    <div class="table-col">Domácí limonáda</div>
                    <div class="table-col">60 Kč</div>
                </div>
                <div class="table-row">
                    <div class="table-col">Cocktails</div>
                    <div class="table-col">KOKTEJLY</div>
                    <div class="table-col">150 Kč</div>
                </div>
                <div class="table-row">
                    <div class="table-col">Hookah</div>
                    <div class="table-col">Vodní dýmka</div>
                    <div class="table-col">250-350 Kč</div>
                </div>
            </div>
        </div>

    </section>

    <section class="dictionary text-center">
        <h2>USEFUL DICTIONARY</h2>
        <a target="_blank" href="/upload/dictionary_chesh_en.pdf" class="button_prague_2017">CZECH DICTIONARY (PDF)</a>
    </section>

    <section class="map text-center">
        <h2 id="interact">E-MAP</h2>
        <div class="row text-center">
            <iframe class="maps" src="https://www.google.com/maps/d/embed?mid=182oV_3Hs0bSf3conydrGr5fDVLc" ></iframe>
            <a target="_blank" href="https://www.google.com/maps/d/viewer?mid=182oV_3Hs0bSf3conydrGr5fDVLc&ll=50.08375608262957%2C14.359296849999964&z=12" class="button_prague_2017">SHOW THE FULL MAP</a>
        </div>
    </section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>