<?
require( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php" );
$APPLICATION->SetTitle( "Как заказать" );
?>
    <div class="bx_page">
        <div class="h1-top">
            <h1>Kup produkty E.Mi już teraz - skorzystaj z usługi zamówień online.</h1>
        </div>
    </div>

    <div class="bx_page">
        <br/>
    </div>

    <div class="bx_page">
        <br/>
    </div>

    <div class="bx_page">
        <br/>
    </div>


    <div class="bx_page">
        <p>1. Wypełnij prostą rejestrację na stronie.
            <br/>
        </p>

        <p>2. Przejdź do sekcji "Produkty" i dodaj produkty, które chcesz do koszyka, klikając różowy przycisk "Dodaj do
            koszyka", który znajduje się po prawej stronie zdjęcia produktu. Jeśli produkt został pomyślnie dodany do
            koszyka, przycisk zmienia kolor na zielony, a na przycisku pojawia się tekst "W koszyku". Potem możesz
            kontynuować zakupy.
            <br/>
        </p>

        <p>3. Po zakończeniu procesu zakupu sprawdź stan swojego zamówienia w koszyku. Aby to zrobić, kliknij link "Mój
            koszyk", który znajduje się w prawej górnej części strony witryny. Tutaj możesz dostosować zamówienie,
            zmienić wymaganą liczbę produktów i zobaczyć całkowitą kwotę zakupu. Jeśli zmieniłeś liczbę produktów,
            usunięto lub dodano nowe, kliknij przycisk "Przelicz", aby zobaczyć faktyczną kwotę zakupu.

            <P> <b>UWAGA! Minimalne zamówienia produktów E.Mi — 20 złotych.</b><br>
            Przy jednorazowych zakupach na kwotę:
        </P>
            <ul>
                <li>
        <p>
            zamówienie na kwotę 1000 zł. – rabat 5%,
        </p>
        </li>
        <li>
            <p>
                zamówienie na kwotę 1500zł. — rabat 10%.
            </p>
        </li>
        </ul>

        <p>4. Żeby złożyć zamówienie , kliknij różowy przycisk «Zrealizuj zamówienie», przycisk znajduje się w prawej
            części ekranu.</p>

        <p>5. Wybierz sposób dostawy.
            <br/>
        </p>

        <p>6. Wybierz sposób opłaty.
            <br/>
        </p>
        <p>7. Kliknij "Zrealizuj zamówienie" i przejdź do płacenia.
            <br/>
        </p>

    </div>
<? require( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php" ); ?>