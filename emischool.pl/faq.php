<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Tips & tricks");
?> 
<div class="bx_page"> 
<div class="h1-top">
		<h1>Porady i sztuczki</h1>
    </div>
 <br>
  <p> <b>Jak zostać oficjalnym przedstawicielem marki E.Mi  oraz szkoły stylizacji paznokci Ekateriny Miroshnichenko?</b>
    <br />
      Firma E.Mi oferuje gotowe rozwiązania do otworzenia szybko rozwijającego się i interesującego biznesu. Możesz otworzyć oficjalne przedstawicielstwo Szkoły Stylizacji Paznokci Ekateriny Miroshnichenko w Twoim mieście, zostać oficjalnym dystrybutorem marki E.Mi lub hurtownikiem. Aby uzyskać więcej informacji, zobacz<a href="/social/open_school.php" > "Jak otworzyć przedstawicielstwo szkoły"</a></p>
 
  <p> <b>Skąd dowiedzieć się czy jest przedstawicielstwo szkoły w moim regionie?</b>
    <br />
      Sprawdzić dostępność szkoły w Twoim regionie możesz w dziale <a href="/contacts/" >"Kontakty"</a>. </p>
 
  <p> <b>Gdzie można kupić produkty E.Mi?</b>
    <br />
      Możesz kupić produkty E.Mi na naszej stronie internetowej w sekcji "Produkty" (szczegółowe instrukcje dotyczące składania zamówień, patrz "Jak zamówić") lub od autoryzowanego dystrybutora E.Mi w Twoim regionie. Możesz znaleźć adres i numery kontaktowe w dziale <a href="/contacts/" >“Kontakty”</a>. </p>
 
  <p> <b>Czy jest możliwość nauczyć się podstaw  (manicure, pedicure, modelowanie i przedłużanie paznokci) w szkole stylizacji paznokci Ekateriny Miroshnichenko?</b>
    <br />
      W Szkole Stylizacji Paznokci Ekateriny Miroshnichenko można ukończyć szkolenie w zakresie "Modelowanie paznokci" i "Manicure / Pedicure". Szczegółowe informacje i harmonogram znajdziesz w dziale <a href="/courses/" >"Kursy"</a></p>

  <p> <b>Czy mogę przystąpić do szkolenia w szkole stylizacji paznokci Ekateriny Miroshnichenko, nie mając praktyki a nie wyksztalcenia w zawodzie stylista paznokci?</b>
    <br />
      Kursy stylizacji paznokci są zaawansowanym szkoleniem, dlatego zalecamy, aby najpierw odbyć szkolenie w zakresie podstawowym (manicure, pedicure, stylista przedłużania i modelowania paznokci). Dopiero wtedy łatwiej będzie przejść do stylizacji i dekorowania paznokci.</p>
 
  <p> <b>Od czego lepiej zacząć?</b>
    <br />
      My proponujemy zacząć od podstawowych kursów: «ABC linii Art. Painting» oraz «TOP-10 , Najlepsze techniki E.Mi». Na tych szkoleniach nauczysz się prawidłowo pracować pędzlem i paint gelem.. Po tym znacznie łatwiej będzie Ci uczęszczać na kursy drugiego i trzeciego poziomu złożoności, takie jak "MIX-Design: TEXTONE, Gady, Kamienie", "Nailcrust", "Malowanie  artystyczne" i "Malowanie stylizowane: Kwiaty, Owoce, Garderoba’’,’’ Zhostovo "i" One stroke"</p>
 
  <p> <b>Gdzie mogę znaleźć harmonogram szkoleń w moim regionie?</b>
    <br />
      Szczegółowy rozkład szkoleń znajdziesz w dziale <a href="/courses/#schedule" >“Harmonogram szkoleń”</a>. </p>
 
  <p> <b>W jakim wieku można zacząć szkolić się w szkole stylizacji paznokci Ekateriny Miroshnichenko?</b>
    <br />
      W szkole stylizacji paznokci Ekateriny Miroshnichenko przyjmują kursantki od 15 roku (zależy od regionu)W szkole stylizacji paznokci Ekateriny Miroshnichenko przyjmują kursantki od 15 roku (zależy od regionu)</p>
 
  <p> <b>Czy można łączyć paint żele E.Mi z produktami innych marek?</b>
    <br />
      Paint żele E.Mi można stosować z produktami innych marek , jednak trzeba pamiętać ze większość lakierów hybrydowych usuwa się za pomocą removeru, , a paint żele ściąga się przy pomocy piłowania. Dla tego zalecamy zdobienia nie na całości paznokcia, pomalowanego lakierem hybrydowym. Na przykład stylizacja, «Velvet sand Liquid stone»</p>
 
  <p> <b>Czy można pomalować naturalny paznokieć paint gelem?</b>
    <br />
      Przed nałożeniem paint gelu naturalny paznokieć trzeba pokryć sztucznym tworzywem: żelem, akrylem lub lakierem hybrydowym. Pokrywać naturalny paznokieć bezpośrednio paint żelem nie można.</p>
 
  <p> <b>Jaka różnica pomiędzy paint żelem E.Mi a EMPASTĄ?</b>
    <br />
      Główne różnice to konsystencja i obecność (lub brak) ostatecznej lepkości. EMPASTA E.Mi jest gęstsza, dzięki czemu smuga lub linia trwa dłużej. Ponadto, EMPASTA nie ma ostatecznej lepkości, więc można ją nakładać zarówno na żel ochronny Finish Gel jak i pod niego.  Suszenie EMPASTY E.Mi to tylko 2-5 sekund, co pozwala szybko wykonać stylizacje z dużą ilością elementów. Paint gel E.Mi ma bardziej płynną konsystencję, jest idealny do rysunków.</p>
 
  <p> <b>Czym różni się polimer «Liquid Stone» od żelu «Liquid Stone»?</b>
    <br />
      Żel «Liquid stone» - to dodatkowy produkt w technice «Kamienie w płynie». Za jego pomocą możemy tworzyć imitację kamieni na paznokciach w każdym kształcie i wysokości w jedną warstwę, nie musimy pokrywać go topem, co oszczędza nam czas pracy. Żel «Liquid Stone» jest bezbarwny i nie możemy go mieszać z pigmentami. Polimer «Liquid Stone» daje możliwość tworzyć kolorowe kamienie, można go mieszać z pigmentami ale wymaga pokrycia topem.</p>
 
  <p> <b>Korzystać z empasty Ażurowe srebro czy Empasty do lepienia?</b>
    <br />
      EMPASTA «Ażurowe srebro» zawiera przezroczystą frakcję, więc wyciśnij niewielką ilość pasty do palety i dokładnie wymieszaj, następnie pozostaw ją przez kilka dni, aby uzyskać gęstszą konsystencję odpowiednią do formowania bagietek. Praca ze świeżą EMPASTA, wyciśniętą z tuby, jest niemożliwa. Im starsza jest Empasta, tym lepiej. W palecie EMPASTA nie wysycha, zachowuje gęstość i jest idealna do pracy.</p>
 
  <p> <b>Jakie pędzle potrzebujemy do pracy z produktami E.Mi? </b>
    <br />
      Do pracy z materiałami E.Mi zalecane są trzy zestawy pędzli: "Zestaw pędzli do malowania artystycznego", "Zestaw pędzli do one stroke" i "Zestaw pędzli uniwersalnych".</p>
 
  <p> <b>Jak prawidłowo dbać o pędzle?</b>
    <br />
      Aby utrzymać pędzel tak długo, jak to możliwe, nie używaj płynów zawierających aceton do czyszczenia, po prostu wytrzyj go wilgotną ściereczką. Pędzel łatwo chwyta również rozproszone promienie ultrafioletu.</p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>