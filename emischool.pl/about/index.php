<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("About company");
?><div class="bx_page">
	<div class="h1-top">
		<h1>O firmie</h1>
	</div>
	<div class="bx_page" style="text-align: center;">
 <img width="1140" alt="56.jpg" src="/upload/medialibrary/f7c/f7cb52963256ebe4b40f9af5c03a36d2.jpg" height="300" title="56.jpg" border="0"> <br>
	</div>
 <br>
	<p>
        Marka E.Mi – to tysiące aktualnych i ekskluzywnych gotowych rozwiązań, stworzonych przez mistrzyni świata stylizacji paznokci Ekaterine Miroshnichenko jak i niepowtarzalne produkty oraz akcesoria do art paintingu paznokci, opracowanych z jej osobistym udziałem. </p>
    <p> Nasza misja –wyłuskanie z każdej stylistki paznokci jej twórczej kreatywności, pomóc w zostaniu prawdziwym artystą i fashion-expertem swojego rzemiosła. </p>
    <p> Marka E.Mi – to prężnie rozwijający się biznes, częścią którego praktycznie może zostać każdy! Na chwilę obecną to pięćdziesiąt przedstawicielstw Szkoły stylizacji paznokci Ekateriny Miroshnichenko w Rosji, krajach Europejskich i Azji, oficjalnych dystrybutorów marki E.Mi i setki punktów sprzedaży na całym świecie.
    </p>
	<p>
 <a href="/social/open_school.php" type="button" class="btn-pink order-button">Zostań partnerem</a>
	</p>
	<p>
 <br>
	</p>
	<h2>GOTOWE ROZWIĄZANIA NAIL&FASHION<br>
 </h2>
	<p>
 <img width="1140" alt="54.jpg" src="/upload/medialibrary/cd4/cd48dcf71294554d8839eab0a34a4986.jpg" height="150" title="98.jpg" border="0"> <br>
	</p>

    <p>Świat mody– to prawdziwa sztuka. Oczarowuje, inspiruje i powoduje, że z niecierpliwością oczekujemy nowego sezonu, który zawsze zadziwia i sprawia niespodzianki.</p>
    <p>Gotowe rozwiązania E.Mi– to nail design od couture. W ciągu roku przedstawiamy kilka ekskluzywnych kolekcji, opracowanych przez mistrzyni świata stylizacji paznokci w nominacji       Fantasy w wersji ОМС (Paryż, 2010), dwukrotnej mistrzyni Europy (Ateny, Paryż, 2009), sędzi międzynarodowej klasy i założycielki autorskiej szkoły stylizacji paznokci Ekaterinę Miroshnichenko</p>
    <p>Każda jej kolekcja –autorski hołd samym aktualnym trendom, które przełożone są nа język nail-design i zaadaptowane do salonowego wykonania: niewiarygodnie piękne i niepowtarzalne na pierwszy rzut oka, lekko ulegną każdej, nawet nie mającej artystycznego podłoża stylistce paznokci po odbytym u nas szkoleniu lub warsztatach wideo.</p>
    <p>Wsród kolekcji takie bestsellery , jak «Zhostovo», «One-stroke», «Reptile», «Crackle manicure», «Ethnik prints», «Velvet sand & Liquid stone» i inne. Zainspiruj się pełnym zbiorem już teraz!</p>

	<p>
 <a href="/social/salon.php" type="button" class="btn-pink order-button">Wybierz salon</a>
	</p>
	<p>
 <br>
	</p>
	<h2>PRODUKTY</h2>
	<p>
 <img width="1140" alt="6542.jpg" src="/upload/medialibrary/6e3/6e3b48a3e4405516b24de034f7ca5b60.jpg" height="150" title="6542.jpg" border="0"> <br>
	</p>
	<p>
        Przy powstaniu każdego nowego koloru lub dekoracji  Ekaterina Miroshnichenko uczestniczy osobiście. Produkcja E.Mi sprosta wymaganiom każdej stylistki paznokci, biorąc pod uwagę wszystkie tajniki pracy, pozwoli skrócić czas pracy oraz daje możliwości rozwoju artystycznego.<br>
	</p>
	<p>
        Prezentujemy takie wyjątkowe produkty, jak EMPASTA, GLOSSEMI, TEXTONE, PRINCOT, а tak że całe kolekcje paint żeli, designerskiej folii w najbardziej aktualnych kolorach, mnóstwo produktów do zdobienia i dekorowania paznokci. Znajdź swój wyjątkowy i niezbędny produkt już teraz.
	</p>
	<p>
 <a href="/catalog/" type="button" class="btn-pink order-button">Wybrać</a>
	</p>
	<p>
 <br>
	</p>
	<h2>SKOŁA STYLIZACJI PAZNOKCI EKATERINY MIROSHNICHENKO<br>
 </h2>
	<p>
 <img width="1140" alt="765.jpg" src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" height="150" title="765.jpg" border="0"> <br>
	</p>
	<p>
        Szkoła stylizacji paznokci Ekateriny Miroshnichenko proponuje wysoce efektywne programy szkoleniowe, opracowane z myślą o stylistkach paznokci, nieposiadających wykształcenia artystycznego. Krok po kroku, od łatwego do bardziej złożonego nasze programy szkoleniowe zaprowadzą Cię na szczyt profesjonalizmu w modelowaniu i stylizacji paznokci.<br>
	</p>
	<p>
        Szkolenia podzielone są na 3 stopnie zaawansowania, oraz kursy dodatkowe. Dla stylistek biorących udział w mistrzostwach Nail-art, proponujemy specjalny program przygotowujący. Nasze absolwentki– wielokrotne mistrzyni regionalnych oraz międzynarodowych mistrzostw. Wybierz swój  program szkoleniowy już teraz na naszej stronie.
	</p>
	<p>
 <a href="/courses/" type="button" class="btn-pink order-button">Wybrać</a>
	</p>
	<p>
 <br>
	</p>
	<h2>OFICJALNE PRZEDSTAWICIELSTWA</h2>
	<p>
 <img width="1140" alt="7645.jpg" src="/upload/medialibrary/060/060926d856ef3ef2bcc0eef18fa948cc.jpg" height="150" title="7645.jpg" border="0"> <br>
	</p>
	<p>
        Marka E.Mi oraz szkoła stylizacji paznokci Ekateriny Miroshnichenko –aktualny kierunek biznesu, który zyskuje co raz większą popularność i popyt międzynarodowy. Na ten moment oficjalne przedstawicielstwa z powodzeniem działają w Rosji, Ukrainie, Kazachstanie, Białorusi, Włoszech, Portugalii, Rumunii, Cyprze, Niemczech, Francji, Litwie, Łotwie, Estonii,  Anglii, Irlandii, Polsce, Grecji, Tunezji, Afryce Południowej ,Arabskich Emiratach , a produkty E.Mi nabyć można na całym świecie.
	</p>
	<p>
        Ty też możesz zostać częścią międzynarodowego teamu marki otrzymując gotowe biznesowe rozwiązania, zostać oficjalnym przedstawicielem lub dystrybutorem marki w swoim mieście, regionie lub państwie. Oficjalne przedstawiciele mają możliwość reprezentowania marki w swoim regionie, prowadzą szkolenia stylizacji paznokci, otrzymują kompleksowe programy rozwoju własnego biznesu, pomóc w doborze i szkoleniu  personelu; wsparcie reklamowe oraz korzystne rozwiązania na zakup całości asortymentu produkcji E.Mi.
	</p>
	<p>
        Aby zostać oficjalnym przedstawicielem lub dystrybutorem marki, wypełnij formularz na stronie lub skorzystaj z telefonu 8 (906) 4272586, e-mail <a href="mailto:smirnova@emi-school.ru">smirnova@emi-school.ru</a>
	</p>
	<p>
 <br>
	</p>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>