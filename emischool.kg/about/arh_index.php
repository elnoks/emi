<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О магазине");
?> 
<div class="bx_page">  
  <br />
 	 
  <p>Мы рады приветствовать вас на сайте нашей компании.</p>
 
  <p style="text-align: justify;"> <img src="/upload/medialibrary/d0b/d0b7d53675283c581d621f5b408161fa.jpg" title="logo_school.jpg" border="0" alt="logo_school.jpg" width="152" height="86"  /></p>
 
  <p style="text-align: justify;"><b>Школа ногтевого дизайна Екатерины Мирошниченко</b>  предлагает обучение дизайну ногтей различной степени сложности: от азов до вершин мастерства. Курсы по дизайну ногтей построены таким образом, что любой, даже не умеющий рисовать человек сможет по окончании обучения повторить сложные авторские произведения. В основе всех курсов по дизайну ногтей лежат авторские методики <b>Екатерины Мирошниченко</b>. Курсы по дизайну ногтей разделены на 3 уровня сложности и дополнительные курсы. Тем, кто стремится покорить звездный олимп и участвовать в конкурсах по нейл-арту, школа ногтевого дизайна предлагает специальные курсы конкурсной подготовки. Ученики школы ногтевого дизайна <b>Екатерины Мирошниченко</b> многократные победители региональных и федеральных конкурсов по дизайну ногтей.</p>
 
  <p style="text-align: justify;"> 
    <br />
   </p>
 	 
  <p style="text-align: justify;"><b> </b><img src="/upload/medialibrary/ecc/ecc2058f09f3852f399073df94a8a76f.jpg" title="logo_EMi.jpg" border="0" alt="logo_EMi.jpg" width="150" height="94"  /></p>
 
  <p style="text-align: justify;"><b>Марка E.Mi</b> - продукция для дизайна ногтей, представляемая Екатериной Мирошниченко, создана с учетом всех нюансов работы нейл-дизайнера и имеет высокое качество. <b>E.Mi</b> &mdash; это линейка красок гелевых и кистей, произведенных в Германии по специальному заказу, а так же декораций для дизайна ногтей и фирменных аксессуаров для работы. Работать на продукции <b>E.Mi </b>так удобно, что вы сможете воплотить свои самые смелые замыслы. Полимеризация красок гелевых происходит за 1 минуту, а некоторые краски полимеризуются за считанные секунды, что значительно сокращает время работы. Можно наносить дизайн сразу на все пальцы руки &ndash; особая консистенция краски гелевой не дает им растекаться, и вы всегда получите такой результат, какой пожелаете. С продукцией <b>E.Mi </b>легко быть первоклассным мастером!</p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>