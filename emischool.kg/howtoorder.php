<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как заказать");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>How to make an order.</h1>
   </div>
 </div>
 
<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

 
<div class="bx_page"> 
  <p>1.	Only orders placed via e-shop are accepted. Orders placed via phone or e-mail will not be processed.
    <br />
   </p>
 
  <p>2.	To avoid any problems with delivery we kindly ask you to control your personal contact details before placing an order. Data required: name and surname, cell phone number including country code, email and shipping address (street, house number, city, ZIP, country).
    <br />
   </p>
 
  <p>3.	In case of any mistakes in contact details Company is not responsible for any additional expenses related to delivery to wrong address.
    <br />
   </p>
 
  <p>4.	We accept and process orders from all countries except Russia, Ukraine, Kazakhstan, Belorussia, Armenia and Azerbaijan. Orders from the countries mentioned above will be redirected to e-shop at www.emi-school.ru.</p>
 
  <p>5.	When finishing the order, Client receives an automatic email notification with order summary without shipping rates. <b>Attention! Please do not provide payment on the basis of automatic notification!</b> Invoice with total amount including delivery costs will be sent within 2 business days. Payment details will be sent in the accompanying letter. Invoice is charged in euros or in dollars if necessary. Please inform our e-shop manager about payment in dollars in advance.
    <br />
   </p>
 
  <p>6.	Invoice is payable within 7 business days after issuing date. In case on non-payment within the permitted term order is automatically cancelled.
    <br />
   </p>
  <p>7.	If you are not able to provide payment within the permitted term please contact our e-shop manager via email post@emischool.com or phone +420 773 208 276 in advance.
    <br />
   </p>
  <p>8.	Before paying the invoice please check if products and products amounts in the invoice are correct.
    <br />
   </p>
  <p>9.	Packaging and shipping are processed within 3 business days after receiving of the payment.
    <br />
   </p>

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>