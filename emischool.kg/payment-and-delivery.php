<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оплата и доставка");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Payment and delivery</h1>
   </div>
 
  <br />

  <p class="MsoListParagraphCxSpFirst" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.<span style="font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">      </span></span></b><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Prices.<o:p></o:p></span></b></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.1<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">All the e-shop prices include 21% VAT.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.2<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Products are supplied without 21% VAT to non-EU countries. <o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.3<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">For all orders over 500 EUR is valid 5% discount (discount is not fundable and is valid only for one order). <o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.4<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Prices do not include delivery expenses.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.5<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">All taxes and other possible expenses in accordance with Client’s country laws pays Client.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpLast" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;"><o:p> </o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><b><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">2. Payment<o:p></o:p></span></b></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">2.1 All payments are effected on the basis of issued invoice.<o:p></o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">2.2 Before paying the invoice please check if products and products amounts in the invoice are correct.<o:p></o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">2.3 We accept payments via:<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpFirst" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; font-family: Symbol;">·<span style="font-stretch: normal; font-size: 7pt; font-family: 'Times New Roman';">         </span></span><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">Bank transfer to our account.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; font-family: Symbol;">·<span style="font-stretch: normal; font-size: 7pt; font-family: 'Times New Roman';">         </span></span><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">Western Union.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpLast" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;"> </span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3. Shipping conditions.<o:p></o:p></span></b></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.1 Products are shipped internationally by TNT Transporting Company.<o:p></o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.2 Shipping rates are not fixed and depend on package weight and shipping destination.<o:p></o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">Transport expenses are counted and shown in the issued invoice (POST).<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpFirst" style="margin-left: 18pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.3.</span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">Packaging and shipping are processed within 3 business days after receiving of the payment.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpLast" style="margin-left: 18pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.4.</span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">All shipping terms are estimated and depend on shipping destination.</span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
 <span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">When order is shipped, TNT Transporting Company sends Client an email with a web link for tracking of the order and tracking number.</span> </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>