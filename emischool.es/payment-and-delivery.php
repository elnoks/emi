<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оплата и доставка");
?><div class="bx_page">
	<div class="h1-top">
		<h1>Payment and delivery</h1>
	</div>
	<p>
		<br>
	</p>
	<p>
 <b>1&nbsp;&nbsp; &nbsp; Precios</b>
	</p>
	<p>
		 1.1&nbsp;&nbsp;Todos los precios de e-shop incluyen el 21% de IVA.
	</p>
	<p>
		 1.2&nbsp;&nbsp;Los productos se suministran sin 21% de IVA a países no Schengen.
	</p>
	<p>
		 1.3&nbsp;&nbsp;Para todos los pedidos superiores a 500 EUR, es válido un descuento del 5% (el descuento no es financiable y solo es válido para un pedido).
	</p>
	<p>
		 1.4&nbsp;&nbsp;Los precios no incluyen los gastos de envío.
	</p>
	<p>
		 1.5&nbsp;&nbsp;Todos los impuestos y otros posibles gastos de acuerdo con las leyes del país del cliente paga al cliente.
	</p>
	<p>
		<br>
	</p>
	<p>
	</p>
	<p>
 <b>2&nbsp;&nbsp; &nbsp;Pago</b>
	</p>
	<p>
		 2.1 Todos los pagos se efectúan sobre la base de la factura emitida.
	</p>
	<p>
		 2.2 Antes de pagar la factura, compruebe si los importes de productos y productos en la factura son correctos.
	</p>
	<p>
		 2.3 Aceptamos pagos a través de:
	</p>
	<p>
	</p>
	<ul>
		<li>
		<p>
            Transferencia bancaria a nuestra cuenta.
		</p>
 </li>
	</ul>
	<div>
		<br>
	</div>
	<p>
	</p>
	<p>
	</p>
	<p>
 <b>3&nbsp;&nbsp; &nbsp;Condiciones de envío</b>
	</p>
	<p>
		 3.1 Los productos son enviados internacionalmente por TNT Transporting Company.
	</p>
	<p>
		 3.2  Las tarifas de envío no son fijas y dependen del peso del paquete y el destino del envío.
	</p>
	<p>
        Los gastos de transporte se cuentan y se muestran en la factura emitida (POST).
	</p>
	<p>
		 3.3 El embalaje y el envío se procesan dentro de los 3 días hábiles posteriores a la recepción del pago.
	</p>
	<p>
		 3.4 Todos los términos de envío se estiman y dependen del destino del envío.
	</p>
    Cuando se envía el pedido, TNT Transporting Company envía al Cliente un correo electrónico con un enlace web para rastrear el pedido y el número de seguimiento.<br>
 <br>
	<p>
 <br>
	</p>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>