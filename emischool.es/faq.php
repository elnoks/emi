<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Tips & tricks");
?> 
<div class="bx_page"> 
<div class="h1-top">
		<h1>Tips & tricks</h1>
    </div>
 <br>
  <p> <b>Cómo convertirse en un representante oficial de la marca E.Mi y la escuela de diseño de uñas de Ekaterina Miroshnichenko?</b>
    <br />
      La compañía E.Mi ofrece soluciones comerciales ya preparadas para lanzar negocios de rápido crecimiento e interesantes. Puede abrir una oficina de representación oficial de la escuela de diseño de uñas por Ekaterina Miroshnichenko en su ciudad; convertirse en un distribuidor oficial de la marca E.Mi o un representante mayorista. La información detallada se puede encontrar en la sección "Cómo abrir una oficina de representación de la Escuela"</a></p>
 
  <p> <b>Si hay una escuela de diseño de uñas en mi ciudad o no?</b>
    <br />
      La Escuela de diseño de uñas de Ekaterina Miroshnichenko en tu ciudad se puede encontrar en la sección <a href="/contacts/" >“Contactos”</a>. </p>
 
  <p> <b>Dónde puedo comprar los productos de E.Mi?</b>
    <br />
      Puede comprar los productos E.Mi en nuestro sitio web en la sección "Productos" (consulte la sección "Cómo realizar un pedido" para obtener instrucciones detalladas) o en un punto de venta. Puede obtener más información sobre los puntos de venta en la sección  <a href="/contacts/" >“Contactos”</a>. </p>
 
  <p> <b>Puedo obtener calificaciones básicas como una manicurista, un maestro de pedicura, un maestro de extensión de uñas en la Escuela de diseño de uñas por Ekaterina Miroshnichenko?</b>
    <br />
      La escuela de diseño de uñas de Ekaterina Miroshnichenko se especializa en el diseño de uñas solamente.</p>
 
  <p> <b>Can I get the training course at the Nail design school by Ekaterina Miroshnichenko without working experience of nail master?</b> 
    <br />
      Puedo obtener el curso de capacitación en la escuela de diseño de uñas de Ekaterina Miroshnichenko sin experiencia laboral de maestro de uñas?</p>
 
  <p> <b>Qué curso es mejor para empezar?</b>
    <br />
      Le recomendamos que comience con cursos básicos: El ABC de las líneas, curso básico y tecnologías de diseño E.Mi, donde aprenderá a trabajar con geles de colores y se convertirá en una mano hábil. Después de estos cursos, será fácil de aprender en el segundo y tercer nivel de complejidad, como la pintura Zhostovo, la pintura de One-stroke, los adornos florales sofisticados, la abstracción compleja, etc.</p>
 
  <p> <b>Dónde puedo encontrar el horario de los cursos en la escuela de diseño de uñas de Ekaterina Miroshnichenko en mi ciudad?</b>
    <br />
      Más detalles sobre el calendario de cursos que puede encontrar en la sección  <a href="/courses/#schedule" >“Horario”</a>. </p>
 
  <p> <b>Cuál es la edad mínima de los estudiantes de la escuela de diseño de uñas por Ekaterina Miroshnichenko?</b>
    <br />
      Aprendices a la edad de 15 años son aceptados.</p>
 
  <p> <b>Puedo aplicar las pinturas de gel E.Mi sobre el gel de otra marca?</b>
    <br />
      Sí, las pinturas de gel E.Mi se pueden aplicar sobre de gel, pero debe recordar que la mayoría de los  geles se eliminan mediante un esmalte de gel especial, mientras que las pinturas de gel de E.Mi se liman. Es por eso que recomendamos hacer un diseño o un adorno en una pequeña superficie de una uña, cubierta con esmalte de gel. Por ejemplo, arena de terciopelo y piedras líquidas o diseños vintage en 3D.</p>
 
  <p> <b>Puedo aplicar las pinturas de gel E.Mi en una uña natural?</b>
    <br />
      Antes de aplicar la pintura de gel E.Mi, una placa de  uña debe cubrirse con gel, acrílico o gel. Aplicar la pintura de gel E.Mi en una placa de uñas natural está prohibida.</p>
 
  <p> <b>Cuál es la diferencia entre las pinturas en gel E.Mi y EMPASTA?</b>
    <br />
      Las principales diferencias son la consistencia y la pegajosidad residual. E.Mi EMPASTA es más grueso, por lo que el trazo o una línea es más preciso, y la textura EMPASTA para el modelado de moldeo permite hacer diseños en 3D. Además, EMPASTA no tiene adherencia residual, por lo que se puede aplicar tanto sobre como debajo del gel de acabado. Secado intermedio para EMPASTA E.Mi One Stroke dura solo 2-5 segundos. Ayuda a dibujar el diseño con una gran cantidad de elementos más rápido. Las pinturas de gel E.Mi son más líquidas, son ideales para adornos.</p>
 
  <p> <b>Cuál es la diferencia entre Liquid stones polymer y Liquid stones gel?</b>
    <br />
      El gel "Liquid stones" es un material adicional para diseñar "Liquid stones". Ayuda a imitar piedras preciosas en uñas de diferentes tamaños y alturas y no requiere una capa superior / esto permite ahorrar tiempo de operación. El gel de "piedras líquidas" es transparente y no se puede mezclar con pigmentos. El polímero "piedras líquidas" imita las piedras de colores ya que puede mezclarlo con pigmentos y debe estar recubierto con gel superior con pegajosidad residual.</p>
 
  <p> <b>Cómo utilizar la pintura en gel EMPASTA Openwork Plata para el diseño de encuadre?</b>
    <br />
      EMPASTA Openwork plata contiene corte transparente, es por eso que presione una pequeña cantidad de pintura en la paleta y mezcle bien; después de esto, deje que la pintura dure algunos días para que tenga una textura gruesa adecuada para el diseño del encuadre. Trabajar con EMPASTA fresco es imposible. Mientras más se mantenga EMPASTA, mejor será. EMPASTA no se seca, se dibuja y se vuelve ideal para el trabajo.</p>
 
  <p> <b>Qué pinceles necesito para trabajar con los materiales de E.Mi?</b>
    <br />
      Recomendamos 3 juegos de pinceles para trabajar con los materiales E.Mi: Set de pinceles Desing Technologies, Set de pinceles para pintura One Stroke y set de pinceles para pintura Zhostovo.</p>
 
  <p> <b>Cómo debo cuidar los pinceles?</b>
    <br />
      No use líquidos que contengan acetato de etilo para la limpieza, la servilleta húmeda es suficiente. También el pincel se establece rápidamente a partir de los rayos dispersos ultravioleta.</p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>