<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Как заказать");
?>
    <div class="bx_page">
        <div class="h1-top">
            <h1>How to make an order.</h1>
        </div>
    </div>

    <div class="bx_page">
        <br/>
    </div>

    <div class="bx_page">
        <br/>
    </div>

    <div class="bx_page">
        <br/>
    </div>


    <div class="bx_page">
        <p>1. Solo se aceptan pedidos realizados a través de e-shop. Los pedidos realizados por teléfono o correo
            electrónico no serán procesados.
            <br/>
        </p>

        <p>2. Para evitar cualquier problema con la entrega, le pedimos que controle sus datos personales de contacto
            antes de realizar un pedido. Datos requeridos: nombre y apellido, número de teléfono, dirección de correo
            electrónico y envío (calle, número de casa, ciudad, código postal, país).
            <br/>
        </p>

        <p>3. En caso de errores en los detalles de contacto, la Empresa no se hace responsable de ningún gasto
            adicional relacionado con la entrega a una dirección incorrecta.
            <br/>
        </p>

        <p>4. Aceptamos y procesamos pedidos de Andalucia.</p>

        <p>5. Al finalizar el pedido, el Cliente recibe una notificación automática por correo electrónico con un
            resumen de pedido sin tarifas de envío. <b>Atención! No proporcione el pago en base a la notificación
                automática!</b> La factura con el precio total, incluidos los gastos de envío, se enviará dentro de los
            2 días hábiles. Los detalles del pago se enviarán en la carta adjunta. La factura se carga en euros.
            <br/>
        </p>

        <p>6.. La factura se paga dentro de los 7 días hábiles después de la fecha de emisión. En caso de no pago dentro
            del plazo permitido, la orden se cancela automáticamente.
            <br/>
        </p>
        <p>7. Si no puede proporcionar el pago dentro del plazo permitido, comuníquese con nuestro gerente de e-shop a
            través del correo electrónico emischoolandalucia@gmail.com o llame al +34 696 753 883, +34 955 138477 con
            anticipación.
            <br/>
        </p>
        <p>8. Antes de pagar la factura, compruebe si los precios de productos y productos en la factura son correctos.
            <br/>
        </p>
        <p>9. El embalaje y el envío se procesan dentro de los 3 días hábiles posteriores a la recepción del pago.
            <br/>
        </p>

    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>