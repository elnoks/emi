<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Cómo abrir la oficina de representación de la escuela E.Mi");
?><div class="bx_page">
	<p style="text-align: center;">
 <img width="800" alt="765.jpg" src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" height="105" title="765.jpg" border="0"><b> <br>
 </b>
	</p>
	<p>
 <b> <br>
 </b>
	</p>
	<p>
 <b>La cooperación con la escuela de diseño de uñas de Ekaterina Miroshnichenko es una oportunidad para obtener una solución comercial preparada y rentable. Obtiene una gama completa de tecnologías: desde el diseño hasta la gestión y la promoción. La escuela original de diseño de uñas de Ekaterina Miroshnichenko siempre presenta las imágenes más recientes, demandadas, elegantes y reconocidas.</b>
	</p>
	<p>
 <b>Sé nuestro representante regional y te enseñaremos a ser tan exitoso y creativo como nosotros!</b> Hoy la Escuela de diseño de uñas de Ekaterina Miroshnichenko es conocida en todo el mundo: prestigiosos premios en concursos internacionales, seminarios en Alemania e Italia, la apertura de una oficina internacional en Europa. <br>
        Puede adquirir el derecho exclusivo de dirigir un negocio altamente rentable e interesante sobre educación y formación avanzada de maestros del diseño de uñas por la marca School of nail design.
	</p>
	<p>
 <b>Únete a los que están en tendencia!</b> La escuela de diseño de uñas de Ekaterina Miroshnichenko creó ambas tecnologías únicas de diseño y educación reconocibles. <br>
 <b>Creatividad y belleza es posible con nosotros!</b> <br>
 <b>Las ventajas de la Escuela de diseño de uñas de Ekaterina Miroshnichenko:</b>
	</p>
	<ul>
		<li>los servicios educativos tienen una gran demanda. Más de 500 maestros de todas partes de Rusia y en el extranjero estudian diferentes programas anualmente;</li>
		<li>los materiales de la marca  <a href="/catalog/">E.Mi</a> se utilizan durante la educación</li>
		<li>el nivel internacional de la compañía. Hay una oficina en Europa (Praga);</li>
		<li>los programas educativos se basan en métodos únicos desarrollados y patentados por Ekaterina Miroshnochenko, una campeona mundial en nominación de diseño de uñas según OMC (Paris, 2011);</li>
	</ul>
	<p>
	</p>
	<p>
 <b>proporcionamos a nuestras oficinas de representación todos los materiales necesarios para el trabajo exitoso del departamento educativo. <br>
     La apertura de la oficina de representación de la Escuela de diseños de uñas le ofrece lo siguiente:</b>
	</p>
	<ol>
		<li><b>Un derecho exclusivo </b>a trabajar con el nombre de la Escuela de diseño de uñas por Ekaterina Miroshnichenko y proporcionar servicios educativos en el territorio señalado en un contrato. Solo una oficina de representación puede operar en una región de España.</li>
		<li><b>Programas educativos </b>desarrollados por Ekaterina Miroshnichenko. El centro educativo ahorra tiempo y dinero en el desarrollo del plan de estudios, métodos de capacitación, material educativo y manuales. Todo esto permite enfocarse en la búsqueda de estudiantes y compensar los gastos de apertura en el menor tiempo posible.</li>
		<li><b>El sistema unido de certificación. </b> La oficina de representación tiene derecho a entregar certificados de sus estudiantes con el holograma de la escuela de diseño de uñas por Ekaterina Miroshnichenko.</li>
		<li><b>El derecho de un descuento exclusivo en los productos <a href="/catalog/">E.Mi</a>.</b></li>
		<li><b>Soporte promocional.</b> . La escuela del autor coloca toda la información sobre sus representantes en revistas profesionales líderes, así como en su propio catálogo publicitario y sitio web oficial; ayuda a organizar exposiciones regionales y conferencias de uñas, ofrece diseños publicitarios para publicar en publicaciones periódicas locales.</li>
	</ol>
	<p>
	</p>
	<p>
 <b>La cooperación como representante oficial de la Escuela de diseño de uñas por Ekaterina Miroshnichenko y un distribuidor oficial de la marca Emi.</b> <br>
        El representante oficial de la Escuela de diseño de uñas por Ekaterina Miroshnichenko tiene el derecho de preferencia en el contrato de la distribución de la marca E.Mi en el territorio, señalado en los términos del contrato. En este caso, Ekaterina Miroshnichenko se suma a los requisitos y condiciones adicionales de cooperación del distribuidor, así como a las obligaciones del precio de compra con un aumento de descuento relevante en los productos de la marca EMi, a los requisitos de los representantes oficiales de la Escuela de diseño de uñas.
	</p>
	<p>
 <b>Los requisitos para abrir una oficina de representación de la Escuela de diseño de uñas por Ekaterina Miroshnichenko:</b>
	</p>
	<ol>
		<li>Un instructor con talento, capaz de copiar con precisión la técnica de Ekaterina Miroshnichenko. Un candidato primero estudia como aprendiz y si él o ella tiene una calificación adecuada de acuerdo con los requisitos de Ekaterina Miroshnichenko, va a un curso especial de instructores.</li>
		<li>Una instalación separada para un centro de entrenamiento de no menos de 16 metros cuadrados. Colocación de letrero de marca según la Escuela de diseño de uñas por el estilo Ekaterina Miroshnichenko.</li>
		<li>Disponibilidad de una entidad legal que tiene derecho a proporcionar servicios educativos (persona autónoma, institución educativa no gubernamental, instituciones educativas privadas, etc.)</li>
		<li>Capital de trabajo para realizar la primera compra de productos, equipar el centro de capacitación y pagar el estudio de un instructor.</li>
		<li>Compromiso representativo de atraer nuevos aprendices sobre la base de las tecnologías desarrolladas.</li>
	</ol>
	<p>
	</p>
	<p>
 <b>Queridos maestros, si están realmente interesados en abrir la oficina de representación de la escuela en su región, primero deben:</b>
	</p>
	<ol>
		<li>saber si hay una oficina de representación en su región. Puede obtener información al respecto en Contactos en nuestro sitio web o llamando al número&nbsp;+420 774 208 276. Si ya hay una oficina de representación en su región, tendremos que rechazarla.</li>
		<li>Envíe fotos con los diseños de las uñas con los diseños de Ekaterina Miroshnichenko en nuestro correo electrónico <a href="mailto:sviridova@emi-school.ru">sviridova@emi-school.ru</a> (por ejemplo, adornos florales sofisticados, rizos, fresa, frambuesa, cereza, etc.). Es necesario para su cita como instructor, ya que su estilo debe corresponderse con los requisitos de Ekaterina Miroshnichenko. Debe copiar con precisión su diseño y técnica.</li>
		<li><b>Sepa la tarifa de educación llamando al número: +420 773 208 276.</b> La educación del instructor se lleva a cabo en dos pasos: como aprendiz y como instructor. La educación se lleva a cabo solo en el centro de capacitación en Rostov-on-Don. El costo depende directamente de la cantidad del curso.</li>
		<li><b>Conozca el volumen de compra en el trimestre llamando al +420 773 208 276.</b> El volumen de compra se fija en el contrato.</li>
		<li>Prepárese para entrenamientos avanzados regulares. El contrato del instructor se firma por el término de un año. Cada año, la propia instructora confirma la calificación en Rostov-on-Don y firma un contrato para el próximo año.</li>
	</ol>
	<p>
	</p>
	<p>
 <b>Para obtener información adicional sobre cómo abrir una oficina de representación de la Escuela de diseño de uñas de Ekaterina Miroshnichenko, llame a la oficina internacional:<br>
		 Korbut Marina, <a href="mailto:korbut@emischool.com">korbut@emischool.com</a>,&nbsp;+420 773 208 276</b>
	</p>
 <br>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>