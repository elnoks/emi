<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetPageProperty("description", "Школа ногтевого дизайна Екатерины Мирошниченко E.Mi проводит курсы по дизайну ногтей в Москве. Также осуществляем продажу оригинальных товаров для дизайна ногтей. Как заказать");
$APPLICATION->SetTitle("Kā pasūtīt");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Kā pasūtīt</h1>
   </div>
 </div>
 
<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>



<div class="bx_page">Iegādājaties E.Mi produkciju uzreiz – te un tagad – izmantojot tiešsaistes pasūtījuma opciju.</div>
 
<div class="bx_page">
  <p> 1. Veiciet vienkāršu reģistrēšanos mājas lapā.
    <br />
   </p>

  <p> 2. Pārejiet šis mājas lapas sadaļā “Produkcija” un pievienojiet Jums nepieciešamos produktus iepirkumu grozā, uzklikšķinot uz rozā pogas “Pievienot grozam”, kas atrodas pa labi no izvēlētās preces foto. Ja prece tika veiksmīgi pievienota iepirkumu grozam, šī poga kļūst zaļa un uz tās parādās uzraksts “Ielikts grozā”. Pēc tam Jūs varat turpināt savu iepirkšanos.
    <br />
   </p>

  <p> 3. Pēc tam, kad Jūs nobeidzāt iepirkuma procesu, nepieciešams pārbaudīt sava pasūtījuma statusu “Iepirkumu grozu”. Lai veiktu šādu pārskatu nepieciešams noklikšķināt uz saiti “Mans grozs”, kas atrodas mājas lapas virsējā augšējā daļā. Šeit Jūs varat veikt pasūtījuma korekciju, mainīt visu nepieciešamo produkcijas daudzumu un aplūkot pirkuma kopējo summu.
    <br />
   Ja Jūs esat mainījuši produkcijas daudzuma apjomu, esat dzēsis kādas Jūsu pasūtījuma pozīcijas vai arī pievienojāt jaunas, lūdzam Jūs nospiest “Pārrēķināt” pogu, lai redzētu pirkuma aktuālo summu.
    <br />

  </p>
    <p>
      Uzmanību! Minimālā E.Mi produkcijas pasūtījuma summa Rīgā – 15 EUR.<br />
        <span style="padding-left: 345px">ārpus Rīgas – 50  EUR bezmaksas piegāde</span><br />
      Veicot produkcijas pasūtījumu, spēkā ir šāda atlaižu sistēma:<br />
	      - pasūtījuma summa no 200 EUR – 10% atlaide.
    </p>

  <p> 4. Lai noformētu izveidotu un nokomplektētu pasūtījumu, lūdzam Jūs nospiest rozā krāsas pogu “Noformēt pasūtījumu”, kas atrodas ekrāna labējā stūrī.</p>

  <p> 5. Izvēlieties piegādes veidu.</p>


  <p> 6. Izvēlieties apmaksas veidu.</p>

  <p> 7. Noklikšķiniet “Noformēt pasūtījumu” un turpiniet pasūtījuma apmaksu.</p>

  <p>
    <br />
   </p>

  <p><b>Interneta veikala menedžera tālrunis: +371 22400008</b></p>

  <p><b>E-pasts: emischoollatvia@gmail.com</b></p>


    <h3>GARANTIJA</h3>





    <p>
        1. Visām www.emischool.lv pirktajām precēm ir ražotāja garantija.
    </p>
    <p>
        2. Ražotāja noteiktais garantijas termiņš precēm ir atšķirīgs, tas var būt līdz pieciem gadiem.
    </p>
    <p>
        3. Informācija par ražotāja garantijas termiņu konkrētajai precei ir meklējama preces aprakstā vai ir iespējams pajautāt www.emischool.lv klientu konsultantam.
    </p>
    <p>
        4. Gadījumos, kad ražotājs kādai precei būs noteicis garantijas termiņu, kas īsāks par diviem gadiem, patērētājs ir tiesīgs divu gadu laikā no pirkuma brīža iesniegt prasību par preces neatbilstību līguma noteikumiem.
    </p>
    <p>
        5. Pretenzijas pieteikšanas tiesības ir spēkā, ja prece tika izmantota tikai paredzētājos apstākļos un tikai mājsaimniecībā.
    </p>
    <p>
        6. Juridiskām personām, kas nav Patērētājs, tiek nodrošināts tikai Ražotāja noteiktais garantijas termiņš.
    </p>
    <p>
        7. Informējam, ka ražotāja garantijas nosacījumi ir spēkā, ja pircējs var uzrādīt:
    </p>
    <p>
        7.1. pirkumu apliecinošu dokumentu (čeks, pavadzīme);
    </p>
    <p>
        7.2. ražotāja vai izplatītāja garantijas karti (gadījumā, ja ražotājs vai izplatītājs tādu ir ietvēris preces komplektācijā).
    </p>
    <p>
        7.3. informāciju par sertificētajiem ražotāja servisa centriem vari atrast, ražotāja vai izplatītāja garantijas kartē, zvanot ražotājā pārstāvniecībai vai uz preces iepakojuma norādītajam izplatītājam, vai arī zvanot pa telefonu 22400008, 27677440, 27276440, vai rakstot uz e-pastu: emischoollatvia@gmail.com.
    </p>
    <p>
        8. Informējam, ka garantijas nosacījumi nav attiecināmi uz preces papildus piederumiem (piemēram, preces aksesuāriem) un barošanas elementiem (piemēram, baterijas, akumulatori).
    </p>
    <p>
        9. Ražotāja garantijas nosacījumi nav spēkā pret bojājumiem, kas radušies pircēja vai lietotāja vainas dēļ un tie ir:
    </p>
    <p>
        9.1. precei ir garantijas plombu, sērijas numuru bojājumi;
    </p>
    <p>
        9.2. prece nav lietota tai paredzētajām vajadzībām un nav ekspluatēta tā, kā tas ir norādīts preces lietošanas instrukcijā;
    </p>
    <p>
        9.3. saskrāpētas, salauztas konstrukcijas, saplaisājis ekrāns, ieliets ūdens tam neparedzētajās vietās, nepareiziem līdzekļiem kopta prece, ja precē ir atrasti nepiederoši priekšmeti, ir pieļauta kukaiņu iekļūšana izstrādājumu iekšienē vai kādas citas pēdas, kas liecina par nepareizu preces ekspluatāciju;
    </p>
    <p>
        9.4. ja bojājums radies barojošā sprieguma, telekomunikāciju, kabeļu tīklu neatbilstībai ražotāja noteiktajiem standartiem, strauju temperatūras svārstību dēļ, kā arī citu sadzīves un ārējo faktoru dēļ, piemēram, kvēpi, dūmi, putekļi, mitrums, triecieni, skrāpējumi;
    </p>
    <p>
        9.5. ja izstrādājumā redzamas nekvalificēta remonta pēdas;
    </p>
    <p>
        9.6. dabiska elementu nolietojuma gadījumos.
    </p>
    <p>
        9.7. ja precei ir izmantoti nestandarta barošanas bloki, piederumi un rezerves daļas, kā arī izejmateriāli (piemēram, kārtridži, toneri u.c.), ko ražotājs nav sertificējis lietošanai ar noteikto preci, un ja tas ir izraisījis šīs preces bojājumus;
    </p>
    <p>
        9.8. papildus aprīkojumam garantija netiek piemērota - baterijām, austiņām, savienojošiem kabeļiem, lādēšanas ierīcēm, pultīm).
    </p>
    <p>
        9.9. ja preci pircējs ir izmantojis ražošanas vai profesionāliem mērķiem (gadījumā, ja konkrētā prece nav paredzēta šādiem nolūkiem);
    </p>
    <p>
        9.10. garantija nedarbojas, ja radušies bojājumi nepareizas preces transportēšanas rezultātā.
    </p>
    <p>
        10. Gadījumā, ja prece ir sabojājusies, pircējam ir šādas iespējas:
    </p>
    <p>
        10.1. dodies uz ražotāja vai izplatītāja garantijas talonā norādīto servisa centru (līdzi ņemot pirkumu apliecinošu dokumentu un garantijas talonu) – rekomendējam kā visātrāko preces bojājuma novēršanas variantu;
    </p>
    <p>
        10.2. vērsties Rīgā, Tallinas ielā 59, darba dienās no plkst. 9.30 līdz 18.30 (līdzi ņemot pirkumu apliecinošu dokumentu un garantijas talonu);
    </p>
    <p>
        10.3. zvanīt pa tālruni 22400008, kur pircēju uzklausīs www.emischool.lv darbinieks un ieteiks labāko risinājumu.
    </p>
    <p>
        11. Gadījumā, kad garantijas apkalpošana nav spēkā, bet pircējs atsakās no maksas remonta, tad pircējam ir jāsedz diagnostikas izmaksas, un pircējs saņem Servisa centra slēdzienu par konstatētajiem bojājumiem. Ja pircējs piekrīt maksas remontam, tad diagnostikas izmaksas nav jāsedz.
    </p>
</div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>