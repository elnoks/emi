<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Школа ногтевого дизайна Екатерины Мирошниченко | О компании E.Mi");
$APPLICATION->SetPageProperty("description", "Научиться создавать удивительные дизайны ногтей можно в школе Екатерины Мирошниченко. Для записи на курс заполните форму на сайте или обратитесь по телефону 8 (906) 427-25-86, e-mail smirnova@emi-school.ru, mbabaev@emi-school.ru.");
$APPLICATION->SetTitle("О компании");
?><div class="bx_page">
	<div class="h1-top">
		<h1>Par kompāniju</h1>
	</div>
	<div class="bx_page" style="text-align: center;"><img src="/upload/medialibrary/f7c/f7cb52963256ebe4b40f9af5c03a36d2.jpg" title="56.jpg" alt="56.jpg" height="300" border="0" width="1140"> 
<br>
</div>
<br>
<p>E.Mi kompānija piedāvā Jums tūkstošiem lielisku un gatavu ekskluzīvu nagu dizaina risinājumu, kurus izstrādājusi nagu dizaina čempione Jekaterina Mirošničenko, kā arī mēs piedāvājam Jūsu uzmanībai profesionālā līmeņa materiālus un aksesuārus nagu apgleznošanai, kas tika izstrādāti zem J. Mirošničenko personīgās vadības. </p>
<p>Mūsu misija – palīdzēt katram meistaram maksimāli atklāt un realizēt savu radošo potenciālu, kā arī beidzot sajust sevi kā patiesu mākslinieku un iegūt modes industrijas eksperta līmeņa zināšanas.</p>
<p>E.Mi kompānija ir veiksmīgs uzņēmums ar straujiem attīstības tempiem, par kura neatņemamo sastāvdaļu var kļūst ikkatrs! Pašlaik mēs apvienojam piecdesmit oficiālo Nagu dizaina skolas pārstāvniecību Krievijā, Eiropas un Āzijas valstīs, kā arī simtiem oficiālu E.Mi kompānijas produkcijas izplatītāju visā pasaulē.</p>
<h2>MŪSU GATAVIE NAIL&FASHION RISINĀJUMI</h2>
<p><img src="/upload/medialibrary/cd4/cd48dcf71294554d8839eab0a34a4986.jpg" title="98.jpg" alt="54.jpg" height="150" border="0" width="1140"> 
<br>
</p>
<p>Augstākā mode ir patiesa māksla. Tā apbur, iedvesmo un iedrošina eksperimentēt un liek ar nepacietību gaidīt jaunu sezonu, kas vienmēr sagādā jaunus un jaunus pārsteigumus.</p>
<p>Gatavie risinājumi no E.Mi kompānijas ir nagu dizains no modes mākslinieces. Visa gada garumā mēs piedāvājam vairākas ekskluzīvās kolekcijas, kuras izstrādājusi pasaules čempione nagu dizainā Fantasy nominācijā (Parīze, 2010.), divkārtējā Eiropas čempione nagu dizainā (Atēnas, Parīze, 2009.) un starptautiskās kategorijas tiesnese, kā arī savas nagu dizaina skolas dibinātāja Jekaterina Mirošničenko.</p>
<p>Katra viņas kolekcija – ir autores veiktā visaktuālāko tendenču nolasīšana, kas tiek pārvērsti nagu dizaina valodā un pielāgoti salona meistara realizācijai: fantastiski skaisti un neatkārtojami tēli, kurus viegli varēs savā darbā izmantot pat tie meistari, kuriem nav mākslinieciskās izglītības, pēc mācību kursa vai meistarklases apgūšanas.</p>
<p>Mūsu kolekcijā ir tādi hīti kā „Apgleznotais porcelāns”, „Ķīnas motīvi”, „Rāpuļu ādas imitācija”, „Krakelūra efekts”, „Etniskie raksti”, „Samtainās smiltis un šķidrie akmeņi” un vēl daudz citu. Smeļaties iedvesmu no pilnas kolekcijas tūlīt!</p>

<h2>PRODUKCIJA</h2>

<p><img src="/upload/medialibrary/6e3/6e3b48a3e4405516b24de034f7ca5b60.jpg" title="6542.jpg" alt="6542.jpg" height="150" border="0" width="1140"> 
<br>
</p>

<p> Katra produkta radīšanā, kā arī jauno krāsu un aksesuāru izstrādā Jekaterina Mirošničenko piedalās personīgi. Viņas produkcija pilnībā atbilst visiem nagu dizaina prasībām un ņem vērā visus meistaru darba nianses, kas rezultātā ļauj samazināt laiku, kas nepieciešams gleznojuma tapšanai un sniedz bezgalīgās iespējas savai radošajai izpausmei.</p>
<p>Mēs piedāvājam tādus unikālus produktus kā EMPASTA, GLOSSEMI, TEXTONE, PRINCOT, kā arī gēla krāsu, aktuālo krāsu dizaina folijas un vēl daudzu citu nagu dekorēšanai nepieciešamu materiālu un aksesuāru kolekcijas. Jūs atradīsiet Jums nepieciešamu produktu te un tagad.</p>
<h2> JEKATERINAS MIROŠNIČENKO NAGU DIZAINA SKOLA</h2>
<p><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" alt="765.jpg" height="150" border="0" width="1140"> 
<br>
</p>
<p> Jekaterinas Mirošničenko Nagu dizaina skola piedāvā Jūsu uzmanībai augstākās efektivitātes mācību programmas, kas paredzētas un izstrādātas speciāli to meistaru kvalifikācijas uzlabošanai, kuriem nav mākslinieciskās izglītības. Mācību kurss, kurš paredzēts iemaņu apgūšanai soli pa solim palīdzēs nagu dizaineriem sasniegt augstāku profesionālisma līmeni.
</p>
<p> Kursi ir iedalīti 3 sarežģītības pakāpēs, kā arī papildus kursos. Tiem, kas vēlas izmēģināt savus spēkus profesionālā olimpa iekarošanā un vēlas piedalīties nagu dizaina konkursos, mēs piedāvājam speciālus konkursu sagatavošanas kursus. Skolas audzēkņi ir daudzkārtējie reģionālo un starptautisko konkursu uzvarētāji nagu dizainā.</p>
<p>Izvēlieties savu mācību programmu mūsu mājas lapā tūlīt! </p>
<h2> OFICIĀLĀS PĀRSTĀVNIECĪBAS </h2>
<p><img src="/upload/medialibrary/060/060926d856ef3ef2bcc0eef18fa948cc.jpg" title="7645.jpg" alt="7645.jpg" height="150" border="0" width="1140"> 
<br>
</p>
<p>E.Mi zīmols un Jekaterinas Mirošničenko Nagu dizaina skola – ir aktuālie uzņēmējdarbības virzieni, kas gūst arvien lielāku popularitāti un starptautisku pieprasījumu. Pašreiz mūsu oficiālās pārstāvniecības veiksmīgi darbojās Krievijā, Ukrainā, Kazahstānā, Baltkrievijā, Itālijā, Portugālē, Rumānijā, Kiprā, Vācijā, Francijā, Lietuvā, Slovākijā, Dienvidkorejā un Apvienotajos Arābu Emirātos, bet E.Mi nagu dizaina produkciju iespējams iegādāties visās pasaules malās.</p>
<p>Jūs pats arī varat kļūt par veiksmīga starptautiska zīmola neatņemamo sastāvdaļu un saņemt savā rīcībā jau gatavu biznesa risinājumu – kļūt par mūsu zīmola oficiālo pārstāvi un izplatītāju savā pilsētā, reģionā un pat valstī. Oficiāliem pārstāvjiem pienākas vairākas privilēģijas un priekšrocības, tādas kā iespēja ekskluzīvi pārstāvēt skolas zīmolu savā reģionā, tiesības realizēt nagu dizaina kursus mūsu zīmola vārdā, kā arī mēs piedāvājam pilnu pavadību personāla atlasei un sagatavošanai, reklāmas atbalstu, izdevīgus piedāvājumus visa E.Mi produkcijas klāsta iegādei.</p>
<p>Lai kļūtu par mūsu zīmola oficiālo pārstāvi un izplatītāju, Jūs varat aizpildīt kontakta formu mūsu mājas lapā, vai arī pa tālruni ********, e-pasts: ********</p>
<p>
<br>
</p>

<p><span style="font-family: Verdana, Arial, Helvetica, sans-serif;"><font size="2">(Лицензия № 147146 от 12.12.2007)</font></span></p>
</div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>