<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetPageProperty("description", "Школа ногтевого дизайна Екатерины Мирошниченко E.Mi проводит курсы по дизайну ногтей в Москве. Также осуществляем продажу оригинальных товаров для дизайна ногтей. Оплата и доставка");
$APPLICATION->SetTitle("Piegāde un apmaksa");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Piegāde un apmaksa</h1>
   </div>
 
  <br />
 <span style="font-weight:bold;">Uzmanību! Minimālais iespējamais pasūtamās E.Mi zīmola produkcijas apjoms – 15 EUR.</span> 
  <p><span style="color: rgb(102, 102, 102); font-family: 'Pt Sans', Arial, Helvetica; font-size: 13px; line-height: 18.5714302062988px; background-color: rgb(249, 249, 249);">Apmaksu iespējams veikt caur autorizētu tiešsaistes banku,</span><span style="color: rgb(102, 102, 102); font-family: 'Pt Sans', Arial, Helvetica; font-size: 13px; line-height: 18.5714302062988px; background-color: rgb(249, 249, 249);"> izmantojot sekojošo maksājumu sistēmu kartes</span>
<ul style="list-style-type:none;">
  <li>
    <img src="/visa.png" style="float: left; position: relative; top: -20px; left: -10px;">
    <span style="color: rgb(102, 102, 102); font-family: 'Pt Sans', Arial, Helvetica; font-size: 13px; line-height: 18.5714302062988px; background-color: rgb(249, 249, 249);">Visa, </span>
  </li>
  <li style="margin-top: 44px;"> 
    <img src="/mastercard.png" style="float: left; margin-top: -37px; position: relative; left: -12px;">
    <span style="color: rgb(102, 102, 102); font-family: 'Pt Sans', Arial, Helvetica; position: relative; top: -15px;font-size: 13px; line-height: 18.5714302062988px; background-color: rgb(249, 249, 249);">MasterCard</span></p>
  </li>
</ul>
  <p>Pasūtījumu apstrāde tiek veikta darba dienās, no 9:00 līdz 18:00. Gadījumā, ja Jums rodas kādi jautājumi, komentāri vai sūdzības, lūdzam Jūs zvanīt pa tālruni +27122400008 vai arī izmantot e-pasta adresi emischoollatvia@gmail.com.
</p>
 
  <p>Lūdzam Jūs rūpīgi iepazīties ar piegādes un apmaksas noteikumiem un kārtību. Nospiežot “OK” pogu pasūtījuma formā, Jūs dodat savu piekrišanu piegādes un apmaksas nosacījumiem.
</p>
 
  <h2> Papildus nosacījumi</h2>
 
  <p> Minimālā pasūtījuma summa 15 EUR. Veicot produkcijas pasūtījumu spēkā ir šāda atlaižu sistēma:
    <br />
   Pasūtījumu summa sākot ar 200 EUR – 10% atlaide </p>
 
    <p>
        <span style="font-family: 'Times New Roman', serif; font-size: 12pt; line-height: 115%;">Pasūtījumu nosūtīšana tiek veikta 3 darbadienu laikā pēc apmaksas saņemšanas.</span> 
    </p>
    <p>
        <span style="font-family: 'Times New Roman', serif; font-size: 12pt; line-height: 115%;">Klients var atgriezt pasūtījumu ne vairāk ka pēc 14 dienām pēc preces saņemšanas.</span>
    </p>



   <div style="clear:both;"></div>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>