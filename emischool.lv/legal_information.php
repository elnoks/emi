<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetPageProperty("description", "Школа ногтевого дизайна Екатерины Мирошниченко E.Mi проводит курсы по дизайну ногтей в Москве. Также осуществляем продажу оригинальных товаров для дизайна ногтей. Юридическая информация");
$APPLICATION->SetTitle("Juridiskā informācija");
?><div class="bx_page">
    <div class="h1-top">
        <h1>Juridiskā informācija</h1>
    </div>
    <br>
    <h2>
        Privātuma politika</h2>
    <p>
        1. Privātuma politikas un attiecību nosacījumi starp Jums un E.Mi zīmolu, kas paredz un iekļauj sevī personīgās sensitīvās informācijas apstrādi, tiek regulēti ar Krievijas Federācijas Federālā likuma Nr.152-FZ „Par personas datiem” no 2006. g. 27. jūlija prasībām, kā arī atbilstošo Latvijas Republikas normatīvo aktu prasībām.
    </p>
    <p>
        2. Privātuma politikas nosacījumi ir spēkā attiecībā uz visu personīgo sensitīvo informāciju, kuru E.Mi zīmols saņēma vai var saņemt no Jums veicot Pasūtījuma reģistrēšanu un/vai noformēšanu mājas lapā, kā arī informāciju, kas nepieciešama E.Mi saistību izpildei attiecībā uz Jūsu iegādājamo preci un/vai Jūsu pieeju personalizētiem mājas lapas servisiem.
    </p>
    <p>
        3. Ar doto nosacījumu akceptu Jūs sniedzat savu piekrišanu tam, ka mājas lapas personalizētu Servisu izmantošana nozīmē Jūsu absolūto piekrišanu Privātuma politikai un tajā paredzētiem Jūsu personīgu datu apstrādes nosacījumiem. Gadījumā, ja Jūs nepiekrītat kādam Privātuma politikas nosacījumiem, Jums uzreiz ir nepieciešams pārtraukt servisa izmantošanu un nekavējoties atstāt mājas lapu.
    </p>
    <p>
        4. Jūs akceptējat, ka Jūsu piekrišana Jūsu personīgo datu apstrādei, kuru Jūs sniedzāt E.Mi zīmolam ir attiecināma uz visām personām, kas saistīta ar E.Mi zīmolu un tā komerciālo darbību.
    </p>
    <p>
        5. Lietotāja personīgie dati, kuru ievākšanu un apstrādi veic E.Mi:
    </p>
    <p>
        5.1. Obligātie personīgie dati, kurus Jūs apzināti un brīvprātīgi nododat E.Mi rīcībā veicot reģistrēšanos un/vai Pasūtījuma noformēšanu mājas lapā, kuri pirmajām kārtam nepieciešami, lai E.Mi spētu izpildīt savas saistības attiecībā uz Jūsu veikto pasūtījumu un iegādāto preci. Pie šiem personīgajiem datiem ir pieskaitāmi: reģistrējamā lietotāja vārds un uzvārds; lietotāja tālrunis; lietotāja e-pasts; preču piegādes adrese.
    </p>
    <p>
        5.2. Neobligātie personīgie dati, kurus Jūs labprātīgi un apzināti nododat mūsu rīcībā pēc sava ieskata, piemēram, Jūsu vecums, dzimums, sociālais stāvoklis utt., kuru Jūs norādāt veicot reģistrēšanos mūsu Mājas lapā vai arī veicot turpmāko jebkura mūsu Mājas lapas personalizētā servisa izmantošanu.
    </p>
    <p>
        5.3. Depersonalizētie personīgie dati, kuri automātiski tiek saņemti Jūsu atrašanās gaitā mājas lapas lappusēs ar Jūsu datorā esošās programmatūras palīdzību: IP-adrese, cookie informācija, informācija par Jūsu tīmekļa pārlūku (vai arī citu rīku, ar kura palīdzību tiek saņemta piekļuve mājas lapai), pieejas laiks un vaicājamās lappuses adrese.
    </p>
    <p>
        6. E.Mi kompānija neveic Jūsu sniedzamo personīgo datu autentiskuma un pareizības pārbaudi un neveic to aktualitātes kontroli. Tomēr E.Mi uzskata, ka Jūs sniegsiet patiesu un aktuālu personīgo informāciju, atbildot uz reģistrācijas formā esošajiem jautājumiem. Jūs uzņematies personīgu un pilnu atbildību par patiesu vai nepatiesu ziņu sniegšanu E.Mi kompānijai.
    </p>
    <p>
        7. E.Mi ievāc un apstrādā tikai tos personīgos datus, kas nepieciešami mūsu mājas lapas personalizētu servisu lietošanai un/vai Jūsu iegādāto preču piegādes realizācijai, bet konkrētāk:
    </p>
    <p>
        7.1. Jūsu veiktā pasūtījuma pieņemšanai, apstrādei un piegādei;
    </p>
    <p>
        7.2. no Jums saņemto maksājumu apstrādei;
    </p>
    <p>
        7.3. lai informētu Jūs par Jūsu veiktā pasūtījuma stāvokli ar e-pasta informatīvo vēstuļu un mobilo izziņu palīdzību;
    </p>
    <p>
        7.4. lai uzlabotu mājas lapas un attiecīgo tiešsaistes servisu darba kvalitāti;
    </p>
    <p>
        7.5. lai sniegtu Jums efektīvu klienta atbalstu;
    </p>
    <p>
        7.6. lai sniegtu Jums personificētus mājas lapas servisu pakalpojumus;
    </p>
    <p>
        7.7. lai veiktu Jums informācijas sūtīšanu, kas ir saistīta ar Jūsu izmantotajiem personalizētiem mājas lapas servisiem;
    </p>
    <p>
        7.8. lai veiktu mājas lapas servisu ērtības un darbības uzlabošanu, kā arī izstrādātu jaunus servisus saņemot Jūsu priekšlikumus un atsauksmes par esošiem mājas lapas servisiem;
    </p>
    <p>
        7.9. lai informētu Jūs par E.Mi zīmola veiktajiem pasākumiem un akcijām;
    </p>
    <p>
        7.10. lai veiktu statistikas un cita veida pētījumus, iesaistot tajās bezpersoniskus datus.
    </p>
    <p>
        8. Attiecībā uz Jūsu personīgajiem datiem tiek saglabāts to pilnīgais privātums, izņemot gadījumus, kad Jūs brīvprātīgi sniedzat mums informāciju par sevi neierobežotā persona loka vispārējai pieejai.
    </p>
    <p>
        9. E.Mi zīmols aizsargā Jūsu personīgos datus atbilstoši šāda veida informācijas aizsardzības prasībām, kas paredzētas atbilstošos normatīvos aktos un nes atbildību par šādas informācijas drošības realizācijas metodēm.
    </p>
    <p>
        10. Lai attiecīgā veidā aizsargātu Jūsu personīgos datus, kā arī nodrošināt to atbilstošu izmantošanu un novērstu nesankcionētās un/vai nejaušās piekļuves risku to saturam, E.Mi kompānija izmanto visus nepieciešamus tehniskus un administratīvus pasākumus. Mūsu rīcība sniegtie Jūsu personīgie dati tiek uzglabāti serveros ar ierobežotu pieeju, kas atrodas apsargājamās telpās.
    </p>
    <p>
        11. E.Mi kompānija ir tiesīga nodot Jūsu personīgos datus trešo personu rīcībā šādos gadījumos:
    </p>
    <p>
        11.1. Jūs acīmredzami paudāt savu piekrišanu šādai darbībai;
    </p>
    <p>
        11.2. Jūsu personīgo datu nodošana ir nepieciešama, lai nodrošinātu Jums iespēju izmantot kādu noteiktu mājas lapas servisu, vai arī ar Līguma nosacījumu izpildes mērķi, saskaņā ar kuru Jūs esat labumu saņēmējs. Turklāt E.Mi kompānija nodrošina Jūsu personīgās informācijas privātumu, bet Jūs tiksiet informēts par šādu informācijas nodošanas aktu;
    </p>
    <p>
        11.3. Jūsu personīgo datu nodošana ir paredzēta saskaņā ar Krievijas Federācijas vai Latvijas Republikas likumdošanas prasībām normatīvo aktu noteiktās procedūras ietvaros.
    </p>
    <p>
        12. Jums ir tiesības jebkurā brīdī atjaunot Jūsu sniegto personīgo informāciju, izmantojot personīgo datu rediģēšanas funkciju.
    </p>
    <p>
        13. Jebkurā brīdī Jūs varat pieprasīt dzēst Jūsu sniegto personīgo informāciju, vēršoties E.Mi kompānijā.
    </p>
    <p>
        14. Jūs sniedzat savu pilnu un brīvprātīgu piekrišanu Jūsu personīgo datu apstrādei, nospiežot pogu „Iepazinos ar Privātuma politikas nosacījumiem”, veicot pasūtījuma noformējumu mājas lapā.
    </p>
    <p>
        15. Jūsu piekrišana personīgo datu apstrādei ir spēkā visa E.Mi kompānijas pakalpojumu lietošanas laikā.
    </p>
    <p>
        16. Visus jautājums par personīgo datu apstrādi Jūs varat adresēt E.Mi kompānijas pārstāvjiem.
    </p>



    <h2>Interneta vietnes www.emischool.lv vispārīgie lietošanas noteikumi.</h2>

    <p>
        1. Pirms sākt lietot SIA „NPCRIZ LATVIA” (reģistrācijas Nr.40103607799, adrese – Ilūkstes ielā 50, Rīgā, LV- 1082) interneta vietni www.emischool.lv, lūdzam rūpīgi izlasīt šos interneta vietnes www.emischool.lv lietošanas noteikumus (turpmāk – noteikumi).
    </p>
    <p>
        2. Ar interneta vietni www.emischool.lv tiek saprasta SIA „NPCRIZ LATVIA” izveidotā interneta vietne, kuras interneta adrese ir www.emischool.lv, ar visu tajā esošo informāciju un materiāliem.
    </p>
    <p>
        3. Apmeklējot interneta vietni www.emischool.lv vai izmantojot tajā izvietoto informāciju par precēm un pakalpojumu sniegšanu, Jūs personiski vai Jūsu pārstāvētā persona, ja darbojaties tās vārdā, piekrīt šiem noteikumiem.
    </p>
    <p>
        4. Gadījumā, ja nepiekrītat šiem noteikumiem, lūdzam neapmeklēt un nelietot interneta vietni www.emischool.lv, kā arī neizmantot tajā piedāvātos pakalpojumus un ietverto informāciju.
    </p>
    <p>
        5. Informējam, ka SIA „NPCRIZ LATVIA” interneta vietnē www.emischool.lv ievadītās informācijas (tai skaitā fizisko personu datu) apstrādes mērķis ir tajā norādīto pakalpojumu sniegšana un palīdzības sniegšana interneta vietnes www.emischool.lv lietošanas laikā.
    </p>
    <p>
        6. SIA „NPCRIZ LATVIA” ir tiesības jebkurā brīdī vienpusēji mainīt interneta vietnes www.emischool.lv saturu un lietošanas noteikumus. Šādas izmaiņas stājas spēkā pēc to publicēšanas interneta vietnē www.emischool.lv.
    </p>
    <p>
        7. Ar brīdi, kad sākat lietot interneta vietni www.emischool.lv vai minētajā vietnē veicat jebkādas aktivitātes, tiks uzskatāms, ka esat iepazinies ar lietošanas brīdī spēkā esošajiem noteikumiem un ievērosiet tos. Katram interneta vietnes lietotājam ir pienākums regulāri pārlasīt noteikumus, lai savlaicīgi būtu lietas kursā par tajos veiktajām izmaiņām. Ja nepiekrītat noteikumiem, tad aizliegts lietot interneta vietni.
    </p>
    <p>
        8. Interneta vietnes www.emischool.lv lietotājiem ir tiesības izmantot tajā piedāvātās iespējas un pakalpojumus par cenu, kāda ir spēkā attiecīgā pakalpojuma piedāvāšanas brīdī. SIA „NPCRIZ LATVIA” ir tiesības jebkurā brīdī pēc saviem ieskaitiem mainīt interneta vietnē www.emischool.lv norādītās cenas un/vai izveidot jaunus maksas pakalpojumus.
    </p>
    <p>
        9. Lai kļūtu par interneta vietnes www.emischool.lv reģistrēto lietotāju, ir jāievēro interneta vietnē www.emischool.lv noteiktā reģistrēšanās kārtība.
    </p>
    <p>
        10. Katram interneta vietnē www.emischool.lv reģistrētajam lietotājam ir pienākums neizpaust citām personām savus piekļuves datus. Ja interneta vietnē www.emischool.lv tiek veiktas darbības ar reģistrēta lietotāja profilu (t.sk. tiek pirktas preces un/vai pakalpojumi), izmantojot pareizu lietotājvārdu un paroli, tad tiek uzskatīts, ka darbības attiecīgajā profilā ir veicis pats reģistrētais lietotājs.
    </p>
    <p>
        11. Interneta vietnes lietotājs piekrīt saņemt informāciju no interneta vietnes www.emischool.lv par dažāda veida aktualitātēm.
    </p>
    <p>
        12. SIA „NPCRIZ LATVIA” neatbild par jebkāda veida izdevumiem un zaudējumiem, kas radušies interneta vietnes www.emischool.lv lietošanas laikā.
    </p>
    <p>
        13. Gadījumos, ja ir neskaidrības par SIA „NPCRIZ LATVIA” interneta vietnē www.emischool.lv ievietoto dokumentu formu aizpildīšanu, lūgums sazināties ar norādīto interneta vietnes www.emischool.lv kontaktpersonu.
    </p>


    <h2>Noteikumi par atteikuma tiesību izmantošanu</h2>



    <h3>I. Atteikuma tiesības:</h3>

    <p>
        1. Jums ir tiesības 14 (četrpadsmit) dienu laikā atteikties no šā līguma, neminot iemeslu.
    </p>
    <p>
        2. Atteikuma tiesību izmantošana beigsies pēc 14 dienām, sākot no dienas, kad jūs esat ieguvis vai trešā persona, kas nav pārvadātājs un ko jūs esat norādījis, ir ieguvusi preces valdījumā.
    </p>
    <p>
        3. Lai izmantotu atteikuma tiesības, jums ir jāinformē mūs SIA “NPCRIZ LATVIA”, juridiskā adrese Ilūkstes iela 50, Rīga, LV-1082, tālruņa numuri: 22400008, e-pasta adrese: emischoollatvia@gmail.com par lēmumu atteikties no šā līguma (skatīt SIA „NPCRIZ LATVIA” interneta vietnē www.emischool.lv ievietotās veidlapas par atteikuma tiesību izmantošanu paraugu).
    </p>
    <p>
        4. Lai atteikuma tiesību termiņš būtu ievērots, pietiek, ja savu paziņojumu par atteikumu tiesību izmantošanu nosūtīsiet pirms atteikuma tiesību termiņa beigām.
    </p>


    <h3>II. Atteikuma radītās sekas:</h3>



    <p>
        5. Ja jūs atteiksieties no šā līguma, mēs jums atmaksāsim visus no jums saņemtos maksājumus, tostarp piegādes izmaksas (izņemot papildu izmaksas, kas radušās tādēļ, ka jūs esat izvēlējies piegādes veidu, kas nav mūsu piedāvātais vislētākais standarta piegādes veids), ne vēlāk kā 14 dienu laikā no dienas, kad mēs tikām informēti par jūsu lēmumu atteikties no šā līguma. Jebkurā gadījumā saistībā ar šādu atmaksāšanu no jums netiks iekasēta nekāda maksa.
    </p>
    <p>
        6. Jums preces jānodod www.emischool.lv birojā Tallinas ielā 59, Rīgā, bez nepamatotas kavēšanās un jebkurā gadījumā ne vēlāk, kā 14 dienu laikā no dienas, kad jūs mums paziņojāt savu lēmumu atteikties no šā līguma. Termiņš būs ievērots, ja jūs preces nosūtīsiet atpakaļ uz biroja adresi Tallinas ielā 59, Rīgā, pirms 14 dienu termiņa beigām. Lūdzu ņemt vērā, ka SIA “NPCRIZ LATVIA” par precēm, kas sūtītas pa pastu, to pieņemšanas brīdī ir tiesības fiksēt iespējamos preces vizuālos bojājumus, kā arī komplektācijas saturu. Gadījumos, kad nosūtot preces uz SIA “NPCRIZ LATVIA” biroju, izmantojot kurjerpastu, SIA “NPCRIZ LATVIA” konstatēs preces bojājumus vai preces neatbilstību vizuālajam stāvoklim un komplektācijai uz preces saņemšanas brīdi, Jūs tiksiet informēts un Jums tiks nosūtīta konstatācijas akta kopija. Rekomendējam preces nodot klātienē, Rīgā, Tallinas ielā 59.
    </p>
    <p>
        7. Jums būs jāsedz ar preču atpakaļ atdošanu saistītās tiešās izmaksas. Maksimālās izmaksas varētu būt aptuveni 50 EUR.
    </p>
    <p>
        8. Jūs esat atbildīgs tikai par preču vērtības samazināšanos, ja preces izmantotas nevis tāpēc, lai konstatētu šo preču raksturu, īpašības un darbību, bet citos nolūkos.
    </p>
    <p>
        9. Atteikuma tiesību izmantošanas termiņā jums ir tiesības preci lietot tiktāl, cik tas nepieciešams preces rakstura, īpašību un darbības pārbaudei. (tikpat lielā mērā, cik to varētu izdarīt pirms preces iegādes parastajā veikalā, piemēram, portatīvajiem datoriem iepazīties vizuāli ar datora izskatu, izmēru, taču neveicot nekādas manipulācijas, tai skaitā aktivizējot uz datora preinstalētu operētājsistēmu; mobilajiem telefoniem - nenoplēšot aizsargplēves, ieslēgt, neievietojot SIM karti un neveicot zvanus, nesinhronizējot datus utt.). Izmantojot atteikuma tiesības, Jūs esat atbildīgs par preces lietošanu, kas pārsniedz preces rakstura, īpašību un darbības pārbaudes nolūkam paredzēto. Jūs esat atbildīgs par tādu preces lietošanu atteikuma tiesību izmantošanas termiņā, kas nav savienojama ar labas ticības principu, kā arī par preces vērtības, kvalitātes un drošuma samazināšanos.
    </p>
    <p>
        SIA „NPCRIZ LATVIA” lūdz patērētājus preci lietot tiktāl, cik tas nepieciešams preces rakstura, īpašību un darbības pārbaudei, piemēram, preci lietot tik daudz, cik to varētu izdarīt pirms preces iegādes parastajā veikalā, piemēram, portatīvajiem datoriem iepazīties vizuāli ar datora izskatu, izmēru, bet lūgums neveikt nekādas manipulācijas, tai skaitā aktivizēt uz datora preinstalētu operētājsistēmu; mobilajiem telefoniem - nenoplēst aizsargplēves, ieslēgt preci, neievietojot SIM karti un neveikt zvanus, nesinhronizēt datus utt.
    </p>
    <p>
        10. Jūs esat atbildīgs par preces kvalitātes un drošuma saglabāšanu atteikuma tiesību realizēšanas termiņā.
    </p>
    <p>
        11. Izņemot šo noteikumu 12. punktā norādītos gadījumus, Jūs varat izmantot atteikuma tiesības un vienpusēji atkāpties no līguma 14 dienu laikā, sedzot:
    </p>
    <p>
        11.1. izdevumus par preces piegādi, kas nav SIA „NPCRIZ LATVIA” piedāvātais vislētākais standarta piegādes veids;
    </p>
    <p>
        11.2. preces atpakaļatdošanas tiešās izmaksas, izņemot gadījumus, kad SIA „NPCRIZ LATVIA” ir piekritis segt šīs izmaksas vai nav informējis Jūs, ka Jums ir jāsedz izmaksas;
    </p>
    <p>
        11.3. preces vērtības samazināšanos, ja prece izmantota citā nolūkā, nevis preces rakstura, īpašību un darbības noskaidrošanai, ko apliecina preces ražotāja autorizētajā servisā veiktā diagnostika. Jūs neesat atbildīgs par preces vērtības samazināšanos, ja SIA „NPCRIZ LATVIA” nebūs Jūs informējis par atteikuma tiesībām patērētāju tiesību aizsardzību regulējošos normatīvajos aktos noteiktajā kārtībā;
    </p>
    <h3>III. Patērētājs nevar izmantot atteikuma tiesības:</h3>

        <p>
            12. Ministru kabineta 2014.gada 20.maija noteikumu Nr.255 “Noteikumi par distances līgumu” 22.punktā noteiktajos gadījumos, tajā skaitā, ja:
        </p>
        <p>
            preces cena ir atkarīga no finanšu tirgus svārstībām, kuras pārdevējs nevar kontrolēt un kuras var rasties atteikumu tiesību termiņā;
        </p>
        <p>
            preces tiek izgatavotas pēc patērētāja norādījumiem vai ir nepārprotami personalizētas;
        </p>
        <p>
            prece ātri bojājas vai tai drīz beidzas derīguma termiņš;
        </p>
        <p>
            patērētājs ir atvēris iepakojumu precei, kuru veselības un higiēnas apsvērumu dēļ nevar atdot atpakaļ, tādām precēm, kā el.zobu birste, depilators, el.bārdas skuveklis, bārdas/matu trimmeris, el.matu griežamā mašīna, ausīs ieliekamās audio austiņas un citas tamlīdzīga rakstura preces, ja ir atvērts šo preču iepakojums un/vai ir bojāta drošības plombe;
        </p>
        <p>
            prece, tās īpašību dēļ pēc piegādes neatgriezeniski sajauksies ar citām lietām;
        </p>
        <p>
            patērētājs ir pieprasījis pārdevējam vai pakalpojuma sniedzējam ierasties un veikt steidzamus remontdarbus vai tehniskās apkopes darbus. Ja pārdevējs vai pakalpojuma sniedzējs, ierodoties pie patērētāja, sniedz papildu pakalpojumu vai piegādā preces, kas nav nepieciešamās rezerves daļas, lai veiktu remontdarbus vai tehniskās apkopes darbus, atteikuma tiesības ir piemērojamas minētajiem papildu pakalpojumiem vai precēm;
        </p>
        <p>
            patērētājs ir atvēris audioierakstu vai videoierakstu, vai datorprogrammu iepakojumu;
        </p>
        <p>
            līgums noslēgts par digitālā satura piegādi, kas netiek piegādāts pastāvīgā datu nesējā, ja digitālā satura piegāde ir uzsākta ar patērētāja iepriekš skaidri paustu piekrišanu un apliecinājumu par atteikuma tiesību zaudēšanu;
        </p>
        <p>
            13. Pirkuma līguma kopijas interneta veikals www.emischool.lv neuzglabā.
        </p>
        <p>
            14. Lai izlabotu pasūtījuma laikā ieviesušās ievadkļūdas, lūdzu, sazinieties ar mums pa telefonu (22400008) vai sūtiet e-pastu: emischoollatvia@gmail.com.
        </p>
        <p>
            15. Pirkuma pieteikumu Jūs varat aizpildīt latviešu vai krievu valodā.
        </p>
        <p>
            16. SIA „ NPCRIZ LATVIA” interneta vietnē www.emischool.lv ir ievietotas veidlapas par atteikuma tiesību izmantošanu. Lejuplādēt
        </p>
        <p>
            17. Interneta veikalā www.emischool.lv apskatāmo preču attēli var nedaudz atšķirties no piedāvātās preces. Lai precizētu vai konkretizētu preces raksturojumu vai novērstu ar tās pasūtīšanu saistītās neskaidrības, lūdzam sazināties ar www.emischool.lv pa e-pastu: emischoollatvia@gmail.com, tel.22400008, lai izvairītos no nepatīkamiem pārpratumiem.
        </p>
        <p>
            18. SIA „NPCRIZ LATVIA” informē, ka konstatējot atpakaļ atdotās preces vērtības zudumu, vērsīsies tiesā ar prasības pieteikumu par SIA „NPCRIZ LATVIA” radīto zaudējumu segšanu, kas radušies patērētāja rīcības rezultātā.
        </p>
        <p>
            19. Sūdzību par preču pieejamību vai kvalitāti lūdzam iesniegt elektroniski, nosūtot uz šādu elektroniskā pasta adresi – emischoollatvia@gmail.com vai lūdzam sūdzību iesniegt rakstveidā, nosūtot uz šādu adresi: Rīga, Tallinas iela 59, LV-1009
        </p>
        <p>
            Sūdzība tiks izskatīta 30dienu laikā no sūdzības saņemšanas dienas, atbildi nosūtot uz sūdzībā norādīto saziņas adresi.
        </p>
        <p>
            Gadījumā, ja iesniegtā sūdzība tiks atzīta par nepamatotu un Jūs sūdzības atzīšanai par nepamatotu nepiekritīsiet, Jums ir tiesības izmantot normatīvajos aktos noteiktā alternatīvo strīdu risināšanas iespējas, iesniedzot preces pārdevējam rakstveida iesniegumu par ārpustiesas strīda risināšanu, norādot:
        </p>


        <p>
            1. Vārdu, uzvārdu, kontaktinformāciju;
        </p>
        <p>
            2. Iesnieguma iesniegšanas datumu
        </p>
        <p>
            3. Strīda būtību, prasījumus un to pamatojumu.
        </p>


        <h2>
            Juridiskā informācija</h2>
        <h3>
            Autortiesības</h3>
        <p>
            Visas tiesības aizsargātas. Autortiesības pieder E.Mi (Jekaterina Mirošničenko) kompānijai vai arī E.Mi kompānijas partneriem. Visi teksti, foto, bildes, skaņas un video faili, kā arī animācijas faili un to kompozīcijas noformējums ir aizsargāti ar autortiesībām un spēkā esošo likumdošanu. Netiek pieļauta nekāda šo failu kopēšana, izmaiņas, izplatīšana vai izmantošana citās mājas lapās ar komerciāliem vai citiem nolūkiem bez iepriekšējās saskaņošanas ar E.Mi kompāniju un tās partneriem, kuri ir šo materiālu autortiesību turētāji.
        </p>
        <h3>
            Preču zīmes</h3>
        <p>
            Preču zīmes, preču zīmols, logotipu un citi individuālās piederības līdzekļi, kas izvietoti dotajā mājas lapā pieder E.Mi (Jekaterina Mirošničenko) kompānijai vai arī E.Mi kompānijas partneriem, kā arī trešajām personām. Mājas lapā publicēta informācija nesniedz nekādu licences tiesību jebkuru preču zīmju izmantošanai bez iepriekšējās rakstiskas īpašnieka atļaujas.
        </p>
            <h3>
                Informācija par uzņēmumu</h3>
            <p>
                <b>Reģistrēts nosaukums</b> SIA "NPCRIZ LATVIA"<br>
                <b>Reģistrācijas numurs, datums</b> LV 40103607799, 20.11.2012<br>
                <b>PVN numurs</b> LV40103607799 (ar 14.02.2013) Eiropas PVN reģistrs<br>
                <b>Juridiskā adrese</b> Rīga, Ilūkstes iela 50 - 6, LV-1082<br>
                <b>Biroja adrese:</b> Tallinas iela 59, Rīga, LV-1009<br>
                <b>E-pasta adrese:</b> emischoollatvia@gmail.com<br>
                <b>Bankas revizīti:</b> AS Swedbank, kods: HABALV22<br>
                <b>Konta Nr.:</b> LV63HABA0551042002890
            </p>
            <h3></h3>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>