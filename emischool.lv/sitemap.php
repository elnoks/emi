<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetPageProperty("description", "Школа ногтевого дизайна Екатерины Мирошниченко E.Mi проводит курсы по дизайну ногтей в Москве. Также осуществляем продажу оригинальных товаров для дизайна ногтей. Карта сайта");

$APPLICATION->SetTitle("Lapas karte");
?>
<div class="container">
<?
$APPLICATION->IncludeComponent(
	"bitrix:main.map", 
	".default", 
	array(
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000",
		"SET_TITLE" => "Y",
		"LEVEL" => "0",
		"COL_NUM" => "1",
		"SHOW_DESCRIPTION" => "Y"
	),
	false
);
?>
	
</div><!-- /.container --><? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>