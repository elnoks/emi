<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetPageProperty("description", "Школа ногтевого дизайна Екатерины Мирошниченко E.Mi проводит курсы по дизайну ногтей в Москве. Также осуществляем продажу оригинальных товаров для дизайна ногтей. Как открыть представительство Школы");
$APPLICATION->SetTitle("Kā atvērt Skolas pārstāvniecību?");
?><div class="bx_page">
	<p style="text-align: center;">
		<img width="800" alt="765.jpg" src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" height="105" title="765.jpg" border="0"><b> <br>
 </b>
	</p>
	<p>
		<b> <br>
 </b>
	</p>
	<p>
 <b>Sadarbība ar Jekaterinas Mirošničenko Nagu dizaina skolu – tā ir iespēja saņemt gatavo un funkcionējošo risinājumu, kas tiešām darbojās un nes Jums stabilu peļņu. Sadarbībā ar mums Jūs saņemsiet pilnu tehnoloģiju komplektu: no nagu dizaina noslēpumiem līdz pat uzņēmuma pārvaldīšanas un reklamēšanas progresīvajām tehnikām. Jekaterinas Mirošničenko izstrādātais nagu dizains – ir vienmēr aktuālie atpazīstamie un klientu pieprasītie tēli, kas atbilst pēdējām modes tendencēm.</b>
	</p>
	<p>
 <b>Kļūstiet par mūsu reģionāliem pārstāvjiem un mēs iemācīsim Jums kā kļūt tik pat radošiem un veiksmīgiem!</b> Šodien E.Mi zīmola Nagu dizaina skola ir plaši pazīstama ne tikai Krievijā, bet arī visā pasaulē. Tai ir prestižie starptautisko konkursu apbalvojumi, semināri Vācijā un Itālijā, starptautiskā biroja atvēršana Eiropā. <br>
		 Jūs varat iegādāties ekskluzīvas tiesības realizēt savā reģionā interesantu un augstākās rentabilitātes pakāpes biznesu nagu servisa speciālistu kvalifikācijas celšanai ar „Jekaterinas Mirošničenko Nagu dizaina skolas” zīmolu.
	</p>
	<p>
 <b>Pievienojieties tiem, kas vienmēr ir lietas kursā par jaunākām modes tendencēm un trendiem!</b> E.Mi. Nagu dizaina skola ne tikai radīja atpazīstamas un unikālas nagu dizaina tehnoloģijas, bet arī izstrādāja ne mazāk unikālu mācību tehniku, lai speciālisti varētu apgūt šo dizainu. <br>
 <b>Tagad, pateicoties mūsu darbam, radošums un skaistums ir pieejami visiem par prieku!</b> <br>
 <b>E.Mi Nagu dizaina skolas priekšrocības:</b>
	</p>
	<ul>
		<li>Mācību pakalpojumu pieprasījums ir apliecināms ar to, ka mūsu mācību centrā dažādās mācību programmās ik gadu tiek apmācīti vairāk kā 500 meistaru no Krievijas un citām valstīm.</li>
		<li>Apmācības tiek veiktas ar unikālu nagu dizaina veidošanas «<a href="/catalog/">E.Mi</a>» zīmola produkciju.</li>
		<li>Kompānijas starptautiskais raksturs. Eiropas birojs (Prāgā).</li>
		<li>Mūsu mācību programmu pamatā – unikālās metodikas, kas izstrādāja un patentēja Jekaterina Mirošničenko, nagu dizaina pasaules čempione OMC versijā (Parīze, 2011. g.).</li>
	</ul>
	<p>
	</p>
	<p>
 <b>Mēs nodrošinām mūsu Pārstāvniecību ar visu nepieciešamo veiksmīgai E.Mi. Nagu dizaina skolas mācību daļas funkcionēšanai.<br>
		 Atvērot E.Mi Nagu dizaina skolas pārstāvniecību Jūs saņemsiet:</b>
	</p>
	<ol>
		<li><b>Ekskluzīvas tiesības </b>tiesības strādāt ar Jekaterinas Mirošničenko Nagu dizaina skolas zīmola, sniedzot izglītojošus un mācību pakalpojumus līgumā norādītajā teritorijā. Vienā reģionā (apgabalā, novadā utt.) var darboties tikai viena E.Mi Pārstāvniecība.</li>
		<li><b>Gatavās mācību programmas</b>, kuras izstrādāja personīgi Jekaterina Mirošničenko. Reģionālais mācību centrs var ietaupīt kolosālu spēku un enerģijas daudzumu, neveltot visu savu laiku mācību materiālu un plānu izstrādei. Tas ļauj novirzīt visus spēkus klausītāju meklēšanai, kas savukārt ļaus ātri atgūt Pārstāvniecības atvēršanā ieguldītos līdzekļus.</li>
		<li><b>Iekļaušanu vienotā sertifikācijas sistēmā. </b>Katra Pārstāvniecība saņem tiesības izsniegt saviem klausītājiem firmas sertifikātus ar E.Mi Nagu dizaina skolas zīmola logo hologrammu.</li>
		<li><b>Tiesības uz ekskluzīvajām atlaidēm <a href="/catalog/">E.Mi</a></b> zīmola produkcijas iegādei.</li>
		<li><b>Reklāmas un informācijas atbalstu.</b> E.Mi skola novieto informāciju par visām savām pārstāvniecībām un to reklāmu vadošajos profesionālos skaistuma nozares žurnālos, kā arī savā reklāmas katalogā un oficiālajā mājas lapā. Mēs palīdzam realizēt reģionālās izstādes un nagu skaistuma nozares speciālistu konferences, nododam Jūsu rīcība sagatavotus reklāmas maketus to publicēšanai periodiskos izdevumos.</li>
	</ol>
	<p>
	</p>
	<p>
 <b>Sadarbības specifika Oficiālā zīmola pārstāvja un Oficiālā E.Mi zīmola produkcijas izplatītāja statusā.</b> <br>
		 Oficiālam Jekaterinas Mirošničenko Nagu dizaina skolas pārstāvim ir virkne zināmu priekšrocību E.Mi zīmola produkcijas izplatīšanai sadarbības līgumā noteiktajā teritorijā. Šādā gadījumā visām parastām Oficiālā E.Mi zīmola pārstāvja līguma prasībām klāt nāk prasības un nosacījumi, uz kādiem notiek sadarbība ar produkcijas izplatītājiem un obligātos produkcijas iepirkumu apjomus ar atbilstošām atlaidēm E.Mi produkcijas izplatītājiem.
	</p>
	<p>
 <b>Prasības Jekaterinas Mirošničenko Nagu dizaina skolas Pārstāvniecības atvēršanai:</b>
	</p>
	<ol>
		<li>Talantīgais nagu dizaina instruktors, kas spēs pilnībā nokopēt Jekaterinas Mirošničenko tehniku. Kandidātam nepieciešams iziet sākotnējo apmācību kā E.Mi Skolas studentam un tikai gadījumā, ja viņa spējas un profesionālās iemaņas apmierina Jekaterinas Mirošničenko noteiktās prasības, kandidāts tiek pielaists īpašam instruktora sagatavošanas kursam.</li>
		<li>Atsevišķa telpa mācību centra atvēršanai, kuras platība nav mazāka par 16 m2. Mācību centra telpas nepieciešams apzīmēt izvietojot J. Mirošničenko Nagu dizaina skolas firmas izkārtes.</li>
		<li>Juridiskās personas reģistrēšana, kuras vārdā iespējams veikt mācību pakalpojumu sniegšanu.</li>
		<li>Apgrozījuma līdzekļi, kas paredzēti E.Mi produkcijas starta komplekta iepirkumam, kā arī mācību centra ierīkošanai un Instruktora apmācības apmaksai.</li>
		<li>Pārstāvju gatavība veikt darbus potenciālu mācekļu piesaistei ar izstrādātu reklāmas tehnoloģiju palīdzību.</li>
	</ol>
	<p>
	</p>
	<p>
 <b>Cienījamie Meistari! Ja Jūs esat apņēmības pilni atvērt mūsu Skolas Pārstāvniecību savā reģionā, Jums ir nepieciešams:</b>
	</p>
	<ol>
		<li>Uzzināt, vai Jūsu reģionā ir mūsu Pārstāvniecība. To iespējams izdarīt mūsu mājas lapas sadaļā „Kontakti”, vai arī piezvanot pa telefonu * * * * **. Gadījumā, ja Jūsu reģionā jau ir mūsu Skolas pārstāvniecība, diemžēl, mēs būsim spiesti Jums atteikt.</li>
		<li>Iesūtīt mums fotogrāfijas ar Jekaterinas Mirošničenko autores nagu dizaina paraugu (piemēram, „Zemenes”, „Avenes”, „Auduma krokas”, „Ziedi ar ierāmējumu” utt.). Tas ir nepieciešams, lai noteiktu vai Jūsu kandidatūra atbilst Instruktora amatam, jo Jūsu darba rokrakstam ir jābūt atbilstošām Jekaterinas Mirošničenko prasībām un Jums ir jābūt spējīgiem precīzi kopēt viņas dizainu un tehnikas.</li>
		<li><b>Uzzināt Instruktora apmācību cenu iespējams, piezvanot mums pa telefonu * * ** * * * *.</b> Instruktora apmācības notiek divos posmos: 1. posmā topošais Instruktors iziet mācekļa kursu, kuru vēlāk pasniegs pats, bet pēc tam 2. posmā – īpašus instruktoru kursus. Apmācības notiek tikai mūsu galvenajā Mācību centrā. Instruktora apmācības cena ir atkarīga no kursu skaita.</li>
		<li><b>Uzzināt par E.Mi zīmola produkcijas kvartāla iepirkumiem iespējams pa tālruni * * * * *.</b> Veicamo iepirkumu apjoms tiek nostiprināts līgumā.</li>
		<li>Būt gataviem veikt regulāru kvalifikācijas paaugstināšanu. Instruktora pienākumu pildīšanas līgums tiek noslēgts uz 1 gada termiņu. Ik gadu instruktors personīgi apliecina savu kvalifikāciju un spējas mūsu Galvenajā mācību centrā, kam seko atkārtotā līguma noslēgšana uz vēl vienu gadu utt.</li>
	</ol>
	<p>
	</p>
	<p>
 <b>Lai saņemtu papildus informāciju par Jekaterinas Mirošničenko Nagu dizaina skolas pārstāvniecības atvēršanu Jūsu varat vērsties mūsu galvenajā birojā.</b>
	</p>
 <br>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>