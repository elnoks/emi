<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetPageProperty("description", "Школа ногтевого дизайна Екатерины Мирошниченко E.Mi проводит курсы по дизайну ногтей в Москве. Также осуществляем продажу оригинальных товаров для дизайна ногтей. Вопрос-ответ");
$APPLICATION->SetTitle("Jautājumi un atbildes");
?> 
<div class="bx_page"> 
<div class="h1-top">
		<h1>Jautājumi un atbildes</h1>
    </div>
 <br>
  <p> <span class="bold">Kā kļūt par oficiālu E.Mi zīmola un Jekaterinas Mirošničenko Nagu dizaina skolas pārstāvi?</span> 
    <br />
   E.Mi kompānija piedāvā Jums jau gatavus risinājums strauji attīstoša un aizraujoša biznesa radīšanai. Jūs varat nodibināt oficiālo Jekaterinas Mirošničenko Nagu dizaina skolas pārstāvniecību savā pilsētā, kļūt par E.Mi zīmola produkcijas oficiālo izplatītāju vai vairumtirdzniecības pārstāvi. Detalizētāku informāciju par šo iespēju Jūs varat aplūkot sadaļā “Kā atvērt E.Mi Skolas pārstāvniecību”.</a></p>
 
  <p> <span class="bold">Kā uzzināt, vai Nagu dizaina skola funkcionē arī manā pilsētā?</span> 
    <br />
   Atrast Jekaterinas Mirošničenko Nagu dizaina skolu Jūsu pilsētā iespējams sadaļā <a href="/contacts/" >&quot;Kontakti&quot;</a>. </p>
 
  <p> <span class="bold">Kur iespējams iegādāties E.Mi produkciju?</span> 
    <br />
   Iegādāties E.Mi zīmola piedāvāto produkciju iespējams mūsu mājas lapā, sadaļā “Produkcija” (detalizētāku informāciju par to, kādā kārtībā iespējams izdarīt pasūtījumu var aplūkot sadaļā “Kā pasūtīt?”), vai ar? E.Mi zīmola oficiālajam izplatītājam Jūsu pilsētā. Precizēt adresi un kontakta tālruņus iespējams sadaļā <a href="/contacts/" >&quot;Kontakti&quot;</a>. </p>
 
  <p> <span class="bold">Vai iespējams apgūt bāziskās specialitātes (manikīra vai pedikīra meistara profesijas, kā arī nagu pieaudzēšanas veidotāja un nagu stilista amatus) Jekaterinas Mirošničenko Nagu dizaina skolā?</span> 
    <br />
   Jekaterinas Mirošničenko Nagu dizaina skolā Jums ir iespēja apgūt jaunas profesionālas iemaņas tādos virzienos kā “Nagu modelēšana” un “Manikīrs/Pedikīrs”. Detalizētāku informāciju iespējams iegūt sadaļā <a href="/courses/" >&quot;Kursi un apmācības&quot;</a>.  </p>
 
  <p> <span class="bold">Vai iespējams apgūt kādu no apmācības kursiem, kurus piedāvā Jekaterinas Mirošničenko Nagu dizaina skolā gadījumā, ja nav nekādas iepriekšējās pieredzes nagu kopšanas meistara darbā?</span> 
    <br />
   Nagu dizaina apmācības tiek uzskatītas par kvalifikācijas paaugstināšanas un profesionālās kompetences uzlabošanas kursiem, tāpēc mēs vispirms iesakām iziet apmācības bāziskās specialitātēs (tādās kā manikīra meistars, pedikīra meistars un speciālists nagu pieaudzēšanā un modelēšanā). Šādā gadījumā Jums būs vieglāk apgūt nagu dizaina un pieaudzēšanas noslēpumus.</p>
 
  <p> <span class="bold">Sākot ar kādu kursu ieteicams uzsākt apmācības?</span> 
    <br />
   Mēs iesakām uzsākt apmācības ar tādiem bāziskiem kursiem kā “Nagu mākslinieciskā apgleznošana. Līniju ābece” un “E.Mi nagu dizaina tehnoloģijas”. Šajos kursos Jūs uzzināsiet galvenos pamatus darbam ar dažādu krāsu nagu gēliem un attīstīsiet savas praktiskās iemaņas. Pēc tam Jums būs vieglāk un vienkāršāk iziet apmācības otrās un trešās sarežģītības pakāp es līmeņa kursos, tādos kā “MIX-Design: TEXTONE, Rāpuļi, Akmeņi”, “Neilokrusts”, “Mākslinieciskais aksesuārs” un “Stilizētā  nagu apgleznošana: ziedi, augļi, garderobe”, “Žostovas gleznošanas stils” un “Ķīnas gleznošanas stils”.</p>
 
  <p> <span class="bold">Kur iespējams atrasts kursu un nodarbību sarakstu Jekaterinas Mirošničenko Nagu dizaina skolā, kas atrodas manā pilsētā?</span> 
    <br />
   Detalizētu kursu un nodarbību sarakstu Jūsu pilsētā iespējams sameklēt sadaļā <a href="/courses/#schedule" >&quot;Kursu un nodarbību saraksts&quot;</a>. </p>
 
  <p> <span class="bold">Sākot ar kādu vecumu iespējams un ieteicams apgūt kursu un nodarbību saturu Jekaterinas Mirošničenko Nagu dizaina skolā?</span> 
    <br />
   Jekaterinas Mirošničenko Nagu dizaina skolā apmācībām un kursiem pieņem mācekļus sākot ar 15 gadu vecumu.</p>
 
  <p> <span class="bold">Vai drīkst uzklāt E.Mi zīmola gēla krāsas uz citu zīmolu gēla nagu laku?</span> 
    <br />
   E.Mi zīmola gēla krāsas iespējams uzklāt uz gēla laku, tomēr tajā pat laikā svarīgi atcerēties par to, ka lielākā daļa gēla laku tiek noņemta ar izmērcēšanu dažādos speciālos laka noņemšanas šķidrumos, bet E.Mi gēla krāsas tiek novīlētas ar nagu bafu. Tāpēc mēs iesakām uzklāt jaunu dizainu vai ornamentu uz nelielu nagu virsmas platības daļu, kas pārklāta ar gēla laku. Piemēram, tādu dizainu kā “Samtainās smilts un Šķidrie akmeņi” vai arī “Apjomīgā vintāža”.</p>
 
  <p> <span class="bold">Vai iespējams uzklāt E.Mi gēla krāsas uz naturālo nagu virsmu?</span> 
    <br />
   Pirms veikt E.Mi gēla krāsas uzklāšanu, nepieciešams veikt naga plāksnes pārklāšanu ar mākslīgu materiālu: gēlu, akrilu vai gēla laku. Kategoriski aizliegts uzklāt E.Mi gēla krāsu uz nagu plāksnes dabīgu virsmu.</p>
 
  <p> <span class="bold">Ar ko atšķiras E.Mi gēla krāsas no EMPASTA?</span> 
    <br />
   Galvenās atšķirības slēpjas tās konsistencē, kā arī pietiekama (vai arī nepietiekama) lipīguma līmeņa. E.Mi zīmola EMPASTA sastāvs ir biezākas konsistences, pateicoties kam krāsas līnijas vilciena aprises ilgāk saglabā savas kontūras, bet EMPASTA konsistence ļauj veidus apjomīgus un izteikti pamanāmus nagu dizainus. Turklāt, EMPASTA nav kāda papildus lipīguma, tāpēc to iespējams uzklāt virs Finish Gel aizsardzības gēla, gan arī zem tā. E.Mi EMPASTA izžūšanas laiks ir tikai 2-5 sekundes, kas ļauj ātri izpildīt dizaina veidošanu ar lielu elementu skaitu. E.Mi zīmola gēla krāsām ir daudz šķidrāka konsistence un tie ideāli der zīmējumu gleznošanai uz nagu virsmas. </p>
 
  <p> <span class="bold">Ar ko atšķiras “Šķidro akmeņu” polimēri no “Šķidro akmeņu” gēla?</span> 
    <br />
   “Šķidro akmeņu” gēls ir palīgmateriāls nagu dizaina veidošanai “Šķidro akmeņu” stilā. Tie ļauj veidot dārgakmeņu imitāciju uz jebkuru izmēru nagu virsmas viena slāņa biezumā bez nepieciešamības pārklāt to ar aizsardzības gēlu, kas ļauj samazināt dizaina veidošanai nepieciešamo laiku. “Šķidro akmeņu” gēlam nav krāsas un to nedrīkst sajaukt ar krāsu pigmentiem. “Šķidro akmeņu” polimērs paredzēts krāsaino dārgakmeņu imitācijas veidošanai, jo to iespējams sajaukt ar pigmentiem un to nepieciešams pārklāt ar aizsargājoša gēla kārtiņu.</p>
 
  <p> <span class="bold">Kā izmantot EMPASTA gēla krāsu “Ažūra sudrabs”, kas paredzēta ietvara veidošanai?</span> 
    <br />
   Zīmola E.Mi EMPASTA “Ažūra sudrabs” krāsa satur savā sastāvā caurspīdīgu frakciju, tāpēc izspiediet nelielu krāsas daudzumu uz krāsu paletes un rūpīgi samaisiet, pēc tam dodiet tai vairākas dienas, lai sajauktos, lai tā sasniegtu biezāku konsistenci, kas derētu ietvara veidošanas manipulācijām. Darboties ar svaigo, tikko no tubiņa izspiesto EMPASTA pastu nav iespējams. Jo ilgāk glabājas EMPASTA, jo labāk. Atrodoties uz krāsu paletes EMPASTA neizžūst, bet gluži otrādi, saglabā savu sabiezinātu konsistenci un ideāli der nagu veidošanas darbiem.</p>
 
  <p> <span class="bold">Kādas otiņas nepieciešamas darbam ar E.Mi materiāliem?</span> 
    <br />
   Darbam ar E.Mi materiāliem ieteicami trīs otiņu komplekti: “Otiņu komplekts mākslinieciskai gleznošanai”, “Otiņu komplekts gleznošanai ķīniešu stilā” un “Universālais otiņu komplekts”.</p>
 
  <p> <span class="bold">Darbam ar E.Mi materiāliem ieteicami trīs otiņu komplekti: “Otiņu komplekts mākslinieciskai gleznošanai”, “Otiņu komplekts gleznošanai ķīniešu stilā” un “Universālais otiņu komplekts”.</span> 
    <br />
   Lai otiņas pēc iespējas ilgāk saglabātu savas īpašības, neizmantojiet to attīrīšanai acetonu saturošus šķidrumus. Lai pilnībā attīrītu otiņas sariņus pietiek ar to izslaukšanu ar mitro salveti. Kā arī otiņas stāvokli viegli var ietekmēt tiešie ultravioletie stari.</p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>