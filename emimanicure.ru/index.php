<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(GetMessage("MAIN_PAGE_META_TITLE"));
$APPLICATION->SetPageProperty("description", GetMessage("MAIN_PAGE_META_DESCRIPTION"));
?>

<div class="container overflou">

    <div class="row">
        <nav role="navigation" class="navbar navbar-default">
            <!-- Логотип и мобильное меню -->
            <div class="navbar-header">

                <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="/" class="navbar-brand"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.svg" alt="" title=""/></a>

            </div>
            <!-- Навигационное меню -->
            <div id="navbarCollapse" class="collapse navbar-collapse navbar-right">
                <ul class="nav navbar-nav">
                    <li class="nav-item"><a href="#" id="nav-item-1">Твой выбор</a></li>
                    <li class="nav-item"><a href="#" id="nav-item-2">Как делают</a></li>
                    <li class="nav-item"><a href="#" id="nav-item-3">Коллекция 2018</a></li>
                    <li class="nav-item"><a href="#" id="nav-item-4">Почему E.Mi</a></li>
                    <li class="nav-item"><a href="#" id="nav-item-5">Где сделать</a></li>
                    <li class="nav-item"><a href="#" id="nav-item-6">В Instagram</a></li>
                </ul>

            </div>
        </nav>


    </div>
</div>

<section class="slider">

    <div id="slider" class="owl-carousel">

        <div class="slide-item">
            <div class="content col-md-6 col-sm-6 col-xs-8">
                <div class="slide-header">E.Mi маникюр</div>
                <div class="slide-sub-header">в вашем городе</div>
                <div class="slide-text">Полина Максимова</div>
                <div class="slide-sub-text">рекомендует</div>
                <div class="slide-tag">
                    <div class="gradient">
                        <a target="_blank" href="https://www.instagram.com/explore/tags/%D0%B1%D1%8B%D1%81%D1%82%D1%80%D0%BE%D0%BC%D0%BE%D0%B4%D0%BD%D0%BE%D1%81%D1%82%D0%BE%D0%B9%D0%BA%D0%BE/">
                            <svg>
                                <circle cx="25" cy="25" r="9" fill="none" stroke="white" stroke-width="3" id="bigCircle"></circle>
                                <circle cx="36" cy="14" r="2" fill="white" stroke="none" stroke-width="3" id="dot"></circle>
                                <rect rx="7" ry="7" x="7" y="7" width="36" height="36" fill="none" stroke="white" stroke-width="3" id="square"></rect>
                            </svg>
                            <p>#быстромодностойко</p>
                        </a>
                    </div>
                    <div class="gradient-mobile">
                        <a target="_blank" href="https://www.instagram.com/explore/tags/%D0%B1%D1%8B%D1%81%D1%82%D1%80%D0%BE%D0%BC%D0%BE%D0%B4%D0%BD%D0%BE%D1%81%D1%82%D0%BE%D0%B9%D0%BA%D0%BE/">
                            <svg>
                                <circle cx="20" cy="20" r="7" fill="none" stroke="white" stroke-width="2.4" id="bigCircle"></circle>
                                <circle cx="29" cy="11" r="1.8" fill="white" stroke="none" stroke-width="2.4" id="dot"></circle>
                                <rect rx="5" ry="5" x="6" y="6" width="28" height="28" fill="none" stroke="white" stroke-width="2.4" id="square"></rect>
                            </svg>
                            <p>#быстромодностойко</p>
                        </a>
                    </div>
                </div>
            </div>
            <img src="<?=SITE_TEMPLATE_PATH?>/images/slide1.jpg" alt="">
        </div>

        <div class="slide-item">
            <div class="content col-md-6 col-sm-6 col-xs-8">
                <div class="slide-header">E.Mi маникюр</div>
                <div class="slide-sub-header">в вашем городе</div>
                <div class="slide-text">Полина Максимова</div>
                <div class="slide-sub-text">рекомендует</div>
                <div class="slide-tag">
                    <div class="gradient">
                        <a target="_blank" href="https://www.instagram.com/explore/tags/%D0%B1%D1%8B%D1%81%D1%82%D1%80%D0%BE%D0%BC%D0%BE%D0%B4%D0%BD%D0%BE%D1%81%D1%82%D0%BE%D0%B9%D0%BA%D0%BE/">
                            <svg>
                                <circle cx="25" cy="25" r="9" fill="none" stroke="white" stroke-width="3" id="bigCircle"></circle>
                                <circle cx="36" cy="14" r="2" fill="white" stroke="none" stroke-width="3" id="dot"></circle>
                                <rect rx="7" ry="7" x="7" y="7" width="36" height="36" fill="none" stroke="white" stroke-width="3" id="square"></rect>
                            </svg>
                            <p>#быстромодностойко</p>
                        </a>
                    </div>
                    <div class="gradient-mobile">
                        <a target="_blank" href="https://www.instagram.com/explore/tags/%D0%B1%D1%8B%D1%81%D1%82%D1%80%D0%BE%D0%BC%D0%BE%D0%B4%D0%BD%D0%BE%D1%81%D1%82%D0%BE%D0%B9%D0%BA%D0%BE/">
                            <svg>
                                <circle cx="20" cy="20" r="7" fill="none" stroke="white" stroke-width="2.4" id="bigCircle"></circle>
                                <circle cx="29" cy="11" r="1.8" fill="white" stroke="none" stroke-width="2.4" id="dot"></circle>
                                <rect rx="5" ry="5" x="6" y="6" width="28" height="28" fill="none" stroke="white" stroke-width="2.4" id="square"></rect>
                            </svg>
                            <p>#быстромодностойко</p>
                        </a>
                    </div>
                </div>
            </div>
            <img src="<?=SITE_TEMPLATE_PATH?>/images/slide2.jpg" alt="">
        </div>

        <div class="slide-item">
            <div class="content col-md-6 col-sm-6 col-xs-8">
                <div class="slide-header">E.Mi маникюр</div>
                <div class="slide-sub-header">в вашем городе</div>
                <div class="slide-text">Полина Максимова</div>
                <div class="slide-sub-text">рекомендует</div>
                <div class="slide-tag">
                    <div class="gradient">
                        <a target="_blank" href="https://www.instagram.com/explore/tags/%D0%B1%D1%8B%D1%81%D1%82%D1%80%D0%BE%D0%BC%D0%BE%D0%B4%D0%BD%D0%BE%D1%81%D1%82%D0%BE%D0%B9%D0%BA%D0%BE/">
                            <svg>
                                <circle cx="25" cy="25" r="9" fill="none" stroke="white" stroke-width="3" id="bigCircle"></circle>
                                <circle cx="36" cy="14" r="2" fill="white" stroke="none" stroke-width="3" id="dot"></circle>
                                <rect rx="7" ry="7" x="7" y="7" width="36" height="36" fill="none" stroke="white" stroke-width="3" id="square"></rect>
                            </svg>
                            <p>#быстромодностойко</p>
                        </a>
                    </div>
                    <div class="gradient-mobile">
                        <a target="_blank" href="https://www.instagram.com/explore/tags/%D0%B1%D1%8B%D1%81%D1%82%D1%80%D0%BE%D0%BC%D0%BE%D0%B4%D0%BD%D0%BE%D1%81%D1%82%D0%BE%D0%B9%D0%BA%D0%BE/">
                            <svg>
                                <circle cx="20" cy="20" r="7" fill="none" stroke="white" stroke-width="2.4" id="bigCircle"></circle>
                                <circle cx="29" cy="11" r="1.8" fill="white" stroke="none" stroke-width="2.4" id="dot"></circle>
                                <rect rx="5" ry="5" x="6" y="6" width="28" height="28" fill="none" stroke="white" stroke-width="2.4" id="square"></rect>
                            </svg>
                            <p>#быстромодностойко</p>
                        </a>
                    </div>
                </div>
            </div>
            <img src="<?=SITE_TEMPLATE_PATH?>/images/slide3.jpg" alt="">
        </div>

    </div>

</section>

<section class="choose-manicure" id="emi">
    <div class="container">
        <div class="row">

            <div class="col-lg-6 col-md-6">
                <div class="title_t"><span class="color">Выбери</span> свой <br> <span class="color">E.MI</span> маникюр</div>
                <div class="text">из коллекции Осень 2018</div>
                <a href="#podpiska" id="scrollto-table-tent" class="button mobile-hidden">Выбрать маникюр</a>
            </div>
            <div class="col-lg-6 col-md-6">

                <div id="nail-slider" class="owl-carousel owl-theme">
                    <div class="item">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/nail_image_2.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/nail_image_4.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/nail_image_1.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/nail_image_5.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/nail_image_3.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/nail_image_6.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/nail_image_7.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/nail_image_8.jpg" alt="">
                    </div>
                </div>

            </div>
            <div class="col-md-12">
                <a href="#podpiska" id="scrollto-table-tent-mobile" class="button mobile-visible">Выбрать маникюр</a>
            </div>
        </div>
    </div>


</section>


<section class="service">
    <div class="container">
        <div class="row">
            <div class="title_t col-lg-12 col-md-12 text-center">Как делают <span class="color">e.mi</span> маникюр</div>

            <div class="col-md-4 text-center">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/polina.png" alt="">
            </div>
            <div class="col-md-8">
                <div>
                    <div class="text"><div class="icon flaticon-clock-3"></div>Быстро <span>- все готово за 1,5 часа</span></div>
                    <div class="text"><div class="icon flaticon-high-heel"></div>Модно <span>- трендовые оттенки и лучшие идеи дизайна</span></div>
                    <div class="text"><div class="icon flaticon-diamond"></div>Стойко <span>- наслаждайтесь результатом целый месяц</span></div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="table-tent">
    <div class="container">
        <div class="row">
            <div class="title_t col-lg-12 col-md-12 text-center"><span class="color">Коллекция</span> осень <span class="color">2018</span></div>
            <div class="text">Выбирайте <span>модный образ вместе с E.Mi</span> маникюр</div>
            <div class="col-md-3 col-sm-2"></div>

            <div class="col-md-6 col-sm-8">
                <div id="table-tent-slider" class="owl-carousel owl-theme">
                    <div class="item">
                        <a id="table_tent_1" href="#">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/table_tent_autumn_1.png" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a id="table_tent_2" href="#">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/table_tent_autumn_2.png" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a id="table_tent_3" href="#">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/table_tent_autumn_3.png" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a id="table_tent_4" href="#">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/table_tent_autumn_4.png" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a id="table_tent_5" href="#">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/table_tent_autumn_5.png" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a id="table_tent_6" href="#">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/table_tent_autumn_6.png" alt="">
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-2"></div>

        </div>
    </div>
</section>



<section class="emilac">
    <div class="container">
        <div class="row">
            <div class="title_t col-lg-12 col-md-12 text-center">Почему <span class="color">я выбираю E.Mi</span> маникюр</div>
            <?/*<div class="text col-md-12">Лучшие салоны и мастера готовы <span>сделать E.Mi маникюр</span> работая по единым стандартам</div>*/?>

            <div class="col-md-4 text-center">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/polina_emilac.png" alt="">
            </div>
            <div class="col-md-8 text-right">

                <div class="text"><span>Стойкий</span> - я уверена в том, что мой маникюр безупречен целый месяц</div>
                <div class="text"><span>Модный</span> - я знаю, что мой маникюр самый модный</div>
                <div class="text"><span>Качество, которому я доверяю</span> - у E.Mi  высокие стандарты качества продукции</div>
                <div class="text"><span>Разнообразие</span> - мой выбор - я хочу экспериментировать!</div>
                <div class="text"><span>Это мой любимый бренд</span> - я люблю E.Mi-маникюр</div>
                <? /*
                <div class="title-block col-lg-12 col-md-12">Как делают <span class="color">e.mi</span>-маникюр</div>

                <div class="blocks row">
                    <div class="block col-xs-4 text-center">
                        <div class="img">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/n11.png" alt="" title="" class="img-responsive"/>
                        </div>
                        <div class="name">
                            по эскизам <br/>Екатерины <br/>Мирошниченко
                        </div>
                    </div>
                    <div class="block col-xs-4 text-center">
                        <div class="img">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/n11.png" alt="" title="" class="img-responsive"/>
                        </div>
                        <div class="name">
                            при помощи <br/>материалов <br/>E.Mi
                        </div>
                    </div>
                    <div class="block col-xs-4 text-center">
                        <div class="img">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/n11.png" alt="" title="" class="img-responsive"/>
                        </div>
                        <div class="name">
                            при помощи <br/>материалов <br/>E.Mi
                        </div>
                    </div>
                </div>
                */?>
            </div>
        </div>
    </div>
</section>


<section class="wrapp_map" id="wrapp_map">

    <div class="container">
        <div class="row text-center">
            <div class="title_t col-lg-12"><span class="color">E.Mi</span>-маникюр в твоём городе</div>

                <div class="col-md-12 col-sm-12">
                    <p>156 городов / 32 000 салонов по всей России</p><a onClick="modal_salon.open();" class="button click_zak sub">выбери салон</a>
                </div>



        </div>


    </div>
    <div class="container-fluid">
        <div id="map">
            <?$APPLICATION->IncludeComponent(
                "ithive:salons",
                "salons-emi",
                array(
                    "KEY" => "AJx6J1UBAAAAY0UMSQIA-7-PSRugSDu-fBl9UAlk8zQeUBoAAAAAAAAAAADRFI8DCuQ1FX0n7T1DHV-L-6UQwg==",
                    "ICON_FILE" => "/bitrix/components/ithive/offices.list/templates/.default/images/icon_arrow_5.png",
                    "ICON_SIZE" => "36, 48",
                    "ICON_OFFSET" => "-15,-30",
                    "INCLUDE_JQUERY" => "N",
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_AS_RATING" => "rating",
                    "TAGS_CLOUD_ELEMENTS" => "150",
                    "PERIOD_NEW_TAGS" => "",
                    "FONT_MAX" => "50",
                    "FONT_MIN" => "10",
                    "COLOR_NEW" => "3E74E6",
                    "COLOR_OLD" => "C0C0C0",
                    "TAGS_CLOUD_WIDTH" => "100%",
                    "SEF_MODE" => "N",
                    "AJAX_MODE" => "Y",
                    "IBLOCK_TYPE" => "contacts",
                    "IBLOCK_ID" => "29",
                    "NEWS_COUNT" => "2000",
                    "USE_SEARCH" => "N",
                    "USE_RSS" => "N",
                    "USE_RATING" => "N",
                    "USE_CATEGORIES" => "N",
                    "USE_REVIEW" => "N",
                    "USE_FILTER" => "Y",
                    "SHOW_FILTER" => "N",
                    "SORT_BY1" => "NAME",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "CHECK_DATES" => "Y",
                    "PREVIEW_TRUNCATE_LEN" => "250",
                    "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "LIST_FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "LIST_PROPERTY_CODE" => array(
                        0 => "ADDRESS",
                        1 => "PHONES",
                        2 => "NAME_SHORT",
                        3 => "",
                    ),
                    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "META_KEYWORDS" => "-",
                    "META_DESCRIPTION" => "-",
                    "BROWSER_TITLE" => "-",
                    "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "DETAIL_FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "DETAIL_PROPERTY_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "DETAIL_DISPLAY_TOP_PAGER" => "N",
                    "DETAIL_DISPLAY_BOTTOM_PAGER" => "N",
                    "DETAIL_PAGER_TITLE" => "Страница",
                    "DETAIL_PAGER_TEMPLATE" => "",
                    "DETAIL_PAGER_SHOW_ALL" => "N",
                    "DISPLAY_PANEL" => "N",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "USE_PERMISSIONS" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "Y",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "FILTER_NAME" => "arrFilter",
                    "FILTER_FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "FILTER_PROPERTY_CODE" => array(
                        0 => "CITY",
                        1 => "PRODUCT_TYPE",
                        2 => "",
                    ),
                    "AJAX_OPTION_SHADOW" => "Y",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "AJAX_OPTION_HISTORY" => "N",
                    "SEF_FOLDER" => "/offices/",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "COMPONENT_TEMPLATE" => "salons-emi",
                    "VARIABLE_ALIASES" => array(
                        "SECTION_ID" => "SECTION_ID",
                        "ELEMENT_ID" => "ELEMENT_ID",
                    )
                ),
                false
            );?>
        </div>
        <script type="text/javascript">

            var modal_salon = new tingle.modal({
                closeMethods: ['overlay', 'button', 'escape'],
                footer: true,
                closeLabel: "Закрыть",
                stickyFooter: true
            });

            modal_salon.setContent(document.querySelector('.shop_search_result').innerHTML);
            modal_salon.addFooterBtn('Отмена', 'tingle-btn tingle-btn--pull-right', function(){
                modal_salon.close();
            });

        </script>
    </div>
</section>


<? /*
<section class="wrapp_rewes" id="wrapp_rewes">

    <div class="container">
        <div class="row">
            <div class="title_t col-lg-12 col-md-12 text-center">отзывы о <span class="color">e.mi</span>-маникюр</div>

            <a href="#wrapp_rewes" target="_blank" class="block col-lg-4 col-md-4">
                <div class="wrapp">
                    <div class="text">
                        <p>
                            “Прекрасный E.Mi-маникюр, мне очень нравится! Цвет и форма как я люблю! Я всегда выбираю
                            сложные розовые и коралловые оттенки.”
                        </p>
                    </div>
                    <div class="name">@kottova</div>
                    <div class="dolj">Татьяна Котова</div>
                </div>
            </a>


            <a href="#wrapp_rewes" target="_blank" class="block col-lg-4 col-md-4">
                <div class="wrapp">
                    <div class="text">
                        <p>
                            “Люблю цвета новой коллекции E.MiLac Shimmer Dreams.<br>
                            Love these new colors by @emiroshnichenko”
                        </p>
                    </div>
                    <div class="name">@TOMBACHIC</div>
                    <div class="dolj">Том Бачик</div>
                </div>
            </a>


            <a href="#wrapp_rewes" target="_blank" class="block col-lg-4 col-md-4">
                <div class="wrapp">
                    <div class="text">
                        <p>
                            “Познакомились с брендом модного салонного маникюра - E.Mi @emimanicure и с его основательницей Екатериной Мирошниченко<br>
                            Кажется, перепробовали вместе все цвета из новой коллекции гель-лаков Fashion Queen, ну, а мои фавориты - манящий Магнетизм и дерзкий красный - Алое пламя”
                        </p>
                    </div>
                    <div class="name">@chistyakova_ionova</div>
                    <div class="dolj">Глюк'oZa</div>
                </div>
            </a>


        </div>
    </div>
</section>
*/ ?>

<? /*

<section class="subscribe">

    <div class="container">
        <div class="row">
            <div class="title_t col-lg-12">Записаться на <span class="color">E.Mi-маникюр</span></div>
            <div class="col-md-3 col-sm-2 col-xs-1"></div>
            <div class="col-md-6 col-sm-8 col-xs-10">
                <form data-toggle="validator" role="form">
                    <div class="form-group has-feedback">
                        <label for="inputName" class="control-label">Как вас зовут?</label>
                        <input type="text" pattern="^[\D]{2,}$" class="form-control" id="inputName" placeholder="Имя Фамилия" data-error="Введите Ваше имя"  required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="inputPhone" class="control-label">Контактный телефон</label>
                        <input type="text" pattern="^\+7\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}$" class="form-control" id="inputPhone" placeholder="+7 (999) 999-99-99" data-error="Введите номер телефона" required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="inputEmail" class="control-label">Электронная почта</label>
                        <input type="email" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control" id="inputEmail" placeholder="E-mail" data-error="Введите правильный E-mail" required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group has-feedback">
                        <div class="checkbox">


                            <label class="control control-checkbox">
                                Соглашение об обработке персональных данных
                                <input type="checkbox" id="terms" data-error="Необходимо согласиться с условиями" checked="checked" required>
                                <div class="control_indicator"></div>
                            </label>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <center><button type="submit" class="button">Записаться</button></center>
                    </div>
                </form>
            </div>
            <div class="col-md-3 col-sm-2 col-xs-1"></div>
        </div>

    </div>

</section>

*/ ?>
<? /*
<section class="podpiska" id="podpiska">
    <div class="container">
        <div class="row">
            <div class="slogan col-lg-7 col-md-7">
                <div class="title">
                    ВСЕ СЕКРЕТЫ БЕСКОНЕЧНО<br>
                    МОДНОГО И БЕЗУПРЕЧНО<br>
                    СТОЙКОГО E.Mi-МАНИКЮРА<br>
                    У ТЕБЯ НА ПОЧТЕ
                </div>
                <p>
                    Подпишись на нашу рассылку и получай информацию о трендах в области нейл-дизайна и новых коллекциях
                    E.Mi-маникюра!
                </p>
            </div>
            <div class="form col-lg-4 col-lg-offset-1 col-md-5">
                <form class="sub_form">
                    <div class="input_row">
                        <div class="infofild">Тебя зовут</div>
                        <input type="text" class="text_inp name_sub" name="name_us" value=""
                               placeholder="Кристина Иванова"/>
                    </div>
                    <div class="input_row">
                        <div class="infofild">Контактный телефон</div>
                        <input type="tel" class="text_inp phone_sub" value="" name="tel" placeholder="+7 (123) 456-78"/>
                    </div>
                    <div class="input_row">
                        <div class="infofild">Электронная почта</div>
                        <input type="email" class="text_inp email_sub" name="e-mail" value=""
                               placeholder="anna.annovna@gmail.com"/>
                    </div>
                    <div class="input_row">
                        <input type="checkbox" name="yeas_us" class="text_inp rules_sub" id="soglas"/>
                        <label for="soglas">Соглашение об обработке персональных данных</label>
                    </div>
                    <input type="submit" value="ок" class="submit feedback but_sub"
                           onclick="SubContect(this); return false;">
                </form>
            </div>
        </div>
    </div>
</section>
*/ ?>
<section class="instagram">

    <div class="container-fluid">
        <div class="row">
            <div class="title_t col-lg-12 col-md-12 text-center"><span class="color">@EMi</span>manicure</div>
        </div>
    </div>

    <?$APPLICATION->IncludeComponent(
        "prgrant:instagram.gallery.emimanicure",
        "",
        Array(
            "IS_JQUERY" => "N",
            "PHP_CACHE_TIME" => "3600",
            "USERNAME" => "emimanicure"
        )
    );?>

</section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>