<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle(GetMessage("MAIN_PAGE_META_TITLE"));
$APPLICATION->SetPageProperty("description", GetMessage("MAIN_PAGE_META_DESCRIPTION"));


use Bitrix\Main;
use Bitrix\Main\Context;
use Bitrix\Main\Web\Cookie;
use Bitrix\Main\Mail\Event;

$request = Context::getCurrent()->getRequest();
$response = Context::getCurrent()->getResponse();
$server = Context::getCurrent()->getServer();


$hash = $request->getQuery("check");

//echo "<pre>";
//print_r($hash);
//echo "</pre>";


$arSelect = Array("ID","IBLOCK_ID","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>GIFT_IBLOCK_ID, "ACTIVE"=>"Y","=PROPERTY_gift_hash"=>$hash);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
$is_hash=false;
while($ob = $res->GetNextElement())
{
    $is_hash=true;
}

if ($is_hash==true){ // если hash найден и существует
    //gift_email_check
    $arSelect = Array("ID","IBLOCK_ID","PROPERTY_*");
    $arFilter = Array("IBLOCK_ID"=>GIFT_IBLOCK_ID, "ACTIVE"=>"Y", "=PROPERTY_gift_action_number"=>GIFT_NUMBER_ACTION, "=PROPERTY_gift_hash"=>$hash);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
    $email_check=false;
    $not_address=false;
    while($ob = $res->GetNextElement())
    {
        $arProps = $ob->GetProperties();

        if ($arProps['gift_address']['VALUE'] != ""){ // если было заполнено поле доставки
            $not_address = true;
        }
        if ($arProps['gift_email_check']['VALUE'] == "Y"){ // если проверка email уже была
            $email_check = true;
            break;
        }
        $arFields = $ob->GetFields();
        $PRODUCT_ID = $arFields["ID"];

    }
    if($email_check==false&&$not_address==false){
        $el = new CIBlockElement;

        $PROP = array();
        $PROP[1874] = "Y"; //Подтверждение email gift_email_check
        $PROP[1873] = $arProps["gift_number_participant"]["VALUE"]; // номер участника
        $PROP[1872] = GIFT_NUMBER_ACTION;                 // номер акции
        $PROP[1865] = $arProps["gift_name"]["VALUE"];     // имя участника
        $PROP[1867] = $arProps["gift_email"]["VALUE"];    // email
        $PROP[1866] = $arProps["gift_phone"]["VALUE"];    // телефон
        $PROP[1871] = $arProps["gift_hash"]["VALUE"];     // hash


        $arLoadProductArray = Array(
            "MODIFIED_BY"       => 17465, // $USER->GetID() элемент изменен текущим пользователем
            "PROPERTY_VALUES"   => $PROP
        );

        $res = $el->Update($PRODUCT_ID, $arLoadProductArray);

        if ($res){
            $tingle_message = "Ваша ссылка была успешно активирована! Пожалуйста, введите данные доставки для отправки подарка.";
        }
    }
    else if($email_check==true&&$not_address==false){


        if ($request->isPost()) { // если была отправлена форма

            $city = $request->getPost('inputCity');
            $address = $request->getPost('inputAddress');
            $index = $request->getPost('inputIndex');

            $salon = $request->getPost('checkbox_salon');
            $master = $request->getPost('checkbox_master');
            $another = $request->getPost('checkbox_another');


            $arSelect = Array("ID","IBLOCK_ID","PROPERTY_*");
            $arFilter = Array("IBLOCK_ID"=>GIFT_IBLOCK_ID, "ACTIVE"=>"Y", "=PROPERTY_gift_action_number"=>GIFT_NUMBER_ACTION, "=PROPERTY_gift_hash"=>$hash);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);

            while($ob = $res->GetNextElement())
            {
                $arProps = $ob->GetProperties();
                $arFields = $ob->GetFields();
                $PRODUCT_ID = $arFields["ID"];
            }
            if ($arProps){
                $el = new CIBlockElement;

                $PROP = array();
                $PROP[1874] = "Y"; //Подтверждение email gift_email_check
                $PROP[1873] = $arProps["gift_number_participant"]["VALUE"]; // номер участника
                $PROP[1872] = GIFT_NUMBER_ACTION;                 // номер акции
                $PROP[1865] = $arProps["gift_name"]["VALUE"];     // имя участника
                $PROP[1867] = $arProps["gift_email"]["VALUE"];    // email
                $PROP[1866] = $arProps["gift_phone"]["VALUE"];    // телефон
                $PROP[1871] = $arProps["gift_hash"]["VALUE"];     // hash

                $PROP[1868] = $city;        // Город доставки
                $PROP[1869] = $address;     // Адрес доставки
                $PROP[1870] = $index;       // Почтовый индекс
                $PROP[1875] = $salon;       // Метка "Салон" gift_tag_salon
                $PROP[1875] = $master;      // Метка "Мастер" gift_tag_master
                $PROP[1877] = $another;     // Метка "Другое" gift_tag_another


                $arLoadProductArray = Array(
                    "MODIFIED_BY"       => 17465, // $USER->GetID() элемент изменен текущим пользователем
                    "PROPERTY_VALUES"   => $PROP
                );

                $res = $el->Update($PRODUCT_ID, $arLoadProductArray);

                if ($res){
                    $tingle_message = "Спасибо за участие в Акции! Мы свяжемся с вами и вышлем подарок на ваш адрес доставки.";
                }
            }
        }
        else{
            $tingle_message = "Введите данные доставки для отправки подарка!";
        }



    }
    else if($email_check==true&&$not_address==true)
    {
        $tingle_message = "Данная ссылка уже была активирована и данные доставки заполнены и отправлены!";
        header( "Refresh:4; url=https://emimanicure.ru/gift/", true, 303);
        // LocalRedirect("/gift/");
    }
}
else{

    $tingle_message = "Ссылка для активации не найдена!";
    header( "Refresh:4; url=https://emimanicure.ru/gift/", true, 303);
    //LocalRedirect("/gift/");
}

?>

<section class="service">
    <div class="container">
        <div class="row">

            <div class="col-md-2">
                <a href="/gift" class="navbar-brand"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo_emi.svg" alt="E.Mi" title="E.Mi"/></a>
            </div>
            <div class="col-md-8">
                <div class="title_t"><span class="color">Новенькое<br></span>для ваших постоянных клиентов<br><span class="color">В подарок!</span></div>
            </div>
            <div class="col-md-2">
                <div id="icon" class="flaticon-business"></div>
            </div>

        </div>
    </div>
</section>
<section class="emilac sent-gift">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title_t col-lg-12 col-md-12 text-center">Ваш адрес <span class="color">доставки</span></div>
                <div class="text col-md-12 text-center">Оставьте, пожалуйста адрес, по которому мы сможем отправить <span>вам подарок</span></div>
                <?/*<div class="text col-md-12">Лучшие салоны и мастера готовы <span>сделать E.Mi маникюр</span> работая по единым стандартам</div>*/?>


                <div class="col-md-6 col-sm-6 subscribe">
                    <form data-toggle="validator" role="form" method="post" action="?check=<?=$hash;?>">
                        <div class="form-group has-feedback">
                            <label for="inputCity" class="control-label">Ваш город</label>
                            <input name="inputCity" type="text" pattern="^[\D]{2,}$" class="form-control" id="inputCity" placeholder="Город" data-error="Введите город доставки"  required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="inputAddress" class="control-label">Адрес доставки</label>
                            <input name="inputAddress" type="text" pattern="^{2,}$" class="form-control" id="inputAddress" placeholder="Адрес" data-error="Введите адрес доставки"  required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="inputIndex" class="control-label">Почтовый индекс</label>
                            <input name="inputIndex" type="text" pattern="^[0-9]{6}$" class="form-control" id="inputIndex" placeholder="Индекс" data-error="Введите индекс местоположения"  required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group group-checkbox has-feedback">
                            <div class="checkbox">
                                <label class="control control-checkbox">
                                    Я руковожу салоном
                                    <input name="checkbox_salon" type="checkbox" id="terms_1">
                                    <div class="control_indicator"></div>
                                </label>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="checkbox">
                                <label class="control control-checkbox">
                                    Я мастер
                                    <input name="checkbox_master" type="checkbox" id="terms_2">
                                    <div class="control_indicator"></div>
                                </label>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="checkbox">
                                <label class="control control-checkbox">
                                    Другое
                                    <input name="checkbox_another" type="checkbox" id="terms_3" checked="checked">
                                    <div class="control_indicator"></div>
                                </label>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <script>
                            $(".group-checkbox input").on("click", function() {
                                $(".group-checkbox input").prop('checked',false);
                                $(this).prop('checked', true);
                                console.log($val);
                            });
                        </script>
                        <div class="form-group text-center">
                            <button type="submit" class="button"><div class="flaticon-business"></div>Отправить</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="sent-gift-text col-md-12"><span>Тейбл-тенты от E.Mi это:</span></div>

                    <div class="sent-gift-text">
                        <ul>
                            <li>Рост среднего чека на маникюр до 25 %</li>
                            <li>Самые модные предложения весна/лето 2018</li>
                            <li>Новые предложения для постоянных клиенток</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>