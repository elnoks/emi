<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
$APPLICATION->SetTitle(GetMessage("MAIN_PAGE_META_TITLE"));
$APPLICATION->SetPageProperty("description", GetMessage("MAIN_PAGE_META_DESCRIPTION"));

use Bitrix\Main;
use Bitrix\Main\Context;
use Bitrix\Main\Web\Cookie;
use Bitrix\Main\Mail\Event;

$request = Context::getCurrent()->getRequest();
$response = Context::getCurrent()->getResponse();
$server = Context::getCurrent()->getServer();

$tingle_message = "";

if ($request->isPost()) {

    $name = $request->getPost('inputName');
    $phone = str_replace(array( '(', ')' ), '', $request->getPost('inputPhone'));
    $email = $request->getPost('inputEmail');

    // проверяем на уже отправленные данные
    $arSelect = Array("ID","IBLOCK_ID","PROPERTY_*");
    $arFilter = Array("IBLOCK_ID"=>GIFT_IBLOCK_ID, "ACTIVE"=>"Y",
        "PROPERTY_gift_action_number"=>GIFT_NUMBER_ACTION
    );
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);

    $fail = false;
    while($ob = $res->GetNextElement())
    {
        $arProps = $ob->GetProperties();
        //print_r($arProps);
        //if ($arProps['gift_action_number'])
        if ($arProps['gift_email']['VALUE'] == $email){
            $fail=true;
            break;
        }
        if ($arProps['gift_phone']['VALUE'] == $phone){
            $fail=true;
            break;
        }
    }
    print_r($fail);
    // ------------------------------------

    if($fail==false){

        $arSelect = Array("ID","IBLOCK_ID","PROPERTY_*");
        $arFilter = Array("IBLOCK_ID"=>GIFT_IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_gift_action_number_VALUE"=>GIFT_NUMBER_ACTION);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
        $count=0;

        while($ob = $res->GetNextElement())
        {
            $count++;
        }

        $count=$count+1;

        $count_remained = GIFT_MAX_PARTICIPANT - $count;

        if ($count<=GIFT_MAX_PARTICIPANT){
            $hash = hash('ripemd256', time().'-'.$email);

            $el = new CIBlockElement;

            $PROP = array();
            $PROP[1873] = $count;               // номер участника
            $PROP[1872] = GIFT_NUMBER_ACTION;   // номер акции
            $PROP[1865] = $name;                // имя участника
            $PROP[1867] = $email;               // email
            $PROP[1866] = $phone;               // телефон
            $PROP[1871] = $hash;                // hash

            $array = Array(
                //"TIMESTAMP_X"       => time(),
                "MODIFIED_BY"       => 17465, // $USER->GetID() элемент изменен текущим пользователем
                //"DATE_CREATE"       => time(),
                "CREATED_BY"        => 17465,
                "IBLOCK_ID"         => GIFT_IBLOCK_ID,
                "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
                "ACTIVE"            => "Y",            // активен
                "PROPERTY_VALUES"   => $PROP,
                "NAME"              => $count."-й участник акции №".GIFT_NUMBER_ACTION,
            );

            $el->Add($array);

            $arEventFields = array(
                "GIFT_NAME" => $name,
                "GIFT_PHONE" => $phone,
                "GIFT_EMAIL" => $email,
                "GIFT_HASH" => $hash,
                "GIFT_COUNT" => $count
            );
            CEvent::Send("GIFT_SEND", "em", $arEventFields, "N", 2435);

            $tingle_message =  "Спасибо за регистрацию в Акции. Письмо было успешно отправлено на ".$email."!";

//            $res_email = Event::send(array(
//                "EVENT_NAME" => "GIFT_SEND",
//                "LID" => "em",
//
//                "C_FIELDS" => array(
//                    "GIFT_NAME" => $name,
//                    "GIFT_PHONE" => $phone,
//                    "GIFT_EMAIL" => $email,
//                    "GIFT_HASH" => $hash,
//                    "GIFT_COUNT" => $count
//                ),
//            ));
//            if($res_email->isSuccess())
//            {
//                $ID = $res_email->getId();
//                echo "Почта отправлена: ".$ID;
//            }
//            else
//            {
//                $error = $res_email->getErrorMessages();
//                echo "Ошибка отправки: <pre>".var_export($error, true)."</pre>";
//            }
            //if ($res_email){}
        }
        else
        {

            $tingle_message = "<h1>К сожалению, количество участников ограничено</h1>";
        }

    }
    else{
        $tingle_message = "<h1>К сожалению, вы уже отправляли свои данные для участия в акции</h1>";
    }
//    $response->addCookie(
//        new Cookie(
//            'selftest-filter-section-include-sub',
//            trim((string)$request->getPost('selftest-filter-section-include-sub'))
//        )
//    );
//
//    $filterPostSection = array_filter((array)$request->getPost('selftest-filter-section'));
//
//    if (!empty($filterPostSection)) {
//        $response->addCookie(new Cookie('selftest-filter-section', implode(',', $filterPostSection)));
//        $response->addHeader('Location', '/path/to/url/');
//        $response->flush();
//    }

}

if (!isset($count)){
    $arSelect = Array("ID","IBLOCK_ID","PROPERTY_*");
    $arFilter = Array("IBLOCK_ID"=>GIFT_IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_gift_action_number"=>GIFT_NUMBER_ACTION);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
    $count=0;
    while($ob = $res->GetNextElement())
    {
        $count++;
    }
    $count_remained = GIFT_MAX_PARTICIPANT - ($count);
}
?><section class="service">
<div class="container">
	<div class="row">
		<div class="col-md-2">
 <a href="/gift/" class="navbar-brand"><img alt="E.Mi" src="/bitrix/templates/emimanicure/images/logo_emi.svg" title="E.Mi"></a>
		</div>
		<div class="col-md-8">
			<div class="title_t">
 <span class="color">Новенькое<br>
 </span>для ваших постоянных клиентов<br>
 <span class="color">В подарок!</span>
			</div>
		</div>
		<div class="col-md-2">
			<div id="icon" class="flaticon-business">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="choose-manicure-desc text-center">
				<p>
					 Тейбл-тенты ЛЕТО 2018<br>
 <span class="color">только для первых <?=GIFT_MAX_PARTICIPANT;?> человек</span>
				</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 text-center subscribe">
			<div class="col-md-3">
 <img src="/bitrix/templates/emimanicure/images/ekaterina_m_gift_3.png" alt="">
			</div>
			<div class="col-md-6 form-gift">
				<div class="text text-center">
					 Получить подарок
				</div>
 <br>
				<form data-toggle="validator" role="form" action="" method="post">
					<div class="form-group has-feedback col-md-4">
 <input name="inputName" type="text" pattern="^[\D]{2,}$" class="form-control" id="inputName" placeholder="Имя" data-error="Введите Ваше имя" required="">
						<div class="help-block with-errors">
						</div>
					</div>
					<div class="form-group has-feedback col-md-4">
 <input name="inputPhone" type="text" pattern="^\+7\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}$" class="form-control" id="inputPhone" placeholder="+7 (999) 999-99-99" data-error="Введите номер телефона" required="">
						<div class="help-block with-errors">
						</div>
					</div>
					<div class="form-group has-feedback col-md-4">
 <input name="inputEmail" type="email" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control" id="inputEmail" placeholder="E-mail" data-error="Введите правильный E-mail" required="">
						<div class="help-block with-errors">
						</div>
					</div>
					<div class="col-md-12">
					</div>
					<div class="form-group has-feedback col-md-8">
						<div class="checkbox">
 <label class="control control-checkbox">
							Соглашение об обработке персональных данных <input name="terms" type="checkbox" id="terms" data-error="Необходимо согласиться с условиями" checked="checked" required="">
							<div class="control_indicator">
							</div>
 </label>
							<div class="help-block with-errors">
							</div>
						</div>
					</div>
					<div class="form-group text-right col-md-4">
 <button type="submit" class="button">Получить</button>
					</div>
				</form>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="flaticon-ribbon">
				</div>
				<div id="table-tent-slider" class="owl-carousel owl-theme">
					<div class="item">
 <a id="table_tent_1" href="#"> <img src="/bitrix/templates/emimanicure/images/table_tent_1.png" alt=""> </a>
					</div>
					<div class="item">
 <a id="table_tent_2" href="#"> <img src="/bitrix/templates/emimanicure/images/table_tent_2.png" alt=""> </a>
					</div>
					<div class="item">
 <a id="table_tent_3" href="#"> <img src="/bitrix/templates/emimanicure/images/table_tent_3.png" alt=""> </a>
					</div>
					<div class="item">
 <a id="table_tent_4" href="#"> <img src="/bitrix/templates/emimanicure/images/table_tent_4.png" alt=""> </a>
					</div>
					<div class="item">
 <a id="table_tent_5" href="#"> <img src="/bitrix/templates/emimanicure/images/table_tent_5.png" alt=""> </a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </section>
<? /*
<section class="table-tent">
    <div class="container">
        <div class="row">
            <div class="title_t col-lg-12 col-md-12 text-center">Как работает <span class="color">тейбл-тент?</span></div>

            <div class="col-md-4 col-sm-5">
                <div class="flaticon-ribbon"></div>
                <div id="table-tent-slider" class="owl-carousel owl-theme">
                    <div class="item">
                        <a id="table_tent_1" href="#">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/table_tent_1.png" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a id="table_tent_2" href="#">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/table_tent_2.png" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a id="table_tent_3" href="#">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/table_tent_3.png" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a id="table_tent_4" href="#">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/table_tent_4.png" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a id="table_tent_5" href="#">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/table_tent_5.png" alt="">
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-8 col-sm-7">
                <div>
                    <ul>
                        <li>Как сезонное меню в ресторане</li>
                        <li>Новые модные готовые варианты каждые 3 месяца</li>
                        <li>Дизайны, которые продают себя сами</li>
                    </ul>

                </div>
            </div>

        </div>
    </div>
</section>
*/ ?> <section class="emilac">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="title_t col-lg-12 col-md-12 text-center">
				 Спешите получить <span class="color">подарок от E.Mi</span>
			</div>
			<div class="title_sub col-lg-12 col-md-12 text-center ">
				 Осталось <span class="color"><?=$count_remained?></span> подарков
			</div>
			 <?/*<div class="text col-md-12">Лучшие салоны и мастера готовы <span>сделать E.Mi маникюр</span> работая по единым стандартам</div>*/?> <? /*
                <div class="col-md-3 col-sm-2"></div>
                <div class="col-md-6 col-sm-8 text-right subscribe">
                    <form data-toggle="validator" role="form" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group has-feedback">
                            <label for="inputName" class="control-label">Ваше имя</label>
                            <input name="inputName" type="text" pattern="^[\D]{2,}$" class="form-control" id="inputName" placeholder="Имя" data-error="Введите Ваше имя"  required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="inputPhone2" class="control-label">Контактный телефон</label>
                            <input name="inputPhone" type="text" pattern="^\+7\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}$" class="form-control" id="inputPhone2" placeholder="+7 (999) 999-99-99" data-error="Введите номер телефона" required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="inputEmail" class="control-label">Электронная почта</label>
                            <input name="inputEmail" type="email" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control" id="inputEmail" placeholder="E-mail" data-error="Введите правильный E-mail" required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group has-feedback">
                            <div class="checkbox">
                                <label class="control control-checkbox">
                                    Соглашение об обработке персональных данных
                                    <input name="terms" type="checkbox" id="terms" data-error="Необходимо согласиться с условиями" checked="checked" required>
                                    <div class="control_indicator"></div>
                                </label>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="button"><div class="flaticon-business"></div> Отправить</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-3 col-sm-2"></div>
                */ ?>
		</div>
	</div>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>