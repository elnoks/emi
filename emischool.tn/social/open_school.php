<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("How to open the E.Mi school representative office");
?><div class="bx_page">
	<p style="text-align: center;">
 <img width="800" alt="765.jpg" src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" height="105" title="765.jpg" border="0"><b> <br>
 </b>
	</p>
	<p>
 <b> <br>
 </b>
	</p>
	<p>
 <b>La coopération avec E.Mi School&nbsp;Tunisie&nbsp;est la possibilité d'obtenir un projet clé en main, apportant un revenu stable. En coopérant avec nous, vous recevez une gamme complète de techniques : du design sur les ongles, la vente jusqu’à la gestion et au développement de votre entreprise. E.Mi Manucure c’est sont les designs toujours uniques, demandés par les clients, élégants et reconnaissables!</b>
	</p>
	<p>
 <b>Devenez nos représentants régionaux et nous vous apprendrons à avoir autant de succès et de créativité !</b> Aujourd'hui, E.Mi School - l'École de Nail Design by Ekaterina Miroshnichenko est connue non seulement en Russie mais dans le monde entier: des prix prestigieux de compétitions internationales, des séminaires en Allemagne et en Italie, l'ouverture d'un bureau international en Europe à Prague et filial en&nbsp;Tunisie à Tunis. <br>
		 Vous pouvez obtenir le droit exclusif d'exercer dans votre région une activité passionnante et très rentable pour la formation et le développement professionnel des spécialistes du service de l'ongle sous le nom de la marque «École de Nail Design by Ekaterina Miroshnichenko».
	</p>
	<p>
 <b>Joignez-vous à ceux qui sont toujours dans la tendance!</b> L'École de Nail Design by Ekaterina Miroshnichenko a créé non seulement une technologie unique des designs reconnaissables mais aussi des techniques d'apprentissage non moins uniques. <br>
 <b>Nous mettons à disposition de tous, la joie de l'art de la création et de la beauté !</b> <br>
 <b>Les avantages de « l'École de Nail Design by Ekaterina Miroshnichenko »:</b>
	</p>
	<ul>
		<li>La demande de services éducatifs est confirmée par le fait que dans notre centre de formation nous préparons chaque année plus de 500 artistes de toutes les régions de France par des programmes divers. </li>
		<li>La formation est assurée sur une marque unique de produits pour le design des ongles the <a href="/catalog/">«E.Mi»</a>.</li>
		<li>Le niveau international de la société. Bureau en Europe (Prague);</li>
		<li>Au cœur du programme de l'auteur, une technique unique développée et brevetée par Ekaterina Miroshnichenko, championne du monde sur la version OMC dans la catégorie Nail Design (Paris, 2011);</li>
	</ul>
	<p>
	</p>
	<p>
 <b>Nous fournissons toutes les représentations et tout le nécessaire pour la réussite de votre travail pour l'unité de la formation de l'»École de Nail Design by Ekaterina Miroshnichenko». <br>
		 En ouvrant un bureau de représentation officielle de l'École de nail design, vous obtenez:</b>
	</p>
	<ol>
		<li><b>Le droit exclusif </b>de travailler sous le nom de marque de l'École de Nail Design by Ekaterina Miroshnichenko, en fournissant les services éducatifs sur le territoire désigné dans votre contrat. Dans un même département, on ne peut ouvrir qu'un seul bureau.</li>
		<li><b>Les programmes de formation sont </b>tous faits et développés par Ekaterina Miroshnichenko. Le centre régional de formation reçoit une économie considérable de temps et de ressources pour le développement des programmes d'études, la mise en place des méthodes, la réalisation d'études, l'élaboration de matériel de formation et les manuels. Cette circonstance permettra d'envoyer la force principale dans l'admission d'élèves et de récupérer rapidement les coûts d'ouverture d'un bureau de représentation.</li>
		<li><b>L'inclusion dans le système d'une seule certification. </b>La représentation a le droit de délivrer à ses élèves les certificats de la marque avec un hologramme de « l'École de Nail Design by Ekaterina Miroshnichenko».</li>
		<li><b>Le droit à des réductions exclusives sur les produits <a href="/catalog/">E.Mi</a>.</b></li>
		<li><b>La publicité et le support informatique.</b> L'école de l'auteur met toutes les informations sur ses représentations dans des revues professionnelles fédérales, ainsi que dans son propre catalogue de publicité et sur le site officiel. Elle contribue à l'organisation des expositions et des conférences régionales, transmet les modèles de publicité pour le placement pour des publications régionales.</li>
	</ol>
	<p>
	</p>
	<p>
 <b>Les caractéristiques de la combinaison de la coopération comme représentation officielle de l'Ecole de Design by Ekaterina Miroshnichenko et comme distributeur officiel de la marque E.Mi.</b> <br>
		 Le représentant officiel de l'École de Nail Design by Ekaterina Miroshnichenko a un droit préférentiel de conclure un accord sur la distribution de la marque E.Mi sur le territoire fixé dans le contrat de la représentation officielle de l'École de design. Dans ce cas, les exigences de la représentation officielle sont ajoutées aux termes et aux conditions de la coopération avec les obligations des distributeurs sur les montants des achats et une augmentation correspondante de la taille des réductions sur les produits E.Mi.
	</p>
	<p>
 <b>Les exigences pour l'ouverture du bureau de représentation de l'École de Nail Design by Ekaterina Miroshnichenko:</b>
	</p>
	<ol>
		<li>Instructeur de talent, parfaitement capable de reproduire la technique unique de l'auteur. D'abord le candidat passe la formation initiale comme apprenti et dans le cas de respect des normes professionnelles, approuvées par Ekaterina Miroshnichenko, il passe la formation spéciale de l'instructeur.</li>
		<li>Disposer d'un espace séparé pour le centre de formation d'au moins 16 mètres carrés. Le placement de l'enseigne, en conformité avec le style de l'entreprise de l'École de Nail Design by Ekaterina Miroshnichenko.</li>
		<li>La présence d'une personne morale habilitée à fournir des services éducatifs.</li>
		<li>Le fonds de roulement pour l'achat des produits pour équiper le centre de formation et pour le paiement des instructeurs.</li>
		<li>Le représentant devra mettre en œuvre les moyens de manière à attirer les élèves dans son centre de formation. </li>
	</ol>
	<p>
	</p>
	<p>
 <b>Chère collègue ! Si vous avez décidé d'ouvrir un bureau de représentation de l'Ecole de Nail Design by Ekaterina Miroshnichenko dans votre région, au commencement vous avez besoin de:</b>
	</p>
	<ol>
		<li>Vérifier s'il y a un bureau dans votre région. Pour cela, contactez-nous par mail ou au&nbsp;71.96.15.58. Si la représentation de l'école existe déjà dans votre région , nous serons obligés de vous refuser.</li>
		<li>Soumettre des photos des capsules avec le design d’Ekaterina Miroshnichenko par email <a href="mailto:tunisia-school@emischool.com">tunisia-school@emischool.com</a> (par exemple, «les fleurs de l’auteur de Ekaterina Miroshnichenko», «les drapés de tissu», «fraise», «framboise», «cerises» etc. Il est nécessaire pour l'approbation de votre candidature en tant qu'instructeur parce que vos designs doivent se conformer pleinement aux exigences d'Ekaterina Miroshnichenko. Vous devez reproduire exactement son design et sa technologie.</li>
		<li><b>Apprendre le coût de la formation des formateurs au:&nbsp;+216 71.96.15.58. </b> Elle se compose de deux étapes:<br>
		 1) Se former en qualité d’élève dans notre école E.Mi School France.<br>
		 2) Se former en qualité de formateur dans la maison mère d’E.Mi School (Rostov sur le Don en Russie). Le coûp dépend du nombre de cours. </li>
		<li><b>Vérifier le nombre d'achats au cours du trimestre au:&nbsp;+216 71.96.15.58. </b> Le volume des achats est fixé par écrit dans le contrat.</li>
		<li>Être prêt pour une formation régulière. Le contrat est valable 1 an. Chaque année l'instructeur doit confirmer personnellement sa compétence à Rostov-sur-le Don et conclure un contrat pour l'année suivante.</li>
	</ol>
	<p>
	</p>
	<p>
 <b>Pour plus d'informations sur l'ouverture de la représentation de l'École de Nail Design, vous pouvez vous adresser au bureau principal: <br>
		 IMM. INES, Rue du Lac Léman, bureau 8, Les Berges du Lac, 1053 Tunis</b>
	</p>
 <br>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>