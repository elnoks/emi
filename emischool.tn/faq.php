<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Tips & tricks");
?> 
<div class="bx_page"> 
<div class="h1-top">
		<h1>Tips & tricks</h1>
    </div>
 <br>
  <p> <b>Comment devenir un représentant officiel de la marque E.Mi et de l'École de Nail Design by Ekaterina Miroshnichenko?</b> 
    <br />
La société E.Mi offre des solutions complètes pour la création d'une entreprise passionnante et de croissance rapide. Vous pouvez ouvrir un bureau de représentation officielle de l'École de Nail Design by Ekaterina Miroshnichenko dans votre ville, devenir distributeur officiel de la marque E.Mi ou son représentant de gros.  Vous pouvez trouver des informations  détaillées dans la section <a href="/social/open_school.php" >“Comment ouvrir un bureau de représentation de l'école”</a></p>
 
  <p> <b>Comment peut-on savoir  s'il y a une école de nail design dans ma ville?</b> 
    <br />
On peut trouver l'École de Nail Design by Ekaterina Miroshnichenko dans votre ville dans la section  <a href="/contacts/" >“Contacts”</a>. </p>
 
  <p> <b>Ou peut-on acheter des produits E.Mi?</b> 
    <br />
Vous pouvez acheter les produits E.Mi sur notre site dans la rubrique «Produits». Vous pouvez voir des instructions détaillées sur la façon de commander dans la section «Comment commander» ou au point de vente de la marque E.Mi. Vérifiez s'il y a un point de vente de la marque E.Mi dans votre ville dans la section <a href="/contacts/" >“Contacts”</a>. </p>
 
  <p> <b>Est-il possible d'apprendre les spécialités de base (manucure, pédicure, styliste de formation et de modélisation des ongles) à l'École de Nail Design by Ekaterina Miroshnichenko?</b> 
    <br />
The nail design school by Ekaterina Miroshnichenko specializes in nail design only.</p>
 
  <p> <b>Est-il possible d'être formé à l'École de Nail Design  by Ekaterina Miroshnichenko, n'ayant aucune expérience d'un maître?</b> 
    <br />
Le cours de nail design est le développement professionnel, donc nous recommandons  premièrement de passer la formation dans les spécialités de base (manucure, pédicure, styliste de formation et de modélisation des ongles). Ainsi, il vous sera plus facile d'apprendre le Nail Design.</p>
 
  <p> <b>Par quel cours faut-il commencer la formation?</b> 
    <br />
Nous vous recommandons de commencer la formation par  les cours de base «Peinture d'art ABC lignes» et «Technologie E.Mi Design». Suivant ces cours, vous apprendrez les bases de travail avec les gels de couleurs et gestes et technique de dessin sur les ongles. Après cela , il vous sera beaucoup plus facile de passer des cours de formation de deuxième et de troisième degré de complexité tels que «la peinture Zhostovo», «la peinture chinoise», «floristique complexe», «abstraction complexe»   etc.</p>
 
  <p> <b>Ou peut-on trouver un horaire de cours à l'École de Nail Design by Ekaterina Miroshnichenko dans ma ville?</b> 
    <br />
Ou peut-on trouver un horaire de cours à l'École de Nail Design by Ekaterina Miroshnichenko dans ma ville? <a href="/courses/#schedule" >“Horaire”</a>. </p>
 
  <p> <b>Peut-on appliquer les peintures de gel E.Mi aux vernis de gel d'autres marques?</b> 
    <br />
Les peintures de gel E.Mi peuvent être appliquées au vernis de gel mais il est important de se rappeler que la majorité du gel vernis est enlevé à l'aide du trempage dans un liquide spécial.</p>
 
  <p> <b>Peut-on appliquer des peintures de gel E.Mi sur l'ongle naturel?</b> 
    <br />
Avant d'appliquer la peinture de gel E.Mi,  la plaque de l'ongle doit être recouverte d'un matériau artificiel: gel, acrilique ou gel vernis. On ne peut pas appliquer la peinture de gel E.Mi sur l'ongle naturel.</p>
 
  <p> <b>Quelle est la différence entre les peintures de gel E.Mi et EMPASTA?</b> 
    <br />
La principale difference est la consistence et la présence (ou l'absence) de collant résiduel. Empasta est plus épaisse, ainsi  un frottement ou une ligne restent plus forts. La cohérence d’EMPASTA vous permet de créer des dessins en trois dimensions. En outre EMPASTA n'a pas de collant résiduel, de sorte qu'elle peut être appliquée comme un gel protecteur sur ou sous le gel de finition. Le séchage intermédiaire pour EMPASTA E.Mi One stroke dure seulement 2-5 secondes, ce qui rend plus facile à effectuer le design avec un grand nombre d'éléments. Les peintures de gel E.Mi ont une consistance plus liquide, elles sont idéales pour la création des dessins. </p>
 
  <p> <b>Quelle est la différence entre un polymère «pierres liquides» et un gel «pierres liquides»?</b> 
    <br />
Gel «pierres liquides» est un matériel supplémentaire pour créer une technique de design «pierres liquides». Il vous permet de créer des imitations de pierres précieuses sur les ongles de toutes tailles et hauteur en une seule couche. Il n'y a pas de nécessité d'appliquer un gel de protection ce qui réduit le temps de créer un design. Le Gel «pierres liquides» n'a pas de couleur, et il ne peut pas être mélangé avec des pigments. Le Polymère «pierres liquides» vous permet de créer l'imitation des pierres de couleurs car il peut être mélangé avec des pigments. Il exige l'application d'un gel protecteur avec un collant résiduel.</p>
 
  <p> <b>Comment utiliser la peinture de gel EMPASTA «Openwork Silver» pour  le 3 D vintage?</b> 
    <br />
EMPASTA «Openwork Silver» comprend une fraction transparente, donc pressez une petite quantité de peinture dans une palette et bien mélangez, puis laissez reposer pendant quelques jours pour qu'elle acquiesce une consistance épaisse appropriée au 3D vintage. L’EMPASTA stocké est  meilleur. EMPASTA ne sèche pas dans la pallette, conserve la consistance épaisse et convient parfaitement pour le travail.</p>
 
  <p> <b>Quels pinceaux sont nécessaires lorsque l'on travaille avec les matériaux E.Mi?</b> 
    <br />
Pour travailler avec des matériaux E.Mi on recommande trois paires de pinceaux : «ensemble de pinceaux pour la peinture artistique», «jeu de pinceaux pour la peinture chinoise» et «ensemble universel de pinceaux».</p>
 
  <p> <b>Comment prendre soin de pinceaux?</b> 
    <br />
Pour que les pinceaux maintiennent leurs propriétés aussi longtemps que possible, il ne faut pas les nettoyer avec un liquide contenant de l'acétone, il suffit de les essuyer avec un chiffon humide. Il faut les protéger également des rayons ultraviolets.</p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>