<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Qui sommes nous?");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Qui sommes nous?</h1>
   </div>
 
  <div class="bx_page" style="text-align: center;"><img src="/upload/medialibrary/f7c/f7cb52963256ebe4b40f9af5c03a36d2.jpg" title="56.jpg" border="0" alt="56.jpg" width="1140" height="300"  /> 
    <br />
   </div>
 
  <br />
 
  <p>La société E.Mi vous propose des milliers de solutions des designs toutes prêtes uniques et exclusives, développées par la championne du monde dans le Nail-art - Ekaterina Miroshnichenko, et des matériaux uniques avec des accessoires pour la peinture décorative sur les ongles développés avec son implication personnelle.</p>
 
  <p>Notre mission est d'aider chaque maître à libérer sa créativité, se sentir un artiste et devenir un véritable expert de la mode.</p>
 
  <p>La société E.Mi est une entreprise prospère, en croissance rapide, dont chacun pourrait faire partie ! A l'heure actuelle ils existent nombreuses représentations officielles de l'Ecole de Nail Design by Ekaterina Miroshnichenko en Russie, en Europe, en Asie et en Afrique, des distributeurs officiels de la marque E.Mi et des centaines de points de vente à  travers le monde.</p>
 
  <h2>READY-MADE SOLUTIONS&amp;NAIL&amp;FASHION</h2>
 
  <p><img src="/upload/medialibrary/cd4/cd48dcf71294554d8839eab0a34a4986.jpg" title="98.jpg" border="0" alt="54.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>La haute couture est un véritable art. Elle fascine,  inspire et fait attendre  la nouvelle saison qui surprend toujours.</p>
 
  <p>Les solutions toutes prêtes de E.Mi sont des designs de haute couture. Durant l'année nous vous présentons quelques collections exclusives, développées par une championne du monde de Nail Design dans la catégorie «Fantaisie» de version OMS (Paris-2010), double champion d'Europe (Athènes, Paris-2009), juge international et fondateur de l'École de l'auteur de design des ongles Ekaterina Miroshnichenko.</p>
 
  <p>Chacune de ses collections est l'interprétation de l'auteur des tendances les plus actuelles, qui ont été traduites dans la langue de nail design et adaptées aux performances de salon. Incroyablement belles et uniques à première vue, elles vont faire succomber chacun de vous, même s'il n'a pas d'éducation artistique après le passage du cours de formation ou master vidéo-classe.</p>
 
  <p>Parmi les collections il y a  des best-sellers comme «peinture Zhostovo», «peinture chinoise», «imitation de peau de reptile», «effet de craquelure», «imprimés ethniques», «sable de velours et des pierres liquides» et d'autres. Inspirez-vous de la collection complète dès maintenant !</p>
 
  <h2>LA PRODUCTION</h2>
 
  <p><img src="/upload/medialibrary/6e3/6e3b48a3e4405516b24de034f7ca5b60.jpg" title="6542.jpg" border="0" alt="6542.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Ekaterina Miroshnichenko a participé personnellement à la création de chaque produit, des nouvelles couleurs et des accessoires. Les produits répondent à toutes les exigences des nail-designers, prennent  en compte toutes les nuances de l’œuvre, permettent de réduire le temps pour créer le modèle et offrent des possibilités  illimitées pour la créativité.</p>
 
  <p>Nous présentons les produits uniques comme EMPASTA, GLOSSEMI, Textone, PRINCOT, ainsi que des collections de peintures de gel, foils, conception des couleurs les plus pertinentes, une grande variété de matériaux pour la décoration des ongles et les accessoires.</p>
 
  <h2>L'ÉCOLE DE NAIL DESIGN BY EKATERINA MIROSHNICHENKO</h2>
 
  <p><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" border="0" alt="765.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Si vous rêvez d'un métier passionnant et créatif, si vous voulez que vos clients aient un sourire reconnaissant, devenez propriétaire d'un savoir-faire exclusif à l’École de Nail Design by Ekaterina Miroshnichenko.</p>
 
  <p>Elle propose des programmes éducatifs uniques de l'auteur pour les concepteurs des ongles, élaborés spécialement pour les maîtres qui ne disposent pas d'une éducation artistique.</p>
 
  <p>Les cours sont divisés en trois degrés de complexité ainsi que des cours supplémentaires. Pour ceux qui cherchent à conquérir l’Olympe professionnel et participer à des compétitions de nail-art, nous proposons des cours spéciaux de formation compétitive. Les élèves de l'école sont plusieurs fois gagnantes des concours régionaux et nationaux de nail design.</p>

 
  <p>Choisissez le programme de formation en ce moment sur notre site!</p>
 
  <h2>LES REPRESENTANTS OFFICIELS</h2>
 
  <p><img src="/upload/medialibrary/060/060926d856ef3ef2bcc0eef18fa948cc.jpg" title="7645.jpg" border="0" alt="7645.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>La marque E.Mi et l'École de Nail Design by Ekaterina Miroshnichenko deviennent de plus en plus populaires et ont une demande internationale. À l'heure actuelle,  les représentations officielles fonctionnent avec succès en Russie, en Ukraine, au Kazakhstan, en Biélorussie, en Italie, au Portugal, en Roumanie, en Chypre, en Allemagne, en France, en Lituanie, en Slovaquie, en Corée du Sud et aux Émirats Arabes Unis. Les produits E.Mi peuvent être achetés dans le monde entier.</p>
 
  <p>Vous pouvez également faire partie d'une marque internationale couronnée de succès et obtenir une solution d'affaires toute prête: devenir un représentant officiel ou un distributeur de la marque dans votre région ou pays.  Les responsables ont des avantages suivants: 
<ul> 
	<li>
la possibilité de représenter exclusivement les écoles de la marque dans leur région;
	</li>
	<li>
le droit des cours de l'auteur sur le design des ongles;
	</li>
	<li>
les processus d'affaires complexes pour l'ouverture et le développement de l'entreprise;
	</li>
	<li>
un support complet pour la sélection, la formation du personnel, le soutien de la publicité fédérale;
	</li>
	<li>
des solutions efficaces pour l’achat de la gamme de produits E.Mi.
	</li>
</ul></p>
 
  <p>Pour devenir un représentant officiel ou un distributeur de la marque, vous pouvez remplir un formulaire sur le site Web ou en téléphonant (216) 55025522, (216) 71961558, e-mail <a href="mailto:tunisia-sale@emischool.com">tunisia-sale@emischool.com</a></p>

  <p>
    <br />
  </p>

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>