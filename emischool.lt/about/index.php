<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Apie kompaniją");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Apie kompaniją</h1>
   </div>
 
  <div class="bx_page" style="text-align: center;"><img src="/upload/medialibrary/f7c/f7cb52963256ebe4b40f9af5c03a36d2.jpg" title="56.jpg" border="0" alt="56.jpg" width="1140" height="300"  /> 
    <br />
   </div>
 
  <br />
 
  <p>E.Mi kompanija – tai tūkstančiai aktualių ir unikalių nagų dizaino variantų, kuriuos kuria Pasaulio ir Europos čempionė nagų dizaino kategorijoje Jekaterina Mirošničenko. Tuo pačiu tai ir unikalūs produktai bei aksesuarai minėtiems dizainams kurti, kurių atsiradimą tiesiogiai lemia talentingoji Jekaterina Mirošničenko.</p>
 
  <p>Mūsų misija – padėti kiekvienam nagų meistrui atskleisti savo kūrybinį potencialą, nors trumpam pasijusti menininku ir tapti tikru mados ekspertu.</p>
 
  <p>E.Mi kompanija– tai žaibišku greičiu besivystantis sėkmingas verslas, kurio dalimi gali tapti kiekvieas! Šiuo metu tai 50 oficialių Jekaterinos Mirošničenko nagų dizaino mokyklos atstovybių Rusijoje, Europoje ir Azijoje ir oficialių prekybinių partnerių, bei šimtai E.Mi  pardavimo vietų visame</p>
 
  <h2>Pilnai išbaigti sprendimai NAIL&FASHION</h2>
 
  <p><img src="/upload/medialibrary/cd4/cd48dcf71294554d8839eab0a34a4986.jpg" title="98.jpg" border="0" alt="54.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Aukštoji mada – tai tikrasis menas. Jis užburia, įkvepia eksperimentuoti ir priverčia su nekantrumu laukti sekančio sezono, kuris būtinai nustebins ir pateiks netikėtų siurprizų.</p>
 
  <p>Išbaigti sprendimai su E.Mi – tai aukštoji mada nagų dizaine. Metų eigoje mes pristatome kelias ekskliuzyvias nagų dizaino kolekcijas, kurias kuria talentingoji čempionė Jekaterina Mirošničenko. Jos pasiekimų saraše Pasaulio čempionės titulas 2010 m. vykusiame Pasauliniame konkurse Paryžiuje (nominacija ,,Fantasy‘‘), du kart Europos čempionės titulas (konkursuose Atėnuose ir Paryžiuje 2009 m.). Ji pati teisėjauja tarptautiniuose konkursuose ir yra E.Mi kompanijos įkūrėja ir kūrybinė siela.</p>
 
  <p>Kiekviena Jekaterinos kolekcija – tai atitinkamo sezono aukštosios mados nuotaika, spalvos, elementai, perleisti per asmeninio požiūrio prizmę ir ,,išversti’’ į nagų dizaino kalbą. Iš pirmo žvilgsnio gali pasirodyti, kad jie labai sudėtingi ir sunkiai atkartojami, bet tai tik išpirmo žvilgsnio! Jekaterinos Mirošničenko mokymų unikalumas ir slypi tame, kad, atrodytų, patys sudėtingiausi dizainai yra nesunkiai įvaldomi, nes instruktorius, mokydamas pagal jos sukurtas programas,  kiekvieną dizainą  išskaido į aiškius išpildymo žingsnelius, kuriuos pakartoti tikrai paprasta ir pradedančiam meistrui.</p>
 
  <p>Autorinių programų kolekciją sudaro ,,Žostovo tapyba’’, ,,Kinietiškadailėsu EMPASTA’’, ,,Reptilijų odos imitacija’’, ,,Krakeliūras’’, ,, Etniniai printai’’, ,,Skysti akmenys ir aksominis smėlis’’ ir kitos.</p>
 
  <h2>PRODUKTAI</h2>
 
  <p><img src="/upload/medialibrary/6e3/6e3b48a3e4405516b24de034f7ca5b60.jpg" title="6542.jpg" border="0" alt="6542.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p> Kiekvieno naujo produkto, naujos spalvos ar aksesuaro kūrime Jekaterina Mirošničenko dalyvauja asmeniškai. Produktai atitinka griežčiausius nagų dizaino meistrų reikalavimus ir darbo specifiką, leidžia taupyti laiką kuriant piešinį ir suteikia neribotą laisvę fantazijai ir kūrybai.</p>
 
  <p>Mes turime tokius unikalius produktus kaip EMPASTA, GLOSSEMI, TEXTONE, PRINCOT, o taip pat gelinių dažų kolekcijas, pačių aktualiausių spalvų blizgias metalines folijas, įvairiausius priedus ir aksesuarus nagų dekoravimui. Susiraskite Jums reikalingą produktą jau dabar.</p>
 
  <h2>JEKATERINOS MIROŠNIČENKO NAGŲ DIZAINO MOKYKLA</h2>
 
  <p><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" border="0" alt="765.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Jeigu Jūs svajojate apie įdomią ir kūrybišką profesiją, norite, kad klientės atsisveikintų sužavėtos, o grįžtų nekantraudamos, pradėkite meistriškumo kelią mūsų mokykloje. </p>
 
  <p>Jekaterinos Mirošničenko nagų dizaino programų, pradedant nuo pirmų nedrąsių žingsnių mokantis piešimo pradmenų, baigiant specialiais seminarais norinčioms išbandyti savo jėgas konkursuose.Mūsų mokyklos instruktorės pasižymi neišsemiama kantrybe ir mokėjimu išmokyti Kiekvienais metais jos kelia savo kvalifikacija E.Mi mokymų centre Rostove ir laiko kvalifikacinį egzaminą pas Jekateriną Mirošničenko.</p>
 
  <p>Nedelskite ir jau dabar išsirinkite mokymų programą!  </p>

  <p>
    <br />
  </p>

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>