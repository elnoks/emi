<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Patarimai");
?> 
<div class="bx_page"> 
<div class="h1-top">
		<h1>Patarimai</h1>
    </div>
 <br>
 
  <p> <b>Kur yra nagų dizaino mokykla Lietuvoje?</b> 
    <br />
Jekaterinos Mirošničenko nagų dizaino mokyklos kontaktus galite rasti skiltyje  <a href="/contacts/" >"Kontaktai"</a>. </p>
 
  <p> <b>Kur aš galiu įsigyti E.Mi produkcijos?</b> 
    <br />
Jūs galite įsigyti E.Mi produktus mūsų svetainėje – kategorija “Poduktai” (detalesnė informacija - skiltyje”Kaip užsakyti”) arba prekybos vietose. Daugiau informacijos apie pardavimo taškus rasite skiltyje  <a href="/contacts/" >“Kontaktai”</a>. </p>
 
  <p> <b>Ar man tinka E.Mi mokymai, jei neturiu manikiūro specialistės patirties?</b> 
    <br />
Nagų dizaino mokymai yra skirti kvalifikacijos kėlimui, todėl pirmiausia mes Jums rekomenduotume baigti bazinius mokymus (manikiūro, pedikiūro specialybės). </p>
 
 
  <p> <b>Nuo kurių mokymų geriausiai pradėti?</b> 
    <br />
Mes rekomenduotume pirmiausia pradėti nuo bazinių nagų dizaino kursų: Linijų ABC, Technologijos, Aparatinis kombinuotas ir saloninis manikiūras. Po šių mokymų bus lengviau mokytis antro ir trečio lygio seminaruose – Žostovo tapyba, Kinietiška dailė, Floristika, Vaisiai ir pan.</p>
 
  <p> <b>Kur aš galiu rasti seminarų grafiką?</b> 
    <br />
Daugiau informacijos paie mokymus rasite kategorijoje  <a href="/courses/#schedule" >“Seminarai”</a>. </p>
 
  <p> <b>Ar galima su E.Mi geliniais dažais dengti natūralų nagą?</b> 
    <br />
Norint dengiant natūralius nagus su E.Mi geliniais dažais, nago plokštelę reikia sutvirtinti su geliu, akrilu arba geliniu laku. Dengtį natūralaus nago tik su geliniais dažais negalima.</p>
 

 
  <p> <b>Kuo skiriasi E.Mi geliniai dažai ir EMPASTA?</b> 
    <br />
Pagrindiniai skirtumaitai yra tirštumas ir lipnumas. E.Mi EMPASTA yra tirštesnė, todėl linijos su ja piešiant yra plonesnės ir tikslesnės, EMPASTOS tekstūra leidžia sukurti iškilų 3D efektą. Taip pat EMPASTA taip pat neturi lipnaus sluoksnio, todėl su ja galima piešti ant nago kuris jau yra padengtas Finish geliu, o jei nepatinka iškilumas – galima uždengti Finish geliu. Piešiant kinietiškas gėles tarpinius sluoksnius užtenka džiovinti lempoje 2-5 s, o tai pagreitina sudėtingų elementų piešimo laiką. Geliniai dažai yra skystesni, todėl jie idealiai tinka ornamentams piešti.</p>
 
  <p> <b>Kuo skiriasi Akmenų gelis nuo Akmenų polimero?</b> 
    <br />
Akmenų gelis yra išskirtinė priemonė sukurti skystiems akmenims. Jo pagalba galite sukurti bet kokio dydžio akmenį ir jo nereikia perdengti topu, o tai keidžia taupyti laiką. Akmenų gelis yra permatomas, todėl į jį negalima maišyti piigmentų. Akmenų polimeras skirtas sukurti spalvotiems akmenimis, nes į jį galima maišyti  pigementus ir jį reikėtų padengti topu.</p>
 
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>