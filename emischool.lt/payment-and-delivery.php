<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Apmokėjimas ir pristatymas");
?>
    <div class="bx_page">
        <div class="h1-top">
            <h1>Apmokėjimas ir pristatymas</h1>
        </div>
    </div>

 <br>
 <br>
<p>
	 <h3>1. Kainos.</h3>
</p>
<p>
	 1.1 Kainos nurodytos su PVM.
</p>
<p>
	 1.2 Į kainą neįeina pristatymo išlaidos.
</p>
<p>
</p>
<p>
    <h3>2. Apmokėjimas.</h3>
</p>
<p>
	 2.1 Po užsakymo gavimo ir apdorojimo, vadybininkas į Jūsų elektroninį paštą atsiųs išankstinio apmokėjimo sąskaitą, kurią Jūs turite apmokėti. Jei norite pasirinkti kitą atsiskaitymo būdą informuokite apie tai vadybininką telefonu arba el. paštu.
</p>
<p>
	 2.2 Galimas atsiskaitymas kurjeriui grynaisiais prekių pristatymo metu.
</p>
<p>
	 2.3 Galite atsiskaityti per Paypal.
</p>
<p>
</p>
<p>
    <h3>3. Prekių pristatymas.</h3>
</p>
<p>
	 3.1 Pristatymas, kurių bendra užsakymo suma yra mažesnė nei 40 EUR, kainuoja 2,90 EUR *, užsakymams, kurių suma viršija 40 EUR - pristatymas nemokamas!
</p>
<p>
	 3.2 Prekės pristatomos nurodytu adresu per 2-3 darbo dienas. Informuojame, jog išimtinais atvejais prekių pristatymas gali vėluoti dėl nenumatytų, nuo Pardavėjo nepriklausančių aplinkybių. Tokiu atveju Pardavėjas įsipareigoja nedelsiant susisiekti su Pirkėju ir suderinti prekių pristatymo klausimus.
</p>
 <br>
 <br>

<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>