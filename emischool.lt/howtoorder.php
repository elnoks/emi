<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Kaip užsakyti");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Kaip užsakyti.</h1>
   </div>
 </div>
 
<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

 
<div class="bx_page"> 
  <p>1.	Kad būtų išvengta su pristatymu susijusių problem, prašome Jums pateikti tikslius savo duomenis - vardą ir pavardę, mobiliojo telefono numerį, pašto kodą, elektroninio pašto ir pristatymo adresą.
    <br />
   </p>
 
  <p>2.	Įmonė neatsako už papildomas išlaidas, susijusiais jei Jūs nurodėte neteisingą adresą.
    <br />
   </p>
 
  <p>3. Mes priimame ir vykdome užsakymus Lietuvos teritorijoje. Užsakymus iš kitų šalių mes persiuniame į centrinį ofisą Prahoje.
    <br />
   </p>
 
  <p>4.	Gavę apmokėjimą, prekes  pristatysime per 2-3 darbo dienas. Informuojame, jog išimtinais atvejais prekių pristatymas gali vėluoti dėl nenumatytų, nuo Pardavėjo nepriklausančių aplinkybių. Tokiu atveju Pardavėjas įsipareigoja nedelsiant susisiekti su Pirkėju ir suderinti prekių pristatymo klausimus.</p>
 
  <p>5.	Negavus apmokėjimo per 5 daro diena, užsakyma yra atšaukiamas.
    <br />
   </p>
 
  <p>6.	Jei norite pasirinkti kitą apmokėjimo už prekes būdą, susisiekite su mumis tel. +370 656 67155 arba info@salonpro.lt
    <br />
   </p>
  <p>7.	Prieš apmokėdami sąskaitą pasitikrinkite ar produktai ir jų kiekiai yra teisingai nurodyti.
    <br />
   </p>

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>