<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("How to open the E.Mi school representative office");
?> 
<div class="bx_page"> 
  <p style="text-align: center;"><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" border="0" alt="765.jpg" width="800" height="105"  /><b>
      <br />
    </b></p>

  <p><b>
      <br />
    </b></p>

  <p> <b>Koostöö Ekaterina Mirošnitšeko Küünedisaini Kooliga on võimaluseks saada endale töötav ja stabiilselt tulutoov ärilahendus. Tehes meiega koostööd saate endale täies ulatuses tehnikate valiku: alates disainist kuni juhtimise ja arendamiseni. Ekaterina Mirošnitšenko autorikooli küünedisainid on alati aktuaalsed, klientide poolt nõutud ning stiilsed ja äratuntavad!</b></p>
 
  <p> <b>Olge meie ametlikeks piirkondlikeks esindajateks ning õpetame teile, kuidas olla sama edukad ja loovad! </b>Tänasel päeval on Ekaterina Mirošnitšenko Küünedisaini Kool tuntud mitte ainult Venemaal, vaid kogu maailmas: prestiižsed auhinnad rahvusvahelistelt konkurssidelt, seminarid Saksamaal ja Itaalias, avatud rahvusvahelised kontorid Euroopas. 
    <br />
   Te võite saada endale eksklusiivse õiguse juhtida enda piirkonnas tulutoovat ja huvitavat äri, mis tegeleb küüneteenuste spetsialistide õpetamise ja nende kvalifikatsiooni tõstmisega brändinime „Ekaterina Mirošnitšenko Küünedisaini Kooli“ all.</p>
 
  <p> <b>Liituge nendega, kes on alati trendikad!</b> Ekaterina Mirošnitšenko Küünedisaini Kool on loonud mitte ainult unikaalseid äratuntava disainiga tehnoloogiaid, vaid ka ainulaadseid õpetustehnikaid. 


    <br />
   <b>Me muudame loomingulisuse ja ilu loomise rõõmu kättesaadavaks kõigile!</b>
    <br />
   <b>„Ekaterina Mirošnitšenko Küünedisaini Kooli“ eelised: </b> </p>

  <ul> 
    <li>Haridusteenuste nõutust kinnitab see, et meie õpekeskuses (asub Rostovis Doni ääres) läbib igal aastal erinevate programmide alusel koolituse üle 500 meistri kõikidest Venemaa regioonidest ja välismaalt.</li>
   
    <li><a href="/catalog/" >E.Mi</a>Õpet viiakse läbi kasutades unikaalse brändi „E.Mi“ tooteid.</li>
   
    <li>Ettevõte rahvusvaheline tase. Kontor Euroopas (Prahas).</li>
   
    <li>Autorikooli õppeprogrammide aluseks on unikaalsed metoodikad, mida on väljatöötanud ja patenteerinud Ekaterina Mirošnitšenko, kes on MLA küünedisaini kategooria maailmameister (Pariis, 2011).</li>
   </ul>
 
  <p></p>
 
  <p> <b>Tagame esindusele kõik vajaliku õpeosakonna „E. Mirošnitšenko Küünedisaini Kool“ eduka töö jaoks.
      <br />
     Kui avate Küünedisaini Kooli ametliku esinduse, te saate:</b> </p>

  <ol> 
    <li>Eksklusiivse õiguse tegutseda Ekaterina Mirošnitšenko Küünedisaini Autorikooli brändi all, pakkudes haridusteenuseid territooriumil, mis on ära näidatud lepingus. Venemaa Föderatsiooni ühes piirkonnas (näiteks oblast, autonoomne piirkond) saab olla avatud vaid üks esindus.</li>
   
    <li>Võimaluse kasutada juba valmis õppeprogramme, mis on välja töötatud Ekaterina Mirošnitšenko poolt. Sellisel viisil saab regionaalne Õppekeskus säästa suurel hulgal aega ja vahendeid, mis oleksid kulunud tundide läbiviimise metoodikate loomise ning õpeplaanide ja -materjalide väljatöötamise peale. See lubab suunata peamised jõud kuulajaskonna suurendamisele ning samal ajal teenida lühema aja jooksul tasa Esinduse avamisega seotud kulutusi.</li>
   
    <li>Kuuluvuse ühtsesse sertifitseerimissüsteemi. Esindus saab omale õiguse väljastada enda õpilastele ja kuulajatele firma sertifikaate ”Ekaterina Mirošnitšenko Küünedisaini Kooli” hologrammigas.</li>
   
    <li><b>Õiguse saada eksklusiivset soodustust  <a href="/catalog/" >E.Mi</a>toodetele. </b></li>
   
    <li>Reklaami ja infolevi tuge. Autorikool avaldab kogu info enda esinduste kohta juhtivates föderaalajakirjades ning ka enda ametlikul veebileheküljel. Aitab läbi viia piirkondlikke näitusi ja nail-konverentse, annab reklaammakette nende avaldamiseks piirkondlikes väljaannetes. </li>
   </ol>
 
  <p></p>
 
  <p>Ekaterina Mirošnitšenko Küünedisaini Kooli esindusena ja samal ajal E.Mi kaubamärgi ametliku edasimüüjana tegutsemise eripärad.</b>
    <br />
   Ekaterina Mirošnitšenko Küünedisaini Kooli ametlikul esindajal on õigus sõlmida E.Mi kaubamärgi edasimüügi lepingut, mis lubab müüa E.Mi tooteid territooriumil, mis on kinnitatud Küünedisaini Kooli ametliku esinduse lepingus. Sellisel juhul Ekaterina Mirošnitšenko Küünedisaini Kooli ametliku esindaja nõuetele lisanduvad ka edasimüüja nõuded ja koostöötingimused, ostusummade kohustused vastava soodustuse suurenemisega E.Mi toodetele.</p>
 
  <p> <b>E. Mirošnitšenko Küünedisaini Kooli esinduse avamise tingimused:</b> </p>

  <ol> 
    <li>Talendikas instruktor, kes on võimeline täielikult kopeerima Ekaterina Mirošnitšenko tehnikat. Kandideerija läbib esmase õppe õpilasena ning juhul, kui ta vastab professionaalsetele nõuetele, mis on kinnitatud Ekaterina Mirošnitšenko poolt, läbib ta ka spetsiaalse instruktori ettevalmistuskursuse. </li>
   
    <li>Eraldiseisev ruum pindalaga vähemalt 16 m2 õppekeskuse jaoks. Firmasildi paigutus vastavalt E. Mirošnitšenko Küünedisaini Kooli firmastiilile.</li>
   
    <li>Juriidiline isik, kellel on õigus pakkuda haridusteenuseid (FIE, eraõppeasutus vms).
Käibevahendid esimaste toodete soetamiseks, õppekeskuse varustamiseks ja Instruktori koolituse eest maksmiseks. 
</li>
   
    <li>Esindaja valmisolek viia läbi töid õpilaste ligitõmbamiseks, kasutades läbitöötatud tehnoloogiaid. 
Lugupeetud meistrid! Kui te olete otsustanud, et tahate avada Kooli esinduse oma piirkonnas, siis vajate: 
</li>
   
    <li>Teada saada, kas teie piirkonnas ei ole juba esindust. Seda võib teha „Kontaktide“ all või helistades numbril +7 906 427-25-86. Kui teie piirkonnas on juba olemas kooli esindus, siis oleme sunnitud teile ära ütlema.</li>
   </ol>
 
  <p></p>
 
  <p> <b>Saata fotod tipsidest, millel on kujutatud Ekaterina Mirošnitšenko disaine, e-posti aadressil sviridova@emi-school.ru (näiteks „Autorililled ääristega“, „Kangakurrud“, „Maasikas“, „Vaarikas, „Kirsid“ jm). See on vajalik, et kinnitada teie kandidatuuri instruktori kohale, kuna teie käekiri peab täielikult vastama Ekaterina Mirošnitšenko nõuetele. Peate väga täpselt kopeerima tema disaini ja tehnikat.</b> </p>

  <ol> 
    <li>Instruktori koolituse hinna saab teada telefonil +7-906-427-25-86. Instruktoriõpet viiakse läbi kahes etapis: õpilase etapp ja instruktori etapp. Õpe viiakse läbi vaid Rostovi Doni ääres asuvas õpekeskuses! Hind sõltub täielikult kursuste arvust. </li>
   
    <li>Ostude hulka kvartali kohta saate teada telefonil +7-906-427-25-86. Ostude hulk kinnitatakse kirjalikult lepingus.</li>
   
    <li>Valmisolekut regulaarseteks kvalifikatsiooni taseme tõstmisteks. Instruktori leping sõlmitakse üheks aastaks. Instruktor peab iga aasta läbima isiklikult kvalifikatsiooni kinnituse Rostovis Doni ääres ning sõlmima lepingu järgmiseks aastaks.</li>
   
    <li>Et saada lisainfot Ekaterina Mirošnitšenko Küünedisaini Kooli esinduse avamise kohta, pöörduge peakontorisse, mis asub Rostovis Doni ääres, tel nr +7-906-427-25-86, e-post 
sviridova@emi-school.ru
</li>
   
    <li> </li>
   </ol>
 
  <p></p>
 
  <p> <b> 
      <br />
</b> </p>
 
  <br />
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>