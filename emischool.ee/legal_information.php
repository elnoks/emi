<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Юридическая информация");
?>
<div class="bx_page">
<div class="h1-top">
		<h1>Konfidentsiaalsuspoliitika</h1>
    </div>
 <br>
<h2>
Konfidentsiaalsuspoliitika
</h2>
<p>
1. Konfidentsiaalsuspoliitika ning Teie ja „E.Mi“ vahelised suhted, mis on seotud isikuandmete töötlusega, on reguleeritud Venemaa Föderaalseadusega  №152-ФЗ (27.07.2006) „Isikuandmetest“. 
</p>
<p>
2. Konfidentsiaalsuspoliitika kehtib isikuandmete, mida „E.Mi“ on saanud või võib saada Teie käest Registreerimisel ja/või Tellimuse vormistamisel Veebilehel ja mis on vajalikud „E.Mi“ jaoks, et täita oma kohustusi Teie poolt soetatavate Toodete ja/või Teie ligipääsu personaliseeritud Veebilehe teenuste juurde, suhtes.
</p>
<p>
3. Käesolevaga Te nõustute, et Teie poolt personaliseeritud Veebilehe Teenuste kasutamine tähendab Teie tingimusteta nõustumist Konfidentsiaalsuspoliitikaga ja selles väljatoodud isikuandmete töötluse tingimustega. Mõne tingimusega mittenõustumisel olete kohustatud viivitamatult lõpetama teenuste kasutamist ja lahkuma Veebilehelt. 
</p>
<p>
4. Te tunnistate, et Teie ”E.Mi’le” antud nõusolek isikuandmete töötlemiseks, laieneb ka kõigile „E.Mi“ partneritele ja tütarettevõtetele. 
</p>
<p>
5. Kasutaja personaalsed andmed, mida kogub ja töötleb „E.Mi“: 
</p>
<p>
5.1. Kohustuslikud personaalsed andmed, mida Te vabatahtlikult ja teadlikult annate Registreerimisel ja/või Tellimuse vormistamise Veebilehel, mis on vajalikud esiteks „E.Mi“-poolsete kohustuste täitmiseks Teie soetatavate Toodete suhtes, näiteks: registreeritava Kasutaja Nimi ja Perekonnanimi; Kontakttelefon; E-posti aadress; Aadress toodete kohaletoimetamiseks.
</p>
<p>
5.2. Mittekohustuslikud isikuandmed, mida Te vabatahtlikult ja teadlikult annate endast edasi oma äranägemise järgi (näiteks vanus, sugu, perekonnaseis jne) Registreerimisel veebilehele või järgnevalt kasutades mõnda personaliseeritud Veebilehe teenust.
</p>
<p>
5.3. Umbisikulised andmed, mida saadakse automaatselt, kui Te viibite mõnel Veebisaidi leheküljel, kasutades Teie seadme tarkvara, näiteks IP-aadress, cookied, info teie brauseri kohta (või muu programmi, millega saadakse ligipääsu Veebisaidi teenustele, kohta), ligipääsu aeg, päritava lehe aadress. 
</p>
<p>
6. „E.Mi“ ei kontrolli Teie esitatud andmete õigsust ega vii läbi nende aktuaalsuse kontrolli. Sellegipoolest „E.Mi“ lähtub sellest, et Te annate usaldusväärseid ja küllaldasi isikuandmeid, täites registreerimisvormi, ning toetate seda infot aktuaalses olekus. Kogu vastutuse valeandmete andmise eest kannate Teie isiklikult. 
</p>
<p>
7. „E.Mi“ kogub ja töötleb vaid neid isikuandmeid, mis on vajalikud personaliseeritud Veebilehe teenuste kasutamisel ja /või „E.Mi“ Toodete tellimisel Veebilehelt, nimelt selleks, et: </p>
<p>
7.1. võtta vastu, töödelda ja kohale toimetada Teie Tellimusi ; 
</p>
<p>
7.2. saada kätte ja töödelda Teie makseid; 
</p>
<p>
7.3. informeerida Teid Teie Tellimuse olekust elektroonsete ja SMS-teavituste vahendusel;
</p>
<p>
7.4. parandada Veebilehe ja seotud tugiteenuste töö kvaliteeti;
</p>
<p>
7.5. pakkuda Teile efektiivset kliendituge;
</p>
<p>
7.6. pakkuda Teile Veebisaidi personaliseeritud teenuseid;
</p>
<p>
7.7. suunata infot, mis on seotud Teie poolt personaliseeritud Veebisaidi teenuste kasutamisega;
</p>
<p>
7.8. parandada Veebisaidi teenuste tööd, nende kasutamise mugavust ning ka selleks, et väljatöötada uusi teenuseid tänu Teie hinnangutele jooksvate Veebisaidi teenuste kohta;
</p>
<p>
7.9. informeerida Teid läbiviidavatest „E.Mi“ üritustest ja kampaaniatest; 
</p>
<p>
7.10. läbi viia statistilisi ja muid uuringuid umbisikuliste andmete alusel. 
</p>
<p>
8. Teie isikuandmete suhtes säilitatakse nende konfidentsiaalsust, välja arvatud juhtudel, kui Te ise avaldate vabatahtlikult infot enda kohta piiramata isikute ringile.  
</p>
<p>
9. „E.Mi“ kaitseb Teie isikuandmeid vastavalt nõuetele, mis on esitatud sellise info kaitseks ning kannab vastutust taolise info turvaliste kaitsemeetodite kasutamise eest. 
</p>
<p>
10. Teie personaalsete andmete kaitseks, nende vastava kasutamise tagamiseks ja nende  juurde mittesanktsioneeritud ja/või juhusliku juurdepääsu ennetamiseks, kasutab „E.Mi“ vajalikke ja küllaldasi tehnilisi ja administratiivseid meetmeid. Teie poolt antud personaalseid andmeid säilitatakse serveritel, millele on piiratud juurdepääs ning mis asuvad kaitstud ruumides.
</p>
<p>
11. „E.Mi’l“ on õigus edastada Teie personaalseid andmeid kolmandatele isikutele järgnevatel juhtudel: 
</p>
<p>
11.1. Te olete ilmselgelt andnud enda nõusoleku taoliseks tegevuseks;
</p>
<p>
11.2. edastus on vajalik selleks, et Te saaksite kasutada mõnda kindlat Veebisaidi teenust või Lepingu, mille järgi Te olete kasusaaja, täitmiseks. Selle juures tagab „E.Mi“ Teie personaalsete andmete konfidentsiaalsust ning Teid teavitatakse sellisest edastusest; 
</p>
<p>
11.3. edastus on ettenähtud vene seadusandlusega seadusandluse poolt kindlaksmääratud protseduuri raames.
</p>
<p>
12. Teil on õigus igal hetkel muuta (uuendada, täiendada) Teie poolt antud personaalseid andmeid, kasutades personaalsete andmete muutmise funktsiooni.
</p>
<p>
13. Te võite igal hetkel nõuda enda Poolt avaldatud isikuandmete kustutamist, pöördudes „E.Mi“ poole. 
</p>
<p>
14. Te kinnitate oma täielikku vabatahtlikku nõustumist oma personaalsete andmete töötlusega, vajutades nupule „Konfidentsiaalsuspoliitikaga tutvunud“, vormistades tellimust Veebilehel. 
</p>
<p>
15. Teie nõustumine personaalsete andmete töötlemiseks kehtib selle aja jooksul, kui Te kasutate „E.Mi“ teenuseid. 
</p>
<p>
16. Kõik küsimused, mis on seotud personaalsete andmete töötlusega, võite suunata „E.Mi’sse“
</p>
<h2>
Juriidiline informatsioon
</h2>
<h3>
Autoriõigused
</h3>
<p>
Kõik õigused kaitstud. Autoriõigused kuuluvad „E.Mi’le“ (Ekaterina Mirošnitšenko) või „E.Mi“ partneritele. Kõik tekstid, fotod, joonistused, heli- ja videofailid, animatsioonid ning nende kompositsiooniline vormistus on autoriõigustega ja kehtiva  intellektuaalomandi kaitse seadusandlusega kaitstud. Ei ole lubatud nende kopeerimine, muutmine või kasutamine muudel veebilehtedel kommerts- või muudel eesmärkidel eelneva „E.Mi“ või selle partnerite, kes on käesolevate materjalide  omanikud, nõusolekuta.
</p>
<h3>
Brändid
</h3>
<p>
Brändid, kaubamärgid, logod ning muud käesolevale veebilehele paigutatud individualiseerimise vahendid on „E.Mi“ (Ekaterina Mirošnitšenko) ja kolmandate isikute omand. Info, mis on avaldatud veebilehel, ei anna mitte mingeid litsentseeritud õigusi ükskõik milliste kaubamärkide kasutamiseks, saamata eelnevalt kirjalikku nõusolekut omanikult.
</p>
<h3>
Info ettevõte kohta
</h3>
<p>
<b>Ettevõte täisnimi:</b> Osaühing „E.Mi“<br>
<b>Juriidiline ja faktiline aadress:</b> 344010, Rostov Doni ääres, Krasnovodskaja tn, maja 4/4<br>
<b>Registrinumber:</b> 1146195002700<br>
<b>Maksukohustuslasena registreerimise number:</b> 6163134280
</p>

</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>