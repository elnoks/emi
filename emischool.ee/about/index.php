<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("About company");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Ettevõtest</h1>
   </div>
 
  <div class="bx_page" style="text-align: center;"><img src="/upload/medialibrary/f7c/f7cb52963256ebe4b40f9af5c03a36d2.jpg" title="56.jpg" border="0" alt="56.jpg" width="1140" height="300"  /> 
    <br />
   </div>
 
  <br />
 
  <p>Ettevõte E.Mi – mitmed tuhanded aktuaalsed ja eksklusiivsed lahendused, mis on välja töötatud küünedisaini maailmameistri Ekaterina Mirošnitšenko poolt, ning küüntele ilumaalingute loomiseks mõeldud unikaalsed materjalid ja aksessuaarid, mis olid välja töötatud tema isiklikul osalusel.</p>
 
  <p>Meie missiooniks on aidata igal meistril avada oma loominguline potentsiaal, tunda end kunstnikuna ja saada tõeliseks moeeksperdiks.</p>
 
  <p>Ettevõte E.Mi on sihikindlalt arenev edukas äri, millest võib igaüks osa saada! Hetkel on E.Mi: 50 Ekaterina Mirošnitšenko Küünedisaini Kooli ametlikku esindust Venemaal, Eruoopas ja Aasias, ametlikud E.Mi kaubamärgi edasimüüjad ja sajad müügipunktid kogu maailmas. </p>
 
  <h2>VALMISLAHENDUSES NAIL&FASHION</h2>
 
  <p><img src="/upload/medialibrary/cd4/cd48dcf71294554d8839eab0a34a4986.jpg" title="98.jpg" border="0" alt="54.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Kõrgmood on tõeline kunst. See on võluv, inspireeriv ning sunnib kannatamatult ootama uut hooaega, mis toob üllatusi ja paneb alati imestama.</p>
 
  <p>E.Mi valmislahendusteks on kõrgmoe nail-disain. Aasta jooksul esindame eksklusiivseid kollektsioone, mis on välja töötatud Ekaterina Mirošnitšenko poolt. Ekaterina Mirošnitšenko on vastavalt MLA-le maailmameister küünedisaini kategoorias Fantasy (Pariis, 2010), kahekordne Euroopa meister (Ateena, Pariis, 2009), rahvusvahelise taseme kohtunik ja Ekaterina Mirošnitšenko Küünedisaini Autorikooli asutaja.</p>
 
  <p>Iga tema kollektsioon on kõige aktuaalsemate trendide tõlgenduseks, mille ta on kirja pannud küünedisaini keelde ja mugavdanud kasutamiseks salongides: uskumatult ilusad ja esmapilgul kordumatud, kuid sellegipoolest lihtsad, saavad nad selgeks igale, isegi kunstihariduseta, küünetehnikule pärast koolituste või video-seminaride läbimist.</p>
 
  <p>Kollektsioonide hulgas on selliseid bestsellereid nagu „Žostovo maalingud“, „Hiina maalingud“, „Reptiiliate naha imitatsioon“, „Krakelüüri efekt“, „Etnilised mustrid“, „Sametliiv ja vedelad kivid“ ja muud. Saage inspiratsiooni kollektsioonide täiskomplektist juba praegu!</p>
 
  <h2>TOOTED</h2>
 
  <p><img src="/upload/medialibrary/6e3/6e3b48a3e4405516b24de034f7ca5b60.jpg" title="6542.jpg" border="0" alt="6542.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Iga toote, uue värvi ja tööaksessuaari loomises osaleb Ekaterina Mirošnitšenko isiklikult.</p>
 
  <p>Tooted vastavad kõikidele küünetehnikute nõudmistele, need arvestavad kõiki töö nüansse, lubavad vähendada aega, mis kulub joonistuse loomiseks ning annavad piiramata võimalusi loomingu jaoks.</p>
 
  <p>Meil on esindatud sellised unikaalsed tooted nagu EMPASTA, GLOSSEMI, TEXTONE, PRINCOT ning lisaks geelvärvide kollketsioonid, kõige aktuaalsemates värvides foolium, paljud materjalid küünte dekoreerimiseks ja aksessuaarid. Leidke endale vajalik toode juba praegu.</p>
 
  <h2>EKATERINA MIROŠNITŠENKO KÜÜNEDISAINI KOOL</h2>
 
  <p><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" border="0" alt="765.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Kui unistate huvitavast ja loovast ametist, tahate, et teie kliendid lahkuksid teie juurest tänuliku naeratusega ja pöörduksid alati tagasi, siis teil on võimalus omandada eksklusiivset meisterlikkust Ekaterina Mirašnitšenko Küünedisaini Koolis. E. Mirašnitšenko esindab unikaalseid autoriharidusprogramme küünetehnikute jaoks. Need on välja töötatud spetsiaalselt meistritele, kellel puudub kunstiharidus.</p>
 
  <p>Kursused on jaotatud kolmeks raskusastmeks ja lisakursusteks. Neile, kes püüdlevad professionaalse Olümpose poole ja tahavad osaleda küüntekusti konkurssidel, pakume spetsiaalseid ettevalmistavaid kursuseid. Kooli õpilastest on saanud mitmekordsed piirkondlike ja föderaalsete küünedisaini konkursside võitjad. </p>
 
  <p>Valige välja oma õppeprogramm juba praegu meie veebilehel!</p>
 
   
  <h2>AMETLIKUD ESINDUSED</h2>
 
  <p><img src="/upload/medialibrary/060/060926d856ef3ef2bcc0eef18fa948cc.jpg" title="7645.jpg" border="0" alt="7645.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Bränd E.Mi ja Ekaterina Mirošnitšenko Küünedisaini Kool on aktuaalsed ärisuunad, mis saavad aina populaarsemaks ja rasvuvahelist nõudlust. Hetkel töötavad ametlikud esindused edukalt Venemaal, Ukrainas, Kasahstanis, Valgevenes, Itaalias, Portugalis, Rumeenias, Küprosel, Saksamaal, Prantsusmaal, Leedus, Eestis, Slovakkias, Lõuna-Koreas ja Araabia Ühendemiraatides. E.Mi tooteid saab aga soetada kõikjal maailmas.</p>
 
  <p>Teistki võib saada üks osa rahvusvahelisest brändist ning võite saada omale valmis ärilahenduse ning hakata ametlikuks esindajaks või kaubamärgi ametlikuks edasimüüjaks enda linnas, maakonnas või riigis. Ametlikel esindajatel on järgnevad eelised: võimalus eksklusiivselt esindada kooli brändi enda piirkonnas; õigus viia läbi küünedisaini autorikursuseid; äriprotsessid äri avamiseks ja arendamiseks; täielik toetus personali valimisel ja õpetamisel, föderaalne reklaamitugi; soodsad võimalused kogu E.Mi tootevaliku ostmiseks.
<ul> 
	<li>
the possibility to represent exclusively the brand of the Nail design school by Ekaterina Miroshnichenko in your region;
	</li>
	<li>
the right to tutor original courses on Ekaterina Miroshnichenko’s nail design;
	</li>
	<li>
full business processes for opening and developing of your business;
	</li>
	<li>
perfective design maintenance and personnel training;
	</li>
	<li>
federal promotional support;
	</li>
	<li>
profit making decisions on all E.Mi product range purchase.
	</li>
</ul></p>
 
  <p>Selleks, et saada ametlikuks esindajaks või kaubamärgi edasimüüjaks, võite täita vormi meie veebilehel või pöörduda meie poole telefoni 8 (906) 4272586 või e-posti smirnova@emi-school.ru, mbabaev@emi-school.ru teel.</p>

  <p>
    <br />
  </p>

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>