<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Tips & tricks");
?> 
<div class="bx_page"> 
<div class="h1-top">
		<h1>Tips & tricks</h1>
    </div>
 <br>
  <p> <b>Kuidas saada E.Mi brändi ja Ekaterina Mirošnitšenko Küünedisaini Kooli ametlikuks esindajaks?</b> 
    <br />
E.Mi ettevõte pakub valmislahendusi hoogsalt areneva ja huvitava äri rajamiseks. Teil on võimalus avada Ekaterina Mirošnitšenko Küünedisaini Kooli esindus enda linnas, saada E.Mi kaubamärgi ametlikuks edasimüüjaks voi hulgimüügi esindajaks. Täpsema info leiate  <a href="/social/open_school.php" >“Kuidas avada Kooli esindus” alt.</a></p>
 
  <p> <b>Kuidas teada saada, kas minu linnas on olemas Küünedisaini Kool?</b> 
    <br />
Leida enda linna Ekaterina Mirošnitsenko Küünedisaini Kooli saate <a href="/contacts/" >"Kontakt" all</a>. </p>
 
  <p> <b>Kust ma saan soetada E.Mi tooteid?</b> 
    <br />
E.Mi tooteid võite soetada meie veebileheküljel jaos "Tooted" (üksikasjalikud juhendid selle kohta, kuidas teha tellimus, leiate jaos "Kuidas tellida") või E.Mi müügipunktis. Kas teie linnas on E.Mi müügipunkt, saate teada jaos <a href="/contacts/" >„Kontakt“.</a>. </p>
 
  <p> <b>Kas Ekaterina Mirošnitšenko Küünedisaini Koolis saab õppida põhierialasid (maniküüri meister, pediküüri meister, küüne kasvatamise ja modelleerimise stilist)?</b> 
    <br />
Ekaterina Mirošnitšenko Küünedisaini kool spetsialiseerub üksnes küünedisainil.</p>
 
  <p> <b>Kas saan läbida väljaõpet Ekaterina Mirošnitšenko Küünedisaini Koolis, omamata eelnevat kogemust küünemeistrina?</b> 
    <br />
Küünedisaini kursused on täiendõpe kvalifikatsiooni tõstmiseks, sellepärast soovitame algul läbida põhierialade õpet (maniküüri meister, pediküüri meister, küüne kasvatamise ja modelleerimise stilist). Nii on teil lihtsam õppida küünedisaini.</p>
 
  <p> <b>Millisest kursusest tasub alustada õppimist?</b> 
    <br />
Soovitame alustada õppimist kursustest „Ilumaalingud – baaskursus“ ja „E.Mi Design tehnoloogiad“. Nendel kursustel saate teada värviliste geelidega töötamise alustest ja seate oma käe paika. Hiljem on teil palju kergem läbida teise ja kolmanda raskusastme kursuseid (näiteks „Žostovo maalingud“, „Hiina maalingud“, „Keeruline floristika“, „Keeruline abstraktsioon“ jm).</p>
 
  <p> <b>Kust saab leida minu linna Ekaterina Mirošnitšenko Kooli kursuste ajakava?</b> 
    <br />
Üksikasjaliku kursuste ajakava leiate jaos <a href="/courses/#schedule" >“Ajakava”</a>. </p>
 
  <p> <b>Millisest vanusest võib läbida Ekaterina Mirošnitšenko Küünedisaini Kooli kursuseid?</b> 
    <br />
Ekaterina Mirošnitšenko Küünedisaini kooli võetakse vastu õpilasi alates 15 aastast.</p>
 
  <p> <b>Kas tohib kanda E.Mi geelvärve teiste kaubamärkide lakkidele?</b> 
    <br />
E.Mi geelvärve võib kanda geellakkidele, kuid selle juures on tähtis meeles pidada, et suuremat osa geellakke võetakse maha leotades spetsiaalses vedelikus, E.Mi geelvärvid aga viilitakse buffiga maha. Sellepärast soovitame kanda disaini või ornamenti küüne osale, mis on juba kaetud geellakiga. Näiteks disainid „Sametliiv ja vedelad kivid“ või „Ruumiline vintage“.</p>
 
  <p> <b>Kas E.Mi geelvärve tohib kanda naturaalküünele?</b> 
    <br />
Enne E.Mi geelvärvi pealekandmist tuleb küüneplaat katta kunstmaterjaliga (näiteks geeli, akrüüli või geellakkiga). E.Mi geelvärvi ei tohi kanda naturaalse küüneplaadi peale. </p>
 
  <p> <b>Kuidas erinevad E-Mi geelvärvid EMPASTA-st?</b> 
    <br />
Peamised erinevused seisnevad konsistentsis ja kleepuva jäägi olemasolus (või selle puudumises). EMPASTA E.Mi on paksem, tänu millele pintslitõmme või joon jäävad selgepiirilisteks pikema aja vältel, EMPASTA „Liistumodelleerimise“ jaoks lubab luua ruumilisi disaine. Lisaks sellele EMPASTA-l puudub kleepuv jääk, sellepärast võib seda kanda nii Finish Gel’i peale kui ka selle alla. Vahepealne kuivatus EMPASTA E.Mi One Stroke jaoks on kõigest 2-5 sekundit, mis lubab luua suurema elementide arvuga disaine lühema aja jooksul. E.Mi geelvärvidel on vedelam konsistents, need sobivad suurepäraselt joonistuste loomiseks.</p>
 
  <p> <b>Mille poolest erineb polümeer „Vedelad kivid“ geelist „Vedelad kivid“?</b> 
    <br />
Geel „Vedelad kivid“ on lisamaterjal „Vedelad kivid“ tehnikas disainide loomisel.  Selle üks kiht lubab luua küüntele erinevas suuruses ja kõrguses vääriskivide imitatsioone ega nõua kaitsegeeli katet, mis lubab luua disaini lühema aja jooksul. Geelil „Vedelad kivid“ ei ole värvi ning seda ei tohi kokku segada pigmentidega. Polümeer „Vedelad kivid“ lubab luua värviliste kivide imitatsioone, sest selle kokku segamine pigmentidega on lubatud, lisaks see tuleb katta kleepuva jäägiga kaitsegeeliga.</p>
 
  <p> <b>Kuidas kasutada geelvärvi EMPASTA „Ažuurhõbe“ liistumodelleerimise jaoks?</b> 
    <br />
EMPASTA „Ažuurhõbe“ sisaldab läbipaistvat fraktsiooni, sellepärast pigistage väike kogus värvi paletile ning segage seda korralikult, seejärel laske sellel mõned päevad seosta, et selle konsistents muutuks paksemaks ning sobiks liistumodelleerimiseks. Töötada värske EMPASTA-ga, mis on tuubist just välja pigistatud, ei ole võimalik. Mida kauem EMPASTA säilitada, seda parem. EMPASTA ei kuiva paletis ära, vaid säilitab oma paksu konsistentsi ning sobib ideaalselt töötamiseks.</p>
 
  <p> <b>Milliseid pintsleid on vaja E.Mi materjalidega töötades?</b> 
    <br />
E.Mi materjalidega töö jaoks on soovitatavad kolm pintslite komplekti: „Pintslite komplekt Ilumaalingute jaoks“, „Pintslite komplekt Hiina maalingute jaoks“ ja „Universaalne pintslite komplekt“.</p>
 
  <p> <b>Kuidas pintsleid õigesti hooldada?</b> 
    <br />
Selleks, et pintsel säilitaks oma omadusi võimalikult kaua, ärge kasutage selle puhastamiseks atsetooni sisaldavaid vedelikke. Piisab selle puhastamisest niiske salvrätikuga. Lisaks pintsel kleepub kokku hajutatud UV-kiirte toimel. </p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>