<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как заказать");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Soetage omale E.Mi tooted just praegu, kasutades selleks online tellimise teenust.</h1>
   </div>
 </div>
 
<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

 
<div class="bx_page"> 
  <p>1.	Läbige lihtne registratsioon veebisaidil.
    <br />
   </p>
 
  <p>2.	2.	Minge edasi kategooriasse „Tooted“ ning lisage teile meeldivad tooted ostukorvi, vajutades roosale nupule „Lisa korvi“, mis asub paremal pool toote pildist. Kui Toode on edukalt korvi lisatud, muutub nupu värv roheliseks ning sellele ilmub kiri „Ostukorvis“. Pärast seda võite jätkata ostlemist.
    <br />
   </p>
 
  <p>3.	3.	Pärast seda kui ostuprotsess on lõpetatud, kontrollige oma tellimuse olekut „Ostukorvis“. Selleks järgige linki „Minu ostukorv“, mis asub veebilehe ülemises paremas osas. Siin saate korrekteerida oma tellimust, muuta toodete hulka ja näha ostu kogusummat. Kui olete muutnud toote hulka, kustutasite nimetused või lisasite uued, siis vajutage nupule „Loe üle“, et näha ostu aktuaalset summat.
Toodete tellimisel kehtib järgnev soodustuste süsteem: 
- Tellimuse summa alates 300 EUR – soodustus 5%,
- Tellimuse summa alates 500 EUR – soodustus 10%.
- Tellides kaupa rohkem, kui 70 EUR eest  Eesti piires postikulu ei lisandu.

    <br />
   </p>
 
  <p>4.	4.	Selleks, et vormistada tellimus, vajutage roosale nupule „Vormista tellimus“, mis asub ekraani paremas osas. Valiga sobiv makse- ja kohaletoimetuseviis.</p>
 
<p>E-poe juhataja telefoninumber: +372 5807 6768
    <br />
   </p>
 
  <p>E-post: estonia-sale@emischool.com
    <br />
   </p>
  

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>