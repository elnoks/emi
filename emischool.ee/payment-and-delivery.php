<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Arve ja toodete transtport");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Arve ja toodete transtport</h1>
   </div>
 
  <br />

  <p class="MsoListParagraphCxSpFirst" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><span style="font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">      </span></span></b><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Tähelepanu!<o:p></o:p></span></b></p>
 
 <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">E.Mi toodete tellimuse minimaalne summa on 10 EUR<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Tellimuste töötlemine toimub esmaspäevast kuni reedeni 9:00–18:00. Kui teil tekkisid küsimused, võite helistada telefonil  +372 5807 6768 või kirjutada kirja e-posti: estonia-sale@emischool.com<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Tutvuge tähelepanelikult ostu- ja kohaletoimetamise tingimustega! Vajutades tellimuse vormi nuppu „OK“, nõustute ostu- ja kohaletoimetamise tingimustega.<o:p></o:p></span></p>
  
  
 <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><b><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;"><o:p></o:p></span></b></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
   
  <p class="MsoListParagraphCxSpFirst" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><span style="font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">      </span></span></b><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Lisatingimused<o:p></o:p></span></b></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Tellimuse minimaalne summa on 10 EUR Toodete tellimise kehtib järgnev soodustuste süsteem:<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Tellimuse summa alates 300 EUR – soodustus 5%<o:p></o:p></span></p>
  
    <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Tellimuse summa alates 500 EUR – soodustus 10%<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpLast" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;"><o:p> </o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><b><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;"><o:p></o:p></span></b></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
  
  <p class="MsoListParagraphCxSpFirst" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><span style="font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">      </span></span></b><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Täiendavad eeskirjad:<o:p></o:p></span></b></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">Tellimuste saatmine viiakse täide kolme tööpäeva jooksul pärast tasu kätte saamist. Kui tellimus saadetakse piirkonnast, kus on olemas ametlik „Ekaterina Mirašnitšenko Küünedisaini Kooli“ esindus, siis selle piirkonna esindaja võtab teiega ühendust viie tööpäeva jooksul. <o:p></o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpFirst" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; font-family: Symbol;"><span style="font-stretch: normal; font-size: 7pt; font-family: 'Times New Roman';">         </span></span><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; font-family: Symbol;"><span style="font-stretch: normal; font-size: 7pt; font-family: 'Times New Roman';">         </span></span><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpLast" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;"> </span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></b></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpFirst" style="margin-left: 18pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpLast" style="margin-left: 18pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
 <span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"></span> </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>