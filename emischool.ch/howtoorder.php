<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как заказать");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Come fare un ordine</h1>
   </div>
 </div>
 
<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

 
<div class="bx_page"> 
  <p>1.	Akceptujeme pouze objednávky přes e-shop. Objednávky prostřednictvím telefonu nebo mailu nebudou zpracovány. 
    <br />
   </p>
 
  <p>2.	Aby se předešlo veškerým problémům s dodáním, laskavě Vás prosíme o kontrolu Vaších kontaktních údajů před zasláním objednávky. Požadované údaje: jméno a příjmení, telefonní číslo včetně předčíslí, e-mail a dodací adresa (ulice, číslo popisné, město, PSČ, stát). 
    <br />
   </p>
 
  <p>3.	V případě chybného uvedení údajů společnost nenese odpovědnost za případné dodatečné náklady, spojené s dodáním na špatnou adresu.
    <br />
   </p>
  
  <p>4.	Přijímáme a zpracováváme objednávky ze všech zemí s výjimkou Ruska, Ukrajiny, Kazachstánu, Běloruska, Arménie a Ázerbájdžánu. Objednávky z těchto zemí budou přesměrovány na e-shop na www.emi-school.ru.
    <br />
   </p>
 
  <p>5.	Po dokončení objednávky klient obdrží automatické upozornění e-mailem se souhrnem objednávky bez přepravní sazby. Pozor! Prosím, neprovádějte platbu na základě automatického oznámení! Faktura s celkovou částkou včetně nákladů spojených s dodáním objednávky bude odeslána do 2 pracovních dnů. Platební podrobnosti budou zaslány v průvodním dopise. Faktura se hradí v eurech nebo v případě nutnosti v dolarech. Prosím, informujte správce e-shopu o platbě v dolarech předem.
    <br />
   </p>
  <p>6.	Faktura je splatná ve lhůtě 7 pracovních dnů od data vydání. V případě, že k úhradě nedojde v rámci povoleného termínu, objednávka se zruší.
    <br />
   </p>
  <p>7.	Pokud platbu nelze provést v rámci povoleného termínu, obraťte se předem na správce e-shopu  prostřednictvím e-mailu post@emischool.com nebo telefonicky na +420 773 208 276.
    <br />
   </p>
   <p>8. Před zaplacením faktury zkontrolujte, zda na faktuře jsou správně uvedené výrobky a jejich množství 
    <br />
   </p>
   <p>9. Balení a expedice se uskuteční během 3 pracovních dnů od obdržení platby.
    <br />
   </p>
   </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>