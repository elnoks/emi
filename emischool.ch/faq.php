<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Tips & tricks");
?> 
<div class="bx_page"> 
<div class="h1-top">
		<h1>Suggerimenti</h1>
    </div>
 <br>
  <p> <b>Jak se stát oficiálním zástupcem značky E.Mi a Školy nehtového desginu Ekateriny Miroshnichenko?</b> 
    <br />
Společnosti E.Mi nabízí hotová řešení pro vytvoření rychle se rozvíjejícího a zajímavého podnikání. Vy můžete otevřít oficiální filiálku Školy nehtového designu Ekateriny Miroshnichenko ve Vašem městě, stát se oficiálním distributorem nebo velkoobchodním zástupcem. Podrobnou informací můžete najít v části „Jak otevřít filiálku Školy“</a></p>
 
  <p> <b>Jak zjistit, zda v mém regionu už je Škola nehtového designu?</b> 
    <br />
Najít Školu nehtového designu Ekateriny Miroshnichenko ve Vašem městě můžete v části <a href="/contacts/" >“Kontakty”</a>.</p>
 
  <p> <b>Kde mohu zakoupit výrobky E.Mi?</b> 
    <br />
Nakoupit výrobky můžete na našem webu v části „Produkce“ (podrobný návod jak udělat objednávku viz část „Jak objednat“), nebo u oficiálního distributora značky E.Mi ve Vašem regionu. Zjistit přesnou adresu a kontaktní čísla můžete v části <a href="/contacts/" >“Kontakty”</a>.</p>
 
  <p> <b>Mohu se ve Škole nehtového designu Ekateriny Miroshnichenko vyučit jako manikérka, nebo specialistka nehtové modeláže?</b> 
    <br />
Ve Škole nehtového designu Ekateriny Miroshnichenko se lze vyučit v oblasti nehtové modeláže a manikúry/pedikúry. Podrobnou informaci naleznete v části <a href="/courses/" >„Kurzy“</a>.</p>
 
  <p> <b>Mohu s vyučit ve škole nehtového designu Ekateriny Miroshnichenko pokud nemám zkušenost jako manikérka?</b> 
    <br />
Kurzy nehtového designu jsou vytvořené v rámci zvyšování kvalifikace, proto doporučujeme si nejdříve udělat základní výcvik v oblasti manikúry nebo nehtové modeláže, pak pro Vás bude snazší se naučit nehtovému designu.</p>
 
  <p> <b>Jakým kurzem mám začít? </b> 
    <br />
My doporučujeme začít základními kurzy: „Umělecká malba. Abeceda linií“ a „Technologie E.Mi Design“. Na těchto kurzech poznáte základní práci z barevnými gely a natrénujete ruce. Po jejich absolvování bude následná výuka na kurzech druhé a třetí úrovně (např. „MIX Design: TEXTONE, Plazi, Kameny“, „Nailkrust“, „Art-doplněk“ a Stylizované malování: květiny, ovoce, oblečení“, „Žostovská malba“, „Čínská malba“) o dost jednodušší.</p>
 
  <p> <b>Kde mohu najít rozvrh kurzů ve Škole Ekateriny Miroshnichenko v mém městě? </b> 
    <br />
Rozvrh kurzů ve Vašem městě můžete zjistit v části <a href="/courses/#schedule" >„Rozvrh“</a>. </p>
 
  <p> <b>Od jakého věku mohu absolvovat kurzy ve Škole nehtového designu Ekateriny Miroshnichenko?</b> 
    <br />
Ve škole nehtového designu Ekateriny Miroshnichenko se lze vzdělávat od 15 let.</p>
 
  <p> <b>Mohu nanášet gelové barvy E.Mi na gel-laky jiných značek?</b> 
    <br />
Gelové barvy E.Mi lze nanášet na gel-laky, ale je potřeba si pamatovat, že většina gel-laků se sundává pomocí speciální tekutiny a gelové barvy E.Mi se musí spilovat. Proto doporučujeme nanášet design nebo ornament na menší část nehtu, která je pokrytá gel-lakem. Například jako v designu „Sametový písek“ nebo „3D Vintage“.</p>
 
  <p> <b>Mohu nanášet gelové barvy E.Mi na přírodní nehet?</b> 
    <br />
Před nanášení gelové barvy E.Mi musíte pokryt nehet umělým materiálem: gelem, akrylem nebo gel-lakem. Na přírodní nehet se gelová barva nanášet nesmí.</p>
 
  <p> <b>Čím se liší gelová barva E.Mi od EMPASTY?</b> 
    <br />
Hlavní rozdíl spočívá v konzistenci a přítomností (nebo nepřítomnosti) zbytkové lepkavosti. EMPASTA E.Mi je hustší, díky tomu bude pak linie nebo tah jasnější, a konzistence EMPASTY pro modelování umožňuje vytváření 3D designů. Kromě toho EMPASTA nemá zbytkovou lepkavost, proto jí můžete nanášet nad ochranný gel Finish gel, anebo i pod něj. Sušení EMPASTY mezi jednotlivými fázemi procesu trvá pouze 2-5 vteřin, což umožňuje rychleji vytvářet design s velkým množstvím elementů. Gelové barvy E.Mi mají řidší konzistenci a ideálně se hodí pro tvorbu obrázků.</p>
 
  <p> <b>Čím se liší polymer „Tekuté kameny“ od gelu „Tekuté kameny“? </b> 
    <br />
Gel „Tekuté kameny“ to je doplňkový materiál pro vytváření designu v technice „Tekuté kameny“. Umožňuje vytvořit na nehtech imitaci drahokamů jakékoliv velikosti a výšky v jedné vrstvě a nepotřebuje následného nanášení ochranného gelu, což zkracuje čas vytváření designu. Gel „Tekuté kameny“ nemá žádnou barvu a nelze ho míchat s pigmenty. Polymer „Tekuté kameny“ umožňuje vytvářet imitaci barevných kamenů, jelikož ho můžeme míchat s pigmenty a zároveň musíme jej pokrývat vrstvou ochranného gelů.</p>
 
  <p> <b>Jak používat gelovou barvu EMPASTA „Prolamované stříbro“ pro modelování?</b> 
    <br />
EMPASTA „Prolamované stříbro“ obsahuje průhlednou frakci, proto vytlačte malé množství barvy v paletku a dobře promíchejte, poté ji nechte několik dnů odstát, aby barva získala hustší konzistencí, která se hodí na modelování. Pracovat s čerstvou EMPASTOU není možné. Čím déle necháme EMPASTU odstát, tím lépe se s ní bude pracovat. V paletce EMPASTA nevysychá, zachovává hustou texturu a ideálně se hodí pro práci.</p>
 
  <p> <b>Které štětce potřebuji pro práci s materiály E.Mi?</b> 
    <br />
Pro práci s materiály E.Mi doporučujeme tři sady štětců: „Sada štětců pro uměleckou malbu“, „Sada štětců pro Čínskou malbu“, „Univerzální sada štětců“.</p>
 
  <p> <b>Jak se mám o štětce správně  starat? </b> 
    <br />
Aby štětec co nejdéle zachoval svoji kvalitu, nepoužívejte k jeho čištění tekutiny, obsahující aceton, stačí ho utřít vlhčeným ubrouskem. Chraňte štětec před UV-zářením. </b></p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>