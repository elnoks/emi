<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Pagamenti e spedizioni");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Pagamenti e spedizioni</h1>
   </div>
 
  <br />

  <p class="MsoListParagraphCxSpFirst" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.<span style="font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">      </span></span></b><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Prezzi.</span></b></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.1<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Tutti i prezzi segnalati nel sito e-commerce includono l’IVA.</span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.2<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">I prezzi non comprendono i costi di spedizione che vengono aggiunti al momento della scelta della spedizione.</span></p>
 
  <p class="MsoListParagraphCxSpLast" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;"></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><b><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">2. Modalità di pagamento.</span></b></p>
 
  
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">2.1 Prima di procedere con il pagamento chiediamo gentilmente di controllare che i prodotti e il totale dell'ordine corrispondano con l’ordine effettuato.</span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">2.3 Sono accettati i pagamenti tramite:</span></p>
 
  <p class="MsoListParagraphCxSpFirst" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; font-family: Symbol;">·<span style="font-stretch: normal; font-size: 7pt; font-family: 'Times New Roman';">         </span></span><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">Bonifico bancario.</span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; font-family: Symbol;">·<span style="font-stretch: normal; font-size: 7pt; font-family: 'Times New Roman';">         </span></span><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">Pay Pal.</span></p>
  
  <p class="MsoListParagraphCxSpMiddle" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; font-family: Symbol;">·<span style="font-stretch: normal; font-size: 7pt; font-family: 'Times New Roman';">         </span></span><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">Carte di Credito (Visa, Visa Electron, Mastercard).</span></p>
 
  <p class="MsoListParagraphCxSpLast" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;"> </span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3. Modalità di spedizione.</span></b></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.1 Gli ordini sono spediti con corriere SDA.</span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.2 La quota delle spese di spedizione è fissa ed è di 10 Euro per qualsiasi importo e avverrà solo dopo l'effettivo accredito dell'importo della fattura sul ns conto corrente.</span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">Il totale dei costi di trasporto sarà inserito nella fattura.</span></p>
 
  <p class="MsoListParagraphCxSpFirst" style="margin-left: 18pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.3.</span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.3 L'ordine verrà processato entro 5 giorni lavorativi in più si dovranno aggiungere i tempi di consegna.</span></p>
 
  <p class="MsoListParagraphCxSpLast" style="margin-left: 18pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.4.</span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.4 I tempi di consegna variano dal luogo di destinazione e dalle condizioni relative allo specifico momento di spedizione ordine (condizioni meteo, scioperi,etc.)</span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>