<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Complaint Guidelines");
?><p>
	 These Complaint Guidelines stipulate the method and conditions of complaints on defects of goods, purchased through the on-line shop emischool.com, operated by our Company
</p>
<p>
	 E.Mi - International s.r.o., with offices at U božích bojovníků 89/1, 130 00, Prague 3 - Žižkov
</p>
<p>
	 ID No.: 24214647
</p>
<p>
	 VAT ID No.: CZ24214647
</p>
<p>
	 registered in the Registry of Companies, maintained at Municipal Court in Prague, Section C, file 189332
</p>
<p>
	 Mailing address: Štefánikova 203/23, 150 00, Prague 5 - Smíchov
</p>
<p>
	 For the complaints processing we recommend to use primarily the mailing address:
</p>
<p>
	 Phone number: 773,208,276
</p>
<p>
	 Contact e-mail: <a href="mailto:prague@emischool.com">prague@emischool.com</a>
</p>
<p>
</p>
<p>
	 1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; For which defects are we responsible?
</p>
<p>
	 As the Seller we are responsible for the goods being free of defects. It means, that the goods:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; has properties, which have been agreed by us, which we describe or which you could expect with respect to the nature of the goods based on advertising;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; is in appropriate quantity, measure or weight, and
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; is meets the requirements of the legislation.
</p>
<p>
	 The difference of colour hues in reality and on electronic imaging equipment is not considered a defect of the goods. If the goods does not match you expectation, if you are the consumer, you have the right to withdraw from the contract within 14 days from the reception of the goods in compliance with Article 5 of the General Terms and Conditions.
</p>
<p>
	 Should the defect demonstrate itself within six months from its reception, it is considered the goods were defective upon its reception.
</p>
<p>
	 We guarantee to the consumers, that the defects will not demonstrate itself in the guarantee period. If you are not an consumer, you are not provided with the guarantee of quality. Article 2 applies only to a consumer.
</p>
<p>
</p>
<p>
	 2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; How long is the guarantee period?
</p>
<p>
	 The guarantee for unused goods is twenty four months from its reception, unless a longer guarantee period is specified on the web portal or in the documents attached to the goods.
</p>
<p>
	 In case a date of minimal shelf life is provided on the goods, the guarantee period lasts until such date.
</p>
<p>
</p>
<p>
	 3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; What are your rights from faulty performance?
</p>
<p>
	 In case a defect of the goods demonstrates itself during the guarantee period, which prevents the product from being properly used, and the defect cannot be removed, you are entitled to its repair free of charge.
</p>
<p>
	 In case of a removable defect on an unused product you can instead of repair require replacement of the defective product or a perfect product, or adequate discount of the purchase price.
</p>
<p>
	 In case of a defect, which cannot be removed and which prevents the goods from being properly used as perfect goods, you are entitled to a replacement of goods, to an adequate discount of the purchase price or you are entitled to withdraw from the purchase contract.
</p>
<p>
	 You are also entitled to a reasonable discount in case we are not able to deliver a new product free of defect, replace its part of to repair a product, or in case we should fail to ensure remedy in an adequate period of time or should the remedy cause you significant difficulties.
</p>
<p>
	 You are not entitled to withdraw from contract or to request delivery of a new product if you are unable to return the goods in a condition in which you received it (except cases specified in the provisions of Article 2110 CC).
</p>
<p>
</p>
<p>
	 4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; When does the right from faulty performance not apply?
</p>
<p>
	 The rights from faulty performance do not apply, if:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; you were aware of the defect prior to the reception of product;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; you cased the defect;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; the guarantee period expired.
</p>
<p>
	 The guarantee and the entitlements from responsibility for defects also do not apply to:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; wear and tear caused by its use;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; defects caused by incorrect use of the goods, non-compliance with the manual, incorrect maintenance or incorrect storage.
</p>
<p>
	 In case of things sold for discounted price we are not responsible for defects, for which the price has been discounted.
</p>
<p>
	 We are not responsible for physical injury of persons, or for damages of property and goods, which were caused by unauthorized handling or misuse of the goods, or by negligence eventually.&nbsp;
</p>
<p>
</p>
<p>
	 5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; How to proceed with complaint?
</p>
<p>
	 Apply the complaint with our Company (or an entity, which is on the web portal designated as a person authorized for repair) without unnecessary delay from the detection of the defect.&nbsp;
</p>
<p>
	 The complaint can be applied as follows:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; for faster processing you can inform us about the complaint in advance by a phone, e-mail of in writing.&nbsp;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; for the fastest possible processing of a claim we recommend to send the claimed goods (other than COD, which we do not accept) to our mailing address (or to address of a person authorized for repair). You can also send the goods to the address of our registered office or any of our business premises.
</p>
<p>
	 Pack the goods for shipping into an appropriate packaging to prevent any damage or destruction.
</p>
<p>
	 The document of purchase or invoice, if issued, or another document of purchase should be attached to the goods together with a description of the defect and a suggestion of a method of solution of the claim. Failure to present any of the above-mentioned documents does not prevent positive processing of the claim in compliance with legal terms.
</p>
<p>
	 The time of application of a claim is the time, when we were notified about the defect and the right from the responsibility for defects of a sold item has been executed.
</p>
<p>
	 We process delivered complaints without unnecessary delay, within 30 days from the day of the application of a complaint, unless agreed otherwise. We will issue a written confirmation about application and resolution of your complaint.
</p>
<p>
	 In case of a disputable complaint we will decide about its acceptance within three business days from the day of its application.
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
	 These Complaint Guidelines are valid and effective from 23.07.2015.
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>