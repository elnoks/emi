<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Legal information");
?>
    <div class="top-wrapper">
        <div class="bredcrumbs">

        </div>
        <div class="h1-top">
            <h1>Legal information</h1>
        </div>
        <br>
    </div>
    <div class="row main-text-block">
        <div class="col-md-12">
            <h1 style="text-transform: uppercase">E.Mi — International s.r.o</h1>
            <h3>Registered in the Registry of Companies, maintained at Municipal Court in Prague,<br> Section C, file 189332</h3>
            <p>U božích bojovníku 89/1, 130 00 Praha-Žižkov</p>
            <p>IČ No. 24214647</p>
            <p>VAT ID No. CZ24214647</p>
            <p>Bankovní čislo: 6858888001/5500</p>
            <p>Swift: RZBCCZPP</p>
            <p>Iban: CZ6855000000006858888001</p>
            <p>Adresa: E.Mi — International s.r.o., Štefánikova 203/23, 150 00, Prague 5 — Smíchov</p>
            <p>Tel. číslo: +420 773 208 276</p>
            <p>Kontakty e-mail: <a href="mailto:prague@emischool.com">prague@emischool.com</a></p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>