<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("How to open the E.Mi school representative office");
?> 
<div class="bx_page"> 
  <p style="text-align: center;"><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" border="0" alt="765.jpg" width="800" height="105"  /><b>
      <br />
    </b></p>

  <p><b>
      <br />
    </b></p>

  <p> <b>Spolupráce se Školou nehtového designu Ekateriny Miroshnichenko umožní Vám získat hotové, funkční a přinášející stabilní zisk podnikání. Při spolupráci s námi, Vy můžete získat celou řadu technologií, které obsahují jak nehtové designy, tak i nástroje řízení a marketingu. Nehtové designy od Ekateriny Miroshnichenko to jsou vždy aktuální, žádané u klientek, stylové a uznávané návrhy.</b></p>
 
  <p> <b>Staňte se našim regionálním zástupcem a my Vás naučíme, jak se stát stejně úspěšnými a tvůrčími! </b> Dneska Škola nehtového designu je známá nejenom v Rusku, ale i v celém světe: prestižní ocenění mezinárodních soutěží, semináře v Německu a Itálii, otevření mezinárodní pobočky v Evropě. 
    <br />
   I Vy můžete získat exkluzívní právo vedení značky ve Vašem regionu a mít výdělečné a zajímavé podnikání v oblasti vzdělávání a zvyšování kvalifikace specialistů nehtových služeb pod značkou „Škola nehtového designu Ekateriny Miroshnichenko“</p>
 
  <p> <b>Připojte se k těm, kdo je vždy v kurzu!</b> Škola nehtového designu Ekateriny Miroshnichenko vytvořila nejen unikátní a uznávané technologie nehtového designu, ale i stejně unikátní techniky vzdělávání.
    <br />
   <b>My zpřístupňujeme pro všechny radost z tvůrčí činnosti a vytváření krásy!</b>
    <br />
   <b>Výhody „Školy nehtového designu Ekateriny Miroshnichenko“:</b> </p>

  <ul> 
    <li>Vysoká poptávka vzdělávacích služeb se potvrzuje tím, že v našem školicím středisku (m. Rostov-na-Donu) se každoročně vzdělává více než 500 nehtových designérek ze všech regionů Ruska a zahraničí.</li>
   
    <li>Vzdělávání probíhá na základě unikátní značky nehtového designu <a href="/catalog/" >E.Mi</a></li>
   
    <li>Mezinárodní pobočka společnosti. Kanceláře v Evropě (Praha).</li>
   
    <li>V základu vzdělávacích programů autorské školy leží unikátní metody, které jsou vytvořeny a patentované Ekaterinou Miroshnichenko, světovou šampionkou v nominaci „Nehtová desing“ (Paříž, 2011).</li>
   </ul>
 
  <p></p>
 
  <p> <b>My poskytneme pro Vás vše, co je potřeba pro úspěšnou práci vzdělávacího oddělení „Školy nehtového designu E. Miroshnichenko“. 
      <br />
     Pokud otevřete oficiální filiálku Školy nehtového designu, získáte:</b> </p>

  <ol> 
    <li><b>Exkluzivní právo</b> pracovat pod značkou Autorské školy nehtového designu Ekateriny Miroshnichenko a poskytovat vzdělávací služby v regionu, který je uveden ve Vaši smlouvě. V každém regionu může být pouze jeden zástupce.</li>
   
    <li><b>Hotové vzdělávací programy</b>, vypracované Ekaterinou Miroshnichenko. Regionální vzdělávací centrum tak šetří obrovské množství času a prostředků na rozpracování vlastních vzdělávacích plánů, vytváření metod vedení lekcí, vzdělávacích materiálů a manuálů. Tato výhoda umožňuje nasměrovat svoje úsilí na nábor studentů a ve velice krátké době vyrovnat náklady na otevření Filiálky.</li>
   
    <li><b>Zapojení se do integrovaného certifikačního systému.</b> Filiálka získává právo vydávat svým absolventům firemní certifikáty s hologramem „Školy nehtového designu Ekateriny Miroshnichenko“.</li>
   
    <li><b>Právo na exkluzivní slevy při koupi produkce <a href="/catalog/" >E.Mi</a> </b></li>
   
    <li><b>Reklamní a informační podporu.</b> Autorská školy rozmisťuje informací o svých filiálkách v předních časopisech a také ve vlastním reklamním katalogu a oficiálním webu společnosti. Pomáhá při organizací regionálních výstav a konferencí, věnovaných nehtovému designu, poskytuje hotové reklamní makety určené k rozmístění v místním tisku.</li>
   </ol>
 
  <p></p>
 
  <p> <b>Důležité rysy spolupráce Oficiální filiálky Školy nehtového designu Ekateriny Miroshnichenko a Oficiálního distributora značky E.Mi. </b>
    <br />
   Oficiální filiálka Školy nehtového designu Ekateriny Miroshnichenko má výhradní právo uzavírat distribuční smlouvy značky E.Mi na území, které je uvedeno ve smlouvě oficiální filiálky Školy nehtového designu. V tomto případě k požadavkům oficiální filiálky Školy nehtového designu se připojují požadavky spolupráce s distributorem, závazky ohledně množství nákupu a odpovídajícímu navýšení slevy na produkci značky E.Mi.</p>
 
  <p> <b>Požadavky na otevření Filiálky školy nehtového designu E. Miroshnichenko:</b> </p>

  <ol> 
    <li>Talentovaný instruktor, který dokáže plně kopírovat techniky Ekateriny Miroshnichenko. Kandidát absolvuje základní kurz jako student a v případě odpovídající úrovně kvalifikace, kterou určuje Ekateriny Miroshnichenko, dále absolvuje speciální přípravný kurz pro instruktory.</li>
   
    <li>Zajištění prostor pro vzdělávací středisko rozměrem více než 16m2. Umístění firemní cedule podle firemního stylu Školy nehtového designu E. Miroshnichenko.</li>
   
    <li>Možnost registrace jako právnické osoby (živnost, s.r.o aj.)</li>
   
    <li>Finanční prostředky k zakoupení startovacího množství produkce, vybavení vzdělávacího centra a prostředky na odměny instruktorů.</li>
   
    <li>Připravenost vykonávat činností spojené s náborem studentů na základě poskytovaných technologií.</li>
   </ol>
 
  <p></p>
 
  <p> <b>Vážení nehtové designérky! Pokud jste pevně rozhodnutí pro otevření filiálky Školy ve Vašem regionu, potřebujete pro začátek:</b> </p>

  <ol> 
    <li>Zjistit, zda už máte nějakou filiálku ve Vašem regionu. Tuto informaci můžete najít v části „Kontakty“  nebo zavolat na číslo (863) 255 73 67. Pokud ve vašem regionu už je otevřená Filiálka, budeme muset Vás odmítnout.</li>
   
    <li>Zašlete foto nehtových tipů s designy Ekateriny Miroshnichenko na mail sviridova@emi-school.ru (například Sofistikované květinové ornamenty, Látka, Jahody, Maliny, Třešně aj.). To je potřeba pro Vaše schválení jako nehtového Instruktora, jelikož Vaše technika musí přesně odpovídat požadavkům Ekateriny Miroshnichenko. Vy musíte přesně kopírovat její návrhy a techniky.</li>
   
    <li><b>Zjistit cenu instruktorského kurz lze na čísle +420722935746. </b>Kurz probíhá ve dvou krocích: v první části se učíte jako student, v druhé části vystupujete jako instruktor. Kurzy probíhají výhradně v Rostově na Donu. Cena kurzu závisí na množství kurzů.</li>
   
    <li><b>Zjistit množství nákupu za čtvrtletí lze na čísle +420722935746. </b>Objem nákupu je písemně ujednán ve smlouvě.</li>
   
    <li>Připravit se na opakované kurzy zvyšování kvalifikace. Kontrakt s instruktory se uzavírá na dobu 1 roku. Každý rok instruktor musí potvrzovat svoji kvalifikaci v Rostově na Donu a na základě potvrzení se prodlužuje smlouva na další rok.</li>
   </ol>
 
  <p></p>
 
  <p> <b>Více informací o otevření oficiální filiálky Školy nehtového designu Ekateriny Miroshnichenko získate na čísle 
      <br />
+420722935746, Korbut Marina, korbut@emischool.com.</b> </p>
 
  <br />
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>