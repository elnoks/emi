<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Школа ногтевого дизайна Екатерины Мирошниченко | О компании E.Mi");
$APPLICATION->SetPageProperty("description", "Научиться создавать удивительные дизайны ногтей можно в школе Екатерины Мирошниченко. Для записи на курс заполните форму на сайте или обратитесь по телефону 8 (906) 427-25-86, e-mail smirnova@emi-school.ru, mbabaev@emi-school.ru.");
$APPLICATION->SetTitle("О компании");
?><div class="bx_page">
	<div class="h1-top">
		<h1>O společnosti</h1>
	</div>
	<div class="bx_page" style="text-align: center;"><img src="/upload/medialibrary/f7c/f7cb52963256ebe4b40f9af5c03a36d2.jpg" title="56.jpg" alt="56.jpg" height="300" border="0" width="1140"> 
<br>
</div>
<br>
<p>Společnosti E.Mi – to je tisíc aktuálních a exkluzivních hotových řešení, vypracovaných světovou šampionkou v nehtovém designu Ekaterinou Miroshnichenko, a také unikátní materiály a doplňky určené k výtvarnému zdobení nehtů, jež byli vypracovány pod jejím osobním dohledem.</p>
<p>Naši misi je pomoct každé nehtové designérce rozvinout svůj tvůrčí potenciál, cítit se jako umělkyně a stát se opravdovou fashion-expertkou.</p>
<p>Společnost E.Mi je rychle se rozvíjející podnikání, součásti kterého se může stát každý. V dnešní době firma má 50 oficiálních filiálek Školy nehotového designu Ekateriny Miroshnichenko v Rusku, státech EU a Asii, téměř stejné množství distributorů značky E.MI a stovky prodejních míst po celém světě.</p>
<h2>Hotová řešení NAIL&Fashion</h2>
<p><img src="/upload/medialibrary/cd4/cd48dcf71294554d8839eab0a34a4986.jpg" title="98.jpg" alt="54.jpg" height="150" border="0" width="1140"> 
<br>
</p>
<p>Vysoká móda je opravdovým uměním. Ona uchvacuje, inspiruje k novým experimentům a nutí netrpělivě očekávat novou sezonu, která vždy překvapuje a přináší novinky.</p>
<p>Hotová řešení E.Mi – to je nehtový design od couture. Během roku nabízíme několik exkluzivních kolekcí, jež byli vypracované světovou šampionkou v nehtovém designu v nominaci Fantasy podle MLA (Paříž, 2010); a dvojnásobnou šampionkou Evropy (Atény, Paříž, 2009), porotkyni mezinárodní kategorie a zakladatelkou autorské školy nehtového designu Ekaterinou Miroshnichenko</p>
<p>Každá její kolekce je autorskou interpretací všech nejaktuálnějších trendů, které jsou přeložený do jazyku nehtového designu a adaptované pro provedení v salonech: neuvěřitelně krásné a na první pohled neopakovatelné návrhy může však realizovat téměř každá nehtová designérka, a to i bez speciálního výtvarného vzdělání po absolvování vzdělávacího kurzu nebo shlédnutí video-manuálu.</p>
<p>V rámci kolekci můžete nalézt i takové bestsellery, jako jsou „Žostovská malba“, „Činská malba“, „Imitace plazí kůže“, „Efekt rozbitého skla“, „Etnické vzory“, „Sametový písek a tekuté kameny“ aj. Inspirujte se celou kolekci právě teď! </p>

<h2>Produkce</h2>

<p><img src="/upload/medialibrary/6e3/6e3b48a3e4405516b24de034f7ca5b60.jpg" title="6542.jpg" alt="6542.jpg" height="150" border="0" width="1140"> 
<br>
</p>

<p> Ekaterina Miroshnichenko se osobně angažuje při vytváření každého výrobků, nové barvy neb doplňku. Výrobky odpovídají všem požadavkům nehtových designérů a pracovních nároků, umožňuje zkrátit čas při vytváření požadovaného vzoru a poskytuje bezhraniční možnosti tvůrčí práce.</p>
<p>V řadě výrobků jsou zastoupené i takové unikátní výrobky jako EMPSATA, GLOSSEMI, TEXTONE, PRINCOT, a také kolekce gelových barev, designérské fólie všech aktuálních odstínů, celé množství materiálů na zdobení nehtů a doplňky.</p>
<h2> Škola nehtového designu Ekateriny Miroshnichenko</h2>
<p><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" alt="765.jpg" height="150" border="0" width="1140"> 
<br>
</p>
<p> Škola nehtového designu Ekateriny Miroshnichenko to jsou vysoce efektivní vzdělávací programy, vypracované přímo na míru nehtovým designérkám, které nemají výtvarné vzdělání. Postupný přechod od jednoduchých věcí k těm složitějším umožňuje osvojit dovednosti nehtového designéra a dosáhnout vysoké profesionální úrovně.
</p>
<p> Kurzy jsou rozděleny do 3 typů podle náročnosti a také jsou vytvořeny i doplňkové kurzy. Pro ty, kteří se snaží překonat profesní strop a zúčastnit se soutěží v nail-artu, my nabízíme speciální kurzy soutěžní přípravy. Mezi studentky školy máme mnohonásobné vítězky regionálních a federálních soutěží v nehtovém designu.</p>
<p>Vyberte si svůj vzdělávací program právě teď na našem webu! </p>
<h2> Oficiální pobočky</h2>
<p><img src="/upload/medialibrary/060/060926d856ef3ef2bcc0eef18fa948cc.jpg" title="7645.jpg" alt="7645.jpg" height="150" border="0" width="1140"> 
<br>
</p>
<p>Značky E.MI a Škola nehtového designu Ekateriny Miroshnichenko jsou aktuálními směry podnikání, které získávají obrovskou popularitu a mezinárodní uznání. V danou chvíli oficiální pobočky úspěšně funguji v Rusku, Ukrajině, Kazachstánu, Bělorusku, Itálii, Portugalsku, Rumunsku, Kypru, Německu, Francii, Litvě, Slovensku, Jižní Koreje a Spojených Arabských Emirátech. Výrobky E.MI můžete zakoupit po celém světe.</p>
<p>Vy se také můžete stát součásti úspěšné mezinárodní značky a získat hotové řešení pro Vaše podnikání – stát se oficiálním zástupcem nebo distributorem značky ve Vašem městě, regionu nebo státě. Oficiální zástupci získávají následující výhody: možnost exkluzivně zastupovat značku školy ve svém regionu; právo pořádat autorské kurzy nehtového designu; komplexní podnikatelské procesy pro otevření a rozvoj podnikání; komplexní podporu v rámci náboru a vzdělávání personálu; federální reklamní podporu; výhodné nabídky na nákup celého sortimentu výrobků E.MI.</p>
<p>Pokud se chcete stát oficiálním zástupcem nebo distributorem značky, vyplňte formulář na našem webu nebo kontaktujte nás na telefonu +420 722935746 Korbut Marina, korbut@emischool.com</p>
<p>
<br>
</p>

<p><span style="font-family: Verdana, Arial, Helvetica, sans-serif;"><font size="2">(Лицензия № 147146 от 12.12.2007)</font></span></p>
</div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>