<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как заказать");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Cum plasezi o comanda</h1>
   </div>
 </div>
 
<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

 
<div class="bx_page"> 
  <p>1.	Completați formularul de înregistrare pe site.
    <br />
   </p>
 
  <p>2.	Mergeți  la sectiunea "Produse" și adăugați produsele dorite  în cosul de cumparaturi făcând clic pe butonul roz "Adauga în cos", care se află în partea dreaptă a imaginii produselor. În cazul în care produsul a fost adăugat cu succes în cos, butonul îsi va schimba culoarea în verde și se va afișa  mesajul "în coș”. Dupa aceea , puteți continua cumpărăturile.
    <br />
   </p>
 
  <p>3.	După ce ați terminat procesul de achiziție, verificați starea comenzii dvs. în sectiunea “Coș de cumparături”. Pentru a face acest lucru, selectați  secțiunea "Coșul meu", care se afla în partea dreapta-sus a paginii. Aici puteți să  modificați comanda și  cantitatea produselor dorite  și  să   vizualizați valoarea totală a achiziției.
Dacă  ati modificat, eliminat, sau adăugat produse, faceți clic pe "Reîncarca" pentru a vedea valoarea totala a produselor.
Atenție! Comnda minima pentru produse E.Mi este de  120 lei.
La comandarea produselor  se aplica următoarele reduceri:
- suma comenzii de cel putin 1000 lei   - 5% reducere (produse cadou),
- suma comenzii de cel putin 2000 lei  - 10% reducere(produse cadou),

    <br />
   </p>
 
  <p>4.	Pentru a finaliza comanda plasată, faceți clic pe butonul de culoare roz "Plasare comandă", care este situat în partea dreapta a ecranului. Selectați metoda adecvată de plată și de livrare.
Telefon contact manager magazin online:  +40771796605 și  +40755249230
E-mail: romania-sale@emischool.com

</p>
 
  
 
  

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>