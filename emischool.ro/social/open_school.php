<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("How to open the E.Mi school representative office");
?> 
<div class="bx_page"> 
  <p style="text-align: center;"><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" border="0" alt="765.jpg" width="800" height="105"  /><b>
      <br />
    </b></p>

  <p><b>
      <br />
    </b></p>

  <p> <b>Cooperarea cu Școala de Nail Design by Ekaterina Miroshnichenko este o oportunitate de a avea un bussines stabil, care va aduce venituri mereu. Cooperand cu noi, vi se va pune la dispozitie o gama completă de instrumente si resurse, de la nail design, la management si promovare.  Modelele de nail design de la Scoala Ekaterinei Miroshnichenko  sunt  întotdeauna in voga, populare printre clienti, elegante și usor de notorietate internationala!</b></p>
 
  <p> <b>Deveniți reprezentanți regionali și  va vom învăța cum sa aveti  succes si  sa fiti creativa! La momentul de fata, Scoala de Nail Design by Ekaterina Miroshnichenko este cunoscuta nu numai în Rusia, dar  si peste tot în lume, obtinand premii prestigioase  la concursuri internaționale, realizarnd seminariilor in Germania și Italia si deschizand un birou internațional în Europa.
    <br />
   Puteți obtine dreptul exclusiv de a dezvolta in regiunea dvstra. o afacere interesanta si  extrem de profitabilă in domeniul de  instruire și dezvoltare a tehnicienilor de unghii sub marca Scoala de Nail Design by Ekaterina Miroshnichenko.</p>
 
  <p> <b>Alăturați-vă celor care sunt mereu in trend!</b> Scoala de Nail Design by Ekaterina Miroshnichenko a dezvoltat  atat o tehnologie unică de nail design recunoscut la nivel international, cat si tehnici unice de învățare a acestor design-uri.
    <br />
   <b>Noi  punem la dispoziția tuturor bucuria creației și a frumusetei!</b>
    <br />
   <b>Avantajele  pe care le ofera  Scoala de Nail Design by Ekaterina Miroshnichenko:</b> </p>

  <ul> 
    <li>Avand in vedere ca in centrul de formare din Rostov -pe- Don se instruiesc, anual, mai mult de 500 de persoane din toate regiunile din Rusia si de peste hotare, se confirma faptul ca exista o cerere pentru serviciile educationale in materie de nail design.</li>
   
    <li>Activitatile formare se desfășoară  intr-un cadru unic pentru designul unghiilor avand marca <a href="/catalog/" >E.Mi</a></li>
   
    <li>La nivel internațional , compania detine un birou în Europa (Praga).</li>
   
    <li>Cursurile se bazeaza pe predarea  tehnicilor unice dezvoltate și patentate de către Ekaterina  Miroshnichenko, campioana mondiala la categoria Nail Design (Paris, 2011).</li>
   </ul>
 
  <p></p>
 
  <p> <b>Noi oferim distribuitorilor suportul necesar pentru Școlii de Nail Design by E. Miroshnichenko.
      <br />
     La deschiderea  unei reprezentante oficiale a  Școlii de Nail Design beneficiati de urmatoarele:</b> </p>

  <ol> 
    <li><b>1.	Dreptul exclusiv de a opera sub numele brand-ului  Scoala de Nail Design by Ekaterina Miroshnichenko si de a furniza servicii educaționale pe teritoriul specificat în contract. </li>
   
    <li><b>2.	Programe de instruire</b> de-a gata elaborate de Ekaterina Miroshnichenko si alte materiale suplimentare care faciliteaza procesul de instruire. Acest lucru va permite sa va canalizati resursele si timpul catre atragerea cursantilor și astfel sa va recuperati cat mai rapid costurile de deschidere a unei reprezentante oficiale.</li>
   
    <li><b>3.	Includerea într-un sistem</b> unic de certificare. Reprezentanta are dreptul de a elibera cursantilor si clientilor certificate ce au pe ele holograma Școlii de Nail Design by Ekaterina Miroshnichenko.</li>
   
    <li><b>4.	Dreptul  la reduceri exclusive la  <a href="/catalog/" >E.Mi</a> produsele.</b></li>
   
    <li><b>5.	Materiale de  publicitate</b> si suport informațional. Scoala raspandeste toate informatiile despre reprezentantii si distribuitorii lor în reviste de specialitate de la nivel federal, precum și în propria sa revista si pe site-ul oficial. Aceasta ajuta la organizarea  expozițiilor regionale și a  conferințelor despre unghii,  oferind sabloane gata de a fi folosite.</li>
   </ol>
 
  <p></p>
 
  <p> <b>Caracteristicile suprapunerii intre un reprezentant oficial al Scolii de Nail Design by Ekaterina Miroshnichenko si un distribuitor oficial al brandului E.Mi.</b>
    <br />
   Reprezentantul oficial al Scolii de Nail Design by Ekaterina Miroshnichenko are dreptul  sa  încheie un acord privind distribuția produselor ce aprtin de marca E.Mi pe teritoriul stabilit în contractul de reprezentare oficială a Scoala de Nail Design. În acest caz, cerințele reprezentantului Scolii se suprapun cu cele ale distribuitorului oficial in ceea ce priveste atingerea targetetelor.</p>
 
  <p> <b>Cerințe pentru deschiderea unei  reprezentanțe a  Scolii de Nail Design by Ekaterina Miroshnichenko:</b> </p>

  <ol> 
    <li>1.	Instructor talentat, capabil sa reproduca intocmai tehnicile Ekaterinei Miroshnichenko.  In prima faza, candidatul participa la cursuri in calitate de cursant, iar in cazul in care se potriveste standardelor profesionale, aprobate  de Ekaterina Miroshnichenko, urmeaza cursurile de specializare pentru a deveni  instructor.</li>
   
    <li>2.	Disponibilitatea unui spatiu de cel putin 16 metri patrati, pentru centrul de instruire. Folosirea  elementelor in stilul corporativ caracteristic Scolii de Nail Design by Ekaterina Miroshnichenko .</li>
   
    <li>3. Existenta unui cadru uridic ce permite furnizarea serviciilor educationale.</li>
   
    <li>4.	Resurse financiare suficiente pentru achiziționarea pachetului de produse de inceput, dotarea  centrului de formare cu echipamentele necesare și formarea instructorilor.</li>
   
    <li>Dragi tehnicieni!  Dacă v-ați decis să deschidetiv un birou de reprezentant al Școlii în regiunea dvs., va recomandam sa urmati urmatorii pasi:</li>
   </ol>
 
  <p></p>
 
  <p> <b>D1.	Verificați dacă există un birou în zona dumneavoastră. Acesta poate fi găsit în sectiunea "Contact" sau apelati 7 906 427-25-86. În cazul în care  in regiunea dvs. exista deja o Scoala,  vom fi nevoiți să vă refuzam colaborarea.</b> </p>

  <ol> 
    <li>2.	Trimiteti la  sviridova@emi-school.ru fotografii cu tipsuri reproducand cat mai bine design-ul modelelor create de Ekaterina Miroshnichenko (de ex. flori sofisticate, fructe, pliuri etc.).  Aceasta etapa este necesar pentru aprobarea candidaturii ca instructor. Capacitatea de a reproduce intocmai stilul si tehnica Ekaterinei Miroshnichenko este esentiala.</li>
   
    <li>3.	Informati-va asupra  costului de urmare a cursurilor la nr. de telefon  + 7-906-427-25-86.  Procesul de specializare se desfasoara  în două etape: în calitate de cursant și de instructor. Cursurile se desfășoară numai la centrul de instruire în Rostov-pe-Don! Costul variaza in functie de numărul de cursuri.</li>
   
    <li><b>4.	Informati-va asupra sumei totale de achiziții pe trimestru la tel.  + 7-906-427-25-86. Volumul achizițiilor este fixat în scris în contract.</b></li>
   
    <li><b>5.	Fiti pregatit/a  pentru antrenament regulat. Contractul de instructor este valabil timp de 1 an. În fiecare an, instructorul  trebuie sa  urmeze un curs de recalificare în Rostov-pe-Don și  sa incheie un nou contract pentru urmatorul an.</b></li>
   
    
   </ol>
 
  <p></p>
 
  <p> <b>Pentru mai multe informații despre deschiderea unei reprezentante  a Scolii deNail Design by Ekaterina Miroshnichenko, puteți contacta sediul central:
      <br />
Rostov-pe-Don, T. + 7-906-427-25-86, trade@emi-school.ru</b> </p>
 
  <br />
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>