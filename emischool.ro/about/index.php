﻿<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("About company");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Despre compania</h1>
   </div>
 
  <div class="bx_page" style="text-align: center;"><img src="/upload/medialibrary/f7c/f7cb52963256ebe4b40f9af5c03a36d2.jpg" title="56.jpg" border="0" alt="56.jpg" width="1140" height="300"  /> 
    <br />
   </div>
 
  <br />
 
  <p>Compania E.Mi prezintă mii de ,,soluții  de-a gata,,, noi  și  exclusiviste , dezvoltate de Ekaterina Miroshnichenko, campioana mondială în nail design, și accesorii, materiale unice pentru nail art create cu aportul ei. </p>
 
  <p>Misiunea E.Mi este de a ajuta fiecare manichiurist/ă să atingă potentialul maxim, să se simta ca un adevarat artist și să devină un adevarat nail- fashion-expert! Pentru a crea cele mai spectaculoase  design-uri E.Mi aveti posibilitatea de a le învăța la School of nail design by Ekaterina Miroshnichenko.</p>
 
  <p>Compania E.Mi s-a dezvoltat rapid într-o afacere de succes, iar dumneavoastră puteti face parte din ea! În acest moment E.Mi detine peste  50 de reprezentanțe oficiale ale School of nail design by Ekaterina Miroshnichenko în Rusia, Europa si Asia, distribuitori oficiali ai brandului E.Mi și sute de puncte de vanzare pe tot mapamondul.</p>
 
  <h2>R,,SOLUTII DE-A GATA,,&UNGHII&MODA</h2>
 
  <p><img src="/upload/medialibrary/cd4/cd48dcf71294554d8839eab0a34a4986.jpg" title="98.jpg" border="0" alt="54.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Haute couture - o adevarata arta. Aceasta fascineaza, inspiră,  îndeamnă la experimente și te face să aștepți cu nerabdare noul sezon, care uimește  și surprinde mereu.</p>
 
  <p>,,Soluțiile  de-a gata,, E.Mi -  sunt haute couture in nail art. Pe parcursul anului, prezentăm colectii exclusiviste  dezvoltate de Ekaterina Miroshnichenko  campioana mondială în nail design la  categoria Fantasy versiunea  MLA (Paris, 2010), dublă campionă europeană  (Atena, Paris, 2009), judecător internațional și fondatoarea  primei scoli internationale  de nail design.</p>
 
  <p>Fiecare colectie a Ekaterinei este o interpretare originală a ultimelor tendințe, transpuse în nail design și adaptate pentru saloanele de înfrumusetare: incredibil de frumoase și unice la prima vedere, usor de realizat dupa absolvirea  cursurilor de specializare sau master-class-urilor video.</p>
 
  <p>Printre colecțiile bestseller , ar fi "pictura zhostovo", "pictura one stroke ," "imitație piele de  reptile," "crackled effectul", "modele etnice", "nisip catifelat și pietre lichide" și altele, sunt printre colectiile Ekaterinei Miroshnichenko . Lasă-te inspirat chiar acum de varietatea  colecțiilor de design ale Ekaterinei Miroshnichenko!</p>
 
  <h2>PRODUSE PENTRU NAIL DESIGN</h2>
 
  <p><img src="/upload/medialibrary/6e3/6e3b48a3e4405516b24de034f7ca5b60.jpg" title="6542.jpg" border="0" alt="6542.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Fiecare nail-master dupa ce a folosit produsele E.Mi poate spune cu convingere: ,,Aceste produse sunt facute pentru mine!,,</p>
 
  <p>Secretul acestei dragoste la prima vedere este simplu: Ekaterina Miroshnichenko se implica personal în crearea fiecarui produs, culoare noua sau accesoriu. Produsele E.Mi te fac să-ți dorești să le folosești chiar dacă esti nail designer, campion mondial sau un reputat expert în nail art. Produsele îndeplinesc toate cerințele  nail –design-erilor, și țin cont  de  toate particularitățile tehnicilor  de lucru,  reducând  timpul de  creare a  modelului,  și oferă oportunități nelimitate pentru creativitate.</p>
 
  <p>Asemenea produse unice cum  ar fi: EMPASTA, GLOSSEMI, TEXTONE, PRINCOT, precum și colecțiile  de vopsele- gel, folii de transfer în  culorile cele mai dorite, o varietate de materiale pentru decor  și accesorii sunt prezente în oferta E.Mi. </p>
 
  <h2>Gaseste-ti produsul potrivit chiar acum!</h2>
 
  <p><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" border="0" alt="765.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Daca visați la o profesie interesantă și creativă, dacă doriți ca clienții dumneavoastră sa plece cu un zâmbet recunoscător și să revină, urmati cursurile de la Scoala de Nail Design by Ekaterina Miroshnichenko.</p>
 
  <p>Scoala de Nail Design by Ekaterina Miroshnichenko oferă programe educaționale originale pentru nail-designers, care sunt create special pentru nail-masters fără studii de artă. Metodele unice ale Ekaterinei Miroshnichenko sunt baza tuturor cursurilor de nail design.</p>
 
  <p>Cursurile de design sunt împărțite în 3 nivele de complexitate și cursuri suplimentare. Cei care iși doresc să cucerească Olympus și să participe la concursuri de nail-art, școala de nail design ofera cursuri speciale de formare. Elevii școlii de nail design by Ekaterina Miroshnichenko sunt multiplii castigatori ale concursurilor regionale și federale de nail design.</p>
 
  <p>În prezent, există 50 de reprezentanții oficiali ai scolii de nail design pe Ekaterina Miroshnichenko și toate au in comun cele mai inlate standarde. Permite-i visului tau sa devina realitate și devino un adevarat expert în nail-art - alege propriul program educațional chiar acum!</p>
 
  <p>Găsește cea mai apropiată școală pe site-ul nostru și realizeaza-ti visul!</p>
 
  <h2>Găsește cea mai apropiată școală pe site-ul nostru și realizeaza-ti visul!</h2>
 
  <p><img src="/upload/medialibrary/060/060926d856ef3ef2bcc0eef18fa948cc.jpg" title="7645.jpg" border="0" alt="7645.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Brandul E.Mi și Școala de nail desing by Ekaterina Miroshnichenko sunt cele doua directii de dezvoltare ale afacerii, care sunt foarte populare și la mare a cautare. În prezent, reprezentanții oficiali ai școlii de nail desing by Ekaterina Miroshnichenko funcționează cu succes în Rusia, Ucraina, Kazahstan, Belarus, Italia, Portugalia, România, Cipru, Germania, Franța, Lituania, Slovacia, Coreea de Sud și Emiratele Arabe Unite. Produsele E.Mi pot fi achizitionate peste tot în lume.</p>
 
  <p>De asemenea, poti face parte dintr-un brand internațional de succes și poti obține o soluție de afaceri de-a gata - să devi un reprezentant oficial al școlii de nail desing by Ekaterina Miroshnichenko sau un distribuitor oficial al brandului E.Mi în orașul sau în regiunea ta. Reprezentanții oficiali ai școlii de nail desing by Ekaterina Miroshnichenko au următoarele avantaje:
<ul> 
	<li>
posibilitatea de a reprezenta exclusiv brand-ul școlii de nail desing by Ekaterina Miroshnichenko în regiunea ta;
	</li>
	<li>
dreptul de a preda cursuri originale de nail desing ale Ekaterinei Miroshnichenko;
	</li>
	<li>
consultanța pentru deschiderea și dezvoltarea afacerii tale;
	</li>
	<li>
consultanta si suport in alegerea și instruirea personalului;
	</li>
	<li>
suport pentru promovare;
	</li>
	<li>
sfaturi pentru creșterea profitului la fiecare gama de produse E.Mi.
	</li>
</ul></p>
 
  <p>Pentru a deveni un reprezentant oficial al școlii de nail desing by Ekaterina Miroshnichenko sau un distribuitor oficial al brandului E.Mi, puteți completa un formular de cerere pe site-ul nostru sau sunati la +420 722935746, persoana de contact Korbut Marina, korbut@emischool.com</p>

  <p>
    <br />
  </p>

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>