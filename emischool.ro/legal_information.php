<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Юридическая информация");
?>
<div class="bx_page">
<div class="h1-top">
		<h1>Politica de confidențialitate</h1>
    </div>
 <br>
<h2>
Politica de confidențialitate
</h2>
<p>
1. Termeni și conditii ale politicii de confidențialitate și relația dintre dumneavoastră  și compania  EMIROSHNICHENCO ROMÂNIA SRL, legate de prelucrarea datelor cu caracter personal sunt reglementate de Legea № 677/2001 pentru protecția  persoanelor cu privire la prelucrarea datelor cu caracter personal și libera circulație a acestor date, modificată și completată,  emischool.ro are obligatia de a administra în conditii de siguranta și numai pentru scopurile specificate, datele personale pe care ni le furnizati despre dumneavoastra . "Despre  date personale". EMIROSHNICHENCO ROMANIA SRL (emischool.ro) este inregistrata în Registrul de evidenta a prelucrărilor de date cu character personal sub numarul 56614934808 
</p>
<p>
2. Politica de confidențialitate se aplică datelor cu caracter personal  pe care compania EMIROSHNICHENCO ROMANIA SRL  le-a primit sau poate sa le primeasca  de la dvs.  la înregistrare și / sau la plasarea unei comenzi pe site, și a datelor necesare  indeplinirii angajamentelor  companiei  EMIROSHNICHENCO ROMANIA SRL  în ceea ce privește produsele achiziționate de dumneavoastră și / sau accesul la serviciile personalizate ale site-ului.
</p>
<p>
3. Sunteți de acord ca utilizarea de către dumneavoastră a serviciilor personalizate ale site constituie acceptarea implicita a politicii de confidentialitate și a condițiilor care se refera la prelucrarea datelor cu caracter personal. Dacă nu sunteți de acord cu orice termen sau condiție a politicii de confidentialitate, opriți  utilizarea serviciilor oferita  și  parasiti site-ul. 
</p>
<p>
4. Confirmati că acordul dvs. in ceea ce priveste prelucrarea datelor cu caracter personal de catre EMIROSHNICHENCO ROMANIA SRL, se aplică tuturor filialelor E.Mi. 
</p>
<p>
5. Tipul de date personale ale utilizatorului colectate  și prelucrate de EMIROSHNICHENCO ROMANIA SRL :
</p>
<p>
5.1. Informații personale obligatorii pe care le oferiti , în mod voluntar și în cunoștință de cauză , la înregistrare și / sau la plasarea unei comenzi pe site, date necesare pentru ca E.Mi sa-și poata îndeplini obligațiile în ceea ce privește produsele achizitionate, cum ar fi: numele utilizatorului înregistrat; numar de  telefon; adresa de e-mail; adresa de livrare.
</p>
<p>
5.2. Informații  personale suplimentare, pe  care le furnizați în mod voluntar și în cunoștință de cauză, cum ar fi:  vârsta, sexul, statutul social, etc., la înregistrare sau atunci când utilizați oricare dintre serviciile personalizate ale site-ului.
</p>
<p>
5.3. Date anonime, primite în mod automat prin intermediul software-ului instalat: adresa IP, cookies, informațiile cu privire la browser (sau alt program care vă permite accesul la serviciile site-ului),  timpul accesarii, adresa paginii solicitate.
</p>
<p>
6. EMIROSHNICHENCO ROMANIA SRL nu verifică autenticitatea  si actualitatea datelor personale. Cu toate acestea, EMIROSHNICHENCO ROMANIA SRL  se bazeaza pe faptul că veti oferi informații personale corecte și veti  actualiza aceste informatii. Întreaga responsabilitate pentru consecintele furnizarii datelor cu caracter personal false sau nevalabile o purtati dvs. 
</p>
<p>
7. EMIROSHNICHENCO ROMANIA SRL  colectează și proceseaza numai datele cu caracter personal care sunt necesare pentru utilizarea de către dvs. a serviciilor personalizate ale site-ului și / sau achiziționarea produselor din catalogul online E.Mi, și anume pentru:
</p>
<p>
7.1. primirea, prelucrarea și livrarea comenzii dvs.; 
</p>
<p>
7.2. prelucrarea și primirea plății; 
</p>
<p>
7.3. a vă informa despre starea comenzii prin intermediul notificării electronice sau prin SMS;
</p>
<p>
7.4. îmbunătățirea calității site-ului și a serviciilor de asistență conexe;
</p>
<p>
7.5. eficientizarea serviciului suport clienti;
</p>
<p>
7.6. oferirea  serviciilor personalizate ale site-ului;
</p>
<p>
7.7. trimiterea  informațiilor referitoare la utilizarea de către dvs. a serviciilor personalizate ale site-ului;
</p>
<p>
7.8. îmbunătățirea serviciilor site-ului, accesibilitatea utilizarii site-ului, precum și pentru a dezvolta noi servicii prin intermediul  feedback-ului oferit  despre serviciile existente  ale site-ului;
</p>
<p>
7.9. a vă informa cu privire la desfășurarea evenimentelor  și activităților E.Mi;
</p>
<p>
7.10. efectuarea de studii statistice și alte cercetari pe baza datelor  anonime.
</p>
<p>
8. Datele cu caracter personal sunt confidențiale, cu excepția cazurilor in care partajati in mod public informații despre dumneavoastra .
</p>
<p>
9. EMIROSHNICHENCO ROMANIA SRL  protejează datele dvs. personale în conformitate cu cerințele de protecție a acestor informații și va fi responsabila pentru utilizarea metodelor sigure pentru a proteja aceste informații.
</p>
<p>
10. EMIROSHNICHENCO ROMANIA SRL  aplica masurile tehnice si administrative necesare pentru a proteja  datele cu caracter  personal, pentru a asigura utilizarea corectă a acestora și pentru a preveni  accesul neautorizat și accidental. Datele personale furnizate sunt stocate pe servere cu acces limitat, care sunt situate în zone protejate.
</p>
<p>
11. EMIROSHNICHENCO ROMANIA SRL  poate transfera informațiile dvs. personale către terți în următoarele cazuri:
</p>
<p>
11.1. Dvs. va exprimati acordul pentru actiuni de tipul acesta;
</p>
<p>
11.2. Transmiterea datelor este necesara pentru utilizarea serviciilor site-ului sau a altor servicii de care beneficiati  pe baza unui contract.  EMIROSHNICHENCO ROMANIA SRL  asigură confidențialitatea informațiilor personale, iar dvs. veți fi notificat în legătură cu transmiterea datelor;
</p>
<p>
11.3. Legislația rusă prevede transmiterea datelor în limitele prevazute de lege.
</p>
<p>
12. Aveți dreptul, in orice moment, sa modificati, actualizati si/sau suplimentati  datele cu caracter personal, utilizând sectiunea de editare a datelor cu caracter personal.
</p>
<p>
13. Puteți solicita în orice moment eliminarea datelor cu caracter personal furnizate, contactand compania EMIROSHNICHENCO ROMANIA SRL .
</p>
<p>
14.  Exprimarea acordului in ceea ce priveste prelucrarea datelor cu caracter personal se face, in mod voluntar, prin selectarea butonului   ‘Politica de confidntialitate’ in timpul  procesului de utilizare a site-ului.
</p>
<p>
15. Consimțământul pentru prelucrarea datelor cu caracter personal este valabil pe parcursul perioadei de utilizare a serviciilor EMIROSHNICHENCO ROMANIA SRL.
</p>
<p>
16. Toate întrebările cu privire la prelucrarea datelor cu caracter personal le puteți trimite  companiei EMIROSHNICHENCO ROMANIA SRL.
</p>
<h2>
Informații juridice
</h2>
<h3>
Drepturi de autor
</h3>
<p>
Toate drepturile sunt rezervate. Drepturile de autor aparțin E.Mi (Ekaterina Miroshnichenko) sau partenerilor E.Mi. Toate textele, fotografiile, imaginile, fisierele audio, video și  animațiile, precum și proiectiile acestora sunt protejate de drepturile de autor și de legislația în vigoare cu privire la protecția proprietății intelectuale. Nu este permisă copierea, modificarea sau utilizarea acestora  pe alte site-uri în scopuri comerciale sau de altă natură, fără acordul prealabil al E.Mi sau partenerilor.
</p>
<h3>
Marcile comerciale
</h3>
<p>
Mărcile comerciale, denumirile comerciale, logo-urile și alte mijloace de individualizare puse pe acest site sunt proprietatea E.Mi (Ekaterina Miroshnichenko) și a părților terte. Informațiile publicate pe site nu acordă nici un drept de licență de a utiliza orice marcă comerciala, fără acordul în scris obtinut în prealabil de la proprietar.
</p>
<h3>
Informații despre  companie
</h3>
<p>
<b>Numele companiei :</b> EMIROSHNICHENCO ROMANIA SRL<br>
<b>Adresa juridica si sediul social: </b> România, Cluj-Napoca, str. Trifoiului,nr.16,bl.H4, ap.5<br>
<b>ОNr. Reg. Com.: J12/2398/2014</b><br>

<p>
CUI: RO33652981<br></p>

</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>