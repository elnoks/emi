<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Tips & tricks");
?> 
<div class="bx_page"> 
<div class="h1-top">
		<h1>Intrebari-raspunsuri</h1>
    </div>
 <br>
  <p> <b>Cum poti deveni reprezentant oficial al brandului  E.Mi  si a Scolii de Nail Design by Ekaterina Miroshnichenko?</b> 
    <br />
Companie E.Mi ofera solutii complete pentru deschiderea unei afaceri interesante care se dezvolta rapid. Aveti optiunea de a deschide o reprezentanta oficiala a Școlii de Nail Design by Ekaterina Miroshnichenko în orașul dvs., a deveni distribuitorul oficial E.Mi sau agent de vanzari E.Mi.  Mai multe informații  pot fi găsite în <a href="/social/open_school.php" >"Cum se deschide o reprezentanță a Școlii".</a></p>
 
  <p> <b>Cum știu dacă există o școală de design de unghii în orașul meu?</b> 
    <br />
In sectiunea <a href="/contacts/" >“Contact”</a>puteti gasi toate reprezentantele Scolii de Nail Design by Ekaterina Miroshnichenko.</p>
 
  <p> <b>De unde pot cumpăra produsele E.Mi?</b> 
    <br />
Produsele E.Mi pot fi achizitionate de pe site-ul nostru la rubrica "Produse" ( pentru instrucțiuni detaliate despre cum să comandați, a se vedea "Cum se plaseaza o comanda") sau direct de la punctele de vânzare  autorizate ale brand-ului  E.Mi.  Pentru a verifica dacă există un punct de vânzare E.Mi în orașul dvs., cautati în secțiunea <a href="/contacts/" >“Contacte”</a>. </p>
 
  <p> <b>Exista posibilitatea de urma cursuri de specializare de bază (manichiura, pedichiură,  constructie și modelare), la Scoala de Nail Design by Ekaterina Miroshnichenko?</b> 
    <br />
Scoala de Nail Design by Ekaterina Miroshnichenko este specializată exclusiv pe design-ul unghiilor.</p>
 
  <p> <b>Pot sa urmez cursurile Scolii de Nail Design by Ekaterina Miroshnichenko, neavând experiență de lucru in domeniu?</b> 
    <br />
Cursurile de design-ul unghiilor sunt cursuri pentru dezvoltarea si perfectionarea  profesionala, de aceea vă recomandăm mai întâi să urmati cursuri de baza (manichiura, pedichiura, constructie si modelare a  unghiilor). Ulterior va va fi mai ușor să va specializati in design-ul unghiilor.</p>
 
  <p> <b>Cu ce curs ar trebui sa incep?</b> 
    <br />
Vă recomandăm să va  începeți  specializarea urmand cursurile de bază "Pictura Artistica - un curs de bază" și "Tehnologii E.Mi Design”. In  cadrul acestor cursuri veti invata elementele de bază in lucrul cu gelurile colorate și  cum sa va folositi corect mâna. După aceea  va fi mult mai ușor să treceti cursurile de nivel doi și trei, care au un  nivel de complexitate mai mare, cum ar fi "Pictura Zhostovo", "Pictura One Stroke", "Floristica sofisticata, "Abstractie sofisticata" și altele.</p>
 
  <p> <b>Unde pot găsi un program al cursurilor Scolii de Nail Design by Ekaterina Miroshnichenko în orașul meu?</b> 
    <br />
Programul detaliat al cursurilor organizate în zona dvs. pot fi găsite în<a href="/courses/#schedule" >“Orar cursuri”</a>. </p>
 
  <p> <b>Care este varsta minima de participare la cursurile Scolii de Nail Design by Ekaterina Miroshnichenko?</b> 
    <br />
Scoala de Nail Design by Ekaterina Miroshnichenko  accepta cursanti cu varsta minima de 15 ani.</p>
 
  <p> <b>Pot aplica gelurile colorate E.Mi  pe ojele semipermanente de la alte brand-uri?</b> 
    <br />
Gelurile colorate E.Mi gel pot fi aplicate pe ojele semipermanente,  insa este important să  mentionam că majoritatea ojelor semipermanente se  îndepărtează  folosind un  lichid special, iar gelurile colorate E.Mi se indeparteaza cu ajutorul buff-ului. De aceea, vă recomandăm sa folositi gelurile colorate la realizarea unui desen sau a unui ornament pe o suprafața  mai mica a  unghiei acoperita cu oja semipermanenta.  De exemplu, puteti realiza modele de Pietre Lichide si Nisip Catifelat sau 3D Vintage.</p>
 
  <p> <b>Pot aplica gelurile colorate E.Mi  pe unghia naturala?</b> 
    <br />
Înainte de a aplica gelurile colorate E.Mi , este necesar ca suprafata unghiei sa fie acoperita cu un produs artificial: gel, acril sau oja semipermanenta. Aplicarea gelurilor colorate E.Mi direct pe unghia naturala nu este posibila.</p>
 
  <p> <b>Care este diferenta intre gelurile colorate E.Mi si EMPASTA?</b> 
    <br />
Principala diferență  este consistenta produsului  și prezența (sau absența) a stratului de dispersie. EMPASTA E.Mi are o consistenta mai densa, ceea ce face ca o linie sau o pensulare sa nu se scurga, ceea ce va permite sa creati modele tridimensionale.  In plus, EMPASTA nu are strat lipicios, astfel încât poate  fi aplicat peste Finish Gel, cat si inainte de acesta.  Timpul de uscare intermediar pentru EMPASTA E.Mi One Stroke este de  numai 2-5 secunde, ceea ce faciliteaza realizarea design-ului  cu un număr  mai mare de elemente. Gelurile colorate  E.Mi au o consistență mai mult lichida,de aceea  acestea sunt ideale pentru pictura propriu-zisa.</p>
 
  <p> <b>Care este diferenta intre polymer-ul si  gelul (olygomer-ul) “Pietre Lichide”?</b> 
    <br />
Gelul pentru  Pietre Lichide  este un material aditional folosit la realizarea modelelor folosind tehnica Pietrelor Lichide. Acesta vă permite să creați imitații ale pietrelor pretioase si semipretioase de orice dimensiune și înălțime aplicand gelul  într-un singur strat si nu necesita  acoperire cu un gel de protecție, ceea ce reduce timpul pentru crearea unui design. Gelul  pentru Pietre Lichide nu are nici o culoare si nu poate fi amestecat cu pigmenți. Polymer-ul pentru Pietre Lichide poate fi amestecat cu pigmenti, ceea ce  vă permite să creați imitații colorate ale  pietrelor pretioase,insa acesta necesită  acoperire cu un Finish gel cu strat de dispersie.</p>
 
  <p> <b>Cum utilizez EMPASTA Openwork Silver?</b> 
    <br />
EMPASTA Openwork Silver cuprinde o fracțiune transparenta, astfel încât se stoarce o cantitate mică de vopsea intr-o paleta și se amestecă bine, apoi se  lasa timp de câteva zile ca sa dobândeasca o consistență mai groasa,potrivita pentru realizarea modelelor vintage.  Este mult mai greu de lucrat  cu gelul EMPASTA  imediat dupa ce a fost stors dintr-un tub. Cu cat este  mai lungtimpul de stocare a gelurilor EMPASTA, cu atât mai bine. EMPASTA nu se usuca daca este lasat in paletar, deoarece capata consistenta potrivita si devine ideal pentru munca.</p>
 
  <p> <b>Ce pensule sunt necesare atunci cand lucrez cu produsele E.Mi?</b> 
    <br />
Pentru a lucra cu materiale E.Mi  sunt recomandate trei seturi de pensule: Set de pensule pentru pictura artistică, Set pensule One stroke și Set universal de pensule.</p>
 
  <p> <b>Cum trebuie ingrihite pensulele?</b> 
    <br />
Pentru ca pensulele sa se pastreze cat mai mult, nu utilizați lichide ce contin acetona la curatare, ci pur și simplu ștergeți cu un servetel umed. De asemenea, pensulele sunt foarte sensibile la razele solare si la UV si se pot polimeriza foarte usor.</p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>