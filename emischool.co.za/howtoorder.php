<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("How to make an order");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>How to make an order.</h1>
   </div>
 </div>
 
<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

 
<div class="bx_page">
    <p>1. Only orders placed via e-shop are accepted. Orders placed via phone or e-mail will not be processed.</p>
    <p>2. To avoid any problems with delivery we kindly ask you for your personal contact details before placing an order. Data required: Name and Surname, cell phone number, email address as well as shipping address (Street, House number, City, ZIP, country).</p>
    <p>3. In case of any mistakes in contact details Company is not responsible for any additional expenses related to delivery to wrong address.</p>
    <p>5. When finishing the order, Client receives an automatic email notification with order summary without shipping rates. Attention! Please do not provide payment on the basis of automatic notification! Invoice with total amount including delivery costs will be sent within 2 business days. Payment details will be sent in the accompanying letter.</p>
    <p>6. Invoice is payable within 7 business days after issuing date. In case on non-payment within the permitted term order is automatically cancelled.</p>
    <p>7. If you are not able to provide payment within the permitted term please contact our manager via email southafrica-school@emi-sa.co.za or 0119760081 in advance.</p>
    <p>8. Before paying the invoice please check if products and products amounts in the invoice are correct.</p>
    <p>9. Packaging and shipping are processed within 2 business days after receiving of the payment.</p>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>