<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Tips & tricks");
?> 
<div class="bx_page"> 
<div class="h1-top">
		<h1>Tips & tricks</h1>
    </div>
 <br>
  <p> <b>How to become an official representative of the E.Mi brand and the school of nail design by Ekaterina Miroshnichenko?</b> 
    <br />
The E.Mi Company offers ready-made business solutions to launch fast growing and interesting business. You can open an official representative office of the Nail design school by Ekaterina Miroshnichenko in your city; become an official distributor of the E.Mi brand or a wholesale representative. Detailed information can be found in section <a href="/social/open_school.php" >“How to open a representative office of the School”</a></p>
 
  <p> <b>Whether there is a nail design school in my city or not?</b> 
    <br />
The School of nail design by Ekaterina Miroshnichenko in your city can be found in section <a href="/contacts/" >“Contacts”</a>. </p>
 
  <p> <b>Where can I buy the E.Mi products?</b> 
    <br />
You can buy the E.Mi products on our website in section “Products” (see section “How to order” for a detailed instruction) or in a sales point. You can know more about sales points in section <a href="/contacts/" >“Contacts”</a>. </p>
 
  <p> <b>Can I get basic qualifications such as a manicurist, a pedicure master, a master of nail extension at the School of nail design by Ekaterina Miroshnichenko?</b> 
    <br />
The nail design school by Ekaterina Miroshnichenko specializes in nail design only.</p>
 
  <p> <b>Can I get the training course at the Nail design school by Ekaterina Miroshnichenko without working experience of nail master?</b> 
    <br />
Nail design courses are the advanced training, that’s why we recommend you to get elementary skills in basic qualifications (a manicurist, a pedicure master, a master of nail extension). This will make it easy to practice nail design.</p>
 
  <p> <b>What course is better to start with?</b> 
    <br />
We recommend you to start with basic courses: Art Painting – basic course and E.Mi design technologies where you will learn how to work with coloured gels and become a skilled hand. After these courses it will be easy to learn at the second and third level of complexity such as Zhostovo Painting, Chinese Painting, Sophisticated Floral Ornaments, Complex abstraction, etc.</p>
 
  <p> <b>Where can I find the schedule of courses at Nail design school by Ekaterina Miroshnichenko in my city?</b> 
    <br />
More details about courses timetable you can find in section <a href="/courses/#schedule" >“Schedule”</a>. </p>
 
  <p> <b>What is the minimal age of students of the nail design school by Ekaterina Miroshnichenko?</b> 
    <br />
Apprentices at the age of 15 are accepted.</p>
 
  <p> <b>Can I apply the E.Mi gel paints on gel varnish of another brand?</b> 
    <br />
Yes, the E.Mi gel paints can be applied on gel varnish, but you should remember that most of gel varnishes are removed by special gel polish remove, while the E.Mi gel paints are filed off. That’s why we recommend to make a design or an ornament on a small surface of a nail, covered with gel polish. For example, Velvet sand and Liquid stones or 3D vintage designs.</p>
 
  <p> <b>Can I apply the E.Mi gel paints on a natural nail?</b> 
    <br />
Before applying the E.Mi gel paint, a nail plate should be covered with gel, acryl or  gel polish. To apply the E.Mi gel paint on a narural nail plate is forbidden.</p>
 
  <p> <b>What is the difference between the E.Mi gel paints and EMPASTA?</b> 
    <br />
The main differences are consistency and residual tack.  E.Mi EMPASTA is thicker, so stroke or a line is more accurate, and EMPASTA texture for Molding modelling allows to make 3D designs. Moreover, EMPASTA doesn’t have residual tack, so it can be applied both on and underneath Finish Gel. In-between drying for EMPASTA E.Mi One Stroke is only 2-5 seconds. It helps to draw design with a big amount of elements faster. The E.Mi gel paints are more liquid, they are ideal for ornaments.</p>
 
  <p> <b>What is the difference between Liquid stones polymer and Liquid stones gel?</b> 
    <br />
“Liquid stones” gel is an additional material to design “Liquid stones”. It helps to imitate gem stones on nails of different size and height and doesn’t require top coat/ this allow saving the operating time. “Liquid stones” gel is transparent and you can’t mix it with pigments. “Liquid stones” polymer imitates colored stones as you can mix it with pigments and should be coated with top gel with residual tack.</p>
 
  <p> <b>How to use EMPASTA gel paint Openwork Silver for framing design?</b> 
    <br />
EMPASTA Openwork Silver contains transparent cut, that’s why press a small amount of paint into palette and mix thoroughly; after this let the paint draw for some days to have thick texture suitable for framing design. To work with fresh EMPASTA is impossible. The more EMPASTA is kept, the better it is. EMPASTA doesn’t dry up, it draws and become ideal for work.</p>
 
  <p> <b>What brushes do I need to work with the E.Mi materials?</b> 
    <br />
We recommend 3 sets of brushes to work with the E.Mi materials: Set of brushes for Art Painting, Set of brushes for One Stroke Painting and  Universal set of brushes.</p>
 
  <p> <b>How should I care for brushes?</b> 
    <br />
Don’t use liquids containing ethyl acetate for cleaning, wet napkin is enough. Also brush is quickly set from ultraviolet scattered rays.</p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>