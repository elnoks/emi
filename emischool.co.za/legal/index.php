<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Details of the Seller");
?>

<p>
    E.Mi South Africa School & Representative Pty Ltd<br>
    25 Loretha Street, Van Riebeeck Park, Kempton Park, 1619.<br>
    Registration No. 2015/292460/07<br>
    VAT ID No. 4790272274<br>
</p>
<p>
    Account number: 073247227<br>
    Branch code: 012442<br>
    Standard Bank. Cheque Account<br>
</p>
<p>
    Mailing address: 25 Loretha Street, Van Riebeeck Park, Kempton Park, 1619.<br>
    Phone number: 0119760081<br>
    Contact e-mail: <a href="mailto:southafrica-school@emi-sa.co.za">southafrica-school@emi-sa.co.za</a> / <a href="mailto:info@emi-sa.co.za">info@emi-sa.co.za</a>
</p>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>