<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Payment and delivery");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Payment and delivery</h1>
   </div>
 
  <br />


    <h3>1. Prices</h3>

    <p>1.1 All the e-shop prices include 14% VAT.</p>
    <p>1.2 Products are supplied without 14% VAT to non-EU countries.</p>
    <p>1.3 For all orders over R10000.00 is valid 5% discount (discount is not fundable and is valid only for one order).</p>
    <p>1.4 Prices do not include delivery expenses.</p>
    <p>1.5 All taxes and other possible expenses in accordance with Client’s country laws pays Client.</p>

    <h3>2. Payment</h3>

    <p>2.1 All payments are effected on the basis of issued invoice.</p>
    <p>2.2 Before paying the invoice please check if products and products amounts in the invoice are correct.</p>
    <p>2.3 We accept payments via:</p>
    <p>- Bank transfer to our account;</p>
    <p>- Pay with my credit or debit card.</p>

    <h3>3. Shipping conditions</h3>
    <p>3.1 Products are shipped by Globeflight Transporting Company.</p>
    <p>3.2 Shipping rates are R100.00</p>
    <p>3.3 Packaging and shipping are processed within 2 business days after receiving of the payment.</p>
    <p>3.4 All shipping terms are estimated and depend on shipping destination.</p>
    <p>3.5 When order is shipped, Globeflight Transporting Company has a waybill number and an email with the waybill number for tracking will be sent.</p>

 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>