<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Ошибка 404");
?>
<div class="container">
	<div class="wrapper-404">
		<h1><?=$APPLICATION->GetTitle()?></h1>
		<h2>Запрашиваемая вами страница не найдена.</h2>
		<div class="text-container">
			<p>
			Возможно вы неправильно набрали адрес.
			Проверьте правильность его написания и, при необходимости, исправьте.
			</p>
			<p>А пока Вы можете:</p>
		</div>
		<div class="btn-container">
			<a href="/catalog/" class="btn-pink">Купить продукцию E.Mi</a>
			<a href="#" class="btn-pink">Записаться на курсы</a>
		</div>
	</div>
<?
$APPLICATION->IncludeComponent("bitrix:main.map", ".default", array(
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"SET_TITLE" => "Y",
	"LEVEL"	=>	"0",
	"COL_NUM"	=>	"1",
	"SHOW_DESCRIPTION" => "Y"
	),
	false
);
?>
	
</div><!-- /.container --><? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>