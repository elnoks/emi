<?php
/**
 * Created by PhpStorm.
 * User: prgrant & A.Volobuev
 * Date: 30.03.2018
 * Time: 14:21
 */


class AmoEmi
{
    protected $token_info = array();
    private $cURL = null;
    private $user_login = "";
    private $user_hash = "";
    protected $link = "";

    public function __construct($account,$login,$hash)
    {
        if (!isset($hash)) exit();
        $this->link = $account;
        $this->user_login = $login;
        $this->user_hash = $hash;
        $this->cURL = curl_init();
        curl_setopt_array($this->cURL, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_REFERER => "courses.emiroshnichenko.ru",
        ));
        if (file_exists(__CLASS__ . ".json")) $this->token_info = json_decode(file_get_contents(__CLASS__ . ".json"), true);
    }

    private function __checkToken()
    {
        if (!isset($this->token_info['auth']) || time() - $this->token_info["time"] >= 15 * 60) /* token timeout is 15 min */
            $this->__getToken($this->user_login, $this->user_hash);
    }

    private function __getToken($user_login, $user_hash)
    {
        curl_setopt($this->cURL, CURLOPT_POST, 1);
        curl_setopt($this->cURL, CURLOPT_COOKIEFILE, $_SERVER["DOCUMENT_ROOT"] . '/php/'.$user_login.'cookieamo.txt');
        curl_setopt($this->cURL, CURLOPT_COOKIEJAR, $_SERVER["DOCUMENT_ROOT"] . '/php/'.$user_login.'cookieamo.txt');
        curl_setopt($this->cURL, CURLOPT_URL, $this->link.'private/api/auth.php?type=json');
        curl_setopt($this->cURL, CURLOPT_POSTFIELDS, array("USER_HASH" => $user_hash, "USER_LOGIN" => $user_login));
        $result = json_decode(curl_exec($this->cURL));
        //var_dump($result);
        $this->token_info = array('auth' => dirname(__FILE__) . '/cookieamo.txt', "time" => time());
    }

    public function set_deals($deals,$user_login){
		$this->__checkToken();
		curl_setopt($this->cURL, CURLOPT_POSTFIELDS, json_encode($deals));
		curl_setopt($this->cURL, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($this->cURL, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($this->cURL, CURLOPT_POSTFIELDS, json_encode($deals));
        curl_setopt($this->cURL, CURLOPT_COOKIEFILE, $_SERVER["DOCUMENT_ROOT"] . '/php/'.$user_login.'cookieamo.txt');
        curl_setopt($this->cURL, CURLOPT_COOKIEJAR, $_SERVER["DOCUMENT_ROOT"] . '/php/'.$user_login.'cookieamo.txt');
		curl_setopt($this->cURL, CURLOPT_URL, $this->link . "api/v2/leads");
		return curl_exec($this->cURL);
	}
	public function set_inleads($inleads,$user_login){
		$this->__checkToken();
		curl_setopt($this->cURL, CURLOPT_POSTFIELDS, json_encode($inleads));
		curl_setopt($this->cURL, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($this->cURL, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($this->cURL, CURLOPT_POSTFIELDS, json_encode($inleads));
        curl_setopt($this->cURL, CURLOPT_COOKIEFILE, $_SERVER["DOCUMENT_ROOT"] . '/php/'.$user_login.'cookieamo.txt');
        curl_setopt($this->cURL, CURLOPT_COOKIEJAR, $_SERVER["DOCUMENT_ROOT"] . '/php/'.$user_login.'cookieamo.txt');
		curl_setopt($this->cURL, CURLOPT_URL, $this->link . "api/v2/incoming_leads/form");
		return curl_exec($this->cURL);
	}
	public function get_inleads($inleads,$user_login,$hash){
        $this->__checkToken();
        curl_setopt($this->cURL,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($this->cURL,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($this->cURL,CURLOPT_HEADER,false);
        curl_setopt($this->cURL,CURLOPT_POST,false);
        curl_setopt($this->cURL, CURLOPT_COOKIEFILE, $_SERVER["DOCUMENT_ROOT"] . '/php/'.$user_login.'cookieamo.txt');
        curl_setopt($this->cURL, CURLOPT_COOKIEJAR, $_SERVER["DOCUMENT_ROOT"] . '/php/'.$user_login.'cookieamo.txt');
        curl_setopt($this->cURL, CURLOPT_URL, $this->link . "api/v2/incoming_leads/?api_key=".$hash.'&login='.$user_login.'&'.$inleads.'&page_size=15');
		return curl_exec($this->cURL);
	}
	public function set_contacts($contacts,$user_login){
		$this->__checkToken();
		curl_setopt($this->cURL, CURLOPT_POSTFIELDS, json_encode($contacts));
		curl_setopt($this->cURL, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($this->cURL, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($this->cURL, CURLOPT_POSTFIELDS, json_encode($contacts));
		curl_setopt($this->cURL, CURLOPT_COOKIEFILE, $_SERVER["DOCUMENT_ROOT"] . '/php/'.$user_login.'cookieamo.txt');
		curl_setopt($this->cURL, CURLOPT_COOKIEJAR, $_SERVER["DOCUMENT_ROOT"] . '/php/'.$user_login.'cookieamo.txt');
		curl_setopt($this->cURL, CURLOPT_URL, $this->link . "api/v2/contacts");
		return curl_exec($this->cURL);
	}
	public function get_contacts($contacts,$user_login){
		$this->__checkToken();
		curl_setopt($this->cURL,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($this->cURL,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
		curl_setopt($this->cURL,CURLOPT_HEADER,false);
		curl_setopt($this->cURL,CURLOPT_POST,false);
		curl_setopt($this->cURL, CURLOPT_COOKIEFILE, $_SERVER["DOCUMENT_ROOT"] . '/php/'.$user_login.'cookieamo.txt');
		curl_setopt($this->cURL, CURLOPT_COOKIEJAR, $_SERVER["DOCUMENT_ROOT"] . '/php/'.$user_login.'cookieamo.txt');
		curl_setopt($this->cURL, CURLOPT_URL, $this->link . "api/v2/contacts/?".$contacts);
		return curl_exec($this->cURL);
	}
}

