<?
ini_set('display_errors','Off');
error_reporting('E_ALL');

require($_SERVER["DOCUMENT_ROOT"] . "/php/amo_crm_emi.php");
require($_SERVER["DOCUMENT_ROOT"] . "/php/amo_functions.php");

$account = "https://emiofficial.amocrm.ru/";
$login = "brand@emi-school.ru";
$hash = "ea7d659ea1ed06aac97b16c084d2708e";
$coning['account']=$account;
$coning['login']=$login;
$coning['hash']=$hash;
$amoemi = new AmoEmi($account, $login, $hash);


$deals['add'] = array(
    array(
        'name'                => 'Запись на курс '.$_POST['data']['NAME'],
        'created_at'          => time(),
        'status_id'           => 19853572,
        'responsible_user_id' => 2374885, #Пользователь Шатова
        'tags'                => 'Запись на курс courses.emiroshnichenko.ru', #Теги
        'custom_fields'       => array(
            array(
                'id'     => 116555, #Регион
                'values' => array(
                    array(
                        'value' => 'Ростовская область'
                    )
                )
            ),
			array(
                'id'     => 530043, #Курс
                'values' => array(
                    array(
                        'value' => $_POST['data']['COURSE'],
                    )
                )
            ),
			array(
                'id'     => 530041, #Примечание
                'values' => array(
                    array(
                        'value' => $_POST['data']['MESSAGE'],
                    )
                )
            ),
            array(
                'id'     => 107493, #тип сделки
                'values' => array(
                    array(
                        'value' => 476435
                    )
                )
            )
        )
    )
);


$resdeals = json_decode( $amoemi->set_deals($deals,$login));

$iddeals=$resdeals->_embedded->items['0']->id;

// -----------------


$nphones=array(
    $_POST['data']['PHONE'],
    substr($_POST['data']['PHONE'],2),
    preg_replace('![^0-9]+!','',$_POST['data']['PHONE']),
    preg_replace('![^0-9]+!', '', substr($_POST['data']['PHONE'],2)),
    '8'.preg_replace('![^0-9]+!', '', substr($_POST['data']['PHONE'],2)),
    '+7'.preg_replace('![^0-9]+!', '', substr($_POST['data']['PHONE'],2))
);



$idcont=amo_search_contact($nphones, $coning)->_embedded->items['0']->id;
$resdeals=$idcont->_embedded->items['0']->leads->id;
$resdeals[]= $iddeals;

if($idcont) {

    $contacts['update'] = array(
        array(
            'id' => $idcont,
            'updated_at' => time(),
            'leads_id' => $resdeals,
        )
    );
}
else {
    $contacts['add'] = array(
        array(
            'name'                => $_POST['data']['NAME'],
            'responsible_user_id' => 2374885, #Пользователь Шатова
            'tags'                => $_SERVER["HTTP_HOST"], #Теги
            'leads_id'            => array( $iddeals ),
            'custom_fields'       => array(
                array(
                    'id'     => 68239, #Email
                    'values' => array(
                        array(
                            'value' => $_POST['data']['EMAIL'],
                            'enum'  => 142091
                        )
                    )
                ),
                array(
                    'id'     => 68237,
                    'values' => array(
                        array(
                            'value' => $_POST['data']['PHONE'],
                            'enum'  => 142081
                        )
                    )
                ),
                array(
                    'id'     => 134157, #Регион
                    'values' => array(
                        array(
                            'value' => 'Ростовская область'
                        )
                    )
                ),
                array(
                    'id'     => 134155, #Источник
                    'values' => array(
                        array(
                            'value' => 271555
                        )
                    )
                )
            )
        )

    );
}
$rescont=$amoemi->set_contacts($contacts,$login);
