<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 12.06.2018
 * Time: 17:44
 */

function amo_search_contact($querys, $amo_con_info){
	$account=$amo_con_info['account'];
	$login=$amo_con_info['login'];
	$hash=$amo_con_info['hash'];

	foreach ($querys as $query) {
		$qcontact = 'query='.$query;
		$amoemi   = new AmoEmi( $account, $login, $hash );
		$res      = json_decode( $amoemi->get_contacts( $qcontact, $login ) );
        if($res->_embedded->items['0']->id){  //Если найден результат, то возвращаем знанчение и прекращаем поиск
			return $res;
			break;
		}
	}

}


function amo_contact_in_array($res) {
	$oldUser['id']=$res->_embedded->items['0']->id; //id контакта
	$oldUser['name']=$res->_embedded->items['0']->name; //полное имя
	$oldUser['leads']=$res->_embedded->items['0']->leads->id;; //Cделки
	foreach ($res->_embedded->items['0']->tags as $tag){
		$arTags[]=array(
			'id' => $tag->id,
			'name' => $tag->name,
		);
	}
	$oldUser['tags']=$arTags; //Теги

	foreach ($res->_embedded->items['0']->custom_fields as $cuFields){  //Разбор доп полей
		switch ($cuFields->name) {
			case 'Телефон':                                                 //Телефон
				$arvalues=array();
				foreach ($cuFields->values as $values) {
					$arvalues[] = array(
						'enum'  => $values->enum,
						'value' => $values->value
					);
				}
				$oldUser['custom_fields']['phone'] = array(
					'id'     => $cuFields->id,
					'values' => $arvalues
				);
				break;

			case 'Email':                                                   //Почта
				$arvalues=array();
				foreach ($cuFields->values as $values) {
					$arvalues[] = array(
						'enum'  => $values->enum,
						'value' => $values->value
					);
					}
					$oldUser['custom_fields']['email'] = array(
						'id'     => $cuFields->id,
						'values' => $arvalues
					);
				break;
			case 'Адреc':                                                   //Адрес
				foreach ($cuFields->values as $values) {
					$arvalues=array(
						'enum' => $values->enum,
						'value' => $values->value
					);
				}
				$oldUser['custom_fields']['address'] = array(
					'id' => $cuFields->id,
					'values' => $arvalues
				);
				break;
			case 'Регион':                                                  //Адрес
				foreach ($cuFields->values as $values) {
					$arvalues=array(
						'enum' => $values->enum,
						'value' => $values->value
					);
				}
				$oldUser['custom_fields']['region'] = array(
					'id' => $cuFields->id,
					'values' => $arvalues
				);
				break;
			case 'Город':                                                   //Город
				foreach ($cuFields->values as $values) {
					$arvalues=array(
						'enum' => $values->enum,
						'value' => $values->value
					);
				}
				$oldUser['custom_fields']['city'] = array(
					'id' => $cuFields->id,
					'values' => $arvalues
				);
				break;
			case 'Источник':                                                  //Источник
				foreach ($cuFields->values as $values) {
					$arvalues=array(
						'enum' => $values->enum,
						'value' => $values->value
					);
				}
				$oldUser['custom_fields']['source'] = array(
					'id' => $cuFields->id,
					'values' => $arvalues
				);
				break;
		}
	}
	return $oldUser;
}

function amo_compare_contacts($curUser,$oldUser) {
	$newUser=$oldUser;

		if(strlen($curUser['name']) >= strlen($oldUser['name'])){
			$newUser['name']=$curUser['name'];
		}

		if($curUser['leads']){
			$newUser['leads']=$oldUser['leads'];
			foreach ($curUser['leads'] as $lead){
				$newUser['leads'][]=$lead;
			}
		} else {
			$newUser['leads']=$oldUser['leads'];
		}

		if($curUser['source']){
			if(!$oldUser['custom_fields']['source']) {
				$newUser['custom_fields']['source'] = array(
                    'id'     => 134155, #Источник
                    'values' => array(
                        array(
                            'value' => 'Сайт',
                            'enum'  => 271555
                        )
                    )
                );
			}
		}

		if($curUser['tags']){
			$strTags='';
			foreach ($oldUser['tags'] as $tag){
				if($tag['name']!='РегистрацияНаСайте'){
					$strTags.=$tag['name'].',';
				}
			}
				foreach ($curUser['tags'] as $tag){
					$strTags.=$tag.',';
				}
				$newUser['tags']=substr($strTags,0,-1);
		}

	if($curUser['email']){
			if($oldUser['custom_fields']['email']){
				foreach ($oldUser['custom_fields']['email']['values'] as $email){
					$emails[]=$email['value'];
				}
				$res=in_array($curUser['email'], $emails);
				if(!$res){
					$newUser['custom_fields']['email']['values'][]=array(
						'enum' => 142091,
						'value' => $curUser['email']);
				}
			}
	}

	if($curUser['phone']){
			$nphones=array(
				$curUser['phone'],
				substr($curUser['phone'],2),
				preg_replace('![^0-9]+!','',$curUser['phone']),
				preg_replace('![^0-9]+!', '', substr($curUser['phone'],2)),
				'8'.preg_replace('![^0-9]+!', '', substr($curUser['phone'],2)),
				'+7'.preg_replace('![^0-9]+!', '', substr($curUser['phone'],2))
			);


			if($oldUser['custom_fields']['phone']){
				foreach ($oldUser['custom_fields']['phone']['values'] as $phone){
					$phones[]=$phone['value'];
				}
				foreach ($nphones as $nphone){
					$res=in_array($nphone, $phones);
						if($res){
							break;
						}

				}
				if(!$res){
					$newUser['custom_fields']['phone']['values'][]=array(
						'enum' => '142081',
						'value' => $curUser['phone']);
				}
			}
	}

	if($curUser['region']){
		if(strlen($curUser['region']) >= strlen($oldUser['custom_fields']['region']['values']['value'])){
			$newUser['custom_fields']['region']=array(
				'id'     => 134157, #Регион
				'values' => array(
					array(
						'value' => $curUser['region']
					)
				)
			);
		}

	}

return $newUser;
}

function amo_prepare_reg_cont_to_update($newUser){
	foreach ($newUser['custom_fields'] as $custom_field){
		$custom_fields[]=$custom_field;
	}
	$contacts['update'] = array(
		array(
			'id' => $newUser['id'],
			'name' =>  $newUser['name'],
			'updated_at' => time(),
			'leads_id' => $newUser['leads'],
			'tags' =>  $newUser['tags'],
			'custom_fields'=>$custom_fields
		)
	);
return $contacts;
}

function amo_prepare_reg_cont_to_add($curUser) {
	$strTag='';
	foreach ($curUser['tags'] as $tag){
		$strTag.=$tag.',';
	}

	$contacts['add'] = array(
		array(
			'name'                => $curUser['name'],
			'responsible_user_id' => 2374873, #Пользователь Шатова
			'tags'                => substr($strTag,0,-1),
			'custom_fields'       => array(
				array(
					'id'     => 68239, #Email
					'values' => array(
						array(
							'value' => $curUser['email'],
							'enum'  => 142091
						)
					)
				),
				array(
					'id'     => 68237, #Телефон
					'values' => array(
						array(
							'value' => $curUser['phone'],
							'enum'  => 142081
						)
					)
				),
				array(
					'id'     => 134157, #Регион
					'values' => array(
						array(
							'value' => $curUser['region']
						)
					)
				),
				array(
					'id'     => 134155, #Источник
					'values' => array(
						array(
							'value' => 'Сайт',
							'enum'  => 271555
						)
					)
				)
			)
		)

	);

	return $contacts;
}