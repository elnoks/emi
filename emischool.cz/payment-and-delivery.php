<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Platba a doručení");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Platba a doručení</h1>
   </div>
 
  <br />
    <h3>1. Ceník.</h3>
    <p>1.1 Všechny ceny v e-shopu obsahuji 21%DPH</p>
    <p>1.2 Pro všechny objednávky nad 5000kč platí 5%. sleva (slevy není možné sčítat a lze uplatnit pouze pro jednu objednávku)</p>
    <h3>2. Platba</h3>
    <p>2.1 Všechny platby jsou uskutečněny na základě vystavené faktury.</p>
    <p>2.2 Než objednávka bude uhrazena, zkontrolujte prosím, zda všechny položky na faktuře a jejich ceny jsou uvedeny správně.</p>
    <p>2.3 Akceptujeme tyto platební metody:</p>
    <ul>
        <li>Bankovním převodem</li>
        <li>Paypal</li>
        <li>Platba platební kartou</li>
    </ul>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>