<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Main\Loader;

Bitrix\Main\Loader::includeModule("sale");

$db_sales = CSaleOrderUserProps::GetList(array("DATE_UPDATE" => "DESC"), array("USER_ID" => 30469)); //$arOrder["CREATED_BY"]) );
if ($ar_sales = $db_sales->Fetch())
{
	$db_propVals = CSaleOrderUserPropsValue::GetList(array("ID" => "ASC"), Array("USER_PROPS_ID" => $ar_sales["ID"]));
	while ($arPropVals = $db_propVals->Fetch())
	{
		$arr[$arPropVals["PROP_CODE"]] = $arPropVals["VALUE"];
	}
}
op($arr);


/*
$APPLICATION->IncludeComponent(
	"bitrix:sale.personal.profile.list", 
	"", 
	array(
	
	),
	false
);
*/


/*
CJSCore::Init(array("fx"));
CJSCore::Init(array('ajax')); 
CJSCore::Init(array("popup"));

$APPLICATION->IncludeComponent(
	"bitrix:sale.order.ajax", 
	"order", 
	array(
		"COMPONENT_TEMPLATE" => "order",
		"PAY_FROM_ACCOUNT" => "N",
		"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
		"ALLOW_AUTO_REGISTER" => "N",
		"ALLOW_APPEND_ORDER" => "Y",
		"SEND_NEW_USER_NOTIFY" => "Y",
		"DELIVERY_NO_AJAX" => "N",
		"SHOW_NOT_CALCULATED_DELIVERIES" => "L",
		"DELIVERY_NO_SESSION" => "Y",
		"TEMPLATE_LOCATION" => "popup",
		"SPOT_LOCATION_BY_GEOIP" => "Y",
		"DELIVERY_TO_PAYSYSTEM" => "d2p",
		"SHOW_VAT_PRICE" => "Y",
		"USE_PREPAYMENT" => "N",
		"COMPATIBLE_MODE" => "Y",
		"USE_PRELOAD" => "Y",
		"ALLOW_NEW_PROFILE" => "Y",
		"SHOW_PAYMENT_SERVICES_NAMES" => "Y",
		"SHOW_STORES_IMAGES" => "N",
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N",
		"ACTION_VARIABLE" => "soa-action",
		"PATH_TO_BASKET" => "/personal/cart/",
		"PATH_TO_PERSONAL" => "index.php",
		"PATH_TO_PAYMENT" => "payment.php",
		"PATH_TO_AUTH" => "/auth/",
		"SET_TITLE" => "Y",
		"DISABLE_BASKET_REDIRECT" => "N",
		"EMPTY_BASKET_HINT_PATH" => "/",
		"USE_PHONE_NORMALIZATION" => "Y",
		"PRODUCT_COLUMNS_VISIBLE" => array(
			0 => "PREVIEW_PICTURE",
			1 => "PROPS",
		),
		"ADDITIONAL_PICT_PROP_18" => "-",
		"ADDITIONAL_PICT_PROP_30" => "-",
		"ADDITIONAL_PICT_PROP_36" => "-",
		"ADDITIONAL_PICT_PROP_44" => "-",
		"ADDITIONAL_PICT_PROP_48" => "-",
		"ADDITIONAL_PICT_PROP_51" => "-",
		"ADDITIONAL_PICT_PROP_54" => "-",
		"ADDITIONAL_PICT_PROP_56" => "-",
		"ADDITIONAL_PICT_PROP_81" => "-",
		"ADDITIONAL_PICT_PROP_82" => "-",
		"ADDITIONAL_PICT_PROP_84" => "-",
		"ADDITIONAL_PICT_PROP_85" => "-",
		"ADDITIONAL_PICT_PROP_88" => "-",
		"ADDITIONAL_PICT_PROP_89" => "-",
		"ADDITIONAL_PICT_PROP_90" => "-",
		"ADDITIONAL_PICT_PROP_91" => "-",
		"ADDITIONAL_PICT_PROP_92" => "-",
		"ADDITIONAL_PICT_PROP_96" => "-",
		"ADDITIONAL_PICT_PROP_97" => "-",
		"ADDITIONAL_PICT_PROP_98" => "-",
		"ADDITIONAL_PICT_PROP_99" => "-",
		"ADDITIONAL_PICT_PROP_103" => "-",
		"ADDITIONAL_PICT_PROP_104" => "-",
		"ADDITIONAL_PICT_PROP_105" => "-",
		"ADDITIONAL_PICT_PROP_106" => "-",
		"ADDITIONAL_PICT_PROP_108" => "-",
		"ADDITIONAL_PICT_PROP_109" => "-",
		"ADDITIONAL_PICT_PROP_111" => "-",
		"ADDITIONAL_PICT_PROP_112" => "-",
		"ADDITIONAL_PICT_PROP_114" => "-",
		"ADDITIONAL_PICT_PROP_116" => "-",
		"ADDITIONAL_PICT_PROP_123" => "-",
		"ADDITIONAL_PICT_PROP_125" => "-",
		"ADDITIONAL_PICT_PROP_132" => "-",
		"ADDITIONAL_PICT_PROP_134" => "-",
		"ADDITIONAL_PICT_PROP_135" => "-",
		"ADDITIONAL_PICT_PROP_139" => "-",
		"ADDITIONAL_PICT_PROP_142" => "-",
		"ADDITIONAL_PICT_PROP_144" => "-",
		"ADDITIONAL_PICT_PROP_147" => "-",
		"ADDITIONAL_PICT_PROP_149" => "-",
		"ADDITIONAL_PICT_PROP_152" => "-",
		"ADDITIONAL_PICT_PROP_153" => "-",
		"ADDITIONAL_PICT_PROP_154" => "-",
		"ADDITIONAL_PICT_PROP_156" => "-",
		"ADDITIONAL_PICT_PROP_158" => "-",
		"ADDITIONAL_PICT_PROP_160" => "-",
		"ADDITIONAL_PICT_PROP_169" => "-",
		"ADDITIONAL_PICT_PROP_170" => "-",
		"ADDITIONAL_PICT_PROP_171" => "-",
		"ADDITIONAL_PICT_PROP_176" => "-",
		"ADDITIONAL_PICT_PROP_177" => "-",
		"ADDITIONAL_PICT_PROP_178" => "-",
		"ADDITIONAL_PICT_PROP_179" => "-",
		"ADDITIONAL_PICT_PROP_181" => "-",
		"ADDITIONAL_PICT_PROP_182" => "-",
		"ADDITIONAL_PICT_PROP_184" => "-",
		"ADDITIONAL_PICT_PROP_186" => "-",
		"ADDITIONAL_PICT_PROP_188" => "-",
		"ADDITIONAL_PICT_PROP_192" => "-",
		"ADDITIONAL_PICT_PROP_193" => "-",
		"ADDITIONAL_PICT_PROP_194" => "-",
		"ADDITIONAL_PICT_PROP_200" => "-",
		"ADDITIONAL_PICT_PROP_201" => "-",
		"ADDITIONAL_PICT_PROP_202" => "-",
		"ADDITIONAL_PICT_PROP_203" => "-",
		"ADDITIONAL_PICT_PROP_206" => "-",
		"ADDITIONAL_PICT_PROP_207" => "-",
		"ADDITIONAL_PICT_PROP_208" => "-",
		"ADDITIONAL_PICT_PROP_212" => "-",
		"ADDITIONAL_PICT_PROP_218" => "-",
		"ADDITIONAL_PICT_PROP_219" => "-",
		"ADDITIONAL_PICT_PROP_221" => "-",
		"ADDITIONAL_PICT_PROP_222" => "-",
		"ADDITIONAL_PICT_PROP_225" => "-",
		"ADDITIONAL_PICT_PROP_226" => "-",
		"ADDITIONAL_PICT_PROP_229" => "-",
		"ADDITIONAL_PICT_PROP_230" => "-",
		"ADDITIONAL_PICT_PROP_231" => "-",
		"ADDITIONAL_PICT_PROP_248" => "-",
		"ADDITIONAL_PICT_PROP_251" => "-",
		"BASKET_IMAGES_SCALING" => "adaptive",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);
*/
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>