<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Reklamační řád");
?><p>
</p>
<p>
 <b>Reklamační řád</b>
</p>
<p>
</p>
<p>
	Tento Reklamační řád upravuje způsob a podmínky reklamace vad zboží zakoupeného prostřednictvím on-line obchodu emischool.com provozovaného naší společností <br>
	 E.Mi - International s.r.o., se sídlem U božích bojovníků 89/1, 130 00, Praha 3 - Žižkov <br>
	 IČ: 24214647 <br>
	 DIČ: CZ24214647 <br>
	 zapsanou v&nbsp;obchodním rejstříku vedeném u Městského soudu v&nbsp;Praze, oddíl C, vložka 189332 <br>
	 Adresa pro doručování: Štefánikova 203/23, 150 00, Praha 5 – Smíchov <br>
	 Pro vyřizování reklamací doporučujeme přednostně využít adresu pro doručování. <br>
	 Telefonní číslo: 773 208 276 <br>
	 Kontaktní e-mail: <a href="mailto:prague@emischool.com">prague@emischool.com</a> <br>
	 &nbsp;
</p>
<p>
</p>
<p>
 <b>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Za jaké vady zboží odpovídáme?</b>
</p>
<p>
	 Jako prodávající odpovídáme za to, že zboží při převzetí nemá vady. To znamená, že zboží:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; má vlastnosti, které mezi námi byly ujednány, které popisujeme nebo které jste mohli očekávat s ohledem na povahu zboží a na základě reklamy;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; je v&nbsp;odpovídajícím množství, míře nebo hmotnosti, a
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; vyhovuje požadavkům právních předpisů.
</p>
<p>
	 Za vadu zboží nelze považovat rozdílnost odstínů barev ve skutečnosti a na elektronických zobrazovacích zařízeních. Pokud zboží neodpovídá Vaší představě, máte v&nbsp;případě, že jste spotřebitelem, právo odstoupit od smlouvy do 14 dnů od převzetí zboží v&nbsp;souladu s&nbsp;článkem 5 Všeobecných obchodních podmínek.
</p>
<p>
 <b>Projeví-li se vada v průběhu šesti měsíců od převzetí, má se za to, že zboží bylo vadné již při převzetí.</b>
</p>
<p>
	 Spotřebitelům dále odpovídáme za to, že se vady nevyskytnou v&nbsp;záruční době. <b>Nejste-li spotřebitelem, není Vám záruka za jakost poskytována.</b>Článek 2 se vztahuje pouze na spotřebitele.
</p>
<p>
	 &nbsp;
</p>
<p>
 <b>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jaká je záruční doba?</b>
</p>
<p>
	 U nepoužitého spotřebního zboží činí záruční doba <b>dvacet čtyři měsíců od převzetí zboží</b>, není-li na webovém rozhraní nebo v&nbsp;dokumentech přiložených ke zboží stanovena delší záruční doba.
</p>
<p>
	 Pokud je na zboží uvedeno datum minimální trvanlivosti, trvá záruční doba do takového data.
</p>
<p>
	 &nbsp;
</p>
<p>
 <b>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jaká práva z&nbsp;vadného plnění máte?</b>
</p>
<p>
	 V případě, že se v průběhu záruční doby vyskytne vada zboží, pro kterou nemůže být zakoupený výrobek používán zcela řádně, a tuto vadu lze odstranit, máte právo na jeho <b>bezplatnou opravu.</b>
</p>
<p>
	 U odstranitelné vady na dosud nepoužitém výrobku můžete místo odstranění vady požadovat <b>výměnu vadného výrobku za bezvadný</b>, nebo <b>přiměřenou slevu z&nbsp;kupní ceny</b>.
</p>
<p>
	 V případě vady, kterou nelze odstranit a která brání tomu, aby zboží mohlo být řádně užíváno jako zboží bez vady, máte právo na <b>výměnu zboží</b>, na <b>přiměřenou slevu z&nbsp;kupní ceny</b> anebo máte právo od kupní smlouvy <b>odstoupit.</b>
</p>
<p>
	 Právo na přiměřenou slevu máte i v případě, že nejsme schopni dodat Vám novou věc bez vad, vyměnit její součást nebo věc opravit, nebo v případě, že nezjednáme nápravu v přiměřené době nebo že by Vám zjednání nápravy působilo značné obtíže.
</p>
<p>
	 Právo odstoupit od smlouvy nebo právo požadovat dodání nové věci nemáte v&nbsp;případě, že nemůžete zboží vrátit ve stavu, v&nbsp;jakém jste jej obdrželi (s výjimkou případů stanovených v&nbsp;ustanovení § 2110 OZ).
</p>
<p>
	 &nbsp;
</p>
<p>
 <b>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kdy nelze práva z&nbsp;vadného plnění uplatnit?</b>
</p>
<p>
	 Práva z&nbsp;vadného plnění Vám nenáleží, pokud:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; jste o vadě před převzetím věci věděli;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; jste vadu sami způsobili;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; uplynula záruční doba.
</p>
<p>
	 Záruka a&nbsp;nároky z&nbsp;odpovědnosti za vady se dále nevztahují na:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; opotřebení zboží způsobené jeho užíváním;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; vady způsobené nesprávným užíváním zboží, nedodržením návodu, nevhodnou údržbou nebo nesprávným skladováním.
</p>
<p>
	 U věcí prodávaných za nižší cenu neodpovídáme za vadu, pro kterou byla nižší cena sjednána.
</p>
<p>
	 Neodpovídáme za újmu na zdraví osob, případně újmu na majetku a zboží, které budou zapříčiněné neodborným zacházením, či zneužitím zboží, popřípadě nedbalostí.&nbsp;
</p>
<p>
	 &nbsp;
</p>
<p>
 <b>5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jak postupovat při reklamaci?</b>
</p>
<p>
	 Reklamaci u naší společnosti (nebo osoby, která je na webovém rozhraní uvedena jako osoba určená k&nbsp;opravě) uplatněte bez zbytečného odkladu od zjištění vady.&nbsp;
</p>
<p>
	 Reklamaci lze uplatnit následujícím způsobem:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; pro rychlejší vyřízení nás o reklamaci můžete předem informovat telefonicky, e-mailem či písemně.
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; reklamované zboží Vám pro co nejrychlejší vyřízení reklamace doporučujeme zaslat (jinak než na dobírku, kterou nepřebíráme) na naši adresu pro doručování (nebo na adresu osoby určené k&nbsp;opravě). Zboží můžete zaslat rovněž na adresu našeho sídla či do jakékoliv naší provozovny.
</p>
<p>
	 Při zasílání zabalte zboží do vhodného obalu tak, aby nedošlo k jeho poškození či zničení.
</p>
<p>
 <b>Ke zboží je vhodné přiložit doklad o zakoupení zboží či daňový doklad - fakturu, byla-li vystavena, nebo jiný dokument prokazující koupi zboží, spolu s popisem vady a&nbsp;návrhem na způsob řešení reklamace. Nepředložení kteréhokoliv z&nbsp;výše uvedených dokumentů nebrání kladnému vyřízení reklamace dle zákonných podmínek.</b>
</p>
<p>
	 Okamžikem uplatnění reklamace je okamžik, kdy nám byl oznámen výskyt vady a uplatněno právo z odpovědnosti za vady prodané věci.
</p>
<p>
 <b>Doručené reklamace vyřizujeme bez zbytečného odkladu, nejpozději však do 30 dnů ode dne uplatnění reklamace</b>, pokud se nedohodneme jinak. O uplatnění a vyřízení reklamace Vám vystavíme písemné potvrzení.
</p>
<p>
 <b>V&nbsp;případě sporné reklamace rozhodneme o jejím přijetí do tří pracovních dnů ode dne uplatnění reklamace.</b>
</p>
 <br>
<p>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>