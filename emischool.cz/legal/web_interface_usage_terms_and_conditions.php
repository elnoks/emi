<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Velkoobchod");
?><p>
</p>
<p>
 <b>Všeobecné obchodní podmínky pro velkoobchod</b>
</p>
<p>
	 Tyto Všeobecné obchodní podmínky pro velkoobchod (dále jen „<b>obchodní podmínky</b>“) se vztahují na smlouvy uzavřené prostřednictvím on-line obchodu E.MI - School of Nail Design by Ekaterina Miroshnichenko umístěného na webovém rozhraní <a href="http://www.emischool.com">www.emischool.com</a> (dále jen „<b>webové rozhraní</b>“) mezi společností
</p>
<p>
	 E.Mi - International s.r.o., se sídlem U božích bojovníků 89/1, 130 00, Praha 3 - Žižkov
</p>
<p>
	 IČ: 24214647
</p>
<p>
	 DIČ: CZ24214647
</p>
<p>
	 zapsanou v&nbsp;obchodním rejstříku vedeném u Městského soudu v&nbsp;Praze, oddíl C, vložka 189332
</p>
<p>
	 Adresa pro doručování: E.Mi - International s.r.o., Štefánikova 203/23, 150 00, Praha 5 - Smíchov
</p>
<p>
	 Telefonní číslo: 773 208 276
</p>
<p>
	 Kontaktní e-mail: <a href="mailto:prague@emischool.com">prague@emischool.com</a>
</p>
<p>
	 jako <b>prodávajícím</b>
</p>
<p>
	 a podnikatelem či právnickou osobou
</p>
<p>
	 jako <b>kupujícím</b>
</p>
<p>
	 (oba dále společně též jen jako „<b>smluvní strany</b>“).
</p>
<p>
	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</p>
<p>
 <b>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Úvodní ustanovení</b>
</p>
<p>
	 1.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Obchodní podmínky vymezují a upřesňují základní práva a povinnosti smluvních stran při uzavíraní smlouvy o prodeji zboží či další smlouvy zde uvedené (dále společně jen jako „<b>smlouva</b>“) prostřednictvím webového rozhraní.
</p>
<p>
	 1.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ustanovení obchodních podmínek jsou nedílnou součástí smlouvy. Ustanovení odchylná od obchodních podmínek je možné sjednat ve smlouvě. Odchylná ujednání ve smlouvě mají přednost před ustanovením obchodních podmínek. Znění obchodních podmínek může prodávající měnit či doplňovat. Práva a povinnosti smluvních stran se řídí vždy tím zněním obchodních podmínek, za jehož účinnosti vznikly. Práva a povinnosti smluvních stran se dále řídí Podmínkami užití webového rozhraní, podmínkami a pokyny uvedenými na webovém rozhraní zejména při uzavírání smlouvy či registraci kupujícího na webovém rozhraní a objednávkou a jejím přijetím ze strany prodávajícího. V&nbsp;otázkách zde neupravených se vztahy smluvních stran řídí právními předpisy, zejména zákonem č.&nbsp;89/2012 Sb., občanský zákoník, v&nbsp;platném znění (dále jen „<b>občanský zákoník</b>“).
</p>
<p>
	 1.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Smlouva a obchodní podmínky jsou vyhotoveny v&nbsp;českém jazyce. Smlouvu lze uzavřít v&nbsp;českém jazyce, ledaže se smluvní strany výslovně dohodnou na&nbsp;jiném jazyce.
</p>
<p>
	 1.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tyto obchodní podmínky se vztahují na kupující, kteří jsou podnikateli, a na právnické osoby. Na smlouvy uzavírané se spotřebitelem se neuplatní tyto obchodní podmínky, ale Všeobecné obchodní podmínky pro maloobchod. V&nbsp;otázkách neupravených těmito obchodními podmínkami pro velkoobchod se vztahy řídí obdobně dle podmínek upravených ve Všeobecných obchodních podmínkách pro maloobchod, vyjma ustanovení na ochranu spotřebitele.
</p>
<p>
	 1.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Zasláním objednávky kupující stvrzuje, že se s&nbsp;těmito obchodními podmínkami seznámil.
</p>
<p>
 <b>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Objednávka a uzavření smlouvy</b>
</p>
<p>
	 2.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Na webovém rozhraní je uveden seznam zboží včetně popisu hlavních vlastností jednotlivých položek. <b>Prezentace zboží uvedená na webovém rozhraní je informativního charakteru a nejedná se o návrh prodávajícího na uzavření smlouvy ve smyslu ustanovení § 1732 odst. 2 občanského zákoníku.</b> Pro uzavření smlouvy je nutné, aby kupující odeslal objednávku, aby došlo k&nbsp;přijetí této objednávky prodávajícím a aby kupující provedl platbu za zboží způsobem uvedeným v&nbsp;čl. 4.3. těchto podmínek.
</p>
<p>
	 Kupující provádí objednávku prostřednictvím webového rozhraní (vyplněním formuláře). Pro podání objednávky je nutné se na webovém rozhraní registrovat. Podmínky registrace jsou upraveny v&nbsp;Podmínkách užití webového rozhraní.
</p>
<p>
	 2.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Prodávající není povinen obdrženou objednávku potvrdit. Nepotvrzená objednávka není pro prodávajícího závazná. Prodávající je oprávněn ověřit si objednávku v&nbsp;případě pochybností o pravosti a vážnosti objednávky. Neověřenou objednávku může prodávající odmítnout.
</p>
<p>
	 2.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Po potvrzení objednávky prodávající informuje kupujícího též o nákladech na dodání zboží, a to zasláním faktury, ve které bude uvedena celková kupní cena zboží včetně nákladů na dodání zboží. Fakturu zašle prodávající kupujícímu na e-mailovou adresu uvedenou v&nbsp;objednávce. Zaslání informace o nákladech na dodání zboží, resp. faktury je svou povahou přijetím objednávky ze strany prodávajícího.
</p>
<p>
	 2.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Smlouva je uzavřena okamžikem, kdy prodávající uhradí kupní cenu zboží, včetně nákladů na dodání zboží způsobem uvedeným v&nbsp;čl. 4.3. těchto podmínek.
</p>
<p>
	 2.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pokud kupující činí objednávku z&nbsp;jiného státu, než je Česká republika, a zároveň v&nbsp;tomto státu je činný národní distributor zboží prodávaného prodávajícím, bude objednávka kupujícího předána k&nbsp;vyřízení tomuto distributorovi.
</p>
<p>
	 2.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Objednávku, kterou prodávající dosud nepřijal, může kupující zrušit telefonicky nebo e-mailem. Všechny objednávky přijaté prodávajícím jsou závazné. Přijatou objednávku je možné bez dalšího zrušit v&nbsp;případě, že kupující nesouhlasí s&nbsp;výší nákladů na dodání zboží, a to telefonicky nebo e-mailem. Jinak je přijatou objednávku možné zrušit pouze po dohodě s&nbsp;prodávajícím. V&nbsp;případě takového zrušení objednávky ze strany kupujícího má prodávající nárok na storno poplatek ve výši 50% z&nbsp;ceny zboží.
</p>
<p>
 <b>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dodací podmínky</b>
</p>
<p>
	 3.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Prodávající je povinen dodat zboží kupujícímu dohodnutým způsobem, řádně zabalené a&nbsp;vybavené potřebnými doklady. Potřebnými doklady jsou zejména návody k použití, certifikáty a další dokumenty nezbytné pro převzetí a užívání zboží. Není-li dohodnuto jinak, jsou doklady poskytovány v&nbsp;českém jazyce.
</p>
<p>
	 3.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Na základě dohody smluvních stran může prodávající zajistit pro kupujícího přepravu zboží a pojištění zboží po dobu přepravy. Cenu dopravy i pojištění je povinen hradit kupující. Za dodání zboží kupujícímu se považuje předání zboží prvnímu dopravci. Dodáním zboží přechází na kupujícího nebezpečí škody na zboží. Doba dodání zboží vždy závisí vzdálenosti dodací adresy, resp. cílového státu, na jeho dostupnosti a na zvoleném způsobu platby.
</p>
<p>
	 Zboží, které je skladem, prodávající zpravidla předá dopravci dvou pracovních dnů od připsání platby na účet prodávajícího či od oznámení o provedení platby prostřednictvím platebních míst Western Union.
</p>
<p>
	 3.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Lhůta pro expedici konkrétního zboží může být uvedena v&nbsp;detailu zboží na webovém rozhraní. Zboží, které není skladem, předá prodávající dopravci, jakmile je to možné.
</p>
<p>
	 3.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Před převzetím zboží je kupující povinen zkontrolovat neporušenost obalu zboží a případné závady neprodleně oznámit přepravci. O závadách bude sepsán protokol. Není-li sepsán protokol o závadách, ztrácí kupující nároky plynoucí z&nbsp;porušeného obalu zboží.
</p>
<p>
	 3.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; V&nbsp;případě, že kupující zboží bezdůvodně nepřevezme, má prodávající vedle nároku na náhradu nákladů spojených s&nbsp;uskladněním zboží a dalších nákladů, které mu z&nbsp;důvodu nepřevzetí zboží kupujícím vzniknou, i nárok na zaplacení smluvní pokuty ve výši 0,5 %&nbsp;z&nbsp;ceny zboží denně za každý den prodlení kupujícího s&nbsp;převzetím zboží.
</p>
<p>
	 3.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Neprodleně po převzetí zboží je kupující povinen zkontrolovat zboží, zejména je povinen zkontrolovat počet kusů zboží a jeho kompletnost. V&nbsp;případě zjištění nesouladu je povinen jej oznámit kupujícímu bez zbytečného odkladu, nejpozději však do 2 pracovních dnů od převzetí zboží. Zjištěné závady je kupující povinen vhodným způsobem zdokumentovat a&nbsp;tuto dokumentaci zaslat prodávajícímu spolu s&nbsp;oznámením závady.
</p>
<p>
 <b>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Platební podmínky</b>
</p>
<p>
	 4.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Na webovém rozhraní jsou uvedeny ceny zboží včetně DPH. Z&nbsp;těchto cen je Prodávající oprávněn poskytnout Kupujícímu slevu, a to zejména v&nbsp;závislosti na množství objednávaného zboží nebo na předchozích nákupech Kupujícího.
</p>
<p>
	 4.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pokud na straně prodávajícího dojde ke zjevné technické chybě při uvedení ceny zboží či výše slevy ve webovém rozhraní nebo v&nbsp;průběhu objednávání, není prodávající povinen dodat kupujícímu zboží za tuto zcela zjevně chybnou cenu či s&nbsp;touto zcela zjevně chybnou výší slevy, a to ani v&nbsp;případě, že prodávající přijal objednávku kupujícího dle těchto obchodních podmínek. Obdobné platí i v&nbsp;případě, že cena či výše slevy uvedené ve webovém rozhraní či v&nbsp;průběhu objednávání již nejsou aktuální, či pokud se cena či výše slevy změní v&nbsp;mezidobí mezi odesláním objednávky a jejím přijetím prodávajícím.
</p>
<p>
	 4.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kupující má možnost zaplatit kupní cenu za zboží prodávajícímu mimo dalších způsobů uvedených ve webovém rozhraní nebo individuálně dohodnutých i některým z níže uvedených způsobů:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; v&nbsp;hotovosti prostřednictvím platebních míst Western Union;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; bezhotovostně před dodáním zboží převodem na bankovní účet prodávajícího (informace k&nbsp;provedení platby budou kupujícímu zaslány prodávajícím.
</p>
<p>
	 Prodávající je oprávněn neumožnit placení zboží po dodání zboží. Tento způsob platby je zpravidla vyhrazen pro stálé zákazníky. Není-li dohodnuto jinak, je faktura přiložena ke zboží.
</p>
<p>
	 4.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; V případě platby v&nbsp;hotovosti prostřednictvím platebních míst Western Union i v&nbsp;případě bezhotovostní platby je cena splatná do sedmi&nbsp;dnů od zaslání informace o nákladech na dodání zboží, resp. faktury (tj. přijetí objednávky). Cena je při bezhotovostní platbě uhrazena okamžikem připsání příslušné částky na bankovní účet prodávajícího, při platbě v&nbsp;hotovosti prostřednictvím platebních míst Western Union okamžikem potvrzení provedení platby ze strany Western Union. O provedení platby prostřednictvím platebního místa Western Union je nutné prodávajícího informovat a sdělit mu údaje potřebné k&nbsp;výplatě peněz.
</p>
<p>
	 4.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; V&nbsp;případě prodlení kupujícího s&nbsp;úhradou ceny zboží je prodávající rovněž oprávněn pozastavit další dohodnuté dodávky zboží, a to až do okamžiku úhrady všech splatných závazků kupujícího.
</p>
<p>
	 4.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Platba zboží je možná v&nbsp;Eurech (EUR).
</p>
<p>
 <b>5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Odstoupení od smlouvy</b>
</p>
<p>
	 5.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Do doby převzetí zboží kupujícím je prodávající oprávněn kdykoliv od kupní smlouvy odstoupit.&nbsp;V&nbsp;takovém případě vrátí prodávající kupujícímu kupní cenu bezhotovostně na účet sdělený mu pro tento účel kupujícím nebo účet, ze kterého byly prostředky poukázány k&nbsp;úhradě kupní ceny (pokud kupující žádný do 5 dnů od odstoupení prodávajícímu nesdělí).
</p>
<p>
	 5.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Prodávající je dále oprávněn odstoupit od smlouvy, je-li kupující v&nbsp;prodlení s&nbsp;úhradou kupní ceny zboží déle než 4 týdny.
</p>
<p>
	 5.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kupující je oprávněn odstoupit od smlouvy, je-li prodávající v&nbsp;prodlení s&nbsp;dodáním zboží déle než 4 týdny od sjednaného dne dodání.
</p>
<p>
	 5.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kupující není oprávněn odstoupit od smlouvy ohledně zboží, které bylo dodáno řádně, včas a bez vad.
</p>
<p>
	 5.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Odstoupení od smlouvy musí být učiněno písemně a v&nbsp;případech smluv sjednaných elektronickou cestou též elektronicky. Odstoupení od smlouvy je účinné doručením oznámení o odstoupení druhé smluvní straně.
</p>
<p>
	 5.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Byl-li společně se zbožím poskytnut dárek, pozbývá darovací smlouva odstoupením od smlouvy kteroukoliv ze smluvních stran účinnosti.
</p>
<p>
 <b>6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Práva z&nbsp;vadného plnění</b>
</p>
<p>
	 6.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Práva kupujícího z&nbsp;vadného plnění se řídí příslušnými obecně závaznými právními předpisy (zejména ustanoveními § 1914 až 1915 a § 2099 až 2117 občanského zákoníku).
</p>
<p>
 <b>7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ochrana obchodního tajemství a obchodní politiky prodávajícího</b>
</p>
<p>
	 7.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Při sjednávání smlouvy a jejím plnění mohou být kupujícímu sděleny informace, které jsou označeny jako důvěrné nebo jejichž důvěrnost vyplývá z&nbsp;jejich povahy. Kupující se zavazuje tyto informace zejména:
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; uchovávat jako důvěrné;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; neposkytnout je bez souhlasu prodávajícího jiné osobě;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; nevyužít je k jinému účelu, než k&nbsp;plnění smlouvy;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; nevyužít jakýmkoliv jiným poškozujícím způsobem.
</p>
<p>
	 7.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dále se kupující zavazuje, že nebude bez souhlasu prodávajícího pořizovat kopie podkladů předaných mu prodávajícím.
</p>
<p>
 <b>8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Závěrečná ustanovení</b>
</p>
<p>
	 8.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pokud vztah související s užitím webového rozhraní nebo právní vztah založený smlouvou obsahuje mezinárodní (zahraniční) prvek, pak smluvní strany sjednávají, že vztah se řídí českým právem.
</p>
<p>
	 8.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Je-li některé ustanovení obchodních podmínek neplatné nebo neúčinné nebo nepoužitelné, nebo se takovým stane, namísto neplatných ustanovení nastoupí, jehož smysl se neplatnému ustanovení co nejvíce přibližuje. Neplatností nebo neúčinností nebo nepoužitelností jednoho ustanovení není dotknutá platnost ostatních ustanovení. Změny a&nbsp;doplňky smlouvy či obchodních podmínek vyžadují písemnou formu.
</p>
<p>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>