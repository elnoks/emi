<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Podmínky užití webového rozhraní");
?><p>
 <b>Podmínky užití webového rozhraní</b>
</p>
<p>
	 Nacházíte se na webovém rozhraní <a href="http://www.emischool.com">www.emischool.com</a> (dále jen „<b>webové rozhraní</b>“), které provozuje naše společnost
</p>
<p>
	 E.Mi - International s.r.o., se sídlem U božích bojovníků 89/1, 130 00, Praha 3 - Žižkov
</p>
<p>
	 IČ: 24214647
</p>
<p>
	 DIČ: CZ24214647
</p>
<p>
	 zapsaná v&nbsp;obchodním rejstříku vedeném u Městského soudu v&nbsp;Praze, oddíl C, vložka 189332
</p>
<p>
	 Adresa pro doručování: Štefánikova 203/23, 150 00, Praha 5 - Smíchov
</p>
<p>
	 Telefonní číslo: + 420 773 208 276
</p>
<p>
	 Kontaktní e-mail: <a href="mailto:prague@emischool.com">prague@emischool.com</a>
</p>
<p>
	 &nbsp;
</p>
<p>
	 Vezměte, prosíme, na vědomí, že bez ohledu na to, zda prostřednictvím webového rozhraní nakupujete či jej pouze navštěvujete, je nutné se řídit dále uvedenými pravidly, která vymezují a&nbsp;upřesňují podmínky užívání všech funkčních součástí webového rozhraní.
</p>
<p>
 <b>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Registrace na webovém rozhraní</b>
</p>
<p>
	 Pro objednávání zboží je nutné se na webovém rozhraní registrovat. Na webovém rozhraní se registrujete prostřednictvím registračního formuláře tam dostupného. Do registračního formuláře je nutné vyplnit požadované údaje, zejména jméno, příjmení a kontaktní e-mail. Registrací je založen uživatelský účet.
</p>
<p>
	 Pro přístup do uživatelského účtu je potřeba uživatelské jméno a heslo. <b>Přístupové údaje do uživatelského účtu uchovávejte v&nbsp;tajnosti. Naše společnost nenese odpovědnost za případné zneužití uživatelského účtu třetí osobou.</b>
</p>
<p>
	 Informace uváděné při registraci musí být pravdivé a úplné.<b> Účet, při jehož založení byly použity nepravdivé nebo neúplné údaje, můžeme bez náhrady zrušit.</b>
</p>
<p>
	 V&nbsp;případě změn ve Vašich údajích doporučujeme provést jejich bezodkladnou úpravu v&nbsp;uživatelském účtu.
</p>
<p>
	 Prostřednictvím uživatelského účtu můžete především objednávat zboží, sledovat objednávky a&nbsp;spravovat uživatelský účet. Případné další funkce uživatelského účtu jsou vždy uvedeny na webovém rozhraní.
</p>
<p>
 <b>Vezměte, prosíme, na vědomí, že máme právo Váš uživatelský účet bez náhrady zrušit, pokud prostřednictvím Vašeho účtu dochází k&nbsp;porušování dobrých mravů, platných právních předpisů nebo těchto podmínek užití.</b>
</p>
<p>
	 &nbsp;
</p>
<p>
 <b>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ochrana osobních údajů</b>
</p>
<p>
	 Při vyplnění objednávky nebo při registraci na webovém rozhraní nám poskytujete některé Vaše osobní údaje. Dále při užívání webového rozhraní dochází k získávání, uchovávání a zpracovávání dalších údajů, k&nbsp;nimž máme přístup. <b>Zadáním osobních údajů a užíváním webového rozhraní souhlasíte se zpracováváním a shromažďováním svých osobních údajů v&nbsp;dále uvedeném rozsahu a k&nbsp;dále uvedeným účelům</b>, a to až do doby vyjádření nesouhlasu s&nbsp;takovým zpracováním.
</p>
<p>
	 Ochrana osobních údajů je pro nás velice důležitá. Při nakládání s&nbsp;osobními údaji proto postupujeme v&nbsp;souladu s&nbsp;právním řádem České republiky, zejména se zákonem č. 101/2000 Sb., o&nbsp;ochraně osobních údajů (dále jen „<b>ZOOÚ</b>“), ve znění pozdějších předpisů.
</p>
<p>
 <b>2.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Co jsou osobní a další údaje?</b>
</p>
<p>
 <b>Osobní údaje</b> jsou ty údaje, které nám dobrovolně poskytujete v&nbsp;rámci vyplnění objednávky či registrace. Osobními údaji se rozumí veškeré informace, které identifikují nebo mohou identifikovat konkrétní osobu. Osobními údaji jsou zejména, avšak nikoliv výlučně, jméno a&nbsp;příjmení, fotografie, datum narození, e-mailová adresa a adresa bydliště či telefonní číslo.
</p>
<p>
 <b>Další údaje</b>, které získáváme automaticky v&nbsp;souvislosti s&nbsp;využíváním webového rozhraní, jsou IP adresa, typ prohlížeče, zařízení a operačního systému, doba a počet přístupů na webové rozhraní, informace získané pomocí souborů cookie a další obdobné informace. <b>Upozorňujeme, že tyto další údaje můžeme získávat i bez registrace a bez ohledu na to, zda na webovém rozhraní nakupujete nebo ne.</b>
</p>
<p>
 <b>2.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jak využíváme osobní a další údaje?</b>
</p>
<p>
	 Prostřednictvím osobních a dalších údajů Vám především umožňujeme přístup k Vašemu účtu a co nejsnazší užívání webového rozhraní.
</p>
<p>
	 Údaje dále využíváme pro komunikaci ohledně správy Vašeho účtu a pro uživatelskou podporu. Údaje mohou být použity pro zlepšování našich služeb, a to včetně využívání analýzy chování uživatelů webového rozhraní.
</p>
<p>
	 Údaje mohou být využity k obchodním a marketingovým účelům, tj. k&nbsp;vedení databáze uživatelů webového rozhraní a k nabízení zboží a služeb, a to po dobu neurčitou. Odesláním objednávky nebo registrací souhlasíte se zasíláním obchodních sdělení všemi elektronickými prostředky.&nbsp;
</p>
<p>
	 Souhlas se zasíláním obchodních sdělení a elektronické pošty za účelem přímého marketingu můžete kdykoliv odvolat formou e-mailu na naši kontaktní e-mailovou adresu.
</p>
<p>
 <b>2.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jak spravujeme a zpracováváme Vaše osobní údaje?</b>
</p>
<p>
	 Naše společnost je správcem osobních údajů ve smyslu ZOOÚ, a je vedena v&nbsp;registru u&nbsp;Úřadu pro ochranu osobních údajů pod registračním číslem _____________.
</p>
<p>
	 Zpracováním Vašich osobních a dalších údajů můžeme pověřit třetí osobu jakožto zpracovatele.
</p>
<p>
	 Osobní i další získávané údaje jsou plně zabezpečeny proti zneužití.
</p>
<p>
	 Osobní údaje budou zpracovávány po dobu neurčitou. Osobní údaje budou zpracovávány v&nbsp;elektronické podobě automatizovaným způsobem nebo v tištěné podobě neautomatizovaným způsobem.
</p>
<p>
 <b>2.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Komu předáváme Vaše osobní údaje?</b>
</p>
<p>
	 Vaše osobní údaje nepředáváme žádné další osobě. Výjimku představují externí dopravci a&nbsp;osoby podílející se na dodání zboží, případně poskytnutí služby. Takovým osobám jsou Vaše osobní údaje předávány v&nbsp;minimálním rozsahu, který je nutný pro doručení zboží nebo poskytnutí služby.
</p>
<p>
 <b>2.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jaká jsou Vaše práva v&nbsp;souvislosti s&nbsp;osobními údaji?</b>
</p>
<p>
	 Máte právo přístupu ke svým osobním údajům a právo na informace o jejich zpracování (informace o účelu zpracování, informace o zdrojích těchto údajů a informace o příjemci). Tyto informace Vám budou bez zbytečného odkladu poskytnuty na požádání. Dále máte právo na opravu osobních údajů a další zákonná práva k těmto údajům.
</p>
<p>
	 Na základě Vaší písemné žádosti odstraníme Vaše osobní údaje z&nbsp;databáze.
</p>
<p>
	 Pokud se domníváte, že naše společnost nebo zpracovatel osobních údajů provádí zpracování Vašich osobních údajů, které je v&nbsp;rozporu se zákonem, můžete:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; požádat nás nebo zpracovatele o vysvětlení;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; požádat, abychom my nebo zpracovatel odstranili takto vzniklý stav. Zejména se může jednat o blokování, provedení opravy, doplnění nebo likvidaci osobních údajů.
</p>
<p>
	 Při ochraně Vašich osobních údajů Vám vyjdeme maximálně vstříc. Pokud ovšem s&nbsp;vyřízením nebudete spokojeni, máte právo obrátit se na příslušné orgány, zejména na Úřad pro ochranu osobních údajů. Tímto ustanovením není dotčeno Vaše oprávnění obrátit se se svým podnětem na Úřad pro ochranu osobních údajů přímo.
</p>
<p>
	 Za poskytnutí informací o zpracování osobních údajů můžeme požadovat přiměřenou úhradu nepřevyšující náklady nezbytné na poskytnutí informace.
</p>
<p>
	 Dozor nad ochranou osobních údajů provádí Úřad pro ochranu osobních údajů (<a href="http://www.uoou.cz)">http://www.uoou.cz)</a>.
</p>
<p>
	 Naše společnost i případní zpracovatelé osobních údajů mají sídlo v&nbsp;České republice.
</p>
<p>
	 &nbsp;
</p>
<p>
 <b>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Google Analytics a soubory cookie</b>
</p>
<p>
	 Webové rozhraní používá službu Google Analytics, poskytovanou společností Google, Inc. (dále jen "<b>Google</b>").
</p>
<p>
 <b>3.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Co je služba Google Analytics?</b>
</p>
<p>
	 Služba Google Analytics používá souborů "cookies", které jsou textovými soubory ukládanými do počítače každého návštěvníka webového rozhraní, a které umožňují analýzu způsobu užívání webového rozhraní.
</p>
<p>
	 Informace vygenerované souborem cookie o užívání stránky (včetně IP adresy) budou společností Google přeneseny a uloženy na serverech ve Spojených státech amerických. Google bude užívat těchto informací pro účely vyhodnocování užívání webového rozhraní a vytváření zpráv o aktivitě jeho uživatelů, určených pro nás a pro užívání internetu vůbec. Google může také poskytnout tyto informace třetím osobám, bude-li to požadováno zákonem nebo budou-li takovéto třetí osoby zpracovávat tyto informace pro Google. Google nebude spojovat IP adresu subjektu s jakýmikoli jinými daty, která má k&nbsp;dispozici.
</p>
<p>
	 Používáním webového rozhraní souhlasíte se zpracováváním údajů o Vás společností Google, a to způsobem a k účelu shora uvedeným.
</p>
<p>
 <b>3.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Můžete ukládání souborů cookies ve svém počítači nějak zabránit?</b>
</p>
<p>
	 Používání souborů cookies můžete odmítnout volbou v příslušném nastavení v&nbsp;internetovém prohlížeči.
</p>
<p>
	 Upozorňujeme na to, že při odmítnutí používání souborů cookies není vyloučeno, že nebudete moci plně využívat veškeré funkce webového rozhraní.
</p>
<p>
	 &nbsp;
</p>
<p>
	 4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ochrana autorských práv
</p>
<p>
	 Obsah webových stránek umístěných na webovém rozhraní (texty, fotografie, obrázky, loga a&nbsp;další), včetně programového vybavení webového rozhraní a těchto podmínek, je chráněn naším autorským právem a může být chráněn dalšími právy dalších osob. Obsah nesmíte měnit, kopírovat, rozmnožovat, šířit ani používat k&nbsp;jakémukoli účelu bez souhlasu naší společnosti či držitele autorských práv. Zejména je zakázáno bezplatné či úplatné zpřístupňování fotografií a&nbsp;textů umístěných na webovém rozhraní.
</p>
<p>
	 Názvy a označení výrobků, zboží, služeb, firem a společností mohou být registrovanými obchodními známkami příslušných vlastníků.&nbsp;
</p>
<p>
 <b>4.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jakými způsoby budeme při porušení autorských práv postupovat?</b>
</p>
<p>
	 Při nerespektování výše popsaného zákazu budeme postupovat v&nbsp;souladu se zákonem č.&nbsp;121/2000 Sb., autorský zákon, ve znění pozdějších předpisů.
</p>
<p>
	 Naše společnost jako držitel autorských práv má zejména právo domáhat se, aby bylo od zásahů do našich autorských práv upuštěno, a požadovat stažení neoprávněných kopií chráněného obsahu.
</p>
<p>
	 Dále máme právo požadovat přiměřené zadostiučinění za způsobenou újmu.
</p>
<p>
	 &nbsp;
</p>
<p>
 <b>5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ostatní vztahy související s&nbsp;využíváním webového rozhraní</b>
</p>
<p>
	 5.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Upozorňujeme Vás na to, že kliknutím na některé odkazy na webovém rozhraní může dojít k opuštění webového rozhraní a k přesměrování na webové stránky třetích subjektů.</b>
</p>
<p>
	 5.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Naše společnost nenese odpovědnost za chyby vzniklé v důsledku zásahů třetích osob do webového rozhraní nebo v důsledku jeho užití v rozporu s&nbsp;jeho určením. Při využívání webového rozhraní nesmíte používat mechanismy, programové vybavení, skripty nebo jiné postupy, které by mohly mít negativní vliv na jeho provoz, tj. především narušit funkci systému nebo nepřiměřeně zatěžovat systém, a dále nesmíte vykonávat žádnou činnost, která by mohla Vám nebo třetím osobám umožnit neoprávněně zasahovat či neoprávněně užít programové vybavení nebo další součásti tvořící webové rozhraní a užívat webové rozhraní nebo jeho části či softwarové vybavení takovým způsobem, který by byl v rozporu s jeho určením či účelem.
</p>
<p>
	 5.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nemůžeme Vám zaručit nepřerušený přístup na webové rozhraní, ani nezávadnost a&nbsp;bezpečnost webového rozhraní. Neodpovídáme za škodu způsobenou při realizaci přístupu a užívání webového rozhraní, včetně případné škody vzniklé při stahování dat zveřejněných na webovém rozhraní, škody způsobené přerušením provozu, poruchou webového rozhraní, počítačovými viry, škodu v důsledku ztráty dat, zisku nebo neoprávněného přístupu k přenosům a datům.
</p>
<p>
	 5.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pokud se při užívání webového rozhraní dopustíte jakéhokoliv nezákonného či neetického jednání, jsme oprávněni omezit, pozastavit nebo ukončit Váš přístup na webové rozhraní, a&nbsp;to bez jakékoli náhrady. V&nbsp;tomto případě jste dále povinni uhradit naší společnosti škodu, která Vaším jednáním dle tohoto odstavce prokazatelně vznikla, a to v&nbsp;plné výši.
</p>
<p>
	 &nbsp;
</p>
<p>
	 Tyto podmínky užití jsou platné a účinné od Klikněte sem a zadejte datum.
</p>
 <br>
 <br>
 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>