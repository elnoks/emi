<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Formulář");
?><p>
 <b>Formulář pro odstoupení od smlouvy</b>
</p>
<p>
	 (vyplňte tento formulář a&nbsp;pošlete jej zpět pouze v&nbsp;případě, že chcete odstoupit od smlouvy)
</p>
<p>
	<br>
</p>
<p>
</p>
<p>
</p>
<table border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
	<td>
		<b> </b>
		<p>
			<b>
			Oznámení o&nbsp;odstoupení od smlouvy</b>
		</p>
		<p>
			 - <b>Adresát</b>: E.Mi - International s.r.o.
		</p>
		<p>
			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;Štefánikova 203/23
		</p>
		<p>
			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;150 00 Praha 5 - Smíchov
		</p>
		<p>
		</p>
		<p>
			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="mailto:prague@emischool.com">prague@emischool.com</a>
		</p>
		<p>
			<br>
		</p>
		<p>
		</p>
		<p>
 <b>- Oznamuji/oznamujeme (*), že tímto odstupuji/odstupujeme (*) od smlouvy o&nbsp;nákupu tohoto zboží:</b>
		</p>
		<p>
		</p>
		<p>
		</p>
		<p>
			<br>
		</p>
		<p>
 <b>- Datum objednání (*)/datum obdržení (*) </b>
		</p>
		<p>
			<br>
		</p>
		<b> </b>
		<p>
			<b> </b>
		</p>
		<b> </b>
		<p>
			<b>
			- Jméno a&nbsp;příjmení spotřebitele/spotřebitelů </b>
		</p>
		<p>
			<br>
		</p>
		<b> </b>
		<p>
			<b> </b>
		</p>
		<b> </b>
		<p>
			<b>
			- Adresa spotřebitele/spotřebitelů </b>
		</p>
		<p>
			<br>
		</p>
		<b> </b>
		<p>
			<b> </b>
		</p>
		<b> </b>
		<p>
			<b>
			- Podpis spotřebitele/spotřebitelů</b> (pouze pokud je tento formulář zasílán v&nbsp;listinné podobě)
		</p>
		<p>
			<br>
		</p>
		<p>
		</p>
		<p>
 <b>- Datum</b>
		</p>
		<p>
			<br>
		</p>
		<p>
		</p>
		<p>
			 (*) Nehodící se škrtněte nebo údaje doplňte.
		</p>
	</td>
</tr>
<tr>
	<td>
		<p>
		</p>
	</td>
</tr>
</tbody>
</table><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>