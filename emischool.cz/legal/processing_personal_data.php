<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("INFORMACE O ZPRACOVÁNÍ OSOBNCÍH ÚDAJŮ");
?>
    <p><strong>Vážená paní, vážený pane,</strong></p>

    <p>toto prohlášení jsme pro vás my, společnost <strong>E.Mi - International s.r.o.</strong>, IČO 24214647, se sídlem U božích bojovníků 89/1, Žižkov, 130 00 Praha 3, zapsaná v obchodním rejstříku vedeném u Městského soudu v Praze pod sp.zn. C 189332, připravili, abychom Vás seznámili s tím, jak shromažďujeme, zpracováváme, používáme a chráníme Vaše osobní údaje a tím i Vaše soukromí.</p>

    <p>S Vašimi osobními údaji nakládáme v souladu s platnou legislativou, zejména s nařízením Evropského parlamentu a Rady (EU) 2016/679 o ochraně fyzických osob v souvislosti se zpracováním osobních údajů a o volném pohybu těchto údajů („GDPR“), zákonem č. 127/2005, o elektronických komunikacích, ve znění pozdějších předpisů a zákonem č. 480/2004 Sb., o některých službách informační společnosti, ve znění pozdějších předpisů. </p>

    <p>V rámci tohoto dokumentu jsme pro Vás zpracovali základní a přehledné informace o tom, jaké osobní údaje zpracováváme, na základě jakého právního důvodu je zpracováváme, k jakým konkrétním účelům je používáme, komu je můžeme předávat a jaká máte v souvislosti se zpracováním Vašich osobních údajů práva. Veškeré podrobnosti, zejména o účelech a způsobu zpracování, době zpracování, jakož i o právech v souvislosti se zpracováním Vašich osobních údajů, naleznete na www.emischool.cz v sekci „Právní informace“.</p>

    <h3>JAKÉ OSOBNÍ ÚDAJE SHROMAŽDUJEME A JAKÝM ZPŮSOBEM JE POUŽÍVÁME</h3>

    <h4>Zpracováváme následující osobní údaje:</h4>

    <ul>
        <li><strong>identifikační údaje</strong> – zejména jméno a příjmení, datum narození</li>
        <li><strong>kontaktní údaje</strong>, které nám umožňují kontakt s Vámi – zejména e-mailovou adresu, telefonní
            číslo, poštovní adresu,
        </li>
        <li><strong>údaje o Vašich objednávkách</strong> – zejména údaje o zboží a službách, které jste si od nás
            objednali, způsobu doručení a platby včetně platebního účtu, údaje o reklamacích,
        </li>
        <li><strong>údaje o Vašem chování na našich webových stránkách</strong> - zejména zboží a služby, které si
            zobrazujete, odkazy, na které klikáte, způsob pohybu na našich webových stránkách, údaje o zařízení, ze
            kterého si naše webové stránky prohlížíte jako je IP adresa, identifikace zařízení, a také údaje získané ze
            souborů cookies,
        </li>
        <li><strong>osobní údaje poskytnuté prostřednictvím sociálních sítí</strong>, když navštívíte naše stránky na
            sociálních sítích - zejména profilový obrázek, lokalitu, seznam přátel,
        </li>
        <li><strong>Vámi poskytnuté osobní údaje</strong> – zejména fotografie, odpovědi z průzkumů,</li>
        <li><strong>osobní údaje související s návštěvou naší školu</strong> – zejména nahrávky z kamerového systému.
        </li>
    </ul>

    <p>
        Vaše osobní údaje zpracováváme pro různé účely a v různém rozsahu buď:<br>
        - bez Vašeho souhlasu, a to na základě plnění smlouvy, našeho oprávněného zájmu nebo z důvodu plnění právní
        povinnosti, nebo<br>
        - na základě Vašeho souhlasu.<br>
    </p>
    <p>
        Skutečnost, zda ke zpracování Vašich osobních údajů je třeba Váš výslovný souhlas závisí na tom, k jakému účelu příslušné zpracování směřuje a v jaké pozici vůči nám vystupujete. Podstatné tedy je, zda pouze navštívíte naše webové stránky, nebo se u nás i zaregistrujete a nakoupíte. Vaše údaje můžeme zpracovávat také tehdy, pokud s námi komunikujete nebo navštívíte naši školu.
    </p>
    <h3>POKUD NAVŠTÍVÍTE NAŠE WEBOVÉ STRÁNKY</h3>

    <h4>Zpracování protokolových souborů</h4>

    <p>V rámci přístupu na naše webové stránky zpracováváme protokolové soubory o přístupu k těmto webovým stránkám, a to za účelem ochrany našich oprávněných zájmů spočívajících v zajištění ochrany webových stránek a zlepšení jejich fungování. Protokolové soubory jsou tak zpracovávány bez Vašeho souhlasu. V této souvislosti zpracováváme následující údaje:</p>
    <ul>
        <li><strong>identifikační údaje</strong></li>
        <li><strong>kontaktní údaj</strong></li>
        <li><strong>údaje o Vašich objednávkách</strong></li>
        <li><strong>údaje o Vašem chování na našich webových stránkách</strong></li>
        <li><strong>osobní údaje poskytnuté prostřednictvím sociálních sítí</strong></li>
        <li><strong>vámi poskytnuté osobní údaje</strong></li>
        <li><strong>osobní údaje související s návštěvou naší školu</strong></li>
    </ul>

    <p>Tyto osobní údaje jsou uchovávány po dobu 10 let od návštěvy webové stránky.</p>

    <h4>Použití souborů cookies a analýza webových stránek</h4>
    <p>Za účelem zlepšení fungování našich webových stránek, vyhodnocení jejich návštěvnosti a za účelem optimalizace
        marketingových aktivit používáme na svých webových stránkách soubory cookies. Pokud tedy navštívíte naše webové
        stránky, do Vašeho zařízení ukládáme a následně z něj čteme tyto malé textové soubory.</p>
    <p>Cookies a další sledovací technologie využíváme zejména proto, abychom byly schopni zkvalitnit a zefektivnit naše
        služby. V případě, že se rozhodnete některé cookies zakázat, může dojít k omezení funkcionality či možnosti
        využítí služeb nabízených prostřednictvím naší webové stránky.</p>

    <h4>Užívání sociální pluginů</h4>
    <p>NNaše webové stránky obsahují také sociální pluginy třetích stran, pomocí nichž mohou návštěvníci webových stránek sdílet obsah se svými přáteli a dalšími kontakty. Jedná se o plugin sítě:</p>
    <ul>
        <li><strong>Facebook</strong> plugin řízený Facebook Inc., 1601 South California Avenue, Palo Alto, CA 94304, USA;</li>
        <li><strong>Youtube</strong> plugin řízený YouTube, LLC 901 Cherry Ave., 94066 San Bruno, CA, USA, a subsidiary of Google Inc., Amphitheatre Parkway, Mountain View, CA 94043;
        </li>
        <li><strong>“VKontakte”</strong> Ltd, INN 7842349892, OGRN 1079847035179, se sídlem 191024, St. Petersburg, ul. Khersonskaya, 12-14, A, office 1-H;
        </li>
        <li><strong>“MAIL.RU”</strong> Ltd, INN 7743001840, KPP 771401001, OGRN 1027739850962, OKPO 52685881, se sídlem 125167, Moscow, Leningradsky Prospect, 39/79;
        </li>
        <li><strong>“YANDEX”</strong> Ltd, INN 7736207543, KPP 997750001, OGRN 1027700229193, OKPO 55187675, se sídlem 119034, Moscow, ul.Lva Tolstogo, 16;
        </li>
        <li><strong>“Jivoste”</strong> Ltd, INN 7725745476, OGRN 1127746026792, KPP 772501001, se sídlem 115280, Moscow, Leninskaya Sloboda street, 19/21;
        </li>
        <li><strong>“Google”</strong> Ltd, INN 7704582421, KPP 770501001, OGRN 1057749528100, OKPO 93279213, se sídlem 212099, Moscow, Smolenskaya ploshchad‘ 3, Smolensky Passazh business center.
        </li>
    </ul>

    <h4>Zpracování osobních údajů pro marketingové účely</h4>
    <p>Pakliže se budete chtít účastnit našeho věrnostního programu nebo budete mít zájem, abychom Vás například
        informovali o nových produktech a zasílali Vám speciální nabídky, budeme Vaše osobní údaje zpracovávat na
        základě souhlasu, a to vždy minimálně v rozsahu jméno, příjmení, datum narození, bydliště, e-mailová adresa,
        telefonní číslo. Obsah těchto nabídek a další reklamní sdělení můžeme profilovat na základě poskytnutých údajů a
        dalších údajů pro profilování obsahu, které zjistíme z Vašeho chování na webových stránkách, případně naší
        nabídky. Vaše osobní údaje začneme zpracovávat až poté, co nám k tomu dáte souhlas.</p>
    <p>Poskytnuté osobní údaje budeme zpracovávat po dobu, na kterou bude souhlas udělen, tj. po dobu nejdéle 10 let.
        Souhlas s tímto zpracováním osobních údajů je zcela dobrovolný, a může být kdykoli odvolán, a to kliknutím na
        políčko ve Vašem uživatelském účtu</p>

    <h3>POKUD SE U NÁS ZAREGISTRUJETE NEBO U NÁS NAKOUPÍTE ZBOŽÍ</h3>

    <p>Pokud se zaregistrujete na našem e-shopu na webových stránkách www.emischool.cz a/nebo u nás nakoupíte,
        zpracováváme Vaše identifikační a kontaktní údaje, údaje o Vašich objednávkách, a to za účelem realizace
        uzavřené kupní smlouvy (dodání objednaného zboží), a také za účelem ochrany našich oprávněných zájmů
        spočívajících v ochraně před případnými soudními spory vyplývajícími z uzavřených kupních smluv.</p>

    <p>E-mailové adresy zákazníků, kteří nakoupí na našem e-shopu, zpracováváme dále za účelem ověření Vaší spokojenosti
        s nákupem. Právním titulem tohoto zpracování e-mailových adres jsou tak naše oprávněné zájmy spočívající v
        zajištění zpětné vazby o průběhu nákupu od zákazníků, což nám umožňuje zlepšit naši nabídku.</p>

    <p>Tyto osobní údaje budeme zpracovávat po dobu 10 let od uzavření kupní smlouvy.</p>

    <h3>POKUD SE STANETE ÚČASTNÍKEM NĚKTERÉHO Z NÁMI NABÍZENÝCH KURZŮ NEHTOVÉHO DESIGNU</h3>

    <p>Vaše osobní údaje zpracováváme dále tehdy, pokud se rozhodnete být účastníkem našich vzdělávacích kurzů v oblasti
        nehtového designu. Výuka probíhá v naší škole nehtového designu – E.Mi School na adrese nám. 14 října 642/17,
        Praha 5, PSČ 150 00 (dále jen „škola“).</p>

    <p>Pokud se stanete účastníkem některého z námi nabízených kurzů nehtového designu, zpracováváme Vaše identifikační
        a kontaktní údaje za účelem realizace uzavřené smlouvy o účasti na kurzu (poskytnutí výuky v oblasti nehtového
        designu), a rovněž také za účelem našich oprávněných zájmů spočívajících v ochraně před případnými soudními
        spory vyplývajícími u zavřených smluv. Pokud se tedy rozhodnete stát účastníkem našich vzdělávacích kurzů,
        zpracováváme osobní údaje bez Vašeho souhlasu. Tyto osobní údaje budeme zpracovávat po dobu 10 let od uzavření
        smlouvy.</p>

    <p>Pokud budete navštěvovat naši školu, budeme rovněž zpracovávat nahrávku z kamerového systému, na které můžete být
        zachyceni, za ochrany našeho i Vašeho majetku a osob pohybujících se ve škole, tedy za účelem našeho oprávněného
        zájmu. Bližší informace Vám případně poskytne vedoucí školy.</p>
    <p>Pro tento účel uchováváme osobní údaje po dobu 30 dnů.</p>

    <p>V rámci návštěvy našich kurzů nehtového designu a souvisejících akcií sami pořizujeme fotografie a/nebo audiovizuální záznamy, ale zároveň také přivítáme, pokud nám i Vy poskytnete fotografie a/nebo audiovizuální záznamy, jež budeme moci využít za účelem propagace našeho zboží a služeb. Takovéto fotografie a/nebo videozáznamy však můžeme zpracovávat až poté, co nám k tomu dáte souhlas. Takto poskytnuté osobní údaje budeme zpracovávat po dobu, na kterou bude souhlas udělen, tj. po dobu nejdéle 10 let. Souhlas s tímto zpracováním osobních údajů je zcela dobrovolný, a může být kdykoli odvolán, a to písemně na adrese E.Mi - International s.r.o., U božích bojovníků 89/1, Žižkov, 130 00 Praha 3, nebo e-mailem zaslaným na adresu post@emischool.com.</p>

    <h3>POKUD NAVŠTÍVÍTE VÝSTAVU, NA KTERÉ PŘEDSTAVUJEME NAŠE ZBOŽÍ A SLUŽBY</h3>

    <p>Abychom Vám mohli blíže představit naše zboží a služby, účastníme se rovněž výstav zaměřených na oblast nehtového
        designu. V rámci prezentace našeho zboží a služeb na takovýchto výstavách pořizujeme pro marketingové a
        propagační účely fotografie a/nebo audiovizuální záznamy. O této skutečnosti Vás informujeme prostřednictvím
        námi zasílaných pozvánky a/nebo informace umístěné u naší výstavní plochy. Návštěvou naší výstavní plochy nám
        udělujete souhlas s pořízením fotografií a/nebo audiovizuálních záznamů zachycujících Vaši podobiznu a/nebo
        projevy osobní povahy ve smyslu občanského zákoníku a současně i souhlas s jejich zpracováním jakožto osobních
        údajů. Tyto osobní údaje budeme zpracovávat nejdéle po dobu 10 let ode dne konání příslušné výstavy. Souhlas s
        tímto zpracováním osobních údajů je zcela dobrovolný, a může být kdykoli odvolán, a to písemně na adrese sídla
        E.Mi - International s.r.o., U božích bojovníků 89/1, Žižkov, 130 00 Praha 3, nebo e-mailem zaslaným na adresu
        post@emischool.com.</p>

    <h3>KOMU PŘEDÁVÁME VAŠE OSOBNÍ ÚDAJE</h3>

    <p>Vaše osobní údaje jsou užívány pro naše interní potřeby, a to pouze z výše uvedených důvodů. Nezbytné služby
        ohledně osobních údajů však nezajišťujeme pouze vlastními silami, ale využíváme i služeb třetích stran. Jedná
        se například o přepravní společnosti, poskytovatele internetových stránek, e-mailu, SMS korespondenci, on-line
        chatu, statistických a monitorovacích systémů, lektory, kteří vedou výuku nehtového designu na naší škole či
        příslušné akreditované osoby, před kterými je v případě profesních kurzů vykonávána profesní zkouška.</p>

    <h3>Z JAKÝCH ZDROJŮ VAŠE OSOBNÍ ÚDAJE ZÍSKÁVÁME</h3>

    <p>Ve většině případů zpracováváme osobní údaje, které nám poskytnete přímo Vy v rámci objednávání zboží nebo
        služeb, vytváření a používání účtu, nebo při komunikaci s námi. Osobní údaje přímo od Vás získáváme také
        sledováním Vašeho chování na našich webových stránkách.</p>

    <h3>PŘEDÁVÁNÍ VAŠICH OSOBNÍCH ÚDAJŮ MIMO EVROPSKOU UNII</h3>

    <p>V některých případech můžeme Vaše osobní údaje předávat také do zemí, kterou nejsou součástí Evropského
        hospodářského prostoru.</p>

    <p>V rámci předání údajů našim zpracovatelům můžeme Vaše údaje předat také do třetích zemí mimo Evropský hospodářský
        prostor, které nezajišťují odpovídající úroveň ochrany osobních údajů. Veškeré takové předávání budeme
        uskutečňovat pouze v případě, že se příslušný zpracovatel zaváže dodržovat standardní smluvní doložky vydané
        Evropskou komisí, dostupné na adrese
        http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2004:385:0074:0084:CS:PDF</p>

    <h3>PRÁVA SPOJENÁ S OCHRANOU OSOBNÍCH ÚDAJŮ</h3>

    <p>Při zpracování osobních údajů nedochází k rozhodování založeném výhradně na automatizovaném zpracování, které by
        mělo právní účinky nebo by se jinak významně dotýkalo subjektů údajů, jejichž osobní údaje zpracováváme.</p>

    <p>Pakliže zpracováváme Vaše osobní údaje, náležejí Vám při naplnění předpokladů stanovených právními předpisy
        následující práva spojená se zpracováním jejich osobních údajů:</p>
    <p><strong>a) právo na přístup k Vašim osobním údajům</strong></p>
    <p>Máte právo vědět, jaké údaje o Vás zpracováváme, za jakým účelem, po jakou dobu, komu Vaše osobní údaje předáváme
        a kde je získáváme, jakož i to, zda Vaše osobní údaje zpracovává někdo jiný, a jaká máte další práva související
        se zpracováním Vašich osobních údajů. Veškeré tyto informace jsou obsaženy v tomto dokumentu a na stránkách
        www.emischool.cz v sekci „Právní informace“. Pokud si však nejste jistí, které osobní údaje o Vás zpracováváme,
        můžete nás požádat o potvrzení, zda osobní údaje, které se Vás týkají, zpracováváme či nikoliv, a pokud tomu tak
        je, máte právo získat přístup k těmto osobním údajům. V rámci práva na přístup nás můžete požádat o kopii
        zpracovávaných osobních údajů, přičemž první kopii Vám poskytneme bezplatně a další kopie s poplatkem.</p>
    <p><strong>b) právo na vysvětlení, opravu, resp. doplnění, Vašich osobních údajů</strong></p>
    <p>Pokud zjistíte, že osobní údaje, které o Vás zpracováváme, jsou nepřesné nebo neúplné, máte právo na to, abychom
        je bez zbytečného odkladu opravili, popřípadě doplnili.</p>
    <p><strong>c) právo na výmaz Vašich zpracovávaných osobních údajů</strong></p>
    <p>V některých případech máte právo, abychom Vaše osobní údaje vymazali. Vaše osobní údaje bez zbytečného odkladu
        vymažeme, pokud je splněn některý z následujících důvodů:</p>
    <p>- Vaše osobní údaje již nepotřebujeme pro účely, pro které jsme je zpracovávali,</p>
    <p>- odvoláte souhlas se zpracováním osobních údajů, přičemž se jedná o údaje, k jejichž zpracování je Váš souhlas
        nezbytný a zároveň nemáme jiný důvod, proč tyto údaje potřebujeme nadále zpracovávat,</p>
    <p>- využijete svého práva vznést námitku proti zpracování u osobních údajů, které zpracováváme na základě našich
        oprávněných zájmů a my shledáme, že již žádné takové oprávněné zájmy, které by toto zpracování opravňovaly,
        nemáme nebo se domníváte, že námi prováděné zpracování osobních údajů přestalo být v souladu s obecně závaznými
        předpisy.</p>
    <p>Toto právo se neuplatní v případě, že zpracování Vašich osobních údajů je i nadále nezbytné pro splnění naší
        právní povinnosti nebo určení, výkon nebo obhajobu našich právních nároků.</p>
    <p><strong>d) právo na omezení zpracování Vašich osobních údajů</strong></p>
    <p>V některých případech můžete kromě práva na výmaz využít právo na omezení zpracování osobních údajů. Toto právo
        Vám umožňuje v určitých případech požadovat, aby došlo k označení Vašich osobních údajů a tyto údaje nebyly
        předmětem žádných dalších operací zpracování – v tomto případě však nikoliv navždy (jako v případě práva na
        výmaz), ale po omezenou dobu. Zpracování osobních údajů musíme omezit když:</p>
    <p>- popíráte přesnost osobních údajů, než se dohodneme, jaké údaje jsou správné;</p>
    <p>- Vaše osobní údaje zpracováváme bez dostatečného právního základu (např. nad rámec toho, co zpracovávat musíme),
        ale Vy budete před výmazem takových údajů upřednostňovat pouze jejich omezení (např. pokud očekáváte, že byste
        nám v budoucnu takové údaje stejně poskytli),</p>
    <p>- Vaše osobní údaje již nepotřebujeme pro shora uvedené účely zpracování, ale Vy je požadujete pro určení, výkon
        nebo obhajobu svých právních nároků, nebo vznesete námitku proti zpracování.</p>
    <p>Po dobu, po kterou šetříme, je-li Vaše námitka oprávněná, jsme povinni zpracování Vašich osobních údajů
        omezit.</p>
    <p><strong>d) právo na přenositelnost Vašich osobních údajů</strong></p>
    <p>Máte právo získat osobní údaje, které se Vás týkají a které jste nám poskytli, ve strukturovaném, běžně
        používaném a strojově čitelném formátu, pro účely jejich předání jinému správci.</p>
    <p><strong>e) právo vznést námitku proti zpracování Vašich osobních údajů</strong></p>
    <p>Máte právo vznést námitku proti zpracování Vašich osobních údajů, k němuž dochází na základě našeho oprávněného
        zájmu. Jde-li o marketingové aktivity, přestaneme Vaše osobní údaje bez dalšího zpracovávat a v ostatních
        případech tak učiníme, pokud nebudeme mít závažné oprávněné důvody pro to, abychom v takovém zpracování
        pokračovali.</p>
    <p><strong>f) právo kdykoli odvolat udělený souhlas se zpracováním Vašich osobních údajů</strong></p>
    <p>Je-li zpracování Vašich osobních údajů založeno na Vašem souhlasu, můžete svůj souhlas kdykoliv odvolat.
        Odvoláním souhlasu není dotčena zákonnost zpracování vycházejícího ze souhlasu, který byl dán před jeho
        odvoláním.</p>
    <p><strong>g) právo podat stížnost</strong></p>
    <p>Shora popsaná práva můžete u nás uplatnit písemně či e-mailem, a to na kontaktních údajích uvedených níže.</p>
    <p>Stejně tak se můžete se svojí stížností obrátit na dozorový orgán, kterým je:</p>

    <p><strong>Úřad pro ochranu osobních údajů</strong></p>
    <p><strong>Pplk. Sochova 27, 170 00 Praha 7</strong></p>
    <p><strong>tel.: 234 665 111</strong></p>
    <p><strong>web: www.uoou.cz</strong></p>

    <h3>KONTAKTNÍ ÚDAJE SPRÁVCE OSOBNÍCH ÚDAJŮ A POVĚŘENCE PRO OCHRANU OSOBNÍCH ÚDAJŮ</h3>

    <p>Ve všech záležitostech souvisejících se zpracováním Vašich osobních údajů, ať již jde o dotaz, uplatnění práva,
        podání stížnosti či cokoliv jiného, se můžete na nás obracet na níže uvedené adrese.</p>

    <p><strong>E.Mi - International s.r.o.</strong></p>
    <p>U božích bojovníků 89/1</p>
    <p>130 00 Praha 3 – Žižkov</p>
    <p>Tel.: +420 773 208 276</p>
    <p>e-mail: post@emischool.com</p>

    <p>Zřídili jsme pozici pověřence pro ochranu osobních údajů, na kterého se můžete obrátit na e-mailové adrese
        post@emischool.com.</p>

    <p>Vaši žádost vyřídíme bez zbytečného odkladu, nejpozději však do jednoho měsíce. Ve výjimečných případech, zejména
        z důvodu složitosti Vašeho požadavku, jsme oprávněni tuto lhůtu prodloužit o další dva měsíce. O takovém
        případném prodloužení Vás budeme informovat.</p>

    <p>Dne 25.5.2018</p>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>