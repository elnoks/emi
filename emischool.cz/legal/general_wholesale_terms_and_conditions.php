<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Maloobchod");
?><p>
 <b>Všeobecné obchodní podmínky pro maloobchod</b>
</p>
<p>
	 Tyto všeobecné obchodní podmínky (dále jen „<b>obchodní podmínky</b>“) se vztahují na smlouvy uzavřené prostřednictvím on-line obchodu E.MI - School of Nail Design by Ekaterina Miroshnichenko umístěného na webovém rozhraní <a href="http://www.emischool.com">www.emischool.com</a> (dále jen „<b>webové rozhraní</b>“) mezi naší společností
</p>
<p>
 <b>E.Mi - International s.r.o.</b>, se sídlem U božích bojovníků 89/1, 130 00, Praha 3 - Žižkov
</p>
<p>
	 IČ: 24214647
</p>
<p>
	 DIČ: CZ24214647
</p>
<p>
	 zapsanou v&nbsp;obchodním rejstříku vedeném u Městského soudu v&nbsp;Praze, oddíl C, vložka 189332
</p>
<p>
	 Adresa pro doručování: E.Mi - International s.r.o., Štefánikova 203/23, 150 00, Praha 5 - Smíchov
</p>
<p>
	 Telefonní číslo: + 420 773 208 276
</p>
<p>
	 Kontaktní e-mail: <a href="mailto:prague@emischool.com">prague@emischool.com</a><a href="file:///C:/Users/%D0%90%D0%BD%D0%B4%D1%80%D0%B5%D0%B9/AppData/Roaming/Skype/My%20Skype%20Received%20Files/2.Maloobchod.docx#_msocom_1">[A1]</a>&nbsp;
</p>
<p>
	 jako <b>prodávajícím</b>
</p>
<p>
	 a Vámi jako kupujícím
</p>
<p>
</p>
 <br>
 <br>
 <br>
<p>
 <b>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ÚVODNÍ USTANOVENÍ</b>
</p>
<p>
	 Kupní smlouvou se zavazujeme dodat Vám zboží uvedené v&nbsp;objednávce, a Vy se zavazujete toto zboží převzít (buď osobně, nebo od dopravce) a zaplatit nám kupní cenu uvedenou v&nbsp;objednávce a jejím přijetí. Kupní cena (nebo jenom „<b>cena</b>“) zahrnuje i náklady spojené s dodáním zboží a případné poplatky související se zvoleným způsobem platby. Výši nákladů na dodání zboží nelze stanovit před podáním objednávky, proto Vám budou v&nbsp;závislosti na dodací adrese sděleny až po jejím učinění, avšak ještě před uzavřením smlouvy.
</p>
<p>
	 Vlastnické právo ke zboží nabýváte zaplacením celé kupní ceny, ne však dříve, než zboží převezmete.
</p>
<p>
 <b>1.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Vztahuje se kupní smlouva pouze na zboží?</b>
</p>
<p>
	 Jako <b>kupní smlouva</b> (nebo jenom „<b>smlouva</b>“) se zde označuje jakákoliv smlouva, uzavřená dle těchto obchodních podmínek. Může tedy jít například i o smlouvu o poskytování služeb.
</p>
<p>
 <b>1.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Je kupní smlouva spotřebitelskou smlouvou?</b>
</p>
<p>
	 O spotřebitelskou smlouvu se jedná v&nbsp;případě, že jste spotřebitelem, tj. pokud jste fyzickou osobou a zboží kupujete mimo rámec své podnikatelské činnosti nebo mimo rámec samostatného výkonu svého povolání. V&nbsp;opačném případě se o spotřebitelskou smlouvu nejedná a nevztahuje se na Vás ochrana spotřebitele dle právních předpisů a těchto obchodních podmínek, ale Všeobecné obchodní podmínky pro velkoobchod.
</p>
<p>
 <b>1.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jaká máte jako spotřebitel zvláštní práva?</b>
</p>
<p>
	 Jako spotřebitel máte především:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; právo odstoupit od smlouvy uzavřené pomocí prostředků komunikace na dálku, jako je např. telefon, e-mail nebo internetový obchod (článek 5 těchto obchodních podmínek);
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; nárok na záruku na nepoužité spotřební zboží v&nbsp;délce 24 měsíců (uplatnění záruky se řídí Reklamačním řádem);
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; právo na sdělení informací před uzavřením smlouvy&nbsp;(informace jsou obsaženy v&nbsp;těchto obchodních podmínkách nebo na webovém rozhraní).
</p>
<p>
 <b>1.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Čím se řídí náš právní vztah?</b>
</p>
<p>
	 Náš právní vztah se řídí následujícími dokumenty:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; těmito obchodními podmínkami, které vymezují a zpřesňují naše vzájemná práva a povinnosti;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Reklamačním řádem, podle kterého budeme postupovat při reklamaci zboží;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Podmínkami užití webového rozhraní, které upravují registraci na webovém rozhraní, ochranu Vašich osobních údajů, ochranu obsahu webového rozhraní a některé další vztahy související s&nbsp;využíváním webového rozhraní;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; podmínkami a pokyny uvedenými na webovém rozhraní zejména při uzavírání smlouvy;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; objednávkou a jejím přijetím z&nbsp;naší strany,
</p>
<p>
	 a v&nbsp;otázkách zde neupravených také následujícími právními předpisy:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; zákonem č. 89/2012 Sb., občanským zákoníkem, v&nbsp;účinném znění (dále jen „<b>občanský zákoník</b>“);
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; zákonem č. 634/1992 Sb., o ochraně spotřebitele, ve znění pozdějších předpisů (pouze pokud jste spotřebitelem).
</p>
<p>
	 Pokud se Vaše bydliště nebo sídlo nachází mimo Českou republiku, nebo pokud náš právní vztah obsahuje jiný mezinárodní prvek, berete na vědomí, že se náš vztah<b> řídí českým právem</b>. Pokud jste spotřebitelem a právní řád státu Vašeho bydliště poskytuje vyšší míru ochrany spotřebitele než český právní řád, je Vám v&nbsp;právních vztazích poskytována tato vyšší míra ochrany.
</p>
<p>
 <b>1.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jak vyjádříte souhlas s&nbsp;obchodními podmínkami?</b>
</p>
<p>
	 Zasláním objednávky a dále též potvrzením ve webovém rozhraní stvrzujete, že jste se s těmito obchodními podmínkami seznámili a souhlasíte s&nbsp;nimi.
</p>
<p>
	 Znění obchodních podmínek můžeme měnit či doplňovat. Vaše práva a povinnosti se řídí vždy tím zněním obchodních podmínek, za jehož účinnosti vznikly.
</p>
<p>
</p>
<p>
 <b>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; KUPNÍ SMLOUVA</b>
</p>
<p>
 <b>2.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jak uzavíráme kupní smlouvu?</b>
</p>
<p>
	 Na webovém rozhraní je uveden seznam zboží včetně popisu hlavních vlastností jednotlivých položek. U každého zboží je uvedena cena&nbsp;včetně veškerých daní, cel a poplatků. <b>Prezentace zboží je informativního charakteru, a nejedná se o náš návrh na uzavření smlouvy ve smyslu § 1732 odst. 2 občanského zákoníku.</b> Pro uzavření smlouvy je nutné, abyste odeslali objednávku, aby došlo k&nbsp;přijetí této objednávky z&nbsp;naší strany a abyste provedli platbu za zboží způsobem uvedeným v&nbsp;čl. 3.1 těchto podmínek.
</p>
<p>
 <b>2.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jak podat objednávku?</b>
</p>
<p>
	 Objednávku lze podat prostřednictvím webového rozhraní (vyplněním formuláře).&nbsp;Pro podání objednávky je nutné se na webovém rozhraní registrovat. Podmínky registrace jsou upraveny v Podmínkách užití webového rozhraní.
</p>
<p>
	 Objednávka musí obsahovat všechny informace předepsané ve formuláři, zejména přesné označení objednávaného zboží (případně číselné označení zboží), počet kusů, zvolený způsob platby a dopravy a Vaše kontaktní údaje (dodací a případně fakturační adresa).
</p>
<p>
	 Před odesláním objednávky Vám bude sdělena rekapitulace objednávky. Doporučujeme zkontrolovat zejména druh a množství zboží, e-mailovou a dodací adresu. <b>V&nbsp;rámci rekapitulace máte poslední možnost měnit zadané údaje.</b>
</p>
<p>
	 Objednávku podáte stisknutím tlačítka „Complete Order“. <b>Údaje uvedené v objednávce považujeme za správné a úplné.</b> O jejich změně nás bezodkladně informujte telefonicky či e-mailem.
</p>
<p>
	 O obdržení objednávky Vás budeme neprodleně informovat.<b> Informace (potvrzení) o obdržení objednávky je zasílána automaticky a nejedná se o přijetí objednávky z&nbsp;naší strany.</b>
</p>
<p>
	 Pokud budeme mít pochybnosti o pravosti a vážnosti objednávky, můžeme Vás kontaktovat za účelem jejího ověření. Neověřenou objednávku můžeme odmítnout. Na takovou objednávku se potom hledí, jako by nebyla podána.
</p>
<p>
	 Po obdržení objednávky Vás budeme informovat též o nákladech na dodání zboží, a to zasláním faktury, ve které bude uvedena celková kupní cena zboží včetně nákladů na dodání zboží. Fakturu Vám zašleme na e-mailovou adresu uvedenou v&nbsp;objednávce. Zaslání informace o nákladech na dodání zboží, resp. faktury je svou povahou přijetím objednávky z&nbsp;naší strany.
</p>
<p>
	 Vezměte na vědomí, že pokud objednávku činíte z&nbsp;jiného státu, než je Česká republika, a zároveň v&nbsp;tomto státu je činný národní distributor našeho zboží, bude Vaše objednávka předána k&nbsp;vyřízení právě tomuto distributorovi.&nbsp;
</p>
<p>
 <b>2.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kdy je tedy smlouva uzavřena?</b>
</p>
<p>
 <b>Kupní smlouva je uzavřena okamžikem, kdy uhradíte kupní cenu zboží, včetně nákladů na dodání zboží způsobem uvedeným v&nbsp;čl. 3.1 těchto podmínek.</b>
</p>
<p>
	 Informace o jednotlivých technických krocích vedoucích k uzavření smlouvy jsou patrné z&nbsp;webového rozhraní.
</p>
<p>
 <b>Můžete již odeslanou objednávku zrušit?</b>
</p>
<p>
	 Objednávku, kterou jsme dosud nepřijali (tj. nebyla Vám zaslána informace o nákladech na dodání zboží z&nbsp;naší strany podle článku 2.2. těchto obchodních podmínek), můžete zrušit telefonicky nebo e-mailem. <b>Všechny námi přijaté objednávky jsou závazné.</b> Přijatou objednávku můžete bez dalšího zrušit v&nbsp;případě, že nesouhlasíte s&nbsp;výší nákladů na dodání zboží, a to telefonicky nebo e-mailem nebo tím, že neprovedete úhradu ceny. Jinak je přijatou objednávku možné zrušit po dohodě s námi. Pokud je takto zrušena objednávka zboží, ohledně kterého nejde odstoupit od smlouvy (podrobněji v&nbsp;článku 5), máme nárok na náhradu nákladů, které jsme již v&nbsp;souvislosti se&nbsp;smlouvou vynaložili.
</p>
<p>
 <b>2.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Může se cena zboží uvedená na webovém rozhraní měnit?</b>
</p>
<p>
	 Ceny prezentovaného zboží zůstávají v platnosti po dobu, kdy jsou zobrazovány ve webovém rozhraní. Případné slevy z ceny zboží nelze vzájemně kombinovat, ledaže je na webovém rozhraní výslovně uvedeno něco jiného.
</p>
<p>
	 V&nbsp;případě, že na naší straně došlo ke zcela zjevné&nbsp;technické chybě při uvedení ceny zboží ve webovém rozhraní nebo v&nbsp;průběhu objednávání, <b>nejsme povinni dodat Vám zboží za tuto zcela zjevně chybnou cenu</b>, a to ani v&nbsp;případě, že Vám bylo zasláno přijetí objednávky podle těchto obchodních podmínek.<b> V&nbsp;takovém případě si vyhrazujeme právo odstoupit od smlouvy.</b>
</p>
<p>
	 Pokud cena uvedená u zboží ve webovém rozhraní nebo v&nbsp;průběhu objednávání již není aktuální, neprodleně Vás na tuto skutečnost upozorníme. Pokud dosud nedošlo k&nbsp;přijetí Vaší objednávky, nejsme povinni smlouvu uzavřít.
</p>
<p>
 <b>Na odeslané objednávky nemá vliv změna ceny, ke které došlo v&nbsp;mezidobí mezi odesláním objednávky a jejím přijetím z&nbsp;naší strany podle článku 2.2 těchto obchodních podmínek.</b>
</p>
<p>
 <b>2.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Máte možnost získat smlouvu v&nbsp;textové podobě?</b>
</p>
<p>
	 Smlouva není uzavírána písemně s&nbsp;podpisy smluvních stran. <b>Smlouvu tvoří tyto obchodní podmínky, Vaše objednávka a její přijetí z&nbsp;naší strany. </b>Celá smlouva Vám bude zaslána e-mailem&nbsp;nebo na Vaši žádost vytištěná poštou. Při zasílání poštou Vás můžeme požádat o úhradu nákladů s&nbsp;tím spojených.
</p>
<p>
 <b>2.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Co když něčemu ve&nbsp; smlouvě nerozumíte?</b>
</p>
<p>
	 V případě dotazu k obchodním podmínkám nebo ke smlouvě nás můžete kontaktovat telefonicky nebo prostřednictvím e-mailu. Rádi Vám poskytneme veškeré potřebné informace.
</p>
<p>
 <b>2.7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; V&nbsp;jakých jazycích lze smlouvu uzavřít?</b>
</p>
<p>
	 Smlouvu lze uzavřít v&nbsp;českém jazyce, ledaže se výslovně dohodneme na&nbsp;jiném jazyce.
</p>
<p>
 <b>2.8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Je smlouva někde uložena?</b>
</p>
<p>
	 Smlouvu (včetně těchto obchodních podmínek) archivujeme v elektronické podobě. Smlouva není přístupná třetím osobám, ale na vyžádání Vám ji zašleme.
</p>
<p>
</p>
<p>
 <b>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PLATEBNÍ PODMÍNKY</b>
</p>
<p>
 <b>3.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jaké způsoby platby přijímáme?</b>
</p>
<p>
	 Kupní cenu můžete uhradit především následujícími způsoby:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; v hotovosti před dodáním zboží prostřednictvím platebních míst Western Union;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; bezhotovostně před dodáním zboží převodem na náš bankovní účet (pokyny Vám budou sděleny v&nbsp;potvrzení objednávky).
</p>
<p>
	 Případné další způsoby platby jsou uvedeny na webovém rozhraní.
</p>
<p>
 <b>3.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kdy nastane splatnost kupní ceny?</b>
</p>
<p>
	 V případě platby v&nbsp;hotovosti prostřednictvím platebních míst Western Union i v&nbsp;případě bezhotovostní platby je cena splatná do sedmi dnů od zaslání informace o nákladech na dodání zboží, resp. faktury (tj. přijetí objednávky). Cena je při bezhotovostní platbě uhrazena okamžikem připsání příslušné částky na náš bankovní účet, při platbě v&nbsp;hotovosti prostřednictvím platebních míst Western Union okamžikem potvrzení o provedení platby ze strany Western Union. Vezměte na vědomí, že o provedení platby prostřednictvím platebního místa Western Union je nutné nás informovat a sdělit nám údaje potřebné k&nbsp;výplatě peněz.
</p>
<p>
 <b>3.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; V&nbsp;jaké měně můžete platit?</b>
</p>
<p>
	 Platba zboží je možná v&nbsp;Eurech (EUR).
</p>
<p>
 <b>3.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kdy můžeme požadovat zálohu?</b>
</p>
<p>
	 Zálohu na kupní cenu můžeme požadovat především u objednávek s&nbsp;celkovou cenou nad 200 EUR.
</p>
<p>
 <b>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DODACÍ PODMÍNKY</b>
</p>
<p>
 <b>4.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jak posíláme zboží?</b>
</p>
<p>
	 Zboží zasíláme prostřednictvím dopravní služby uvedené na webovém rozhraní. Případné další způsoby dodání zboží jsou uvedené rovněž na webovém rozhraní. Konkrétní způsob dodání zboží můžete zvolit v&nbsp;objednávce. Pokud žádný způsob dopravy nezvolíte, můžeme ho určit my.
</p>
<p>
 <b>4.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jaké jsou náklady na dodání zboží?</b>
</p>
<p>
	 Náklady na dodání zboží závisí vždy na velikosti a povaze zboží, dodací adrese, resp. státu dodání a na ceníku zvoleného dopravce.
</p>
<p>
	 Náklady na dodání zboží nelze stanovit před podáním objednávky. Tyto náklady Vám budou vždy sděleny ještě před uzavřením smlouvy.
</p>
<p>
 <b>4.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kdy Vám zboží dodáme?</b>
</p>
<p>
	 Doba dodání zboží vždy závisí na dodací adrese, na dostupnosti zboží a na zvoleném způsobu dopravy a platby.
</p>
<p>
	 Zboží, které je skladem, zpravidla předáme dopravci do dvou pracovních dnů&nbsp;od připsání platby na náš účet nebo od oznámení o provedení platby prostřednictvím platebních míst Western Union.
</p>
<p>
	 Zboží, které není skladem, předáme dopravci, jakmile je to možné. O přesném datu Vás budeme informovat.
</p>
<p>
	 Upozorňujeme Vás na to, že dobu dodání zboží externími dopravci nemůžeme ovlivnit. <b>Případné stížnosti týkající se doby dodání je nutné řešit přímo s&nbsp;dopravcem.</b>
</p>
<p>
	 Dodáním zboží podle těchto obchodních podmínek se rozumí okamžik, kdy je Vám zboží doručeno. Pokud bezdůvodně odmítnete převzít zboží, nepovažuje se tato skutečnost za nesplnění povinnosti dodat zboží z&nbsp;naší strany, ani za odstoupení od smlouvy z&nbsp;Vaší strany.
</p>
<p>
 <b>4.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jak postupovat při převzetí zboží?</b>
</p>
<p>
 <b>Při převzetí zboží zkontrolujte neporušenost obalu zboží</b>. Pokud zjistíte nedostatky, neprodleně informujte dopravce i nás. Pokud odmítnete zásilku s&nbsp;poškozeným obalem převzít, nepovažuje se to za bezdůvodné odmítnutí zboží.
</p>
<p>
	 Podpisem dodacího listu (nebo jiného obdobného dokumentu) stvrzujete, že obal zásilky nebyl porušen. Reklamace zboží z&nbsp;důvodu porušení obalu zásilky v&nbsp;takovém případě již není možná.
</p>
<p>
	 Okamžikem převzetí zboží (nebo okamžikem, kdy jste měli povinnost zboží převzít, ale v&nbsp;rozporu se smlouvou jste tak neučinili), na Vás přechází odpovědnost za nahodilou zkázu, poškození či ztrátu zboží.
</p>
<p>
 <b>4.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Co se stane v&nbsp;případě, že zboží nepřevezmete?</b>
</p>
<p>
	 Pokud je z&nbsp;důvodů na Vaší straně nutno zboží doručovat opakovaně nebo jiným než sjednaným způsobem, <b>jste povinni uhradit náklady spojené s&nbsp;takovým doručováním.</b>
</p>
<p>
	 V&nbsp;případě, že zboží bezdůvodně nepřevezmete, <b>máme nárok na náhradu nákladů spojených s&nbsp;dodáním zboží a jeho uskladněním</b>, jakož i dalších nákladů, které nám z&nbsp;důvodu nepřevzetí zboží vzniknou. Tyto náklady nepřesáhnou 50 Euro centů za každý den trvání uskladnění. Náklady na uskladnění mohou dosáhnout maximálně částky celkem 20 EUR nebo výše kupní ceny, pokud je nižší než 20 EUR.
</p>
<p>
	 Dále máme v&nbsp;takovém případě právo od smlouvy odstoupit.
</p>
<p>
</p>
<p>
 <b>5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ODSTOUPENÍ OD KUPNÍ SMLOUVY</b>
</p>
<p>
 <b>5.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jak můžete od smlouvy odstoupit?</b>
</p>
<p>
	 Od kupní smlouvy můžete odstoupit <b>ve lhůtě 14 dnů ode dne převzetí zboží</b>; je-li dodávka rozdělena do několika částí, ode dne převzetí poslední dodávky. Oznámení o odstoupení od kupní smlouvy doporučujeme zaslat na naši doručovací adresu nebo e-mail. Pro odstoupení od smlouvy lze využít vzorový formulář. Přijetí oznámení Vám bez zbytečného odkladu potvrdíme.
</p>
<p>
	 Odstoupení od smlouvy nemusíte nijak zdůvodňovat.
</p>
<p>
 <b>5.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jaké má odstoupení od smlouvy důsledky?</b>
</p>
<p>
	 Odstoupením od smlouvy se smlouva od počátku ruší a hledí se na ni, jako kdyby nebyla uzavřena.
</p>
<p>
	 Byl-li společně se zbožím poskytnut dárek, pozbývá darovací smlouva odstoupením od smlouvy kteroukoliv ze stran účinnosti. Dárek nám zašlete zpět společně s&nbsp;vraceným zbožím.
</p>
<p>
 <b>5.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kdy od smlouvy odstoupit nelze?</b>
</p>
<p>
	 V&nbsp;souladu s § 1837 občanského zákoníku není možné odstoupit mimo jiné od následujících smluv:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; o dodávce zboží, které bylo po dodání nenávratně smíseno s jiným zbožím;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; o dodávce zboží v uzavřeném obalu, které jste z obalu vyňali a z hygienických důvodů jej není možné vrátit.
</p>
<p>
 <b>5.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jakým způsobem nám vrátíte zboží?</b>
</p>
<p>
	 Zboží jste povinni nám vrátit do 14 dnů od odstoupení&nbsp;od smlouvy na naši doručovací adresu, do jakékoliv provozovny nebo na adresu našeho sídla.<b> Zboží nezasílejte na dobírku.</b> Zboží zaslané na dobírku nejsme povinni převzít.
</p>
<p>
	 Vrácené zboží musí být nepoškozené, neopotřebené a neznečištěné a, je-li to možné, v původním obalu.
</p>
<p>
	 K&nbsp;vrácenému zboží doporučujeme přiložit:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; kopii dodacího listu a faktury, pokud tyto dokumenty byly vystaveny, nebo jiný doklad prokazující koupi zboží;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; písemné vyjádření o odstoupení od smlouvy (na našem formuláři nebo jinak) a zvoleném způsobu vrácení peněz (převod na účet, osobní převzetí hotovosti nebo poštovní poukázka či jinak). Do vyjádření uveďte adresu pro doručování, telefon a e-mail.
</p>
<p>
 <b>Nepředložení některého z&nbsp;výše uvedených dokladů nebrání kladnému vyřízení Vašeho odstoupení od smlouvy dle zákonných podmínek.</b>
</p>
<p>
 <b>5.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kdy dostanete zpět své peníze?</b>
</p>
<p>
	 Veškeré přijaté peněžní prostředky Vám vrátíme do 14 dnů od odstoupení od smlouvy. Berte však na vědomí, že nejsme povinni vrátit Vám peníze dříve, než nám vrátíte zboží nebo prokážete, že jste nám zboží odeslali.
</p>
<p>
	 Vedle kupní ceny máte i nárok na vrácení nákladů na dodání zboží k Vám. Jestliže jste však zvolili jiný než nejlevnější způsob dodání zboží, který nabízíme, vrátíme Vám náklady na dodání zboží <b>ve výši odpovídající nejlevnějšímu nabízenému způsobu dodání zboží.</b>
</p>
<p>
	 Peníze Vám vrátíme:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; stejným způsobem, jakým jsme je přijali, nebo
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; způsobem, jaký budete požadovat;
</p>
<p>
	 Vedle výše uvedených způsobů můžeme peníze vždy vrátit i zasláním na Vámi sdělený bankovní účet nebo účet, ze kterého byly prostředky poukázány k&nbsp;úhradě kupní ceny (pokud nám do deseti dnů od odstoupení od smlouvy žádný nesdělíte). Přijetím těchto obchodních podmínek vyslovujete svůj souhlas se zasláním peněžních prostředků dle předchozí věty za podmínky, že Vám tímto způsobem nevzniknou žádné další náklady.
</p>
<p>
 <b>Náklady spojené s&nbsp;odesláním vráceného zboží na naši adresu&nbsp; hradíte Vy, a to i v&nbsp;případě, že zboží nemůže být vráceno pro svou povahu obvyklou poštovní cestou.</b>
</p>
<p>
 <b>5.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Co když bylo vrácené zboží poškozené?</b>
</p>
<p>
	 Při zasílání zabalte zboží do vhodného obalu tak, aby nedošlo k jeho poškození či zničení. Za zboží podstatně poškozené či zničené při přepravě v důsledku použití nevhodného obalu nelze vrátit kupní cenu a náklady na dodání zboží (případně její část odpovídající vzniklé škodě).
</p>
<p>
	 Pokud zjistíme, že Vámi vrácené zboží je poškozené, opotřebené, znečištěné či částečně spotřebované, máme vůči Vám nárok na náhradu škody. Nárok na úhradu vzniklé škody můžeme jednostranně započíst proti Vašemu nároku na vrácení kupní ceny a nákladů na dodání zboží, tj. vrácena Vám bude částka snížená o vzniklou škodu.
</p>
<p>
 <b>5.7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kdy můžeme odstoupit od kupní smlouvy my?</b>
</p>
<p>
	 Vyhrazujeme si právo odstoupit od smlouvy v&nbsp;následujících případech:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; technickou chybou byla na webovém rozhraní uvedena zcela zjevně chybná cena zboží (článek 2.4. těchto obchodních podmínek);
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; zboží z&nbsp;objektivních příčin (především proto, že zboží se již nevyrábí, dodavatel přestal dodávat do ČR atd.) není možné za původních podmínek dodat;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; plnění se stane objektivně nemožným nebo protiprávním.
</p>
<p>
	 V&nbsp;případě, že nastala některá z&nbsp;výše uvedených skutečností, budeme Vás o našem odstoupení od smlouvy neprodleně informovat. Odstoupení je vůči Vám účinné okamžikem, kdy je Vám doručeno.
</p>
<p>
	 Pokud jste již zcela nebo zčásti uhradili kupní cenu, vrátíme Vám přijatou částku bezhotovostně na účet, který nám pro tento účel sdělíte, nebo ze kterého jste provedli úhradu. Peníze vrátíme do pěti&nbsp;dnů od odstoupení od kupní smlouvy.
</p>
<p>
</p>
<p>
 <b>6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PRÁVA Z&nbsp;VADNÉHO PLNĚNÍ</b>
</p>
<p>
	 Vaše práva z&nbsp;vadného plnění se řídí příslušnými obecně závaznými právními předpisy (zejména ustanoveními §&nbsp;1914 až 1925, § 2099 až 2117 a § 2158 až 2174 občanského zákoníku).
</p>
<p>
	 Při uplatňování práv z&nbsp;vadného plnění budeme postupovat v souladu s naším&nbsp;Reklamačním řádem. Před odesláním reklamace se s&nbsp;Reklamačním řádem důkladně seznamte, aby mohla být reklamace vyřízena co nejrychleji a k&nbsp;Vaší spokojenosti.
</p>
<p>
</p>
<p>
 <b>7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ZÁVĚREČNÁ USTANOVENÍ</b>
</p>
<p>
 <b>7.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jaká oprávnění máme k&nbsp;výkonu naší činnosti a kdo nás při ní kontroluje?</b>
</p>
<p>
	 K&nbsp;prodeji zboží jsme oprávněni na základě živnostenského oprávnění. Naše činnost nepodléhá jinému povolování.
</p>
<p>
	 Živnostenskou kontrolu provádí v rámci své působnosti příslušný živnostenský úřad. Kontrolu dodržování právních předpisů týkajících se technických požadavků na zboží a bezpečnost zboží provádí Česká obchodní inspekce (<a href="http://www.coi.cz/)">http://www.coi.cz/)</a>. Česká obchodní inspekce provádí též kontrolu dodržování předpisů na ochranu spotřebitele. Práva spotřebitelů hájí i jejich zájmová sdružení a jiné subjekty na jejich ochranu.
</p>
<p>
 <b>7.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jak vyřizujeme stížnosti?</b>
</p>
<p>
	 Případné stížnosti vyřizujeme prostřednictvím svého kontaktního e-mailu. Dále se můžete obrátit na subjekty uvedené v&nbsp;článku 7.1. Ve vztahu k&nbsp;našim zákazníkům nejsme vázáni žádnými kodexy chování, ani žádné takové nedodržujeme.
</p>
<p>
 <b>7.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Co byste ještě měli vědět?</b>
</p>
<p>
	 Při uzavírání smlouvy jsou použity prostředky komunikace na dálku (zejména síť internet). Náklady vzniklé při použití prostředků komunikace na dálku (především náklady na internetové připojení nebo na telefonní hovory) hradíte sami.<b> Tyto náklady se neliší od běžné sazby.</b>
</p>
<p>
	 Nebude-li dohodnuto jinak, veškerá korespondence související se smlouvou mezi námi probíhá v&nbsp;písemné formě, a to buď zasláním e-mailem, doporučeně poštou nebo osobním doručením. Z&nbsp;naší strany Vám budeme doručovat na adresu elektronické pošty uvedenou v&nbsp;objednávce nebo ve Vašem uživatelském účtu.
</p>
<p>
	 V&nbsp;případě, že je některé ustanovení těchto obchodních podmínek neplatné, neúčinné nebo nepoužitelné (nebo se takovým stane), použije se namísto něj ustanovení, které se svým smyslem nejvíce blíží neplatnému, neúčinnému nebo nepoužitelnému ustanovení. Neplatností, neúčinností nebo nepoužitelností jednoho ustanovení není dotčena platnost ostatních ustanovení.<b> Měnit či doplňovat smlouvu (včetně obchodních podmínek) lze pouze písemnou formou.</b>
</p>
<p>
	 V&nbsp;souladu se zákonem č. 185/2001 Sb., zákon o odpadech, ve znění pozdějších předpisů, naše společnost zajišťuje bezplatné převzetí Vámi zakoupeného zboží, které je svou povahou elektrozařízením (dále jen „zpětný odběr“). Zpětný odběr je možné provést odevzdáním použitého elektrozařízení na kontaktní adrese naší společnosti. Před odevzdáním použitého elektrozařízení nás, prosím, předem kontaktujte.
</p>
<p>
</p>
<p>
	 Tyto obchodní podmínky jsou platné a účinné od Klikněte sem a zadejte datum.
</p>
<p>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>