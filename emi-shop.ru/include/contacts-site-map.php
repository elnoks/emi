<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?
global $DEPORTAMENT;
?>
<div id="YMapsID" style="width: 100%; height: 450px;"></div>

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript">

    var TCHK = <?= json_encode($DEPORTAMENT['DEPORTAMENT']["UF_DEP_COORDINATE"])?>

        ymaps.ready(init);

    function init() {
        var myMap = new ymaps.Map("YMapsID", {
            center: [<?= reset($DEPORTAMENT['DEPORTAMENT']["UF_DEP_COORDINATE"])?>],
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        });

        myMap.controls.remove('zoomControl');
        myMap.controls.remove('rulerControl');
        myMap.controls.remove('searchControl');
        myMap.controls.remove('geolocationControl');
        myMap.controls.remove('trafficControl');
        myMap.controls.remove('typeSelector');

        $.each(TCHK, function (index, value) {

            var valueNew = value.split(",");
            myMap.geoObjects
                .add(new ymaps.GeoObject(
                    {
                        geometry: {
                            type: "Point",
                            coordinates: [valueNew[0], valueNew[1]]
                        }
                    }, {

                        iconLayout: 'default#imageWithContent',
                        iconImageHref: "/local/templates/aspro_next/images/icon_arrow_5.png",
                        iconImageSize: [36, 48],
                        iconImageOffset: [-15, -50],
                        iconContentOffset: [15, 15]

                    }
                ));
        });

    }


</script>