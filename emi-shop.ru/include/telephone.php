
<?
// pgrant 22.06.2017
// Вывод контактов магазина (раньше выводились только контакты школы)
// Добавлена функция getPartnerType() в класс Partner файла emi-school.ru\bitrix\php_interface\include\functions.php

if ($GLOBALS["partnerSchool"]->getPartnerType() == "shop") {
    ?>
        <a class="h_phone"><span><?=$GLOBALS["partnerShop"]->getPhone(); ?></span></a>
    <?
} else {
    ?>
        <a class="h_phone"><span><?=$GLOBALS["partnerSchool"]->getPhone(); ?></span></a>
    <?
}
if ($USER->IsAdmin()){
    echo $GLOBALS["partnerSchool"]->getArPartner();
}
/* <a class="h_phone" href="tel:<?=$GLOBALS["partnerSchool"]->getPhone();?>"><span><?=$GLOBALS["partnerSchool"]->getPhone();?></span></a>*/
// Исправлена ошибка tel
?>

