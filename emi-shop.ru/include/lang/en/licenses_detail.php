    <p>
        This, in accordance with Federal Law No. 152-ФЗ “On Personal Data” dated July 27, 2006, you confirm your consent to the processing of information by E.Mi LLC personal data: collection, systematization, accumulation, storage, clarification (updating, modification) , use, transfer solely for the purpose of selling software in your name, as described below, blocking, depersonalization, destruction.
</p>
<blockquote>
    The company "E.Mi" & nbsp; guarantees the confidentiality of the information received. The processing of personal data is carried out in order to efficiently execute orders, contracts and other obligations assumed by the company E.Mi Ltd. & nbsp; as binding. & Nbsp;
</blockquote>
<p>
    If it is necessary to provide your personal data to the right holder, distributor or reseller of the software in order to register the software in your name, you consent to the transfer of your personal data. E.Mi LLC guarantees that the copyright holder, distributor or reseller of the software protects personal data under conditions similar to those set out in the Privacy Policy of personal data.
</p>
<p>
    This consent applies to the following personal information about you: last name, first name and patronymic, e-mail address, mailing address for delivery of orders, contact phone number, payment details.
</p>
<p>
    The term of the consent is unlimited. You may revoke this consent at any time by sending a written notice to the address: & nbsp; 344010, Rostov-on-Don, ul. & Nbsp; Khalturinsky, 160/102 & nbsp; with the note "Withdrawal of consent to the processing of personal data."
</p>
<blockquote>
    Please note that withdrawal of consent to the processing of personal data entails the deletion of your account from the website (<a href="https://emi-shop.ru"> https://emi-shop.ru </a>), as well as the destruction of records containing your personal data in the personal data processing systems of the company E.Mi LLC, which may make it impossible to use the Internet services of the company E.Mi LLC.
</blockquote>
<p>
    I guarantee that the information provided by me is complete, accurate and reliable, and that the presentation of information does not violate the current legislation of the Russian Federation, the legal rights and interests of third parties. All submitted information is filled by me in relation to myself personally.
</p>
<strong>
    This consent is valid for the entire period of storage of personal data, unless otherwise provided by the legislation of the Russian Federation.
</strong>
 <br>