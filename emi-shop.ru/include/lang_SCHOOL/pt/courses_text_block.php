﻿<?if ($_SERVER['REQUEST_URI']=='/courses/') {?>
<div>
	<p>A escola Ekaterina Miroshnichenko oferece cursos de design de unhas populares. Os cursos são realizados por especialistas experientes nas técnicas originais. Você tem uma oportunidade única para melhorar as suas habilidades ou obter uma nova profissão. Os cursos de formação são divididos em 3 níveis de complexidade. Escolha um curso de complexidade adequada e inicie a sua formação numa data próxima.</p>
<p>Ekaterina Miroshnichenko cria regularmente novas técnicas e formas de desenhos de unhas que ela partilha com as suas alunas no âmbito da formação de design de unhas.</p>
<h2>As principais vantagens dos cursos de design de unhas são:</h2>
<ul>
<li>Aprender tendências modernas de nail art sob a orientação de profissionais.</li>
<li>Aprender os principais princípios de trabalho com os produtos E.Mi de alta qualidade.</li>
<li>Programas gerais desenvolvidos para especialistas, sem educação artística.</li>
</ul>
<h2>O que inclui a formação?</h2>
<p>Atualmente temos os nossos representantes da escola em toda a Rússia, na Comunidade de Estados Independentes e outros países estrangeiros. É possível participar nos cursos de design de unhas de acordo com a programação do representante mais próximo de si.</p>
<p>Durante o curso você vai aprender as principais técnicas de design dos modelos escolhidos, treinar a sua execução, obter aconselhamento prático e instruções.</p>
<p>Após o fim do curso de formação de design de unhas você será capaz de executar ideias criativas interessantes, oferecer às suas clientes novos serviços e até mesmo participar em competições profissionais de alto nível.</p>
</div><br>
<?}?>