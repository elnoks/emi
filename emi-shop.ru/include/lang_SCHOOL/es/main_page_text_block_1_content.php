<h2 class="heading">Ekaterina Miroshnichenko
  <br />
 <span class="subheading">DE UNA MAESTRA DE UÑAS A CAMPEONA DEL MUNDO</span></h2>
 
<div> 
  <div><font size="4">Fundadora y principal tecnóloga de la marca E.Mi, autora y diseñadora de Enamel over Gold (2008), Imitation of a reptile skin (2009), Crackled Effect y Ethnic Prints (2010), Velvet sand y Liquid stones (2012), 3D Vintage (2013), TEXTONE y Combiture (2014); un diseñadora de materiales únicos: Black Tulip, Velvet sand, GLOSSEMI, EMPASTA, TEXTONE, PRINCOT; una campeóna mundial de diseño de uñas (París, 2010), dos veces campeóna de Europa (Atenas, París, 2009), árbitra internacional.</font></div>
 
  <div> 
    <br />
   </div>
 
  <div> 
    <br />
   </div>
 </div>
