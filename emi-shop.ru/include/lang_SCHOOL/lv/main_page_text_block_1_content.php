
<h2 class="heading">Jekaterina Mirošničenko
  <br />
 <span class="subheading">No meistara līdz pasaules čempionam</span></h2>
 
<div> 
  <div><font size="4">Zīmola E.Mi dibinātājam un galvenā tehnoloģe, tādu tehnoloģiju kā “Zelta lējums” (2008. gadā), “Rāpuļu ādas imitācija” (2009. gadā), “Krakelūra efekts un etniskie motīvi” (2010. gadā), “Samtainās smiltis un šķidrie akmeņi” (2012. gadā), “Apjomīgā vintāža” (2013. gadā), TEXTONE&Combiture (2014. gadā) un unikālu materiālu rādītāja šo tehniku realizācijai: “Melnā tulpe”, “Samtainās smiltis”, GLOSSEMI, EMPASTA, TEXTONE, PRINCOT; kā arī pasaules čempione nagu dizainā (Parīzē 2010. gadā) un divkārtējā Eiropas nagu dizaina čempione (Atēnās un Parīzē, 2009. gadā), starptautiskās kategorijas nagu dizaina un skaistuma tiesnese.</font></div>
 
  <div> 
    <br />
   </div>
 
  <div> 
    <br />
   </div>
 </div>
