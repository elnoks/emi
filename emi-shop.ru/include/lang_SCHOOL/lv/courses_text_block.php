﻿<?if ($_SERVER['REQUEST_URI']=='/courses/') {?>
<div>
	<p>Jekaterinas Mirošničenko Nagu dizaina skola piedāvā aktuālus nagu dizaina kursus. Apmācības veic pieredzējuši meistari, kas vadās ar Jekaterinas Mirošničenko izstrādāto autora metodiku. Jums ir unikāla iespēja paaugstināt savu profesionālo kompetenci un meistarību, kā arī pilnīgu jaunu amatu un iemaņas. Mācību programmas iedalītas trīs sarežģītības līmeņos. Izvēlieties atbilstošas sarežģītības pakāpes kursus un uzsāciet nodarbības jau tuvākos pieejamos datumos.</p>
<p>Jekaterina Mirošničenko regulāri izstrādā un piedāvā jaunas tehnikas un paņēmienus nagu stila veidošanai, ar kuriem viņa dalās savu klausītāju vidū, apmācot viņus nagu dizaina amatu noslēpumos.</p>
<h2>Galvenās nagu dizaina kursu priekšrocības</h2>
<ul>
<li>Iespēja apgūt aktuālus nagu mākslas un dizaina virzienus, tehnikas un noslēpumus kopā ar amata profesionāļiem, tādējādi paplašinot un uzlabojot savu profesionālo kompetenci.</li>
<li>Izpētīt principus, kas ir pamatā E.Mi augstākās kvalitātes populārās nagu dizaina un kopšanas produkcijas iedarbības pamatā.</li>
<li>Universālas programmas, kas izstrādātas un paredzētas speciali tiem nagu kopšanas speciālistiem, kuriem nav mākslinieciskās izglītības.</li>
</ul>
<h2>Kas paredzēts apmācību ietvaros?</h2>
<p>J. Mirošničenko Nagu dizaina skolas pārstāvniecības ir atvērtas un veiksmīgi darbojās gan Krievijā, gan arī daudzās citās valstīs, tostarp arī Latvijā. Apgūt nagu dizaina kursus Rīgā iespējams atbilstoši plānoto kursu un apmācību sarakstam.</p>
<p>Nodarbībās Jums būs iespēja iepazīties ar galvenajiem izvēlētas nagu dizaina veidošanas tehnikas paņēmieniem, iespējams trenēties to izpildīšanā, kā arī iegūsiet praktiskus padomus un ieteikumus no pasniedzējiem.</p>
<p>Beidzot nagu dizaina apmācības Jums būs iespēja realizēt aizraujošas radošas idejas, kā arī piedāvāt saviem klientiem jaunu pakalpojumu klāstu un pat piedalīties augstākā līmeņa amatu sāncensībās un konkursos.</p>
</div><br>
<?}?>