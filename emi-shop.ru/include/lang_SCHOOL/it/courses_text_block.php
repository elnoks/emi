<?if ($_SERVER['REQUEST_URI']=='/courses/') {?>
<div>
	<p>La scuola di Ekaterina Miroshnichenko offre corsi sulle nail art più famose. I corsi sono condotti da esperti specializzati nelle tecniche originali E.Mi: hai un’opportunità unica per migliorare le tue capacità o imparare una nuova professione. I corsi sono divisi in 3 livelli di difficoltà, scegli un corso proporzionato con le tue capacità e inizia subito la tua formazione.</p>
<p>Ekaterina Miroshnichenko regolarmente crea nuove tecniche e tipologie di nail design che condivide con i suoi studenti durante i corsi.</p>
<h2>I principali vantaggi dei corsi sono:</h2>
<ul>
<li>Imparare le tendenze della nail art moderna sotto la guida di una professionista.</li>
<li>Imparare le principali caratteristiche del lavoro con i prodotti di alta qualità E.Mi.</li>
<li>Disponibilità di corsi generali sviluppati appositamente per onicotecniche senza formazione artistica.</li>
</ul>
</div><br>
<?}?>