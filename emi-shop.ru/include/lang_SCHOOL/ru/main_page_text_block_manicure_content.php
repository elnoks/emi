
<h2 class="heading">E.Mi-маникюр
    <br />
 </h2>
 
<div><span class="subheading"><font size="4"> </font> 
    <div><font size="4">Комплексные услуги E.Mi-маникюр востребованы у клиентов  32000 салонов красоты в 156 городах мира. Европейские стандарты качества продукции E.Mi  гарантируют безопасность процедуры для мастера и его клиента.</font></div>

</div>
<div>
    <h3 class="heading">
        <span class="subheading">Быстро</span></h3>
    <div class="icons-img">
        <img src="include/img/icons/icon_7.png" alt=""/><font size="4">Все готово за 1-1,5 часа</font>
    </div>

    <h3 class="heading">
        <span class="subheading">Модно</span></h3>
    <div class="icons-img">
        <img src="include/img/icons/icon_8.png" alt=""/><font size="4">Каждый месяц новые коллекции</font>
    </div>

    <h3 class="heading">
        <span class="subheading">Стойко</span></h3>
    <div class="icons-img">
        <img src="include/img/icons/icon_9.png" alt=""/><font size="4">Безупречный маникюр на 1 месяц</font>
    </div>

</div>