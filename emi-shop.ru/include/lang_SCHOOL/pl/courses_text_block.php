﻿<?if ($_SERVER['REQUEST_URI']=='/courses/') {?>
<div>
	<p>Szkoła Ekateriny Miroshnichenko oferuje aktualne kursy w zakresie stylizacji paznokci. Szkolenia prowadzone są przez doświadczonych instruktorów metodą autorską. Masz wyjątkową możliwość doskonalenia swoich umiejętności lub opanowania nowego zawodu. Programy nauczania są podzielone na trzy poziomy złożoności. Wybierz kursy o odpowiedniej złożoności i rozpocznij zajęcia w nadchodzących terminach.</p>
<p>Ekaterina Miroshnichenko regularnie oferuje nowe techniki stylizacji paznokci, które dzieli ze swoimi słuchaczami w ramach nauczania stylizacji paznokci.</p>
<h2>Główne zalety szkoleń stylizacji paznokci:</h2>
<ul>
<li>Opanowanie aktualnych technik nail art pod okiem profesjonalistów.</li>
<li>Nauka i szczegóły pracy na popularnych i wysokojakościowych produktach E.Mi.</li>
<li>Unikatowe programy szkoleniowe, przeznaczone do stylistek paznokci, nie mających wykształcenia artystycznego.</li>
</ul>
<h2>Co obejmuje szkolenie?</h2>
<p>Przedstawicielstwa szkoły na dzień dzisiejszy otwarte są w Rosji, Europie i Azji. Skorzystać z kursów stylizacji paznokci można w najbliższym do Ciebie przedstawicielstwie zgodnie z harmonogramem szkoleń.</p>
<p>Po ukończeniu szkolenia z projektowania paznokci będziesz mógł realizować ciekawe, kreatywne pomysły, oferować swoim klientom nowe usługi, a nawet brać udział w profesjonalnych zawodach na wysokim poziomie.</p>
</div><br>
<?}?>