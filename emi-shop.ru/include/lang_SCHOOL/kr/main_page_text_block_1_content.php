<h2 class="heading">Ekaterina Miroshnichenko
  <br />
 <span class="subheading">From a nail master to a world champion</span></h2>
 
<div> 
  <div><font size="4">Ekaterina Miroshnichenko는 ‘판타지’ 부분의 월드챔피언 (MLA, Paris, 2010)으로 많이 알려져있습니다. 이 밖에도 유로피언 챔피언을 2회 (Athens, Paris, 2009)나 수상하는 등 다수의 화려한 경력을 자랑합니다. 네일아트 국제 심사위원으로도 활동하고 있는 그녀는 네일디자인 트레이닝 및 매뉴얼 교재의 저자이기도 합니다.</font></div>
 
  <div> 
    <br />
   </div>
 
  <div> 
    <br />
   </div>
 </div>
