
<h2 class="heading">E.Mi brand
  <br />
 <span class="subheading">The history of success.</span></h2>
 
<div>
    <div><font size="4"> «E.Mi» 는 모든 아티스트 분들의 안목과 취향을 고려한 고퀄러티 네일아트용품 브랜드로 독일에서 특별생산되는 컬러젤과 브러쉬, 네일 데코재료들과 그 외 다양한 네일도구를 판매하고 있습니다. 지금까지 여러분들이 꿈꿔오셨던 편리함을 «E.Mi»를 통해 느끼실 수 있을 것입니다.</font></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div><font size="4">«E.Mi»는 1분간의 중합과정을 거쳐 고객 분들께 차별화된 품질을 제공해 드리고 있습니다. 또한 «E.Mi» 만의 특별한 제작기술로 컬러를 사용할 때 흐르지 않아 한번에 모든 손에 아트가 가능하며, 원하시던 결과를 그대로 느끼실 수 있습니다.</font></div>

</div>