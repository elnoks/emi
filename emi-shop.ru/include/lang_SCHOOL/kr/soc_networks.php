<a href="https://www.facebook.com/EmiKoreaSchool/" target="_blank" title="EMi Korea @EmiKoreaSchool">
	<i class="icon-social fb"></i>
</a>
<a href="https://www.instagram.com/emikorea/" target="_blank" title="E.Mi School of Nail Design in Korea">
	<i class="icon-social in"></i>
</a>
<a href="http://www.youtube.com/user/emischoolcom/featured" target="_blank" title="Channel Ekaterina Miroshnichenko on YouTube">
	<i class="icon-social yt"></i>
</a>