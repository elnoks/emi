<h2 class="heading">Ekaterina Miroshnichenko
  <br />
 <span class="subheading">From a nail master to a world champion</span></h2>
 
<div> 
  <div><font size="4">A founder and the main E.Mi brand technologist, an author and designer of  Enamel over Gold (2008), Imitation of a reptile skin (2009) , Crackled Effect and Ethnic Prints (2010), Velvet sand and Liquid stones (2012), 3D Vintage (2013), TEXTONE&Combiture (2014); a designer of unique materials: Black Tulip, Velvet sand, GLOSSEMI, EMPASTA, TEXTONE, PRINCOT; a world champion nail design (Paris, 2010), two-time European champion (Athens, Paris, 2009), an international referee.</font></div>
 
  <div> 
    <br />
   </div>
 
  <div> 
    <br />
   </div>
 </div>
