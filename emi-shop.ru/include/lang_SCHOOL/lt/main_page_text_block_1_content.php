<h2 class="heading">Jekaterina Mirošničenko
  <br />
 <span class="subheading">Nuo nagų meistrės iki pasaulio čempionato nugalėtojos</span></h2>
 
<div> 
  <div><font size="4">Įkurėja ir pagrindinė E.Mi prekinio ženklo technologė, autorė ir dizainerė – Auksinė emalė (2008), Odos imitacija. Reptilijos (2009), Krakeliūras ir Etnoprintai (2010), Barchatinis smėlis ir Akmenų gelis (2012), 3D Vintažas (2013), Textonai ir Prinkotai (2014); Dizainerė unikalių priemonių – Black Tulip, Barchatinis smėlis, Glossemi, Empasta, Textone, Princot; nagų dizaino pasalio čempionato nugalėtoja (Paryžius 2010), du kartus Europos nagų dizaino čempionatų nugalėtoja (2009 m. Atėnai, Paryžius), tarptautinės kategorijos teisėja.</font></div>
 
  <div> 
    <br />
   </div>
 
  <div> 
    <br />
   </div>
 </div>
