
<h2 class="heading">E.Mi manicure
    <br />
 </h2>
 
<div><span class="subheading"><font size="4"> </font> 
    <div><font size="4">The popularity of the comprehensive E.Mi manicure speaks for itself, with beauty studios in 156 cities around the world providing the service to 32 000 clients. All E.Mi products are made to be on par with the European quality standards, guaranteeing the procedure’s safety to both the client and the nail artist.</font></div>
</div>


<h2 class="heading">
    <span class="subheading">Always in trend.</span></h2>