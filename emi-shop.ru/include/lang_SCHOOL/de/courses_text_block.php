﻿<?if ($_SERVER['REQUEST_URI']=='/courses/') {?>
<div>
	<p>Ekaterina Miroshnichenko’s school offers popular naildesign courses. The courses are conducted by experienced experts of the original techniques. You have a unique opportunity to improve your skills or get a new profession. The training courses are divided into 3 levels of complexity. Choose a course of a corresponding complexity and start your training at an early date.</p>
<p>Ekaterina Miroshnichenko regularly creates new techniques and ways of nail designs which she shares with her students in the framework of nail design training.</p>
<h2>The main advantages of nail design courses are:</h2>
<ul>
<li>Learning modern nail-art tendencies under professionals’ guidance.</li>
<li>Learning the main principles of work with popular E.Mi products of high quality.</li>
<li>General programs developed for specialists without artistic education.</li>
</ul>
<h2>What does the training include?</h2>
<p>Currently we have our school representatives all over Russia, in the CIS and other foreign countries. It is possible to study at the nail design courses in Moscow and other cities according to the schedule of the nearest to you representative.</p>
<p>During the course you will learn the main techniques of the chosen nail-art design, train their performance, get practical advice and instructions.</p>
<p>After the end of the nail design training course you will be able to realize interesting creative ideas, offer your clients new services and even take part in professional competitions at a high level.</p>
</div><br>
<?}?>