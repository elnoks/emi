<?
    if (LANGUAGE_ID=="en"||LANGUAGE_ID=="uk"||LANGUAGE_ID=="af"||LANGUAGE_ID=="ln"||LANGUAGE_ID=="dn"){
        if (LANGUAGE_ID=="en"){
            ?>
            <span class="flag-icon flag-icon-eu"></span><span class="caret"></span>
            <?
        }
        if (LANGUAGE_ID=="uk"){
            ?>
            <span class="flag-icon flag-icon-gb"></span><span class="caret"></span>
            <?
        }
        if (LANGUAGE_ID=="af"){
            ?>
            <span class="flag-icon flag-icon-za"></span><span class="caret"></span>
            <?
        }
        if (LANGUAGE_ID=="ln"){
            ?>
            <span class="flag-icon"></span><span class="caret"></span>
            <?
        }
        if (LANGUAGE_ID=="ds"){
            ?>
            <span class="flag-icon"></span><span class="caret"></span>
            <?
        }
    }
    else {
        if(LANGUAGE_ID != 'ae' || LANGUAGE_ID != 'ro'){
            ?>
            <span class="flag-icon flag-icon-<?=LANGUAGE_ID?>"></span><span class="caret"></span>
            <?
        }
    }
    //echo $_SERVER["DOCUMENT_ROOT"];
?>

<div id="choose-country-popup" class="country-container">
    <?if(LANGUAGE_ID != 'ae' || LANGUAGE_ID != 'ro'){?>
        <div class="col-sm-6">
            <a href='https://emischool.com'><span class="flag-icon flag-icon-eu"></span><span>Europe</span></a>
            <a href='https://emi-school.ru'><span class="flag-icon flag-icon-ru"></span><span>Russia</span></a>
            <a href='https://emischool.az'><span class="flag-icon flag-icon-az"></span><span>Azerbaijan</span></a>
            <a href='http://emi-shop.by'><span class="flag-icon flag-icon-by"></span><span>Belarus</span></a>
            <a href='https://emischool.com.cy'><span class="flag-icon flag-icon-cy"></span><span>Cyprus</span></a>
            <a href='https://emischool.cz'><span class="flag-icon flag-icon-cz"></span><span>Czech Republic</span></a>
            <?
            /* AVOLOBUEV 26.04
             <a href='https://dnr.emi-school.ru'><span class="flag-icon flag-icon-dn"></span><span>Donetsk People's Republic</span></a>
             */
            ?>
            <a href='https://dnr.emi-school.ru'><span class="flag-icon "></span><span>Donetsk region</span></a>
            <a href='https://emischool.ee'><span class="flag-icon flag-icon-ee"></span><span>Eesti</span></a>
            <a href='https://emischool.fr'><span class="flag-icon flag-icon-fr"></span><span>France</span></a>
            <a href='https://www.emschool-germany.com'><span class="flag-icon flag-icon-de"></span><span>Germany</span></a>
            <a href='https://emischool.ie'><span class="flag-icon flag-icon-ie"></span><span>Ireland</span></a>
            <a href='https://emischool.it'><span class="flag-icon flag-icon-it"></span><span>Italy</span></a>
            <a href='https://emischool.kz'><span class="flag-icon flag-icon-kz"></span><span>Kazakhstan</span></a>
            <a href='https://emischool.kg'><span class="flag-icon flag-icon-kg"></span><span>Kyrgyzstan</span></a>
        </div>
        <div class="col-sm-6">
            <a href='https://emischool.lv'><span class="flag-icon flag-icon-lv"></span><span>Latvija</span></a>
            <a href='https://emischool.lt'><span class="flag-icon flag-icon-lt"></span><span>Lithuania</span></a>

            <a href='https://lnr.emi-school.ru'><span class="flag-icon "></span><span>Lugansk region</span></a>
            <? /*
            <a href='https://lnr.emi-school.ru'><span class="flag-icon flag-icon-ln"></span><span>Lugansk People's Republic</span></a>
            */ ?>

            <a href='https://emischool.md'><span class="flag-icon flag-icon-md"></span><span>Moldova</span></a>
            <a href='https://emischool.pl'><span class="flag-icon flag-icon-pl"></span><span>Poland</span></a>
            <a href='https://emischool.co.za'><span class="flag-icon flag-icon-za"></span><span>Republic of South Africa</span></a>
            <a href='https://emischool.ro'><span class="flag-icon flag-icon-ro"></span><span>Romania</span></a>
            <a href='https://emischool.kr'><span class="flag-icon flag-icon-kr"></span><span>South Korea</span></a>
            <a href='https://emischool.es'><span class="flag-icon flag-icon-es"></span><span>Spain</span></a>
            <a href='https://emischool.ch'><span class="flag-icon flag-icon-ch"></span><span>Switzerland</span></a>
            <a href='https://emischool.tn'><span class="flag-icon flag-icon-tn"></span><span>Tunisia</span></a>
            <a href='https://emi.ua'><span class="flag-icon flag-icon-ua"></span><span>Ukraine</span></a>
            <a href='https://emischool.ae'><span class="flag-icon flag-icon-ae"></span><span>United Arab Emirates</span></a>
            <a href='https://emischool.uk'><span class="flag-icon flag-icon-gb"></span><span>United Kingdom</span></a>
        </div>
    <?}else{?>

    <?}?>
</div>
<?//script to choose-country-popup was placed in file templates/.default/scrits/script.js?>
