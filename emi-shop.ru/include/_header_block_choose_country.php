<span class="flag"><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/<?=LANGUAGE_ID?>.png" alt="<?=GetMessage("HEADER_CHOOSE_COUNTRY_".LANGUAGE_ID)?>"></span><span class="caret"></span>
<div id="choose-country-popup" class="country-container">
    <?if(LANGUAGE_ID != 'ae'){?>
        <!-- 26-05-2015 Начало -->
        <a href='http://emischool.com'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/en.png" alt="Europe"><span>Europe</span></a>
        <a href='http://emi-school.ru'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/ru.png" alt="Russia"><span>Russia</span></a>
        <a href='http://emi-shop.by'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/by.png" alt="Belarus"><span>Belarus</span></a>
        <a href='http://emischool.com.cy'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/cy.png" alt="Cyprus"><span>Cyprus</span></a>
        <a href='http://emischool.cz'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/cz.png" alt="Czech Republic"><span>Czech Republic</span></a>
		<a href='http://dnr.emi-school.ru'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/dn.png" alt="Donetsk People's Republic"><span>Donetsk People's Republic</span></a>
        <a href='http://emischool.ee'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/ee.png" alt="Eesti"><span>Eesti</span></a>
        <a href='http://emischool.fr'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/fr.png" alt="France"><span>France</span></a>
        <a href='https://www.emschool-germany.com'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/de.png" alt="Germany"><span>Germany</span></a>
        <a href='http://emischool.ie'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/ie.png" alt="Ireland"><span>Ireland</span></a>

        <a href='http://emischool.it'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/it.png" alt="Italy"><span>Italy</span></a>
        <a href='http://emischool.kz'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/kz.png" alt="Kazakhstan"><span>Kazakhstan</span></a>
        <a href='http://emischool.kg'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/kg.png" alt="Kyrgyzstan"><span>Kyrgyzstan</span></a>
        <a href='http://emischool.lv'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/lv.png" alt="Latvija"><span>Latvija</span></a>
        <a href='http://emischool.lt'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/lt.png" alt="Lithuania"><span>Lithuania</span></a>
        <a href='http://emischool.md'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/md.png" alt="Moldova"><span>Moldova</span></a>
        <a href='http://kr.emischool.com'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/kr.png" alt="South Korea"><span>South Korea</span></a>
        <a href='http://emi.ua'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/ua.png" alt="Ukraine"><span>Ukraine</span></a>
        <a href='http://emischool.ae'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/ae.png" alt="United Arab Emirates"><span>United Arab Emirates</span></a>

        <!--<a href='http://emischool.pt'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/pt.png" alt="Portugal"><span>Portugal</span></a>-->
        <!-- <a href='http://emischool.sk'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/sl.png" alt="Slovakia"><span>Slovakia</span></a> 25-04-2016 Шабанов -->

        <!-- Михаил 26-05-2015 Конец -->

    <?}else{?>
        <a href='http://emischool.ae'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/ae.png" alt="United Arab Emirates"><span>United Arab Emirates</span></a>
        <a href='http://emischool.com'><img src="<?=DEFAULT_TEMPLATE_DIR_PATH?>img/countries/en.png" alt="Europe"><span>Europe</span></a>
    <?}?>
</div>
<?//script to choose-country-popup was placed in file templates/.default/scrits/script.js?>
