<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-5">
        <img src="<?= SITE_TEMPLATE_PATH . '/images/'; ?>main_photo.jpg" style="width: 100%"
             class="img-responsive shadow">
    </div>
    <div class="col-lg-8 col-md-8 col-sm-7">
        <h2>E.Mi</h2>

        <p>E.Mi – international brand of fashion salon manicure. E.Mi choose stars, nail industry leaders and
            stylish women in 28 countries of the world.</p>

        <p>E.Mi love for European quality, constant innovations in the creation of materials, a unique approach to design
            nail and integrated business solutions for beauty salons.</p>

        <h2 class="heading">Global brand</h2>

        <p>E.Mi – this is a global brand. To create products using raw materials from world leading suppliers from
            Europe, South Korea and the United States.</p>
        <br>
        <a href="<?= SITE_DIR ?>company/" class="btn btn-default">Read more</a>
    </div>
</div>