<? $frame = new \Bitrix\Main\Page\FrameHelper('header-regionality-block'); ?>
<? $frame->begin(); ?>
<?

if ($USER->isAdmin() && $USER->GetID() == "30469" && $_REQUEST["type"] == "test") {

    $APPLICATION->IncludeComponent(
        "emi:departament",
        ".default",
        array(
            "COMPONENT_TEMPLATE" => ".default",
            "COMPOSITE_FRAME_MODE" => "A",
            "COMPOSITE_FRAME_TYPE" => "AUTO",
            "HL_BLOCK_ID" => "17"
        ),
        false
    );
} else {
    $APPLICATION->IncludeComponent(
        "BKV:departament",
        "popup_regions",
        Array()
    );
}

?>

<? $frame->end(); ?>