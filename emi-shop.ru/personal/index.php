<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(GetMessage("PERSONAL_TITLE"));
?>
<?
global $USER;
if(!$USER->isAuthorized()){
	LocalRedirect(SITE_DIR.'auth/');
}
else{
	//LocalRedirect(SITE_DIR.'personal/personal-data');?>
	<?$APPLICATION->IncludeComponent(
	"bitrix:sale.personal.section", 
	"main", 
	array(
		"ACCOUNT_PAYMENT_ELIMINATED_PAY_SYSTEMS" => array(
			0 => "0",
		),
		"ACCOUNT_PAYMENT_PERSON_TYPE" => "1",
		"ACCOUNT_PAYMENT_SELL_SHOW_FIXED_VALUES" => "Y",
		"ACCOUNT_PAYMENT_SELL_TOTAL" => array(
			0 => "100",
			1 => "200",
			2 => "500",
			3 => "1000",
			4 => "5000",
			5 => "",
		),
		"ACCOUNT_PAYMENT_SELL_USER_INPUT" => "N",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHECK_RIGHTS_PRIVATE" => "N",
		"COMPATIBLE_LOCATION_MODE_PROFILE" => "N",
		"CUSTOM_PAGES" => "",
		"CUSTOM_SELECT_PROPS" => array(
		),
		"NAV_TEMPLATE" => "",
		"ORDER_HISTORIC_STATUSES" => array(
			0 => "F",
		),
		"PATH_TO_BASKET" => "/basket/",
		"PATH_TO_CATALOG" => "/catalog/",
		"PATH_TO_CONTACT" => "/contacts",
		"PATH_TO_PAYMENT" => "/order/payment/",
		"PER_PAGE" => "20",
		"PROP_1" => array(
		),
		"PROP_2" => "",
		"SAVE_IN_SESSION" => "Y",
		"SEF_FOLDER" => "/personal/",
		"SEF_MODE" => "Y",
		"SEND_INFO_PRIVATE" => "N",
		"SET_TITLE" => "Y",
		"SHOW_ACCOUNT_COMPONENT" => "N",
		"SHOW_ACCOUNT_PAGE" => "N",
		"SHOW_ACCOUNT_PAY_COMPONENT" => "N",
		"SHOW_BASKET_PAGE" => "Y",
		"SHOW_CONTACT_PAGE" => "Y",
		"SHOW_ORDER_PAGE" => "Y",
		"SHOW_PRIVATE_PAGE" => "Y",
		"SHOW_PROFILE_PAGE" => "Y",
		"SHOW_SUBSCRIBE_PAGE" => "Y",
		"USER_PROPERTY_PRIVATE" => "",
		"USE_AJAX_LOCATIONS_PROFILE" => "N",
		"COMPONENT_TEMPLATE" => "main",
		"ACCOUNT_PAYMENT_SELL_CURRENCY" => "RUB",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"ORDER_HIDE_USER_INFO" => array(
			0 => "0",
		),
		"PROP_51" => array(
		),
		"PROP_12" => array(
		),
		"PROP_14" => array(
		),
		"PROP_55" => array(
		),
		"PROP_8" => array(
		),
		"PROP_62" => array(
		),
		"PROP_64" => array(
		),
		"PROP_27" => array(
		),
		"PROP_25" => array(
		),
		"PROP_35" => array(
		),
		"PROP_18" => array(
		),
		"PROP_16" => array(
		),
		"PROP_20" => array(
		),
		"PROP_49" => array(
		),
		"PROP_52" => array(
		),
		"PROP_13" => array(
		),
		"PROP_15" => array(
		),
		"PROP_5" => array(
		),
		"PROP_9" => array(
		),
		"PROP_65" => array(
		),
		"PROP_28" => array(
		),
		"PROP_26" => array(
		),
		"PROP_36" => array(
		),
		"PROP_58" => array(
		),
		"PROP_19" => array(
		),
		"PROP_17" => array(
		),
		"PROP_21" => array(
		),
		"PROP_50" => array(
		),
		"PROP_61" => array(
		),
		"PROP_33" => array(
		),
		"PROP_34" => array(
		),
		"PROP_37" => array(
		),
		"PROP_38" => array(
		),
		"PROP_39" => array(
		),
		"PROP_40" => array(
		),
		"PROP_41" => array(
		),
		"PROP_42" => array(
		),
		"PROP_43" => array(
		),
		"PROP_44" => array(
		),
		"PROP_45" => array(
		),
		"PROP_46" => array(
		),
		"PROP_47" => array(
		),
		"PROP_48" => array(
		),
		"PROP_6" => array(
		),
		"PROP_4" => array(
		),
		"PROP_29" => array(
		),
		"PROP_31" => array(
		),
		"PROP_57" => array(
		),
		"PROP_7" => array(
		),
		"PROP_30" => array(
		),
		"PROP_32" => array(
		),
		"PROP_3" => array(
		),
		"PROP_66" => array(
		),
		"PROP_67" => array(
		),
		"ORDER_RESTRICT_CHANGE_PAYSYSTEM" => array(
			0 => "0",
		),
		"ORDER_DEFAULT_SORT" => "STATUS",
		"ORDER_REFRESH_PRICES" => "N",
		"ALLOW_INNER" => "N",
		"ONLY_INNER_FULL" => "N",
		"ORDERS_PER_PAGE" => "20",
		"PROFILES_PER_PAGE" => "20",
		"MAIN_CHAIN_NAME" => "Мой кабинет",
		"SEF_URL_TEMPLATES" => array(
			"index" => "index.php",
			"orders" => "orders/",
			"account" => "account/",
			"subscribe" => "subscribe/",
			"profile" => "profiles/",
			"profile_detail" => "profiles/#ID#",
			"private" => "private/",
			"order_detail" => "orders/#ID#",
			"order_cancel" => "cancel/#ID#",
		)
	),
	false
);?>
<?}?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>