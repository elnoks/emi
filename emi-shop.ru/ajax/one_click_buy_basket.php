<?define("STATISTIC_SKIP_ACTIVITY_CHECK", "true");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?if(\Bitrix\Main\Loader::includeModule('aspro.next')):?>
	<?$APPLICATION->IncludeComponent("BKV:oneclickbuy.next", "shop", array(
		"BUY_ALL_BASKET" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600000",
		"CACHE_GROUPS" => "N",
        "EMAIL_SEND"=>$_REQUEST["EMAIL"],
		"SHOW_LICENCE" => CNext::GetFrontParametrValue('SHOW_LICENCE'),
		"SHOW_DELIVERY_NOTE" => COption::GetOptionString('aspro.next', 'ONECLICKBUY_SHOW_DELIVERY_NOTE', 'N', SITE_ID),
		"PROPERTIES" => (strlen($tmp = COption::GetOptionString('aspro.next', 'ONECLICKBUY_PROPERTIES', 'FIO,PHONE,EMAIL,COMMENT', SITE_ID)) ? explode(',', $tmp) : array()),
		"REQUIRED" => (strlen($tmp = COption::GetOptionString('aspro.next', 'ONECLICKBUY_REQUIRED_PROPERTIES', 'FIO,PHONE', SITE_ID)) ? explode(',', $tmp) : array()),
        "DEFAULT_PERSON_TYPE" => 68,
        "DEFAULT_DELIVERY" => 165,
        "DEFAULT_PAYMENT" => 145,
		),
		false
	);?>
<?endif;?>