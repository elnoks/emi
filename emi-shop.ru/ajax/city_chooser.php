<? define("STATISTIC_SKIP_ACTIVITY_CHECK", "true"); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>

<a href="#" class="close jqmClose"><i></i></a>
<div class="form">
    <div class="form_head">
        <h2><?= \Bitrix\Main\Localization\Loc::getMessage('CITY_CHOISE'); ?></h2>
    </div>

    <? global $arTheme, $APPLICATION;
    $arTheme = CNext::GetFrontParametrsValues(SITE_ID);
    $urlback = htmlspecialchars($_GET['url']);/*if($urlback)
		$urlback = urldecode($urlback)*/;

    $template = strtolower($arTheme["REGIONALITY_VIEW"]);
    if ($arTheme["REGIONALITY_SEARCH_ROW"] == "Y" && $template == "select")
        $template = "popup_regions";
    ?>


    <?
        $APPLICATION->IncludeComponent(
            "BKV:departament_NEW_LOGIK",
            "popup_regions",
            Array(
                "URL" => $urlback,
                "POPUP" => "Y",
                "DEP_URL_AJAX" => $_SESSION["DEP_URL_AJAX"]
            )
        );
    ?>
    <script type="text/javascript">

        var url_dir_aja = "<?=$urlback?>";

        var xhr;
        if (xhr && xhr.readyState != 4) {
            xhr.abort();
        }
        $(document).ready(function () {

            $("#search").autocomplete({
                source: function (request, response) {
                    xhr = $.ajax({
                        type: 'POST',
                        url: '/local/components/BKV/mobileLocation/templates/.default/ajax.php',
                        dataType: "json",
                        data: {
                            "STR": request.term,
                            "DEP": DEPORTAMENT,
                            "THIS_URL": url_dir_aja
                        },
                        success: function (msg) {
                            if (jQuery.type(msg) === "null") {
                                msg = {0: {"NAME": "По данному запросу не найдено"}};
                            }

                            console.log(msg);
                            response(msg);
                        }
                    });
                },
                minLength: 3,
                appendTo: $("#search").parent()
            })
                .data("ui-autocomplete")._renderItem = function (ul, item) {
                var region = (item.REGION_NAME ? " (" + item.REGION_NAME + ")" : "");
                return $("<li>")
                    .data("item.autocomplete", item)
                    .append('<form   data-id="' + item.ID + '" action="https://' + item.URL + '" method="POST">' +
                        '<input type="hidden" name="LOC_ID" value="' + item.ID + '">' +
                        '<input type="hidden" name="BS" value="' + item.BUSCKET + '">' +
                        '<button class="cityLink" type="submit">' + item.NAME + region+'</button>' +
                        '</form>'
                    )
                    .appendTo(ul);
            };
        });

        var current_region_item = $('.cities .items_block .item.current'),
            current_region_obl = '';

        $('.cities .item:not(.current)').each(function () {
            if ($(this).data('id_parent') == current_region_item.data('id')) {
                $(this).addClass('shown');

            }
        });

        if ($('.popup_regions .parent_block').length) {
            $('.popup_regions .parent_block').each(function () {
                var _this = $(this),
                    item = _this.find('.item[data-id=' + current_region_item.data('id') + ']');

                if (item.length) {
                    item.addClass('current');
                    current_region_obl = item.parent();
                    current_region_obl.addClass('current shown');

                }
            })
        }
        if ($('.popup_regions .block.regions').length) {
            $('.popup_regions .block.regions').each(function () {
                var _this = $(this),
                    obl_block = _this.find('.parent_block'),
                    item = '';
                if (!obl_block.length) {
                    var countRegion = $(".regions .item");
                    if (countRegion.length > 0) {
                        if (current_region_obl) {
                            item = _this.find('.item[data-id=' + current_region_obl.data('id') + ']').addClass('current');

                            if (item.length) {
                                item.addClass('current');
                                current_region_obl = item.parent();
                                current_region_obl.addClass('current shown');

                                var idRegion = current_region_obl.find(".current").attr("data-id");
                                var currentCity = $(".cities .current");
                                var currentCityID = currentCity.find("form").attr("data-id");
                                var nameCity = currentCity.text();
                                nameCity = nameCity.replace(/^\s*/
                                    , '').replace(/\s*$/, '');


                                if (idRegion > 0) {
                                    $.ajax({
                                        type: "POST",
                                        url: "/local/JQactionADMIN/getVilige.php",
                                        dataType: "json",
                                        beforeSend: function () {

                                            if ($(".block.cities .title .search_loader_li_desc").length < 1) {
                                                $(".block.cities .title").append(' <span class="menu_title search_loader_li_desc"><span class="search_loader"> </span></span>');
                                            }
                                        },
                                        data: {
                                            "raiyon": idRegion,
                                            "type": ["CITY", "VILLAGE"],
                                            "selectLoc": currentCityID,
                                            "this_url": url_dir_aja
                                        },
                                        success: function (msg) {

                                            if ($(".search_loader_li_desc").length > 0) {
                                                $(".search_loader_li_desc").remove()
                                            }


                                            if (msg != "false") {
                                                $.each(msg.LOCATION, function (indexV, valueV) {

                                                    if (nameCity != valueV.NAME) {
                                                        var string =
                                                            '<div class="item  shown" data-id="' + valueV.REGION_ID + '" data-id_parent="' + valueV.PARENT_ID + '">' +
                                                            '<form  data-id="' + valueV.ID + '" action="https://' + valueV.URL + '" method="POST">' +
                                                            '<input type="hidden" name="LOC_ID" value="' + valueV.ID + '">' +
                                                            '<input type="hidden" name="BS" value="' + msg.BUSCKET + '">' +
                                                            '<input type="hidden" name="DEP_ID" value="' + valueV.ID_DEP + '">' +
                                                            '<button type="submit">' + valueV.NAME + '</button>' +
                                                            '</form>' +
                                                            '</div>';

                                                        $(".block.cities .items_block").append(string);
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        } else {

                            item = _this.find('.item[data-id=' + current_region_item.data('id') + ']');
                            if (item.length) {
                                item.addClass('current');
                                current_region_obl = item.parent();
                                current_region_obl.addClass('current shown');

                                var idRegion = current_region_obl.find(".current").attr("data-id");
                                var currentCity = $(".cities .current");
                                var currentCityID = currentCity.find("form").attr("data-id");
                                var nameCity = currentCity.text();
                                nameCity = nameCity.replace(/^\s*/
                                    , '').replace(/\s*$/, '');


                                if (idRegion > 0) {
                                    $.ajax({
                                        type: "POST",
                                        url: "/local/JQactionADMIN/getVilige.php",
                                        dataType: "json",
                                        beforeSend: function () {

                                            if ($(".block.cities .title .search_loader_li_desc").length < 1) {
                                                $(".block.cities .title").append(' <span class="menu_title search_loader_li_desc"><span class="search_loader"> </span></span>');
                                            }
                                        },
                                        data: {
                                            "raiyon": idRegion,
                                            "type": ["CITY", "VILLAGE"],
                                            "selectLoc": currentCityID,
                                            "this_url": url_dir_aja
                                        },
                                        success: function (msg) {


                                            if ($(".search_loader_li_desc").length > 0) {
                                                $(".search_loader_li_desc").remove()
                                            }

                                            if (msg != "false") {
                                                $.each(msg.LOCATION, function (indexV, valueV) {

                                                    if (nameCity != valueV.NAME) {
                                                        var string =
                                                            '<div class="item  shown" data-id="' + valueV.REGION_ID + '" data-id_parent="' + valueV.PARENT_ID + '">' +
                                                            '<form  data-id="' + valueV.ID + '" action="https://' + valueV.URL + '" method="POST">' +
                                                            '<input type="hidden" name="LOC_ID" value="' + valueV.ID + '">' +
                                                            '<input type="hidden" name="BS" value="' + msg.BUSCKET + '">' +
                                                            '<input type="hidden" name="DEP_ID" value="' + valueV.ID_DEP + '">' +
                                                            '<button type="submit">' + valueV.NAME + '</button>' +
                                                            '</form>' +
                                                            '</div>';

                                                        $(".block.cities .items_block").append(string);
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
            })
        }

        $(document).on('click', '.popup_regions .block.regions .item', function () {

            var _this = $(this),
                obl_block = _this.parent('.parent_block');
            _this.siblings().removeClass('current');
            _this.addClass('current');


            var parent = _this.closest(".parent_block").data('id');
            var parentChilde = _this.data('id');
            var idBlock, type;

            if (obl_block.length) {
                $.ajax({
                    type: "POST",
                    url: "/local/JQactionADMIN/getVilige.php",
                    beforeSend: function () {

                        if ($(".block.cities .title .search_loader_li_desc").length < 1) {
                            $(".block.cities .title").append(' <span class="menu_title search_loader_li_desc"><span class="search_loader"> </span></span>');
                        }
                    },
                    dataType: "json",
                    data: {
                        "raiyon": parentChilde,
                        "type": ["CITY", "VILLAGE"],
                        "this_url": url_dir_aja
                    },
                    success: function (msg) {

                        if ($(".search_loader_li_desc").length > 0) {
                            $(".search_loader_li_desc").remove()
                        }


                        $(".block.cities .items_block").empty();
                        if (msg != "false") {
                            $.each(msg.LOCATION, function (indexV, valueV) {
                                var string =
                                    '<div class="item  shown" data-id="' + valueV.REGION_ID + '" data-id_parent="' + valueV.PARENT_ID + '">' +
                                    '<form  data-id="' + valueV.ID + '" action="https://' + valueV.URL + '" method="POST">' +
                                    '<input type="hidden" name="LOC_ID" value="' + valueV.ID + '">' +
                                    '<input type="hidden" name="BS" value="' + msg.BUSCKET + '">' +
                                    '<input type="hidden" name="DEP_ID" value="' + valueV.ID_DEP + '">' +
                                    '<button type="submit">' + valueV.NAME + '</button>' +
                                    '</form>' +
                                    '</div>';

                                $(".block.cities .items_block").append(string);
                            });
                        }
                    }
                });


                $('.cities .item').removeClass('current shown');
                $('.cities .item[data-id=' + parentChilde + '][data-id_parent=' + idBlock + ']').addClass('current shown');

            } else {

                if ($('.popup_regions .parent_block').length) {
                    var parent_block = $('.popup_regions .parent_block[data-id=' + _this.data('id') + ']')
                    $('.popup_regions .parent_block').siblings().removeClass('current shown');
                    parent_block.addClass('current shown');

                    $('.cities .item').removeClass('current shown');
                    $('.cities .item[data-id_parent=' + _this.data('id') + ']').addClass('current shown');
                } else {
                    var parent = _this.closest(".parent_block").data('id');
                    var parentChilde = _this.data('id');
                    var idBlock;
                    if (parent == parentChilde) {
                        idBlock = parentChilde;
                    } else {
                        idBlock = parent;
                    }
                    $('.cities .item').removeClass('current shown');
                    $('.cities .item[data-id=' + parentChilde + '][data-id_parent=' + idBlock + ']').addClass('current shown');
                }

            }
        })

        $(document).on('click', '.popup_regions .block.regions.old .item', function () {

            var _this = $(this),
                obl_block = _this.parent('.parent_block');
            _this.siblings().removeClass('current');
            _this.addClass('current');

            var parent = _this.closest(".parent_block").data('id');
            var parentChilde = _this.data('id');
            var idBlock, type;

            $.ajax({
                type: "POST",
                url: "/local/JQactionADMIN/getVilige.php",
                beforeSend: function () {

                    if ($(".block.cities .title .search_loader_li_desc").length < 1) {
                        $(".block.cities .title").append(' <span class="menu_title search_loader_li_desc"><span class="search_loader"> </span></span>');
                    }
                },
                dataType: "json",
                data: {
                    "raiyon": parentChilde,
                    "type": ["SUBREGION", "CITY", "VILLAGE"],
                    "this_url": url_dir_aja
                },
                success: function (msg) {


                    if ($(".search_loader_li_desc").length > 0) {
                        $(".search_loader_li_desc").remove()
                    }


                    $(".block.cities .items_block").empty();
                    if (msg != "false") {

                        var section = {};
                        var subregion = "";
                        var city = "";
                        $.each(msg.LOCATION, function (indexV, valueV) {
                            if (valueV.TYPE_CODE == "SUBREGION") {
                                subregion += '<div class="item dark_link" data-id="' + valueV.ID + '"><span>' + valueV.NAME + '</span></div>';
                            } else {
                                city +=
                                    '<div class="item  shown" data-id="' + valueV.REGION_ID + '" data-id_parent="' + valueV.PARENT_ID + '">' +
                                    '<form  data-id="' + valueV.ID + '" action="https://' + valueV.URL + '" method="POST">' +
                                    '<input type="hidden" name="LOC_ID" value="' + valueV.ID + '">' +
                                    '<input type="hidden" name="BS" value="' + msg.BUSCKET + '">' +
                                    '<input type="hidden" name="DEP_ID" value="' + valueV.ID_DEP + '">' +
                                    '<button type="submit">' + valueV.NAME + '</button>' +
                                    '</form>' +
                                    '</div>';
                            }

                        });

                        $(" .block.regions.subregion .items_block").empty();
                        $(" .block.regions.subregion .items_block").append('<div class="parent_block current shown" data-id="' + parentChilde + '">' + subregion + '</div>');
                        $(".block.cities .items_block").empty();
                        $(".block.cities .items_block").append(city);

                    }
                    return false;
                }
            });
        });


        $('.h-search .wrapper .search_btn').on('click', function () {
            var block = $(this).closest('.wrapper').find('#search');
            if (block.length) {
                block.trigger('focus');
                block.data('ui-autocomplete').search(block.val());
            }
        })

    </script>
</div>
