<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>
<?
use Bitrix\Main\Application,
    Bitrix\Main\SystemException;

$context = Application::getInstance()->getContext();
$request = $context->getRequest();
$get = $request->getQueryList()->toArray();
if (count($get)) {
    $signer = new Bitrix\Main\Security\Sign\Signer;
    $flag = 1;
    try {
        $u = $signer->unsign($get["U"], "change.status");
    } catch (SystemException $exception) {
        echo 'произошла ошибка';
        $flag = 0;
    }
    if ($flag) {
        global $USER;
        $u = base64_decode($u);
        $arGroup = $USER->GetUserGroup($u);
        if (!in_array(103, $arGroup)) {
            $res = \Bitrix\Main\UserGroupTable::add(array(
                "USER_ID" => $u,
                "GROUP_ID" => 103,
            ));
            if ($res->isSuccess()) {
                echo "пользователю присвоен статус 'Мастер'";
            } else {
                echo 'произошла ошибка2';
            }
        } else {
            echo "пользователю присвоен статус 'Мастер'";
        }
    }
} else {
    echo "Ошибка";
}
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>