<?require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<? 
$path = "../bitrix/components/emi/youtube.feed";
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

require_once 'Zend/Loader.php';

$mem_key = "youtube:descriptions";
$video_id = $_REQUEST["video_id"];
//$GLOBALS["memcache"]->flush();
$arrDescriptions = $GLOBALS["memcache"]->get($mem_key);

if (!$arrDescriptions[$video_id])
{
	try{
		Zend_Loader::loadClass('Zend_Gdata_YouTube');
		$yt = new Zend_Gdata_YouTube();
		$entry = $yt->getVideoEntry($video_id);
		$description = $entry->mediaGroup->description;
		$arrDescriptions[$video_id] = $description."";
		$GLOBALS["memcache"]->set($mem_key, $arrDescriptions, false);
	}
	catch (Exception $e){
		die();
	}
}

echo $arrDescriptions[$video_id];

?>