<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>

<?
use Bitrix\Main\Application;
$context = Application::getInstance()->getContext();
$request = $context->getRequest();
$get = $request->getQueryList()->toArray();
if (count($get)) {

    $signer = new Bitrix\Main\Security\Sign\Signer;

    $r = $signer->unsign($get["R"], "activ.key");
    $r = htmlspecialchars(base64_decode($r));

    if (!empty($r) && $r == "Activ") {
        $q = $signer->unsign($get["Q"], "activ.user");
        $q = unserialize(base64_decode($q));

        if ($q["ID"] > 0 && is_int(intval($q["ID"]))) {
            $ID = htmlspecialchars(intval($q["ID"]));
            $EMAIL = htmlspecialchars($q["EMAIL"]);
            $group= array(3,5,4);
            if ($q["TYPE"] == "MASTER") {
                $group[count($group)] = 103;
            } elseif ($q["TYPE"] == "SALON") {
                $group[count($group)] = 121;
            }

            $arGroups = CUser::GetUserGroup($ID);
            CUser::SetUserGroup($ID, $group);

            $user = new CUser;
            $fields = Array(
                "ACTIVE" => "Y",
                "UF_ACTIV_MANAGER" => $EMAIL,
            );
            $user->Update($ID, $fields);
            if (!$user)
                $strError = $user->LAST_ERROR;

            if (empty($strError)) {
                echo "Пользователь успешно активирован";
            }else{
                echo "Ошибка";
            }
        } else {
            echo "Ошибка";
        }
    } else {
        echo "Ошибка";
    }
} else {
    echo "Ошибка";
}
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>