<?
define("STOP_STATISTICS", true);
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/SBERBANK.txt"); //log
CModule::IncludeModule("sale");

$partner_shop_id = $_REQUEST["partnerCode"];
$checkStatusUrl = 'https://securepayments.sberbank.ru/payment/rest/getOrderStatus.do';

//подключаем файл с логинами от площадок
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/sale/ru/payment/sberbankTEST/logins.php");

if(!array_key_exists($partner_shop_id, $logins)){ //если так вышло, что сюда попали с Партнера, у которого не указан логин и пароль для площадки Сбербанка, то возвращаем ошибку
	echo "Не найден такой ID партнёра";
	return;
}

if(array_key_exists("orderNumber", $_REQUEST) && array_key_exists("orderId", $_REQUEST)){
	$orderIdBitrix = $_REQUEST["orderNumber"];//в системе магазина
	$orderId = $_REQUEST["orderId"];//в системе ПС
	$params = array(
		'userName' => $logins[$partner_shop_id]["USERNAME"], 
		'password' => $logins[$partner_shop_id]["PASSWORD"], 
		'orderId' => $orderId 
	);
	
	$result = file_get_contents(
		$checkStatusUrl,
		false,
		stream_context_create(
			array(
				'http' => array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => http_build_query($params)
				)
			)
		)
	);
	
	$resultToArray = json_decode($result, true);
	if(array_key_exists('errorCode', $resultToArray)){
		echo "Ошибка " . $resultToArray['errorMessage'];
		return;
	}
	if(array_key_exists("orderStatus", $resultToArray)){
		if($resultToArray["orderStatus"] == 2){
			CSaleOrder::PayOrder($orderIdBitrix, "Y", True, True, 0, array("PAY_VOUCHER_NUM" => "556"));
		}
	}
	
	
	LocalRedirect("/personal/order/detail");
}