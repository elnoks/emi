<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Школа ногтевого дизайна Екатерины Мирошниченко | О компании E.Mi");
$APPLICATION->SetPageProperty("description", "Научиться создавать удивительные дизайны ногтей можно в школе Екатерины Мирошниченко. Для записи на курс заполните форму на сайте или обратитесь по телефону 8 (906) 427-25-86, e-mail smirnova@emi-school.ru, mbabaev@emi-school.ru.");
$APPLICATION->SetTitle("О компании");
?><div class="bx_page">
	<div class="h1-top">
		<h1>О компании</h1>
	</div>
	<div class="bx_page" style="text-align: center;"><img src="/upload/medialibrary/f7c/f7cb52963256ebe4b40f9af5c03a36d2.jpg" title="56.jpg" alt="56.jpg" height="300" border="0" width="1140"> 
<br>
</div>
<br>
<p>Компания E.Mi - новатор и трендсеттер модного салонного маникюра. Бренд E.Mi - выбор лидеров нейл-индустрии и стильных женщин в 24 странах мира.</p>
<p>С материалами и технологиями E.Mi вы всегда будете в тренде. Успешный салон красоты, востребованный мастер – настоящие fashion-эксперты в мире модного маникюра!</p>
<p>Станьте частью  успешного модного бизнеса, присоединяйтесь к E.Mi International Team!
<br><br>
<a href='/social/open_school.php' type="button" class="btn-pink order-button">Стать партнером</a>
</p>
<h2>E.Mi-маникюр</h2>
<p><img src="/upload/medialibrary/cd4/cd48dcf71294554d8839eab0a34a4986.jpg" title="98.jpg" alt="54.jpg" height="150" border="0" width="1140"> 
<br>
</p>
<p>E.Mi-маникюр – бесконечно модный маникюр, созданный по эскизам нейл-кутюрье  Екатерины Мирошниченко при помощи материалов E.Mi.</p>
<h2>Коллекции E.Mi-маникюра</h2>
<p>Коллекции E.Mi-маникюра вдохновлены высокой модой и блистают на ведущих подиумах мира.
Каждый модный сезон выходит новая коллекция акутуальных образов, прекрасно дополняющих гардероб современной стильной женщины.
<a href='/gallery/#photonail'>Смотрите</a> актуальную коллекцию бесконечно модного E.Mi-маникюра прямо сейчас!
Коллекции E.Mi-маникюра представлены в 15 000 ведущих салонов красоты мира.
<br><br>
<a href='/social/salon.php' type="button" class="btn-pink order-button">Выбрать салон</a>
<br><br>

</p>

<h2>Материалы E.Mi</h2>

<p><img src="/upload/medialibrary/6e3/6e3b48a3e4405516b24de034f7ca5b60.jpg" title="6542.jpg" alt="6542.jpg" height="150" border="0" width="1140"> 
<br>
</p>

<p>Материалы E.Mi имеют экономичный расход и позволяют сократить время на одну процедуру. В модной палитре E.Mi каждый сезон появляются самые актуальные цвета по версии института Pantone и признанных экспертов мировой fashion-индустрии.</p>
<p>EMPASTA, E.MiLac, Charmicon, NAILCRUST, NAILDRESS – настоящие бестселлеры для создания модного маникюра в салонах красоты.
E.Mi Gel System –  уникальная система для укрепления, удлинения и моделирования ногтей БЕЗ ОПИЛА.
E.Mi Care System - профессиональная система для комплексного ухода за кожей рук и стоп.
Выберите необходимый вам продукт прямо сейчас
<br><br>
<a href='/catalog/' type="button" class="btn-pink order-button">Выбрать</a>
</p>
<h2>Школа E.Mi</h2>
<p><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" alt="765.jpg" height="150" border="0" width="1140"> 
<br>
</p>
<p>Обучение в Школе E.Mi по авторским программам Веры и Екатерины Мирошниченко прошли уже более 20 000 мастеров. 
Стать востребованным мастером маникюра, педикюра, нейл-стилистом, хорошо зарабатывать вместе с E.Mi легко!
</p>
<p>Все курсы построены от простого к сложному, с пошаговой отработкой под руководством опытных преподавателей. У вас точно получится!


Выберите свою программу обучения прямо сейчас на нашем сайте!
<br><br>
<a href='/courses/' type="button" class="btn-pink order-button">Выбрать</a>
</p>

<p><span style="font-family: Verdana, Arial, Helvetica, sans-serif;"><font size="2">(Лицензия № 147146 от 12.12.2007)</font></span></p>
</div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>