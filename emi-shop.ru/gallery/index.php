<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(GetMessage("GALLERY_TITLE"));
$APPLICATION->SetPageProperty("TITLE", GetMessage("GALLERY_PAGE_META_TITLE"));
ini_set('error_reporting', E_ERROR );

?><h2 class="margin-top-0"><?=GetMessage("GALLERY_TITLE_VIDIO")?></h2>
<div class="video-container shadow">
	 <?$APPLICATION->IncludeComponent(
	"prgrant:video.gallery",
	"gallery_new",
	Array(
		"PLAYLIST_SKIN" => "right-thumb",
		"VG_AUTOPLAY" => "N",
		"VG_DISABLE_PANEL_TIMEOUT" => "700",
		"VG_PLAYER_ID" => "1",
		"VG_PLAYLIST_SKIN" => "right-thumb",
		"VG_VIDEO_ID" => array(),
		"VG_VIDEO_PLAYLIST" => array(0=>YOUTUBE_GALLERY_SECOND_PLAYLIST,),
		"VG_VIDEO_VALUE" => "",
		"VG_YOUTUBE_KEY" => "AIzaSyD7ho3783ibUiT3yMBZhLrJYgbjTmXfVt4"
	)
);?>
</div>
<h2><?= GetMessage("GALLERY_TITLE_MASTER_CLASS")?></h2>
<div class="video-container shadow">
	 <?$APPLICATION->IncludeComponent(
	"prgrant:video.gallery",
	"gallery_new",
	Array(
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"PLAYLIST_SKIN" => "left-thumb",
		"VG_AUTOPLAY" => "N",
		"VG_DISABLE_PANEL_TIMEOUT" => "700",
		"VG_PLAYER_ID" => "2",
		"VG_PLAYLIST_SKIN" => "left-thumb",
		"VG_VIDEO_ID" => array(),
		"VG_VIDEO_PLAYLIST" => array(0=>YOUTUBE_VIDEO,),
		"VG_VIDEO_VALUE" => "",
		"VG_YOUTUBE_KEY" => "AIzaSyD7ho3783ibUiT3yMBZhLrJYgbjTmXfVt4"
	)
);?>
</div>
 <?

if(file_exists('..'.CURLANG_INCLUDE_PATH.'flipbook')){
    $dir='..'.CURLANG_INCLUDE_PATH.'flipbook';
    $files=array_slice(scandir($dir),2);


    sort($files,SORT_NATURAL);
    foreach ($files as $key=>$file) {
        $files[$key]=$dir.'/'.$file;
    }
    //print_r($files);
    $filesj=json_encode($files);
?>
<div>
	<h2 id="flipbook"><?= GetMessage("GALLERY_TITLE_CATALOG_PUBLICK")?></h2>
	 <?
    $APPLICATION->SetAdditionalCSS("/bitrix/templates/.default/plugins/dflip/css/dflip.css");
    $APPLICATION->SetAdditionalCSS("/bitrix/templates/.default/plugins/dflip/css/themify-icons.css");
    ?> <? /*
    <div class="_df_thumb" id="_df_thumb_flip"
         tags="2018"
         source="<?=GetMessage("MAIN_PAGE_4BLOCKS_FLIPBOOK_SRC");?>"
         thumb="<?=GetMessage("MAIN_PAGE_4BLOCKS_FLIPBOOK_IMG");?>" >
        <?=GetMessage("MAIN_PAGE_4BLOCKS_FLIPBOOK");?>
    </div>
    */ ?>
	<div id="flipbookContainer" class="_df_book grey_block">
	</div>
	 <script src="/bitrix/templates/.default/plugins/dflip/js/dflip.min.js" type="text/javascript"></script> <script>


        var dFlipLocation = "/bitrix/templates/.default/plugins/dflip/";
        $(document).ready(function () {
            var source = '<?=GetMessage("MAIN_PAGE_4BLOCKS_FLIPBOOK_SRC");?>';
            var imgs = eval('<?=$filesj?>');
            //console.log(imgs);
            var options = {
                webgl: true,
                //webglShadow: true,
                soundEnable: false,
                height: 700,
                <? if (LANGUAGE_ID=="ru"){?>
                // TRANSLATION ru
                text: {
                    toggleSound: "Включить/Выключить Звуки",
                    toggleThumbnails: "Включить Эскизы",
                    toggleOutline: "Включить Контур/Закладка",
                    previousPage: "Предыдущая страница",
                    nextPage: "Следующая страница",
                    toggleFullscreen: "Полноэкранный режим",
                    zoomIn: "Приблизить",
                    zoomOut: "Отдалить",
                    toggleHelp: "Включить Помощь",

                    singlePageMode: "Одностраничный Режим",
                    doublePageMode: "Двустраничный Режим",
                    downloadPDFFile: "Скачать PDF файл",
                    gotoFirstPage: "Перейти на первую страницу",
                    gotoLastPage: "Перейти на последнюю страницу",
                    play: "Запуск автовоспроизведения",
                    pause: "Пауза автовоспроизведения",

                    share: "Поделиться"
                },
                <?}?>
                // allControls: "altPrev,pageNumber,altNext,play,outline,thumbnail,zoomIn,zoomOut,fullScreen,share,download,more,pageMode,startPage,endPage,sound",
                // moreControls: "download,pageMode,startPage,endPage,sound",
                // hideControls: "",
                scrollWheel: false,
                pdfRenderQuality: 0.90,
                backgroundColor: "#fff",
                duration: 600
            };

            var flipBook = $("#flipbookContainer").flipBook(imgs, options);

        });

    </script>
</div>
 <? } ?> <!-- publications-->
<h2><?= GetMessage("GALLERY_TITLE_PUBLICK")?></h2>
 <?
$arrFilterPublications = array("SECTION_CODE" => "publications");
$arrFilterPhoto = array("SECTION_CODE" => "photo");
?> <?$APPLICATION->IncludeComponent(
	"emi:news.publications",
	".default",
	Array(
		"ACTIVE_DATE_FORMAT" => "",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "3600000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"DETAIL_PICTURE",1=>"",),
		"FILTER_NAME" => "arrFilterPublications",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => GALLERY_IBLOCK_ID,
		"IBLOCK_TYPE" => "news",
		"IMAGE_POSITION" => "left",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "6",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_TITLE" => "Галерея",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"PROPERTY_PUB_PAGES",2=>"",),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_DETAIL_LINK" => "Y",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "",
		"STRICT_SECTION_CHECK" => "N",
		"S_ASK_QUESTION" => "",
		"S_ORDER_PROJECT" => "",
		"S_ORDER_SERVICE" => "",
		"TITLE" => GetMessage("GALLERY_PAGE_BLOCKS_PUBLIC"),
		"T_CHARACTERISTICS" => "",
		"T_DOCS" => "",
		"T_GALLERY" => "",
		"T_GOODS" => "",
		"T_PROJECTS" => "",
		"T_REVIEWS" => "",
		"T_SERVICES" => "",
		"T_STAFF" => "",
		"USE_SHARE" => "N"
	)
);?> <!-- /.publications --> <?
//ajax photo gallery
$APPLICATION->IncludeFile("/include/ajax/photo_gallery.php");?>
    <h2>Instagram <a href="https://www.instagram.com/emi_official_world/">@emi_official_world</a></h2>
<?$APPLICATION->IncludeComponent(
	"prgrant:instagram.gallery.emimanicure",
	"",
	Array(
		"IS_JQUERY" => "N",
		"PHP_CACHE_TIME" => "3600",
		"USERNAME" => "emi_official_world"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>