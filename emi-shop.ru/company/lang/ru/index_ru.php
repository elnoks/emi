     <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-5">
                <img style="width: 100%" class="img-responsive shadow" src="<?=SITE_TEMPLATE_PATH.'/images/';?>main_photo.jpg">
            </div>
            <div class="col-lg-8 col-md-7 col-sm-7">
                <h2>E.Mi</h2>

                <p>E.Mi – международный бренд модного салонного маникюра. E.Mi выбирают звезды, лидеры нейл-индустрии и стильные женщины в 28 странах мира.</p>

                <p>E.Mi любят за европейское качество, постоянные новации в создании материалов, уникальный подход  к дизайну ногтей и комплексные бизнес-решения для салонов красоты.</p>

                <h2 class="heading">Глобальный бренд</h2>

                <p>E.Mi – это глобальный бренд. Для создания продукции используется сырье от ведущих мировых поставщиков из Европы, Южной Кореи и США.</p>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 col-sm-12">
                <h2 class="heading">Звезды выбирают E.Mi <br>
                </h2>
                <div>
                <span class="subheading">
                    <div>
                        <span style="font-size: 18px;">E.Mi-маникюр выбирают звезды Голливуда – Нуми Рапас, Даутцен Крус, Рианна, и российские иконы стиля – Глюкоза, Полина Максимова.</span>
                    </div>
                </span>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-md-2 col-sm-2">
                        <img src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_4.jpg" class="img-responsive shadow">
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_5.jpg" class="img-responsive shadow">
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_6.jpg" class="img-responsive shadow">
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_7.jpg" class="img-responsive shadow">
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_8.jpg" class="img-responsive shadow">
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_9.jpg" class="img-responsive shadow">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <h2 class="heading">E.Mi-маникюр на мировых Неделях моды <br>
                </h2>
                <div>
                    <span class="subheading">
                        <div>
                            <span style="font-size: 18px;">E.Mi-маникюр блистает на мировых подиумах в Париже, Москве, Лондоне и Сеуле.<br></span>
                        </div>
                    </span>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <img class="img-responsive shadow" src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_11.jpg">
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <img class="img-responsive shadow" src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_12.jpg">
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <img class="img-responsive shadow" src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_10.jpg">
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <img class="img-responsive shadow" src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_13.jpg">
                    </div>
                </div>
            </div>
        </div>

        <br><br>

        <div class="row">
            <div class="col-lg-8 col-md-7 col-sm-7">
                <h2>Екатерина Мирошниченко</h2>

                <p>Основатель бренда E.Mi и нейл-кутюрье. Создатель коллекций модного E.Mi-маникюра. Чемпионка мира по дизайну ногтей, автор и разработчик уникальных курсов и технологий модного маникюра Fresh Boom и Aqvin (2018), Нейл-Ювелир (2016), TEXTONE&amp;Combiture (2014), «Объемный винтаж» (2013), «Бархатный песок и жидкие камни» (2012), «Эффект кракелюра и этнические принты» (2010), «Имитация кожи рептилий» (2009), «Золотое литье» (2008) и уникальных материалов для их выполнения: Charmicon, NailDress, Nailcrust, Gemty, EMPASTA «Черный тюльпан», «Бархатный песок».</p>
            </div>
            <div class="col-lg-4 col-md-5 col-sm-5">
                <img class="img-responsive shadow" src="<?=SITE_TEMPLATE_PATH.'/images/';?>Ekaterina.jpg">
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-5">
                <img class="img-responsive shadow" src="<?=SITE_TEMPLATE_PATH.'/images/';?>Vera.jpg">
            </div>
            <div class="col-lg-8 col-md-7 col-sm-7">
                <h2>Вера Мирошниченко</h2>

                <p>15 лет экспертизы в салонном бизнесе. Сооснователь бренда E.Mi, главный технолог
                    E.Mi Gel System и бизнес-тренер. Автор программ E.Mi Business School для руководителей салонов
                    красоты: "Как привлечь и удержать клиентов в салоне красоты?", "Как привлечь и удержать персонал
                    в салоне красоты?", "Эффективный маркетинг салона красоты", "Планирование и бюджетирование в
                    салоне красоты".</p>


                <h2 class="heading">Глобальный бренд</h2>

                <p>E.Mi – это глобальный бренд. Для создания продукции используется сырье от ведущих мировых поставщиков из Европы, Южной Кореи и США.</p>
            </div>
        </div>
