     <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-5">
                <img style="width: 100%" class="img-responsive shadow" src="<?=SITE_TEMPLATE_PATH.'/images/';?>main_photo.jpg">
            </div>
            <div class="col-lg-8 col-md-7 col-sm-7">
                <h2>E.Mi</h2>

                <p>E.Mi – nternational brand of fashion salon manicure. E.Mi choose stars, nail industry leaders and stylish women in 28 countries.</p>

                <p>E.Mi love for European quality, constant innovations in the creation of materials, a unique approach to the design of nails and integrated business solutions for beauty salons.</p>

                <h2 class="heading">Глобальный бренд</h2>

                <p>E.Mi – This is a global brand. To create products using raw materials from the world's leading suppliers from Europe, South Korea and the United States.</p>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 col-sm-12">
                <h2 class="heading">Stars choose E.Mi <br>
                </h2>
                <div>
                <span class="subheading">
                    <div>
                        <span style="font-size: 18px;">E.Mi-Manicure is chosen by Hollywood stars - Numi Rapas, Doutzen Cruz, Rihanna, and Russian style icons - Glukoza, Polina Maximova.</span>
                    </div>
                </span>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-md-2 col-sm-2">
                        <img src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_4.jpg" class="img-responsive shadow">
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_5.jpg" class="img-responsive shadow">
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_6.jpg" class="img-responsive shadow">
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_7.jpg" class="img-responsive shadow">
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_8.jpg" class="img-responsive shadow">
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_9.jpg" class="img-responsive shadow">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <h2 class="heading">E.Mi-manicure at world fashion weeks<br>
                </h2>
                <div>
                    <span class="subheading">
                        <div>
                            <span style="font-size: 18px;">E.Mi-manicure shines on the world's catwalks in Paris, Moscow, London and Seoul.<br></span>
                        </div>
                    </span>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <img class="img-responsive shadow" src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_11.jpg">
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <img class="img-responsive shadow" src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_12.jpg">
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <img class="img-responsive shadow" src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_10.jpg">
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <img class="img-responsive shadow" src="<?=SITE_TEMPLATE_PATH.'/images/';?>photo_13.jpg">
                    </div>
                </div>
            </div>
        </div>

        <br><br>

        <div class="row">
            <div class="col-lg-8 col-md-7 col-sm-7">
                <h2>Ekaterina Miroshnichenko</h2>

                <p>Founder of the brand E.Mi and neyl-couturier. The creator of the collections of fashionable E.Mi-manicure. World champion in nail design, author and developer of unique courses and technologies of fashionable manicure Fresh Boom and Aqvin (2018), Neil Jeweler (2016), TEXTONE & amp; Combiture (2014), “Volumetric vintage” (2013), “Velvet sand and liquid stones ”(2012),“ Craquelure effect and ethnic prints ”(2010),“ Reptile skin imitation ”(2009),“ Gold casting ”(2008) and unique materials for their performance: Charmicon, NailDress, Nailcrust, Gemty, EMPASTA“ Black Tulip, Velvet Sand.</p>
            </div>
            <div class="col-lg-4 col-md-5 col-sm-5">
                <img class="img-responsive shadow" src="<?=SITE_TEMPLATE_PATH.'/images/';?>Ekaterina.jpg">
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-5">
                <img class="img-responsive shadow" src="<?=SITE_TEMPLATE_PATH.'/images/';?>Vera.jpg">
            </div>
            <div class="col-lg-8 col-md-7 col-sm-7">
                <h2>Vera Miroshnichenko</h2>

                <p>15 years of expertise in the salon business. Co-founder of the brand E.Mi, chief technologist
                                         E.Mi Gel System and business coach. Author of E.Mi Business School programs for salon managers
                                         beauty: "How to attract and retain customers in a beauty salon?", "How to attract and retain staff
                                         in a beauty salon? "," Effective marketing of a beauty salon "," Planning and budgeting in
                                         beauty salon. "</p>


                <h2 class="heading">Global Brand</h2>

                <p>E.Mi – This is a global brand. To create products using raw materials from the world's leading suppliers from Europe, South Korea and the United States.</p>
            </div>
        </div>
