<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оформление заказа");
?>

<?
BasketClon::setSession();
?>

<?$APPLICATION->IncludeComponent(
	"BKV:sale.order.ajax", 
	"asproOrder", 
	array(
		"PAY_FROM_ACCOUNT" => "N",
		"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
		"COUNT_DELIVERY_TAX" => "Y",
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"ALLOW_AUTO_REGISTER" => "N",
		"SEND_NEW_USER_NOTIFY" => "Y",
		"DELIVERY_NO_AJAX" => "Y",
		"DELIVERY_NO_SESSION" => "Y",
		"TEMPLATE_LOCATION" => "popup",
		"DELIVERY_TO_PAYSYSTEM" => "d2p",
		"USE_PREPAYMENT" => "N",
		"PROP_1" => "",
		"PROP_3" => "",
		"PROP_2" => "",
		"PROP_4" => "",
		"PROP_5" => "",
		"SHOW_STORES_IMAGES" => "Y",
		"PATH_TO_BASKET" => SITE_DIR."basket/",
		"PATH_TO_PERSONAL" => SITE_DIR."personal/",
		"PATH_TO_PAYMENT" => SITE_DIR."order/payment/",
		"PATH_TO_AUTH" => SITE_DIR."auth/",
		"SET_TITLE" => "Y",
		"PRODUCT_COLUMNS" => "",
		"DISABLE_BASKET_REDIRECT" => "N",
		"DISPLAY_IMG_WIDTH" => "90",
		"DISPLAY_IMG_HEIGHT" => "90",
		"COMPONENT_TEMPLATE" => "asproOrder",
		"ALLOW_NEW_PROFILE" => "N",
		"SHOW_PAYMENT_SERVICES_NAMES" => "Y",
		"COMPATIBLE_MODE" => "Y",
		"BASKET_IMAGES_SCALING" => "adaptive",
		"ALLOW_USER_PROFILES" => "N",
		"TEMPLATE_THEME" => "blue",
		"SHOW_TOTAL_ORDER_BUTTON" => "N",
		"SHOW_PAY_SYSTEM_LIST_NAMES" => "Y",
		"SHOW_PAY_SYSTEM_INFO_NAME" => "Y",
		"SHOW_DELIVERY_LIST_NAMES" => "Y",
		"SHOW_DELIVERY_INFO_NAME" => "Y",
		"SHOW_DELIVERY_PARENT_NAMES" => "Y",
		"BASKET_POSITION" => "after",
		"SHOW_BASKET_HEADERS" => "Y",
		"DELIVERY_FADE_EXTRA_SERVICES" => "Y",
		"SHOW_COUPONS_BASKET" => "Y",
		"SHOW_COUPONS_DELIVERY" => "Y",
		"SHOW_COUPONS_PAY_SYSTEM" => "Y",
		"SHOW_NEAREST_PICKUP" => "Y",
		"DELIVERIES_PER_PAGE" => "8",
		"PAY_SYSTEMS_PER_PAGE" => "8",
		"PICKUPS_PER_PAGE" => "5",
		"SHOW_MAP_IN_PROPS" => "N",
		"SHOW_MAP_FOR_DELIVERIES" => "",
		"PROPS_FADE_LIST_1" => array(
			0 => "1",
			1 => "2",
			2 => "3",
			3 => "4",
			4 => "6",
			5 => "7",
		),
		"PROPS_FADE_LIST_2" => "",
		"PRODUCT_COLUMNS_VISIBLE" => array(
			0 => "PREVIEW_PICTURE",
			1 => "DISCOUNT_PRICE_PERCENT_FORMATED",
			2 => "PRICE_FORMATED",
		),
		"ADDITIONAL_PICT_PROP_13" => "-",
		"ADDITIONAL_PICT_PROP_14" => "-",
		"PRODUCT_COLUMNS_HIDDEN" => array(
		),
		"USE_YM_GOALS" => "N",
		"USE_CUSTOM_MAIN_MESSAGES" => "N",
		"USE_CUSTOM_ADDITIONAL_MESSAGES" => "N",
		"USE_CUSTOM_ERROR_MESSAGES" => "N",
		"SHOW_ORDER_BUTTON" => "always",
		"SKIP_USELESS_BLOCK" => "Y",
		"SERVICES_IMAGES_SCALING" => "standard",
		"COMPOSITE_FRAME_MODE" => "N",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"ALLOW_APPEND_ORDER" => "Y",
		"SHOW_NOT_CALCULATED_DELIVERIES" => "L",
		"SPOT_LOCATION_BY_GEOIP" => "N",
		"SHOW_VAT_PRICE" => "Y",
		"USE_PRELOAD" => "Y",
		"SHOW_PICKUP_MAP" => "Y",
		"PICKUP_MAP_TYPE" => "yandex",
		"PROPS_FADE_LIST_51" => array(
		),
		"PROPS_FADE_LIST_12" => array(
		),
		"PROPS_FADE_LIST_14" => array(
		),
		"PROPS_FADE_LIST_55" => array(
		),
		"PROPS_FADE_LIST_8" => array(
		),
		"PROPS_FADE_LIST_27" => array(
		),
		"PROPS_FADE_LIST_25" => array(
		),
		"PROPS_FADE_LIST_35" => array(
		),
		"PROPS_FADE_LIST_18" => array(
		),
		"PROPS_FADE_LIST_16" => array(
		),
		"PROPS_FADE_LIST_20" => array(
		),
		"PROPS_FADE_LIST_49" => array(
		),
		"PROPS_FADE_LIST_52" => array(
		),
		"PROPS_FADE_LIST_13" => array(
		),
		"PROPS_FADE_LIST_15" => array(
		),
		"PROPS_FADE_LIST_5" => array(
		),
		"PROPS_FADE_LIST_9" => array(
		),
		"PROPS_FADE_LIST_28" => array(
		),
		"PROPS_FADE_LIST_26" => array(
		),
		"PROPS_FADE_LIST_36" => array(
		),
		"PROPS_FADE_LIST_58" => array(
		),
		"PROPS_FADE_LIST_19" => array(
		),
		"PROPS_FADE_LIST_17" => array(
		),
		"PROPS_FADE_LIST_21" => array(
		),
		"PROPS_FADE_LIST_50" => array(
		),
		"PROPS_FADE_LIST_61" => array(
		),
		"PROPS_FADE_LIST_33" => array(
		),
		"PROPS_FADE_LIST_34" => array(
		),
		"PROPS_FADE_LIST_37" => array(
		),
		"PROPS_FADE_LIST_38" => array(
		),
		"PROPS_FADE_LIST_39" => array(
		),
		"PROPS_FADE_LIST_40" => array(
		),
		"PROPS_FADE_LIST_41" => array(
		),
		"PROPS_FADE_LIST_42" => array(
		),
		"PROPS_FADE_LIST_43" => array(
		),
		"PROPS_FADE_LIST_44" => array(
		),
		"PROPS_FADE_LIST_45" => array(
		),
		"PROPS_FADE_LIST_46" => array(
		),
		"PROPS_FADE_LIST_47" => array(
		),
		"PROPS_FADE_LIST_48" => array(
		),
		"PROPS_FADE_LIST_6" => array(
		),
		"PROPS_FADE_LIST_4" => array(
		),
		"PROPS_FADE_LIST_29" => array(
		),
		"PROPS_FADE_LIST_31" => array(
		),
		"PROPS_FADE_LIST_57" => array(
		),
		"PROPS_FADE_LIST_7" => array(
		),
		"PROPS_FADE_LIST_30" => array(
		),
		"PROPS_FADE_LIST_32" => array(
		),
		"PROPS_FADE_LIST_3" => array(
		),
		"PROPS_FADE_LIST_62" => array(
		),
		"PROPS_FADE_LIST_63" => "",
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N",
		"ACTION_VARIABLE" => "soa-action",
		"USE_PHONE_NORMALIZATION" => "Y",
		"ADDITIONAL_PICT_PROP_18" => "-",
		"ADDITIONAL_PICT_PROP_30" => "-",
		"ADDITIONAL_PICT_PROP_36" => "-",
		"ADDITIONAL_PICT_PROP_44" => "-",
		"ADDITIONAL_PICT_PROP_48" => "-",
		"ADDITIONAL_PICT_PROP_51" => "-",
		"ADDITIONAL_PICT_PROP_54" => "-",
		"ADDITIONAL_PICT_PROP_56" => "-",
		"ADDITIONAL_PICT_PROP_81" => "-",
		"ADDITIONAL_PICT_PROP_82" => "-",
		"ADDITIONAL_PICT_PROP_84" => "-",
		"ADDITIONAL_PICT_PROP_85" => "-",
		"ADDITIONAL_PICT_PROP_88" => "-",
		"ADDITIONAL_PICT_PROP_89" => "-",
		"ADDITIONAL_PICT_PROP_90" => "-",
		"ADDITIONAL_PICT_PROP_91" => "-",
		"ADDITIONAL_PICT_PROP_92" => "-",
		"ADDITIONAL_PICT_PROP_96" => "-",
		"ADDITIONAL_PICT_PROP_97" => "-",
		"ADDITIONAL_PICT_PROP_98" => "-",
		"ADDITIONAL_PICT_PROP_99" => "-",
		"ADDITIONAL_PICT_PROP_103" => "-",
		"ADDITIONAL_PICT_PROP_104" => "-",
		"ADDITIONAL_PICT_PROP_105" => "-",
		"ADDITIONAL_PICT_PROP_106" => "-",
		"ADDITIONAL_PICT_PROP_108" => "-",
		"ADDITIONAL_PICT_PROP_109" => "-",
		"ADDITIONAL_PICT_PROP_111" => "-",
		"ADDITIONAL_PICT_PROP_112" => "-",
		"ADDITIONAL_PICT_PROP_114" => "-",
		"ADDITIONAL_PICT_PROP_116" => "-",
		"ADDITIONAL_PICT_PROP_123" => "-",
		"ADDITIONAL_PICT_PROP_125" => "-",
		"ADDITIONAL_PICT_PROP_129" => "-",
		"ADDITIONAL_PICT_PROP_132" => "-",
		"ADDITIONAL_PICT_PROP_134" => "-",
		"ADDITIONAL_PICT_PROP_135" => "-",
		"ADDITIONAL_PICT_PROP_139" => "-",
		"ADDITIONAL_PICT_PROP_142" => "-",
		"ADDITIONAL_PICT_PROP_144" => "-",
		"ADDITIONAL_PICT_PROP_147" => "-",
		"ADDITIONAL_PICT_PROP_149" => "-",
		"ADDITIONAL_PICT_PROP_152" => "-",
		"ADDITIONAL_PICT_PROP_153" => "-",
		"ADDITIONAL_PICT_PROP_154" => "-",
		"ADDITIONAL_PICT_PROP_156" => "-",
		"ADDITIONAL_PICT_PROP_158" => "-",
		"ADDITIONAL_PICT_PROP_160" => "-",
		"ADDITIONAL_PICT_PROP_169" => "-",
		"ADDITIONAL_PICT_PROP_170" => "-",
		"ADDITIONAL_PICT_PROP_171" => "-",
		"ADDITIONAL_PICT_PROP_176" => "-",
		"ADDITIONAL_PICT_PROP_177" => "-",
		"ADDITIONAL_PICT_PROP_178" => "-",
		"ADDITIONAL_PICT_PROP_179" => "-",
		"ADDITIONAL_PICT_PROP_181" => "-",
		"ADDITIONAL_PICT_PROP_182" => "-",
		"ADDITIONAL_PICT_PROP_184" => "-",
		"ADDITIONAL_PICT_PROP_186" => "-",
		"ADDITIONAL_PICT_PROP_188" => "-",
		"ADDITIONAL_PICT_PROP_192" => "-",
		"ADDITIONAL_PICT_PROP_193" => "-",
		"ADDITIONAL_PICT_PROP_194" => "-",
		"ADDITIONAL_PICT_PROP_200" => "-",
		"ADDITIONAL_PICT_PROP_201" => "-",
		"ADDITIONAL_PICT_PROP_202" => "-",
		"ADDITIONAL_PICT_PROP_203" => "-",
		"ADDITIONAL_PICT_PROP_206" => "-",
		"ADDITIONAL_PICT_PROP_207" => "-",
		"ADDITIONAL_PICT_PROP_208" => "-",
		"ADDITIONAL_PICT_PROP_212" => "-",
		"ADDITIONAL_PICT_PROP_256" => "-",
		"ADDITIONAL_PICT_PROP_259" => "-",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"PROPS_FADE_LIST_66" => array(
		),
		"ADDITIONAL_PICT_PROP_248" => "-",
		"ADDITIONAL_PICT_PROP_251" => "-",
		"PROPS_FADE_LIST_68" => array(
		),
		"PROPS_FADE_LIST_69" => "",
		"ADDITIONAL_PICT_PROP_218" => "-",
		"ADDITIONAL_PICT_PROP_219" => "-",
		"ADDITIONAL_PICT_PROP_221" => "-",
		"ADDITIONAL_PICT_PROP_222" => "-",
		"ADDITIONAL_PICT_PROP_225" => "-",
		"ADDITIONAL_PICT_PROP_226" => "-",
		"ADDITIONAL_PICT_PROP_229" => "-",
		"ADDITIONAL_PICT_PROP_230" => "-",
		"ADDITIONAL_PICT_PROP_231" => "-",
		"ADDITIONAL_PICT_PROP_272" => "-"
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>