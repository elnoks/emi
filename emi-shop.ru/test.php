<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

use Bitrix\Main\Application,
    Bitrix\Main\Context,
    Bitrix\Main\Loader,
	Bitrix\Main\Mail\Event,
	Bitrix\Main\Service\GeoIp,
	Bitrix\Sale,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Diag\Debug;

//Debug::writeToFile($item);






// Добавление аналогов и сопутствующих товаров по имени
/*
$related = [

];

$analogues = [

];

$items = [
    'E.MiLac Белый лотос №001, 6 мл.',
];

foreach ($iterator as $iteration){
    $filter = [
        'select' => ["ID"],
        'filter' => ['IBLOCK_ID' => 176, 'NAME' => ${$iteration}]
    ];
    ${$iteration."Query"} = \Bitrix\Iblock\ElementTable::getList($filter)-> fetchAll();
    foreach (${$iteration."Query"} as $item) ${$iteration."Id"}[] = $item["ID"];
}
op($itemsId);
foreach ($itemsId as $item) {
//    if(!empty($relatedId)) CIBlockElement::SetPropertyValuesEx($item, $iblock, ['RELATED' => $relatedId]);
    if(!empty($analoguesId)) CIBlockElement::SetPropertyValuesEx($item, $iblock, ['ANALOGUES' => $analoguesId]);
    CIBlockElement::SetPropertyValuesEx($item, $iblock, ['SPECIAL_OFFER' => 6090]);
    CIBlockElement::SetPropertyValuesEx($item, $iblock, ['rating' => 3.3]);
    CIBlockElement::SetPropertyValuesEx($item, $iblock, ['vote_sum' => 5]);
    CIBlockElement::SetPropertyValuesEx($item, $iblock, ['vote_count' => 1]);
}
*/

/*
// получение типа цены по SITE_ID
$resultCurrency = \Bitrix\Sale\Internals\SiteCurrencyTable::getSiteCurrency(SITE_ID);
$arSelect = array('ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY');
$arFilter = array(
    '=PRODUCT_ID' => 257931,
    '=CURRENCY' => $resultCurrency,
);

if(empty($priceList))
{
    $iterator = \Bitrix\Catalog\PriceTable::getList(array(
        'select' => $arSelect,
        'filter' => $arFilter
    ))->fetchAll();
}
*/


/*
// Список сайтов
$filter = ["select" => ["LID", "NAME"]];
$rsSites = Bitrix\Main\SiteTable::getList($filter);
while($arSite = $rsSites->Fetch())
    $result[$arSite["LID"]] = "[".$arSite["LID"]."] ".$arSite["NAME"];
*/
//


/*
$arResult = \Bitrix\Iblock\ElementTable::getList(
    array(
        'select' => array("ID", 'PRICE' => 'CATALOG'),
        'filter' => array('IBLOCK_ID' => 176,"ID" => 168941),
        'runtime' =>
            array(
                new \Bitrix\Main\Entity\ReferenceField(
                    'CATALOG',
                    \Bitrix\Catalog\PriceTable::getEntity(),
                    [
                        '=this.ID' => 'ref.PRODUCT_ID',
                    ]
                ),
            ),
    ))->fetchAll();

foreach ($arResult as $item) {
    $new[$item["ID"]] = $item;
}
foreach ($new as $item) {
    //$productID = CCatalogProduct::add(array("ID" => $item["ID"]));
    op($item[PRICEID]);
//    $boolResult = CPrice::DeleteByProduct($item["ID"], $item["PRICEID"]);

    $arFields = Array(
        "PRODUCT_ID" => $item["ID"],
        "CATALOG_GROUP_ID" => 29,
        "PRICE" => "",
        "CURRENCY" => "KZT",
    );
    CPrice::Add($arFields);
}


*/
/*
Loader::includeModule("sale");


$res = CSaleOrderPropsValue::GetList(
    $arOrder = array("*"),
    $arFilter = array("ORDER_ID" => 34191, "CODE" => ["CITY", "STREET","HOUSE", "FLAT", "ADDRESS"]),
    $arGroupBy = false,
    $arNavStartParams = false,
    $arSelectFields = array()
);

while ($arVals = $res->Fetch()){
    op($arVals);
    if($res->nSelectedCount == 1) {
        $fullAddress = $arVals["VALUE"];
    }else{
        $fullAddress .= Loc::getMessage("EVENT_" . $arVals["CODE"]) . $arVals["VALUE"];
    }
}
op($fullAddress);
*/
/*
// получить платежные системы
$rsPaySystem = \Bitrix\Sale\Internals\PaySystemActionTable::getList(['filter' => array('ACTIVE'=>'Y')]);
$arPaySystemArr = [];
while($arPaySystem = $rsPaySystem->fetch())
{
	if ($arPaySystem["PAY_SYSTEM_ID"] != Bitrix\Sale\PaySystem\Manager::getInnerPaySystemId())
    	$arPaySystemArr[$arPaySystem["ID"]] = "[".$arPaySystem["PAY_SYSTEM_ID"]."] - ".$arPaySystem["PSA_NAME"];
}

$mSelect = "<select multiple name size='10'>";
foreach ($arPaySystemArr as $key => $item) {
    $mSelect .= "<option value='" . $key . "'>" . $item . "</option>";
}

$mSelect .= "</select>";

echo $mSelect;
*/

/*
// Изменение количества товара по краснодарскому складу.
$IBLOCK_ID = 176;
$STORE_ID = 65; // Краснодарский склад
$KOL = 9999999;
$arFilter = Array(
    "IBLOCK_ID"=>$IBLOCK_ID,
    "ACTIVE"=>"Y"
);
$ar_fields = \Bitrix\Iblock\ElementTable::getList(
    array(
        'select' => array("ID"),
        'filter' => array('IBLOCK_ID' => 176),
    )) -> fetchAll();

foreach ($ar_fields as $item) {

    $arFields = Array(
        "PRODUCT_ID" => $item["ID"],
        "STORE_ID" => $STORE_ID,
        "AMOUNT" => $KOL,
    );
    $result = CCatalogStoreProduct::Add($arFields);
    if (!$result) {
        $result = CCatalogStoreProduct::UpdateFromForm($arFields);
    }
    if ($result == 1) $result = $ar_fields["ID"] . " - изменено кол-во на: " . $KOL . " | ID склада: " . $STORE_ID . ", ";
    echo $result . "<br>";
}
*/

/*
//Установка цен
$arResult = \Bitrix\Iblock\ElementTable::getList(
    array(
        'select' => array("ID", 'PRICE' => 'CATALOG.PRICE' ),
        'filter' => array('IBLOCK_ID' => 176),
        'runtime' =>
            array(
                new \Bitrix\Main\Entity\ReferenceField(
                    'CATALOG',
                    \Bitrix\Catalog\PriceTable::getEntity(),
                    [
                        '=this.ID' => 'ref.PRODUCT_ID',
                    ]
                ),
            ),
    )) -> fetchAll();

foreach ($arResult as $item){
	$new[$item["ID"]] = $item;
}
foreach ($new as $item) {
    $productID = CCatalogProduct::add(array("ID" => $item["ID"]));
    $arFields = Array(
        "PRODUCT_ID" => $item["ID"],
        "CATALOG_GROUP_ID" => 29,
        "PRICE" => $item["PRICE"] * 6,
        "CURRENCY" => "KZT",
    );
    CPrice::Add($arFields);
}
*/

/*
//	op($item);

$item = Bitrix\Sale\Location\LocationTable::getList(
    array(
        'filter' => array('=NAME.LANGUAGE_ID' => LANGUAGE_ID),
        'select' => array(
        	'ID',
			'CODE',
			'PARENT_ID',
			'TYPE_ID',
			'COUNTRY_ID',
			'REGION_ID',
			'CITY_ID',
			'NAME_LANG' => 'NAME.NAME',
		),
        "order" => array("NAME_LANG" => "asc")
    )
)->fetch();

op($item);
*/
/*
$basket = \Bitrix\Sale\Basket::loadItemsForFUser(
    \Bitrix\Sale\Fuser::getId(),
    \Bitrix\Main\Context::getCurrent()->getSite()
);

$item = $basket->getExistsItem('catalog', 257967);


op($item);
*/
/*
$filter = array(
			'select' => array("ID"),
			'filter' => array(
							// "DATE_INSERT" =>  date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time()),
							// 'USER_ID' => 37,
							'LID' => "s1"
						),
		);
op($filter);	
*/
// $order = Sale\Order::load(33607);
// op($order);	

// $orderList = Sale\Order::getList($filter);
// $orderList = Sale\Order::getList($filter);

// file_put_contents($_SERVER['DOCUMENT_ROOT']."/upload/statistics.txt", date("Y-m-d H:i:s")." - ".print_r($_POST, true)."\n----------\n", FILE_APPEND);
/*


while($orderResult = $orderList->fetch()){
	op($orderResult);
}
*/

/*
$orderList = Sale\Order::getList(array(
	'select' => array('ID'),
	'filter' => array(
		'LID' => '',
		// 'DATE_INSERT' => date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time()),
		'CANCELED' => 'N'
	),
	'order' => array('ID' => 'DESC')
));

// $order = Sale\Order::load(33607);
while($orderResult = $orderList->fetch()){
	op($orderResult);
}
*/

// $ipAddress = Bitrix\Main\Service\GeoIp\Manager::getRealIp();
// $locCode = \Bitrix\Sale\Location\GeoIp::getLocationCode("94.180.246.48", LANGUAGE_ID);

// $result = GeoIp\Manager::getDataResult("94.180.246.48", "ru");
// op($result);


/*
$template = strtolower($arTheme["USE_REGIONALITY"]["DEPENDENT_PARAMS"]["REGIONALITY_VIEW"]["VALUE"]);
if($arTheme["USE_REGIONALITY"]["DEPENDENT_PARAMS"]["REGIONALITY_SEARCH_ROW"]["VALUE"] == "Y" && $template == "select")
    $template = "popup_regions";


$APPLICATION->IncludeComponent(
    "BKV:departament_",
    $template,
    Array(

    )
);
*/

/*
$APPLICATION->IncludeComponent(
	"emi:departament",
	".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"BLOCK_ID" => "17",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);
*/

/*
$arFilter = array("UF_DEP_SITE" => SITE_ID);
$hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById(HL_DEPORTAMENT)->fetch();
$entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

$rsData = $entity_data_class::getList(array(
    "select" => array('*'),
    "filter" => $arFilter,
    "order" => array()
));
if ($arRes = $rsData->Fetch()) {
    op($arRes);
}
*/

/*
$filter = \Bitrix\Catalog\GroupTable::getList(array(
    'filter' => array("ID" => array(15, 60)),
    'select' => array("ID", "NAME"),
    'order' => array("NAME" => "ASC"),
))->fetchAll();


foreach ($filter as $key => $info) {
    op($info);

}*/
/*

$context = Context::getCurrent();
$request = $context->getRequest();


op($request->getRequestUri());
op($request->isHttps());

$arResult['URI'] = $request->getRequestedPageDirectory();

op($arResult);
*/
