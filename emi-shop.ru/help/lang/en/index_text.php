<h2 class="no-top-space">How to place an order</h2>
<p>
    Checkout on our website is easy. Simply add the selected items to the cart, then go to
         Shopping cart, check the correctness of the ordered items and click "Checkout" or "Quick order".
</p>
<h3>Quick order</h3>
<p>
    The “Quick order” function allows the buyer not to go through the entire ordering procedure on his own. You
         fill out the form, and in a short time the store manager will call you back. He will clarify all the conditions of the order,
         answer questions about the quality of the product, its features. It will also tell you about payment options and
         delivery.
</p>
<p>
    According to the results of the call, the user either, having received the clarification, places the order on his own, having completed it
         necessary positions, or agrees to the design in the form in which it is now. Receives confirmation
         to mail or mobile phone and waiting for delivery.
</p>
<h3>Checkout in standard mode</h3>
<p>
    If you are sure of your choice, you can place your order yourself by filling out the form in stages.
</p>
<h4>
    Filling the address</h4>
<p>
    Select from the list the name of your region and locality. If you have not found your location in
         list, select the “Other location” value and enter the name of your location in the “City” column.
         Enter the correct index.
</p>
<h4>
    Delivery</h4>
<p>
    Depending on the place of residence you will be offered delivery options. Choose any convenient way. Read more about
        delivery terms, see "<a target="_blank" href="<?= SITE_DIR; ?>help/delivery/">Delivery</a> ".
</p>
<h4>
    Payment</h4>
<p>
    Choose the best payment method. Read more about all options in the section “<a target ="_blank" href = "<?= SITE_DIR;?>help/payment/">Payment</a> »
</p>
<h4>
    Customer
</h4>
<p>
    Enter your details: name, shipping address, phone number. In the "Comments on the order" enter information
         which may be useful to the courier, for example: entrances in the house are considered from right to left.
</p>
<h4>
    Checkout</h4>
<p>
    Check that the information entered is correct: order items, location selection, customer data. Click
         button "Checkout".
</p>
<p>
    Our service remembers the data about the user, information about the order and next time will offer you to repeat to
         I enter the data of the previous order. If conditions do not suit you, choose other options.
</p>
<p>
</p>