<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(GetMessage("HOW_TO_ORDER_TITE"));
?>

<? $APPLICATION->IncludeComponent(
    "bitrix:main.include",
    ".default",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "AREA_FILE_SHOW" => "file",
        "PATH" => "/help/lang/".LANGUAGE_ID."/how_to_order/index_".LANGUAGE_ID.".php",
        "EDIT_TEMPLATE" => ""
    ),
    false
); ?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>