<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("viewed_show", "Y");
$APPLICATION->SetTitle(GetMessage("MAIN_TITLE"));
?>
<? global $isShowSale, $isShowCatalogSections, $isShowCatalogElements, $isShowMiddleAdvBottomBanner, $isShowBlog, $USER, $DEPORTAMENT_MAIN; ?>


<? $APPLICATION->IncludeComponent(
    "emi:com.banners.emi",
    "top_owl_banner_emi",
    array(
        "IBLOCK_TYPE" => $DEPORTAMENT_MAIN["UF_BANER_TOP_MAIN_TYPE"],
        "IBLOCK_ID" => $DEPORTAMENT_MAIN["UF_BANER_TOP_MAIN"],
        "TYPE_BANNERS_IBLOCK_ID" => "232",
        "SET_BANNER_TYPE_FROM_THEME" => "N",
        "NEWS_COUNT" => "10",
        "NEWS_COUNT2" => "4",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "ID",
        "SORT_ORDER2" => "DESC",
        "PROPERTY_CODE" => array(
            0 => "TEXT_POSITION",
            1 => "TARGETS",
            2 => "TEXTCOLOR",
            3 => "URL_STRING",
            4 => "BUTTON1TEXT",
            5 => "BUTTON1LINK",
            6 => "BUTTON2TEXT",
            7 => "BUTTON2LINK",
            8 => "",
        ),
        "CHECK_DATES" => "Y",
        "CACHE_GROUPS" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "SITE_THEME" => $SITE_THEME,
        "BANNER_TYPE_THEME" => "TOP",
        "BANNER_TYPE_THEME_CHILD" => "TOP_SMALL_BANNER",
    ),
    false
); ?>


    <div class="grey_block small-padding">
        <div class="maxwidth-theme">


            <? $APPLICATION->IncludeComponent(
                "emi:com.banners.emi",
                "adv_middle",
                array(
                    "IBLOCK_TYPE" => $DEPORTAMENT_MAIN["UF_LINE_BANER_TYPE"],
                    "IBLOCK_ID" => $DEPORTAMENT_MAIN["UF_LINE_BANER"],
                    "TYPE_BANNERS_IBLOCK_ID" => "232",
                    "SET_BANNER_TYPE_FROM_THEME" => "N",
                    "NEWS_COUNT" => "10",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "ID",
                    "SORT_ORDER2" => "DESC",
                    "PROPERTY_CODE" => array(
                        0 => "URL",
                        1 => "",
                    ),
                    "CHECK_DATES" => "Y",
                    "CACHE_GROUPS" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "BANNER_TYPE_THEME" => "SMALL"
                ),
                false
            ); ?>
        </div>
        <hr>
    </div>


    <div class="maxwidth-theme">
        <? $APPLICATION->IncludeComponent("bitrix:main.include", ".default",
            array(
                "COMPONENT_TEMPLATE" => ".default",
                "PATH" => SITE_DIR . "include/mainpage/comp_catalog_sections.php",
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "",
                "AREA_FILE_RECURSIVE" => "Y",
                "EDIT_TEMPLATE" => "standard.php"
            ),
            false
        ); ?>
    </div>


    <div class="maxwidth-theme">
        <? $APPLICATION->IncludeComponent("bitrix:main.include", ".default",
            array(
                "COMPONENT_TEMPLATE" => ".default",
                "PATH" => SITE_DIR . "include/mainpage/comp_news_akc.php",
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "",
                "AREA_FILE_RECURSIVE" => "Y",
                "EDIT_TEMPLATE" => "standard.php"
            ),
            false
        ); ?>
    </div>

<?
if (
    SITE_ID == "l5" || //Екатеринбург
    SITE_ID == "m9" || //Красноярск
    SITE_ID == "o5" || //Самара
    SITE_ID == "o7" || //Саратов
    SITE_ID == "l1" || //Сургут
    SITE_ID == "n3" || //Ижевск
    SITE_ID == "p2" || //Киров
    SITE_ID == "l8" || //Оренбург
    SITE_ID == "k4" || //Абакан
    SITE_ID == "m2" || //Пермь
    SITE_ID == "n2" || //Челябинск
    SITE_ID == "k6" || //Калуга
    SITE_ID == "p7" || //Новосибирск
    SITE_ID == "n1" || //Хабаровск
    SITE_ID == "n8" || //Петропавловск-Камчатский
    SITE_ID == "k3" || //Ставрополь
    SITE_ID == "p5" || //Москва
    SITE_ID == "n2" || //Челябинск
    SITE_ID == "r7" || //Волгоград
    SITE_ID == "r9" || //Иваново
    SITE_ID == "o4"    //Пятигорск
){}
else{
?>
    <div class="grey_block">
        <div class="maxwidth-theme">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "front",
                array(
                    "COMPONENT_TEMPLATE" => "front",
                    "PATH" => SITE_DIR . "include/mainpage/comp_catalog_hit.php",
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "",
                    "AREA_FILE_RECURSIVE" => "Y",
                    "EDIT_TEMPLATE" => "standard.php",
                    "PRICE_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "STORES" => array(
                        0 => "",
                        1 => "",
                    ),
                    "STIKERS_PROP" => "HIT",
                    "SALE_STIKER" => "SALE_TEXT"
                ),
                false
            ); ?>
        </div>
    </div>
<?
}
?>

<?
$isShowBlog = false;
if ($isShowBlog):?>
    <div class="maxwidth-theme">
        <? $APPLICATION->IncludeComponent("bitrix:main.include", ".default",
            array(
                "COMPONENT_TEMPLATE" => ".default",
                "PATH" => SITE_DIR . "include/mainpage/comp_blog.php",
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "",
                "AREA_FILE_RECURSIVE" => "Y",
                "EDIT_TEMPLATE" => "standard.php"
            ),
            false
        ); ?>
    </div>
<? endif; ?>


    <div class="maxwidth-theme">
        <? /*
	<?global $arRegion, $isShowCompany;?>
	<div class="company_bottom_block">
		<div class="row wrap_md">
			<div class="col-md-3 col-sm-3 hidden-xs img">
				<?$APPLICATION->IncludeFile(SITE_DIR."include/mainpage/company/front_img.php", Array(), Array( "MODE" => "html", "NAME" => GetMessage("FRONT_IMG") )); ?>
			</div>
			<div class="col-md-9 col-sm-9 big">
				<?if($arRegion):?>
					<?=$arRegion['DETAIL_TEXT'];?>
				<?else:?>
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "front", Array("AREA_FILE_SHOW" => "file","PATH" => SITE_DIR."include/mainpage/company/front_info.php","EDIT_TEMPLATE" => ""));?>
				<?endif;?>
			</div>
		</div>
	</div>
    */ ?>


        <? $APPLICATION->IncludeComponent("bitrix:main.include", ".default",
            array(
                "COMPONENT_TEMPLATE" => ".default",
                "PATH" => SITE_DIR . "include/mainpage/lang/" . LANGUAGE_ID . "/info_previe/comp_info_previe.php",
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "",
                "AREA_FILE_RECURSIVE" => "Y",
                "EDIT_TEMPLATE" => "standard.php"
            ),
            false
        ); ?>


        <? //$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
        //        array(
        //            "COMPONENT_TEMPLATE" => ".default",
        //            "PATH" => SITE_DIR."include/mainpage/comp_instagramm.php",
        //            "AREA_FILE_SHOW" => "file",
        //            "AREA_FILE_SUFFIX" => "",
        //            "AREA_FILE_RECURSIVE" => "Y",
        //            "EDIT_TEMPLATE" => "standard.php"
        //        ),
        //        false
        //    );?>
    </div>
    <br>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>