<div class="inside_page_content">
    <div>Background information on the site helps the resource visitor to quickly find answers to questions and gives hints on how to make certain things faster and easier.</div>
    <br />
    <div>In section &quot;<a href="<?=SITE_DIR?>blog/" >Статьи</a>&quot;You will find interesting materials on how to handle technology, optimal ways to use building materials and many other things that will allow you to quickly select the right product and use it as easily as possible.</div>
    <br />
    <div>We collected answers to typical questions of visitors in the section&quot;<a href="<?=SITE_DIR?>info/faq/" >Question answer</a>&quot;. If something is not clear to you on the site, we suggest visiting this section in order to deal with all the difficulties in a matter of minutes. If the answer could not be found, you can ask us on the same page, and we will answer you for sure!</div>
    <div>And finally, information on manufacturers and their product list can be found on the page &quot;<a href="<?=SITE_DIR?>info/brands/" >Brands</a>&quot;.</div>
</div>