<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$aMenuLinksExt = array();
global $arTheme, $DEPORTAMENT;


if ($arMenuParametrs = CNext::GetDirMenuParametrs(__DIR__)) {
    if ($arMenuParametrs['MENU_SHOW_SECTIONS'] == 'Y') {


        $catalog_id = $DEPORTAMENT["DEPORTAMENT"]["UF_DEP_CATALOG"];

        $arSections = CNextCache::CIBlockSection_GetList(
            array('SORT' => 'ASC', 'ID' => 'ASC', 'CACHE' => array('TAG' => CNextCache::GetIBlockCacheTag($catalog_id), 'MULTI' => 'Y')),
            array(
                'IBLOCK_ID' => $catalog_id,
                'ACTIVE' => 'Y',
                'GLOBAL_ACTIVE' => 'Y',
                'ACTIVE_DATE' => 'Y',
                '<=DEPTH_LEVEL' => \Bitrix\Main\Config\Option::get("aspro.next", "MAX_DEPTH_MENU", 2)
            )
        );

        $arSectionsByParentSectionID = CNextCache::GroupArrayBy($arSections, array('MULTI' => 'Y', 'GROUP' => array('IBLOCK_SECTION_ID')));
    }

    if ($arSections)
        CNext::getSectionChilds(false, $arSections, $arSectionsByParentSectionID, $arItemsBySectionID, $aMenuLinksExt);
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);

?>