<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Сравнение товаров");
global $DEPORTAMENT,$DEPORTAMENT_MAIN;
?>
<?
$APPLICATION->IncludeComponent(
    "bitrix:catalog.compare.result",
    "main",
    array(
        "ACTION_VARIABLE" => "action",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "BASKET_URL" => "/personal/basket.php",
        "CONVERT_CURRENCY" => "Y",
        "DETAIL_URL" => "",
        "CURRENCY_ID" => $DEPORTAMENT["DEPORTAMENT"]["UF_DEP_CURRENCY"],
        "DISPLAY_ELEMENT_SELECT_BOX" => "N",
        "ELEMENT_SORT_FIELD" => "sort",
        "ELEMENT_SORT_FIELD_BOX" => "name",
        "ELEMENT_SORT_FIELD_BOX2" => "id",
        "ELEMENT_SORT_ORDER" => "asc",
        "ELEMENT_SORT_ORDER_BOX" => "asc",
        "ELEMENT_SORT_ORDER_BOX2" => "desc",
        "FIELD_CODE" => array(
            0 => "NAME",
            1 => "PREVIEW_PICTURE",
            2 => "DETAIL_PICTURE",
        ),
        "HIDE_NOT_AVAILABLE" => "N",
        "IBLOCK_ID" => $DEPORTAMENT["DEPORTAMENT"]["UF_DEP_CATALOG"],
        "IBLOCK_TYPE" => $DEPORTAMENT["DEPORTAMENT"]["DEP_CATALOG_TYPE"],
        "NAME" => "CATALOG_COMPARE_LIST",
        "PRICE_CODE" => $DEPORTAMENT["DEPORTAMENT"]["UF_DEP_TYPE_PRICE_CODE"],
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_ID_VARIABLE" => "id",
        "PROPERTY_CODE" => array(
            0 => "",
            1 => "",
        ),
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "SHOW_PRICE_COUNT" => "1",
        "TEMPLATE_THEME" => "blue",
        "USE_PRICE_COUNT" => "N",
        "COMPONENT_TEMPLATE" => "main"
    ),
    false
); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>