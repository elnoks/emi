<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle(GetMessage("CATALOG_TITLE"));
global $DEPORTAMENT, $DEPORTAMENT_MAIN, $arTheme;

$APPLICATION->IncludeComponent(
    "bitrix:catalog",
    "catalogDep",
    array(
        "IBLOCK_TYPE" => $DEPORTAMENT["DEPORTAMENT"]["DEP_CATALOG_TYPE"],
        "IBLOCK_ID" => $DEPORTAMENT["DEPORTAMENT"]["UF_DEP_CATALOG"],
        "HIDE_NOT_AVAILABLE" => "L",
        "BASKET_URL" => "/basket/",
        "ACTION_VARIABLE" => "action",
        "PRODUCT_ID_VARIABLE" => "id",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => "/catalog/",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "SET_TITLE" => "Y",
        "SET_STATUS_404" => "Y",
        "USE_ELEMENT_COUNTER" => "Y",
        "USE_FILTER" => "Y",
        "FILTER_NAME" => "NEXT_SMART_FILTER",
        "FILTER_FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "FILTER_PROPERTY_CODE" => array(
            0 => "CML2_ARTICLE",
            1 => "IN_STOCK",
            2 => "",
        ),
        "FILTER_PRICE_CODE" => $DEPORTAMENT["DEPORTAMENT"]["UF_DEP_TYPE_PRICE_CODE"],
        "FILTER_OFFERS_FIELD_CODE" => array(),
        "FILTER_OFFERS_PROPERTY_CODE" => array(),
        "USE_REVIEW" => "Y",
        "MESSAGES_PER_PAGE" => "10",
        "USE_CAPTCHA" => "Y",
        "REVIEW_AJAX_POST" => "Y",
        "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
        "FORUM_ID" => "#FORUM_ID#",
        "URL_TEMPLATES_READ" => "",
        "SHOW_LINK_TO_FORUM" => "Y",
        "POST_FIRST_MESSAGE" => "N",
        "USE_COMPARE" => "Y",
        "COMPARE_NAME" => "CATALOG_COMPARE_LIST",
        "COMPARE_FIELD_CODE" => array(
            0 => "NAME",
            1 => "TAGS",
            2 => "SORT",
            3 => "PREVIEW_PICTURE",
            4 => "",
        ),
        "COMPARE_PROPERTY_CODE" => array(
            0 => "CML2_ARTICLE",
            1 => "CML2_BASE_UNIT",
            2 => "CML2_MANUFACTURER",
            3 => "",
        ),
        "COMPARE_OFFERS_FIELD_CODE" => array( ),
        "COMPARE_OFFERS_PROPERTY_CODE" => array(),
        "COMPARE_ELEMENT_SORT_FIELD" => "shows",
        "COMPARE_ELEMENT_SORT_ORDER" => "asc",
        "DISPLAY_ELEMENT_SELECT_BOX" => "N",
        "PRICE_CODE" => $DEPORTAMENT["DEPORTAMENT"]["UF_DEP_TYPE_PRICE_CODE"],
        "USE_PRICE_COUNT" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "PRICE_VAT_SHOW_VALUE" => "N",
        "PRODUCT_PROPERTIES" => array(
            '0' => "SPECIAL_OFFER",
        ),
        "USE_PRODUCT_QUANTITY" => "Y",
        "CONVERT_CURRENCY" => "Y",
        "CURRENCY_ID" => $DEPORTAMENT["DEPORTAMENT"]["UF_DEP_CURRENCY"],
        "OFFERS_CART_PROPERTIES" => "",
        "SHOW_TOP_ELEMENTS" => "Y",
        "SECTION_COUNT_ELEMENTS" => "Y",
        "SECTION_TOP_DEPTH" => "2",
        "SECTIONS_LIST_PREVIEW_PROPERTY" => "DESCRIPTION",
        "SHOW_SECTION_LIST_PICTURES" => "Y",
        "PAGE_ELEMENT_COUNT" => "20",
        "LINE_ELEMENT_COUNT" => "4",
        "ELEMENT_SORT_FIELD" => "sort",
        "ELEMENT_SORT_ORDER" => "asc",
        "ELEMENT_SORT_FIELD2" => "id",
        "ELEMENT_SORT_ORDER2" => "asc",
        "LIST_PROPERTY_CODE" => array(
            0 => "TORGOVAYA_MARKA",
            1 => "",
        ),
        "INCLUDE_SUBSECTIONS" => "Y",
        "LIST_META_KEYWORDS" => "-",
        "LIST_META_DESCRIPTION" => "-",
        "LIST_BROWSER_TITLE" => "-",
        "LIST_OFFERS_FIELD_CODE" => array(),
        "LIST_OFFERS_PROPERTY_CODE" => array(),
        "LIST_OFFERS_LIMIT" => "10",
        "SORT_BUTTONS" => array(
            0 => "POPULARITY",
            1 => "NAME",
            2 => "PRICE",
        ),
        "SORT_PRICES" => "REGION_PRICE",
        "DEFAULT_LIST_TEMPLATE" => "block",
        "SECTION_DISPLAY_PROPERTY" => "",
        "LIST_DISPLAY_POPUP_IMAGE" => "Y",
        "SECTION_PREVIEW_PROPERTY" => "DESCRIPTION",
        "SHOW_SECTION_PICTURES" => "Y",
        "SHOW_SECTION_SIBLINGS" => "Y",
        "DETAIL_PROPERTY_CODE" => array(
            0 => "",
            1 => "",
        ),
        "DETAIL_META_KEYWORDS" => "-",
        "DETAIL_META_DESCRIPTION" => "-",
        "DETAIL_BROWSER_TITLE" => "-",
        "DETAIL_OFFERS_FIELD_CODE" => array(
            0 => "NAME",
            1 => "PREVIEW_PICTURE",
            2 => "DETAIL_PICTURE",
            3 => "DETAIL_PAGE_URL",
            4 => "",
        ),
        "DETAIL_OFFERS_PROPERTY_CODE" => array(
            0 => "FRAROMA",
            1 => "ARTICLE",
            2 => "SPORT",
            3 => "VLAGOOTVOD",
            4 => "AGE",
            5 => "RUKAV",
            6 => "KAPUSHON",
            7 => "FRCOLLECTION",
            8 => "FRLINE",
            9 => "FRFITIL",
            10 => "VOLUME",
            11 => "FRMADEIN",
            12 => "FRELITE",
            13 => "SIZES",
            14 => "TALL",
            15 => "FRFAMILY",
            16 => "FRSOSTAVCANDLE",
            17 => "FRTYPE",
            18 => "FRFORM",
            19 => "COLOR_REF",
            20 => "",
        ),
        "PROPERTIES_DISPLAY_LOCATION" => "DESCRIPTION",
        "SHOW_BRAND_PICTURE" => "Y",
        "SHOW_ASK_BLOCK" => "Y",
        "ASK_FORM_ID" => "3",
        "SHOW_ADDITIONAL_TAB" => "Y",
        "PROPERTIES_DISPLAY_TYPE" => "TABLE",
        "SHOW_KIT_PARTS" => "Y",
        "SHOW_KIT_PARTS_PRICES" => "Y",
        "LINK_IBLOCK_TYPE" => "1c_catalog",
        "LINK_IBLOCK_ID" => "176",
        "LINK_PROPERTY_SID" => "ANALOGUES",
        "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
        "USE_ALSO_BUY" => "Y",
        "ALSO_BUY_ELEMENT_COUNT" => "5",
        "ALSO_BUY_MIN_BUYES" => "2",
        "USE_STORE" => "N",
        "USE_STORE_PHONE" => "Y",
        "USE_STORE_SCHEDULE" => "Y",
        "USE_MIN_AMOUNT" => "N",
        "MIN_AMOUNT" => "10",
        "STORE_PATH" => "/contacts/stores/#store_id#/",
        "MAIN_TITLE" => "Наличие на складах",
        "MAX_AMOUNT" => "20",
        "USE_ONLY_MAX_AMOUNT" => "Y",
        "OFFERS_SORT_FIELD" => "shows",
        "OFFERS_SORT_ORDER" => "asc",
        "OFFERS_SORT_FIELD2" => "shows",
        "OFFERS_SORT_ORDER2" => "asc",
        "PAGER_TEMPLATE" => "main",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Товары",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
       // "IBLOCK_STOCK_ID" => "258",
        "SHOW_QUANTITY" => "Y",
        "SHOW_MEASURE" => "Y",
        "SHOW_QUANTITY_COUNT" => "Y",
        "USE_RATING" => "Y",
        "DISPLAY_WISH_BUTTONS" => "Y",
        "DEFAULT_COUNT" => "1",
        "SHOW_HINTS" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "ADD_SECTIONS_CHAIN" => "Y",
        "ADD_ELEMENT_CHAIN" => "Y",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "PARTIAL_PRODUCT_PROPERTIES" => "Y",
        "DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
        "STORES" => $DEPORTAMENT["DEPORTAMENT"]["UF_DEP_STOCK"],
        "USER_FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "SHOW_EMPTY_STORE" => "Y",
        "SHOW_GENERAL_STORE_INFORMATION" => "N",
        "TOP_ELEMENT_COUNT" => "8",
        "TOP_LINE_ELEMENT_COUNT" => "4",
        "TOP_ELEMENT_SORT_FIELD" => "name",
        "TOP_ELEMENT_SORT_ORDER" => "asc",
        "TOP_ELEMENT_SORT_FIELD2" => "shows",
        "TOP_ELEMENT_SORT_ORDER2" => "asc",
        "TOP_PROPERTY_CODE" => array(
            0 => "",
            1 => "",
        ),
        "COMPONENT_TEMPLATE" => "catalogDep",
        "DETAIL_SET_CANONICAL_URL" => "N",
        "SHOW_DEACTIVATED" => "N",
        "TOP_OFFERS_FIELD_CODE" => array(
            0 => "ID",
            1 => "",
        ),
        "TOP_OFFERS_PROPERTY_CODE" => array(
            0 => "",
            1 => "",
        ),
        "TOP_OFFERS_LIMIT" => "10",
        "SECTION_TOP_BLOCK_TITLE" => "Лучшие предложения",
        "OFFER_TREE_PROPS" => array(
            0 => "SIZES",
            1 => "COLOR_REF",
        ),
        "USE_BIG_DATA" => "Y",
        "BIG_DATA_RCM_TYPE" => "bestsell",
        "SHOW_DISCOUNT_PERCENT" => "Y",
        "SHOW_OLD_PRICE" => "Y",
        "VIEWED_ELEMENT_COUNT" => "20",
        "VIEWED_BLOCK_TITLE" => "Ранее вы смотрели",
        "ELEMENT_SORT_FIELD_BOX" => "name",
        "ELEMENT_SORT_ORDER_BOX" => "asc",
        "ELEMENT_SORT_FIELD_BOX2" => "id",
        "ELEMENT_SORT_ORDER_BOX2" => "desc",
        "ADD_PICT_PROP" => "MORE_PHOTO",
        "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
        "DETAIL_ADD_DETAIL_TO_SLIDER" => "Y",
        "SKU_DETAIL_ID" => "oid",
        "USE_MAIN_ELEMENT_SECTION" => "Y",
        "SET_LAST_MODIFIED" => "N",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "SHOW_404" => "Y",
        "MESSAGE_404" => "",
        "AJAX_FILTER_CATALOG" => "N",
        "SECTION_BACKGROUND_IMAGE" => "-",
        "DETAIL_BACKGROUND_IMAGE" => "-",
        "DISPLAY_ELEMENT_SLIDER" => "10",
        "SHOW_ONE_CLICK_BUY" => "Y",
        "USE_GIFTS_DETAIL" => "Y",
        "USE_GIFTS_SECTION" => "Y",
        "USE_GIFTS_MAIN_PR_SECTION_LIST" => "Y",
        "GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "8",
        "GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
        "GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
        "GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
        "GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT" => "8",
        "GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE" => "N",
        "GIFTS_SECTION_LIST_BLOCK_TITLE" => "Подарки к товарам этого раздела",
        "GIFTS_SECTION_LIST_TEXT_LABEL_GIFT" => "Подарок",
        "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
        "GIFTS_SHOW_OLD_PRICE" => "Y",
        "GIFTS_SHOW_NAME" => "Y",
        "GIFTS_SHOW_IMAGE" => "Y",
        "GIFTS_MESS_BTN_BUY" => "Выбрать",
        "GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "8",
        "GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
        "GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
        "OFFER_HIDE_NAME_PROPS" => "N",
        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
        "DETAIL_SET_VIEWED_IN_COMPONENT" => "N",
        "SECTION_PREVIEW_DESCRIPTION" => "Y",
        "SECTIONS_LIST_PREVIEW_DESCRIPTION" => "N",
        "SHOW_DISCOUNT_TIME" => "Y",
        "SHOW_RATING" => "Y",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "DETAIL_OFFERS_LIMIT" => "0",
        "DETAIL_EXPANDABLES_TITLE" => "Аксессуары",
        "DETAIL_ASSOCIATED_TITLE" => "Похожие товары",
        "DETAIL_PICTURE_MODE" => "MAGNIFIER",
        "SHOW_UNABLE_SKU_PROPS" => "Y",
        "HIDE_NOT_AVAILABLE_OFFERS" => "N",
        "DETAIL_STRICT_SECTION_CHECK" => "Y",
        "COMPATIBLE_MODE" => "Y",
        "TEMPLATE_THEME" => "blue",
        "LABEL_PROP" => "",
        "PRODUCT_DISPLAY_MODE" => "Y",
        "COMMON_SHOW_CLOSE_POPUP" => "N",
        "PRODUCT_SUBSCRIPTION" => "Y",
        "SHOW_MAX_QUANTITY" => "N",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "MESS_BTN_COMPARE" => "Сравнение",
        "MESS_BTN_DETAIL" => "Подробнее",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "MESS_BTN_SUBSCRIBE" => "Подписаться",
        "SIDEBAR_SECTION_SHOW" => "Y",
        "SIDEBAR_DETAIL_SHOW" => "N",
        "SIDEBAR_PATH" => "",
        "USE_SALE_BESTSELLERS" => "Y",
        "FILTER_VIEW_MODE" => "VERTICAL",
        "FILTER_HIDE_ON_MOBILE" => "N",
        "INSTANT_RELOAD" => "N",
        "COMPARE_POSITION_FIXED" => "Y",
        "COMPARE_POSITION" => "top left",
        "USE_RATIO_IN_RANGES" => "Y",
        "USE_COMMON_SETTINGS_BASKET_POPUP" => "N",
        "COMMON_ADD_TO_BASKET_ACTION" => "ADD",
        "TOP_ADD_TO_BASKET_ACTION" => "ADD",
        "SECTION_ADD_TO_BASKET_ACTION" => "ADD",
        "DETAIL_ADD_TO_BASKET_ACTION" => array(
            0 => "BUY",
        ),
        "DETAIL_ADD_TO_BASKET_ACTION_PRIMARY" => array(
            0 => "BUY",
        ),
        "TOP_PROPERTY_CODE_MOBILE" => "",
        "TOP_VIEW_MODE" => "SECTION",
        "TOP_PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
        "TOP_PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false}]",
        "TOP_ENLARGE_PRODUCT" => "STRICT",
        "TOP_SHOW_SLIDER" => "Y",
        "TOP_SLIDER_INTERVAL" => "3000",
        "TOP_SLIDER_PROGRESS" => "N",
        "SECTIONS_VIEW_MODE" => "LIST",
        "SECTIONS_SHOW_PARENT_NAME" => "Y",
        "LIST_PROPERTY_CODE_MOBILE" => "",
        "LIST_PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
        "LIST_PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false}]",
        "LIST_ENLARGE_PRODUCT" => "STRICT",
        "LIST_SHOW_SLIDER" => "Y",
        "LIST_SLIDER_INTERVAL" => "3000",
        "LIST_SLIDER_PROGRESS" => "N",
        "DETAIL_MAIN_BLOCK_PROPERTY_CODE" => "",
        "DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE" => "",
        "DETAIL_USE_VOTE_RATING" => "N",
        "DETAIL_USE_COMMENTS" => "N",
        "DETAIL_BRAND_USE" => "N",
        "DETAIL_DISPLAY_NAME" => "Y",
        "DETAIL_IMAGE_RESOLUTION" => "16by9",
        "DETAIL_PRODUCT_INFO_BLOCK_ORDER" => "sku,props",
        "DETAIL_PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",
        "DETAIL_SHOW_SLIDER" => "N",
        "DETAIL_DETAIL_PICTURE_MODE" => array(
            0 => "POPUP",
            1 => "MAGNIFIER",
        ),
        "DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
        "MESS_PRICE_RANGES_TITLE" => "Цены",
        "MESS_DESCRIPTION_TAB" => "Описание",
        "MESS_PROPERTIES_TAB" => "Характеристики",
        "MESS_COMMENTS_TAB" => "Комментарии",
        "LAZY_LOAD" => "N",
        "LOAD_ON_SCROLL" => "N",
        "USE_ENHANCED_ECOMMERCE" => "N",
        "DETAIL_DOCS_PROP" => "-",
        "STIKERS_PROP" => "SPECIAL_OFFER",
        "USE_SHARE" => "Y",
        "TAB_OFFERS_NAME" => "",
        "TAB_DESCR_NAME" => "",
        "TAB_CHAR_NAME" => "",
        "TAB_VIDEO_NAME" => "",
        "TAB_REVIEW_NAME" => "",
        "TAB_FAQ_NAME" => "",
        "TAB_STOCK_NAME" => "",
        "TAB_DOPS_NAME" => "",
        "BLOCK_SERVICES_NAME" => "",
        "BLOCK_DOCS_NAME" => "",
        "CHEAPER_FORM_NAME" => "",
        "DIR_PARAMS" => AsproNextCustom::GetDirMenuParametrs(__DIR__),
        "SHOW_CHEAPER_FORM" => "N",
        "LANDING_TITLE" => "Популярные категории",
        "LANDING_SECTION_COUNT" => "7",
        "SECTIONS_TYPE_VIEW" => "sections_1",
        "SECTION_ELEMENTS_TYPE_VIEW" => "list_elements_1",
        "ELEMENT_TYPE_VIEW" => "FROM_MODULE",
        "SHOW_ARTICLE_SKU" => "Y",
        "SORT_REGION_PRICE" => "Розничная",
        "SHOW_MEASURE_WITH_RATIO" => "N",
        "ALT_TITLE_GET" => "NORMAL",
        "SHOW_COUNTER_LIST" => "Y",
        "SHOW_DISCOUNT_TIME_EACH_SKU" => "N",
        "USER_CONSENT" => "N",
        "USER_CONSENT_ID" => "0",
        "USER_CONSENT_IS_CHECKED" => "Y",
        "USER_CONSENT_IS_LOADED" => "N",
        "SHOW_HOW_BUY" => "Y",
        "TITLE_HOW_BUY" => "Как купить",
        "SHOW_DELIVERY" => "Y",
        "TITLE_DELIVERY" => "Доставка",
        "SHOW_PAYMENT" => "Y",
        "TITLE_PAYMENT" => "Оплата",
        "SHOW_GARANTY" => "Y",
        "TITLE_GARANTY" => "Условия гарантии",
        "USE_FILTER_PRICE" => "N",
        "DISPLAY_ELEMENT_COUNT" => "Y",
        "RESTART" => "Y",
        "USE_LANGUAGE_GUESS" => "Y",
        "NO_WORD_LOGIC" => "Y",
        "SHOW_SECTION_DESC" => "Y",
        "TITLE_SLIDER" => "Рекомендуем",
        "VIEW_BLOCK_TYPE" => "N",
        "SHOW_SEND_GIFT" => "Y",
        "SEND_GIFT_FORM_NAME" => "",
        "USE_ADDITIONAL_GALLERY" => "N",
        "BLOCK_LANDINGS_NAME" => "",
        "BLOG_IBLOCK_ID" => "",
        "BLOCK_BLOG_NAME" => "",
        "RECOMEND_COUNT" => "5",
        "VISIBLE_PROP_COUNT" => "4",
        "BUNDLE_ITEMS_COUNT" => "3",
        "STORES_FILTER" => "TITLE",
        "STORES_FILTER_ORDER" => "SORT_ASC",
        "FILE_404" => "",
        "SEF_URL_TEMPLATES" => array(
            "sections" => "",
            "section" => "#SECTION_CODE_PATH#/",
            "element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
            "compare" => "compare.php?action=#ACTION_CODE#",
            "smart_filter" => "#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/",
        ),
        "VARIABLE_ALIASES" => array(
            "compare" => array(
                "ACTION_CODE" => "action",
            ),
        )
    ),
    false
); ?>

    <script src="/local/templates/aspro_next/js/mo.min.js"></script>
    <script>
        (function (window) {

            'use strict';

            // taken from mo.js demos
            function isIOSSafari() {
                var userAgent;
                userAgent = window.navigator.userAgent;
                return userAgent.match(/iPad/i) || userAgent.match(/iPhone/i);
            };

            // taken from mo.js demos
            function isTouch() {
                var isIETouch;
                isIETouch = navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;
                return [].indexOf.call(window, 'ontouchstart') >= 0 || isIETouch;
            };

            // taken from mo.js demos
            var isIOS = isIOSSafari(),
                clickHandler = isIOS || isTouch() ? 'touchstart' : 'click';

            function extend(a, b) {
                for (var key in b) {
                    if (b.hasOwnProperty(key)) {
                        a[key] = b[key];
                    }
                }
                return a;
            }

            function Animocon(el, options) {
                this.el = el;
                this.options = extend({}, this.options);
                extend(this.options, options);

                this.checked = false;

                this.timeline = new mojs.Timeline();

                for (var i = 0, len = this.options.tweens.length; i < len; ++i) {
                    this.timeline.add(this.options.tweens[i]);
                }

                var self = this;
                self.options.onCheck();
                self.timeline.replay();
            }

            Animocon.prototype.options = {
                tweens: [
                    new mojs.Burst({})
                ],
                onCheck: function () {
                    return false;
                },
                onUnCheck: function () {
                    return false;
                }
            };

            // grid items:


            function init_compare_animate(item) {

                var el = item.querySelector('.compare_item.in'), elspan = item.querySelector('.compare_item.in i');

                new Animocon(el, {
                    tweens: [
                        // icon scale animation
                        new mojs.Tween({
                            duration: 1500,
                            easing: mojs.easing.ease.out,
                            onUpdate: function (progress) {
                                if (progress > 0.3) {
                                    var elasticOutProgress = mojs.easing.elastic.out(1.26 * progress - 0.26);
                                    elspan.style.WebkitTransform = elspan.style.transform = 'scale3d(' + elasticOutProgress + ',' + elasticOutProgress + ',1)';
                                }
                                else {
                                    elspan.style.WebkitTransform = elspan.style.transform = 'scale3d(0,0,1)';
                                }
                            }
                        }),
                        // ring animation
                        new mojs.Shape({
                            parent: el,
                            duration: 750,
                            type: 'circle',
                            radius: {0: 24},
                            fill: 'transparent',
                            stroke: '#F35186',
                            strokeWidth: {20: 0},
                            opacity: 0.2,
                            x: Math.floor(Math.random() * 60) - 30,
                            y: Math.floor(Math.random() * 60) - 30,
                            easing: mojs.easing.bezier(0, 1, 0.5, 1),
                            css: "z-index: 9999"
                        }),
                        new mojs.Shape({
                            parent: el,
                            duration: 500,
                            delay: 100,
                            type: 'circle',
                            radius: {0: 12},
                            fill: 'transparent',
                            stroke: '#F35186',
                            strokeWidth: {5: 0},
                            opacity: 0.2,
                            x: Math.floor(Math.random() * 60) - 30,
                            y: Math.floor(Math.random() * 60) - 30,
                            easing: mojs.easing.sin.out,
                            css: "z-index: 9999"
                        }),
                        new mojs.Shape({
                            parent: el,
                            duration: 500,
                            delay: 180,
                            type: 'circle',
                            radius: {0: 6},
                            fill: 'transparent',
                            stroke: '#F35186',
                            strokeWidth: {2: 0},
                            opacity: 0.5,
                            x: Math.floor(Math.random() * 60) - 30,
                            y: Math.floor(Math.random() * 60) - 30,
                            isRunLess: true,
                            easing: mojs.easing.sin.out,
                            css: "z-index: 9999"
                        }),
                        new mojs.Shape({
                            parent: el,
                            duration: 800,
                            delay: 240,
                            type: 'circle',
                            radius: {0: 10},
                            fill: 'transparent',
                            stroke: '#F35186',
                            strokeWidth: {8: 0},
                            opacity: 0.3,
                            x: Math.floor(Math.random() * 60) - 30,
                            y: Math.floor(Math.random() * 60) - 30,
                            easing: mojs.easing.sin.out,
                            css: "z-index: 9999"
                        }),
                        new mojs.Shape({
                            parent: el,
                            duration: 800,
                            delay: 240,
                            type: 'circle',
                            radius: {0: 14},
                            fill: 'transparent',
                            stroke: '#F35186',
                            strokeWidth: {5: 0},
                            opacity: 0.4,
                            x: Math.floor(Math.random() * 60) - 30,
                            y: Math.floor(Math.random() * 60) - 30,
                            easing: mojs.easing.sin.out,
                            css: "z-index: 9999"
                        }),
                        new mojs.Shape({
                            parent: el,
                            duration: 600,
                            delay: 330,
                            type: 'circle',
                            radius: {0: 13},
                            fill: 'transparent',
                            stroke: '#F35186',
                            strokeWidth: {7: 0},
                            opacity: 0.4,
                            x: Math.floor(Math.random() * 60) - 30,
                            y: Math.floor(Math.random() * 60) - 30,
                            easing: mojs.easing.sin.out,
                            css: "z-index: 9999"
                        })

                    ],
                    onCheck: function () {
                        //el.style.color = '#F35186';
                    },
                    onUnCheck: function () {
                        //el.style.color = '#b1b1b1';
                    }
                });

            }

            var wishlist_items = [].slice.call(document.querySelectorAll('.like_icons'));

            function init_wishlist_animate(item) {
                //wishlist_items.forEach(function(item, i, arr) {

                var el = item.querySelector('.wish_item.in'), elspan = item.querySelector('.wish_item.in i');

                new Animocon(el, {
                    tweens: [
                        // icon scale animation
                        new mojs.Tween({
                            duration: 1500,
                            easing: mojs.easing.ease.out,
                            onUpdate: function (progress) {
                                if (progress > 0.3) {
                                    var elasticOutProgress = mojs.easing.elastic.out(1.26 * progress - 0.26);
                                    elspan.style.WebkitTransform = elspan.style.transform = 'scale3d(' + elasticOutProgress + ',' + elasticOutProgress + ',1)';
                                }
                                else {
                                    elspan.style.WebkitTransform = elspan.style.transform = 'scale3d(0,0,1)';
                                }
                            }
                        }),
                        // ring animation
                        new mojs.Shape({
                            parent: el,
                            duration: 750,
                            type: 'circle',
                            radius: {0: 24},
                            fill: 'transparent',
                            stroke: '#F35186',
                            strokeWidth: {20: 0},
                            opacity: 0.2,
                            x: Math.floor(Math.random() * 60) - 30,
                            y: Math.floor(Math.random() * 60) - 30,
                            easing: mojs.easing.bezier(0, 1, 0.5, 1),
                            css: "z-index: 9999"
                        }),
                        new mojs.Shape({
                            parent: el,
                            duration: 500,
                            delay: 100,
                            type: 'circle',
                            radius: {0: 12},
                            fill: 'transparent',
                            stroke: '#F35186',
                            strokeWidth: {5: 0},
                            opacity: 0.2,
                            x: Math.floor(Math.random() * 60) - 30,
                            y: Math.floor(Math.random() * 60) - 30,
                            easing: mojs.easing.sin.out,
                            css: "z-index: 9999"
                        }),
                        new mojs.Shape({
                            parent: el,
                            duration: 500,
                            delay: 180,
                            type: 'circle',
                            radius: {0: 6},
                            fill: 'transparent',
                            stroke: '#F35186',
                            strokeWidth: {2: 0},
                            opacity: 0.5,
                            x: Math.floor(Math.random() * 60) - 30,
                            y: Math.floor(Math.random() * 60) - 30,
                            isRunLess: true,
                            easing: mojs.easing.sin.out,
                            css: "z-index: 9999"
                        }),
                        new mojs.Shape({
                            parent: el,
                            duration: 800,
                            delay: 240,
                            type: 'circle',
                            radius: {0: 10},
                            fill: 'transparent',
                            stroke: '#F35186',
                            strokeWidth: {8: 0},
                            opacity: 0.3,
                            x: Math.floor(Math.random() * 60) - 30,
                            y: Math.floor(Math.random() * 60) - 30,
                            easing: mojs.easing.sin.out,
                            css: "z-index: 9999"
                        }),
                        new mojs.Shape({
                            parent: el,
                            duration: 800,
                            delay: 240,
                            type: 'circle',
                            radius: {0: 14},
                            fill: 'transparent',
                            stroke: '#F35186',
                            strokeWidth: {5: 0},
                            opacity: 0.4,
                            x: Math.floor(Math.random() * 60) - 30,
                            y: Math.floor(Math.random() * 60) - 30,
                            easing: mojs.easing.sin.out,
                            css: "z-index: 9999"
                        }),
                        new mojs.Shape({
                            parent: el,
                            duration: 600,
                            delay: 330,
                            type: 'circle',
                            radius: {0: 13},
                            fill: 'transparent',
                            stroke: '#F35186',
                            strokeWidth: {7: 0},
                            opacity: 0.4,
                            x: Math.floor(Math.random() * 60) - 30,
                            y: Math.floor(Math.random() * 60) - 30,
                            easing: mojs.easing.sin.out,
                            css: "z-index: 9999"
                        })

                    ],
                    onCheck: function () {
                        //el.style.color = '#F35186';
                    },
                    onUnCheck: function () {
                        //el.style.color = '#C0C1C3';
                    }
                });

                //});
            }

            // init_wishlist_animate();
            $('.wish_item_button').on('click', function () {
                init_wishlist_animate(this);
            });

            $('.compare_item_button').on('click', function () {
                init_compare_animate(this);
            });

        })(window);

    </script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>