<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $APPLICATION,$DEPORTAMENT;
$arIBlock['ID'] = '176';

$aMenuLinksExt = $APPLICATION->IncludeComponent(
    "bitrix:menu.sections",
    "",
    array(
        "IS_SEF" => "N",
        "ID" => $_REQUEST["ID"],
        "IBLOCK_TYPE" => $DEPORTAMENT["DEPORTAMENT"]["UF_DEP_CATALOG"],
        "IBLOCK_ID" => $DEPORTAMENT["DEPORTAMENT"]["DEP_CATALOG_TYPE"],
        "SECTION_URL" => "",
        "DEPTH_LEVEL" => "4",
        "CACHE_TYPE" => "Y",
        "CACHE_TIME" => "36000000"
    ),
    false
);
$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>