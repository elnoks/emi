<?
$MESS["ERROR_MESSAGE"] = "Error 404";
$MESS["ERROR_TEXT_1"] = "Page not found";
$MESS["ERROR_TEXT_2"] = "The address is incorrect or <br /> this page does not exist";
$MESS["ERROR_TEXT_3"] = "Go to home";
$MESS["ERROR_TEXT_4"] = "go back";
$MESS["ERROR_TEXT_5"] = "or";
$MESS["ERROR_TEXT_6"] = "Page not found";