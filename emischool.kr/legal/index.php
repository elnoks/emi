<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Details of the Seller");
?><p>
	 E.Mi&nbsp;— International s.r.o<br>
	 Registered in&nbsp;the&nbsp;Registry of&nbsp;Companies, maintained at&nbsp;Municipal Court in&nbsp;Prague, Section C, file 189332
</p>
<p>
	 U božích bojovníku 89/1, 130 00 Praha-Žižkov<br>
	 IČ No. 24214647<br>
	 VAT ID&nbsp;No. CZ24214647
</p>
<p>
	 Account number: 6858888001/5500<br>
	 Swift: RZBCCZPP<br>
	 Iban: CZ6855000000006858888001
</p>
<p>
	 Mailing address: E.Mi&nbsp;— International s.r.o., Štefánikova 203/23, 150 00, Prague 5&nbsp;— Smíchov<br>
	 Phone number: +420 773 208 276<br>
	 Contact e-mail:&nbsp;<a href="mailto:prague@emischool.com">prague@emischool.com</a>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>