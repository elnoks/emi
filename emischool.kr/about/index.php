<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("About company");
?><div class="bx_page">
	<div class="h1-top">
		<h1>About company</h1>
	</div>
	<div class="bx_page" style="text-align: center;">
 <img width="1140" alt="56.jpg" src="/upload/medialibrary/f7c/f7cb52963256ebe4b40f9af5c03a36d2.jpg" height="300" title="56.jpg" border="0"> <br>
	</div>
 <br>
	<p>
        E.Mi는 세계 챔피언 네일 디자인인 Ekaterina Miroshnichenko가 개발한 최신 독창적인 수천가지의 기성품 솔루션과 함께 자신의 참여로 제작된 네일 아트를 위한 고유한 재료 및 액세서리를 제공합니다.
        E.Mi 임무는 모든 네일리스트가 자신의 잠재력을 이끌어 내고 아티스트를 느끼며 진정한 패션 전문가가 되도록 돕는 것입니다. Ekaterina Miroshnichenko의 네일 디자인 학교에서 E.Mi 놀라운 디자인을 하는 방법을 배울 수 있습니다.
        E.Mi는 성공적인 사업을 개발하고 있으며, 당신도 일부가 될 수 있습니다! 현재 E.Mi는 러시아, 유럽 및 아시아의 Ekaterina Miroshnichenko, E.Mi 브랜드 유통 업체 및 전세계 수많은 판매점에서 네일 디자인 스쿨의 공식 대표 50 개를 포함합니다.
    </p>
	<p>
 <a href="/social/open_school.php" type="button" class="btn-pink order-button">Become a partner</a>
	</p>
	<p>
 <br>
	</p>
	<h2>READY-MADE SOLUTIONS&NAIL&FASHION<br>
 </h2>
	<p>
 <img width="1140" alt="54.jpg" src="/upload/medialibrary/cd4/cd48dcf71294554d8839eab0a34a4986.jpg" height="150" title="98.jpg" border="0"> <br>
	</p>
	<p>
        오트 쿠튀르는 예술입니다. 그것은 실험을 즐겁게 하고 격려하며 언제나 놀라움을 선사하는 새로운 계절을 고대합니다.
        E.Mi 기성품 솔루션은 오트 쿠튀르 네일 아트입니다. 올해에는 MLA (파리, 2010)에 따라 Fantasy 범주의 세계 챔피언 네일 디자인인 Ekaterina Miroshnichenko가 개발한 독점적인 컬렉션을 2회 유럽 챔피언 (아테네, 파리, 2009), 국제 심판 및 네일 디자인의 학교의 설립자입니다.
        <br>
	</p>
	<p>
        각 Ekaterina의 컬렉션은 손톱 디자인으로 구체화 된 최신 경향에 대한 독창적인 해석이며 첫눈에 믿을 수 없을만큼 아름답고 독창적이며 훈련 과정 또는 마스터 클래스 비디오 이후에 쉽게 이행 할 수 있습니다.
        Zhostovo Painting, One Stroke Painting, Imitation of reptile skin, Crackled effect, Ethnic Prints, Velvet sand 및 liquid stones 등의 베스트 셀러가 Ekaterina Miroshnichenko 컬렉션에 포함되어 있습니다. 지금 Ekaterina Miroshnichenko의 전체 디자인 컬렉션에서 영감을 얻으십시오!
        <br>
	</p>
	<p>
 <a href="/social/salon.php" type="button" class="btn-pink order-button">Choose a salon</a>
	</p>
	<p>
 <br>
	</p>
	<h2>PRODUCTS FOR NAIL DESIGN</h2>
	<p>
 <img width="1140" alt="6542.jpg" src="/upload/medialibrary/6e3/6e3b48a3e4405516b24de034f7ca5b60.jpg" height="150" title="6542.jpg" border="0"> <br>
	</p>
	<p>
        E.Mi 제품을 사용하는 모든 네일리스트는 안전하다고 말할 수 있습니다. "이 제품은 나를 위해 만들었습니다!"
        첫눈에 반해이 사랑의 비밀은 간단합니다. Ekaterina Miroshnichenko는 각 제품, 새로운 색상 및 액세서리를 만드는 데 개인적으로 참여합니다. 네일 디자이너, 세계 챔피언, 네일 아트의 존경받는 전문가가 함께 일하고자하는 E.Mi 제품을 만듭니다. 그래서 E.Mi 제품이 네일 디자이너의 모든 요구 사항을 충족하고 작업의 모든 특성을 고려하며 작동 시간을 절약하고 창의력을 발휘할 수 있는 기회를 제공합니다.
        <br>
	</p>
	<p>
        EMPASTA, GLOSSEMI, TEXTONE, PRINCOT과 같은 독창적인 제품은 물론 젤 페인트 컬렉션, 가장 우수한 색상의 디자이너 호일, 다양한 장식 재료 및 액세서리가 E.Mi에서 제공됩니다. 지금 필요한 제품을 찾으세요!
	</p>
	<p>
 <a href="/catalog/" type="button" class="btn-pink order-button">Choose</a>
	</p>
	<p>
 <br>
	</p>
	<h2>SCHOOL OF NAIL DESIGN BY EKATERINA MIROSHNICHENKO<br>
 </h2>
	<p>
 <img width="1140" alt="765.jpg" src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" height="150" title="765.jpg" border="0"> <br>
	</p>
	<p>
        재미있고 창의적인 직업에 대한 꿈을 꾸고 싶다면 고객이 고맙게 여기는 미소를 지으며 돌아오고 다시 돌아 오기를 원하면 Ekaterina Miroshnichenko가 디자인 한 네일 스쿨의 교육 과정을 이수하러 오세요.
        Ekaterina Miroshnichenko의 디자인 학교는 네일리스트를 위한 독창적인 교육 프로그램을 제공합니다. 네일 디자이너는 미술 교육없이 네일리스트를 위해 특별히 정교합니다. Ekaterina Miroshnichenko의 독특한 방법은 모든 네일 디자인 과정의 기초입니다.
        <br>
	</p>
	<p>
        디자인 과정은 3 단계의 복잡성과 추가 과정으로 나뉩니다. 올림푸스를 정복하고 네일 아트 경연에 참가하고자하는 사람들은 네일 디자인 학교에서 특별 교육 과정을 제공합니다. Ekaterina Miroshnichenko의 디자인 스쿨 학생들은 지역 및 연방 네일 디자인 공모전을 여러 번 수상했습니다.
        현재 Ekaterina Miroshnichenko의 디자인 학교의 50 개의 공식 대표가 있으며 모두 높은 수준의 공동 작업을 하고 있습니다. 꿈을 실현시키고 네일 아트의 진정한 전문가가 되어라 - 지금 자신의 교육 프로그램을 선택하세요!
        저희 웹 사이트에서 가장 가까운 학교를 찾고 당신의 꿈을 이루세요!

    </p>
	<p>
 <a href="/courses/" type="button" class="btn-pink order-button">Choose</a>
	</p>
	<p>
 <br>
	</p>
	<h2>OFFICIAL REPRESENTATIVES</h2>
	<p>
 <img width="1140" alt="7645.jpg" src="/upload/medialibrary/060/060926d856ef3ef2bcc0eef18fa948cc.jpg" height="150" title="7645.jpg" border="0"> <br>
	</p>
	<p>
        E.Mi 브랜드와 Ekaterina Miroshnichenko의 네일 디자인 스쿨은 인기가 높고 수요가 많은 최신 비즈니스 라인입니다. 현재 러시아, 우크라이나, 카자흐스탄, 벨로루시, 이탈리아, 포르투갈, 루마니아, 사이프러스, 독일, 프랑스, 리투아니아, 슬로바키아, 한국 및 아랍 에미리트에서 네일 디자인 스쿨의 공식 대표인 Ekaterina Miroshnichenko가 성공적으로 운영되고 있습니다. E.Mi 제품은 전세계에서 구입할 수 있습니다.
	</p>
	<p>
        또한 성공적인 국제 브랜드의 일부가 될 수 있으며 기성품인 비즈니스 솔루션을 얻을 수 있습니다 - Ekaterina Miroshnichenko의 네일 디자인 학교 공식 대표 또는 귀하의 도시, 지역 또는 국가에서 E.Mi 브랜드의 공식 대리점이 될 수 있습니다. Ekaterina Miroshnichenko의  네일 디자인 학교의 공식 대표자는 다음과 같은 이점을 얻습니다:
	</p>
	<ul>
		<li>귀하 지역의 Ekaterina Miroshnichenko의 네일 디자인 학교 브랜드를 독점적으로 대변 할 가능성;</li>
		<li>Ekaterina Miroshnichenko의 네일 디자인에 대한 독창적인 코스를 튜터링 할 권리;</li>
		<li>비즈니스 개방 및 개발을 위한 완벽한 비즈니스 프로세스;</li>
		<li>완벽한 설계 유지 보수 및 인력 교육;</li>
		<li>연방 판촉 지원;</li>
		<li>모든 E.Mi 제품 범위 구입에 대한 이익 결정</li>
	</ul>
	<p>
	</p>
	<p>
        Ekaterina Miroshnichenko 또는 E.Mi 브랜드 공식 판매 업체의 네일 디자인 학교의 공식 대표자가 되려면 웹 사이트에서 신청서를 작성하거나 전화해주세요  +420 722935746 Korbut Marina, <a href="mailto:korbut@emischool.com">korbut@emischool.com</a>
	</p>
	<p>
 <br>
	</p>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>