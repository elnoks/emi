<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Web Interface usage terms and conditions");
?><p>
	 Web Interface usage terms and conditions
</p>
<p>
	 You entered the <a href="http://www.emischool.com">www.emischool.com</a> web interface (hereinafter only "the web interface"), operated by our Company
</p>
<p>
	 E.Mi - International s.r.o., with offices at U božích bojovníků 89/1, 130 00, Prague 3 - Žižkov
</p>
<p>
	 ID No.: 24214647
</p>
<p>
	 VAT No.: CZ24214647
</p>
<p>
	 registered in the Companies' register maintained at the Municipal court in Prague, Section C, file 189332
</p>
<p>
	 Address for Service: Štefánikova 203/23, 150 00, Prague 5 - Smíchov
</p>
<p>
	 Phone number: + 420 773 208 276
</p>
<p>
	 Contact e-mail: <a href="mailto:prague@emischool.com">prague@emischool.com</a>
</p>
<p>
</p>
<p>
	 Please, note, that regardless of whether you purchase using the web interface, or you only visit, you should adhere with the following rules, which stipulate and specify the the terms and conditions of all functional parts of the web interface.
</p>
<p>
	 1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Web interface registration
</p>
<p>
	 You should register in order to order goods at the web interface. You register on the web interface using a registration form therein. Enter the required information into the registration form, especially the name, surname and a contact e-mail. By registering you create a user's account.
</p>
<p>
	 You need a user's name and password to enter the user's account. Keep the login information for the user's account confidential. Our Company bears no responsibility for eventual unauthorized use of the user's account by a third person.
</p>
<p>
	 The information provided upon the registration have to be truthful and complete. The account, created using false or incomplete information, may be canceled without compensation.
</p>
<p>
	 In case of changes of your information we recommend to modify the user account without delay.
</p>
<p>
	 Using the user's account you can order goods, track orders and maintain the user's account. Eventual other functions of the user's account are always listed on the web interface.&nbsp;
</p>
<p>
	 Please, note that we are entitled to cancel your user's account without compensation, if good manners, relevant legislation or these terms and conditions are violated using your user's account.&nbsp;&nbsp;
</p>
<p>
</p>
<p>
	 2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Protection of personal data
</p>
<p>
	 Upon entering an order or registration on the web interface you provide us with your personal data. Use of the web interface also means storage and processing of other information, to which we have access. By entering personal information and using the web interface you agree with processing and collection of your personal data in the scope specified below for the purposes specified below, until declaration of dissent with such processing.
</p>
<p>
	 Protection of personal data is very important for us. Therefore, while processing personal data, we comply with the legislation of Czech Republic, especially the Act No. 101/2000 Coll., on protection of personal data (hereinafter only "APPD"), as amended.
</p>
<p>
	 2.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; What is personal and other data?
</p>
<p>
	 Personal data is data you voluntarily provided during completion of an order or registration. Personal data means all information, identifying or possibly identifying a specific person. Personal data is, especially, without limitation, a name and surname, photographs, date of birth, e-mail address and residential address or a phone number.
</p>
<p>
	 Other data,&nbsp; collected automatically in relation with the use of the web interface, such as IP address, type of browser, equipment and operating system, duration and number of accesses to the web interface, information collected using the cookie files and other similar information. Please, note, that we can collect other data even without registration and regardless of whether you make purchase on the web interface or not.
</p>
<p>
	 2.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; How do we use personal and other data?
</p>
<p>
	 We use to personal and other data especially to provide you with access to your account and the easiest possible use of the web interface.
</p>
<p>
	 We use the data for communication regarding the administration of your account and for user support. The data can be used to improve our services, including the use of analysis of the behavior of the interface users.
</p>
<p>
	 The data can be used for commercial and marketing purposes, i.e. to maintain a database of users of the web interface and to offer goods and services for an indefinite period of time. By posting an order of by registration you agree with receiving commercial messages by all electronic means.
</p>
<p>
	 The consent with receiving of commercial messages and electronic mail for the purpose of direct marketing can be canceled anytime by an e-mail to our contact e-mail address.
</p>
<p>
	 2.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; How do we administer and process your personal data?
</p>
<p>
	 Our Company is the administrator of personal data in the sense of APPD and is registered at the Personal Data Protection Administration under registration number 0059797&nbsp;/ 001.
</p>
<p>
	 We can appoint a third person a processor of your personal and other data.
</p>
<p>
	 Personal and other collected data is comprehensively secured against misuse.
</p>
<p>
	 Personal data will be processed for an indefinite period of time. Personal data will be process in electronic format in an authorized way or printed by non-automatic method.
</p>
<p>
	 2.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Who receives your personal data from us?
</p>
<p>
	 Your personal data is not passed to anyone. The exception are external freighters and persons participating on the delivery of goods or provision of service. Such persons receive your personal information in minimal scope, necessary for the delivery of goods or provision of service.
</p>
<p>
	 2.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; What are your right with respect to personal data?
</p>
<p>
	 You have a right of access to your personal data and a right to information about their processing (information on the purpose of processing, information on sources of the data and information on the receiver). This information will be provided to you without unnecessary delay upon request. You also have right to correction of personal data and other legal rights to this data.
</p>
<p>
	 Upon your written request we will remove your personal data from the database.
</p>
<p>
	 If you suspect, that our Company or the processor of personal data perform the processing of your personal data in violation of the law, you can:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; request an explanation from us or the processor;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; request that we or the processor remove the condition. It concerns especially blocking, performing remedy, completion or disposal of personal data.
</p>
<p>
	 We will assist you in protection of your personal data as much as possible. In case you are not satisfied with the solution, you can contact appropriate authorities, especially the Authority for protection of personal data. This provision does not affect your right to contact the Authority for protection of personal data with your suggestion directly.
</p>
<p>
	 We are entitled to request reasonable compensation for the processing of personal data, not in excess of the necessary costs of providing the information.
</p>
<p>
	 The supervision of the protection of personal data is performed by the Authority for protection of personal data (<a href="http://www.uoou.cz)">http://www.uoou.cz)</a>.
</p>
<p>
	 Our Company and eventual processors of personal data have registered offices in Czech Republic.
</p>
<p>
</p>
<p>
	 3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Google Analytics and cookie files
</p>
<p>
	 The web interface uses the Google Analytics service, provided by Google, Inc. (hereinafter only "Google").
</p>
<p>
	 3.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; What is Google Analytics service?
</p>
<p>
	 Google Analytics service uses "cookie" files, which are text files, saved on the computer of each visitor of the web interface, and which enable an analysis of usage of the web interface.
</p>
<p>
	 Information generated by the cookie file about the usage (including the IP address) will be transfered by Google and saved on servers in the United States of America. Google will use the information for the purposes of evaluation of the usage of the web interface and to create reports on the users' activity, intended for us and for the usage of the Internet in general. Google can also provide this information to third parties, should it be required by law or should such third parties process the information for Google. Google will not connect the subject's IP address with any other data available to it.
</p>
<p>
	 By using the web interface you agree with processing of data about you by Google by a method and for a purpose described above.
</p>
<p>
	 3.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Can you prevent the saving of cookie files in your computer?
</p>
<p>
	 You can refuse the use of cookie files in the appropriate preference in the web browser.
</p>
<p>
	 Please, note, that refusal to use cookie files may mean you will not be able to fully use all functions of the web interface.
</p>
<p>
</p>
<p>
	 4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Copyright protection
</p>
<p>
	 The contents of web pages, located on the web interface (texts, photos, images, logos etc.), including program features of the web interface and these terms and conditions, are protected by our copyright and may be protected by rights of other entities. You may not alter, copy, duplicate, distribute or use it for any purpose without the consent of our Company or the owner of the copyright. It is especially forbidden to make photos and texts, located at the web interface, available neither free of charge nor against payment.
</p>
<p>
	 The names and designations of products, goods, services, firms and companies may be registered trademarks of respective owners.&nbsp;
</p>
<p>
	 4.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; How will we proceed in case of copyright violation?
</p>
<p>
	 In case the described prohibition is not respected, we will proceed in accordance with Act No. &nbsp;121/2000 Coll., copyright law, as amended.
</p>
<p>
	 Our Company, as an owner of copyrights, is entitled to demand the violation of our copyrights ceases and to request withdrawal of unauthorized copies of protected contents.
</p>
<p>
	 We are also entitled to request adequate compensation for incurred damages.
</p>
<p>
</p>
<p>
	 5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Other relationships related to the use of the web interface
</p>
<p>
	 5.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please, note, that by clicking on any link on the web interface you may leave the web interface and be redirected to web pages of third parties.
</p>
<p>
	 5.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Our Company is not responsible for errors cause by interference of third parties in the web interface or cause by its use in contradiction with its intent. When using the web interface you may not use mechanisms, programs, scripts or other processes, which could have negative impact on its operation, i.e. especially to impair the system's function or to load the system excessively and you also may not perform any activity, which could enable you or any third party to interfere or use the programs or other parts of the web interface in an unauthorized manner and to use the web interface or its parts or software in violation with its intent or purpose.
</p>
<p>
	 5.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; We cannot guarantee uninterrupted access to the web interface or flawlessness and safety of the web interface. We are not responsible for damages, caused during access and use of the web interface, including eventual damages incurred after download of data, published at the web interface, damages caused by an interruption of operation, defect of the web interface, computer virii, damages caused by data loss, loss of profit or unauthorized access to transmissions and data.
</p>
<p>
	 5.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If, while using the web interface, you commit any unlawful or unethical action, we are entitled to restrict, suspend or terminate your access to the web interface without any compensation. In such case you are also obliged to compensate our Company in full for damages demonstrably caused by your actions according to this paragraph.
</p>
<p>
</p>
<p>
	 The term and conditions of use are valid and effective from&nbsp;23.07.2015.
</p>
<p>
	 &nbsp;
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>