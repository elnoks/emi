<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("About Company");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Σχετικά με την Εταιρεία</h1>
   </div>
 
  <div class="bx_page" style="text-align: center;"><img src="/upload/medialibrary/f7c/f7cb52963256ebe4b40f9af5c03a36d2.jpg" title="56.jpg" border="0" alt="56.jpg" width="1140" height="300"  /> 
    <br />
   </div>
 
  <br />
 
  <p>Η εταιρεία E.Mi προσφέρει χιλιάδες μοναδικές λύσεις στον τομέα του nail design, τις οποίες έχει εμπνευστεί η πρωταθλήτρια του κόσμου στο nail design Ekaterina Miroshnichenko, αλλά και μοναδικά υλικά και αξεσουάρ nail-art, τα οποία σχεδιάστηκαν επίσης με τη συμμετοχή της.</p>
 
  <p>Αποστολή της E.Mi είναι να βοηθήσει κάθε τεχνίτρια να αναπτύξει τις ικανότητές της στο έπακρο, αναδεικνύοντας την καλλιτεχνική της φύση και οδηγώντας τη σε μια επιτυχημένη πορεία ως fashion-expert! Για να μάθει κανείς να δημιουργεί τα μοναδικά σχέδια της E.Mi θα πρέπει να απευθυνθεί σε μία από τις σχολές nail design της Ekaterina Miroshnichenko.</p>
 
  <p>Η E.Mi είναι μια ταχέως αναπτυσσόμενη και επιτυχημένη εταιρεία, με παρουσία σε δεκάδες χώρες ανά τον κόσμο. Συνολικά πενήντα επίσημες αντιπροσωπείες της βρίσκονται στη Ρωσία αλλά και σε χώρες της Ευρώπης και της Ασίας, ενώ υπάρχουν εκατοντάδες σημεία πώλησης σε όλο τον κόσμο.</p>
 
  <h2>ΕΤΟΙΜΕΣ ΛΥΣΕΙΣ NAIL & FASHION</h2>
 
  <p><img src="/upload/medialibrary/cd4/cd48dcf71294554d8839eab0a34a4986.jpg" title="98.jpg" border="0" alt="54.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Η υψηλή μόδα είναι μια πραγματική τέχνη, καθώς συναρπάζει και ενθαρρύνει τους ανθρώπους που ασχολούνται με αυτήν να προβαίνουν σε τολμηρά πειράματα με εκθαμβωτικά αποτελέσματα, δημιουργώντας μια αίσθηση ανυπομονησίας για τις νέες κολεξιόν και τις εκπλήξεις που αυτές θα φέρουν.</p>
 
  <p>Οι έτοιμες λύσεις της E.Mi είναι η υψηλή μόδα στον χώρο του nail design. Οι συλλογές της E.Mi παρουσιάζονται κάθε χρόνο και αποτελούν αποκλειστική δημιουργία της Ekaterina Miroshnichenko, της παγκόσμιας πρωταθλήτριας στο nail design (2010), δύο φορές πρωταθλήτριας Ευρώπης, διαιτητή διεθνούς κλάσης και ιδρυτή της Σχολής nail design E.Mi.</p>
 
  <p>Κάθε συλλογή της Ekaterina Miroshnichenko αντανακλά τις πιο δημοφιλείς και επίκαιρες τάσεις της μόδας, που αποτυπώνονται στη μοναδική γλώσσα του nail design. Όμορφα και μοναδικά σχέδια με την πρώτη ματιά, αλλά και εύκολα στην εκμάθηση μέσω σεμιναρίων ή σχετικών master class βίντεο.</p>
 
  <p>Μεταξύ αυτών συγκαταλέγονται και οι bestseller συλλογές, όπως τα: Zhostovo Painting, One Stroke Painting, Imitation of reptile skin, Crackled effect, Ethnic Prints, Velvet sand και liquid stones αλλά και πολλές άλλες συλλογές της Ekaterina Miroshnichenko. Είναι στο χέρι σας να εμπνευστείτε από τις ολοκληρωμένες συλλογές της άμεσα και γρήγορα!</p>
 
  <h2>ΠΡΟΙΟΝΤΑ ΓΙΑ NAIL DESIGN</h2>
 
  <p><img src="/upload/medialibrary/6e3/6e3b48a3e4405516b24de034f7ca5b60.jpg" title="6542.jpg" border="0" alt="6542.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Τα προϊόντα της E.Mi είναι ειδικά φτιαγμένα για κάθε άνθρωπο που ανήκει στο χώρο του nail design.</p>
 
  <p>Με τις γνώσεις της ως nail designer, αλλά και ως αξιοσέβαστη ειδικός σε θέματα nail-art η Ekaterina Miroshnichenko κρύβεται πίσω από τη δημιουργία κάθε νέου προϊόντος, νέου χρώματος αλλά και αξεσουάρ, κάνοντάς το αξεπέραστο σε ποιότητα και προσαρμόζοντάς το στις υψηλές απαιτήσεις της κάθε τεχνίτριας. Για αυτό άλλωστε, τα προϊόντα E.Mi απαντούν σε όλες τις ανάγκες των σύγχρονων nail-designers, λαμβάνοντας υπόψη όλες τις ιδιαιτερότητες της δουλειάς, εξοικονομώντας εργασιακό χρόνο αλλά και προσφέροντας καταπληκτικές ευκαιρίες για να αναπτύξει κανείς τη δημιουργικότητά του.</p>
 
  <p>Μεταξύ των προϊόντων που θα βρείτε στην E.Mi, είναι και τα μοναδικά EMPASTA, GLOSSEMI, TEXTONE, PRINCOT, όπως επίσης και οι συλλογές χρωμάτων gel, σχέδια με foil, μια μεγάλη ποικιλία από υλικά διακόσμησης νυχιών αλλά και μοναδικά αξεσουάρ. Βρείτε τα προϊόντα που θα καλύψουν όλες τις ανάγκες σας και θα σας χαρίσουν χρόνο και φήμη!</p>
 
  <h2>Η ΣΧΟΛΗ ΣΧΕΔΙΑΣΜΟΥ ΝΥΧΙΩΝ ΤΗΣ EKATERINA MIROSHNICHENKO</h2>
 
  <p><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" border="0" alt="765.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Εάν ονειρεύεστε μια ενδιαφέρουσα και δημιουργική επαγγελματική πορεία στο χώρο του nail design, θέλετε να χτίσετε μια μακροχρόνια και σταθερή σχέση με τις πελάτισσές σας και να είστε πραγματική επαγγελματίας, εμπλουτίστε τις γνώσεις σας μέσω των σεμιναρίων της σχολή της Ekaterina Miroshnichenko στην Ελλάδα.</p>
 
  <p>Τα μοναδικά εκπαιδευτικά προγράμματα για τους σχεδιαστές που δεν είχαν καμία προηγούμενη καλλιτεχνική εκπαίδευση αποτελούν μια μοναδική ευκαιρία, ενώ οι τεχνικές της Ekaterina Miroshnichenko που παρουσιάζονται, αποτελούν τη βάση όλων των σεμιναρίων nail design.</p>
 
  <p>Τα σεμινάρια nail design χωρίζονται σε 3 επίπεδα δυσκολίας και σε επιμέρους μαθήματα. Μάλιστα η σχολή παραδίδει και ειδικά προσαρμοσμένα σεμινάρια για εκείνους που επιθυμούν να λάβουν μέρος σε διαγωνισμούς nail-art, με τεράστια επιτυχία, καθώς δεκάδες συμμετέχοντες των σεμιναρίων της σχολής nail design της Ekaterina Miroshnichenko έχουν κερδίσει βραβεία σε τοπικούς αλλά και διεθνείς διαγωνισμούς.</p>
 
  <p>Αυτήν τη στιγμή, υπάρχουν 50 επίσημοι αντιπρόσωποι της Σχολής Nail Design της Ekaterina Miroshnichenko και όλοι λειτουργούν βάσει των υψηλών προδιαγραφών που ορίζει η ίδια. Διαλέξτε το πρόγραμμα που σας ταιριάζει τώρα στην ιστοσελίδα μας και δώστε πνοή στα όνειρά σας!</p>

 
  <h2>ΕΠΙΣΗΜΟΙ ΑΝΤΙΠΡΟΣΩΠΟΙ</h2>
 
  <p><img src="/upload/medialibrary/060/060926d856ef3ef2bcc0eef18fa948cc.jpg" title="7645.jpg" border="0" alt="7645.jpg" width="1140" height="150"  /> 
    <br />
   </p>

  <p>Τα προϊόντα E.Mi και η Σχολή Nail Design της Ekaterina Miroshnichenko αποτελούν μια βιώσιμη επιχειρηματική ευκαιρία που απολαμβάνει όλο και αυξανόμενη δημοτικότητα και διεθνής αναγνώριση. Την παρούσα στιγμή υπάρχουν αρκετές επιτυχημένες αντιπροσωπείες που λειτουργούν στη Ρωσία, στην Ουκρανία, στο Καζακστάν, στη Λευκορωσία, στην Ιταλία, στην Πορτογαλία, στη Ρουμανία, στην Κύπρο, στην Ελλάδα, στη Γερμανία, στη Γαλλία, στη Λιθουανία, στη Σλοβακία, στη Νότια Κορέα και στα Ηνωμένα Αραβικά Εμιράτα. Μπορείτε να προμηθευτείτε τα προϊόντα της E.Mi σε όλο τον κόσμο. Μπορείτε και εσείς να γίνετε μέλος της διεθνούς επιτυχημένης μάρκας και να εξελιχθείτε μέσω της έτοιμης επιχειρηματικής λύσης. Γίνετε επίσημος αντιπρόσωπος ή διανομέας της E.Mi. στην περιοχή σας. Οι επίσημοι αντιπρόσωποι απολαμβάνουν τα ακόλουθα οφέλη:
<ul> 
	<li>
τη δυνατότητα αποκλειστικής αντιπροσώπευσης της Σχολής στην περιοχή σας
	</li>
	<li>
το δικαίωμα διεξαγωγής εκπαιδευτικών σεμιναρίων με θέμα το nail design
	</li>
	<li>
πρόσβαση στα ολοκληρωμένα επιχειρηματικά πακέτα για το άνοιγμα και την ανάπτυξη της επιχείρησης
	</li>
	<li>
την πλήρη υποστήριξη για την επιλογή και εκπαίδευση προσωπικού
	</li>
	<li>
τη διαφημιστική υποστήριξη
	</li>
	<li>
τις συμφέρουσες λύσεις για την αγορά ολόκληρου φάσματος των προϊόντων της E.Mi.
	</li>
</ul></p>
 
  <p>Για να γίνετε επίσημος αντιπρόσωπος ή διανομέας της E.Mi., μπορείτε να συμπληρώσετε την αίτηση στην ιστοσελίδα μας, να μας καλέσετε στο +357 25103268, +30 2130 412428, ή να στείλετε e-mail στο greece-school@emischool.com.</p>

  <p>
    <br />
  </p>

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>