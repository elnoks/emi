<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("General Retail Terms and Conditions");
?><p>
	 These General Retail Terms and Conditions (hereinafter only "terms and conditions") apply to contracts signed through on-line E.MI - School of Nail Design by Ekaterina Mirochnichenko store, located at the web portal <a href="http://www.emischool.com">www.emischool.com</a> (hereinafter only "web portal"), between Company
</p>
<p>
	 E.Mi - International s.r.o., with offices at U božích bojovníků 89/1, 130 00, Prague 3 - Žižkov
</p>
<p>
	 ID No.: 24214647
</p>
<p>
	 VAT ID No.: CZ24214647
</p>
<p>
	 registered in the Registry of Companies, maintained at Municipal Court in Prague, Section C, file 189332
</p>
<p>
	 Mailing address: E.Mi - International s.r.o., Štefánikova 203/23, 150 00, Prague 5 - Smíchov
</p>
<p>
	 Phone number: + 420 773 208 276
</p>
<p>
	 Contact e-mail: <a href="mailto:prague@emischool.com">prague@emischool.com</a>
</p>
<p>
	 as the Seller
</p>
<p>
	 and you as the Buyer
</p>
<p>
</p>
<p>
	 1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; OPENING CLAUSES
</p>
<p>
	 By the Purchase Contract we agree to supply to you the goods listed in a purchase order and you agree to accept the goods (either in person or from the forwarder) and to pay us the purchase price, mentioned in the purchase order, and to accept it. The purchase price (or only "the price") includes also the costs associated with the delivery of goods and eventual fees, associated with the selected method of payment. The costs of delivery of goods cannot be established prior to the placement of order, therefore you will be informed about the costs based on the delivery address after the placement of an order, but prior to contract execution.
</p>
<p>
	 You assume the ownership of the goods by paying the complete purchase price, but not before you accept the goods.
</p>
<p>
	 1.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Does the purchase contract apply only to the goods?
</p>
<p>
	 The Purchase contract (or only "the contract") herein means any contract executed according to these terms and conditions. Therefore it can mean e.g. a contract for services.
</p>
<p>
	 1.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Is the purchase contract a consumer's contract?
</p>
<p>
	 It is a consumer's contract if you are a consumer, i.e. if you are a physical entity and you buy the goods outside your business activity or independent performance of your occupation. Otherwise it is not a consumer's contract and the protection of consumers according to the legislation and these terms and conditions does not apply to you, however the General wholesale terms and conditions do.
</p>
<p>
	 1.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; What are your special rights as consumer?
</p>
<p>
	 As a consumer, you have especially:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; the right to withdraw from contract executed using means of remote communication, such as telephone, e-mail or internet shop (Article 5 herein);
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; entitlement to 24-months guarantee on unused consumer goods (guarantee claims are governed by the Complaint Guidelines);
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; the right to information prior to execution of contract (information is included herein or on the web portal).
</p>
<p>
	 1.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; What governs our legal relationship?
</p>
<p>
	 Our legal relationship is governed by the following documents:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; these terms and conditions, which delimit and specify our mutual rights and obligations;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="/legal/complaint_guidelines.php">Complaint guidelines</a>, according to which we will proceed in case of complaints;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="/legal/web_interface_usage_terms_and_conditions.php">Terms and conditions of the web portal</a>, which stipulate the registration on the web portal, protection of your personal data, protection of the contents of the web portal and certain other relationships, associated with the use of the web portal;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; terms and conditions published on the web portal, especially while executing a contract;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; purchase order and its acceptance by us,
</p>
<p>
	 and in matters not stipulated also by the following legislation:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Act No. 89/2012 Coll., Civic Code, as amended (hereinafter only "the Civic Code");
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Act No. 634/1992 Coll., on consumer's protection, as amended (only if you are a consumer).
</p>
<p>
	 If your place of residence or registered office are located outside Czech Republic, or if our legal relationship contains other international element, be advised, that our relationship is governed by Czech law. If you are a consumer and the legislation of the Country of your residence provides you with a higher level of protection than the Czech legislation, you will be provided with such higher level of protection in legal relationships.
</p>
<p>
	 1.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; How do you express consent with the terms and conditions?
</p>
<p>
	 By sending a purchase order and also by confirmation on the web portal you confirm, that you are familiar and agree with these terms and conditions.
</p>
<p>
	 We can change or modify the wording of terms and conditions. Your rights and obligations are always governed by the wording of the terms and conditions, under which they originated.
</p>
<p>
</p>
<p>
	 2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PURCHASE CONTRACT
</p>
<p>
	 2.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; How to execute a purchase contract?
</p>
<p>
	 The web portal lists goods including a description of the main features of individual items. Each item is provided with a price including all taxes, duties and fees. The presentation of goods is only informative and does not represent our proposal to execute a contract in the sense of Art. 1732 Sec. 2 of the Civic Code. The execution of the contract requires you to send the purchase order so it is received by us and that you pay for goods by a method mentioned in Art. 3.1 herein.
</p>
<p>
	 2.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; How to place a purchase order?
</p>
<p>
	 A purchase order can be placed through the web portal (by completing the form). Registration on the web portal is necessary for order placement. The conditions of registration are stipulated in <a href="/legal/web_interface_usage_terms_and_conditions.php">Terms and Conditions of use of the web portal</a>.
</p>
<p>
	 A purchase order has to contain all information required in the form, especially accurate designation of the ordered goods (eventually numerical designation of goods), quantity, selected method of payment and transport and your contact information (delivery and billing address eventually).
</p>
<p>
	 Prior to sending the purchase order you will receive a summary of your order. We recommend to check especially the type and quantity of goods, e-mail and delivery address. Within the summary you have the last opportunity to change the data.
</p>
<p>
	 You will place a purchase order by pressing "Complete Order" button. We consider the information in a purchase order to be correct and complete. Inform us of any changes of the information by phone or e-mail without delay.
</p>
<p>
	 We will inform you about receiving the order without delay. Information (confirmation) of a reception of a purchase order is dispatched automatically and does not mean acceptance of the order by us.
</p>
<p>
	 In case of doubts about the authenticity and seriousness of an order we will contact you for its verification. We can reject an unverified order. Such purchase order is considered as never posted.
</p>
<p>
	 After the reception we will inform you also about the costs of goods delivery by sending an invoice, in which the total purchase price of the good, including costs of delivery, will be stated. We will send the invoice to you to an e-mail address stated in the order. Basic information on costs of delivery, or the invoice respectively, represents acceptance of the order by us.
</p>
<p>
	 Please note, that if you place an order from a Country, other than Czech Republic, and a national distributor of our goods operates in such Country, your order will be transferred for execution by such distributor.&nbsp;
</p>
<p>
	 2.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; When is the contract executed, then?
</p>
<p>
	 The purchase contract is executed when you pay the purchase price of the goods, including the costs of delivery of the goods by a method stipulated in Art. 3.1 herein.
</p>
<p>
	 The information about individual technical steps towards the execution of the contract are obvious from the web portal.
</p>
<p>
	 Can you cancel a posted order?
</p>
<p>
	 You can cancel a purchase order we did not accept yet (i.e. you did not receive information about the costs of delivery of the goods from us according to article 2.2 herein) by phone or e-mail. All orders accepted by us are binding. You can cancel an accepted order in case you do not agree with the amount of costs of delivery by not paying the price. Otherwise an accepted order can be cancelled only in agreement with us. If an order is cancelled, preventing withdrawal from contract (see article 5 for details), we are entitled for payment of costs we already incurred in relation to the contract.
</p>
<p>
	 2.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Can the price of goods, listed on web portal, be changed?
</p>
<p>
	 The prices of the presented goods remain valid for the period it is shown on the web portal. Eventual discounts from prices of goods cannot be combined, unless expressly stated on the web portal otherwise.
</p>
<p>
	 In case of an obvious technical error in listing a price on the web portal or during ordering on our side, we are not obliged to deliver the goods to you for this obviously erroneous price, even in case that a confirmation of the order has been sent to you according to these terms and conditions. In such case we reserve the right to withdraw from the contract.
</p>
<p>
	 If the price listed for goods on the web portal or during ordering is not up-to-date, we will notify you without delay. If your order was not accepted, we are not obliged to execute the contract.
</p>
<p>
	 Posted orders are not affected by changes of prices, which took place between posting of an order and its reception on our side, according to the article 2.2 herein.
</p>
<p>
	 2.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Can you obtain the contract in text form?
</p>
<p>
	 The contract is not executed in writing with signatures of the Parties. The contract comprises of these Terms and Conditions, your purchase order and its reception on our side. The entire contract will be send to you by e-mail or printed by mail upon your request. In case of mailing we may ask you to pay the associated fees.
</p>
<p>
	 2.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; What if you do not understand something in the contract?
</p>
<p>
	 You can contact us by phone or e-mail in case of questions regarding the terms and conditions. We will gladly provide any necessary information.
</p>
<p>
	 2.7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In which languages can the contract be executed?
</p>
<p>
	 The Contract can be executed in Czech, unless we expressly agree on another language.
</p>
<p>
	 2.8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Is the contract archived anywhere?
</p>
<p>
	 We archive the contract (including these Terms and Conditions) in the electronic form. The contract is not accessible to third parties, but we will send it to you upon request.
</p>
<p>
</p>
<p>
	 3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PAYMENT CONDITIONS
</p>
<p>
	 3.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Which payment types we accept?
</p>
<p>
	 You can pay the purchase price especially by the following methods:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; in cash prior to delivery of goods through Western Union payment points;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; cashless prior to delivery of goods by a transfer to our bank account (the instructions will be provided to you in purchase order confirmation).
</p>
<p>
	 Eventual other payment methods are listed on the web portal.
</p>
<p>
	 3.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; When is the purchase price due?
</p>
<p>
	 In case of payment in cash through Western Union payment points and in case of cashless payment the price is due within seven days from dispatch of information on costs of delivery o goods, invoice respectively (i.e. acceptance of purchase order). The price is, in case of cashless payment, considered as paid upon crediting of the amount to our bank account, in case of cash payment at a Western Union payment point upon confirmation of the payment by Western Union. Please, be advised, that we have to be&nbsp; notified about the payment through the Western Union payment point and the information for payment of the money has to be provided.
</p>
<p>
	 3.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In which currency can you make payment?
</p>
<p>
	 Payment in Euros (EUR) is possible.
</p>
<p>
	 3.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; When can we require deposit?
</p>
<p>
	 We can require a deposit on the purchase price especially for purchase orders with total value over EUR 200.
</p>
<p>
	 4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DELIVERY TERMS
</p>
<p>
	 4.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; How we ship goods?
</p>
<p>
	 We ship goods through a forwarding service listed on the web portal. Eventual other delivery methods are <a href="/payment-and-delivery.php">listed on the web portal</a> as well. Particular methods of delivery can be selected in the purchase order. If you don't select any method of delivery, we can select it.
</p>
<p>
	 4.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; What are the cost of delivery of goods?
</p>
<p>
	 The costs of delivery always depend on the size and nature of the goods, delivery address, Country of delivery respectively and on the price list of the selected forwarder.
</p>
<p>
	 The costs of delivery of goods cannot be determined prior to the posting of an order. You will be informed about the costs prior to execution of the contract.
</p>
<p>
	 4.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; When will we deliver the goods to you?
</p>
<p>
	 The delivery period always depends on the delivery address, on the availability of the goods and on the selected method of delivery and payment.
</p>
<p>
	 We usually hand the goods on stock over to the forwarder within two business days from the payment being credited to our account or from the notice of payment through the Western Union payment points.
</p>
<p>
	 We will hand over the goods, which is not on stock, to the forwarder as soon as possible. We will notify you about the exact date.
</p>
<p>
	 Please note, that we cannot influence the period of delivery by external forwarders. Eventual complaints regarding the delivery period should be addressed to the forwarder.
</p>
<p>
	 The delivery of goods according to these terms and conditions means the moment of delivery of goods to you. If you refuse to accept goods without a good reason, the fact is considered neither a breach of obligations to deliver the goods&nbsp; by us, nor a withdrawal from contract by you.
</p>
<p>
	 4.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; How to accept the goods?
</p>
<p>
	 Upon accepting the goods, check the integrity of the packaging. If you detect any discrepancies, contact the forwarder and us immediately. If you refuse to accept a consignment with damaged packaging, it is not considered a unreasonable refusal of goods.
</p>
<p>
	 By signing a delivery note (or another similar document) you confirm, that the packaging of the consignment was intact.&nbsp; In such case complaints for damaged packaging of goods is no longer possible.
</p>
<p>
	 The moment of acceptance of goods (or moment when you were obliged to accept goods, but in conflict with the contract you did not), you assume the responsibility for accidental destruction, damage or loss of goods.
</p>
<p>
	 4.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; What happens is you do not accept the goods?
</p>
<p>
	 If, for reasons on your side, the goods have to be delivered repeatedly or by other than agreed method, you are obliged to pay the costs associated with such delivery.
</p>
<p>
	 In case you refuse to accept the goods unreasonably, we are entitled for compensation of cost of delivery associated with delivery and storage of the goods, as well as other costs we shall incur due to the goods not being accepted. These costs shall not exceed 50 Euro cents per each day of storage. The costs of storage may reach the total of EUR 20 or the value of the purchase price at maximum, if it is below EUR 20.
</p>
<p>
	 In such case we are also entitled to withdrawal from the contract.
</p>
<p>
</p>
<p>
	 5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; WITHDRAWAL FROM CONTRACT
</p>
<p>
	 5.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; How can you withdraw from a contract?
</p>
<p>
	 You can withdraw from a purchase contract in а period of 14 days from the day of acceptance of the goods; should the delivery be split into several parts, from the delivery of the last consignment. We recommend to send the the notice of withdrawal from a purchase contract to our mailing address or e-mail. The withdrawal from contract can be made on a <a href="/legal/contract_termination_form.php">sample form</a>. We will confirm the reception of the notice without unnecessary delay.
</p>
<p>
	 You do not have to give any reasons for withdrawal from contract.
</p>
<p>
	 5.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; What are the consequences of withdrawal from contract?
</p>
<p>
	 By withdrawal from contract the contract is cancelled from the beginning and it is considered as never executed.
</p>
<p>
	 In case a present has been provided with the goods, the deed of gift becomes ineffective upon withdrawal from the contract by any of the parties. Send the gift back together with the returned goods.
</p>
<p>
	 5.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; When is it impossible to withdraw from contract?
</p>
<p>
	 In accordance with Art. 1837 of the Civic Code it is impossible to withdraw, without limitation, from the following contracts:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; on delivery of goods, which was after delivery irretrievably mixed with other goods;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; on delivery of goods in an closed packaging, which you unpacked from packaging and for sanitary reasons is cannot be returned.
</p>
<p>
	 5.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; How will you return the goods to us?
</p>
<p>
	 You are obliged to return the goods to us within 14 days from the withdrawal from contract to our mailing address, to any business premises or to the address of our offices. Do not send the goods COD. We are not obliged to accept goods sent COD.
</p>
<p>
	 Returned goods have to be free of damage, wear and tear and unsoiled, and, if possible, in original packaging.
</p>
<p>
	 We recommend to include with the goods:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a copy of the delivery note and invoice, if these documents were issued, or another document of the purchase of goods;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; written notice of withdrawal from the contract (on our form or otherwise) and selected method of money return (transfer to account, personal acceptance of cash or postal order or otherwise). Include a mailing address, phone and e-mail in the notice.
</p>
<p>
	 Failure to present any of the above.mentioned documents does not prevent us from accepting your withdrawal from contract in compliance with legal terms.
</p>
<p>
	 5.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; When will you receive your money?
</p>
<p>
	 We will return all received funds to you within 14 days from the withdrawal from contract. However, note, that we are not obliged to return to you any funds, until you return the goods or document, that you dispatched the goods to us.
</p>
<p>
	 Along with the purchase price you are entitled to return of any costs of delivery to you. If you selected other than the least expensive method of delivery of goods, offered by us, we will return the costs of delivery of goods to you in the amount matching the least expensive method of delivery of goods.
</p>
<p>
	 We will return funds:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; by the same method by which we received it, or
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; by a method you will request;
</p>
<p>
	 Along with the above-mentioned methods we can always return funds to a bank account designated by you or to an account, from which the funds were transferred for the payment of the purchase price (if you fail to designate any account within ten days from the withdrawal from contract). By accepting these Terms and Conditions you express your consent with the transfer of funds according to the last sentence under the condition, that because of it you will not incur any further cost.
</p>
<p>
	 Costs associated with dispatched of returned goods to our address &nbsp;are born by you, even in case the goods, because of its nature, cannot be returned by a regular mail.
</p>
<p>
	 5.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; What if the goods were damaged?
</p>
<p>
	 Pack the goods for shipping into an appropriate packaging to prevent any damage or destruction. The purchase price and the costs of delivery of goods substantially damaged or destroyed during transport due to inappropriate packaging cannot be returned (or its part representing the damage).
</p>
<p>
	 If we find out the goods returned by you is damaged, worn, soiled or partially consumed, we are entitled to require damages from you. We can set the damages unilaterally off against your claim for return of the purchase price and costs of delivery of goods, i.e. you will be returned only the amount reduced by the damage.
</p>
<p>
	 5.7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; When can we withdraw from contract?
</p>
<p>
	 We reserve the right to withdraw from contract in the following cases:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; due to technical error obviously incorrect price of goods was published on the web portal (Art. 2.4 herein);
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; goods cannot be delivered under the original conditions for objective reasons (especially if the goods is no longer manufactured, supplier ceased to supply to CZ etc.).
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; execution becomes objectively impossible or illegal.
</p>
<p>
	 In case any of the above-mentioned circumstance occur, we will inform you about our withdrawal from contract without delay. The withdrawal becomes effective towards you in the time of delivery to you.
</p>
<p>
	 If you paid the purchase price in full or in part, we will return the received amount by a cashless transfer to an account you will specify for this purpose, or from which the payment was executed. We will return the money within five say from the withdrawal from purchase contract.
</p>
<p>
</p>
<p>
	 6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; RIGHTS FROM FAULTY PERFORMANCE
</p>
<p>
	 Your rights from a faulty performance are governed by generally binding legislation (especially articles 1914 to 1925 and 2099 to 2117 and 2158 to 2174 of the Civic Code).
</p>
<p>
	 Upon execution of right from faulty performance we will proceed in compliance with our&nbsp;<a href="/legal/complaint_guidelines.php">Complaint Guideline</a>. Prior to sending a complaint, familiarize yourself with the Complaint Guidelines, in order to process the complaint as fast as possible to your satisfaction.
</p>
<p>
</p>
<p>
	 7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; FINAL PROVISIONS
</p>
<p>
	 7.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; What authorization to our business activities we have and who supervises it?
</p>
<p>
	 We are authorized to sell goods based on a business license. Our activity is not subject to any other authorization.
</p>
<p>
	 The applicable trade authority performs trade supervision within its jurisdiction. Inspection of compliance with legislation regarding technical requirements on goods and safety is performed by Czech Trade Inspection (Česká obchodní inspekce <a href="http://www.coi.cz/)">http://www.coi.cz/)</a>. Czech Trade Inspection performs also inspection of compliance with legislation for consumer protection. The rights of consumers and their interest groups and other entities of their protection.
</p>
<p>
	 7.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; How we process complaints?
</p>
<p>
	 We process eventual complaints through our contact e-mail. You can also contact the entities mentioned in Article 7.1. In the relationship with our customers we are not bound by any Codes of conduct, nor do we observe such codes.
</p>
<p>
	 7.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; What you should know?
</p>
<p>
	 The contract is executed using means of remote communication (especially the Internet). Costs of the use of remote communication means (especially costs of Internet connection or phone calls) are born by us. These costs are not different from usual rates.
</p>
<p>
	 Unless agreed otherwise, all correspondence, relating to the contract, takes place between us in writing, either by sending an e-mail, registered mail or by personal delivery. We will deliver to the electronic mail address, mentioned in the purchase order or in your user's account.
</p>
<p>
	 In case that certain provision herein is (or becomes) invalid, ineffective or unenforceable, it will be replaced by a provision, meaning of which is as close to the invalid, ineffective or unenforceable provision as possible. The invalidity, ineffectiveness or unenforceability of one provision does not affect the validity of other provisions. Changes or addenda to this contract (including the terms and conditions) can be performed only in writing.
</p>
<p>
	 In compliance with the Act No. 185/2001 Coll., Act on wastes, as amended, our Company ensures free reception of goods purchased by you, which has the nature of electrical equipment (hereinafter only "re-supply"). Resupply can be performed by giving the used electrical equipment to the contact address of our Company. Prior to giving the used electrical equipment to us, please, contact us.
</p>
<p>
</p>
<p>
</p>
<p>
	 These Terms and Conditions are valid and effective from 23.07.2015.
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>