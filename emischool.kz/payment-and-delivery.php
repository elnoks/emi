<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Школа ногтевого дизайна Екатерины Мирошниченко E.Mi проводит курсы по дизайну ногтей в Москве. Также осуществляем продажу оригинальных товаров для дизайна ногтей. Оплата и доставка");
$APPLICATION->SetTitle("Оплата и доставка");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Оплата и доставка</h1>
   </div>
 
  <br />
  <p><span style="color: rgb(102, 102, 102); font-family: 'Pt Sans', Arial, Helvetica; font-size: 13px; line-height: 18.5714302062988px; background-color: rgb(249, 249, 249);">Предоплата производится на карту Казкоммерцбанк или через QIWI терминалы.</span>

  <p>Если у вас возникли вопросы вы можете позвонить по тел. <?=$GLOBALS["partnerShop"]->getPhones();?> или написать на электронную почту <?=$GLOBALS["partnerShop"]->getEmail();?></p>
 
  <p>Внимательно ознакомьтесь с правилами оплаты и доставки! Нажимая кнопку «ОК» формы заказа, вы соглашаетесь с правилами оплаты и доставки.</p>

 
  <p><span style="font-family: 'Times New Roman', serif; font-size: 12pt; line-height: 115%;">Стоимость доставки - 2000 тг. (не зависимо от суммы заказа).</span> 
      <br />Отправка заказов осуществляется на следующий рабочий день после получения оплаты.<br />
      Доставка осуществляется через курьерскую службу EMS Kazpost по всей территории РК.
      <br />
      Для подтверждения наличия товара по заказу с Вами свяжутся наши операторы.</p>



   <div style="clear:both;"></div>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>