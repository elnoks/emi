<?
/**
 * Created by PhpStorm.
 * User: prgrant & A.Volobuev
 * Date: 30.03.2018
 * Time: 14:21
 */
class AmoEmi
{
    protected $token_info = array();
    private $cURL = null;
    private $user_login = "";
    private $user_hash = "";
    protected $link = "";

    public function __construct($account,$login,$hash)
    {
        if (!isset($hash)) exit();
        $this->link = $account;
        $this->user_login = $login;
        $this->user_hash = $hash;
        $this->cURL = curl_init();
        curl_setopt_array($this->cURL, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_REFERER => "emischool.com",
        ));
        if (file_exists(__CLASS__ . ".json")) $this->token_info = json_decode(file_get_contents(__CLASS__ . ".json"), true);
    }

    private function __checkToken()
    {
        if (!isset($this->token_info['auth']) || time() - $this->token_info["time"] >= 15 * 60) /* token timeout is 15 min */
            $this->__getToken($this->user_login, $this->user_hash);
    }

    private function __getToken($user_login, $user_hash)
    {
        curl_setopt($this->cURL, CURLOPT_POST, 1);
        curl_setopt($this->cURL, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookieamo.txt');
        curl_setopt($this->cURL, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookieamo.txt');
        curl_setopt($this->cURL, CURLOPT_URL, $this->link.'private/api/auth.php?type=json');
        curl_setopt($this->cURL, CURLOPT_POSTFIELDS, array("USER_HASH" => $user_hash, "USER_LOGIN" => $user_login));
        $result = json_decode(curl_exec($this->cURL));
        //var_dump($result);
        $this->token_info = array('auth' => dirname(__FILE__) . '/cookieamo.txt', "time" => time());
    }

    public function set_deals($deals){
		$this->__checkToken();
		curl_setopt($this->cURL, CURLOPT_POSTFIELDS, json_encode($deals));
		curl_setopt($this->cURL, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($this->cURL, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($this->cURL, CURLOPT_POSTFIELDS, json_encode($deals));
        curl_setopt($this->cURL, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookieamo.txt');
        curl_setopt($this->cURL, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookieamo.txt');
		curl_setopt($this->cURL, CURLOPT_URL, $this->link . "api/v2/leads");
		return curl_exec($this->cURL);
	}
}
?>
