<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("General Wholesale Terms and Conditions");
?><p>
	 These General Wholesale Terms and Conditions (hereinafter only "terms and conditions") apply to contracts signed through on-line E.MI - School of Nail Design by Ekaterina Mirochnichenko store, located at the web interface <a href="http://www.emischool.com">www.emischool.com</a> (hereinafter only "web interface"), between Company
</p>
<p>
	 E.Mi - International s.r.o., with offices at U božích bojovníků 89/1, 130 00, Prague 3 - Žižkov
</p>
<p>
	 ID No.: 24214647
</p>
<p>
	 VAT ID No.: CZ24214647
</p>
<p>
	 registered in the Registry of Companies, maintained at Municipal Court in Prague, Section C, file 189332
</p>
<p>
	 Mailing address: E.Mi - International s.r.o., Štefánikova 203/23, 150 00, Prague 5 - Smíchov
</p>
<p>
	 Phone number: 773,208,276
</p>
<p>
	 Contact e-mail: <a href="mailto:prague@emischool.com">prague@emischool.com</a>
</p>
<p>
	 as the Seller
</p>
<p>
	 and a businessperson or a legal entity
</p>
<p>
	 as the Buyer
</p>
<p>
	 (both commonly also only as "the Parties").
</p>
<p>
	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</p>
<p>
	 1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Opening clauses
</p>
<p>
	 1.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The terms and conditions define and specify basic rights and obligations of the Parties at the time of signing the contract on selling goods and other contract mentioned here (hereinafter also commonly "the contract") through the web interface.&nbsp;&nbsp;
</p>
<p>
	 1.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The stipulations of the terms and conditions are integral part of the contract. The stipulations &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; discrepant from the terms and conditions can be agreed in the contract. The discrepant agreements in the contract have precedence before the stipulations of the terms and conditions. The Seller can change or modify the wording of terms and conditions. The rights and obligations of the Parties are always governed by the wording of the terms and conditions, under which they originated. The rights and conditions of the Parties are also governed by the Terms and Conditions of use of the web interface , by the terms and conditions and the instructions on the web interface, especially during signing of the contract or the registration of the Buyer on the web interface and by the purchase order and its reception by the Seller. The relationship of the Parties in matters not stipulated herein is governed by legislation, especially by Act No. 89/2012 Coll., Civic Code as amended (hereinafter only "the Civic Code").
</p>
<p>
	 1.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The contract and the terms and conditions are in Czech. The Contract can be executed in Czech, unless the Parties expressly agree on another language.
</p>
<p>
	 1.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; These terms and conditions apply to Buyers, which are businesspersons, and to legal entities. Contracts executed with a consumer are not subject to these terms and conditions, but to the General Retail Terms and Conditions. In matters not stipulated in these wholesale terms and conditions the relationships are governed similarly by the terms and conditions stipulated in the General retail terms and conditions, except the stipulations for consumer protection.
</p>
<p>
	 1.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Buyer confirms, by sending a purchase order, that he/she is familiar with these terms and conditions.
</p>
<p>
	 2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Purchase order and contract execution
</p>
<p>
	 2.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The web interface lists goods including a description of the main features of individual items. The presentation of goods, provided on the web interface, is only informative and does not represent the Seller's proposal to execute a contract in the sense of Art. 1732 Sec. 2 of the Civic Code. After execution of the contract the Buyer should send the purchase order so it is received by the Seller and the Buyer performs the payment for goods by a method mentioned in Art. 4.3 herein.
</p>
<p>
	 The Buyer places an order through the web interface (by completing the form). Registration on the web interface is necessary for order placement. The conditions of registration are stipulated in Terms and Conditions of use of the web interface.
</p>
<p>
	 2.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Seller is not obliged to confirm the received order. Unconfirmed order is not binding for the Seller. The Seller is entitled to verify the order in case of a doubt about the authenticity and earnestness of the order. Unverified order can be rejected by the Seller.
</p>
<p>
	 2.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; After the confirmation the Seller informs the Buyer also about the costs of goods delivery by sending an invoice, in which the total purchase price of the good, including costs of delivery, will be stated. The Seller will send the invoice to the Buyer to an e-mail address stated in the order. Basic information on costs of delivery, or the invoice respectively, represents acceptance of the order by the Seller.&nbsp;
</p>
<p>
	 2.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The contract is executed when the Seller pays the purchase price of the goods, including the costs of delivery of the goods by a method stipulated in Art. 4.3 herein.
</p>
<p>
	 2.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If the Buyer places an order from a Country, other than Czech Republic, and a national distributor of goods sold by the Buyer operates in such Country, the Buyer's order will be transferred for execution by such distributor.&nbsp;
</p>
<p>
	 2.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; An order, yet unaccepted by the Seller, can be cancelled by the Buyer by phone or an e-mail. All orders accepted by the Seller are binding. Accepted order can be cancelled in case the Buyer does not agree with the amount of costs of delivery, either by phone or an e-mail. Otherwise an accepted order can be cancelled only in agreement with the Seller. In case of such cancellation by the Buyer the Seller is entitled to a cancellation fee amounting to 50% of the price of goods.
</p>
<p>
	 3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Delivery terms
</p>
<p>
</p>
<p>
	 3.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Seller is obliged to deliver the goods to the Buyer in an agreed manner, properly packed and equipped by necessary documents. The necessary documents are especially the user's manual, certificates and other documents, necessary for acceptance and use of the goods. Unless agreed otherwise, the documents are provided in Czech.
</p>
<p>
	 3.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In accordance with the Parties' agreement the Seller can ensure the transport of goods for the Buyer and insurance of goods during the transport. Prices of transport and insurance will be covered by the Buyer. The goods are considered delivered upon hand-over of goods to the forwarder. The hazard of damage of the goods is transferred to the Buyer upon the delivery of goods. The delivery period always depends on the distance to the delivery address, the target state respectively, on its accessibility and on the chosen payment method.
</p>
<p>
	 Goods on stock is usually handed over by the Seller to the forwarder within two business days from crediting of the payment to the Seller's account or from a notice of payment through Western Union payment locations.
</p>
<p>
	 3.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The deadline for shipment of goods may be stated in the goods' details on the web interface. The Seller will hand goods, which is not in stock, over to the forwarder as soon as possible.
</p>
<p>
	 3.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The buyer is obliged, prior to acceptance of the goods, to check the integrity of the packaging and to report the eventual defects to the forwarder without delay. The defects will be recorded in a protocol. If the protocol of defects is not prepared, the Buyer looses the entitlement arising from the goods' damaged packaging.
</p>
<p>
	 3.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In case the Buyer will not accept the goods without reason, the Seller is entitled, along with a compensation of costs associated with storage of goods and other costs, arising due to the Buyer not accepting the goods, also to a payment of contractual penalty amounting to 0.5 % of the price of goods per day for each day of default of the Buyer to accept the goods.
</p>
<p>
	 3.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Buyer is obliged to check the goods immediately after acceptance, especially to check the quantity of goods and its completeness. In case non-compliance is detected, he is obliged to notify the Buyer about it without unnecessary delay, but 2 business days from accepting the goods at the latest. The Buyer is obliged to document the detected defects in an appropriate manner and to send the documentation to the Seller together with the report of defects.
</p>
<p>
</p>
<p>
	 4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Payment conditions
</p>
<p>
	 4.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The web interface lists prices of goods including VAT. The Seller is entitled to provide the Buyer with discounts of these prices, especially in relation to quantity of ordered goods or previous purchases of the Buyer.
</p>
<p>
</p>
<p>
	 4.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In case of an obvious technical error on the side of the Seller when listing a price of goods or the amount of discount on the web interface or during placement of order, the Seller is not obliged to deliver the goods to the Buyer at this obviously incorrect price or with the obviously incorrect discount, even when the Seller accepted the Buyer's order under these Terms and Conditions. Correspondingly it applies in case the price or amount of discount, listed on the web interface or during the placement of order are no longer valid, or if the price or amount of discount change between the sending of the order and its reception.
</p>
<p>
</p>
<p>
	 4.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Buyer can pay the purchase price of the goods to the Seller, along with other methods of listed on the web interface, or agreed individually, by one of the methods listed below:
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; in cash through Western Union payment points;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; cashless prior to delivery of goods, transfer to bank account of the Seller (the Seller will provide the Buyer with information for payment)
</p>
<p>
	 The Seller is entitled to refuse payment for goods after delivery. This method of payment is usually reserved for regular customers. Unless agreed otherwise, the invoice is attached with the goods.
</p>
<p>
	 4.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In case of payment in cash through Western Union payment points and in case of cashless payment the price is due within seven days from dispatch of information on costs of delivery o goods, invoice respectively (i.e. acceptance of purchase order). The price is, in case of cashless payment, considered as paid upon crediting of the amount to the bank account of the Seller, in case of cash payment at a Western Union payment point upon confirmation of the payment by Western Union. The Seller has to be&nbsp; notified about the payment through the Western Union payment point and the information for payment of the money has to be provided.
</p>
<p>
	 4.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In case of default of the Buyer with the payment of the price of goods the Seller is also entitled to suspend further agreed deliveries of goods until all past-due obligations of the Buyer are settled.
</p>
<p>
	 4.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Payment in Euros (EUR) is possible.
</p>
<p>
</p>
<p>
	 5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Withdrawal from Contract
</p>
<p>
	 5.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Seller is entitled to withdraw from the Contract until the acceptance of goods by the Buyer. In such case the Seller will return to the Buyer the purchase price in cashless transfer to the account provided for this purpose by the Buyer or to an account, from which the money was transferred to pay the purchase price (if the Buyer does not provide any within 5 days from the withdrawal from the contract).
</p>
<p>
</p>
<p>
	 5.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Seller is also entitled to withdraw from contract if the Buyer is in default with payment of the purchase price for more than 4 weeks.
</p>
<p>
</p>
<p>
	 5.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Buyer is entitled to withdraw from the contract if the Seller is in default with the delivery of goods for more than 4 weeks from the agreed delivery.
</p>
<p>
</p>
<p>
	 5.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Buyer is not entitled to withdraw from the contract for goods, which was delivered properly, in time and without defect.
</p>
<p>
</p>
<p>
	 5.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Withdrawal from a contract has to be executed in writing and, in case of contracts agreed electronically, also electronically. Withdrawal from contract is effective upon delivery of a withdrawal notice to the respective party.
</p>
<p>
	 5.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In case a present has been provided with the goods, the deed of gift becomes ineffective upon withdrawal from the contract by any of the parties.
</p>
<p>
</p>
<p>
	 6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rights from faulty performance
</p>
<p>
	 6.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The rights of the Buyer from a faulty performance are governed by generally binding legislation (especially articles 1914 to 1915 and 2099 to 2117 of the Civic Code).
</p>
<p>
</p>
<p>
	 7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Protection of Seller's trade secrets and business policy
</p>
<p>
	 7.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; During the negotiation of contract and its performance the Buyer may be informed about matters, which are designated as confidential or confidentiality of which arises form its nature. With respect to this information the Buyer agrees especially:
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; to hold as confidential;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; not to provide it without the Seller's consent to another person;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; not to use it for other purpose than for performance of contract;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; not to use it in any damaging way.
</p>
<p>
	 7.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Buyer also agrees not to create copies of documents, provided by the Seller, without the Seller's consent.
</p>
<p>
</p>
<p>
	 8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Final provisions
</p>
<p>
	 8.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If the relationship associated with the use of the web interface or the legal relationship, established by a contract, contains an international (foreign) element, the Parties agree that the relationship is governed by the Czech law.
</p>
<p>
	 8.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Should any provision of the Terms and Conditions be or become invalid or ineffective or unenforceable, the invalid provision will be replaced by a provision, meaning of which is as close to the invalid provision as possible. The invalidity or ineffectiveness or unenforceability of one provision does not affect the validity of other provisions. Changes and addenda of a contract or the Terms and Conditions require written form.
</p>
<p>
</p>
<p>
	 These Terms and Conditions are valid and effective from 23.07.2015.
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>