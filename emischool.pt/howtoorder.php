<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("How to make an order");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>How to make an order.</h1>
   </div>
 </div>
 
<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

 
<div class="bx_page"> 
  <p>1.	Só os pedidos feitos via e-shop serão aceites. As encomendas efetuadas através de telefone ou e-mail não serão processados.
    <br />
   </p>
 
  <p>2.	Para evitar quaisquer problemas com a entrega, pedimos-lhe para verificar as suas informações pessoais de contato antes de colocar uma ordem. Dados necessários: nome e sobrenome, número de telemóvel incluindo o prefixo do país, e-mail e endereço de entrega (rua, número, cidade, CP, país).
    <br />
   </p>
 
  <p>3.	Em caso de eventuais erros nos dados de contacto a Empresa não é responsável por quaisquer despesas adicionais relacionadas com a entrega no endereço errado.
    <br />
   </p>
 
  <p>4.	Nós aceitamos e processamos encomendas de todos os países, exceto a Rússia, Ucrânia, Cazaquistão, Bielorússia, Arménia e Azerbaijão. As encomendas dos países mencionados acima serão redirecionadas para a e-shop em www.emi-school.ru.</p>
 
  <p>5.	Quando terminar a encomenda, o Cliente recebe uma notificação automática por e-mail com resumo da encomenda, sem custos de envio. <b>Atenção! Por favor, não efetuar o pagamento com base na notificação automática!</b> A fatura com o montante total, incluindo os custos de envio será enviada no prazo de 2 dias úteis. Os detalhes de pagamento serão enviados na carta de acompanhamento. A fatura será cobrada em euros ou em dólares, se necessário. Por favor, informe o nosso gerente da e-shop sobre o pagamento em dólares com antecedência.
    <br />
   </p>
 
  <p>6.	A fatura deve ser paga no prazo de 7 dias úteis após a data de emissão. No caso de não pagamento dentro do prazo será automaticamente cancelada.
    <br />
   </p>
  <p>7.	Se não lhe for possível efetuar o pagamento dentro do prazo permitido entre com antecedência em contato com o nosso gerente da e-shop via e-mail portugal-sale@emischool.com ou telefone +351 91 803 7777.
    <br />
   </p>
  <p>8.	Antes de pagar a fatura por favor, verifique se os produtos e as quantidades na fatura estão corretos.
    <br />
   </p>
  <p>9.	A embalagem e o envio serão processados no prazo de 3 dias úteis após o recebimento do pagamento. 
    <br />
   </p>

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>