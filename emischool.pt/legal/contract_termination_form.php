<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Contract termination form");
?><p>
	(complete this form and send it back only in case you want to terminate the contract)
</p>
<p>
</p>
<table border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
	<td width="614" style="width: 460.6pt;">
		<p>
			 Contract termination notice
		</p>
		<p>
			 - Addressee E.Mi - International s.r.o.
		</p>
		<p>
			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Štefánikova 203/23
		</p>
		<p>
			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 150 00 Prague 5 - Smíchov
		</p>
		<p>
		</p>
		<p>
			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="mailto:prague@emischool.com">prague@emischool.com</a>
		</p>
		<p>
		</p>
		<p>
			 - I declare/we declare (*) that hereby I/we (*) terminate the purchase contract of the following goods:
		</p>
		<p>
		</p>
		<p>
			 &nbsp;- Date of order (*)/date of receipt (*)
		</p>
		<p>
		</p>
		<p>
			 - Name and Surname of the consumer/consumers
		</p>
		<p>
		</p>
		<p>
			 - Consumer's/consumers' address
		</p>
		<p>
		</p>
		<p>
			 - Consumer's/consumers' signature (only when sent in hard copy)
		</p>
		<p>
		</p>
		<p>
			 - Date
		</p>
		<p>
		</p>
		<p>
			 (*) Strike out the inapplicable
		</p>
	</td>
</tr>
</tbody>
</table>
<p>
</p>
<p>
	 &nbsp;
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>