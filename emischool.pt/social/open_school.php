<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("How to open the E.Mi school representative office");
?> 
<div class="bx_page"> 
  <p style="text-align: center;"><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" border="0" alt="765.jpg" width="800" height="105"  /><b>
      <br />
    </b></p>

  <p><b>
      <br />
    </b></p>

  <p> <b>Cooperação com a escola de design de unhas Ekaterina Miroshnichenko é uma oportunidade para obter uma solução rentável chave-na-mão. Pode obter uma gama completa de tecnologias: design, gestão e promoção. A Escola de design de unhas Ekaterina Miroshnichenko apresenta sempre as imagens mais recentes, solicitadas, elegantes e bem reconhecidas.</b></p>
 
  <p> <b>Seja nosso representante local e vamos ensiná-lo a ser tão bem sucedido e criativo como nós somos! Hoje, a Escola de design de unhas Ekaterina Miroshnichenko é reconhecida em todo o mundo: prestigiados prémios em competições internacionais, seminários na Alemanha e Itália e a abertura de um escritório internacional na Europa.
    <br />
   Você pode adquirir o direito exclusivo de gerir um negócio altamente rentável e interessante sobre a educação e a formação avançada de designers de unhas, suportada pela Escola da marca de design de unhas.</p>
 
  <p> <b>Junte-se aqueles que seguem as tendências! </b> A escola de Nail design Ekaterina Miroshnichenko criou tanto as tecnologias únicas de design reconhecido como formação.
    <br />
   <b>Criatividade e beleza são possíveis connosco!</b>
    <br />
   <b>As vantagens da Escola de design de unhas Ekaterina Miroshnichenko:</b> </p>

  <ul> 
    <li>serviços de formação são alvo de grande procura. Mais de 500 formadoras master de todas as partes da Rússia e no estrangeiro aprendem programas diferentes anualmente;</li>
   
    <li>os materiais <a href="/catalog/" >E.Mi</a> são usados durante a educação;</li>
   
    <li>o nível internacional da empresa. Há um escritório na Europa (Praga);</li>
   
    <li>os programas educacionais são baseados em métodos únicos desenvolvidos e patenteados por Ekaterina Miroshnichenko,nomeada  campeã do mundo em design de unhas segundo indicação da OMC (Paris, 2011);</li>
   </ul>
 
  <p></p>
 
  <p> <b>nós fornecemos aos nossos representantes todos os materiais necessários para o trabalho de sucesso do departamento educacional.
      <br />
     A abertura de uma representação da Escola de design de unhas proporciona-lhe o seguinte:</b> </p>

  <ol> 
    <li><b>O direito exclusivo</b> de trabalhar com o nome da Escola de design de unhas Ekaterina Miroshnichenko e fornecer serviços educacionais no território designado por contrato. </li>
   
    <li><b>Os programas educacionais </b>desenvolvidos por Ekaterina Miroshnichenko. O centro educacional economiza tempo e finanças no desenvolvimento de currículo, métodos de treinamento e materiais educativos e manuais. Tudo isto permite foco na busca de estudantes e compensar os gastos com abertura no menor período de tempo possível.</li>
   
    <li><b>Um sistema unido de certificação. </b>O representante garante o direito de entregar aos seus alunos, certificados com holograma da Escola de design de unhas Ekaterina Miroshnichenko.</li>
   
    <li><b>O direito a um desconto exclusivo sobre os produtos <a href="/catalog/" >E.Mi</a>.</b></li>
   
    <li><b>Apoio à promoção.</b> A escola de autor coloca todas as informações sobre os seus representantes em revistas líderes da especialidade, bem como no seu próprio catálogo de publicidade e site oficial; suporte nas exposições regionais e conferências sobre unhas e produtos, fornece layouts de publicidade para publicar em periódicos locais.</li>
   </ol>
 
  <p></p>
 
  <p> <b>A cooperação como um representante oficial da Escola de design de unhas por Ekaterina Miroshnichenko e um distribuidor oficial da marca Emi.</b>
    <br />
   O representante oficial da Escola de design de unhas Ekaterina Miroshnichenko tem o direito de preferência no contrato de distribuição da marca E.Mi no território, indicado nos termos do contrato. Neste caso, requisitos adicionais e termos de cooperação de distribuição, bem como obrigações sobre o preço de compra com o aumento de desconto relevante sobre os produtos da marca EMI são adicionados às exigências dos representantes oficiais da Escola de design de unhas Ekaterina Miroshnichenko.</p>
 
  <p> <b>Os requisitos para abrir uma representação da Escola de design de unhas Ekaterina Miroshnichenko:</b> </p>

  <ol> 
    <li>Um(a) instrutor(a) talentosa(o), capaz de copiar com precisão as técnicas de Ekaterina Miroshnichenko. Será uma candidata(o) para fazer os primeiros cursos como aprendiz e se ele ou ela tiverem uma qualificação adequada de acordo com exigências de Ekaterina Miroshnichenko, então poderá participar num um curso especial de instrutores.</li>
   
    <li>Instalações separadas para um centro de formação não inferior a 16 metros quadrados. Colocação de placards com a marca de acordo com as especificações e estilo da Escola de design de unhas Ekaterina Miroshnichenko.</li>
   
    <li>Existência de entidade jurídica que tenha o direito de prestar serviços de formação (empresário em nome individual, instituição de ensino não-governamental, instituições privadas de ensino, etc.)</li>
   
    <li>Capital de investimento para fazer a primeira compra de produtos, equipar o centro de formação e pagar as formações e estudos de um instrutor.</li>
   
    <li>Compromisso do Representante em atrair novos alunos com base nas tecnologias desenvolvidas.</li>
   </ol>
 
  <p></p>
 
  <p> <b>Se você está realmente interessado na abertura de uma representação da Escola, na sua região ou país, em primeiro lugar, é necessário:</b> </p>

  <ol> 
    <li>saber se já existe um representante na sua região ou país. Poderá obter mais informações em Contatos no nosso site ou ligue para o número (863) 255 73 67. Se já existe um representante na sua região, teremos de recusar.</li>
   
    <li>Enviar fotos de tips de unhas com designs de Ekaterina Miroshnichenko para o nosso email sviridova@emi-school.ru (por exemplo, Arranjos Florais Sofisticados, Morangos, Framboesas, Cereja, etc.). É necessário para a sua nomeação como um instrutor, porque seu estilo deve corresponder em absoluto aos requisitos de Ekaterina Miroshnichenko. Você deve copiar com precisão os desenhos e técnica dela.</li>
   
    <li><b>Saber os custos associados à formação ligando para o número: +420 722935746. A formação de instrutor é realizada em duas etapas: como aprendiz e como instrutor. A formação é realizada somente no centro de formação em Rostov-on-Don. O custo depende diretamente da quantidade de cursos.</li>
   
    <li><b>Saber qual o volume de compras trimestral ligando para o número +420 722935746.</b> o volume de compras é estabelecido no contrato.</li>
   
    <li>Estar pronto para formações avançadas regulares. O contrato de instrutor é assinado pelo prazo de um ano. Todos os anos, o instrutor tem de revalidar a qualificação em Rostov-on-Don e assina um contrato para o ano seguinte.</li>
   </ol>
 
  <p></p>
 
  <p> <b>Para obter informações adicionais sobre a abertura de uma representação da Escola de design de unhas Ekaterina Miroshnichenko, por favor, ligue para o escritório internacional:
      <br />
Korbut Maria, korbut@emischool.com, +420 722935746</b> </p>
 
  <br />
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>