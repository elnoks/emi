<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Tips & tricks");
?> 
<div class="bx_page"> 
<div class="h1-top">
		<h1>Tips & tricks</h1>
    </div>
 <br>
  <p> <b>Como se tornar um representante oficial da marca E.Mi e da escola de design de unhas Ekaterina Miroshnichenko?</b> 
    <br />
A empresa E.Mi oferece soluções de negócios prontos para o lançamento, de crescimento rápido e de negócios interessantes. Você pode abrir uma representação da escola de design de unhas Ekaterina Miroshnichenko no seu país; tornar-se um distribuidor oficial da marca E.Mi ou um revendedor. Informações detalhadas podem ser encontradas na seção <a href="/social/open_school.php" >"Como abrir uma representação da Escola"</a>
</p>
 
  <p> <b>Existe uma escola de design de unhas na minha cidade/país?</b> 
    <br />
A Escola de design de unhas Ekaterina Miroshnichenko na sua cidade/país pode ser encontrada na seção <a href="/contacts/" >"Contatos"</a>.</p>
 
  <p> <b>Onde posso comprar os produtos E.Mi?</b> 
    <br />
Você pode comprar os produtos E.Mi no nosso site na seção "Produtos" (ver secção "Como encomendar" para instruções detalhadas) ou num ponto de venda. Você pode saber mais sobre pontos de venda na secção <a href="/contacts/" >"Contactos"</a>.</p>
 
  <p> <b>Posso obter qualificações básicas, tais como manicure, pedicure na Escola de design de unhas Ekaterina Miroshnichenko?</b> 
    <br />
A escola de design de unhas Ekaterina Miroshnichenko é especializada em design de unhas e conetrução.</p>
 
  <p> <b>Posso obter cursos de formação na escola de design de unhas Ekaterina Miroshnichenko, sem experiência como técnica de unhas?</b> 
    <br />
Cursos de design de unhas são formações avançadas, é por isso que recomendamos que obtenha habilidades elementares em qualificações básicas (manicure,pedicure, extensão de unhas). Isto tornará mais fácil praticar design de unhas.</p>
 
  <p> <b>Com que curso é melhor começar?</b> 
    <br />
Recomendamos que comece com cursos básicos: Art Painting – basic course and E.Mi design technologies onde vai aprender como trabalhar com géis coloridos e ganhar capacidades de desenho. Após estes cursos será fácil de aprender no segundo e terceiro níveis de complexidade, como Pintura Zhostovo, Pintura Chinesa, Arranjos Florais Sofisticados, Abstrações Complexas, etc.</p>
 
  <p> <b>Onde posso encontrar o calendário de cursos na escola de design de unhas Ekaterina Miroshnichenko?</b> 
    <br />
Pode encontrar mais detalhes sobre o calendário dos cursos na seção <a href="/courses/#schedule" >"Calendário"</a>.</p>
 
  <p> <b>Qual é a idade mínima dos estudantes da escola de design de unhas Ekaterina Miroshnichenko?</b> 
    <br />
São aceites formandas com idade a partir dos 15 anos.</p>
 
  <p> <b>Posso aplicar as tintas gel E.Mi em verniz gel de outra marca?</b> 
    <br />
Sim, as tintas de gel E.Mi podem ser aplicadas sobre verniz gel, mas deve lembrar-se que a maioria dos vernizes de gel são removidos por um removedor especial, enquanto as tintas gel E.Mi são tiradas com lima. É por isso que recomendamos que faça um desenho ou um enfeite numa pequena superfície da unha, coberto com verniz de gel. Por exemplo, areia aveludada e pedras líquidas ou desenhos em 3D vintage.</p>
 
  <p> <b>Posso aplicar as tintas de gel E.Mi numa unha natural?</b> 
    <br />
Antes de aplicar a tinta de gel E.Mi, deve aplicar uma camada com gel, acrílico ou verniz de gel. Não é permitido aplicar a tinta gel E.Mi na unha natural.</p>
 
  <p> <b>Qual é a diferença entre a tinta de gel de E.Mi e EMPASTA?</b> 
    <br />
As principais diferenças são a consistência e aderência residual. EMI EMPASTA é mais espesso, por isso, uma pincelada ou uma linha são mais precisas e a textura EMPASTA permite fazer modelos e desenhos em 3D. Além disso, EMPASTA não tem goma residual, logo tanto pode ser aplicado por cima como por baixo gel finalizante. O tempo de secagem entre camadas é de apenas 2-5 segundos. Isso ajuda mais rapidamente a fazer desenhos com uma grande quantidade de elementos. As tintas de gel E.Mi são mais líquidas, eles são ideais para arranjos.</p>
 
  <p> <b>Qual é a diferença entre o polímero de Pedra Líquida (Liquid Stone polymer) e gel de pedras líquidas (Liquid Stone gel?</b> 
    <br />
Gel "Liquid Stone" é um material adicional para desenhar "pedras líquidas". Ajuda a imitar pedras preciosas de vários tamanhos e alturas em unhas e não requer top coat/ isto permite poupar o tempo de execução. Gel "Liquid Stone" é transparente e não pode misturá-lo com pigmentos. "Liquid Stone" polymer imita pedras coloridas, pois pode misturá-lo com pigmentos e deve ser revestida com top gel com goma.</p>
 
  <p> <b>Como usar gel EMPASTA Openwork Silver para criar uma moldura no desenho?</b> 
    <br />
EMPASTA Openwork Silver uma textura transparente, é por isso que deve pressionar uma pequena quantidade de tinta na paleta e misturar bem; depois deixe a tinta acamar por alguns dias para ter uma textura espessa apropriada para desenhar a moldura. Não é possível trabalhar com EMPASTA fresco. Quanto mais EMPASTA é mantido, melhor fica. O EMPASTA não seca, ele acama e tornar-se ideal para trabalhar.</p>
 
  <p> <b>Que pincéis são necessários para trabalhar com os materiais E.Mi?</b> 
    <br />
Recomendamos 3 conjuntos de pincéis para trabalhar com os materiais E.Mi: Conjunto de pincéis para Art Painting, Conjunto de pincéis para One Stroke e conjunto de pincéis universal.</p>
 
  <p> <b>Como devo cuidar dos pincéis?</b> 
    <br />
Não use líquidos que contêm acetato etílico para limpeza, é suficiente usar um guardanapo molhado. Manter pincel afastado dos raios ultravioletas dispersos.</p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>