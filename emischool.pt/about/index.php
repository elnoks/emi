<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("About company");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>About company</h1>
   </div>
 
  <div class="bx_page" style="text-align: center;"><img src="/upload/medialibrary/f7c/f7cb52963256ebe4b40f9af5c03a36d2.jpg" title="56.jpg" border="0" alt="56.jpg" width="1140" height="300"  /> 
    <br />
   </div>
 
  <br />
 
  <p>A marca E.Mi apresenta milhares das mais recentes e exclusivas soluções prontas a usar, desenvolvidas por Ekaterina Miroshnichenko, campeã mundial de design de unhas, bem como materiais e acessórios para Nail Art criados com a sua colaboração.</p>
 
  <p>A missão da E.Mi é ajudar todas as técnicas de unhas a realçar o seu potencial, fazê-las sentirem-
	se uma artista e torná-las especialistas em moda! Poderá aprender a criar os fantásticos designs  E.Mi na escola de design de unhas Ekaterina Miroshnichenko.
  </p>
 
  <p>A companhia E.Mi está a desenvolver rapidamente um negócio de sucesso e você pode fazer parte dele! Atualmente, a E.Mi inclui cinquenta representantes oficiais da Escola de design de unhas Ekaterina Miroshnichenko na Rússia, Europa e Ásia, distribuidores da marca E.Mi e cem pontos de venda por todo o mundo.</p>
 
  <h2>SOLUÇÕES PRONTAS A USAR&amp;UNHA&amp;MODA</h2>
 
  <p><img src="/upload/medialibrary/cd4/cd48dcf71294554d8839eab0a34a4986.jpg" title="98.jpg" border="0" alt="54.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Alta costura é uma arte real. Deslumbra, incentiva a fazer experiências e cria antecipação por cada nova temporada, que sempre espanta e surpreende.</p>
 
  <p>As soluções prontas a usar da E.Mi são a alta costura na nail art. Durante o ano, apresentamos algumas coleções exclusivas desenvolvidas por Ekaterina Miroshnichenko, campeã mundial de design de unhas na categoria Fantasia, de acordo com o MLA (Paris, 2010), duas vezes campeã europeia (Atenas, Paris, 2009), júri internacional e a fundadora da escola original de design de unhas.
</p>
 
  <p>Cada coleção de Ekaterina é uma interpretação original das últimas tendências, incorporadas no design de unhas e ajustadas à utilização em salões de beleza: incrivelmente belas e únicas à primeira vista, eles são fáceis de conseguir, após um curso de formação ou através de vídeos master-class.</p>
 
  <p>Sucessos como pintura Zhostovo, pintura One Stroke, Imitação de pele de réptil, efeito Crackled, Impressões Étnicas, areia de Veludo e pedras líquidas e outros estão entre a coleção de Ekaterina Miroshnichenko. Sinta-se de imediato inspirada pela gama completa de design da coleção de Ekaterina Miroshnichenko!</p>
 
  <h2>PRODUTOS PARA DESIGN DE UNHAS</h2>
 
  <p><img src="/upload/medialibrary/6e3/6e3b48a3e4405516b24de034f7ca5b60.jpg" title="6542.jpg" border="0" alt="6542.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Qualquer técnica de unhas, tendo usado produtos E.Mi, pode afirmar com segurança: “Estes produtos foram feitos para mim!”</p>
 
  <p>O segredo deste amor à primeira vista é simples: Ekaterina Miroshnichenko participa pessoalmente na criação de cada produto, novas cores e acessórios. Uma designer de unhas, campeã mundial, uma respeitada especialista em nail art faz os seus próprios produtos com que pretende trabalhar. É por isso que os produtos E.Mi satisfazem todos os requisitos das designers de unhas, têm em conta todas as particularidades de trabalho, permitindo poupar tempo de execução e abrir espaço à criatividade.</p>
 
  <p>Produtos únicos como EMPASTA, GLOSSEMI, TEXTONE, PRINCOT bem como géis de cor, foil de desenho com cores de topo, uma grande variedade de materiais de decoração e acessórios são apresentados na E.Mi. Encontre já o produto de que precisa!</p>
 
  <h2>ESCOLA DE DESIGN DE UNHAS DE EKATERINA MIROSHNICHENKO</h2>
 
  <p><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" border="0" alt="765.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Se sonha com uma profissão interessante e criativa, se quer deixar as suas clientes com um sorriso e fazê-las voltar, obtenha os cursos de formação na Escola de design de unhas de Ekaterina Miroshnichenko.</p>
 
  <p>A Escola de design de unhas Ekaterina Miroshnichenko oferece programas de educação originais para designers de unhas. Os métodos únicos de Ekaterina Miroshnichenko são a base dos cursos de design de unhas.</p>
 
  <p>Cursos de design estão divididos em 3 níveis de complexidade e outros cursos adicionais. Aquelas que aspiram a conquistar o Olimpo e participar em competições de nail art, a escola de design de unhas oferece cursos de treino especial. Várias estudantes da Escola de design de unhas Ekaterina Miroshnichenko tornaram-se vencedoras de competições regionais e federais de design de unhas.</p>
 
  <p>Até à presente data, existem 50 representantes oficiais da Escola de design de unhas Ekaterina Miroshnichenko e todas trabalham segundo os padrões uniformes mais elevados. Deixe os seus sonhos tornarem-se realidade e torne-se uma verdadeira especialista em nail art – escolha o seu próprio programa educacional agora mesmo!</p>
 
  <p>Encontre-o na escola mais próxima, no nosso site e obtenha o seu sonho.!</p>
 
  <h2>REPRESENTANTES OFICIAIS</h2>
 
  <p><img src="/upload/medialibrary/060/060926d856ef3ef2bcc0eef18fa948cc.jpg" title="7645.jpg" border="0" alt="7645.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>A marca E.Mi e a Escola design de unhas de Ekaterina Miroshnichenko tem as últimas linhas de produção mais populares e de grande procura. Atualmente, os representantes oficiais da Escola de design de unhas Ekaterina Miroshnichenko operam atualmente com sucesso na Russia, Ucrânia, Kazaquistão, Bielorússia, Itália, Portugal, Roménia, Chipre, Alemanha, França, Lituânia, Eslováquia, Coreia do Sul e Emirados Árabes Unidos. Os produtos E.Mi podem ser adquiridos em todo o mundo.</p>
 
  <p>Você também pode fazer parte de uma marca internacional de sucesso e obter prontas soluções de negócios - tornar-se um representante oficial da Escola de Nail design Ekaterina Miroshnichenko ou um distribuidor oficial da marca E.Mi na sua cidade, região ou país. Representantes oficiais da escola de design de unhas por Ekaterina Miroshnichenko usufruem das seguintes vantagens:
<ul> 
	<li>
a possibilidade de representar exclusivamente a marca da escola de design de unhas Ekaterina Miroshnichenko na sua região;
	</li>
	<li>
o direito de ensinar cursos originais Ekaterina Miroshnichenko em design de unhas;
	</li>
	<li>
processos de negócios completos para abertura e desenvolvimento do seu negócio;
	</li>
	<li>
manutenção de aperfeiçoamento de designs e treino pessoal;
	</li>
	<li>
apoio promocional corporativo;
	</li>
	<li>
lucrativos decisões sobre de gestão para obtenção de lucros em toda a gama de produtos E.Mi.
	</li>
</ul></p>
 
  <p>Para se tornar um representante oficial da escola de design de unhas Ekaterina Miroshnichenko ou um distribuidor oficial da marca E.Mi, poderá preencher um formulário de inscrição no nosso site ou ligue para +420 722935746 Korbut Marina, korbut@emischool.com</p>

  <p>
    <br />
  </p>

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>