<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оплата и доставка");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Payment and delivery</h1>
   </div>
 
  <br />

  <p class="MsoListParagraphCxSpFirst" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.<span style="font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">      </span></span></b><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Preços.<o:p></o:p></span></b></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.1<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Todos os preços da e-shop incluem 23% de IVA.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.2<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Para todos as encomendas superiores a 500 euros será válido um desconto de 5% (desconto não é acumulável e é válido apenas por cada encomenda). <o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.3<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Os preços não incluem despesas de envio.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt 21.3pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">1.4<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">  </span></span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;">Todos as taxas e outras despesas possíveis, serão pagas pelo Cliente de acordo com as leis do país do cliente.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpLast" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif; background: white;"><o:p> </o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><b><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">2. Pagamento<o:p></o:p></span></b></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">2.1 A Todos os pagamentos serão efetuados com base na fatura emitida.<o:p></o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">2.2 Antes de pagar a fatura por favor, verifique se os produtos e quantidades estão corretas.<o:p></o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">2.3 Aceitamos pagamentos via:<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpFirst" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; font-family: Symbol;">·<span style="font-stretch: normal; font-size: 7pt; font-family: 'Times New Roman';">         </span></span><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">Transferência bancária para a nossa conta.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpMiddle" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; font-family: Symbol;">·<span style="font-stretch: normal; font-size: 7pt; font-family: 'Times New Roman';">         </span></span><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;">Cartão de crédito.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpLast" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; font-family: 'Times New Roman', serif;"> </span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><b><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3. Condições de envio.<o:p></o:p></span></b></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.1 Os produtos serão enviados internacionalmente via TNT.<o:p></o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.2 Os custos de envio não são fixos e dependem do peso do pacote e destino.<o:p></o:p></span></p>
 
  <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">As despesas de transporte serão contabilizadas e mostradas na fatura emitida.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpFirst" style="margin-left: 18pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.3.</span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">O embalamento e transporte serão processados no prazo de 3 dias úteis após o recebimento de pagamento.<o:p></o:p></span></p>
 
  <p class="MsoListParagraphCxSpLast" style="margin-left: 18pt; text-align: justify; text-indent: -18pt;"><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">3.4.</span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">Todos os termos de envio são estimados e dependem de destino da entrega.</span><span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
 <span lang="EN-US" style="font-size: 12pt; line-height: 115%; font-family: 'Times New Roman', serif;">Quando a encomenda é enviada, poderá verificar o estado da sua encomenda no site da transportadora TNT com o respetivo número de envio.</span> </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>