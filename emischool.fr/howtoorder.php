<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Comment faire une commande");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Achetez les produits E.Mi en ce moment en utilisant le service de commande en ligne.</h1>
   </div>
 </div>
 
<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

<div class="bx_page">
  <br />
</div>

 
<div class="bx_page"> 
  <p>1.	Inscrivez-vous sur le site.
    <br />
   </p>
 
  <p>2.	Passez dans la section «Produits» et ajoutez vos produits préférés dans votre panier en cliquant sur le bouton  «Ajoutez au panier», qui est situé à  droite de la photographie de la marchandise.  Si le produit a été ajouté au panier avec succès, le bouton change de couleur et passe au vert et il s’affichera «dans le panier». Ensuite, vous pouvez continuer vos achats.
    <br />
   </p>
 
  <p>3.	Quand vous avez terminé le processus d'achat, vérifiez l'état de votre commande dans le «Panier». Pour le faire, passez dans « Mon panier», qui est dans le coin supérieur droit de la page. Ici, vous pouvez régler l'ordre, changer le nombre de produits requis et voir le montant total de votre achat.
    <br />
   </p>

   <p>Si vous changez le nombre de produits,  en retirant ou ajoutant de nouveaux noms, cliquez  sur «Convert» pour voir le montant réel de l'achat.</p>
   
   <p><b>Attention ! La commande minimale -  40€</b></p>
	<p>Lors de la commande de produits exploite le système des réductions suivantes:</br>
	- Le montant de commande de 400€ – 5% de réduction,</br>
	- Le montant de commande de 600€ – 10% de réduction.
	</p>
<p>4. Pour passer votre commande, cliquez sur le bouton rose «Commandez», qui est situé sur le côté droit de l'écran. Choisissez la méthode de paiement et de livraison.</br>
Tel. Manager de la boutique en ligne: 06 19 57 02 83</br>
E-mail: france-sales@emischool.fr
</p>

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>