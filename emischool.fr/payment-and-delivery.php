<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetPageProperty("description", "Школа ногтевого дизайна Екатерины Мирошниченко E.Mi проводит курсы по дизайну ногтей в Москве. Также осуществляем продажу оригинальных товаров для дизайна ногтей. Оплата и доставка");
$APPLICATION->SetTitle("Paiement et livraison");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Paiement et livraison</h1>
   </div>
 
  <br />
 <span style="font-weight:bold;">Attention ! La commande de produits E.Mi est de minimum – 40€.</span> 
  <p>

  <p>Le paiement est effectué par l'intermédiaire du serveur d'autorisation du Centre de traitement  avec l'utilisation les cartes bancaires de systèmes de paiement suivants : <b>Visa, MasterCard</b>
</p>
	<p>
		Le traitement des commandes est effectué du lundi au vendredi, de 9:00 au 18:00. Si vous avez des questions, vous pouvez appeler tél:
		<b>04 27 77 13 55, 06 19 57 02 83</b> ou écrire sur l’email <b>france-sales@emischool.fr</b>
	</p>

  <p>Lisez attentivement les règles de paiement et de livraison ! En appuyant sur la commande  «OK», vous acceptez les règles  de paiement et de livraison.</p>
 
  <h2> Conditions supplémentaires </h2>
 
  <p> La commande minimum est de 50€. Lors la commande de produits le système des réductions est le suivant: </p>
  <p>Le montant de la commande supérieure à 400€ – 5% de réduction</br>
	Le montant de la commande supérieure à 600€ – 10% de réduction
  </p>
 
  <p><span style="font-family: 'Times New Roman', serif; font-size: 12pt; line-height: 115%;">l'envoi des commandes s'effectue en 3 jours ouvrables après réception du paiement.</span>  
    <br />
  </p>



   <div style="clear:both;"></div>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>