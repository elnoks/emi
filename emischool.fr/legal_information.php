<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Information légale");
?><div class="bx_page">
	<div class="h1-top">
		<h1>Conditions générales de vente</h1>
	</div>
	<h3>Article 1 - Acceptation des conditions</h3>
	<p>
		 Le client reconnait avoir pris connaissance, au moment de la commande, des présentes conditions générales de vente et déclare les accepter sans réserve. Les présentes conditions générales de vente régissent les relations contractuelles entre E.Mi FRANCE et son client, les deux parties les acceptant sans réserve.
	</p>
	<h3>Article 2 - Champs d'application et modification des conditions</h3>
	<p>
		 Les présentes conditions générales de vente s'appliquent à toutes commandes passées sur le site Internet <a href="http://www.emischool.fr">www.emischool.fr</a> qui se réserve le droit d'adapter ou de modifier à tout moment les présentes conditions générales de vente. En cas de modification, il sera appliqué à chaque commande les conditions générales de ventes en vigueur au jour de la commande. Les photographies illustrant les produits, n'entrent pas dans le champ contractuel. Si des erreurs s'y introduisent, en aucun cas E.Mi FRANCE ne sera tenu pour responsable.
	</p>
	<h3>Article 3 - Disponibilité des produits</h3>
	<p>
		 Nos produits et prix sont valables tant qu'ils sont visibles sur le site, dans la limite des stocks disponibles. Pour les produits non stockés dans notre magasin, nos offres sont valables sous réserve de disponibilité chez notre fournisseur. Dans ce cadre, la disponibilité des produits est indiqué sur leurs fiches. Ces informations provenant directement de nos fournisseurs, des erreurs ou modifications peuvent exceptionnellement exister. Dans l'éventualité d'une indisponibilité de produit aprés validation de votre commande, nous vous en informerons par mail ou par téléphone dés réception des informations reçues par nos fournisseurs. Votre commande sera automatiquement annulée et vous serez remboursé si votre compte bancaire a été débité au plus tard dans les 30 jours à compter du paiement des sommes versées par le client.
	</p>
	<h3>Article 4 - Prix</h3>
	<p>
		 Les prix de nos produits sont indiqués en euros TTC et sans frais de port. E.Mi FRANCE se réserve le droit de modifier ses prix à tout moment et s'engage à appliquer les tarifs en vigueur qui vous auront été indiqués au moment de votre commande, sous réserve de disponibilité à cette date. Toutes les commandes sont payables en Euros uniquement. Les produits demeurent la propriété de E.Mi FRANCE jusqu'au règlement total du montant de la commande.
	</p>
	<h3>Article 5 - Frais de port</h3>
	<p>
		 Les frais de port varient en fonction du poids de la marchandise et ne sont pas compris dans les prix indiqués. Ils vous seront communiqués en fin de commande.
	</p>
	<h3>Article 6 - Commande</h3>
	<p>
		 Sur Internet : <a href="http://www.emischool.fr">www.emischool.fr</a> Les systèmes d'enregistrement automatique sont considérés comme valant preuve de la nature, du contenu et de la date de la commande. E.Mi FRANCE confirme l'acceptation de la commande par l'envoi d'un message de confirmation à l'adresse mail du client que celui-ci aura communiqué. La vente ne sera conclue qu'à compter de l'envoi de cette confirmation. E.Mi FRANCE se réserve le droit de refuser ou d'annuler toute commande d'un client, notamment en cas d'insolvabilité ou d'un défaut de paiement de la commande concernée ou d'un litige relatif au paiement d'une commande antérieure. E.Mi FRANCE annulera toutes commandes sous un délai de 8 jours à compter de sa date d'émission si le règlement n'a pas été effectué. Les mentions indiquées par le client lors de la saisie des informations à sa commande n'engagent que celui-ci. E.Mi FRANCE n'est pas tenu responsable des erreurs commises par le client dans le libellé des coordonnées du destinataire de la commande (adresse de livraison, adresse de facturation) et des retards de livraison ou de l'impossibilité de livrer les produits commandés que ces erreurs pourraient engendrer.
	</p>
	<h3>Article 7 - Validation</h3>
	<p>
		 Vous déclarez avoir pris connaissance et accepté les présentes Conditions générales de vente avant de passer votre commande. La validation de votre commande vaut donc acceptation de ces Conditions générales de vente. Sauf preuve contraire, les données enregistrées par E.Mi FRANCE constituent la preuve de l'ensemble des transactions passées par E.Mi FRANCE et ses clients.
	</p>
	<h3>Article 8 - Paiement</h3>
	<p>
		 Le règlement peut se faire par Chèque, Paypal ou argent comptant si retrait au magasin (la marchandise sera expédiée dés le crédit apparaissant sur notre compte).
	</p>
	<p>
		 Vous pouvez effectuer le règlement:
	</p>
	 - par Chèque - à l'ordre de E.Mi France, 9 place du Bicentenaire, 42100 St Etienne. (La livraison sera effectuée sous 5-8 jours après encaissement du chèque ) <br>
	 - par Paypal - transaction sécurisée sur le site Paypal. <br>
	 - au comptant - lors du retrait en magasin au 9 place du Bicentenaire, 42100 St Etienne. <br>
	<p>
		 Dans tous les cas, la marchandise ne sera expédiée qu'après vérification et réception du paiement sur nos comptes.
	</p>
	<h3>Article 9 - Livraison</h3>
	<p>
		 Les produits commandés sont transportés aux risques et périls du destinataire.
	</p>
	<p>
		 Les produits sont livrés à l'adresse de livraison que vous avez indiqué au cours du processus de commande par DPD.
	</p>
	<p>
		 Les délais de livraison peuvent varier de 4 à 7 jours ouvrables pour la France métropolitaine, après encaissement du chèque effectué, sous réserve de disponibilité des produits et de 10 à 30 jours pour les produits que nous n'aurions pas en stock.
	</p>
	<p>
		 Il s'agit d'un délai moyen en fonction de la disponibilité de l'article. E.Mi FRANCE ne pourra etre tenu responsable des conséquences dues à un retard de livraison de la part de LA POSTE, DPD et ne donnent pas le droit au client de réclamer des dommages et intérets.
	</p>
	<h3>A LA LIVRAISON</h3>
	<p>
		 Vous devez vérifier la conformité de la marchandise livrée au moment de la livraison et avant de signer le bon de livraison (La Poste, DPD). Vous devez indiquer sur le bon de livraison et sous forme de réserves manuscrites accompagnée de votre signature toutes anomalies concernant la livraison (produits endommagés). Cette vérification est considérée comme effectuée dés lors que l'acheteur, ou une personne autorisée par elle, a signé le bon de livraison. Vous devrez également confirmer par courrier recommandé ces réserves au transporteur au plus tard dans les trois jours ouvrables suivant la réception du ou des articles et transmettre une copie de ce courrier à E.Mi France, 9 place du Bicentenaire, 42100 St Etienne.
	</p>
	<p>
		 Si les produits nécessitent de nous être renvoyés, ils doivent l'être dans les 7 jours ouvrés suivant la livraison. Toutes réclamations formulées hors de ce délai ne pourra être acceptées.
	</p>
	<p>
		 Le retour du produit ne pourra être accepté que pour les produits dans leurs états d'origine (facture obligatoire, emballage, pastille de protection, accessoires, notice).
	</p>
	<p>
		 Si au moment de la livraison, l'emballage d'origine est abimé, déchiré, ouvert, vous devez alors vérifier l'état des articles. S'ils ont été endommagés, vous devez impérativement refuser le colis et noter une réserve sur le bordereau de livraison (colis refusé car ouvert ou endommagé). Nous vous rappelons qui si vous refusez le colis le retour est gratuit par la poste sous 72 heures.
	</p>
	<p>
		 ERREUR DE NOTRE PART : E.Mi FRANCE s'engage à vous échanger les produits ne correspondant pas à votre commande résultant d'une erreur de notre part. Dans ce cas, veuillez en faire état dans les 48 heures suivant la réception des articles, de manière détaillée uniquement par email (<a href="mailto:mynailshopfr@gmail.com">mynailshopfr@gmail.com</a>). Aprés confirmation de notre part par retour d'e-mail, nous renvoyer le ou les produits dans leurs emballages d'origine, intacts non descellées et non utilisés accompagnés de tous les accessoires éventuels, facture, notices d'utilisation et documentations à l'adresse suivante : E.Mi France, 9 place du Bicentenaire, 42100 St Etienne.
	</p>
	<h3>Toutes réclamations formulées hors de ce délai ne pourra etre acceptées.</h3>
	<p>
		 ERREUR DE VOTRE PART : E.Mi FRANCE accorde un droit de rétractation de 7 jours francs à partir de la date de livraison pour retourner à vos propres frais les produits ne vous convenant pas.
	</p>
	<p>
		 PROCEDURE DE RETOUR : tout retour devra nous être signalé obligatoirement par e-mail (<a href="mailto:France-sales@emischool.fr">France-sales@emischool.fr</a>) et après confirmation de notre part par retour d'e-mail, vous pourrez nous réexpédier les produits. Nous attirons votre attention sur le fait que tout produit faisant l'objet d'une rétractation de la part du consommateur, devra nous être retourné dans un état neuf, propre, prêt à la revente. Tout produit qui aurait été abimé, qui serait incomplet, ou dont l'emballage d'origine serait détérioré ou absent, ne sera ni remboursé, ni échangé. En cas d'échange éventuel, les frais de réexpédition restent à la charge du client.
	</p>
	<p>
		 IMPORTANT! Tout retour de produits qui ne sera pas effectué selon la procédure ci-dessus ne sera pas pris en compte.
	</p>
	<h3>Article 10 - Revente des produits</h3>
	<p>
		 Le client s'interdit toute revente partielle ou totale des produits à d'autres professionnels ou non professionnels.
	</p>
	<h3>Article 11 - Responsabilité</h3>
	<p>
		 En achetant nos produits Techniques : gels-vernis permanents - gel paints- vernis à ongles, le client atteste avoir au minimum un CAP d'esthétique ou suivi une formation de Styliste/Prothésiste Ongulaire et par conséquent avoir les capacités à les mettre en application correctement.
	</p>
	<p>
		 E.Mi FRANCE ne peut être tenu pour responsable des dommages de toute nature, tant matériels qu'immatériels ou corporels, qui pourraient résulter d'un mauvais fonctionnement ou de la mauvaise utilisation des produits commercialisés. Il en est de même pour les éventuelles modifications des produits résultant des fabricants.
	</p>
	<p>
		 La responsabilité de E.Mi FRANCE ne saurait être mise en cause pour de simples erreurs ou omissions qui auraient pu subsister malgré toutes les précautions prises dans la présentation des produits. En cas de difficultés dans l'application du présent contrat, le client et E.Mi FRANCE se réservent la possibilité, avant toute action en justice, de rechercher une solution amiable. A défaut, le Tribunal de Commerce de SAINT ETIENNE est seul compétent, quelque soit le lieu de livraison et le mode de paiement accepté.
	</p>
	<h3>Article 12 - Droit applicable, litiges</h3>
	<p>
		 Le présent contrat est soumis à la loi française. La langue du présent contrat est la langue française. En cas de litige, les tribunaux français seront seuls compétents.
	</p>
	<h3>Article 13 - Service clientèle</h3>
	<p>
		 Pour toute information ou question, notre service clientèle est à votre disposition :
	</p>
	<p>
		 - par email: <a href="mailto:France-sales@emischool.fr">France-sales@emischool.fr</a>&nbsp;
	</p>
	<p>
		 - par courrier: E.Mi France, 9 place du Bicentenaire, 42100 St Etienne.&nbsp;
	</p>
	<p>
		 - par téléphone: 04, 69 35 32 84, 06 19 57 02 83.
	</p>
	<h3>Article 13 - Service clientèle</h3>
	<p>
		 Les informations nominatives collectées aux fins de la vente à distance est obligatoire, ces informations étant indispensables pour le traitement et l'acheminement des commandes, l'établissement des factures et des contrats de garantie. Le défaut de renseignement entraine la non-validation de la commande.
	</p>
	<p>
		 Le client dispose (article 34 de la loi du 6 janvier 1978) d'un droit d'accès, de modification, de rectification et de suppression des données qui le concernent, qu'il peut exercer par courrier auprès de E.Mi France, 9 place du Bicentenaire, 42100 St Etienne ou par e-mail: <a href="mailto:France-sales@emischool.fr">France-sales@emischool.fr</a>
	</p>
	<p>
		 E.Mi FRANCE s'engage à ne pas communiquer, gratuitement ou avec contrepartie, les coordonnées de ses clients à un tiers.
	</p>
	<h3>Article 13 - Service clientèle</h3>
	<p>
		 Le siège social d’E.Mi France est situé au 9 place du Bicentenaire, 42100 St Etienne.
	</p>
	<h2>Conditions générales de vente Formations</h2>
</div>
<p>
	 Les présentes conditions s'appliquent pendant toute la durée de mise en ligne des formations proposées par l’entreprise E.Mi School France, sur son site <a href="https://www.emischool.fr">https://www.emischool.fr</a>.
</p>
<p>
	 Les présentes conditions sont conclues entre, d'une part, l’entreprise E.MI SCHOOL France SIRET : 51033482400032, Siège: 9 place du Bicentenaire, 42100 St Etienne, France, ci-après dénommée «E.MI SCHOOL FRANCE», et d'autre part, toute personne, ci-après dénommée « stagiaire » souscrivant une formation avec E.Mi School France.
</p>
<h3>
CLAUSES GENERALES </h3>
<p>
	 Nos formations sont soumises aux présentes conditions générales de vente qui ont pour objet de définir les droits et obligations des parties dans le cadre de la vente de l'ensemble des formations proposées par E.MI SCHOOL France au stagiaire. En cas de contradiction entre les présentes conditions générales de vente et tout autre document figurant sur le site, y compris charte de qualité ou document équivalent, les présentes conditions générales prévalent.
</p>
<h3>
Article 1 - Le Prix </h3>
<p>
	 Les prix des formations sont indiqués en EUROS toutes taxes comprises.
</p>
<p>
	 E.MI SCHOOL France se réserve le droit de modifier ses prix et ses frais à tout moment, mais les formations seront facturées sur la base des tarifs en vigueur au moment de la validation de la commande. Cependant, si la demande de documentation a été faite dans le mois précédent le changement de tarif et que la commande est réalisée dans les 30 jours qui suivent le changement de tarif, E.Mi School France maintiendra les tarifs en vigueur au moment de la demande de documentation.
</p>
<h3>
Article 2 - Commander avec&nbsp;</h3>
<p>
	E.Mi School France Sur internet : <a href="https://www.emischool.fr">https://www.emischool.fr</a>; ou en envoyant votre règlement à : E.Mi SCHOOL France 9 place du Bicentenaire, 42100 St Etienne ; ou par téléphone au 04 69 35 32 84 ; ou par mail : <a href="mailto:france-school@emischool.fr">france-school@emischool.fr</a>
</p>
<h3>
Article 3 - Validation de la commande </h3>
<p>
	 Le stagiaire déclare avoir pris connaissance et accepté les présentes conditions générales de vente (CGV) avant la passation de sa commande. La validation de sa commande vaut donc acceptation de ces conditions générales de vente. En conséquence, le stagiaire reconnaît être parfaitement informé du fait que son accord concernant le contenu des présentes conditions générales de vente ne nécessite pas la signature manuscrite de ce document&nbsp; dans la mesure où le stagiaire souhaite commander les formations proposées sur le site Web.
</p>
<p>
	 La signature du contrat de formation vaut aussi acceptation des CGV. E.Mi SCHOOL France s'engage à délivrer au stagiaire ces formations en respectant les délais à compter de la validation de la commande par E.Mi SCHOOL France, c’est-à-dire après l'expiration du droit de rétractation. Un contrat de formation sera envoyé au stagiaire et il le retournera à E.MI SCHOOL France. C’est seulement à réception de ce contrat que l’inscription de la stagiaire sera validée et ça place réservé. Les données enregistrées par E.MI SCHOOL France constituent la preuve de l'ensemble des transactions passées entre E.MI SCHOOL France et ses stagiaires. Les données enregistrées par le système de paiement constituent la preuve des transactions financières.
</p>
<h3>
Article 4- Paiement </h3>
<p>
	 Le règlement de la formation achetée&nbsp; peut s'effectuer selon les moyens de paiement acceptés par E.MI SCHOOL FRANCE : Par Chèque (cheque d’acompte + cheque de solde qui sera encaissé le premier jour de la formation) ; Par Chèque ou Espèces (cheque d’acompte, montant voir dans le contrat + le solde en espèces à régler le premier jour de la formation).
</p>
<p>
	 Le stagiaire s’engage à verser la totalité du prix susmentionné selon les modalités de paiement suivantes:
</p>
<p>
	 Selon l’article L 6353-5 du code du travail, à compter de la date de signature du présent contrat, la/le stagiaire a un délai de dix jours pour se rétracter sans fournir de motif. Si elle (il) souhaite se rétracter, elle (il) en informe l’organisme de formation par lettre recommandée avec demande d’avis de réception. Dans ce cas, aucune somme ne peut être exigée de la/du stagiaire.
</p>
<p>
	 Selon l’article L 6353-7 du code du travail, passé le délai de dix jours, le contrat peut être résilié par l’apprenant, si en cas de force majeure dûment reconnue, il est empêché de suivre la formation. Dans ce cas, seules les prestations effectivement dispensées sont rémunérées à due proportion de leur valeur prévue au contrat.
</p>
<p>
	 Si l’apprenant souhaite abandonner la formation pour un autre motif que la&nbsp; force majeure dûment reconnue, voir les points 13 et 14 ci-dessous.
</p>
<p>
	 En cas d’inscription avec un acompte, l’apprenant s’engage de manière ferme à souscrire à la formation. La date de l’acompte fait donc foi comme date d’inscription.
</p>
<h3>
Article 5. Responsabilité d’E.MI SCHOOL FRANCE </h3>
<p>
	 Les photographies, textes, graphismes, informations et caractéristiques illustrant les produits et les formations présentés ne sont pas contractuels.
</p>
<h3>
Article 6. Protection du site et non concurrence </h3>
<p>
	 L'ensemble du contenu du site est protégé par la législation sur le droit d'auteur. E.MI SCHOOL est une marque déposée et protégée et ne peut faire l'objet d'aucune utilisation par des personnes étrangères à E.MI SCHOOL. Toute partie ou image du site ne peut être téléchargée ou imprimée que pour un usage personnel et non commercial. Toute autre utilisation devra se faire avec l'accord d’E.MI SCHOOL. Dans le cas contraire, le contrevenant s'expose à des poursuites. Le stagiaire s'interdit de reproduire, copier, vendre, revendre ou exploiter dans un but commercial quel qu'il soit toute partie de la formation, toute utilisation de la formation ou tout droit d'accès à la formation. Dans le cas contraire, le contrevenant s'expose à des poursuites.
</p>
<h3>
Article 7. Droit de réserve d’E.Mi School France </h3>
<p>
	 E.Mi School France se réserve le droit de refuser d'honorer une demande émanant d'un stagiaire qui n'aurait pas réglé totalement ou partiellement une formation précédente ou avec lequel un litige de paiement serait en cours d'administration ou si celle-ci se heurte au respect de l'ordre public et des bonnes mœurs.
</p>
<h3>
Article 8. Propriété intellectuelle </h3>
<p>
	 Tous les éléments du site <a href="http://www.emischool.fr">www.emischool.fr</a> , qu'ils soient visuels ou sonores, y compris la technologie sous-jacente, sont protégés par le droit d'auteur, des marques ou des brevets. Ils sont la propriété exclusive de l’entreprise E.Mi School France. L'utilisateur qui dispose d'un site Internet à titre personnel et qui désire placer, pour un usage personnel, sur son site, un lien simple renvoyant directement à la page d'accueil du site d’E.MI SCHOOL FRANCE, doit obligatoirement en demander l'autorisation à l’E.MI SCHOOL FRANCE. Il ne s'agira pas dans ce cas d'une convention implicite d'affiliation. En revanche, tout lien hypertexte renvoyant au site d’E.MI SCHOOL France et utilisant la technique du framing ou du in-line linking est formellement interdit. Dans tous les cas, tout lien, même tacitement autorisé, devra être retiré immédiatement sur simple demande d’E.MI SCHOOL FRANCE.
</p>
<h3>
Article 9. Intégralité </h3>
<p>
	 Dans l'hypothèse où l'une des clauses du présent contrat serait nulle et non avenue par un changement de législation, de réglementation ou par une décision de justice, cela ne saurait en aucun cas affecter la validité et le respect des présentes conditions générales de vente.
</p>
<h3>
Article 10. Durée </h3>
<p>
	 Les présentes conditions s'appliquent pendant toute la durée de mise en ligne des formations proposées par le centre de formation E.MI SCHOOL FRANCE.
</p>
<h3>
Article 11. Validité Géographique </h3>
<p>
	 Les présentes conditions sont applicables en France métropolitaine, dans les DOM-TOM, dans l’Union européenne ainsi que dans le reste du monde.
</p>
<h3>
Article 12. Droit applicable </h3>
<p>
	 Le présent contrat est soumis à la loi française. La langue du présent contrat est la langue française. En cas de litige sur son interprétation et/ou exécution non résolu à l’amiable le tribunal de St Etienne sera seul compétent.
</p>
<h3>
Article 13. Droit de rétraction </h3>
<p>
	 a. Délai légal du droit de rétraction
</p>
<p>
	 Selon l’article L 6353-5 du code du travail, à compter de la date de signature du contrat de formation, le stagiaire a un délai de dix jours pour se rétracter sans fournir de motif. S’il souhaite se rétracter, il en informe l’organisme de formation par lettre recommandée avec demande d’avis de réception. Dans ce cas, aucune somme ne peut être exigée du stagiaire.
</p>
<p>
	 Selon l’article L 6353-7 du code du travail, passé le délai de dix jours mentionné ci-dessus, le contrat peut être résilié par le stagiaire si en cas de force majeure dûment reconnue, il est empêché de suivre la formation. Dans ce cas, seules les prestations effectivement dispensées sont rémunérées à due proportion de leur valeur prévue au contrat.
</p>
<p>
	 Toute demande de cessation anticipée ne peut concerner que l’intégralité de la formation suivie.
</p>
<p>
	 En outre, dans la mesure où l’inscrit aurait réalisé tout ou partie de sa formation dans des délais plus courts que ceux prévus dans le calendrier prévisionnel de la formation, il pourra lui être demandé une somme déterminée au prorata de ce qui a été réalisé.
</p>
<p>
	 b. Remboursement
</p>
<p>
	 En cas d’exercice du droit de rétraction, E.MI SCHOOL FRANCE procédera au remboursement des sommes versées dans les 30 jours qui suivent la réception de la demande de rétractation.
</p>
<h3>
Article 14. Force majeure </h3>
<p>
	 Si par suite de cas de force majeure dûment reconnue (évènement comportant toutes les caractéristiques suivantes : irrésistible, imprévisible, et extérieur, au sens de l’article 1148 du code civil et de son interprétation par la jurisprudence), le stagiaire inscrit à titre individuel est empêché de suivre sa formation, celui-ci dispose de la possibilité de rompre le contrat par lettre recommandée avec accusé de réception (LRAR), envoyée à E.MI SCHOOL France 9 place du Bicentenaire, 42100 St Etienne.
</p>
<p>
	 Dans ce cas, seules les formations effectivement dispensées seront remboursées à due proportion de leur valeur prévue au contrat.
</p>
<h3>
Article 15. Confidentialité et données personnelles </h3>
<p>
	 E.MI SCHOOL FRANCE est soucieux de la protection des données personnelles. L’entreprise s’engage à assurer le meilleur niveau de protection des données personnelles en conformité avec la loi informatique et libertés. E.MI SCHOOL FRANCE s'engage à ce que les informations saisies ou envoyées par e-mail ne soient pas traitées à d'autres fins que pour l'exécution des travaux commandés à la demande exclusive du stagiaire ni cédées à des tiers, c'est-à-dire des organismes ou sociétés extérieurs au groupe ou encore des personnes physiques. E.MI SCHOOL FRANCE protège les informations personnelles du stagiaire de toute utilisation ou divulgation sans son autorisation.
</p>
<p>
	 Pour toute information sur la protection des données personnelles, consulter le site de la Commission informatique et liberté <a href="http://www.cnil.fr">www.cnil.fr</a>
</p>
<p>
	 Identité du responsable du traitement
</p>
<p>
	 Les données personnelles sont collectées par : E.MI SCHOOL France, 9 place du Bicentenaire, 42100 St Etienne
</p>
<p>
	 Article 16 - Finalité – exploitation des données concernant les internautes et stagiaires
</p>
<p>
	 a. Gestion des commandes
</p>
<p>
	 Les informations et données concernant le stagiaire sont nécessaires à la gestion de sa commande et à aux relations commerciales associées. Ces informations et données sont également conservées à des fins de sécurité, afin de respecter les obligations légales et réglementaires.
</p>
<p>
	 b. Personnalisation des formations
</p>
<p>
	 Les informations et données concernant les personnes intéressées par les formations d’E.MI SCHOOL FRANCE permettent d’améliorer et de personnaliser les formations proposées et les informations adressées aux internautes et stagiaires.
</p>
<p>
	 c. Les informations et newsletters
</p>
<p>
	 Afin de tenir les internautes informés de l’actualité des offres d’E.MI SCHOOL FRANCE et des avantages dont ils peuvent bénéficier, ils pourront recevoir par email des offres de l’enseigne. Le taux d’ouverture des envois est calculé afin de l’adapter au mieux aux besoins des internautes.
</p>
<p>
	 Si l’internaute ne souhaite pas recevoir d’offres ni d’informations, il peut s’opposer aux envois en le signalant lors de la création de son compte, ou à tout moment en le précisant par mail à <a href="mailto:France-school@emischool.fr">France-school@emischool.fr</a>.
</p>
 Les informations et données renseignées dans le cadre d’E.MI SCHOOL FRANCE sont exclusivement destinées à E.MI SCHOOL FRANCE, pour les besoins de la personnalisation des formations et informations qui sont adressées aux internautes.<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>