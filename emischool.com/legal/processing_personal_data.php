<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Information note on processing of personal data");
?>
    <p><strong>Dear Madam, Sir,</strong></p>


    <p>This statement is for us, E. Mi -International s.r.o, ID 24214647, with registered office at U bojových 89/1 , Žižkov, 130 00 Praha 3, registered in    the Commercial Register kept at the Municipal Court of    Prague under file number  C 189332 , have prepared to share with you how we collect,  process, use and protect your personal data and thus your privacy.</p>

    <p>We handle your personal data in    compliance with    valid legislation, in particular    Regulation (EU) 2016/679 of the European Parliament and of the Council on the protection of individuals with a view to combating terrorism ,  the connection with the processing of personal data and the free movement of such data ("GDPR") , Act No. 127/2005 on electronic communications, as amended, and Act No. 480/2004 Coll., on certain information society services, as amended regulations.</p>

    <p>Within this document, we have prepared  basic and clear information about what personal information we process on the legal background of the process,  what specific information use we use, who we can pass on and what you have in    in relation to the processing of personal data your rights.  All details, in particular on the purposes and the method of processing, the processing time, as well as the rights in    processing of your personal data can be found at www.emischool.cz     in " Legal Information " section.</p>

    <h4>What personal data we collect and how we use it</h4>
    <p>We process the following personal data:</p>
    <ul>
        <li><strong>identification data</strong>  - in particular, name and surname, date of birth</li>
        <li><strong>contact details which allow us to contact you</strong> - especially e-mail address, phone number, postal address,</li>
        <li><strong>details of your orders</strong> - in particular, the details of the goods and services you have ordered from us, the way of delivery and payment method, including the payment account, the claim details etc.</li>
        <li><strong>information about your behavior on our website</strong> - especially the goods and services you display, the links you click on, the way you surf through our website, the data about the device from which you view our website, such as IP address, device identification, and also data obtained from cookies</li>
        <li><strong>personal data provided through social networks</strong> when you visit our social networking sites - especially profile image, site, friends list</li>
        <li><strong>The personal data you provide</strong> - especially photos, answers fo surveys,</li>
        <li><strong>personal data related to   our school</strong> - especially recordings from   camera system.</li>
    </ul>

    <p>We process your personal data for various purposes and to varying degrees either:</p>
    <p>-  Without your consent, based on the contract, our legitimate interests or because of the performance of a legal obligation, OR</p>
    <p>-  Based on your consent.</p>

    <p>The fact that your personal data needs to be processed depends on your intended purpose and the position you are taking .  So essential is, that if you will just visit our website or will also register and purchase something.  We may also process your data if you communicate with us or visit our school.</p>

    <h3>IF YOU VISIT OUR WEBSITE</h3>

    <p><strong>Processing of log files</strong></p>
    <p>As a part of access to our website, we process access log files,  in order to protect our legitimate interests in protecting websites and improving their performance.  Log files are processed without your consent.</p>
    <p>In this context, we process the following data:</p>
    <ul>
        <li>identification data</li>
        <li>contact details</li>
        <li>details of your orders</li>
        <li>information about your behavior on our website</li>
        <li>personal data provided through social networks</li>
        <li>the personal data you provide</li>
        <li>personal data related to   our school</li>
    </ul>

    <p>These personal data are kept for 10 years after visiting the website.</p>

    <p><strong>Using cookies with and analyzing websites</strong></p>
    <p>In order to improve the functionality of our website, evaluation of traffic, and to optimize the use of marketing activities started on our website, we use cookies.  So, if you visit our website, we store your device information and then   we read these small text files.</p>
    <p>Cookies and other tracking technologies are used primarily to improve our services and make them more efficient.  If you decide to disable cookies, there might be limited functionality or limitation to use the services offered through our website.</p>

    <p><strong>Using social plugins</strong></p>
    <p>Our site also includes third-party social plug-ins that allow web visitors to share content with their friends and other contacts.  This is a network plugin:</p>
    <ul>
        <li><strong>Facebook</strong> plugin operated by Facebook Inc., 1601 South California Avenue, Palo Alto, CA 94304, USA;</li>
        <li><strong>Youtube</strong> plugin operated by YouTube, LLC 901 Cherry Ave., 94066 San Bruno, CA, USA, a subsidiary of Google Inc., Amphitheatre Parkway, Mountain View, CA 94043;</li>
        <li><strong>“VKontakte”</strong> Ltd, INN 7842349892, OGRN 1079847035179, with registered offices at 191024, St. Petersburg, ul. Khersonskaya, 12-14, A, office 1-H;</li>
        <li><strong>“MAIL.RU”</strong> Ltd, INN 7743001840, KPP 771401001, OGRN 1027739850962, OKPO 52685881, with registered offices at 125167, Moscow, Leningradsky Prospect, 39, building no. 79;</li>
        <li><strong>“YANDEX”</strong> Ltd, INN 7736207543, KPP 997750001, OGRN 1027700229193, OKPO 55187675, with registered offices at 119034, Moscow, ul.Lva Tolstogo, 16;</li>
        <li><strong>“Jivoste”</strong> Ltd, INN 7725745476, OGRN 1127746026792, KPP 772501001, with registered offices at 115280, Moscow, Leninskaya Sloboda street, 19, office no. 21;</li>
        <li><strong>“Google”</strong> Ltd, INN 7704582421, KPP 770501001, OGRN 1057749528100, OKPO 93279213, with registered offices at 212099, Moscow, Smolenskaya square 3, Smolensky Passazh business center.</li>
    </ul>

    <p><strong>Processing of personal data for marketing purposes</strong></p>
    <p>If you want to participate in our loyalty program, or you're interested in information about new products and special offers we will process your personal data based on consent, always at least    name, surname, date of birth, address, e-mail and address and telephone number.  The content of these offers and other advertising messages can be profiled on the basis of information provided and other content profiling data that we will find out from your behavior on our website or in the order form.  We will only be able to process your data after we have your consent.</p>
    <p>Personal data will be processed  for as long as the consent is granted, meaning for a maximum of 10 years.  Giving an agreement with processing of personal data is entirely voluntary and can be revoked at any time by clicking on the box in your user account</p>

    <h4>IF YOU REGISTER WITH US OR BUY PRODUCTS WITH US</h4>
    <p>If you register in our e-shop on the website www.emischool.cz and / or buy our products, we process your identification and contact details, details of your orders , for the purpose of  realization of purchase contract (delivery of ordered goods)  and also in order to protect our legitimate interests against possible litigation arising out of  concluded purchase contracts.</p>

    <p>E-mail addresses of customers who buy in our e-shop, are processed for further  verification of your satisfaction with the  purchase.  The legitimate title of this about processing email addresses is our legitimate interests of   providing feedback on the course of buying from customers, allowing us to improve our offer as well.</p>

    <p>We will have this personal data  processed for 10 years from the purchase contract closure.</p>

    <h4>IF YOU BECOME A PARTICIPANT OF ANY OF THE OUR NAIL DESIGN COURSES</h4>
    <p>We further process your personal data if you decide to be a participant in our training courses in nail design field.  lectures are conducted in our nail design school - E.Mi  School  at st.  14 října r 642/17, Prague 5, Postal Code 150 00 (hereinafter "School").</p>

    <p>If you become a participant of any of nail design courses that we offer, we process your identity and contact information for the purpose of implementation of the contract agreements, as well as for our legitimate interests in the   protection against possible litigation resulting from closed contracts.  So, if you decide to become a participant in our training courses, we process your personal information without your consent.  We will have this personal data  processed for 10 years after the conclusion of the contract.</p>

    <p>If you attend our school,  we will also process the recording from  -  a camera system to which you can be captured, to protect our and your property and people moving at school, that is, to our legitimate interest .  For more information, please contact your school supervisor.</p>
    <p>For this purpose, we keep personal data for 30 days.</p>

    <p>During the visit of our courses of nail design and related events, we capture photos and / or audio recordings, but also welcome you to provide photographs and / or  audio-visual records that we can use to promote our products and services.  However, we can process such photos and / or videos only after we have your consent.  The so offered personal data will be processed  for as long as the consent is granted, meaning for a maximum of 10 years.  This personal data processing is entirely voluntary and can be revoked at any time in writing at E. Mi -International sro , at U božích bojovníků 89/1, Žižkov, 130 00 Prague 3, or by e-mail sent to post @ emischool .com .</p>

    <h4>IF YOU ARE VISITING THE EXHIBITION WHERE WE PRESENT OUR PRODUCTS AND SERVICES</h4>
    <p>In order to introduce our products and services deeper, we also participate in exhibitions focused on nail design.  In the framework of the presentation of our products and services at such exhibitions, we acquire photographs and / or audiovisual recordings for marketing and promotional purposes.  We inform you through this fact   through the invitations and / or information placed at our exhibition area.  By visiting our exhibition area, you consent to us with  taking photographs and / or audiovisual recordings capturing your portrayal and / or personal expressions within the meaning of the Civil Code and, at the same time, agreeing to    their processing as personal data.  We will process these personal data for no longer than 10 years from the date of the exhibition.  Agreement with processing of personal data is entirely voluntary and may be revoked at any time in writing at the address of E. Mi -International sro , U božích bojovníků 89/1, Žižkov, 130 00 Praha 3, or by e-mail sent to Post @ emischool.com .</p>

    <h4>WHOM WE COMMIT YOUR PERSONAL DATA TO</h4>
    <p>Your personal information is used for our internal needs only for the above reasons.  However, there are services concerning personal data, however, needed for third-party services.  These are, for example, shipping companies, internet providers, e-mail, SMS correspondence, on-line chat, statistical and monitoring systems , lecturers teaching nail design at our school or relevant accredited person    in the case of professional courses, a professional examination is carried out.</p>

    <h4>FROM    WHAT SOURCES YOUR PERSONAL INFORMATION WILL BE OBTAINED</h4>
    <p>In most cases, we process personal information that we provide directly in order to order goods or services, create and use an account, or communicate with us.  We also get personal information from you by tracking your behavior on our website.</p>


    <p><strong>Passing your personal data outside the European Union</strong></p>
    <p>In some cases, we may also transfer your personal data to countries that are not part of the European Economic Area.</p>

    <p>As part of the data transfer to our processors, we can pass your data also to the third world countries outside the European Economic Area, which do not ensure an adequate level of data protection.  We will only make such transmissions if the responsible processor undertakes to comply with the standard contractual clauses issued by the European Commission available at  http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2004:385:0074:0084:EN:PDF</p>



    <p><strong>Rights associated with personal data protection</strong></p>
    <p>There are no personal data processing decisions based solely on automated processing, which would have legal effects or would otherwise significantly touched the data subjects whose personal data is processed.</p>

    <p>If we process your personal information, the following rights associated with the processing of your personal data are the following to you, when fulfilling the prerequisites set forth by law:</p>
    <p><strong>a) The right of access to   your personal data</strong></p>
    <p>You have the right to know what information is processed, for what purpose, for how long, to whom your personal data is transferred, as well as whether someone else has processed your personal data, and what have other rights related to the processing of V personal data.  All this information is contained in this document and on the website www.emischool.cz under the section “Legal Information ".  However, if you are unsure about which personal data processed, you can ask us to confirm whether personal data is processed or not, and if so, you are entitled to access to this personal data.  In context of the right to access us, you can request a copy of the personal data processed. The first copy will be provided free of charge and additional copies of the charge.</p>
    <p><strong>b) The right to explanation, correction, resp.  completing your personal information</strong></p>
    <p>If you find that the personal data we process is inaccurate or incomplete, you have the right to ask to correct it without undue delay corrected or supplemented.</p>
    <p><strong>c) The right to delete your processed personal data</strong></p>
    <p>In some cases, you have the right to our personal information deleted.  We will delete your personal information without undue delay if any of the following is true:</p>
    <p>- you withdraw consent to the processing of your personal data, which is data that your consent is necessary to process, and at the same time we have no other reason why we need to process such data,</p>
    <p>- you exercise your right to object to the processing of personal data we process on our legitimate interests and we find that no such legitimate interests would justify such processing have or do not believe that your personal data processing has ceased to exist compliance with generally binding regulations</p>
    <p>This right does not apply if the processing of personal data is still necessary to fulfill our legal obligations and determine our performance or defense of legal claims.</p>
    <p><strong>(d) The right to limit the processing of your personal data</strong></p>
    <p>In some cases you may, in addition to the right of cancellation, exercise the right to limit the processing of personal data.  This right allows you, in certain cases, to require that your personal data be flagged and that these data are not subject to any further processing operations - in this case, however, not forever (as in the case of a right of cancellation) but for a limited period of time.  We need to restrict the processing of personal data when:</p>
    <p>-                You deny the accuracy of personal data before agreeing what data is correct;</p>
    <p>-                Process your personal data without sufficient legal basis (e.g. Beyond what process we have), but in the y you before deleting such data to favor only their limitations (e.g. If you expect that you give us in the future, such data are well provided i)</p>
    <p>-                We no longer require personal data for the above-mentioned processing purposes, but You require them to identify, exercise or defend your legal claims or object to processing.</p>
    <p>After a period during which we save, if Y our objection is justified, we are obliged to process personal data in limit.</p>
    <p><strong>(d) The right to the portability of your personal data</strong></p>
    <p>You have the right to obtain personally identifiable information that is relevant to you and which you have provided to us in a structured, commonly used and machine-readable format for forwarding them to another Administrator.</p>
    <p><strong>e)     The right to object to the processing of your personal data</strong></p>
    <p>You have the right to object to the processing of your personal data, which is based on our legitimate interest. In the case of marketing activities, we will cease processing your personal data without further processing and in other cases, we will do so unless we have serious legitimate reasons to continue such processing</p>
    <p><strong>f) Whenever right to withdraw consent to the processing of your personal data</strong></p>
    <p>If your personal data is processed on your consent, you may withdraw your consent at any time.  Revocation of consent is without prejudice to the lawfulness of processing based on consent given prior to his / her removal.</p>
    <p><strong>g) Right to lodge a complaint</strong></p>
    <p>The rights described above can be claimed by us in writing or by e-mail, using the contact details listed below .</p>
    <p>Likewise, you can contact your supervisor with your complaint:</p>

    <p><strong>Office for Personal Data Protection</strong></p>
    <p><strong>Pplk.  Sochova 27, 170 00 Praha 7</strong></p>
    <p><strong>tel .: 234    665    111</strong></p>
    <p><strong>web: www.uoou.cz</strong></p>

    <p>Contact details of the personal data administrator and the Data Protection Officer</p>
    <p>For all matters related to the processing of your personal data, whether it is a question of exercising the right, complaint or anything else, we can  contact us at the address below.</p>

    <p><strong>E. Mi - International sro</strong></p>
    <p>For U božích bojovníků  89/1</p>
    <p>130 00 Prague 3 - Žižkov</p>
    <p>Tel .:  +420 773 208 276</p>
    <p>e-mail: post@emischool.com</p>

    <p>We have set up a Privacy Commissioner who can be contacted at post @ emischool.com .</p>

    <p>We will resolve your request without undue delay, but no later than within one month.  In exceptional cases, in particular from    due to the complexity of your request, we are entitled to extend this period by another two months.  We will inform you of any such extension.</p>

    <p>On 25.5.2018</p>





<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>