<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("How to open the E.Mi school representative office");
?><div class="bx_page">
	<p style="text-align: center;">
 <img width="800" alt="765.jpg" src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" height="105" title="765.jpg" border="0"><b> <br>
 </b>
	</p>
	<p>
 <b> <br>
 </b>
	</p>
	<p>
 <b>Cooperation with the Nail design school by Ekaterina Miroshnichenko is an opportunity to get a ready-made and profitable business solution. You obtain a full range of technologies: from design to management and promotion. Original school of nail design by Ekaterina Miroshnichenko always presents latest, demanded, stylish and well-recognized images.</b>
	</p>
	<p>
 <b>Be our regional representatives and we’ll teach you how to be so successful and creative as we are!</b> Today the School of nail design by Ekaterina Miroshnichenko is well-known all over the world: prestigious awards at international competitions, seminars in Germany and Italy, the opening of an international office in Europe. <br>
		 You can acquire an exclusive right to run a highly profitable and interesting business on education and advanced training of nail design masters by the School of nail design brand.
	</p>
	<p>
 <b>Join those who are in trend!</b> The Nail design school by Ekaterina Miroshnichenko created both unique technologies of recognizable design and education. <br>
 <b>Creativity and beauty is possible with us!</b> <br>
 <b>The advantages of the School of nail design by Ekaterina Miroshnichenko:</b>
	</p>
	<ul>
		<li>educational services are in great demand. More than 500 masters from all parts of Russia and abroad study different programs annually;</li>
		<li>the <a href="/catalog/">E.Mi</a> brand materials are used during the education;</li>
		<li>the company international level. There is an office in Europe (Prague);</li>
		<li>the educational programs are based on unique methods developed and patented by Ekaterina Miroshnochenko, a world champion in nail design nomination according to OMC (Paris, 2011);</li>
	</ul>
	<p>
	</p>
	<p>
 <b>we provide our representative offices with all necessary materials for successful work of educational department. <br>
		 The opening of the School of nail designs representative office gives you the following:</b>
	</p>
	<ol>
		<li><b>An exclusive right </b>to work by the name of the School of nail design by Ekaterina Miroshnichenko and provide educational services on the territory pointed in a contract. Only one representative office can operate in a region of the Russian Federation (oblast, kray, autonomous region).</li>
		<li><b>Educational programs </b>developed by Ekaterina Miroshnichenko. The educational centre saves time and finance on the curriculum development, training methods and educational materials and handbooks. All this allows focusing on search of students and compensating the expenses on opening within the shortest possible period of time.</li>
		<li><b>The united system of certification. </b>The representative office has a right to hand their students certificates with hologram of the School of nail design by Ekaterina Miroshnichenko.</li>
		<li><b>The right of an exclusive discount on the <a href="/catalog/">E.Mi</a> products.</b></li>
		<li><b>Promotional support.</b> The author’s school places all information about its representatives in leading professional magazines as well as in its own advertising catalogue and official website; helps to hold the regional exhibitions and nail-conferences, gives advertising layouts to publish in local periodicals.</li>
	</ol>
	<p>
	</p>
	<p>
 <b>The cooperation as an official representative of the School of nail design by Ekaterina Miroshnichenko and an official distributor of the Emi brand.</b> <br>
		 The official representative of the School of nail design by Ekaterina Miroshnichenko has the preemptive right in contract of the E.Mi brand distribution on the territory, pointed in the terms of contract. In this case, additional requirements and terms of distributor cooperation as well as obligations on the purchase price with relevant discount increase on the EMi brand products are added to the requirements of the official representatives of the School of nail design by Ekaterina Miroshnichenko.
	</p>
	<p>
 <b>The requirements to open a representative office of the School of nail design by Ekaterina Miroshnichenko:</b>
	</p>
	<ol>
		<li>A talented instructor, capable to copy accurately Ekaterina Miroshnichenko’s technique. A candidate first studies as an apprentice and if he or she has an appropriate qualification according to Ekaterina Miroshnichenko’s requirements, goes on a special course of instructors.</li>
		<li>A separate facility for a training centre no less than 16 square metres. Placement of branded signboard according to the School of nail design by Ekaterina Miroshnichenko style.</li>
		<li>Availability of legal entity which has a right to provide educational services (self-employed individual, non-governmental educational institution, private educational institutions, etc.)</li>
		<li>Working capital to make the first purchase of products, equip the training centre and pay for an instructor’s study.</li>
		<li>Representative commitment to attract new apprentices on basis of the developed technologies.</li>
	</ol>
	<p>
	</p>
	<p>
 <b>Dear masters, if you are really interested in opening of the School representative office in your region, firstly you need to:</b>
	</p>
	<ol>
		<li>know whether there is a representative office in you region. You can get information about it in Contacts on our website or call the number&nbsp;+420 774 208 276. If there is already a representative office in your region, we’ll have to refuse.</li>
		<li>Send photos of nail tips with Ekaterina Miroshnichenko’s designs on our email <a href="mailto:sviridova@emi-school.ru">sviridova@emi-school.ru</a> (for example, Sophisticated Floral Ornaments, Rivels, Strawberry, Raspberry, Cherry, etc.). It is necessary for your appointment as an instructor because your style must absolutely correspond to Ekaterina Miroshnichenko’s requirements. You must copy accurately her design and technique.</li>
		<li><b>Know the education fee calling the number: +420 773 208 276.</b> Instructor education is held in two steps: as an apprentice and an instructor. Education is held only in the training centre in Rostov-on-Don. The cost directly depends on the course quantity.</li>
		<li><b>Know the purchase volume in quarter calling the number +420 773 208 276.</b> the purchase volume is fixed in the contract.</li>
		<li>Be ready for regular advanced trainings. The instructor contract is signed for the term of one year. Every year the instructor herself confirms qualification in Rostov-on-Don and signs a contract for the next year.</li>
	</ol>
	<p>
	</p>
	<p>
 <b>For additional information on opening a representative office of the School of nail design by Ekaterina Miroshnichenko, please, call the international office: <br>
		 Korbut Marina, <a href="mailto:korbut@emischool.com">korbut@emischool.com</a>,&nbsp;+420 773 208 276</b>
	</p>
 <br>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>