<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("About company");
?><div class="bx_page">
	<div class="h1-top">
		<h1>About company</h1>
	</div>
	<div class="bx_page" style="text-align: center;">
 <img width="1140" alt="56.jpg" src="/upload/medialibrary/f7c/f7cb52963256ebe4b40f9af5c03a36d2.jpg" height="300" title="56.jpg" border="0"> <br>
	</div>
 <br>
	<p>
		When it comes to a chic salon manicure, E.Mi is a trendsetter and an innovator. We are the first choice of stylish ladies and nail industry leaders in 28 countries.<br/>
		Stay on top of the game with E.Mi materials and techniques! Create a successful beauty salon, become a sought-after nail artist and establish yourself as manicure fashion expert!<br/>
		Become a part of a successful fashion business - join our E.Mi International Team!
	</p>
	<p>
 <a href="/social/open_school.php" type="button" class="btn-pink order-button">Become a partner</a>
	</p>
	<p>
 <br>
	</p>
	<h2>E.Mi Manicure<br>
 </h2>
	<p>
 <img width="1140" alt="54.jpg" src="/upload/medialibrary/cd4/cd48dcf71294554d8839eab0a34a4986.jpg" height="150" title="98.jpg" border="0"> <br>
	</p>
	<p>
		 E.Mi manicure styles are timeless and trendy, created from the designs by Ekaterina Miroshnichenko, a true nail couturier, with the use of exclusive E.Mi materials.<br>
	</p>
	<h2>E.Mi Manicure collections&nbsp;</h2>
	<p>
		 E.Mi manicure collections draw inspiration from haute couture and can be seen at the hottest fashion shows around the world.&nbsp; Each fashion season brings a new collection of delicate designs ready to complement your stylish outfits. <a href="http://emi-school.ru/gallery/#photonail">Take a look</a>&nbsp;at our latest collection of chic E.Mi manicure right now! We are proud to say that our manicure collections can be found in 15 000 of the world's best beauty salons around the globe.<br>
	</p>
	<p>
 <a href="/social/salon.php" type="button" class="btn-pink order-button">Choose a salon</a>
	</p>
	<p>
 <br>
	</p>
	<h2>E.Mi Materials</h2>
	<p>
 <img width="1140" alt="6542.jpg" src="/upload/medialibrary/6e3/6e3b48a3e4405516b24de034f7ca5b60.jpg" height="150" title="6542.jpg" border="0"> <br>
	</p>
	<p>
		 E.Mi materials are low in consumption and allow you to shorten the time required for each session. Each season we update our color palette to include the most popular shades of the season, according to the Pantone Institute and the world's most renowned fashion experts.<br>
	</p>
	<p>
		 EMPASTA, E.MiLac, Charmicon, NAILCRUST, NAILDRESS – they are the true bestsellers when it comes to creating stylish salon manicure. E.Mi Gel System is a unique system for nail strengthening, lengthening and modeling - without the need to use a nail file! E.Mi Care System is our professional line of products for complex hand and foot care.&nbsp; Don't hesitate and pick the perfect products right now!
	</p>
	<p>
 <a href="/catalog/" type="button" class="btn-pink order-button">Choose</a>
	</p>
	<p>
 <br>
	</p>
	<h2>E.Mi School<br>
 </h2>
	<p>
 <img width="1140" alt="765.jpg" src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" height="150" title="765.jpg" border="0"> <br>
	</p>
	<p>
		 More than 20 000 nail artists have already participated in our unique E.Mi School programs created by Vera and Ekaterina Miroshnichenko.&nbsp; Become a true nail artist, a professional pedicurist, or a nail stylist and secure a good income for yourself - it's all easy with E.Mi!<br>
	</p>
	<p>
		 All courses start with the easy techniques, gradually introducing the more difficult ones - all under the supervision of our experienced teachers.&nbsp; So don't worry - you'll do great! Visit our website and choose the best course right now!&nbsp; &nbsp;
	</p>
	<p>
 <a href="/courses/" type="button" class="btn-pink order-button">Choose</a>
	</p>
	<p>
 <br>
	</p>
	<h2>OFFICIAL REPRESENTATIVES</h2>
	<p>
 <img width="1140" alt="7645.jpg" src="/upload/medialibrary/060/060926d856ef3ef2bcc0eef18fa948cc.jpg" height="150" title="7645.jpg" border="0"> <br>
	</p>
	<p>
		 The E.Mi brand and the School of Nail Design by Ekaterina Miroshnichenko are the latest business lines which are popular and in great demand. Currently, the official representatives of the nail design school by Ekaterina Miroshnichenko are successfully operating in Russia, Ukraine, Kazakhstan, Kyrgyzstan, Belarus, Italy, Ireland, Portugal, Poland, Romania, Cyprus, Greece, Germany, France, Lithuania, Litva, Estonia, Tunis, Switzerland, United Kingdom, Israel, &nbsp;South Korea, South Africa and the United Arab Emirates. The E.Mi products can be purchased all over the world.
	</p>
	<p>
		 You also can be a part of a successful international brand and get ready-made business solutions – become an official representative of the School of Nail Design by Ekaterina Miroshnichenko or an official distributor of the E.Mi brand in your city, region or country. Official representatives of the School Nail Design by Ekaterina Miroshnichenko get the following advantages:
	</p>
	<ul>
		<li>the possibility to represent exclusively the brand of the School of Nail Design by Ekaterina Miroshnichenko in your region;</li>
		<li>the right to tutor original courses on Ekaterina Miroshnichenko’s nail design;</li>
		<li>full business processes for opening and developing of your business;</li>
		<li>perfective design maintenance and personnel training;</li>
		<li>federal promotional support;</li>
		<li>profit making decisions on all E.Mi product range purchase.</li>
	</ul>
	<p>
	</p>
	<p>
		 To become an official representative of the School of Nail Design by Ekaterina Miroshnichenko or an official distributor of the E.Mi brand, you can fill in an application form on our website or call +420 722935746 Korbut Marina, <a href="mailto:korbut@emischool.com">korbut@emischool.com</a>
	</p>
	<p>
 <br>
	</p>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>