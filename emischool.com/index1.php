<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
IncludeTemplateLangFile(__FILE__);

$APPLICATION->SetTitle("E.Mi");
$APPLICATION->SetPageProperty("description", "E.Mi");
?>
<!-- CAROUSEL==================================================-->
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"main_page_slider",
	Array(
		"IBLOCK_TYPE" => "sliders",
		"IBLOCK_ID" => "15",
		"NEWS_COUNT" => "25",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "",
		"SORT_ORDER2" => "",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array("DETAIL_PICTURE",""),
		"PROPERTY_CODE" => array("SHOW","TOP_MARGIN","LEFT_MARGIN","TEXT_BLOCK_CAPTION","TEXT_BLOCK_CAPTION_COLOR","TEXT","TEXT_COLOR","BUTTON_TEXT","BUTTON_TEXT_COLOR","BUTTON_COLOR","LINK",""),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N"
	)
);?>
<!-- /.carousel -->
      	<!-- CONTENT==================================================-->
      	<div class="container container-main-page-bg">
      	<!-- 4 blocks -->
      		<div class="four-blocks-container">
	      		<div class="row">
	      			<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 block ">
	      				<a href="http://emi-school.ru/gallery/" class="shadow">
	      					<h2><?=GetMessage("TEST_ME1")?></h2>
	      					<?$APPLICATION->IncludeComponent(
	"emi:youtube.lastvideo", 
	".default", 
	array(
		"FEED_TYPE" => "PLAYLIST",
		"FEED_ID" => "PLw2vuUxscijuw91eomznnzEE4JG9y-EtD"
	),
	false
);?>
	      				</a>
	      			</div>
	      			<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 block">
	      				<a href="http://emi-school.ru/social/" class="shadow">
		      				<h2>Мастер-классы</h2>
	      					<?$APPLICATION->IncludeComponent(
	"emi:youtube.lastvideo", 
	".default", 
	array(
		"FEED_TYPE" => "PLAYLIST",
		"FEED_ID" => "PLw2vuUxscijuMJhHbcFFFU45T_g-0h0Yl"
	),
	false
);?>
	      				</a>
	      			</div>
	      			<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 block">
		      			<a href="/contacts/" class="shadow">
		      				<h2>Где купить?</h2>
		      				<div class="block-container">
			      				<div class="block-img-container" style="text-align: center">
			      					<img src="/include/img/map_pink.jpg" alt="">
			      				</div>
			      			</div>
	      				</a>
	      			</div>
	      			<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 block">
	      				<a href="/social/" class="shadow">
		      				<h2>Сообщества</h2>
		      				<div class="block-container">
			      				<div class="block-img-container main-instagram">
			      				<?$APPLICATION->IncludeComponent(
	"emi:instagram.last_photo", 
	".default", 
	array(
		"USER_ID" => "emiroshnichenko"
	),
	false
);?>
			      				</div>
			      			</div>
	      				</a>
	      			</div>
	      		</div>
	      	</div>
	      	<hr class="featurette-divider">
      		<!-- /. 4 blocks -->
      	
        	<!-- MAIN-TEXT-BLOCK-1 -->
        	<div class="row main-text-block">
          		<div class="col-md-7">
            		<?$APPLICATION->IncludeComponent(
							"bitrix:main.include", 
							".default", 
							array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => SITE_DIR."include/main_page_text_block_1_content.php",
								"EDIT_TEMPLATE" => ""
							),
							false
						);?>
          		</div>
          		<div class="col-md-5">
            		<?$APPLICATION->IncludeComponent(
							"bitrix:main.include", 
							".default", 
							array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => SITE_DIR."include/main_page_text_block_1_img.php",
								"EDIT_TEMPLATE" => ""
							),
							false
						);?>
          		</div>
        	</div>
        	<!-- /.main-text-blocks-1 -->
        	<!-- COURSES-SLIDER -->
	        <?
	        $arPriceCode = array("Розничная");
	        if ($partnerPrice = $GLOBALS["partnerSchool"]->getPriceCode())
	        {
	        	$arPriceCode[] = $partnerPrice;
	        	$r_o_p_c = $partnerPrice;
	        }
	        $APPLICATION->IncludeComponent(
				"oc:product.slider",
				"",
				Array(
					"IBLOCK_TYPE" => "",
					"IBLOCK_ID" => "19",
					"AMOUNT_OF_SLIDES" => "5",
					"SECTION_CODE" => "",
					"PRICE_CODE" => $arPriceCode,
					"REGION_OWN_PRICE_CODE" => $r_o_p_c,
					"TITLE" => "Наши курсы",
					"SITE_ID" => 's1',
					"SORT_BY1" => "",
					"SORT_ORDER1" => "",
					"SORT_BY2" => "",
					"SORT_ORDER2" => "",
					"FILTER_NAME" => "",
					"FIELD_CODE" => array("",""),
					"CHECK_DATES" => "Y",
					"CACHE_TYPE" => "Y",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "Y",
					"SET_STATUS_404" => "N",
					"SITE_PATH_BEFORE_DETAIL_PAGE_URL" => '/courses'
				)
			);?>
			<!-- /.courses-slider-->
        	<!-- MAIN-TEXT-BLOCK-2 -->
        	<div class="row main-text-block">
          		<div class="col-md-5">
          			<?$APPLICATION->IncludeComponent(
							"bitrix:main.include", 
							".default", 
							array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => SITE_DIR."include/main_page_text_block_2_img.php",
								"EDIT_TEMPLATE" => ""
							),
							false
						);?>
          		</div>
          		<div class="col-md-7">
          			<?$APPLICATION->IncludeComponent(
							"bitrix:main.include", 
							".default", 
							array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => SITE_DIR."include/main_page_text_block_2_content.php",
								"EDIT_TEMPLATE" => ""
							),
							false
						);?>
          		</div>
        	</div>
        	<!-- /.main-text-blocks-2 -->
        	<!-- PRODUCTS-SLIDER -->
	       	<?$APPLICATION->IncludeComponent(
				"oc:product.section.slider",
				"",
				Array(
					"IBLOCK_TYPE" => "1c_catalog",
					"IBLOCK_ID" => "16",
					"AMOUNT_OF_SLIDES" => "5",
					"TITLE" => "Разделы продукции",
					"SORT_BY1" => "SORT",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "",
					"FIELD_CODE" => array("",""),
					"CHECK_DATES" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "Y",
					"SET_STATUS_404" => "N",
					"SECTION_LEVEL" => "1" 
				)
			);?>
	        <!-- /.products-slider-->
        </div>
    	<!-- /.container -->
       	
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>