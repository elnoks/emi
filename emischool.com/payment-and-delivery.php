<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оплата и доставка");
?><div class="bx_page">
	<div class="h1-top">
		<h1>Payment and delivery</h1>
	</div>
	<p>
		<br>
	</p>
	<p>
 <b>1&nbsp;&nbsp; &nbsp; Prices</b>
	</p>
	<p>
		 1.1&nbsp;&nbsp;All the e-shop prices include 21% VAT.
	</p>
	<p>
		 1.2&nbsp;&nbsp;Products are supplied without 21% VAT to non-EU countries.
	</p>
	<p>
		 1.3&nbsp;&nbsp;For all orders over 500 EUR is valid 5% discount (discount is not fundable and is valid only for one order).
	</p>
	<p>
		 1.4&nbsp;&nbsp;Prices do not include delivery expenses.
	</p>
	<p>
		 1.5&nbsp;&nbsp;All taxes and other possible expenses in accordance with Client’s country laws pays Client.
	</p>
	<p>
		<br>
	</p>
	<p>
	</p>
	<p>
 <b>2&nbsp;&nbsp; &nbsp;Payment</b>
	</p>
	<p>
		 2.1 All payments are effected on the basis of issued invoice.
	</p>
	<p>
		 2.2 Before paying the invoice please check if products and products amounts in the invoice are correct.
	</p>
	<p>
		 2.3 We accept payments via:
	</p>
	<p>
	</p>
	<ul>
		<li>
		<p>
			 Bank transfer to our account;
		</p>
 </li>
		<li>
		<p>
			 Western Union;
		</p>
 </li>
		<li>
		<p>
			 Pay with my credit or debit card;
		</p>
 </li>
		<li>
		<p>
			 PayPal.
		</p>
 </li>
	</ul>
	<div>
		<br>
	</div>
	<p>
	</p>
	<p>
	</p>
	<p>
 <b>3&nbsp;&nbsp; &nbsp;Shipping conditions</b>
	</p>
	<p>
		 3.1 Products are shipped internationally by TNT Transporting Company.
	</p>
	<p>
		 3.2 Shipping rates are not fixed and depend on package weight and shipping destination.
	</p>
	<p>
		 Transport expenses are counted and shown in the issued invoice (POST).
	</p>
	<p>
		 3.3 Packaging and shipping are processed within 3 business days after receiving of the payment.
	</p>
	<p>
		 3.4 All shipping terms are estimated and depend on shipping destination.
	</p>
	 When order is shipped, TNT Transporting Company sends Client an email with a web link for tracking of the order and tracking number.<br>
 <br>
	<p>
 <br>
	</p>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>