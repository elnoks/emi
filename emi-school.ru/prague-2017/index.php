<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Прага 2017");
?>
    <div class="top-wrapper">
        <div class="bredcrumbs">

        </div>
        <div class="h1-top">
            <h1>Прага 2017</h1>
        </div>
        <br>
    </div>
    <section class="headers text-center">
        <div class="row">
            <h1>Добро пожаловать в краткий путеводитель по праге</h1>
            <h3>Специально для вас мы подготовили самую необходимую информацию и подобрали интересные места.</h3>
            <p>Мы расскажем Вам, какими способами можно передвигаться по Праге. Интерактивная карта поможет с легкостью ориентироваться на местности и содержит краткую информацию о ресторанах и достопримечательностях.</p>
            <a class="button_prague_2017" href="#interact">Перейти сразу к карте</a>
            <p class="bold">Пожалуйста, ознакомьтесь с важной информацией о приобретении билетов и передвижении по городу, расположенной ниже:</p>
        </div>
    </section>
    <script>
        $(document).ready(function(){
            $("body").on('click', '[href*="#"]', function(e){
                var fixed_offset = 100;
                $('html,body').stop().animate({ scrollTop: $(this.hash).offset().top - fixed_offset }, 1000);
                e.preventDefault();
            });
        });
    </script>
    <section class="info text-center">
        <h2>Как проехать?</h2>
        <div class="row text-left">
            <div class="col-md-6">
                <i class="fa fa-bus" aria-hidden="true"></i><h3>Общественный транспорт</h3>
                <p class="top">Все билеты в Чехии действуют определенное время: 30 минут, 90 минут, 1 день и 3 дня.</p>
                <p>Они стоят 24, 32, 110 и 310 крон соответственно. Билеты можно купить в специальных автоматах, а также в магазинчиках «Relay», которые есть практически на каждой станции метро/трамвайной остановке. После покупки билет обязательно нужно пробить в специальном аппарате: в метро он находится на входе, около эскалаторов, в автобусах и трамваях – внутри салона. Билет необходимо хранить на протяжении всей поездки.</p>
                <p>Вы не только должны зайти в транспорт с действующим пробитым билетом, но и выйти из него вы также обязаны с еще действующим билетом. То есть при времени поездки в 35 минут вам необходимо купить билет на 90 минут во избежание штрафов и проблем</p>
                <p class="top">Транспорт разделяется на ночной и дневной.</p>
                <p>Сориентироваться очень просто: номера ночных автобусов и трамваев всегда начинаются с девятки, причем у трамваев 2х-значные номера (например, 98 маршрут), а у автобусов – 3х-значные (например, 908 маршрут).</p>

            </div>

            <div class="col-md-6">
                <div class="subway">
                    <img src="/bitrix/templates/.default/img/maps-cost-prague-2017.jpg" alt="Карта метро в Праге">
                </div>
                <br><br>
                <div class="subway">
                    <img src="/bitrix/templates/.default/img/bus-stop-praha.jpg" alt="Автобусная остановка в Праге">
                </div>
            </div>

            <div class="col-md-12">
                <p>На каждой остановке трамвая и автобуса висит подробное расписание для каждого маршрута, на котором указаны остановки, расстояние до каждой из них, а также расписание, по которому ездит маршрут. Расписание разбито на 2 части: расписание в будние дни и расписание в выходные дни. В выходные дни транспорт ходит реже.</p>
                <p>Название остановки, где вы находитесь, выделено жирным шрифтом. Остановки ниже вашей остановки – те, до которых едет тот или иной маршрут в том направлении, в котором вы стоите. Остановки выше – те, до которых едет тот или иной маршрут в противоположном вам направлении. </p>
                <p class="top">Также скоро будут праздники:</p>
                <ul>
                    <li><strong>5 июля</strong> - День славянских святых Кирилла и Мефодия*</li>
                    <li><strong>6 июля</strong> - День казни Яна Гуса*</li>
                </ul>
                <p class="small-text">* Государственные праздники, соответственно общественный транспорт будет ходить по расписание воскресного дня.</p>
            </div>

        </div>


        <div class="row margin-top"></div>

        <div class="row text-left background-row">
            <div class="padding-row">
                <div class="col-md-6">
                    <i class="fa fa-subway" aria-hidden="true"></i><h3>Метро</h3>
                    <p class="top">Перемещение по городу посредством метрополитена - пожалуй, самый быстрый способ.</p>
                    <p class="top">График работы:</p>
                    <p>Метро начинает работать ранним утром, в 4:34 . Именно в это время с конечной станции Letňany выходит первый поезд. Заканчивает работу метрополитен в 00:40 (на этой же станции). Максимальное время от одной станции до другой составляет 2 минуты. Приблизительный интервал движения поездов в час пик составляет около 2-3 минут, в другое время днём необходимый поезд нужно будет подождать от 5 до 10 минут, а вечером - 10-12 минут.</p>
                    <p class="top">Станции пересечения линий пражского метро:</p>
                    <p>В Пражском метро существует три станции, на которых можно осуществить пересадку.</p>
                    <ul>
                        <li>Mustek - переход между <span class="yellow">желтой</span> и <span class="green">зеленой</span> веткой метро</li>
                        <li>Muzeum - переход между <span class="red">красной</span> и <span class="green">зеленой</span> веткой метро</li>
                        <li>Florenc - переход между <span class="yellow">желтой</span> и <span class="red">красной</span> веткой метро</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="subway">
                        <img src="/bitrix/templates/.default/img/maps-subway-prague-2017.jpg" alt="Карта метро в Праге">

                        <div class="text-center">
                            <p class="small-text">* Ближайшая станция метро к отелю – Florenc</p>
                            <a href="/bitrix/templates/.default/img/maps-subway-prague-2017-big.jpg" download="Катра метро Прага" class="button_prague_2017_white">Скачать карту метро</a>
                        </div>
                    </div>
<!--                    <div>-->
<!--                        <p class="top">Метро Праги на русском языке:</p>-->
<!--                        <p>Ознакомьтесь с основными терминами, которые вам пригодятся при использовании Пражского метро. Их вы услышите и увидите спустившись в подземное царство метрополитена.</p>-->
<!--                        <ul>-->
<!--                            <li>eskalátor - эскалатор</li>-->
<!--                            <li>linka (A, B, C) - линия (A, B, C)</li>-->
<!--                            <li>metro - метро</li>-->
<!--                            <li>přestup - переход</li>-->
<!--                            <li>stanice metra - станция метро</li>-->
<!--                            <li>vstup - вход</li>-->
<!--                            <li>výstup - выход</li>-->
<!--                            <li>pozor — внимание</li>-->
<!--                            <li>provoz přerušen — движение приостановлено</li>-->
<!--                            <li>Ukončete prosím výstup a nástup, dveře se zavírají - Пожалуйста, окончите высадку и посадку, двери закрываются.</li>-->
<!--                            <li>Příští stanice: [название станции] - Следующая станция: [название станции]</li>-->
<!--                            <li>Přestup na linku A/B/C - Переход на линию A/B/C</li>-->
<!--                            <li>[название станции], přestupní stanice - [название станции], пересадочная станция</li>-->
<!---->
<!--                        </ul>-->
<!--                    </div>-->

                </div>
                <div class="col-md-12">
                    <p class="top">Метро Праги на русском языке:</p>
                    <p>Ознакомьтесь с основными терминами, которые вам пригодятся при использовании Пражского метро. Их вы услышите и увидите спустившись в подземное царство метрополитена.</p>
                </div>
                <div class="col-md-6">
                    <ul>
                        <li>eskalátor - эскалатор</li>
                        <li>linka (A, B, C) - линия (A, B, C)</li>
                        <li>metro - метро</li>
                        <li>přestup - переход</li>
                        <li>stanice metra - станция метро</li>
                        <li>vstup - вход</li>
                        <li>výstup - выход</li>
                        <li>pozor — внимание</li>

                    </ul>
                </div>
                <div class="col-md-6">
                    <ul>
                        <li>provoz přerušen — движение приостановлено</li>
                        <li>Ukončete prosím výstup a nástup, dveře se zavírají - Пожалуйста, окончите высадку и посадку, двери закрываются.</li>
                        <li>Příští stanice: [название станции] - Следующая станция: [название станции]</li>
                        <li>Přestup na linku A/B/C - Переход на линию A/B/C</li>
                        <li>[название станции], přestupní stanice - [название станции], пересадочная станция</li>
                    </ul>
                </div>


            </div>
        </div>

        <div class="row margin-bottom"></div>

        <div class="row text-left">
            <div class="col-md-6">
                <i class="fa fa-taxi" aria-hidden="true"></i>
                <h3>Такси</h3>
                <p> +420 212 290 290, +420 773 183 161 (Веселое такси) RU<br>
                    +420 773-745-176 (Rutaxi.cz) RU<br>
                    +420 776 683 043 (Taxi в Праге) RU<br>
                    +420 257 257 257 (City TAXI) ENG<br>
                    (+420) 737 222 333 (Modrý Anděl) ENG</p>
                <p>Также в Праге, популярен UBER. Использование данного приложения, исключит необходимость разговаривать с диспетчерами такси.</p>
                <p>Чаевые составляют примерно 10% от стоимости (как и во всей Европе), но в Чехии существует свое правило: вы не оставляете чаевые после оплаты заказа перед уходом, а называете при оплате сумму, в которую сразу включено ваше вознаграждение таксисту или официанту.</p>
            </div>
            <div class="col-md-6">
                <div class="taxi">
                    <img src="/bitrix/templates/.default/img/taxi-prague-2017.jpg" alt="Такси в Праге">
                </div>
            </div>


        </div>
    </section>

    <section class="cost text-center">
        <h2>Примерные цены</h2>
        <div class="row text-center">
            <div class="table">
                <div class="table-row">
                    <div class="table-col">ОБЕД</div>
                    <div class="table-col">OBĚD</div>
                    <div class="table-col">250-300 КРОН (СУП + ОСНОВНОЕ БЛЮДО)</div>
                </div>
                <div class="table-row">
                    <div class="table-col">УЖИН</div>
                    <div class="table-col">VEČEŘE</div>
                    <div class="table-col">250-300 КРОН (ОСНОВНОЕ БЛЮДО + ДЕСЕРТ)</div>
                </div>
                <div class="table-row">
                    <div class="table-col">САЛАТЫ</div>
                    <div class="table-col">Saláty</div>
                    <div class="table-col">150-180 КРОН</div>
                </div>
                <div class="table-row">
                    <div class="table-col">ПИВО</div>
                    <div class="table-col">Pivo</div>
                    <div class="table-col">40 КРОН</div>
                </div>
                <div class="table-row">
                    <div class="table-col">КОФЕ</div>
                    <div class="table-col">KÁVA</div>
                    <div class="table-col">60 КРОН</div>
                </div>
                <div class="table-row">
                    <div class="table-col">Домашний лимонад</div>
                    <div class="table-col">Domácí limonáda</div>
                    <div class="table-col">60 КРОН</div>
                </div>
                <div class="table-row">
                    <div class="table-col">КОКТЕЙЛИ</div>
                    <div class="table-col">KOKTEJLY</div>
                    <div class="table-col">150 КРОН</div>
                </div>
                <div class="table-row">
                    <div class="table-col">КАЛЬЯН</div>
                    <div class="table-col">Vodní dýmka</div>
                    <div class="table-col">250-350 КРОН</div>
                </div>
            </div>
        </div>

    </section>

    <section class="dictionary text-center">
        <h2>ОСНОВНОЙ СЛОВАРЬ</h2>
        <a target="_blank" href="/upload/dictionary_chesh.pdf" class="button_prague_2017">Чешский словарь (pdf)</a>
    </section>

    <section class="map text-center">
        <h2 id="interact">Интерактивная карта</h2>
        <div class="row text-center">
            <iframe class="maps" src="https://www.google.com/maps/d/embed?mid=132ZJJvnmRqlh7EBJSeD2d2Otjgo" ></iframe>
            <a target="_blank" href="https://www.google.com/maps/d/viewer?mid=132ZJJvnmRqlh7EBJSeD2d2Otjgo&ll=50.08217659282073%2C14.416613764648446&z=14" class="button_prague_2017">Показать полную карту</a>
        </div>
    </section>



<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>