<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION;

   $aMenuLinksExt=$APPLICATION->IncludeComponent(
	"emi-school:menu.sections",
	"",
	array(
		"IS_SEF" => "N",
		"ID" => $_REQUEST["ID"],
		"IBLOCK_TYPE" => "1c_catalog",
		"IBLOCK_ID" => CATALOG_IBLOCK_ID,
		"SECTION_URL" => "",
		"DEPTH_LEVEL" => "4",
		"CACHE_TYPE" => "Y",
		"CACHE_TIME" => "36000000"
	),
	false
);
$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>