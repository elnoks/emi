<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

/////////////////////////////// Password protect for order.emi-school.ru //////////////////////////
if (SITE_ID == 'eo') {
define('ADMIN_USERNAME','admin'); 	// Admin Username
define('ADMIN_PASSWORD','KlsHJnd2gD');  	// Admin Password

    if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW']) ||
        $_SERVER['PHP_AUTH_USER'] != ADMIN_USERNAME ||$_SERVER['PHP_AUTH_PW'] != ADMIN_PASSWORD) {
        Header("WWW-Authenticate: Basic realm=\"Memcache Login\"");
        Header("HTTP/1.0 401 Unauthorized");

        echo <<<EOB
                    <html><body>
                    <h1>Rejected!</h1>
                    <big>Wrong Username or Password!</big>
                    </body></html>
EOB;
        exit;
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////

$APPLICATION->SetTitle(GetMessage("MAIN_PAGE_META_TITLE"));
$APPLICATION->SetPageProperty("description", GetMessage("MAIN_PAGE_META_DESCRIPTION"));
?>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

    <!-- CAROUSEL==================================================-->

<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "main_page_slider",
    Array(
        "IBLOCK_TYPE" => MAIN_PAGE_NEWS_SLIDER_IBLOCK_TYPE,
        "IBLOCK_ID" => MAIN_PAGE_NEWS_SLIDER_IBLOCK_ID,
        "NEWS_COUNT" => "25",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "",
        "SORT_ORDER2" => "",
        "FILTER_NAME" => "",
        "FIELD_CODE" => array("DETAIL_PICTURE",""),
        "PROPERTY_CODE" => array("SHOW","TOP_MARGIN","LEFT_MARGIN","TEXT_BLOCK_CAPTION","TEXT_BLOCK_CAPTION_COLOR","TEXT","TEXT_COLOR","BUTTON_TEXT","BUTTON_TEXT_COLOR","BUTTON_COLOR","LINK",""),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "N",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "N",
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "N",
        "PAGER_TEMPLATE" => "",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N"
    )
);?>
    <!-- /.carousel -->
    <!-- CONTENT==================================================-->
    <div class="container container-main-page-bg">
        <!-- 4 blocks -->
        <div class="four-blocks-container">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 block ">
                    <a href="<?=CURRENT_SITE_URL?>/gallery/" class="shadow">
                        <div class="h2"><?=GetMessage("MAIN_PAGE_4BLOCKS_VIDEO")?></div>
                        <?//if (LANGUAGE_ID=="ie"){?>
                        <?//}else{?>
                        <?$APPLICATION->IncludeComponent(
                            "emi:youtube.lastvideo",
                            ".default",
                            array(
                                "FEED_TYPE" => "PLAYLIST",
                                "FEED_ID" => YOUTUBE_GALLERY_SECOND_PLAYLIST, //Шабанов 28-08-2015, свой плейлист для каждого региона
                                "COMPONENT_TEMPLATE" => ".default"
                            ),
                            false
                        );?>
                        <?//}?>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 block">
                    <a href="<?=CURRENT_SITE_URL?>/gallery/#masterclas" class="shadow">
                        <div class="h2"><?=GetMessage("MAIN_PAGE_4BLOCKS_MASTER_CLASS")?></div>

                        <?$APPLICATION->IncludeComponent(
                            "emi:youtube.lastvideo",
                            ".default",
                            array(
                                "FEED_TYPE" => "PLAYLIST",
                                "FEED_ID" => YOUTUBE_VIDEO
                            ),
                            false
                        );?>

                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 block">
                    <a href="/contacts/" class="shadow">
                        <div class="h2"><?=GetMessage("MAIN_PAGE_4BLOCKS_WHERE_BUY")?></div>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            ".default",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => CURLANG_INCLUDE_PATH . "main_page_block_where_to_buy.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            false
                        );?>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 block">
                    <?
                    if (LANGUAGE_ID=="ru" OR LANGUAGE_ID=="en" OR LANGUAGE_ID=="tn" OR LANGUAGE_ID=="cz" OR LANGUAGE_ID=="uk" OR LANGUAGE_ID=="gr" OR LANGUAGE_ID=="lt" OR LANGUAGE_ID=="es" OR LANGUAGE_ID=="ae" OR LANGUAGE_ID=="ch" OR LANGUAGE_ID=="cy"
                        OR LANGUAGE_ID=="ee" OR LANGUAGE_ID=="fr" OR LANGUAGE_ID=="ie" OR LANGUAGE_ID=="kr" OR LANGUAGE_ID=="lv" OR LANGUAGE_ID=="pl" OR LANGUAGE_ID=="ro"){
                        ?>
                        <a href="/gallery/#flipbook" class="shadow">
                            <div class="h2"><?=GetMessage("MAIN_PAGE_4BLOCKS_SOCIAL")?></div>
                            <div class="block-container">
                                <div class="block-img-container main-instagram">
                                    <img src="<?=GetMessage("MAIN_PAGE_4BLOCKS_FLIPBOOK_IMG")?>" alt="">
                                </div>
                            </div>
                        </a>
                        <?

                    }
                    else{
                        ?>
                        <a href="/social/" class="shadow">
                            <div class="h2"><?=GetMessage("MAIN_PAGE_4BLOCKS_SOCIAL")?></div>
                            <div class="block-container">
                                <div class="block-img-container main-instagram">
                                    <?$APPLICATION->IncludeComponent(
                                        "prgrant:instagram.gallery.last_image",
                                        ".default",
                                        array(
                                            "USERNAME" => "emi_official_world",
                                            "IS_JQUERY" => "N"
                                        ),
                                        false
                                    );?>
                                </div>
                            </div>
                        </a>

                    <?}?>

                </div>
            </div>
        </div>
        <hr class="featurette-divider">
        <!-- /. 4 blocks -->

        <?
        if (LANGUAGE_ID=='ru' || LANGUAGE_ID=='en' || LANGUAGE_ID=='cz' || LANGUAGE_ID=='uk' || LANGUAGE_ID=='ie'|| LANGUAGE_ID=='gr'){
        ?>

            <!-- MAIN-TEXT-BLOCK-2 -->
            <div class="row main-text-block">
                <div class="col-md-5 col-sm-5">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_2_img_new.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
                <div class="col-md-7 col-sm-7">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_2_content.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
            </div>
            <!-- /.main-text-blocks-2 -->

            <!-- MAIN-TEXT-BLOCK-STARS -->
            <div class="row main-text-block">
                <div class="col-md-12 col-sm-12">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_stars_content.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
                <div class="col-md-12 col-sm-12">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_stars_img.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
            </div>
            <!-- /.main-text-blocks-stars -->

            <!-- MAIN-TEXT-BLOCK-WEEKS -->
            <div class="row main-text-block">
                <div class="col-md-12 col-sm-12">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_weeks_content.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
                <div class="col-md-12 col-sm-12">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_weeks_img.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
            </div>
            <!-- /.main-text-blocks-weeks -->

            <!-- MAIN-TEXT-BLOCK-MANICURE -->
            <div class="row main-text-block">
                <div class="col-md-5 col-sm-5">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_manicure_content.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
                <div class="col-md-7 col-sm-7">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_manicure_img.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
            </div>

            <?

                $arRegions = $GLOBALS["partnerShop"]->getRegionsFromIBlock();
                $curId = $GLOBALS["partnerShop"]->getCurrentRegionId();
                foreach ($arRegions as $reg){
                    $regions['NAME'][]=$reg['NAME'];
                    if($reg['REGION_ID']==$curId) {
                        $regions['curNAME']=$reg['NAME'];
                    }
                }
                sort($regions['NAME']);
                if ($USER->IsAuthorized()) {
                    $curuser['mail']=$USER->GetEmail();
                    $curuser['fio']=$USER->GetFullName();
                    $filter = Array("LOGIN" => $USER->GetLogin());
                    $rsUsers = CUser::GetList(($by = "NAME"), ($order = "asc"), $filter);
                    while ($arUser = $rsUsers->Fetch()) {
                        $curuser['tel']=$arUser['PERSONAL_MOBILE'];
                    }

                }
                CJSCore::Init();
                ?>
<?/*
<!---->
<!--    <div style="text-align: center; margin: 0 15px; width:500px; " class="popupColl" id="popupColl" style="display:none;">-->
<!--                <form class="getCollectionFormFirst" id="getCollectionFormFirst"  method="POST" style="text-align: center">-->
<!--                    <a class="close"></a>-->
<!--                    <h1>Получить коллекцию в мой салон:</h1><br>-->
<!--                    <div id="table-tent-slider-popup" class="owl-carousel owl-theme col-md-6" onclick="return true;">-->
<!--                        <div class="item">-->
<!--                            <a id="table_tent_1" href="#">-->
<!--                                <img style="height: 300px;" src="include/img/tabletent/table_tent_1.png" alt="">-->
<!--                            </a>-->
<!--                            <br>-->
<!--                            <span class="slide-text">-->
<!--							Коллекция E.Mi-маникюр весна-лето 2018-->
<!--						</span>-->
<!--                        </div>-->
<!--                        <div class="item">-->
<!--                            <a id="table_tent_2" hreg">-->
<!--                            <img style="height: 300px;" src="include/img/tabletent/table_tent_2.png" alt="">-->
<!--                            </a>-->
<!--                            <br>-->
<!--                            <span class="slide-text">-->
<!--							Коллекция E.Mi-маникюр весна-лето 2018-->
<!--						</span>-->
<!--                        </div>-->
<!--                        <div class="item">-->
<!--                            <a id="table_tent_3" hreg">-->
<!--                            <img style="height: 300px;" src="include/img/tabletent/table_tent_3.png" alt="">-->
<!--                            </a>-->
<!--                            <br>-->
<!--                            <span class="slide-text">-->
<!--							Коллекция E.Mi-маникюр весна-лето 2018-->
<!--						</span>-->
<!--                        </div>-->
<!--                        <div class="item">-->
<!--                            <a id="table_tent_4" hreg">-->
<!--                            <img style="height: 300px;" src="include/img/tabletent/table_tent_4.png" alt="">-->
<!--                            </a>-->
<!--                            <br>-->
<!--                            <span class="slide-text">-->
<!--							Коллекция E.Mi-маникюр весна-лето 2018-->
<!--						</span>-->
<!--                        </div>-->
<!--                        <div class="item">-->
<!--                            <a id="table_tent_5" hreg">-->
<!--                            <img style="height: 300px;" src="include/img/tabletent/table_tent_5.png" alt="">-->
<!--                            </a>-->
<!--                            <br>-->
<!--                            <span class="slide-text">-->
<!--							Коллекция E.Mi-маникюр весна-лето 2018-->
<!--						</span>-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                    <br>-->
<!--                    <select style="margin-bottom: 15px; width:200px" name="region">-->
<!--                        --><?//
//                        foreach ($regions['NAME'] as $regs) {
//                            if($regs!=$regions['curNAME']) {
//                                ?>
<!--                                <option value="--><?//=$regs?><!--">--><?//=$regs?><!--</option>-->
<!--                                --><?//
//                            } else {
//                                ?>
<!--                                <option selected value="--><?//=$regs?><!--">--><?//=$regs?><!--</option>-->
<!--                                --><?//
//                            }
//                        }
//                        ?>
<!--                    </select>-->
<!--                    <input style="margin-bottom: 15px; width:200px" type="text" id='fio' name="name" placeholder="ФИО" required value="--><?//=$curuser['fio']?><!--"><br>-->
<!--                    <input style="margin-bottom: 15px; width:200px" type="text" id="phone" name="phone" placeholder="+7(999)999-99-99" required value="--><?//=$curuser['tel']?><!--"><br>-->
<!--                    <input style="margin-bottom: 15px; width:200px" type="text" id="mail" name="mail" placeholder="Электронная почта" required value="--><?//=$curuser['mail']?><!--"><br>-->
<!--                    <input type="hidden" id="Coll" name="Coll" value="ffColl"><br>-->
<!--                    <span id="valid"></span>-->
<!--                    <br>-->
<!--                    <button class="btn-pink" id="getCollBTN">Получить</button>-->
<!--                </form>-->
<!--    </div>-->
<!---->
<!---->
<!--                <div class="thanksCol" id="gift-modal-thanksCol" style="display:none; text-align: center">-->
<!--                    <a class="close"></a>-->
<!--                    <h1>Осталось только уточнить, куда доставить...</h1><br>-->
<!--                    <p style="font-size: 18px;">Электронная версия коллекции уже у вас на e-mail. </br>-->
<!--                        Наши менеджеры свяжутся с вами для уточнения данных доставки. </p></br>-->
<!--                    <form class="getCollectionFormSec" id="getCollectionFormSec" action="getColl.php" method="POST" style="text-align: center">-->
<!--                        <a class="close"></a>-->
<!--                        <input style="margin-bottom: 15px; width:200px" type="text" id='salon_name' name="salon_name" placeholder="Название салона" required"><br>-->
<!--                        <input style="margin-bottom: 15px; width:200px" type="text" id="salon_adress" name="salon_adress" placeholder="Адрес доставки" required"><br>-->
<!--                        <br>-->
<!--                        <button class="btn-pink" id="getCollBTNSec">Отправить</button>-->
<!--                    </form>-->
<!--                </div>-->
<!---->
<!--                <div class="thanks" id="gift-modal-thanks" style="display:none; text-align: center">-->
<!--                    <a class="close"></a>-->
<!--                    <h1>Данные отправлены</h1><br>-->
<!--                    <p style="font-size: 18px;">Ваша коллекция уже в пути.<br />-->
<!--                        А пока вы можете скачать-->
<!--                        <a style="font-size: 25px !important;" href="https://yadi.sk/i/XLj_rvER3WjEay" target="_blank">дайджест модного маникюра 2018</a></p><br>-->
<!--                    <button class="btn-pink" id="close">Закрыть</button>-->
<!--                </div>-->
*/?>
            <!-- /.main-text-blocks-manicure -->
            <!-- COURSES-SLIDER -->
            <?

            if ($GLOBALS["partnerSchool"]->haveSchoolPartner) {
                $APPLICATION->IncludeComponent(
                    "oc:product.slider",
                    "",
                    Array(
                        "IBLOCK_TYPE" => "",
                        "IBLOCK_ID" => COURSES_IBLOCK_ID,
                        "AMOUNT_OF_SLIDES" => "5",
                        "SECTION_CODE" => "",
                        "PRICE_CODE" => array(
                            0 => $GLOBALS["partnerSchool"]->getPriceCode(),
                        ),
                        "TITLE" => GetMessage("MAIN_PAGE_SLIDER_COURSES"),
                        "SITE_ID" => SITE_ID,
                        "SORT_BY1" => "",
                        "SORT_ORDER1" => "",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array("", ""),
                        "CHECK_DATES" => "Y",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "SET_STATUS_404" => "N",
                        "SITE_PATH_BEFORE_DETAIL_PAGE_URL" => '/courses'
                    )
                );
            } ?>
            <!-- /.courses-slider-->
            <!-- MAIN-TEXT-BLOCK-1 -->
            <div class="row main-text-block">
                <div class="col-md-5 col-sm-5">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_1_img_new.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
                <div class="col-md-7 col-sm-7">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_1_content.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
            </div>
            <!-- /.main-text-blocks-1 -->
            <!-- MAIN-TEXT-BLOCK-3 -->
            <div class="row main-text-block">
                <div class="col-md-7 col-sm-7">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_3_content.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
                <div class="col-md-5 col-sm-5">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_3_img.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
            </div>
                <?
                } else {
                ?>


            <!-- /.main-text-blocks-3 -->


<!--     OLD MAIN-->
            <div class="row main-text-block">
                <div class="col-md-7 col-sm-7">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_1_content.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
                <div class="col-md-5 col-sm-5">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_1_img.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
            </div>

            <?
            if ($GLOBALS["partnerSchool"]->haveSchoolPartner) {
                $APPLICATION->IncludeComponent(
                    "oc:product.slider",
                    "",
                    Array(
                        "IBLOCK_TYPE" => "",
                        "IBLOCK_ID" => COURSES_IBLOCK_ID,
                        "AMOUNT_OF_SLIDES" => "5",
                        "SECTION_CODE" => "",
                        "PRICE_CODE" => array(
                            0 => $GLOBALS["partnerSchool"]->getPriceCode(),
                        ),
                        "TITLE" => GetMessage("MAIN_PAGE_SLIDER_COURSES"),
                        "SITE_ID" => SITE_ID,
                        "SORT_BY1" => "",
                        "SORT_ORDER1" => "",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array("", ""),
                        "CHECK_DATES" => "Y",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "SET_STATUS_404" => "N",
                        "SITE_PATH_BEFORE_DETAIL_PAGE_URL" => '/courses'
                    )
                );
            } ?>

            <div class="row main-text-block">
                <div class="col-md-5 col-sm-5">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_2_img.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
                <div class="col-md-7 col-sm-7">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => CURLANG_INCLUDE_PATH . "main_page_text_block_2_content.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    ); ?>
                </div>
            </div>

            <?
}
            $APPLICATION->IncludeComponent("oc:product.section.slider", "product.section.slider", Array(
                "IBLOCK_TYPE" => "1c_catalog",    // Тип информационного блока (используется только для проверки)
                "IBLOCK_ID" => CATALOG_IBLOCK_ID,    // Код информационного блока
                "AMOUNT_OF_SLIDES" => "5",    // Количество элементов в слайдере
                "TITLE" => GetMessage("MAIN_PAGE_SLIDER_PRODUCTS"),
                "SORT_BY1" => "SORT",    // Поле для первой сортировки
                "SORT_ORDER1" => "DESC",    // Направление для первой сортировки
                "SORT_BY2" => "SORT",    // Поле для второй сортировки
                "SORT_ORDER2" => "ASC",    // Направление для второй сортировки
                "FILTER_NAME" => "",    // Фильтр
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                "CACHE_TYPE" => "A",    // Тип кеширования
                "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                "CACHE_FILTER" => "Y",    // Кешировать при установленном фильтре
                "SET_STATUS_404" => "N",    // Устанавливать статус 404, если не найдены элемент или раздел
                "SECTION_LEVEL" => "1",    // Уровень вложенности раздела
            ),
                false
            );

        ?>

    </div>
<?
/*
    <!-- /.container -->
<!--    <script>-->
<!--        $(function(){-->
<!--            $("#phone").mask("+7 (999) 999-99-99");-->
<!--        });-->
<!--        $(document).ready(function(){-->
<!--            var pattern = /^[a-z0-9_.-]+@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/i;-->
<!--            var mail = $('#mail');-->
<!---->
<!--            mail.blur(function(){-->
<!--                if(mail.val() != ''){-->
<!--                    if(mail.val().search(pattern) == 0){-->
<!--                        $('#valid').text('');-->
<!--                        $('#getCollBTN').attr('disabled', false);-->
<!--                        mail.removeClass('error').addClass('ok');-->
<!--                    }else{-->
<!--                        $('#valid').text('Пожалуйста проверьте правильность введенной почты');-->
<!--                        $('#getCollBTN').attr('disabled', true);-->
<!--                        mail.addClass('ok');-->
<!--                    }-->
<!--                }else{-->
<!--                    $('#valid').text('Поле e-mail не должно быть пустым!');-->
<!--                    mail.addClass('error');-->
<!--                    $('#getCollBTN').attr('disabled', true);-->
<!--                }-->
<!--            });-->
<!--        });-->
<!---->
<!--        BX.ready(function() {-->
<!--            $(document).ready(function() {-->
<!--                $("#table-tent-slider-popup").owlCarousel({-->
<!--                    responsiveClass: true,-->
<!--                    responsive: {-->
<!--                        0: {-->
<!--                            items: 1-->
<!--                        },-->
<!--                        600: {-->
<!--                            items: 1-->
<!--                        },-->
<!--                        1000: {-->
<!--                            items: 1-->
<!--                        }-->
<!--                    },-->
<!--                    loop: true,-->
<!--                    autoHeight: true,-->
<!--                    stagePadding: 0,-->
<!--                    autoPlay: true,-->
<!--                    slideSpeed : 300,-->
<!--                    paginationSpeed : 400,-->
<!--                    singleItem:true,-->
<!--                    lazyLoad : true,-->
<!--                    margin: 30,-->
<!--                    navigation: false,-->
<!--                    pagination: false,-->
<!--                    dots: false-->
<!--                });-->
<!---->
<!--            });-->
<!--            var getColl = new BX.PopupWindow('call_feedback', window.body, {-->
<!--                offsetTop: 220,-->
<!--                offsetLeft: 0,-->
<!--                lightShadow: true,-->
<!--                closeIcon: true,-->
<!--                closeByEsc: true,-->
<!--                overlay: {-->
<!--                    backgroundColor: '#333333', opacity: '85'-->
<!--                }-->
<!--            });-->
<!--            var getCollThanks = new BX.PopupWindow('call_feedback', window.body, {-->
<!--                offsetTop: 220,-->
<!--                offsetLeft: 0,-->
<!--                lightShadow: true,-->
<!--                closeIcon: true,-->
<!--                closeByEsc: true,-->
<!--                overlay: {-->
<!--                    backgroundColor: '#333333', opacity: '85'-->
<!--                }-->
<!--            });-->
<!--            var collThanks = new BX.PopupWindow('call_feedback', window.body, {-->
<!--                offsetTop: 220,-->
<!--                offsetLeft: 0,-->
<!--                lightShadow: true,-->
<!--                closeIcon: true,-->
<!--                closeByEsc: true,-->
<!--                overlay: {-->
<!--                    backgroundColor: '#333333', opacity: '85'-->
<!--                }-->
<!--            });-->
<!--            getColl.setContent(BX('popupColl'));-->
<!--            BX.bindDelegate(-->
<!--                document.body, 'click', {className: 'showGetColl' },-->
<!--                BX.proxy(function(e){-->
<!--                    if(!e)-->
<!--                        e = window.event;-->
<!--                    getColl.show();-->
<!--                    return BX.PreventDefault(e);-->
<!--                }, getColl)-->
<!--            );-->
<!---->
<!--        $("a#close").click(function (event) {-->
<!--            getColl.close();-->
<!--        });-->
<!--        $("form#getCollectionFormFirst").submit(function(event) {-->
<!--            var formdata =document.forms["getCollectionFormFirst"];-->
<!--            var regionval = 'region:'+formdata.elements["region"].value;-->
<!--            var fionval = 'fio:'+formdata.elements["fio"].value;-->
<!--            var phonenval = 'phone:'+formdata.elements["phone"].value;-->
<!--            var mailnval = 'mail:'+formdata.elements["mail"].value;-->
<!--            event.preventDefault();-->
<!--            Cookies.set('ffcoll', regionval+','+fionval+','+phonenval+','+mailnval, { expires: 7 });-->
<!--            var $form = $(this),-->
<!--                term = $form.serialize(),-->
<!--                url = $form.attr("action");-->
<!--            var posting = $.post( url, term );-->
<!--            getColl.close();-->
<!--            getCollThanks.setContent(BX('gift-modal-thanksCol'));-->
<!--            getCollThanks.show();-->
<!---->
<!--        });-->
<!--        $("button#close").click(function (event) {-->
<!--            getCollThanks.close();-->
<!--            collThanks.close();-->
<!--        });-->
<!--        $("form#getCollectionFormSec").submit(function (event) {-->
<!--            event.preventDefault();-->
<!--            var $form = $(this),-->
<!--                term = $form.serialize(),-->
<!--                url = $form.attr("action");-->
<!--            var posting = $.post( url, term );-->
<!--            getCollThanks.close();-->
<!--            collThanks.setContent(BX('gift-modal-thanks'));-->
<!--            collThanks.show();-->
<!---->
<!--        });-->
<!--        });-->
<!--        </script>-->
*/
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>