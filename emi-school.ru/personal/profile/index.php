<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(GetMessage("PERSONAL_CABINET_PAGE_MENU_PROFILE"));

?>
<?
redirectToLoginIfNotAuthorized($APPLICATION->GetCurPage());
$APPLICATION->IncludeComponent("bitrix:main.profile", "emi_personal", Array(
	"SET_TITLE" => "N",	// Устанавливать заголовок страницы
	),
	false
);
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>