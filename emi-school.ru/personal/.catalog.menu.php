<?
if($GLOBALS["partnerSchool"]->haveSchoolPartner)
{
    $aMenuLinks = Array(
        Array(
            GetMessage("PERSONAL_CABINET_PAGE_MENU_CABINET"),
            "/personal/",
            Array(),
            Array(),
            ""
        ),
        Array(
            GetMessage("PERSONAL_CABINET_PAGE_MENU_COURSES"),
            "/courses/personal/order/",
            Array(),
            Array(),
            ""
        ),
        Array(
            GetMessage("PERSONAL_CABINET_PAGE_MENU_ORDER"),
            "/personal/order/",
            Array(),
            Array(),
            ""
        ),
        Array(
            GetMessage("PERSONAL_CABINET_PAGE_MENU_SUBSCRIBE"),
            "/personal/subscribe/",
            Array(),
            Array(),
            ""
        ),
        Array(
            GetMessage("PERSONAL_CABINET_PAGE_MENU_PROFILE"),
            "/personal/profile/",
            Array(),
            Array(),
            ""
        ),
        Array(
            GetMessage("PERSONAL_CABINET_PAGE_MENU_AGREEMENT"),
            "/personal/agreement/",
            Array(),
            Array(),
            ""
        )
    );
}
else // prgrant 14.11.2017
{
    $aMenuLinks = Array(
        Array(
            GetMessage("PERSONAL_CABINET_PAGE_MENU_CABINET"),
            "/personal/",
            Array(),
            Array(),
            ""
        ),
        Array(
            GetMessage("PERSONAL_CABINET_PAGE_MENU_ORDER"),
            "/personal/order/",
            Array(),
            Array(),
            ""
        ),
        Array(
            GetMessage("PERSONAL_CABINET_PAGE_MENU_SUBSCRIBE"),
            "/personal/subscribe/",
            Array(),
            Array(),
            ""
        ),
        Array(
            GetMessage("PERSONAL_CABINET_PAGE_MENU_PROFILE"),
            "/personal/profile/",
            Array(),
            Array(),
            ""
        ),
        Array(
            GetMessage("PERSONAL_CABINET_PAGE_MENU_AGREEMENT"),
            "/personal/agreement/",
            Array(),
            Array(),
            ""
        )
    );
}
?>