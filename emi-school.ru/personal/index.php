<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(GetMessage("PERSONAL_CABINET_PAGE_INDEX_TITLE"));
redirectToLoginIfNotAuthorized($APPLICATION->GetCurPage());
?>
<div class="row">
	<div class="col-md-12">
		<p><?=GetMessage("PERSONAL_CABINET_PAGE_INDEX_P")?></p>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>