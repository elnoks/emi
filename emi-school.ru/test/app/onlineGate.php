<?php
// Autor Miroslav Novak www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// $Id: onlineGate.php,v 1.4 2011/02/15 09:26:41 mira Exp $

require('PayuGate.php');

require('configGate.php');

$payu = new PayuGate($config);

$result = $payu->receiveNotificationAndGet($getReply);

if ($result == 0) {

	// zde je prostor pro kod ulozeni noveho stavu platby ve vasi aplikaci

	$payu->closeNotification("OK"); // uzavreni notifikace, je mozne vratit i neco jineho nez "OK" napr kdyz pri zpracovani doslo k necekane chybe, notifikace tak brana bude opakovat pozdeji
} else {
	$payu->closeNotification("ERROR " . $result); // pokud je result nenulovy, nechame notifikaci opakovat vraceni ne "OK"
}
