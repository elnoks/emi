<?php
// Autor Miroslav Novak www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// $Id: orderGate.php,v 1.5 2013/09/24 19:25:42 mira Exp $

require('PayuGate.php');

require('configGate.php');


$payu = new PayuGate($config);

if (isset($_REQUEST["SubmitButton"])) {

	$order = array(
		'session_id'=>$_POST['session_id'],
		'amount'=>$_POST['amount'],
		'desc'=>$_POST['desc'],
		'first_name'=>$_POST['first_name'],
		'last_name'=>$_POST['last_name'],
		'email'=>$_POST['email'],
		
		'order_id'=>$_POST['order_id'],
		'desc2'=>$_POST['desc2'],
		'street'=>$_POST['street'],
		'street_hn'=>$_POST['street_hn'],
		'street_an'=>$_POST['street_an'],
		'city'=>$_POST['city'],
		'post_code'=>$_POST['post_code'],
		'country'=>$_POST['country'],
		'phone'=>$_POST['phone'],
		'language'=>$_POST['language'],
		'pay_type'=>$_POST['pay_type'],
	);
	
	$payu->sendOrder($order);
	
} else {


$templateId = 3;
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php echo($payu->getTemplateHead($templateId)); ?>
</head>
<body>
	<form method='post'>
        <table>
			<tr><td>session_id</td><td><input type='text' name='session_id'></td></tr>
			<tr><td>amount</td><td><input type='text' name='amount'></td></tr>
			<tr><td>desc</td><td><input type='text' name='desc'></td></tr>
			<tr><td>first_name</td><td><input type='text' name='first_name'></td></tr>
			<tr><td>last_name</td><td><input type='text' name='last_name'></td></tr>
			<tr><td>email</td><td><input type='text' name='email'></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>order_id</td><td><input type='text' name='order_id'></td></tr>
			<tr><td>desc2</td><td><input type='text' name='desc2'></td></tr>
			<tr><td>street</td><td><input type='text' name='street'></td></tr>
			<tr><td>street_hn</td><td><input type='text' name='street_hn'></td></tr>
			<tr><td>street_an</td><td><input type='text' name='street_an'></td></tr>
			<tr><td>city</td><td><input type='text' name='city'></td></tr>
			<tr><td>post_code</td><td><input type='text' name='post_code'></td></tr>
			<tr><td>country</td><td><input type='text' name='country'></td></tr>
			<tr><td>phone</td><td><input type='text' name='phone'></td></tr>
            <tr>
                <td>
                    language
                </td>
                <td>
					<select name="language">
						<option value="cs">cs
						<option value="en">en
					</select>
                <td>
            </tr>

			<tr>
				<td colspan="2">
					<?php echo($payu->getTemplateRenderer()); ?>
				</td>
			</tr>
			
			<!--
			<tr><td>pay_type</td><td><input type='text' name='pay_type'></td></tr>
			-->
            <tr>
                <td colspan="2" style="text-align: center">
					<input type=submit name='SubmitButton' value='Submit'>
				</td>
            </tr>
        </table>
    
    </form>
	<a href="index.php">Zpet na hlavni stranu</a>
</body>
</html>

<?php
}
