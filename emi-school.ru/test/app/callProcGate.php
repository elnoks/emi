<?php
// Autor Miroslav Novak www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// $Id: callProcGate.php,v 1.1 2011/02/13 13:28:35 mira Exp $

require('PayuGate.php');

require('configGate.php');



if (isset($_REQUEST["SubmitButton"])) {
	$payu = new PayuGate($config);
	$session_id = $_POST['session_id'];
	$procedure = $_POST['procedure'];
	
	if ($procedure == 'get') {
		$result = $payu->callGet($session_id, $trans_data);
	} else if ($procedure == 'confirm') {
		$result = $payu->callConfirmCancel('confirm', $session_id, $trans_data);
	} else if ($procedure == 'cancel') {
		$result = $payu->callConfirmCancel('cancel', $session_id, $trans_data);
	}
	
	echo "Vysledek volan procedury ".$procedure."<br>";
	echo "result=".$result."  (0=ok)<br>";
	if (isset($trans_data['status'])) echo "status=".$trans_data['status']."<br>";
	echo "trans_data:"; var_dump($trans_data);

	
} else {

?>

<html>
<body>
	<form method='post'>
        <table>
            <tr>
                <td>
                    procedure
                </td>
                <td>
					<select name="procedure">
						<option value="get">get
						<option value="confirm">confirm
						<option value="cancel">cancel
					</select>
                <td>
            </tr>
			<tr><td>session_id</td><td><input type='text' name='session_id	'></td></tr>
            <tr>
                <td colspan="2" style="text-align: center">
					<input type=submit name='SubmitButton' value='Submit'>
				</td>
            </tr>
        </table>
    
    </form>
</body>
</html>

<?php
}

?>
<a href="index.php">zpet</a>
