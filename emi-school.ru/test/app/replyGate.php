<?php
// Autor Miroslav Novak www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// $Id: replyGate.php,v 1.3 2011/02/13 13:35:35 mira Exp $

require('PayuGate.php');

require('configGate.php');


$payu = new PayuGate($config);

// je vraceny jako GET parametry pri presmerovani prohlizece klienta zpet z payu, muze byt snadno zmenen utocnikem
$result = $payu->receiveReply($trans_data);
echo "<h1>navrat z brany</h1>";
echo "result=".$result."  (0=ok)<br>";
if (isset($trans_data['pay_type'])) echo "pay_type=".$trans_data['pay_type']."<br>";

echo "trans_data:"; var_dump($trans_data);

// stav objednavky ziskany primo z brany, je kontrolovan proti hashi tajnym klicem a tato informac je proto bezpecna.
// v teto ukazce nedokaze rozlisit duplicitni objednavku, protoze objednavka s duplicitnim session_id selze a tato funkce proste vrati stav te puvodni objednavky s timto session_id
// pro bezpecnost je nutne zajistiti aplikaci unikatni session_id pri tvorbe objednavky a toto session_id overit pri odpovedi

echo "<h2>overeny stav z brany</h2>";
$result = $payu->callGet($trans_data['session_id'], $trans_data);
echo "result=".$result."  (0=ok)<br>";
if (isset($trans_data['status'])) echo "status=".$trans_data['status']."<br>";
echo "trans_data:"; var_dump($trans_data);

?>
<a href="index.php">zpet</a>

