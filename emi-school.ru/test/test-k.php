<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");?>
<?CModule::IncludeModule("iblock");
    if(intval($_REQUEST["IBLOCK_ID_FIELDS"])>0){
        $bError = false;
        $IBLOCK_ID = intval($_REQUEST["IBLOCK_ID_FIELDS"]);
        $ib = new CIBlock;
        $arFields = CIBlock::GetArrayByID($IBLOCK_ID);
        $arFields["GROUP_ID"] = CIBlock::GetGroupPermissions($IBLOCK_ID);
        $arFields["NAME"] = $arFields["NAME"]."_new";
        unset($arFields["ID"]);
        if($_REQUEST["IBLOCK_TYPE_ID"]!="empty")
            $arFields["IBLOCK_TYPE_ID"]=$_REQUEST["IBLOCK_TYPE_ID"];
        $ID = $ib->Add($arFields);
            if(intval($ID)<=0)
                $bError = true;        
        if($_REQUEST["IBLOCK_ID_PROPS"]!="empty")
            $iblock_prop=intval($_REQUEST["IBLOCK_ID_PROPS"]);
        else
            $iblock_prop=$IBLOCK_ID;

        $iblock_prop_new = $ID;
        $ibp = new CIBlockProperty;
        $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblock_prop));
        while ($prop_fields = $properties->GetNext()){
            if($prop_fields["PROPERTY_TYPE"] == "L"){
                $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"),
                                                               Array("IBLOCK_ID"=>$iblock_prop, "CODE"=>$prop_fields["CODE"]));
                while($enum_fields = $property_enums->GetNext()){
                    $prop_fields["VALUES"][] = Array(
                      "VALUE" => $enum_fields["VALUE"],
                      "DEF" => $enum_fields["DEF"],
                      "SORT" => $enum_fields["SORT"]
                    );
                }
            }
            $prop_fields["IBLOCK_ID"]=$iblock_prop_new;
            unset($prop_fields["ID"]);
            foreach($prop_fields as $k=>$v){
                if(!is_array($v))$prop_fields[$k]=trim($v);
                if($k{0}=='~') unset($prop_fields[$k]);
            }
            $PropID = $ibp->Add($prop_fields);
            if(intval($PropID)<=0)
                $bError = true;
        }
        if(!$bError && $IBLOCK_ID>0)
            LocalRedirect($APPLICATION->GetCurPageParam("success=Y",array("success","IBLOCK_ID_FIELDS")));
        else 
            LocalRedirect($APPLICATION->GetCurPageParam("error=Y",array("success","IBLOCK_ID_FIELDS")));
    }
    $str .='<form action='.$APPLICATION->GetCurPageParam().' method="post">[table]';    
    if($_REQUEST["success"]=="Y") $str .='[tr][td]<font color="green">ИБ успешно скопирован</font>[b][/td][/tr]';
    elseif($_REQUEST["error"]=="Y") $str .='[tr][td]<font color="red">Произошла ошибка</font><br/>[/td][/tr]';
    $str .='[tr][td]Копируем мета данные ИБ в новый ИБ[/b]<br/>[/td][/tr]';
    $res = CIBlock::GetList(Array(),Array(),true);
        while($ar_res = $res->Fetch())
            $arRes[]=$ar_res;
    $str .='[tr][td]Копируем ИБ:<br><select name="IBLOCK_ID_FIELDS">';
    foreach($arRes as $vRes)    
        $str .= '<option value='.$vRes['ID'].'>'.$vRes['NAME'].' ['.$vRes["ID"].']</option>';
    $str .='</select>[/td]';
    $str .='[td]Копируем в новый ИБ свойства другого ИБ: *<br><select name="IBLOCK_ID_PROPS">';
    $str .='<option value="empty">';
    foreach($arRes as $vRes)    
        $str .= '<option value='.$vRes['ID'].'>'.$vRes['NAME'].' ['.$vRes["ID"].']</option>';
    $str .='</select>[/td][/tr]';
    $str .='[tr][td]Копируем ИБ в тип:<br><select name="IBLOCK_TYPE_ID">';
    $str .='<option value="empty">';
    $db_iblock_type = CIBlockType::GetList();
    while($ar_iblock_type = $db_iblock_type->Fetch()){
       if($arIBType = CIBlockType::GetByIDLang($ar_iblock_type["ID"], LANG))
          $str .= '<option value='.$ar_iblock_type["ID"].'>'.htmlspecialcharsex($arIBType["NAME"])."</option>";
    }
    $str .='</select>[/td][/tr]';
    $str .='[tr][td]<br/>* если значение не указано мета данные ИБ секции "Свойства" берутся из ИБ первого поля[/td][/tr]';
    $str .='[tr][td]<input type="submit" value="копируем">[/td][/tr]';
    $str .='[/table]</form>';    
    echo $str;?>
<!-- 1. Link to jQuery (1.8 or later), -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <!-- 33 KB -->

<!-- fotorama.css & fotorama.js. -->
<link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.2/fotorama.css" rel="stylesheet"> <!-- 3 KB -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.2/fotorama.js"></script> <!-- 16 KB -->

<!-- 2. Add images to <div class="fotorama"></div>. -->
<!-- <div class="fotorama" data-nav="thumbs"  data-width="100%"
     data-ratio="800/600"
     data-minwidth="400"
     data-maxwidth="100%"
     data-minheight="300"
     data-maxheight="100%" data-fit="contain" data-loop="true" data-keyboard="true">
 
	<img src="img/1.jpg">
  <img src="img/2.jpg">
  <img src="img/3.jpg">
  <img src="img/4.jpg">
  <img src="img/5.jpg">
  <img src="img/6.jpg">
  <img src="img/1.jpg">
  <img src="img/2.jpg">
  <img src="img/3.jpg">
  <img src="img/4.jpg">
  <img src="img/5.jpg">
  <img src="img/6.jpg">
  <img src="img/1.jpg">
  <img src="img/2.jpg">
  <img src="img/3.jpg">
  <img src="img/4.jpg">
  <img src="img/5.jpg">
  <img src="img/6.jpg">
</div> -->

<!-- 3. Enjoy! -->

Вторая галерея
<!--  <link rel="stylesheet" href="gallery-2.15.2/css/blueimp-gallery.min.css">
<script src="gallery-2.15.2/js/blueimp-gallery.min.js"></script>


<div id="blueimp-gallery" class="blueimp-gallery">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<div id="links" class="links clearfix">
    <a href="img/1.jpg" title="Banana">
        <img src="img/1.jpg" alt="Banana">
    </a>
    <a href="img/2.jpg" title="Apple">
        <img src="img/2.jpg" alt="Apple">
    </a>
    <a href="img/3.jpg" title="Orange">
        <img src="img/3.jpg" alt="Orange">
    </a>
</div>
<style>
.links a {
	display:block;
	width:75px;
	height:75px;
	float:left;
	margin:0;
	padding:0;
	overflow:hidden;
}
#links a img {
}
</style>
<script>
$(window).load(function() {
	
	document.getElementById('links').onclick = function (event) {
	    event = event || window.event;
	    var target = event.target || event.srcElement,
	        link = target.src ? target.parentNode : target,
	        options = {index: link, event: event},
	        links = this.getElementsByTagName('a');
	    blueimp.Gallery(links, options);
	}
	$('#links a img').each(function(){
		if($(this).width() > $(this).height()) {
			$(this).css("max-height", "75px");
		} else {
			$(this).css("max-width", "75px");
		}
	
	});
});

</script>
-->
<br> 
<br>
<br>
<br>
<br>
<br>
<?$APPLICATION->IncludeFile("/include/ajax/photo_gallery.php");?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>