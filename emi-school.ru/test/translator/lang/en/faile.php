<?
$MESS["LEARNING"] = "Settings";
$MESS["LEARNING_2"] = "Access";
$MESS["LEARNING_3"] = "General Parameters";
$MESS["LEARNING_4"] = "Module Access Permissions";
$MESS["LEARNING_5"] = "(website specific)";
$MESS["LEARNING_6"] = "(specific #TEST#)";
$MESS["LEARNING_SITE_1"] = "&quot;#SITE#&quot;";
$MESS["LEARNING_SITE_2"] = "<p>paragraph style</p>";
$MESS["LEARNING_SITE_3"] = "<a href='http://link'>a href style link</a>";
$MESS["LEARNING_SITE_4"] = "<a href=\"#link\">a href-2 style link</a>";
$MESS["LEARNING_SITE_5"] = "<span style='font-size:12px'>span inline style</span>";
$MESS["LEARNING_SITE_6"] = "<span style=\"font-size:16px\">span inline style font-size</span>";
?>