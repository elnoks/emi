<?php
$MESS["LEARNING"] = "Nastavení";  // [en] Settings
$MESS["LEARNING_2"] = "Přístup";  // [en] Access
$MESS["LEARNING_3"] = "Obecné Parametry";  // [en] General Parameters
$MESS["LEARNING_4"] = "Modul Oprávnění K Přístupu";  // [en] Module Access Permissions
$MESS["LEARNING_5"] = "(konkrétní webové stránky)";  // [en] (website specific)
$MESS["LEARNING_6"] = "(specifický #TEST#)";  // !!!SYMBOL # [en] (specific #TEST#)
$MESS["LEARNING_SITE_1"] = "\"#WEB#\"";  // !!!SYMBOL # [en] &quot;#SITE#&quot;
$MESS["LEARNING_SITE_2"] = "<p>styl odstavce</p>";  // !!!HTML [en] <p>paragraph style</p>
$MESS["LEARNING_SITE_3"] = "<a href=\"http://link\">a href styl odkaz</a>";  // !!!HTML [en] <a href='http://link'>a href style link</a>
$MESS["LEARNING_SITE_4"] = "<a href=\"#link\">a href-2 styl odkaz</a>";  // !!!SYMBOL # !!!HTML [en] <a href="#link">a href-2 style link</a>
$MESS["LEARNING_SITE_5"] = "<span style=\"font-size:12px\">span inline styl</span>";  // !!!HTML [en] <span style='font-size:12px'>span inline style</span>
$MESS["LEARNING_SITE_6"] = "<span style=\"font-size:16px\">span inline styl písma-velikost</span>";  // !!!HTML [en] <span style="font-size:16px">span inline style font-size</span>
