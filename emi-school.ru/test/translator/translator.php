<?php

$params = array(
    'bitrix' => array(
        'from' => 'en',
        'to' => 'cz'
    ),
    'yandex' => array(
        'key' => 'trnsl.1.1.20171116T094111Z.3e0af90cd69d0982.934c2b5a0bd0ba940aad81133b481b92ef7af863', // api translate at senin@emi-school.ru
        'from' => 'en',
        'to' => 'cs'
    ),
    'extensions' => array(
        'php'
    ),
    'permissions' => array(
        'file' => 0644,
        'folder' => 0755
    )
);

$translator = new CTranslator($params);

class CTranslator
{
    /**
     * @var array
     */
    protected $params = array();

    /**
     * @var array
     */
    protected $errors = array();

    /**
     * @var array, tmp lang files of given lang folder
     */
    protected $langFiles = array();

    /**
     * @var array all added lang files
     */
    protected $addedFiles = array();

    /**
     * @var array all files which must be checked
     */
    protected $checkFiles = array();

    /**
     * Return yandex answer and http response code
     *
     * @param string $string
     * @return array
     */
    public function yandexRequest($string)
    {
        // https://translate.yandex.net/api/v1.5/tr.json/translate?lang=en-ru&key=trnsl.1.1.20171116T094111Z.3e0af90cd69d0982.934c2b5a0bd0ba940aad81133b481b92ef7af863&text=hey

        $curl = curl_init('https://translate.yandex.net/api/v1.5/tr.json/translate');

        $requestData = array();
        $requestData['key'] = $this->params['yandex']['key'];
        $requestData['text'] = $string;
        $requestData['lang'] = $this->params['yandex']['from'] . '-' . $this->params['yandex']['to'];
        $requestData['format'] = 'html';

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($requestData));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = array();
        $result['answer'] = curl_exec($curl);

        //echo $result['answer'].'\r\n';

        if (empty($result['answer'])) {
            $result['error'] = curl_error($curl) . '(' . curl_errno($curl) . ')';
        }

        $result['status'] = \curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        return $result;
    }

    /**
     * Translate given string
     *
     * @param string $string
     * @return string
     */
    protected function translateString($string)
    {
        $translatedString = '';

        if (strlen($string) > 0) {
            $result = $this->yandexRequest($string);
            if($result['status'] == 200){
                $result['answer'] = json_decode($result['answer']);
                if($result['answer']->code == 200){
                    $translatedString = $result['answer']->text[0];
                } else {
                    $this->errors[] = 'Bad yandex answer';
                }
            } else {
                $this->errors[] = 'Bad yandex request';
            }
        }

        return $translatedString;
    }

    /**
     * Translate all lang file messages and return translated result as string
     *
     * @param $filePath
     * @param $savePath
     * @return string
     */
    protected function translateFile($filePath, $savePath)
    {
        $translatedFileContent = "<?php\r\n";

        $needToCheck = false;

        if (is_file($filePath)) {
            /**
             * @var array $MESS
             */
            include $filePath;
            if (is_array($MESS)) {
                foreach ($MESS as $key => $value) {
                    $translatedFileContent .= '$MESS["' . $key . '"] = "';
                    $translatedFileContent .= addslashes($this->translateString($value));
                    $translatedFileContent .= '";  //';

                    //you can add your own check here
                    
                    if(strpos($value, '#') !== false){
                        $needToCheck = true;
                        $translatedFileContent .= ' !!!SYMBOL #';
                    }

                    if(strpos($value, '<') !== false){
                        $needToCheck = true;
                        $translatedFileContent .= ' !!!HTML';
                    }

                    $translatedFileContent .= ' ['.$this->params['bitrix']['from'].'] '. $value . "\r\n";
                }
            }
        }

        if($needToCheck){
            $this->checkFiles[] = $filePath;
            echo '<div style="color: blue;">File '.$filePath.' must be checked</div>';
        }

        return $translatedFileContent;
    }

    /**
     * Recursively collect all lang files in given directory and creates new lang files in OUT language
     *
     * @param $langDir
     */
    protected function translateFolder($langDir)
    {
        $this->langFiles = array();
        $subDirs = $this->getSubDirs($langDir);

        $fromDir = '';
        $toDir = '';
        foreach ($subDirs as $key => $dir) {
            $info = pathinfo($dir);
            if ($info['basename'] == $this->params['bitrix']['from']) {
                $fromDir = $dir;
            } elseif ($info['basename'] == $this->params['bitrix']['to']) {
                $toDir = $dir;
            }

            if (!empty($fromDir) && !empty($toDir))
                break;
        }

        if (empty($fromDir)) {
            echo 'Folder ' . $this->params['bitrix']['from'] . ' not found in ' . $langDir . '<br />';
        } else {
            $dirs = $this->getSubDirs($fromDir, 'translate');
            while($dirs){
                $subDirs = $this->getSubDirs(reset($dirs), 'translate');
                $dirs = array_merge($dirs, $subDirs);
                unset($dirs[0]);
            }

            if(!empty($this->langFiles)){
                foreach ($this->langFiles as $key => $filePath)
                {
                    $savePath = str_replace('/lang/'.$this->params['bitrix']['from'].'/', '/lang/'.$this->params['bitrix']['to'].'/', $filePath);
                    if(!is_file($savePath)){

                        if(!file_exists(dirname($savePath)))
                            mkdir(dirname($savePath), $this->params['permissions']['folder'], true);

                        $content = $this->translateFile($filePath, $savePath);
                        $f = fopen($savePath, 'w');
                        fwrite($f, $content);
                        fclose($f);
                        chmod($savePath, $this->params['permissions']['file']);

                        $this->addedFiles[] = $savePath;
                        echo 'File '.$savePath.' added\r\n';
                    }
                }
            }
        }
    }

    /**
     * Return all sub directories of given directory, plus collects lang files in translate mode
     *
     * @param string $dir
     * @param string $mode , read || translate
     * @return array
     */
    protected function getSubDirs($dir, $mode = 'read')
    {
        if (is_dir($dir)) {
            $path = opendir($dir);
            $subDirs = array();
            while (($file = readdir($path)) !== false) {
                $filePath = $dir . '/' . $file;
                if ($file != '.' && $file != '..' && $filePath != $_SERVER['SCRIPT_FILENAME']) {

                    if (is_dir($filePath)) {
                        $subDirs[] = $filePath;
                    } else {
                        if ($mode == 'translate') {
                            $info = pathinfo($filePath);
                            if(in_array($info['extension'], $this->params['extensions'])){
                                $this->langFiles[] = $filePath;
                            }
                        }
                    }

                }
            }
            closedir($path);
            return $subDirs;
        }

        return array();
    }

    /**
     * @param array $params
     */
    protected function checkSetupParams($params)
    {
        if (empty($params['bitrix']['from'])) {
            $this->errors[] = 'No <span style="color:red;">FROM</span> lang given.<br>';
        }

        if (empty($params['bitrix']['to'])) {
            $this->errors[] = 'No <span style="color:red;">TO</span> lang given.<br>';
        }

        if (empty($params['extensions'])) {
            $this->errors[] = 'No file extensions given <br>';
        }

        if (empty($params['permissions']['file'])) {
            $this->errors[] = 'No file permissions given <br>';
        }

        if (empty($params['permissions']['folder'])) {
            $this->errors[] = 'No folder permissions given <br>';
        }

        if (empty($params['yandex']['from'])) {
            $this->errors[] = 'No yandex from lang given <br>';
        }

        if (empty($params['yandex']['to'])) {
            $this->errors[] = 'No yandex to lang given <br>';
        }

        if (empty($params['yandex']['key'])) {
            $this->errors[] = 'No yandex key given <br>';
        }
    }

    /**
     * CTranslator constructor.
     * @param array $params
     */
    function __construct($params)
    {
        $this->params = $params;

        $this->checkSetupParams($params);

        if (!empty($this->errors)) {
            echo '<pre>'; print_r($this->errors); echo '</pre>';
            die('interrupted. errors');
        } else {
            $startDir = getcwd();
            $dirs = $this->getSubDirs($startDir);
            foreach ($dirs as $key => $dir) {
                $info = pathinfo($dir);
                if ($info['basename'] == 'lang') {
                    $this->translateFolder($dir);
                    unset($dirs[$key]);
                }
            }

            while ($dirs) {
                if (!empty($this->errors)) {
                    echo '<pre>'; print_r($this->errors); echo '</pre>';
                    die('interrupted. errors');
                }
                $subDirs = $this->getSubDirs(reset($dirs));

                foreach ($subDirs as $key => $dir) {
                    $info = pathinfo($dir);
                    if ($info['basename'] == 'lang') {
                        $this->translateFolder($dir);
                        unset($subDirs[$key]);
                    }
                }

                $dirs = array_merge($dirs, $subDirs);
                unset($dirs[0]);
                echo '<pre>'; print_r($dirs); echo '</pre><hr />';
            }

            echo '<div style="font-weight:bold; font-size:2em; color:green;">All operations done</div>';
            echo '<div style="font-weight:bold; font-size:2em; color:blue;">CHECK THIS FILES:</div>';
            echo '<pre>'; print_r($this->checkFiles); echo '</pre>';
            echo '<div style="font-weight:bold; font-size:2em; color:blue;">ADDED FILES:</div>';
            echo '<pre>'; print_r($this->addedFiles); echo '</pre>';
        }
    }
}