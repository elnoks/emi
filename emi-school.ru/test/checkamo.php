<?php
require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Main\Type;
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 09.06.2018
 * Time: 13:55
 */
//$arOrder=array (
//    'SITE_ID' => 's2',
//    'LID' => 's2',
//    'PERSON_TYPE_ID' => 3,
//    'PRICE' => 9500.0,
//    'CURRENCY' => 'RUB',
//    'USER_ID' => 30469,
//    'PAY_SYSTEM_ID' => '24',
//    'PRICE_DELIVERY' => 0.0,
//    'DELIVERY_ID' => '8',
//    'DISCOUNT_VALUE' => 0.0,
//    'TAX_VALUE' => 0.0,
//    'TRACKING_NUMBER' => NULL,
//    'PAYED' => NULL,
//    'CANCELED' => NULL,
//    'STATUS_ID' => 'N',
//    'RESERVED' => NULL,
//    'UPDATED_1C' => 'N',
//    'DATE_INSERT' => '09.06.2018 13:53:29',
//    'DATE_UPDATE' => '09.06.2018 13:53:29',
//    'AFFILIATE_ID' => false,
//    'CREATED_BY' => '30469',
//    'DATE_STATUS' => '09.06.2018 13:53:29',
//    'EMP_STATUS_ID' => '30469',
//    'ID' => 29347,
//    'ACCOUNT_NUMBER' => '16129347',
//    'RUNNING' => 'N',
//    'ORDER_WEIGHT' => 0.0,
//    'BASKET_ITEMS' =>
//        array (
//            0 =>
//                array (
//                    'MODULE' => 'catalog',
//                    'PRODUCT_ID' => '245047',
//                    'ID' => '2012033',
//                    'LID' => 's2',
//                    'QUANTITY' => '1.0000',
//                    'WEIGHT' => '0.00',
//                    'DELAY' => 'N',
//                    'CAN_BUY' => 'Y',
//                    'PRICE' => '4000.0000',
//                    'CUSTOM_PRICE' => 'N',
//                    'BASE_PRICE' => '4000.0000',
//                    'PRODUCT_PRICE_ID' => '175622',
//                    'PRICE_TYPE_ID' => '15',
//                    'CURRENCY' => 'RUB',
//                    'BARCODE_MULTI' => 'N',
//                    'RESERVED' => 'N',
//                    'RESERVE_QUANTITY' => NULL,
//                    'NAME' => 'Топ-10. Лучшие техники E.Mi-дизайна',
//                    'CATALOG_XML_ID' => NULL,
//                    'VAT_RATE' => '0.0000',
//                    'NOTES' => 'Розничная',
//                    'DISCOUNT_PRICE' => '0.0000',
//                    'PRODUCT_PROVIDER_CLASS' => '\\Bitrix\\Catalog\\Product\\CatalogProvider',
//                    'CALLBACK_FUNC' => NULL,
//                    'ORDER_CALLBACK_FUNC' => NULL,
//                    'PAY_CALLBACK_FUNC' => NULL,
//                    'CANCEL_CALLBACK_FUNC' => NULL,
//                    'DIMENSIONS' =>
//                        array (
//                            'WIDTH' => '0',
//                            'HEIGHT' => '0',
//                            'LENGTH' => '0',
//                        ),
//                    'TYPE' => NULL,
//                    'SET_PARENT_ID' => NULL,
//                    'DETAIL_PAGE_URL' => '/courses/catalog/245047/',
//                    'FUSER_ID' => '24236623',
//                    'MEASURE_CODE' => '796',
//                    'MEASURE_NAME' => 'шт',
//                    'ORDER_ID' => 29347,
//                    'DATE_INSERT' =>
//                        Bitrix\Main\Type\DateTime::__set_state(array(
//                            'value' =>
//                                DateTime::__set_state(array(
//                                    'date' => '2018-06-09 13:52:51.000000',
//                                    'timezone_type' => 3,
//                                    'timezone' => 'Europe/Moscow',
//                                )),
//                        )),
//                    'DATE_UPDATE' =>
//                        Bitrix\Main\Type\DateTime::__set_state(array(
//                            'value' =>
//                                DateTime::__set_state(array(
//                                    'date' => '2018-06-09 13:53:29.770089',
//                                    'timezone_type' => 3,
//                                    'timezone' => 'Europe/Moscow',
//                                )),
//                        )),
//                    'PRODUCT_XML_ID' => '245047',
//                    'SUBSCRIBE' => 'N',
//                    'RECOMMENDATION' => NULL,
//                    'VAT_INCLUDED' => 'Y',
//                    'SORT' => '100',
//                    'DATE_REFRESH' => NULL,
//                    'DISCOUNT_NAME' => NULL,
//                    'DISCOUNT_VALUE' => NULL,
//                    'DISCOUNT_COUPON' => NULL,
//                    'PROPS' =>
//                        array (
//                            'DATE_START' =>
//                                array (
//                                    'CODE' => 'DATE_START',
//                                    'ID' => '4220335',
//                                    'VALUE' => '20.06.2018',
//                                    'SORT' => '100',
//                                    'NAME' => 'Дата начала',
//                                ),
//                            'ID_COURSE' =>
//                                array (
//                                    'CODE' => 'ID_COURSE',
//                                    'ID' => '4220336',
//                                    'VALUE' => '235426',
//                                    'SORT' => '100',
//                                    'NAME' => 'Курс',
//                                ),
//                            'COURSE_LINK' =>
//                                array (
//                                    'CODE' => 'COURSE_LINK',
//                                    'ID' => '4220337',
//                                    'VALUE' => '/courses/1_uroven/top_10_luchshie_tekhniki_e_mi_dizayna/?date=2018-06-20',
//                                    'SORT' => '100',
//                                    'NAME' => 'Ссылка на курс',
//                                ),
//                            'PRODUCT.XML_ID' =>
//                                array (
//                                    'CODE' => 'PRODUCT.XML_ID',
//                                    'ID' => '4220338',
//                                    'VALUE' => '245047',
//                                    'SORT' => '100',
//                                    'NAME' => 'Product XML_ID',
//                                ),
//                        ),
//                ),
//            1 =>
//                array (
//                    'MODULE' => 'catalog',
//                    'PRODUCT_ID' => '245040',
//                    'ID' => '2012034',
//                    'LID' => 's2',
//                    'QUANTITY' => '1.0000',
//                    'WEIGHT' => '0.00',
//                    'DELAY' => 'N',
//                    'CAN_BUY' => 'Y',
//                    'PRICE' => '5500.0000',
//                    'CUSTOM_PRICE' => 'N',
//                    'BASE_PRICE' => '5500.0000',
//                    'PRODUCT_PRICE_ID' => '175615',
//                    'PRICE_TYPE_ID' => '15',
//                    'CURRENCY' => 'RUB',
//                    'BARCODE_MULTI' => 'N',
//                    'RESERVED' => 'N',
//                    'RESERVE_QUANTITY' => NULL,
//                    'NAME' => 'Художественная роспись. Азбука линий ',
//                    'CATALOG_XML_ID' => NULL,
//                    'VAT_RATE' => '0.0000',
//                    'NOTES' => 'Розничная',
//                    'DISCOUNT_PRICE' => '0.0000',
//                    'PRODUCT_PROVIDER_CLASS' => '\\Bitrix\\Catalog\\Product\\CatalogProvider',
//                    'CALLBACK_FUNC' => NULL,
//                    'ORDER_CALLBACK_FUNC' => NULL,
//                    'PAY_CALLBACK_FUNC' => NULL,
//                    'CANCEL_CALLBACK_FUNC' => NULL,
//                    'DIMENSIONS' =>
//                        array (
//                            'WIDTH' => '0',
//                            'HEIGHT' => '0',
//                            'LENGTH' => '0',
//                        ),
//                    'TYPE' => NULL,
//                    'SET_PARENT_ID' => NULL,
//                    'DETAIL_PAGE_URL' => '/courses/catalog/245040/',
//                    'FUSER_ID' => '24236623',
//                    'MEASURE_CODE' => '796',
//                    'MEASURE_NAME' => 'шт',
//                    'ORDER_ID' => 29347,
//                    'DATE_INSERT' =>
//                        Bitrix\Main\Type\DateTime::__set_state(array(
//                            'value' =>
//                                DateTime::__set_state(array(
//                                    'date' => '2018-06-09 13:52:52.000000',
//                                    'timezone_type' => 3,
//                                    'timezone' => 'Europe/Moscow',
//                                )),
//                        )),
//                    'DATE_UPDATE' =>
//                        Bitrix\Main\Type\DateTime::__set_state(array(
//                            'value' =>
//                                DateTime::__set_state(array(
//                                    'date' => '2018-06-09 13:53:29.791532',
//                                    'timezone_type' => 3,
//                                    'timezone' => 'Europe/Moscow',
//                                )),
//                        )),
//                    'PRODUCT_XML_ID' => '245040',
//                    'SUBSCRIBE' => 'N',
//                    'RECOMMENDATION' => NULL,
//                    'VAT_INCLUDED' => 'Y',
//                    'SORT' => '200',
//                    'DATE_REFRESH' => NULL,
//                    'DISCOUNT_NAME' => NULL,
//                    'DISCOUNT_VALUE' => NULL,
//                    'DISCOUNT_COUPON' => NULL,
//                    'PROPS' =>
//                        array (
//                            'DATE_START' =>
//                                array (
//                                    'CODE' => 'DATE_START',
//                                    'ID' => '4220339',
//                                    'VALUE' => '14.06.2018',
//                                    'SORT' => '100',
//                                    'NAME' => 'Дата начала',
//                                ),
//                            'COURSE_LINK' =>
//                                array (
//                                    'CODE' => 'COURSE_LINK',
//                                    'ID' => '4220340',
//                                    'VALUE' => '/courses/1_uroven/khudozhestvennaya_rospis_azbuka_liniy/',
//                                    'SORT' => '100',
//                                    'NAME' => 'Ссылка на курс',
//                                ),
//                            'ID_COURSE' =>
//                                array (
//                                    'CODE' => 'ID_COURSE',
//                                    'ID' => '4220341',
//                                    'VALUE' => '235425',
//                                    'SORT' => '100',
//                                    'NAME' => 'Курс',
//                                ),
//                            'PRODUCT.XML_ID' =>
//                                array (
//                                    'CODE' => 'PRODUCT.XML_ID',
//                                    'ID' => '4220342',
//                                    'VALUE' => '245040',
//                                    'SORT' => '100',
//                                    'NAME' => 'Product XML_ID',
//                                ),
//                        ),
//                ),
//        ),
//    'ORDER_PROP' =>
//        array (
//            20 => 'ТЕСТОВЫЙ ЗАКАЗ',
//            40 => 'ROSTOV_SCHOOL',
//            42 => 'COURSE',
//            21 => 'it2@emi-school.ru',
//            22 => '+7(928)600-55-89',
//            25 => '671',
//            24 => 'Москва',
//        ),
//    'DISCOUNT_LIST' =>
//        array (
//        ),
//    'TAX_LIST' =>
//        array (
//        ),
//    'VAT_RATE' => 0.0,
//    'VAT_SUM' => 0.0,
//    'PROFILE_NAME' => 'ТЕСТОВЫЙ ЗАКАЗ',
//    'PAYER_NAME' => 'ТЕСТОВЫЙ ЗАКАЗ',
//    'USER_EMAIL' => 'it2@emi-school.ru',
//    'DELIVERY_LOCATION' => '671',
//);


$rsUsers = CUser::GetList( ( $by = "id" ), ( $order = "desc" ), Array( "ID"                   => $_REQUEST["CHECK_ID"],
                                                                       "ACTIVE"               => "Y",
                                                                       "UF_IWEB_SMS_PASSWORD" => $_REQUEST["CHECK_CODE"],
                                                                        "LOGIN" => 'AVolobuev'
), Array( "UF_*" ) );
$region = $GLOBALS["partnerShop"]->getCurrentUserArRegion();

while ( $rsUser = $rsUsers->Fetch() ) {
$curUser=array(
  'name' => $rsUser['NAME'].' '.$rsUser['LAST_NAME'].' '.$rsUser['SECOND_NAME'],
  'email' => $rsUser['EMAIL'],
  'phone' => $rsUser['PERSONAL_MOBILE'],
  'source' => 'Сайт',
  'tags' => array('168317'=>'РегистрацияНаСайте'),
  'region' => $region['NAME']
);
}

include_once ('sendUser.php');
send_user_emiofficial($curUser);
