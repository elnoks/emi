<?
require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
define("CATALOG_TYPE","ROSTOV");
$_REQUEST['filter_submit']='';
$GLOBALS['arrFilter']['PROPERTY']['BREND'] = "ZZBrend";
$arResult["VARIABLES"]["SECTION_CODE"] = "vystavki";

if(count($GLOBALS['arrFilter']['PROPERTY']['BREND'])>0){
	$arrBannerFilter=array(
			'PROPERTY'=>array(
					'BRAND'=>$GLOBALS['arrFilter']['PROPERTY']['BREND'],
			)
	);
}
//Добавляем фильтрацию по баннерам с текущим разделом и незаполненным
$arrBannerFilter=Array(Array("LOGIC" => "OR",
		array('PROPERTY_SECTION'=>false),
		array('PROPERTY_SECTION'=>$arResult["VARIABLES"]["SECTION_CODE"])
));

$arrBannerFilter["SECTION_CODE"] = ($_REQUEST['filter_submit']=='y' && count($GLOBALS['arrFilter']['PROPERTY']['BREND'])>0)?"brend":CATALOG_TYPE;

$APPLICATION->IncludeComponent(
	"oc:getlist",
	"banners",
	Array(
		"IBLOCK_ID" => "1",
		"AR_FILTER" => $arrBannerFilter,
		"AR_SELECT" => Array("PREVIEW_PICTURE","PROPERTY_PARTNER_VALUE"),
		"PAGESIZE" => "10",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000"
	),
false
);?>