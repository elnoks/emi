<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 12.06.2018
 * Time: 16:58
 */

function send_user_emiofficial($curUser) {
	require_once( $_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/amo_crm_emi.php' );
	require_once( $_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/amo_functions.php' );

	$amo_con_info['account'] = "https://emiofficial.amocrm.ru/";
	$amo_con_info['login']   = "brand@emi-school.ru";
	$amo_con_info['hash']    = "ea7d659ea1ed06aac97b16c084d2708e";

	define( "LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/log_amo_ru_newUser.txt" );

	$querys  = array( $curUser['email'], $curUser['phone'], preg_replace('![^0-9]+!', '', substr($curUser['phone'],2)));  //Запросы для поиска
	$res     = amo_search_contact( $querys, $amo_con_info ); //Поиск контакта по запросам
	$oldUser = amo_contact_in_array( $res ); //Результат по контакту в массив

	if($oldUser['id']!=''){
		$newUser=amo_compare_contacts($curUser,$oldUser);
		$contacts=amo_prepare_reg_cont_to_update($newUser);
	} else {
		$contacts=amo_prepare_reg_cont_to_add($curUser);
	}

	$amoemi   = new AmoEmi( $amo_con_info['account'], $amo_con_info['login'], $amo_con_info['hash'] );
	$rescont=$amoemi->set_contacts($contacts,$amo_con_info['login']);

	AddMessage2Log(var_export('curUser--//--//--//--//--//', true));
	AddMessage2Log(var_export($curUser, true));

	if($oldUser['id']!=''){
		AddMessage2Log(var_export('oldUser--//--//--//--//--//', true));
		AddMessage2Log(var_export($oldUser, true));
	}

	if(isset($newUser)){
		AddMessage2Log(var_export('newUser--//--//--//--//--//', true));
		AddMessage2Log(var_export($newUser, true));
	}

	AddMessage2Log(var_export('contacts--//--//--//--//--//', true));
	AddMessage2Log(var_export($contacts, true));

	AddMessage2Log(var_export('rescont--//--//--//--//--//', true));
	AddMessage2Log(var_export(json_decode($rescont), true));
}



