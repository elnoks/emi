<?php
/**
 * Created by PhpStorm.
 * User: prgrant
 * Date: 27.09.2017
 * Time: 13:43
 */

require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$IBLOCK_ID = 170;  //ИБ тунис каталог
$STORE_ID = 79; // Тунис склад
//$KOL = 9999999;
$arFilter = Array(
    "IBLOCK_ID"=>$IBLOCK_ID,
    "ACTIVE"=>"Y"
);

$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter);
while($ar_fields = $res->GetNext())
{

    $ar_res = CCatalogProduct::GetByID($ar_fields["ID"]);

    $KOL = $ar_res["QUANTITY"];
    $arFields = Array(
        "PRODUCT_ID" => $ar_fields["ID"],
        "STORE_ID" => $STORE_ID,
        "AMOUNT" => $KOL,
    );
    $result = CCatalogStoreProduct::Add($arFields);
    if (!$result){
        $result = CCatalogStoreProduct::UpdateFromForm($arFields);
    }
    if ($result==1) $result = $ar_fields["ID"]." - изменено кол-во на: ".$KOL." | ID склада: ".$STORE_ID.", ";
    echo $result."<br>";

}


?>