<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>

<?
use Bitrix\Main\Page\Asset;
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/js/owlcarousel/assets/owl.carousel.min.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/js/owlcarousel/assets/owl.theme.default.css');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/owlcarousel/owl.carousel.min.js');
?>

<?if($arResult['ITEMS']):?>

        <div class="owl-carousel owl-theme top-slider swipeignore">

            <?foreach($arResult["ITEMS"][$arParams["BANNER_TYPE_THEME"]]["ITEMS"] as $arItem):?>
                <?
                //if ($USER->IsAdmin()){

                    if (
                        SITE_ID == "l5" || //Екатеринбург
                        SITE_ID == "m9" || //Красноярск
                        SITE_ID == "o5" || //Самара
                        SITE_ID == "o7" || //Саратов
                        SITE_ID == "l1" || //Сургут
                        SITE_ID == "n3" || //Ижевск
                        SITE_ID == "p2" || //Киров
                        SITE_ID == "l8" || //Оренбург
                        SITE_ID == "k4" || //Абакан
                        SITE_ID == "m2" || //Пермь
                        SITE_ID == "n2" || //Челябинск
                        SITE_ID == "k6" || //Калуга
                        SITE_ID == "p7" || //Новосибирск
                        SITE_ID == "n1" || //Хабаровск
                        SITE_ID == "n8" || //Петропавловск-Камчатский
                        SITE_ID == "k3" || //Ставрополь
                        SITE_ID == "p5" || //Москва
                        SITE_ID == "n2" || //Челябинск
                        SITE_ID == "r7" || //Волгоград
                        SITE_ID == "r9" || //Иваново
                        SITE_ID == "m5" || //Тамбов
                        SITE_ID == "p4" || //Липецк
                        SITE_ID == "n4" || //Уфа
                        SITE_ID == "o4"    //Пятигорск
                    ){
                        if ($arItem['NAME']=="Чп"){
                            goto end;
                        }
                    }
                //}
                ?>

                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    $background = is_array($arItem["DETAIL_PICTURE"]) ? $arItem["DETAIL_PICTURE"]["SRC"] : $this->GetFolder()."/images/background.jpg";
                    $target = $arItem["PROPERTIES"]["TARGETS"]["VALUE_XML_ID"];
                    ?>
                    <div class="item">
                        <a class="target" href="<?=$arItem["PROPERTIES"]["URL_STRING"]["VALUE"]?>" <?=(strlen($target) ? 'target="'.$target.'"' : '')?>>
                            <img id="<?=$this->GetEditAreaId($arItem['ID']);?>" src="<?=$background?>" alt="">
                        </a>
                        <? /*
                        <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" style="background-image: url('<?=$background?>') !important; width: 100%; height: 450px;">
                            <?if($arItem["PROPERTIES"]["URL_STRING"]["VALUE"]):?>
                                <a class="target" href="<?=$arItem["PROPERTIES"]["URL_STRING"]["VALUE"]?>" <?=(strlen($target) ? 'target="'.$target.'"' : '')?>></a>
                            <?endif;?>
                        </div>
                        */ ?>
                    </div>
                <?end:?>
            <?endforeach;?>
        </div>

    <script>
        var owl_top_slider = $('.owl-carousel.top-slider');
        owl_top_slider.owlCarousel({
            loop: true,
            autoplay: true,
            autoplayTimeout: 4000,
            autoplayHoverPause: true,
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            items: 1,
            margin: 0,
            nav:true,
            checkVisible:false,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
        });

        //$(document).ready(function() {
            owl_top_slider.init();
        //});
        $(window).resize(function() {
            owl_top_slider.init();
        });

    </script>
<?endif;?>

