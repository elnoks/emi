<?
$MESS["CATALOG_NOT_AVAILABLE"] = "Not available";
$MESS["CATALOG_QUANTITY"] = "Quantity";
$MESS["CATALOG_QUANTITY_FROM_TO"] = "From # FROM # to # TO #";
$MESS["CATALOG_QUANTITY_FROM"] = "From # FROM #";
$MESS["CATALOG_QUANTITY_TO"] = "To # TO #";
$MESS["CT_BCS_QUANTITY"] = "Quantity";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "All information associated with this entry will be deleted. Continue?";
$MESS["NO_PRODUCTS"] = "Unfortunately, this section is empty. <br /> You can find the necessary products in others:";
$MESS['CATALOG_FROM'] = 'from';
$MESS["CT_NAME_NOT_FOUND"] = "No products found";
$MESS["CT_NAME_SHOW_ALL"] = "Show All";
$MESS["CATALOG_VIEW_MORE"] = "Show Primary";
$MESS["PROPERTIES"] = "Characteristics";
$MESS["CATALOG_READ_MORE"] = "Details";
$MESS["CATALOG_ADD"] = "Add to cart";
$MESS["CATALOG_WISH"] = "Postpone";
$MESS["CATALOG_WISH_OUT"] = "On Deferred";
$MESS["CATALOG_COMPARE"] = "Compare";
$MESS["CATALOG_COMPARE_OUT"] = "By comparison";
$MESS["CATALOG_ADDED"] = "In the basket";
$MESS["CATALOG_SUBSCRIBE"] = "Subscribe";
$MESS["CATALOG_IN_SUBSCRIBE"] = "Unsubscribe";
$MESS["CATALOG_ORDER"] = "Order";
$MESS["WITHOUT_DISCOUNT"] = "Price without discount";
$MESS["EMPTY_CATALOG_DESCR"] = "Directory is empty";
$MESS['CT_IS_AVAILABLE'] = "Available";
$MESS['DONT_AVAILABLE'] = "Not Available";
$MESS["MANY_GOODS"] = "Lots";
$MESS["SUFFICIENT_GOODS"] = "Enough";
$MESS["FEW_GOODS"] = "Few";
$MESS["NO_GOODS"] = "By order";
$MESS["MEASURE_DEFAULT"] = "pcs";
$MESS["CATALOG_IN_CART"] = "In the basket";
$MESS["TITLE_QUANTITY"] = "pieces";
$MESS["UNTIL_AKC"] = "Until the end of the action";
$MESS["TITLE_QUANTITY_BLOCK"] = "Balance";
$MESS["CATALOG_ECONOMY"] = "Savings";
$MESS["SHOW_RATING"] = "Display Rating";
$MESS["FAST_VIEW"] = "Quick View";
?>