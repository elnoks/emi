<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */


$this->setFrameMode(true);

$class_block="s_".$this->randString();
$arTab=array();
$col=4;
if($arParams["LINE_ELEMENT_COUNT"]>=3 && $arParams["LINE_ELEMENT_COUNT"]<4)
    $col=3;
if($arResult["SHOW_SLIDER_PROP"]){?>
    <div class="tab_slider_wrapp specials <?=$class_block;?> best_block clearfix" itemscope itemtype="http://schema.org/WebPage">
        <?$arParams['SET_TITLE'] = 'N';$arParamsTmp = urlencode(serialize($arParams));?>
        <span class='request-data' data-value='<?=$arParamsTmp?>'></span>

        <div class="top_blocks">
            <?if($arParams["NAME_BLOCK"]):?>
                <div class="title_wrapper"><div class="title_block sm"><?=$arParams["NAME_BLOCK"];?></div></div>
            <?endif;?>
            <ul class="tabs ajax">
                <?$i=1;
                foreach($arResult["TABS"] as $code => $arTab):?>
                    <li data-code="<?=$code?>" <?=($i==1 ? "class='cur clicked'" : "")?>><span><?=$arTab["TITLE"];?></span></li>
                    <?$i++;?>
                <?endforeach;?>
                <li class="stretch"></li>
            </ul>
        </div>

        <ul class="tabs_content">
            <?$j=1;?>
            <?foreach($arResult["TABS"] as $code => $arTab){?>
                <li class="tab <?=$code?>_wrapp <?=($j == 1 ? "cur opacity1" : "");?> " data-code="<?=$code?>" data-col="<?=$col;?>" data-filter="<?=($arTab["FILTER"] ? urlencode(serialize($arTab["FILTER"])) : '');?>">
                    <div class="tabs_slider <?=$code?>_slides wr">
                        <?
                        if($j++ == 1)
                        {
                            if($arTab["FILTER"])
                                $GLOBALS[$arParams["FILTER_NAME"]] = $arTab["FILTER"];

                            $bAjaxMode = (isset($_POST["AJAX_POST"]) && $_POST["AJAX_POST"] == "Y");

                            if ($bAjaxMode) {
                                require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
                                global $APPLICATION;
                                if (\Bitrix\Main\Loader::includeModule("aspro.next")) {
                                    $arRegion = CNextRegionality::getCurrentRegion();
                                }
                            }
                            ?>

                            <? if ((isset($arParams["IBLOCK_ID"]) && $arParams["IBLOCK_ID"]) || $bAjaxMode): ?>
                            <?
                            $arIncludeParams = ($bAjaxMode ? $_POST["AJAX_PARAMS"] : $arParamsTmp);
                            $arGlobalFilter = ($bAjaxMode ? unserialize(urldecode($_POST["GLOBAL_FILTER"])) : array());
                            $arComponentParams = unserialize(urldecode($arIncludeParams));
                            ?>


                            <?
                            if ($bAjaxMode && (is_array($arGlobalFilter) && $arGlobalFilter))
                                $GLOBALS[$arComponentParams["FILTER_NAME"]] = $arGlobalFilter;

                            $arComponentParams["TAB_FILTER"][]=$code;


                            ?>

                            <?
                            $APPLICATION->IncludeComponent(
                                "emi:tabs.next.catalog.section",
                                ".default_OLD",
                                $arComponentParams,
                                false, array("HIDE_ICONS" => "Y")
                            ); ?>

                            <? endif; ?>
                        <?
                        }
                        ?>
                    </div>
                </li>
            <?}?>

        </ul>

    </div>
<?}?>