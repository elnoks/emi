<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc;


$arComponentParameters = array(
    'GROUPS' => array(
    ),
    'PARAMETERS' => array(
        'HL_BLOCK_ID' => array(
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage("HLLIST_COMPONENT_BLOCK_ID_PARAM"),
            'TYPE' => 'TEXT'
        ),
    )
);