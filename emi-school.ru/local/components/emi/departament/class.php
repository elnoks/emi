<?

use \Bitrix\Main\Application,
    \Bitrix\Main\Web\Cookie,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Context,
    \Bitrix\Sale\Location,
    \Bitrix\Highloadblock\HighloadBlockTable as HL,
    \Bitrix\Main\Service\GeoIp,
    \Bitrix\Main\Page\Asset;

Loader::includeModule('highloadblock');
Loader::includeModule("sale");
Loader::includeModule("catalog");

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);


if (!Loader::includeModule('highloadblock') || (!Loader::includeModule('sale'))) {
    ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALLED'));
    return;
}

class departmentLocalization extends CBitrixComponent
{
    public function __construct($component = null)
    {
        parent::__construct($component);
    }

    public function onPrepareComponentParams($params)
    {
        $params = parent::onPrepareComponentParams($params);
        if ((int)$params['HL_BLOCK_ID'] > 0 && (int)$params['SECTION_ID'] . '' != $params['HL_BLOCK_ID'] && Loader::includeModule('highloadblock'))
            return $params;
        else
            ShowError(Loc::getMessage('BLOCK_SECTION_NOT_FOUND'));

        return $params;
    }

    public function executeComponent()
    {
//        unset($_SESSION["DEPORTAMENT"]);
//        unset($_SESSION["DEPORTAMENT_MAIN"]);
//        unset($_SESSION["DEP_INFO"]);
//        unset($_SESSION["CURRENT_CITY"]);

        $request = Context::getCurrent()->getRequest();

        if (!$request->isAjaxRequest()) {

            $this->getHlDepartment("Y");
            $this->getHlDepartment();

            if ($this->arResult["DEPORTAMENT"]["UF_DEP_MAIN"] == 1 && !isset($_SESSION["CURRENT_CITY"])){
                $_SESSION["CURRENT_CITY"] = $this->arResult["CURRENT_CITY"] = $this->getSityByGeoIp();
                if($this->arResult["CURRENT_CITY"]["ID"] != $this->arResult["DEP_INFO"]) {
                    $this->getNeedleDepartment($_SESSION["CURRENT_CITY"]["REGION_ID"]);
                }
            }else{
                op($_SESSION["CURRENT_CITY"]);

            }
        }
        $this->includecomponentTemplate();
    }

    private function getLoc($arSelect, $arFilter)
    {
/*
        $filter = \Bitrix\Sale\Location\LocationTable::getList(array(
            'filter' => $arSelect,
            'select' => $arFilter,
        ))->fetchAll();*/
    }

    private function redirectUrl($target)
    {
        $request = Context::getCurrent()->getRequest();
        $curUrl = ($request->isHttps() ? 'https://' : 'http://');

        $rsSites = Bitrix\Main\SiteTable::getById($target);
        if ($arSite = $rsSites->Fetch())
            $curUrl .= $arSite["SITE_NAME"];

        $curUrl .= $request->getRequestedPageDirectory();
        LocalRedirect($curUrl);
    }

    private function getSityByGeoIp()
    {
        Bitrix\Main\Loader::includeModule("sale");
        $ipAddress = Bitrix\Main\Service\GeoIp\Manager::getRealIp();
        $ipAddress = "188.130.250.1";
        $locCode = \Bitrix\Sale\Location\GeoIp::getLocationCode($ipAddress, LANGUAGE_ID);

        $item = Bitrix\Sale\Location\LocationTable::getList(
            array(
                'filter' => array('=CODE' => $locCode, '=NAME.LANGUAGE_ID' => LANGUAGE_ID),
                'select' => array(
                    '*',
                    'NAME_RU' => 'NAME.NAME',
                    'TYPE_CODE' => 'TYPE.CODE'),
                "order" => array()
            )
        )->fetch();

        return $item;
    }

    private function getTypeLocation($idType = "", $code = "")
    {
        if ($idType != false) {
            $NAME = "ID";
            $val = $idType;
        }

        if ($code != false) {
            $NAME = "CODE";
            $val = $code;
        }

        $res = \Bitrix\Sale\Location\TypeTable::getList(array(
            'select' => array('*', 'NAME_RU' => 'NAME.NAME'),
            'filter' => array('=NAME.LANGUAGE_ID' => LANGUAGE_ID, $NAME => $val)
        ));

        if ($item = $res->fetch()) {

            if ($idType != false) {
                return $item["CODE"];
            }

            if ($code != false) {
                return $item["ID"];
            }
        }
    }

    private function getNeedleDepartment($regionId){
        $arSelect = array("UF_DEP_LOCATION", "UF_DEP_SITE");
        $hlblock = HL::getById($this->arParams["HL_BLOCK_ID"])->fetch();
        if (!empty($hlblock)) {
            $item = array();
            $entity = HL::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            $rsData = $entity_data_class::getList(array(
                "select" => $arSelect,
                "filter" => array(),
                "order" => array()
            ));
            while ($arRes = $rsData->Fetch()) {
                $locations = json_decode($arRes["UF_DEP_LOCATION"], true);
                foreach ($locations as $loc){
                    if(in_array($regionId, $loc)){
                        $this->redirectUrl($arRes["UF_DEP_SITE"]);
                        break;
                    }
                }
            }
        }
    }

    private function getHlDepartment($isMain = "")
    {
        if ($isMain == "Y") {
            $arFilter = array("UF_DEP_MAIN" => 1);
            $arSelect = array("*");
        } else {
            $arFilter = array("UF_DEP_SITE" => SITE_ID);
            $arSelect = array("*");
        }
        $hlblock = HL::getById($this->arParams["HL_BLOCK_ID"])->fetch();
        if (!empty($hlblock)) {
            $item = array();
            $entity = HL::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            $rsData = $entity_data_class::getList(array(
                "select" => $arSelect,
                "filter" => $arFilter,
                "order" => array()
            ));
            if ($arRes = $rsData->Fetch()) {
                $curDepartment = json_decode($arRes["UF_DEP_LOCATION"], true);
                foreach ($curDepartment as $key => $infoDepLocation) {
                    $searchCKey = array_key_exists("CITY", $infoDepLocation);
                    if ($searchCKey) {
                        $id = $this->getTypeLocation(false, "CITY");
                        $item = array(
                            "ID" => $infoDepLocation["CITY"],
                            "CITY_ID" => $infoDepLocation["CITY"],
                            "COUNTRY_ID" => $infoDepLocation["COUNTRY"],
                            "REGION_ID" => $infoDepLocation["REGION"],
                            "PARENT_ID" => !$infoDepLocation["SUBREGION"] ? $infoDepLocation["REGION"] : $infoDepLocation["SUBREGION"],
                            "NAME_LANG" => $arRes["UF_DEP_NAME"],
                            "TYPE_ID" => $id
                        );
                        break;
                    }
                }
            }
            if ($isMain == "Y") {
                $GLOBALS["DEPORTAMENT_MAIN"] = $arRes;
//                $_SESSION["DEPORTAMENT_MAIN"] = $this->arResult["DEPORTAMENT_MAIN"] = $arRes["ID"];
            } else {
                //                $this->arResult["ITEM"] = $item;
                global $DEPORTAMENT_MAIN;
                if($arRes["UF_DEP_CATALOG"] == 0){
                    $arRes["UF_DEP_CATALOG"] = $DEPORTAMENT_MAIN["UF_DEP_CATALOG"];
                }
                $GLOBALS["DEP_INFO"] = $this->arResult["DEP_INFO"] = $item["ID"];
                $GLOBALS["DEPORTAMENT"] = $this->arResult["DEPORTAMENT"] = $arRes;
            }
        } else
            ShowError(Loc::getMessage('HL_BLOCK_ID_NOT_FOUND'));
    }
}