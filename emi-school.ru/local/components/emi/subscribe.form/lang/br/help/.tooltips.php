<?
$MESS["SHOW_HIDDEN_TIP"] = "Especifica para exibir todas as rubricas ativas no editor de inscrição.";
$MESS["PAGE_TIP"] = "Especifica o nome do caminho da página de pesquisa.";
$MESS["CACHE_TYPE_TIP"] = "Tipo de cache";
$MESS["CACHE_TIME_TIP"] = "Tempo de cache (seg.)";
$MESS["USE_PERSONALIZATION_TIP"] = "Especifica a personalização das assinaturas do usuário atual.";
?>