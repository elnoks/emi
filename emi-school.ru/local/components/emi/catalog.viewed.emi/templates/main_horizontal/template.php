<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
$arParams['TITLE_BLOCK'] = strlen($arParams['TITLE_BLOCK']) ? $arParams['TITLE_BLOCK'] : GetMessage('CATALOG_VIEWED_TITLE');
?>
<?
use Bitrix\Main\Page\Asset;
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/js/owlcarousel/assets/owl.carousel.min.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/js/owlcarousel/assets/owl.theme.default.css');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/owlcarousel/owl.carousel.min.js');
?>
<!-- noindex -->
<?if(strlen($arResult['ERROR'])):?>
	<?ShowError($arResult['ERROR']);?>
<?else:?>
	<?if($arResult['ITEMS']):?>

		<div class="viewed_block horizontal">
			<h3 class="title_block sm"><?=$arParams["TITLE_BLOCK"]?></h3>

            <div class="owl-carousel owl-theme swipeignore" style="z-index: initial">

                <?foreach($arResult['ITEMS'] as $key=>$arItem):?>
                    <?
                    if($key > 7)
                        continue;
                    $isItem = (isset($arItem['PRODUCT_ID']) ? true : false);
                    ?>
                    <li class="item_block">
                        <?if($isItem):?>
                            <div data-id=<?=$arItem['PRODUCT_ID']?> data-picture='<?=str_replace('\'', '"', CUtil::PhpToJSObject($arItem['PICTURE']))?>' class="item_wrap item <?=($isItem ? 'has-item' : '' );?>" id=<?=$this->GetEditAreaId($arItem['PRODUCT_ID'])?>>
                                <?
                                $this->AddEditAction($arItem['PRODUCT_ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT'));
                                $this->AddDeleteAction($arItem['PRODUCT_ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
                                ?>
                            </div>
                        <?else:?>
                            <div class="item_wrap item"></div>
                        <?endif;?>
                    </li>
                <?endforeach;?>
            </div>
		</div>

        <script>
            var owl_viewed_slider = $('.viewed_block .owl-carousel');
            owl_viewed_slider.owlCarousel({

                autoplay: false,
                animateOut: 'fadeOut',
                animateIn: 'fadeIn',
                items: 5,
                margin: 15,
                nav:true,
                checkVisible:false,
                loop: true,
                responsive : {
                    0 : {
                        <?if(count($arResult['ITEMS'])<=1):?>
                            loop: false,
                        <?endif;?>
                            items: 1
                    },
                    480 : {
                        <?if(count($arResult['ITEMS'])<=2):?>
                            loop: false,
                        <?endif;?>
                            items: 2
                    },
                    768 : {
                        <?if(count($arResult['ITEMS'])<=3):?>
                            loop: false,
                        <?endif;?>
                            items: 3
                    },
                    992 : {
                        <?if(count($arResult['ITEMS'])<=4):?>
                            loop: false,
                        <?endif;?>
                            items: 4
                    },
                    1600 : {
                        <?if(count($arResult['ITEMS'])<=5):?>
                            loop: false,
                        <?endif;?>
                            items: 5
                    }
                },
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            });

            $(document).ready(function() {
                owl_viewed_slider.init();
            });
            $(window).resize(function() {
                owl_viewed_slider.init();
            });

        </script>

        <script type="text/javascript">
            BX.message({
                LAST_ACTIVE_FROM_VIEWED: '<?=$arResult['LAST_ACTIVE_FROM']?>',
                SHOW_MEASURE_VIEWED: '<?=($arParams['SHOW_MEASURE'] !== 'N' ? 'true' : '')?>',
                SITE_TEMPLATE_PATH: '<?=SITE_TEMPLATE_PATH?>',
                CATALOG_FROM_VIEWED: '<?=GetMessage('CATALOG_FROM')?>',
                SITE_ID: '<?=SITE_ID; ?>'
            });
            var lastViewedTime = BX.message('LAST_ACTIVE_FROM_VIEWED');
            var bShowMeasure = BX.message('SHOW_MEASURE_VIEWED');
            var $viewedSlider = $('.viewed_block .item_block');

            showViewedItems(lastViewedTime, bShowMeasure, $viewedSlider);
        </script>
	<?endif;?>
<?endif;?>
<!-- /noindex -->