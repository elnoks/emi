<?
$MESS["SITE_LIST_TIP"] = "Especifique aqui o site cujos pedidos serão exportados.";
$MESS["GROUP_PERMISSIONS_TIP"] = "Selecione aqui os grupos de usuários cujos membros tem permissão para exportar para 1C.";
$MESS["USE_ZIP_TIP"] = "Define o uso de compactação ZIP, se possível.";
$MESS["EXPORT_PAYED_ORDERS_TIP"] = "Se selecionado, somente pedidos pagos serão exportados para 1C";
$MESS["EXPORT_ALLOW_DELIVERY_ORDERS_TIP"] = "Se selecionado, somente os pedidos com autorização de entrega serão exportados para 1C.";
$MESS["EXPORT_FINAL_ORDERS_TIP"] = "Especifique aqui o status mínimo que um pedido deve ter para exportação.";
$MESS["FINAL_STATUS_ON_DELIVERY_TIP"] = "Especifique aqui o status a ser aplicado para um pedido se a exportação para 1C tiver sido concluída e o site, notificado.";
$MESS["REPLACE_CURRENCY_TIP"] = "A moeda do item pode ser substituída por uma outra especificada neste campo, o que não irá converter o preço atual.";
?>