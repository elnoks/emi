<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>
<?$isAjax = (isset($_GET["AJAX_REQUEST"]) && $_GET["AJAX_REQUEST"] == "Y");?>
<?if($arResult['ITEMS']):?>
	<?if(!$isAjax):?>
		<div class="banners-small blog">
	<?endif;?>

            <? $i=0;?>
			<?foreach($arResult['ITEMS'] as $arItems):?>
				<div class="items row">

					<?foreach($arItems['ITEMS'] as $key => $arItem):?>
						<?
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

						// preview image
                        if($arItem["PROPERTIES"]["PUB_PAGES"]["VALUE"]){
                            $bImage = CFile::ResizeImageGet($arItem["PROPERTIES"]["PUB_PAGES"]["VALUE"][0], Array("width" => 866, "height" => 620), BX_RESIZE_IMAGE_PROPORTIONAL, false);
                        }

                        //$bImage = (is_array($arItem['DETAIL_PICTURE']) && $arItem['DETAIL_PICTURE']['SRC']);
						$imageSrc = ($bImage ? $bImage['src'] : false);

						// use detail link?
						$bDetailLink = $arParams['SHOW_DETAIL_LINK'] != 'N' && (!strlen($arItem['DETAIL_TEXT']) ? ($arParams['HIDE_LINK_WHEN_NO_DETAIL'] !== 'Y' && $arParams['HIDE_LINK_WHEN_NO_DETAIL'] != 1) : true);

						$isWideBlock = (isset($arItem['CLASS_WIDE']) && $arItem['CLASS_WIDE']);
						$hasWideBlock = (isset($arItem['CLASS']) && $arItem['CLASS']);

						?>
						<?if(isset($arItem['START_DIV']) && $arItem['START_DIV'] == 'Y'):?>
							<div class="col-md-4 col-sm-4">
						<?endif;?>
							<div class="<?=((isset($arItem['CLASS']) && $arItem['CLASS']) ? $arItem['CLASS'] : 'col-md-4 col-sm-4');?>">
								<div class="item shadow animation-boxs <?=($isWideBlock ? 'wide-block' : '')?> <?=($hasWideBlock ? '' : 'normal-block')?>"  id="<?=$this->GetEditAreaId($arItem['ID']);?>">
									<div class="inner-item">
										<?if($bImage):?>
											<div class="image shine">
												<?if($bDetailLink):?><a href="#" onclick="openPublication(<?=$i?>);return false;"><?endif;?>
												<img class="thumbnail-gallery" src=<?=$imageSrc?> alt="<?=($bImage ? $arItem['DETAIL_PICTURE']['ALT'] : $arItem['NAME'])?>" title="<?=($bImage ? $arItem['DETAIL_PICTURE']['TITLE'] : $arItem['NAME'])?>" />
												<?if($bDetailLink):?></a><?endif;?>
											</div>
										<?endif;?>
										<div class="title">
											<?if($bDetailLink):?><a href="#" onclick="openPublication(<?=$i?>);return false;"><?endif;?>
												<span><?=$arItem['NAME']?></span>
											<?if($bDetailLink):?></a><?endif;?>
										</div>
									</div>
								</div>
							</div>
						<?if(isset($arItem['END_DIV']) && $arItem['END_DIV'] == 'Y'):?>
							</div>
						<?endif;?>

                        <?
                            $i=$i + count($arItem["PROPERTIES"]["PUB_PAGES"]["VALUE"]);

                            $pics[$i]["img"] = $arItem["PROPERTIES"]["PUB_PAGES"]["VALUE"];
                            $pics[$i]["title"] = $arItem['NAME'];

//                            echo "<pre>";
//                            print_r($pics);
//                            echo "</pre>";
                        ?>

					<?endforeach;?>
				</div>
			<?endforeach;?>
			<div class="bottom_nav" <?=($isAjax ? "style='display: none; '" : "");?>>
				<?if( $arParams["DISPLAY_BOTTOM_PAGER"] == "Y" ){?><?=$arResult["NAV_STRING"]?><?}?>
			</div>
	<?if(!$isAjax):?>
		</div>
	<?endif;?>
<?endif;?>
<?
// PhotoSwipe

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/js/photoswipe/photoswipe.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/js/photoswipe/default-skin/default-skin.css');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/photoswipe/photoswipe-ui-default.min.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/photoswipe/photoswipe.min.js');

?>
<?//<!-- Root element of PhotoSwipe. Must have class pswp. --> ?>
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>
<script>
    var openPublication = function (item_index) {
        //console.log(item_index);
        var pswpElement = document.querySelectorAll('.pswp')[0];
        var items = [
            <?foreach($pics as $pic){
                foreach ($pic["img"] as $picture) {
                    $file = CFile::ResizeImageGet($picture, Array("width" => 1600, "height" => 1600), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                    echo "{";
                        echo "src: '".$file["src"]."',";
                        echo "w: ".$file["width"].",";
                        echo "h: ".$file["height"].",";
                        //echo "msrc: 'path/to/small-image.jpg',";
                        echo "title: '".$pic['title']."'";
                    echo "},";
                }
            }
            ?>
        ];


        var options = {
            bgOpacity: 0.6,
            // Same as above, but this timer applies when mouse leaves the window
            timeToIdleOutside: 1000,

            // Delay until loading indicator is displayed
            loadingIndicatorDelay: 1000,
            timeToIdle: 4000,

            // Buttons/elements
            closeEl: true,
            captionEl: true,
            fullscreenEl: true,
            zoomEl: true,
            shareEl: false,
            counterEl: true,
            arrowEl: true,
            preloaderEl: true,
            history: false,
            focus: false,
            showAnimationDuration: 0,
            hideAnimationDuration: 0,
        };

        var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

        gallery.init();

        if (item_index) {
            //item_index = item_index - 1;
            gallery.goTo(item_index);
        }
    };

</script>