<?
$MESS["IBLOCK_CACHE_FILTER"] = "Кешировать при установленном фильтре";
$MESS["T_IBLOCK_DESC_ASC"] = "По возрастанию";
$MESS["T_IBLOCK_DESC_DESC"] = "По убыванию";
$MESS["T_IBLOCK_DESC_FID"] = "ID";
$MESS["T_IBLOCK_DESC_FNAME"] = "Название";
$MESS["T_IBLOCK_DESC_FSORT"] = "Сортировка";
$MESS["T_IBLOCK_DESC_IBORD1"] = "Поле для первой сортировки";
$MESS["T_IBLOCK_DESC_IBBY1"] = "Направление для первой сортировки";
$MESS["T_IBLOCK_DESC_IBORD2"] = "Поле для второй сортировки";
$MESS["T_IBLOCK_DESC_IBBY2"] = "Направление для второй сортировки";
$MESS["T_IBLOCK_DESC_LIST_ID"] = "Код информационного блока";
$MESS["T_IBLOCK_DESC_LIST_TYPE"] = "Тип информационного блока (используется только для проверки)";
$MESS["T_IBLOCK_DESC_AMOUNT_OF_SLIDES"] = "Количество элементов в слайдере";
$MESS["T_IBLOCK_GET_SECTION_LEVEL"] = "Уровень вложенности раздела";
$MESS["T_IBLOCK_FILTER"] = "Фильтр";
$MESS["T_IBLOCK_DESC_CHECK_DATES"] = "Показывать только активные на данный момент элементы";
$MESS["CP_BNL_SET_STATUS_404"] = "Устанавливать статус 404, если не найдены элемент или раздел";
?>