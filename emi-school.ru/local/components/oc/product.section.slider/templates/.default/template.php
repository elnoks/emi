<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- Owl carousel -->
<link rel="stylesheet" href="<?="/bitrix/templates/.default/plugins/owl.carousel/owl-carousel/owl.carousel.css"?>">
<link rel="stylesheet" href="<?="/bitrix/templates/.default/plugins/owl.carousel/owl-carousel/owl.theme.css"?>">
<script src="<?="/bitrix/templates/.default/plugins/owl.carousel/owl-carousel/owl.carousel.js"?>"></script>
	<div class="slider-container shadow">
			<? if($arParams["TITLE"]):?>
			<div class="h2"><?=$arParams["TITLE"]?></div>
			<? endif;?>
        	<div id="product-slider" class="owl-carousel product-slider">
        		<? foreach ($arResult["ITEMS"] as $arItem):?>
        		<? if(is_array($arItem["PICTURE"])):?>
	  				<div class="item shadow"> 
	  					<?if ($arItem["SECTION_PAGE_URL"]=='aksessuary/#catalog') {
	  							$arItem["SECTION_PAGE_URL"] = 'catalog/#aksessuary';
	  						}?>
	  					<a href="<?=$arItem["SECTION_PAGE_URL"]?>">
	  						<img class="lazyOwl" data-src="<?=$arItem["PICTURE"]["SRC"]?>" src="<?=$arItem["PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>">
		  					<div class="heading">
		  						<span><?=$arItem["NAME"]?></span>
		  					</div>
		  				</a>
	  				</div>
	  			<? endif;?>
  				<? endforeach;?>
			</div>
	</div>
			<script>
			$(document).ready(function() {
 				$("#product-slider").owlCarousel({
    				items : 5,
    				lazyLoad : true,
    				navigation : true,
    				pagination: false,
    				navigationText:	['<i class="fa fa-angle-left fa-3x"></i>','<i class="fa fa-angle-right fa-3x"></i>']
  				}); 
			});
			</script>
        <!-- /.products-slider-->