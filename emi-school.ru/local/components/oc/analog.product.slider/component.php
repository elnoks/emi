<?
/*компонент слайдера, который выбирает элементы инфоблока (или раздела инфоблока) с ценами.
 * TO DO (возможно) выбор свойств вроде новинки, акции или популярного.*/
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

/** @global CIntranetToolbar $INTRANET_TOOLBAR */
global $INTRANET_TOOLBAR;

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
if(strlen($arParams["IBLOCK_TYPE"])<=0)
	$arParams["IBLOCK_TYPE"] = "catalog";
$arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);
$arParams["SECTION_LEVEL"] = intval($arParams["SECTION_LEVEL"]);
$arParams["INCLUDE_SUBSECTIONS"] = $arParams["INCLUDE_SUBSECTIONS"]!="N";

$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
if(strlen($arParams["SORT_BY1"])<=0)
	$arParams["SORT_BY1"] = "ACTIVE_FROM";
if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER1"]))
	$arParams["SORT_ORDER1"]="DESC";

if(strlen($arParams["SORT_BY2"])<=0)
	$arParams["SORT_BY2"] = "SORT";
if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER2"]))
	$arParams["SORT_ORDER2"]="ASC";

if(!is_array($arParams["PRICE_CODE"]))
	$arParams["PRICE_CODE"] = array();

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	$arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
	if(!is_array($arrFilter))
		$arrFilter = array();
}

$arParams["CHECK_DATES"] = $arParams["CHECK_DATES"]!="N";

$arParams["DETAIL_URL"]=trim($arParams["DETAIL_URL"]);

$arParams["CACHE_FILTER"] = $arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
	$arParams["CACHE_TIME"] = 0;
	///////
/*if ($this->StartResultCache()){
	
	CModule::IncludeModule('iblock');
	
	$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], array("Розничная"));
	$ob = CIBlockElement::GetList(array("IBLOCK_SECTION_ID" => "ASC"), array('IBLOCK_ID' => $arParams["IBLOCK_ID"], "ID" => $arParams["ANALOGUES"]), false, false, array('ID', "NAME", "PREVIEW_PICTURE", 'DETAIL_PAGE_URL','CATALOG_GROUP_5'));
		while($elem = $ob->GetNext()){
			$arResult["ITEMS"][$elem["ID"]]["PRICES"] = CIBlockPriceTools::GetItemPrices($arParams["IBLOCK_ID"], $arResult["PRICES"], $elem, true, array(), 0, 's1');
			if($elem["PREVIEW_PICTURE"])
				$arResult["ITEMS"][$elem["ID"]]['PREVIEW_PICTURE'] = CFile::GetFileArray($elem["PREVIEW_PICTURE"]);
				
			$arResult["ITEMS"][$elem["ID"]]["NAME"] = $elem["NAME"];
			$arResult["ITEMS"][$elem["ID"]]["DETAIL_PAGE_URL"] = $elem["DETAIL_PAGE_URL"];
		}	
	
	$this->SetResultCacheKeys(array(
			"PRICES"		
	));
	$this->IncludeComponentTemplate();
	
}else
	$this->AbortResultCache();
*/
/////////
	
	


if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $bUSER_HAVE_ACCESS, $arNavigation, $arrFilter)))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	if(is_numeric($arParams["IBLOCK_ID"]))
	{
		$rsIBlock = CIBlock::GetList(array(), array(
			"ACTIVE" => "Y",
			"ID" => $arParams["IBLOCK_ID"],
		));
	}
	else
	{
		$rsIBlock = CIBlock::GetList(array(), array(
			"ACTIVE" => "Y",
			"CODE" => $arParams["IBLOCK_ID"],
		));
	}
	if($arResult = $rsIBlock->GetNext())
	{
		$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arResult["ID"], $arParams["PRICE_CODE"]);
		
		//SELECT
		$arSelect = array(
				"ID", 
				"NAME", 
				"DETAIL_PAGE_URL",
				"PREVIEW_PICTURE",
			);
		foreach($arResult["PRICES"] as $value)
		{
			$arSelect[] = $value["SELECT"];
		} 
		
		$arFilter = array (
			"IBLOCK_ID" => $arResult["ID"],
			"ID" => $arParams["SLIDER_PROPERTY_TYPE"],
			"ACTIVE" => "Y",
		);
		if($arParams["SECTION_CODE"]) {
			$arFilter = array_merge($arFilter,array(
				"SECTION_CODE" => $arParams["SECTION_CODE"],
			));
		}
		
		//ORDER BY
		$arSort = array(
			$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
			$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
		);
		if(!array_key_exists("ID", $arSort))
			$arSort["ID"] = "DESC";

		$arResult["ITEMS"] = array();
		$rsElement =  CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, false, $arSelect);
		while($obElement = $rsElement->GetNextElement())
		{
			$arItem = $obElement->GetFields();
			
			if(isset($arItem["PREVIEW_PICTURE"]))
			{
				$arItem["PREVIEW_PICTURE"] = (0 < $arItem["PREVIEW_PICTURE"] ? CFile::GetFileArray($arItem["PREVIEW_PICTURE"]) : false);
				
			}
			$arItem["PRICES"] = CIBlockPriceTools::GetItemPrices($arResult["ID"], $arResult["PRICES"], $arItem, true, array(), 0, $arParams["SITE_ID"]);

			$arResult["ITEMS"][] = $arItem;
		}
		$this->SetResultCacheKeys(array(
			"ID",
			"IBLOCK_TYPE_ID",
			"LIST_PAGE_URL",
			"NAME"
		));
		$this->IncludeComponentTemplate();
	}
	else
	{
		$this->AbortResultCache();
		ShowError(GetMessage("T_NEWS_NEWS_NA"));
		@define("ERROR_404", "Y");
		if($arParams["SET_STATUS_404"]==="Y")
			CHTTP::SetStatus("404 Not Found");
	}
}

?>