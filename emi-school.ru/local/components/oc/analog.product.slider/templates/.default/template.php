<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- Owl carousel -->
<link rel="stylesheet" href="<?="/bitrix/templates/.default/plugins/owl.carousel/owl-carousel/owl.carousel.css"?>">
<link rel="stylesheet" href="<?="/bitrix/templates/.default/plugins/owl.carousel/owl-carousel/owl.theme.css"?>">
<script src="<?="/bitrix/templates/.default/plugins/owl.carousel/owl-carousel/owl.carousel.js"?>"></script>
	<!-- PRODUCTS-SLIDER -->
	<div class="slider-container shadow">
			<? if($arParams["TITLE"]):?>
			<h2><?=$arParams["TITLE"]?></h2>
			<?endif;?>
        <div id="<?=$arParams['DIV_ID']?>" class="owl-carousel product-slider">
        	<?foreach ($arResult["ITEMS"] as $arItem):?>
	  			<div class="item-container shadow relative"> 
	  				<?
					if($arParams["COURSES_PAGE"]){
						$detailUrl = substr($arItem["DETAIL_PAGE_URL"], 8); //вырезание "/courses"
					}else{ 
						$detailUrl = $arItem["DETAIL_PAGE_URL"];
					}
					?>
	  				<a href="<?=$arParams["SITE_PATH_BEFORE_DETAIL_PAGE_URL"].$detailUrl?>">
			  			<div class="item-image-container">
			  			
			  				<?if(is_array($arItem["PREVIEW_PICTURE"])):?>
								<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt>
							<?else:?>
								<img src="<?=$this->GetFolder() . '/img/no_photo.png'?>" alt>
							<?endif;?>		
						</div>
		  				<div class="item-name-container">
							<span class="item-name"><?=$arItem["NAME"]?></span>
						</div>	
		  			</a>
					<div class="item-price-container">
						<?if (!empty($arItem["PRICES"])):?>
							<span class="item-price"><?=$arItem["PRICES"][$arParams["PRICE_CODE"][0]]["PRINT_VALUE"]?></span>
						<?else:?>
							<span class="item-price"><?=GetMessage("NO_PRICE")?></span>
						<?endif;?>
					</div>
	  			</div>
  			<?endforeach;?>
		</div>
	</div>
		<script>
			$(document).ready(function() {
 				$("#" + "<?=$arParams['DIV_ID']?>").owlCarousel({
    				items : <?=$arParams["AMOUNT_OF_SLIDES"]?>,
    				lazyLoad : true,
    				navigation : true,
    				pagination: false,
    				navigationText:	['<i class="fa fa-angle-left fa-3x"></i>','<i class="fa fa-angle-right fa-3x"></i>']
  				}); 
			});
			</script>
        <!-- /.products-slider-->