<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if($arResult["ITEMS"][0]){
?>
    <script src="<?="/bitrix/templates/.default/plugins/portfoliojs/"?>js/jquery.easing.1.3.min.js" type="text/javascript"></script>
    <script src="<?="/bitrix/templates/.default/plugins/portfoliojs/"?>js/jquery.touchSwipe.min.js" type="text/javascript"></script>
    <script src="<?="/bitrix/templates/.default/plugins/portfoliojs/"?>js/jquery.imagesloaded.min.js" type="text/javascript"></script>
    <script src="<?="/bitrix/templates/.default/plugins/portfoliojs/"?>js/jquery.scrollTo-1.4.3.1-min.js" type="text/javascript"></script>
    <script src="<?="/bitrix/templates/.default/plugins/portfoliojs/"?>js/spin.min.js" type="text/javascript"></script>
    <script src="<?="/bitrix/templates/.default/plugins/portfoliojs/"?>js/portfolio.js" type="text/javascript"></script>
	<!-- PRODUCTS-SLIDER -->
	<div>
			<? if($arParams["TITLE"]):?>
			<h2><?=$arParams["TITLE"]?></h2>
			<? endif;?>
        	<div id="gallery">
        		<? foreach ($arResult["ITEMS"] as $arItem):?>
        		<?/* if(is_array($arItem["PREVIEW_PICTURE"])):?>
  				<div > 
  					<a href="<?=$arParams["SITE_PATH_BEFORE_DETAIL_PAGE_URL"].$arItem["DETAIL_PAGE_URL"]?>">*/?>
  						<img data-src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>">
	  					<?/*<div class="info">
		  					<div class="heading">
		  						<span><?=$arItem["NAME"]?></span>
		  						<? if (!empty($arItem["PRICES"])):?>
			  					<div class="price"><span><?=$arItem["PRICES"][$arParams["PRICE_CODE"][0]]["PRINT_VALUE"]?><!--  </span><i class="fa fa-rouble"></i>--></div>
			  					<? endif;?>
		  					</div>
		  					
		  				</div>*/
		  				/*?>
	  				</a>
  				</div>
  				<? endif;*/?>
  				<? endforeach;?>
		</div>
	</div>
		<?/*<script>
			$(document).ready(function() {
 				$(".fotoslider").owlCarousel2({
    				items : 2,
    				lazyLoad : true,
    				navigation : true,
    				pagination: false,
    				navigationText:	['<i class="fa fa-angle-left fa-3x"></i>','<i class="fa fa-angle-right fa-3x"></i>']
  				}); 
			});
			</script>*/?>
			<script type="text/javascript">
		      $(document).ready(function() {
		            var p = $("#gallery").portfolio({ 
		                loop: true, // loop on/off
		                easingMethod: 'easeOutQuint', // other easing methods please refer: http://gsgd.co.uk/sandbox/jquery/easing/
		                height: '400px', // gallery height
		                width: '100%', // gallery width in pixels or in percentage
		                lightbox: true, // dim off other images, highlights only currently viewing image
		                logger: false // for debugging purpose, turns on/off console.log() statements in the script
				    });
		            p.init();
		        }); ;
			</script>
        <!-- /.products-slider-->
<?}?>