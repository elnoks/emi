<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if($arResult["ITEMS"][0]){
?>
<!-- Owl carousel -->
<link rel="stylesheet" href="<?="/bitrix/templates/.default/plugins/owl.carousel/owl-carousel/owl.carousel.css"?>">
<link rel="stylesheet" href="<?="/bitrix/templates/.default/plugins/owl.carousel/owl-carousel/owl.theme.css"?>">
<script src="<?="/bitrix/templates/.default/plugins/owl.carousel/owl-carousel/owl.carousel.js"?>"></script>
	<!-- PRODUCTS-SLIDER -->
	<div class="slider-container shadow">
			<? if($arParams["TITLE"]):?>
			<h2><?=$arParams["TITLE"]?></h2>
			<?endif;?>
        <div class="courses-slider owl-carousel product-slider courses-sliderm-num-<?=$arParams["AMOUNT_OF_SLIDES"]?>">
        	<?foreach ($arResult["ITEMS"] as $arItem):?>
	  			<div class="item-container shadow relative"> 
					<?
					if($arParams["CATALOG_PAGE"]){
						$detailPage = substr($arItem["DETAIL_PAGE_URL"],8);
					}else{ 
						$detailPage = $arItem["DETAIL_PAGE_URL"];
					}
					?>
	  				<a href="<?=$detailPage?>">
			  			<div class="item-image-container">
			  			
			  				<?if(is_array($arItem["PREVIEW_PICTURE"])):?>
								<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt>
							<?else:?>
								<img src="<?=$this->GetFolder() . '/img/no_photo.png'?>" alt>
							<?endif;?>		
						</div>
		  				<div class="item-name-container">
							<span class="item-name"><?=$arItem["NAME"]?></span>
						</div>	
		  			</a>
					<div class="item-price-container">
						<?if (!empty($arItem["PRICES"])):?>
							<span class="item-price"><?=$arItem["PRICES"][$arParams["PRICE_CODE"][0]]["PRINT_VALUE"]?></span>
						<?else:?>
							<span class="item-price"><?=GetMessage("NO_PRICE")?></span>
						<?endif;?>
					</div>
	  			</div>
  			<?endforeach;?>
		</div>
	</div>
		<script>
			$(document).ready(function() {
				$(".courses-sliderm-num-<?=$arParams["AMOUNT_OF_SLIDES"]?>").owlCarousel({
    				items : <?=$arParams["AMOUNT_OF_SLIDES"]?>,
    				lazyLoad : true,
    				navigation : true,
    				pagination: false,
    				navigationText:	['<i class="fa fa-angle-left fa-3x"></i>','<i class="fa fa-angle-right fa-3x"></i>']
  				}); 
			});
			</script>
        <!-- /.products-slider-->
<?}?>