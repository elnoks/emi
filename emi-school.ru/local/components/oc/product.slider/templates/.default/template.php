<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- Owl carousel -->
<link rel="stylesheet" href="<?="/bitrix/templates/.default/plugins/owl.carousel/owl-carousel/owl.carousel.css"?>">
<link rel="stylesheet" href="<?="/bitrix/templates/.default/plugins/owl.carousel/owl-carousel/owl.theme.css"?>">
<script src="<?="/bitrix/templates/.default/plugins/owl.carousel/owl-carousel/owl.carousel.js"?>"></script>
	<!-- PRODUCTS-SLIDER -->
	<div class="slider-container shadow">
			<? if($arParams["TITLE"]):?>
			<div class="h2"><?=$arParams["TITLE"]?></div>
			<? endif;?>
        	<div class="courses-slider owl-carousel product-slider courses-sliderm-num-<?=$arParams["AMOUNT_OF_SLIDES"]?>">
        		<? foreach ($arResult["ITEMS"] as $arItem):?>
        		<? if(is_array($arItem["PREVIEW_PICTURE"])):?>
  				<div class="item shadow"> 
  					<a href="<?=$arParams["SITE_PATH_BEFORE_DETAIL_PAGE_URL"].$arItem["DETAIL_PAGE_URL"]?>">
  						<img class="lazyOwl" data-src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>" <?if($arParams["CATALOG_PAGE"]) echo 'style="max-width:165px"';?>>
	  					<div class="info">
		  					<div class="heading">
		  						<span><?=$arItem["NAME"]?></span>
		  						<? if (!empty($arItem["PRICES"])):?>
			  					<div class="price"><span><?=$arItem["PRICES"][$arParams["PRICE_CODE"][0]]["PRINT_VALUE"]?><!--  </span><i class="fa fa-rouble"></i>--></div>
			  					<? endif;?>
		  					</div>
		  					
		  				</div>
	  				</a>
  				</div>
  			<? endif;?>
  				<? endforeach;?>
		</div>
	</div>
		<script>
			$(document).ready(function() {
 				$(".courses-sliderm-num-<?=$arParams["AMOUNT_OF_SLIDES"]?>").owlCarousel({
    				items : <?=$arParams["AMOUNT_OF_SLIDES"]?>,
    				lazyLoad : true,
    				navigation : true,
    				pagination: false,
    				navigationText:	['<i class="fa fa-angle-left fa-3x"></i>','<i class="fa fa-angle-right fa-3x"></i>']
  				}); 
			});
			</script>
        <!-- /.products-slider-->