<?
$MESS["IBLOCK_CACHE_FILTER"] = "Cache if the filter is active";
$MESS["T_IBLOCK_DESC_ASC"] = "Ascending";
$MESS["T_IBLOCK_DESC_DESC"] = "Descending";
$MESS["T_IBLOCK_DESC_FID"] = "ID";
$MESS["T_IBLOCK_DESC_FNAME"] = "Name";
$MESS["T_IBLOCK_DESC_FSORT"] = "Sorting";
$MESS["T_IBLOCK_DESC_IBORD1"] = "Field for the elements first sorting pass";
$MESS["T_IBLOCK_DESC_IBBY1"] = "Direction for the elements first sorting pass";
$MESS["T_IBLOCK_DESC_IBORD2"] = "Field for the elements second sorting pass";
$MESS["T_IBLOCK_DESC_IBBY2"] = "Direction for the elements second sorting pass";
$MESS["T_IBLOCK_DESC_LIST_ID"] = "Information block code";
$MESS["T_IBLOCK_DESC_LIST_TYPE"] = "Type of information block (used for verification only)";
$MESS["T_IBLOCK_DESC_AMOUNT_OF_SLIDES"] = "Amount of slides";
$MESS["T_IBLOCK_GET_SECTION_CODE"] = "Section for getting elements";
$MESS["T_IBLOCK_FILTER"] = "Filter";
$MESS["T_IBLOCK_DESC_CHECK_DATES"] = "Show only currently active elements";
$MESS["CP_BNL_SET_STATUS_404"] = "Set 404 status if no element or section was found";
$MESS["IBLOCK_PRICE_CODE"] = 'Price code';
$MESS["SITE_ID"] = "Site ID";
$MESS["SITE_PATH_BEFORE_DETAIL_PAGE_URL"] = "Site pathe before detail page url";
?>