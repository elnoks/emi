<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$obCache = new CPHPCache();
$cacheLifetime = $arParams["PHP_CACHE_TIME"];
$cacheID = 'instagramLastPost'.SITE_ID;
$cachePath = '/'.$cacheID;

if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
    $vars = $obCache->GetVars();
    $arResult["POSTS"] = $vars['arInstagramLastPost'.SITE_ID];
} elseif ($obCache->StartDataCache()) {
    $arResult["POSTS"] = $this->getPosts($arParams["USERNAME"]);
    $obCache->EndDataCache(array('arInstagramLastPost'.SITE_ID => $arResult["POSTS"]));
}
//print_r($arResult["POSTS"]);
$this->IncludeComponentTemplate();
?>
