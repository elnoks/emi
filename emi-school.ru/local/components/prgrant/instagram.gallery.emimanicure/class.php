<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class CInstagram extends CBitrixComponent
{
    public function getPosts($username)
    {

        if (function_exists('curl_init')) {
            $url = "https://apinsta.herokuapp.com/u/" . $username . "/";
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch);
            curl_close($ch);
            return json_decode($response);


        } else {
            return false;
        }
    }
//    public function getPosts2Page($username,$end_cursor)
//    {
//        if (function_exists('curl_init')) {
//
//            //$end_cursor = json_decode($response)->user->media->page_info->end_cursor;
//            $url = "https://www.instagram.com/" . $username . "/?__a=1&max_id=".$end_cursor;
//            $ch = curl_init($url);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
//            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//            $response = curl_exec($ch);
//            curl_close($ch);
//            print_r(json_decode($response)->user->media->page_info->end_cursor);
//            return json_decode($response);
//
//
//
//            $url = "https://www.instagram.com/" . $username . "/?__a=1";
//            $ch = curl_init($url);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
//            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//            $response = curl_exec($ch);
//            curl_close($ch);
//            return json_decode($response);
//
//
//        } else {
//            return false;
//        }
//    }

}?>
