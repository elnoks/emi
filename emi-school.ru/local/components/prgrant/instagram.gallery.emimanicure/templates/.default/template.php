<?
//   echo "<pre>";
//   print_r($arResult["POSTS"]);
?>
<div class="instagram">
    <div class="instagram-list">
    <? foreach ($arResult["POSTS"]->graphql->user->edge_owner_to_timeline_media->edges as $key => $post): ?>

        <div class="col-md-2 col-sm-3 col-xs-4">
            <a href="https://www.instagram.com/p/<?=$post->node->shortcode?>" target="_blank">

                <img src="<?=$post->node->thumbnail_resources[3]->src?>">
                <div class="instagram-info">
                    <div class="instagram-icon instagram-icon--likes">
                        <span><? echo number_format($post->node->edge_liked_by->count, 0, '.', ' '); ?></span>
                    </div>
                    <div class="instagram-icon instagram-icon--comments">
                        <span><? echo number_format($post->node->edge_media_to_comment->count, 0, '.', ' '); ?> <span class="horizontal-line"></span></span>
                    </div>
                </div>

            </a>
        </div>
    <? endforeach; ?>
    </div>
</div>
