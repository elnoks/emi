<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$obCache = new CPHPCache();
$cacheLifetime = $arParams["PHP_CACHE_TIME"];
$cacheID = 'instagramEmimanicure'.SITE_ID;
$cachePath = '/'.$cacheID;
if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
    $vars = $obCache->GetVars();
    $arResult["POSTS"] = $vars['arInstagramEmimanicure'.SITE_ID];
} elseif ($obCache->StartDataCache()) {
    $arResult["POSTS"] = $this->getPosts($arParams["USERNAME"]);
    $obCache->EndDataCache(array('arInstagramEmimanicure'.SITE_ID => $arResult["POSTS"]));
}

$this->IncludeComponentTemplate();
?>
