<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class CInstagram extends CBitrixComponent
{
    public function getPosts($username, $endcursor = '')
    {
        if (function_exists('curl_init')) {
            $url = "https://apinsta.herokuapp.com/u/" . $username . "/";
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch);
            curl_close($ch);

            return json_decode($response);
        } else {
            return false;
        }
    }
}?>
