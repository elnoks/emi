<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => GetMessage("DSS_INSTAGRAM_NAME"),
    "DESCRIPTION" => GetMessage("DSS_INSTAGRAM_DESCRIPTION"),
    "PATH" => array(
        "ID" => "prgrant",
        "NAME" => "prgrant"
    ),
);
?>
