<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * Created by PhpStorm.
 * User: Andrey Senin
 * Date: 22.08.2017
 * Time: 10:39
 */

if ($arResult['VG_VIDEO_ITEMS'])
{

?>
<div id="gallery_<?=$arResult['VG_PLAYER_ID']?>">
    <?
    foreach ($arResult['VG_VIDEO_ITEMS'] as $item){
    ?>
        <div data-type="youtube"
             data-videoid="<?=$item['ID'];?>"
             data-title="<?=$item['TITLE'];?>"
             data-description="<?=$item['DESCRIPTION'];?>">
        </div>
    <?
    }
    ?>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#gallery_<?=$arResult['VG_PLAYER_ID']?>").unitegallery({
            gallery_theme: "video",
            theme_skin: "<?=$arResult["VG_PLAYLIST_SKIN"];?>",
            theme_autoplay: <?=$arResult["VG_AUTOPLAY"];?>,
            theme_disable_panel_timeout: <?=$arResult["VG_DISABLE_PANEL_TIMEOUT"];?>,
            gallery_width: 900,
            gallery_height: 313
        });
    });
</script>
<?
}
?>