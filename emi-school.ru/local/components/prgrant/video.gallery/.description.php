<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * Created by PhpStorm.
 * User: Andrey Senin
 * Date: 22.08.2017
 * Time: 10:58
 */
$arComponentDescription = array(
    "NAME" => GetMessage("YOUTUBE_GALLERY_NAME"),
    "DESCRIPTION" => GetMessage("YOUTUBE_GALLERY_DESC"),
    "ICON" => "/images/youtube.gallery.gif",
    "PATH" => array(
        "ID" => "prgrant",
    ),
);

?>