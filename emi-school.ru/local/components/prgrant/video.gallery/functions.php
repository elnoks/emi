<?php
/**
 * Created by PhpStorm.
 * User: Bogdan
 * Date: 13.04.2018
 * Time: 12:09
 */


function sort_date($a, $b)
{
    return strcmp($b["PUBLISHED_DATE"], $a["PUBLISHED_DATE"]);
}

function trim_to_dot($str) {
    $pattern = '/^.{50,150}?[\?|\!|\.]/';
    $res = array();
    if (preg_match($pattern, $str, $res) && !empty($res[0])){
        $rtn = $res[0];
    }
    else
    {
        $rtn = $str;
    }

    $rtn = str_replace("↓","",$rtn);

    if (strlen($rtn)>60){
        $rtn = substr($rtn, 0, 60);
        $rtn = $rtn."...";
    }
    $pos = strpos($rtn, "🌸");
    if ($pos === false) {
    }
    else
    {
        $rtn=substr($rtn, 0, $pos);
    }

    $pos = strpos($rtn, "🌺");
    if ($pos === false) {
    }
    else
    {
        $rtn=substr($rtn, 0, $pos);
    }

//    echo $rtn;
    return $rtn;
}

function get_youtube_data($id,$key){
    if ($key){
        $url = "https://www.googleapis.com/youtube/v3/videos?part=snippet&id=$id&key=$key";
        $json = file_get_contents($url);
        $json=json_decode($json);

        return array(
            "TITLE"=>$json->items[0]->snippet->title,
            "DESCRIPTION"=>trim_to_dot($json->items[0]->snippet->description),
            "PUBLISHED_DATE"=>$json->items[0]->snippet->publishedAt,
        );
    }
}

function get_youtube_data_playlists($playlist_id,$key){
    if ($key){
        $url = "https://www.googleapis.com/youtube/v3/playlistItems?maxResults=25&part=snippet&playlistId=$playlist_id&key=$key";
        $json = file_get_contents($url);
        $json=json_decode($json);
        foreach ($json->items as $item){
            $ids[] = $item->snippet->resourceId->videoId;
        }
        return $ids;
    }
}