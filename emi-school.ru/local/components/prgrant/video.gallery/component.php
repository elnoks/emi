<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * Created by PhpStorm.
 * User: Andrey Senin
 * Date: 22.08.2017
 * Time: 11:39
 */

Bitrix\Main\Page\Asset::getInstance()->addJs("/local/components/prgrant/video.gallery/js/video.gallery.js");
//$APPLICATION->AddHeadScript('/bitrix/components/prgrant/video.gallery/js/video.gallery.js');
include_once("functions.php");

$arResult['VG_PLAYLIST_SKIN'] = $arParams["VG_PLAYLIST_SKIN"];
if (!$arParams['VG_PLAYLIST_SKIN'])
    $arResult['VG_PLAYLIST_SKIN']="right-thumb";

$arResult['VG_AUTOPLAY'] = $arParams["VG_AUTOPLAY"];
    ($arParams["VG_AUTOPLAY"]=="Y") ? $arResult['VG_AUTOPLAY']="true" : $arResult['VG_AUTOPLAY']="false";
if (!$arParams['VG_AUTOPLAY'])
    $arResult['VG_AUTOPLAY']="false";

$arResult['VG_DISABLE_PANEL_TIMEOUT'] = $arParams["VG_DISABLE_PANEL_TIMEOUT"];
if (!$arParams['VG_DISABLE_PANEL_TIMEOUT'])
    $arResult['VG_DISABLE_PANEL_TIMEOUT']=2500;

$arResult['VG_VIDEO_VALUE'] = $arParams["VG_VIDEO_VALUE"];
if (!$arParams['VG_VIDEO_VALUE'])
    $arResult['VG_VIDEO_VALUE'] = "";

$arResult['VG_PLAYER_ID'] = $arParams["VG_PLAYER_ID"];
if (!$arParams['VG_PLAYER_ID'])
    $arResult['VG_PLAYER_ID'] = "0";

$arResult['VG_VIDEO_PLAYLIST'] = $arParams["VG_VIDEO_PLAYLIST"];
if (!$arParams['VG_VIDEO_PLAYLIST'])
    $arResult['VG_VIDEO_PLAYLIST'] = "";

$arResult['VG_YOUTUBE_KEY'] = $arParams["VG_YOUTUBE_KEY"];
if (!$arParams['VG_YOUTUBE_KEY'])
    $arResult['VG_YOUTUBE_KEY'] = "";

if ($arResult['VG_VIDEO_PLAYLIST']){ // если переданы плейлисты

    $obCache = new CPHPCache();
    $cacheLifetime = 60*60*12;
    $cacheID = 'VG_PL_'.SITE_ID.$arResult['VG_PLAYER_ID'];
    $cachePath = '/'.$cacheID;

    if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
        $vars = $obCache->GetVars();
        $arResult['VG_VIDEO_VALUE'] = $vars[$cacheID];

    }
    elseif ($obCache->StartDataCache()) {

        foreach ($arResult['VG_VIDEO_PLAYLIST'] as $playlist_id){
            $arPlaylist[] = get_youtube_data_playlists($playlist_id,$arResult['VG_YOUTUBE_KEY']);
        }
        $arResult['VG_VIDEO_VALUE']=$arPlaylist;
        //echo "new_cache";
        $obCache->EndDataCache(array($cacheID => $arResult["VG_VIDEO_VALUE"]));
    }

}

$obCache = new CPHPCache();
$cacheLifetime = 60*60*12;
$cacheID = 'VG_VIDEO_'.SITE_ID.$arResult['VG_PLAYER_ID'];
$cachePath = '/'.$cacheID;

if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
    $vars = $obCache->GetVars();
    $arResult['VG_VIDEO_ITEMS'] = $vars[$cacheID];
}
elseif ($obCache->StartDataCache()) {
    foreach ($arResult['VG_VIDEO_VALUE'] as $key => $item){

        $pattern = '#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#';
        // #([\/|\?|&]vi?[\/|=]|youtu\.be\/|embed\/)(\w+)#

        if (is_array($item)) {
            foreach ($item as $sub_key => $sub_item) {
                preg_match($pattern, $sub_item, $matches);
                if ($matches[0])
                {
                    $video_item = $matches[0];
                }
                else
                {
                    if ($sub_item) $video_item = $sub_item;
                }
                if ($video_item){
                    $arResult['VG_VIDEO_ITEMS'][$sub_key] = array("ID"=>$video_item) + get_youtube_data($video_item, $arResult['VG_YOUTUBE_KEY']);
                }

            }
        }
        else
        {
            preg_match($pattern, $item, $matches);
            if ($matches[0])
            {
                $video_item = $matches[0];
            }
            else
            {
                if ($item) $video_item = $item;
            }

            if ($video_item) {
                $arResult['VG_VIDEO_ITEMS'][$key] = array("ID"=>$video_item) + get_youtube_data($video_item, $arResult['VG_YOUTUBE_KEY']);
            }
        }

    }

    usort($arResult['VG_VIDEO_ITEMS'], "sort_date");//сортировка по дате функция sort_date в functions

    $obCache->EndDataCache(array($cacheID => $arResult["VG_VIDEO_ITEMS"]));
}

$this->IncludeComponentTemplate();

?>