<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * Created by PhpStorm.
 * User: Andrey Senin
 * Date: 22.08.2017
 * Time: 11:43
 */

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "VG_VIDEO_ID" => array(
            "PARENT" => "BASE",
            "NAME" => "Видеоролики (id)",
            "TYPE" => "STRING",
            "DEFAULT" => '',
            "MULTIPLE" => "Y",
        ),
        "VG_AUTOPLAY" => array(
            "PARENT" => "BASE",
            "NAME" => "Автоматическое воспроизведение",
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "VG_DISABLE_PANEL_TIMEOUT" => array(
            "PARENT" => "BASE",
            "NAME" => "Время скрытого плейлиста при загрузке (мс)",
            "TYPE" => "STRING",
            "DEFAULT" => '2500',
        ),
        "VG_PLAYLIST_SKIN" => array(
            "PARENT" => "BASE",
            "NAME" => "Вид плейлиста",
            "TYPE" => "LIST",
            "MULTIPLE" => "N",
            "VALUES" => array(
                "right-thumb"=>"Справа: с изображениями",
                "left-thumb"=>"Слева: с изображениями",
                "right-no-thumb"=>"Справа: заголовок и описание",
                "right-title-only"=>"Справа: только заголовок",
                "bottom-text"=>"Снизу: заголовок и описание"
            ),
            "DEFAULT" => "right-thumb",
            "REFRESH" => "Y",
        ),
        "VG_YOUTUBE_KEY" => array(
            "PARENT" => "BASE",
            "NAME" => "YouTube KEY (Data API v3)",
            "TYPE" => "STRING",
            "DEFAULT" => '',
            "MULTIPLE" => "N",
        ),

    ),
);