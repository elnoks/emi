<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

global $DEPORTAMENT_MAIN, $DEPORTAMENT;

    $arResult["DEPORTAMENT_MAIN_STATUS"]  = $DEPORTAMENT_MAIN["UF_DEP_MAIN"];
    $arResult["DEPORTAMENT_STATUS"]  = $DEPORTAMENT["DEPORTAMENT"]["UF_DEP_MAIN"];

if (count($DEPORTAMENT_MAIN["UF_DEP_SOCIAL"]) && $arResult["DEPORTAMENT_MAIN_STATUS"] ) {
    foreach ($DEPORTAMENT_MAIN["UF_DEP_SOCIAL"] as $info) {

        $arSocKey = stristr($info, '/', true);
        $arSocUrl = stristr($info, '/');
        $arSocUrl = substr($arSocUrl, 1);

        $arResult["DEPORTAMENT_MAIN_SOCIAL"][$arSocKey][] = $arSocUrl;

    }
}

if (count($DEPORTAMENT["DEPORTAMENT"]["UF_DEP_SOCIAL"])) {

    foreach ($DEPORTAMENT["DEPORTAMENT"]["UF_DEP_SOCIAL"] as $info) {

        $arSocKey = stristr($info, '/', true);
        $arSocUrl = stristr($info, '/');
        $arSocUrl = substr($arSocUrl, 1);

        $arResult["DEPORTAMENT_SOCIAL"][$arSocKey][] = $arSocUrl;

    }
}


if ($this->StartResultCache(false, array(($arParams['CACHE_GROUPS'] === 'N' ? false : $USER->GetGroups()), $arResult, $bUSER_HAVE_ACCESS, $arNavigation))) {
    $this->SetResultCacheKeys(array(
        'SOCIAL_VK',
        'SOCIAL_FACEBOOK',
        'SOCIAL_TWITTER',
        'SOCIAL_INSTAGRAM',
        'SOCIAL_ODNOKLASSNIKI',
        'SOCIAL_GOOGLEPLUS',
        'SOCIAL_YOUTUBE',
        'SOCIAL_MAIL',
    ));

    $this->IncludeComponentTemplate();
}
?>
