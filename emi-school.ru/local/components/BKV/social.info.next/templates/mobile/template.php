<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?global $DEPORTAMENT_MAIN, $DEPORTAMENT,$USER;?>


<div class="social-icons">
    <!-- noindex -->
        <? if ($arResult["DEPORTAMENT_MAIN_SOCIAL"]) { ?>
            <ul>
                <? foreach ($arResult["DEPORTAMENT_MAIN_SOCIAL"] as $code => $infoSocMain) { ?>
                    <? foreach ($infoSocMain as $URL) { ?>

                        <?
                        switch ($code) {
                            case "SOCIAL_VK":
                                $class = "vk";
                                $classImg = "Vk";
                                break;
                            case "SOCIAL_FACEBOOK":
                                $class = "facebook";
                                $classImg = "Facebook";
                                break;
                            case "SOCIAL_TWITTER":
                                $class = "twitter";
                                $classImg = "Twitter";
                                break;
                            case "SOCIAL_INSTAGRAM":
                                $class = "instagram";
                                $classImg = "Instagram";
                                break;
                            case "SOCIAL_TELEGRAM":
                                $class = "telegram";
                                $classImg = "Telegram";
                                break;
                            case "SOCIAL_YOUTUBE":
                                $class = "ytb";
                                $classImg = "Youtube";
                                break;
                            case "SOCIAL_ODNOKLASSNIKI":
                                $class = "odn";
                                $classImg = "Odnoklassniki";
                                break;
                            case "SOCIAL_GOOGLEPLUS":
                                $class = "gplus";
                                $classImg = "Googleplus";
                                break;
                            case "SOCIAL_MAIL":
                                $class = "mail";
                                $classImg = "Mailru";
                                break;
                        }


                        ?>
                        <li class="<?= $class ?>">
                            <a href="<?= $URL ?>" class="dark-color" target="_blank" rel="nofollow"
                               title="<?=GetMessage('TEMPL_' . $code)?>">
                                <?=CNext::showIconSvg("fb", SITE_TEMPLATE_PATH."/images/svg/social/".$classImg.".svg");?>
                                <?=GetMessage('TEMPL_' . $code)?>
                            </a>
                        </li>
                    <? } ?>
                <? } ?>
            </ul>
        <? } ?>
    <!-- /noindex -->
</div>