<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>



<div class="social-icons">

    <? if ($arResult["DEPORTAMENT_MAIN_SOCIAL"]) { ?>
        <ul>
            <? foreach ($arResult["DEPORTAMENT_MAIN_SOCIAL"] as $code => $infoSocMain) { ?>
                <? foreach ($infoSocMain as $URL) { ?>

                    <?
                    switch ($code) {
                        case "SOCIAL_VK":
                            $class = "vk";
                            break;
                        case "SOCIAL_FACEBOOK":
                            $class = "facebook";
                            break;
                        case "SOCIAL_TWITTER":
                            $class = "twitter";
                            break;
                        case "SOCIAL_INSTAGRAM":
                            $class = "instagram";
                            break;
                        case "SOCIAL_TELEGRAM":
                            $class = "telegram";
                            break;
                        case "SOCIAL_YOUTUBE":
                            $class = "ytb";
                            break;
                        case "SOCIAL_ODNOKLASSNIKI":
                            $class = "odn";
                            break;
                        case "SOCIAL_GOOGLEPLUS":
                            $class = "gplus";
                            break;
                        case "SOCIAL_MAIL":
                            $class = "mail";
                            break;
                    }


                    ?>
                    <li class="<?= $class ?>">
                        <a href="<?= $URL ?>" target="_blank" rel="nofollow"
                           title="<?= GetMessage('TEMPL' . $code) ?>">
                            <?= GetMessage('TEMPL' . $code) ?>
                        </a>
                    </li>

                <? } ?>
            <? } ?>
        </ul>
    <? } ?>
    <!-- /noindex -->
</div>


<div class="social-icons">

    <!-- noindex -->


    <? if ($arResult["DEPORTAMENT_SOCIAL"] && $arResult["DEPORTAMENT_STATUS"] != 1) { ?>
        <ul>
            <? foreach ($arResult["DEPORTAMENT_SOCIAL"] as $code => $infoSocMain) { ?>
                <? foreach ($infoSocMain as $URL) { ?>

                    <?
                    switch ($code) {
                        case "SOCIAL_VK":
                            $class = "vk";
                            break;
                        case "SOCIAL_FACEBOOK":
                            $class = "facebook";
                            break;
                        case "SOCIAL_TWITTER":
                            $class = "twitter";
                            break;
                        case "SOCIAL_INSTAGRAM":
                            $class = "instagram";
                            break;
                        case "SOCIAL_TELEGRAM":
                            $class = "telegram";
                            break;
                        case "SOCIAL_YOUTUBE":
                            $class = "ytb";
                            break;
                        case "SOCIAL_ODNOKLASSNIKI":
                            $class = "odn";
                            break;
                        case "SOCIAL_GOOGLEPLUS":
                            $class = "gplus";
                            break;
                        case "SOCIAL_MAIL":
                            $class = "mail";
                            break;
                    }


                    ?>
                    <li class="<?= $class ?>">
                        <a href="<?= $URL ?>" target="_blank" rel="nofollow"
                           title="<?= GetMessage('TEMPL' . $code) ?>">
                            <?= GetMessage('TEMPL' . $code) ?>
                        </a>
                    </li>

                <? } ?>
            <? } ?>
        </ul>
    <? } ?>
    <!-- /noindex -->
</div>
