$(document).ready(function () {
    var blockConfirm = $(".confirm_phone");
    if (blockConfirm.length > 0) {
        $(document).on("click", "button[name='confirm_phone_btn']", function (event) {
            var thisBTN = $(this);

            var allPole = thisBTN.closest("form").serializeArray();
            var objSend = {};
            $.each(allPole, function (index, value) {
                objSend[value["name"]] = value["value"]
            });


            if (Number(objSend.confirm_code_phone) > 0 && objSend.confirm_code_phone) {
                $.ajax({
                    type: "POST",
                    url: template + "/ajax.php",
                    data: objSend,
                    dataType: "json",
                    success: function (msg) {
                        dellError();
                        if (msg.STATUS == "N") {
                            $("label").append('<label id="input-error" class="error" for="input_EMAIL">' + msg["ERROR"] + '</label>')
                            $("input[name='confirm_code_phone']").css({"border": "1px solid red"})
                        } else if (msg.STATUS == "Y") {


                            $("label").append('<label id="input-error" class="error succes" for="input_EMAIL">' + msg["ERROR"] + '</label>')
                            $("input[name='confirm_code_phone']").css({"border": "1px solid green"})

                            setTimeout("document.location.href='/personal/'", 2000);
                        }
                    }
                });
            } else {
                dellError();
                $("label").append('<label id="input-error" class="error" for="input_EMAIL">Ошибка, не указан код подтверждения</label>')
                $("input[name='confirm_code_phone']").css({"border": "1px solid red"})
            }
        })
    }


    $(document).on("click", "button[name='confirm_phone_new']", function (eve) {

        eve.preventDefault();

        var thisBTN = $(this);
        var flag = thisBTN.prop("value");
        var id_user = $("input[name='id_user']").prop("value");

        if (flag = "Y") {

            $(".block_phone_new").removeClass("show");
            $(".block_phone_new").addClass("hide");

            $.ajax({
                type: "POST",
                url: template + "/ajax.php",
                data: {
                    "confirm_phone_new": flag,
                    "id_user": id_user
                },
                dataType: "json",
                success: function (msg) {

                    if (msg.NEW == "Y") {
                        clearInterval(msg.TIME);
                        $("#timer").css({"display": "block"});
                        var remain_bv = msg.TIME
                        var intervalIDNew = setInterval(function () {
                            remain_bv = remain_bv - 1;
                            parseTime_bv(remain_bv);
                            if (remain_bv <= 0) {
                                $(".block_phone_new").addClass("show");
                                $(".block_phone_new").removeClass("hide");
                                $("#timer").css({"display": "none"});
                                clearInterval(intervalIDNew);
                            }
                        }, 1000);

                    }
                }
            });
        }
    })
});

function dellError() {
    $("#input-error").remove()
    $("input[name='confirm_code_phone']").css({"border": "1px solid #eeeeee"})
}

function parseTime_bv(timestamp) {
    if (timestamp < 0) timestamp = 0;

    var day = Math.floor((timestamp / 60 / 60) / 24);
    var hour = Math.floor(timestamp / 60 / 60);
    var mins = Math.floor((timestamp - hour * 60 * 60) / 60);
    var secs = Math.floor(timestamp - hour * 60 * 60 - mins * 60);
    var left_hour = Math.floor((timestamp - day * 24 * 60 * 60) / 60 / 60);

    $('span.afss_day_bv').text(day);
    $('span.afss_hours_bv').text(left_hour);

    if (String(mins).length > 1)
        $('span.afss_mins_bv').text(mins);
    else
        $('span.afss_mins_bv').text("0" + mins);
    if (String(secs).length > 1)
        $('span.afss_secs_bv').text(secs);
    else
        $('span.afss_secs_bv').text("0" + secs);

}

$(document).ready(function () {


    if ($("div.confirm_phone_btn").length > 0) {

        var intervalID = setInterval(function () {
            remain_bv = remain_bv - 1;
            parseTime_bv(remain_bv);
            if (remain_bv <= 0) {

                $(".block_phone_new").addClass("show");
                $(".block_phone_new").removeClass("hide");
                $("#timer").css({"display": "none"});

                var id_user = $("input[name='id_user']").prop("value");

                $.ajax({
                    type: "POST",
                    url: template + "/ajax.php",
                    data: {
                        "dell_pass": "Y",
                        "id_user": id_user
                    },
                    dataType: "json",
                });


                clearInterval(intervalID);
            }
        }, 1000);

    }


});

