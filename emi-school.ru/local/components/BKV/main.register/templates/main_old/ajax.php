<? require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php"); ?>

<?

use Bitrix\Main\Application;


$context = Application::getInstance()->getContext();
$request = $context->getRequest();

if ($request->isAjaxRequest()) {

    $postList = $request->getPostList()->toArray();


    if ($postList["dell_pass"] == "Y") {
        $postList["id_user"] = htmlspecialchars(unserialize(base64_decode($postList["id_user"])));

        $user = new CUser;
        $fields = Array(
            "UF_IWEB_SMS_PASSWORD" => "",
        );
        $save = $user->Update($postList["id_user"], $fields);


    } elseif ($postList["confirm_phone_new"] == "Y" && !empty($postList["id_user"])) {

        $postList["id_user"] = htmlspecialchars(unserialize(base64_decode($postList["id_user"])));
        $status = UserSms::passNew($postList["id_user"]);
        echo json_encode($status);

    } elseif (!empty($postList["id_user"]) && !empty($postList["confirm_code_phone"])) {

        $signer = new Bitrix\Main\Security\Sign\Signer;
        $ID = $signer->unsign($postList["id_user"], "main.register");
        $postList["id_user"] = htmlspecialchars(unserialize(base64_decode($ID)));
        $postList["confirm_code_phone"] = intval($postList["confirm_code_phone"]);
        $rsUser = CUser::GetByID($postList["id_user"]);
        $arUser = $rsUser->Fetch();


        if ($arUser["UF_IWEB_SMS_PASSWORD"] == $postList["confirm_code_phone"] && $arUser["ACTIVE"] != "Y") {
            $user = new CUser;
            $fields = Array(
                "ACTIVE" => "Y",
            );
            $save = $user->Update($postList["id_user"], $fields);

            if ($save) {
                global $USER;
                $USER->Authorize($postList["id_user"]);

                echo json_encode(array("ERROR" => "Успешная активация", "STATUS" => "Y"));

            } else {
                echo json_encode(array("ERROR" => "Ошибка, обратитесь к администратору", "STATUS" => "N"));
            }


        } else {
            echo json_encode(array("ERROR" => "Не верный код подтверждения или регистрация подтверждена", "STATUS" => "N"));
        }
    } else {
        echo json_encode(array("ERROR" => "Ошибка, не указан код подтверждения", "STATUS" => "N"));
    }
} else {
    echo json_encode(array("ERROR" => "Ошибка, обратитесь к администратору", "STATUS" => "N"));
}
?>


