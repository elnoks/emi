<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<div class="form-block border_block confirm_phone">
    <div class="wrap_md">
        <form id="confirm-page-form">
            <div class="form-control bg">
                <div class="main_info iblock">
                    <p><? echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT") ?></p>
                </div>
                <div class="wrap_md">
                    <div class="iblock label_block">
                        <label for="input_LOGIN"><?= GetMessage("REGISTER_CONFIRM_CODE") ?><span
                                    class="star">*</span></label>
                        <?


                        $signer = new Bitrix\Main\Security\Sign\Signer;
                        $signedIdUser = $signer->sign(base64_encode(serialize($arResult['VALUES']["USER_ID"])), 'main.register');
                        ?>
                        <input type="hidden" name="id_user" value="<?= $signedIdUser ?>">
                        <input size="10" id="input_LOGIN" type="text" name="confirm_code_phone" value=""
                               aria-required="true">
                        <div id="timer">
                            <span><?= GetMessage("REGISTER_CONFIRM_timer") ?></span>

                            <span class="afss_mins_bv">00</span>:
                            <span class="afss_secs_bv">00</span>
                        </div>
                    </div>

                    <div class="iblock text_block block_phone_new hide">
                        <button class="btn btn-default short" type="button" name="confirm_phone_new"
                                value="Y">
                            <?= GetMessage("REGISTER_CONFIRM_BTN_NEW") ?>
                        </button>
                    </div>

                </div>
                <div class="iblock text_block confirm_phone_btn">
                    <button class="btn btn-default short" type="button" name="confirm_phone_btn"
                            value="Y">
                        <?= GetMessage("REGISTER_CONFIRM_BTN") ?>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    var template =<?=CUtil::PhpToJSObject($this->GetFolder())?>;
    var remain_bv =<?=CUtil::PhpToJSObject($arResult['VALUES']["UF_SMS_TIME"])?>;
</script>