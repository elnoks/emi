<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


use Bitrix\Main,
    Bitrix\Main\Application;


$context = Application::getInstance()->getContext();
$request = $context->getRequest();


$arTmpField = $arFields = $arUFields = array();

$arTmpField = array_combine($arResult['SHOW_FIELDS'], $arResult['SHOW_FIELDS']);
unset($arTmpField["PASSWORD"]);
unset($arTmpField["CONFIRM_PASSWORD"]);


if ($arParams["SHOW_FIELDS"]) {
    foreach ($arParams["SHOW_FIELDS"] as $name) {
        $arFields[$arTmpField[$name]] = $name;
    }
} else {
    $arFields = $arTmpField;
}
$arFields["PASSWORD"] = "PASSWORD";
$arFields["CONFIRM_PASSWORD"] = "CONFIRM_PASSWORD";
if ($arTheme["PERSONAL_ONEFIO"]["VALUE"] == "Y") {
    if (isset($arFields["LAST_NAME"]))
        unset($arFields["LAST_NAME"]);
    if (isset($arFields["SECOND_NAME"]))
        unset($arFields["SECOND_NAME"]);
}

if (in_array($arParams["SHOW_FIELDS"], "LOGIN")) {
    unset($arFields["LOGIN"]);
}

$arResult["arFields"] = $arFields;

if ($request->isAjaxRequest() && !empty($arParams["GET_FORM"])) {


   if($request->getPost("setValue")){
       $arSaveForm = $request->getPost("setValue");
       unset($arSaveForm["EMAIL"],$arSaveForm["PASSWORD"],$arSaveForm["CONFIRM_PASSWORD"]);
       $arResult["VALUES"] =  $request->getPost("setValue");
       $_REQUEST["REGISTER"] =  $request->getPost("setValue");

   }


    $arFildsHTML = array();
    $keyBlock = "";
    foreach ($arFields as $FIELD) {

        $html = "";

        if ($FIELD == "UF_USER_TYPE") {
            continue;
        }


        if (($FIELD != "LOGIN" && $arTheme["LOGIN_EQUAL_EMAIL"]["VALUE"] == "Y") || $arTheme["LOGIN_EQUAL_EMAIL"]["VALUE"] != "Y") {
            $html .= '  <div class="form-control bg"><div class="wrap_md"><div class="iblock label_block">';

            $text = GetMessage("REGISTER_FIELD_" . $FIELD);
            if ($FIELD == "NAME") {
                if ($arTheme["PERSONAL_ONEFIO"]["VALUE"] == "Y")
                    $text = GetMessage("REGISTER_FIELD_FIO");
            }
            $html .= ' <label
                    for="input_' . $FIELD . '">' . $text;
            if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                $html .= '<span class="star">*</span>';
            }
            $html .= '</label>';
        }
        if (array_key_exists($FIELD, $arResult["ERRORS"])) {
            $class = 'class="error"';
        }
        $FIELD = strtoupper($FIELD);
        switch ($FIELD) {

            case "PASSWORD":

                $keyBlock = "OTHER";

                if (array_key_exists($FIELD, $arResult["ERRORS"])) {
                    $error = 'error';
                } else {
                    $error = '';
                }

                $html .= '<input 
                size="30" 
                type="password" 
                id="input_' . $FIELD . '" 
                name="REGISTER[' . $FIELD . ']" 
                required  autocomplete="off" 
                class="password ' . $error . '"/>';

                break;
            case "CONFIRM_PASSWORD":
                if (array_key_exists($FIELD, $arResult["ERRORS"])) {
                    $error = 'error';
                } else {
                    $error = '';
                }

                $keyBlock = "OTHER";
                $html .= ' <input 
                size="30" 
                type="password" 
                id="input_' . $FIELD . '"
                name="REGISTER[' . $FIELD . ']" 
                required
                autocomplete="off"
                class="confirm_password ' . $error . '"/>';


                break;
            case "PERSONAL_GENDER":
                $keyBlock = "OTHER";
                $html .= ' <select name="REGISTER[' . $FIELD . ']" id="input_' . $FIELD . '">
                    <option value="">' . GetMessage("USER_DONT_KNOW") . '</option>
                    <option
                            value="M"' . $arResult["VALUES"][$FIELD] == "M" ? " selected=\"selected\"" : "" . '>' . GetMessage("USER_MALE") . '</option>
                    <option
                            value="F"' . $arResult["VALUES"][$FIELD] == "F" ? " selected=\"selected\"" : "" . '>' . GetMessage("USER_FEMALE") . '</option>
                </select>';

                break;
            case "PERSONAL_COUNTRY":
                $keyBlock = "OTHER";
                $html .= '<select name="REGISTER[' . $FIELD . ']" id="input_' . $FIELD . '">';

                foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value) {
                    $html .= '<option value="' . $value . '"';
                    if ($value == $arResult["VALUES"][$FIELD]) {
                        $html .= 'selected="selected"';
                    }
                    $html .= ' >';
                    $html .= $arResult["COUNTRIES"]["reference"][$key] . '</option>';

                }
                $html .= '</select>';

                break;
            case "WORK_COUNTRY":
                $keyBlock = "WORK";

                $html .= '<select name="REGISTER[' . $FIELD . ']" id="input_' . $FIELD . '">';

                foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value) {
                    $html .= '<option value="' . $value . '"';
                    if ($value == $arResult["VALUES"][$FIELD]) {
                        $html .= 'selected="selected"';
                    }
                    $html .= ' >';
                    $html .= $arResult["COUNTRIES"]["reference"][$key] . '</option>';

                }
                $html .= '</select>';

                break;
            case "PERSONAL_PHOTO":
                $keyBlock = "OTHER";
                $html .= '<input size="30" type="file" id="input_' . $FIELD . '" name="REGISTER_FILES_' . $FIELD . '"/>';
                break;
            case "WORK_LOGO":
                $keyBlock = "WORK";
                $html .= '<input size="30" type="file" id="input_' . $FIELD . '" name="REGISTER_FILES_' . $FIELD . '"/>';
                break;
            case "PERSONAL_NOTES":
                $keyBlock = "OTHER";
                $html .= '<textarea cols="30" rows="5" id="input_' . $FIELD . '"
                          name="REGISTER[' . $FIELD . ']">' . $arResult["VALUES"][$FIELD] . '</textarea>';
            case "WORK_NOTES":
                $keyBlock = "WORK";
                $html .= '<textarea cols="30" rows="5" id="input_' . $FIELD . '"
                          name="REGISTER[' . $FIELD . ']">' . $arResult["VALUES"][$FIELD] . '</textarea>';
                break;
            case "PERSONAL_STREET":
                $keyBlock = "OTHER";
                $html .= '<textarea cols="30" rows="5" id="input_' . $FIELD . '"
                          name="REGISTER[' . $FIELD . ']">' . $arResult["VALUES"][$FIELD] . '</textarea>';
                break;
            case "EMAIL":
                $keyBlock = "OTHER";
                $html .= '<input size="30" type="email" id="input_' . $FIELD . '" 
                       name="REGISTER[' . $FIELD . ']" ' . ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y" ? "required" : "") . '
                       value="' . $arResult["VALUES"][$FIELD] . '" ' . $class . ' id="emails"/>';
                break;

                $keyBlock = "CONTACKT";
                $html .= '<input size="30" type="text" id="input_' . $FIELD . '"
                       name="REGISTER[' . $FIELD . ']" ' . ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y" ? "required" : "") . '
                       value="' . htmlspecialcharsbx($_REQUEST["REGISTER"]["NAME"]) . '" ' . $class . '/>';
                break;
            case "WORK_STATE":
            case "WORK_CITY":
            case "WORK_STREET":
            case "WORK_COMPANY":
                $keyBlock = "WORK";
                $html .= '<input size="30" type="text" id="input_' . $FIELD . '"
                       name="REGISTER[' . $FIELD . ']" ' . ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y" ? "required" : "") . '
                       value="' . htmlspecialcharsbx($_REQUEST["REGISTER"]["NAME"]) . '" ' . $class . '/>';
                break;
            case "LAST_NAME":
                $keyBlock = "CONTACKT";
                $html .= '<input size="30" type="text" id="input_' . $FIELD . '"
                       name="REGISTER[' . $FIELD . ']" ' . ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y" ? "required" : "") . '
                       value="' . htmlspecialcharsbx($_REQUEST["REGISTER"]["NAME"]) . '" ' . $class . '/>';
                break;
            case "PERSONAL_PHONE":
                $keyBlock = "CONTACKT";
                if (array_key_exists($FIELD, $arResult["ERRORS"])) {
                    $error = 'error';
                }
                $html .= ' <input size="30" type="tel" id="input_' . $FIELD . '" name="REGISTER[' . $FIELD . ']"
                       class="phone_input ' . $error . '" ' . ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y" ? "required" : "") . '
                       value="' . $arResult["VALUES"][$FIELD] . '"/>';

                break;
            default:

                if ($FIELD == "LOGIN" && $arTheme["LOGIN_EQUAL_EMAIL"]["VALUE"] == "Y") {
                    $type = 'type="hidden" value="1"';
                } else {
                    $type = 'type="text"';
                }
                $html .= '<input 
                                size="30"
                                id="input_' . $FIELD . '" ' . $type . '
                                name="REGISTER[' . $FIELD . ']" 
                                ' . ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y" ? "required" : "") . '
                                value="' . $arResult["VALUES"][$FIELD] . '"
                         />';
                if ($FIELD == "PERSONAL_BIRTHDAY") {
                    $html .= $APPLICATION->IncludeComponent(
                        'bitrix:main.calendar',
                        '',
                        array(
                            'SHOW_INPUT' => 'N',
                            'FORM_NAME' => 'regform',
                            'INPUT_NAME' => 'REGISTER[PERSONAL_BIRTHDAY]',
                            'SHOW_TIME' => 'N'
                        ),
                        null,
                        array("HIDE_ICONS" => "Y")
                    );
                }
                break;
        }
		
        if (($FIELD != "LOGIN" && $arTheme["LOGIN_EQUAL_EMAIL"]["VALUE"] == "Y") || $arTheme["LOGIN_EQUAL_EMAIL"]["VALUE"] != "Y") {
            if (array_key_exists($FIELD, $arResult["ERRORS"])) {
                $html .= ' <label class="error">' . GetMessage("REGISTER_FILL_IT") . '</label>';
            }
            $html .= '</div><div class="iblock text_block">';
            if ($arTheme["LOGIN_EQUAL_EMAIL"]["VALUE"] != "Y" && $FIELD == 'EMAIL') {
                $html .= GetMessage("REGISTER_FIELD_TEXT_" . $FIELD . '_SHORT');
            } else {
                $html .= GetMessage("REGISTER_FIELD_TEXT_" . $FIELD);
            }
            $html .= '</div></div></div>';
        }

        $arResult["VIVOD"][$keyBlock][$FIELD] = $html;


    }

}
