<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application;


$context = Application::getInstance()->getContext();
$request = $context->getRequest();

if ($request->isAjaxRequest()) {
    $postList = $request->getPostList()->toArray();

    $postList["arParamsReg"]["GET_FORM"] = $postList["type"];

    switch ($postList["type"]) {
        case "SALON":
            $filds = array(
                "WORK_COMPANY",
                "LAST_NAME",
                "NAME",
                "EMAIL",
                "PERSONAL_PHONE",
                "UF_USER_TYPE",
                "WORK_STATE",
                "UF_INN",
                "UF_KPP",
                "UF_OGRN",
                "WORK_CITY",
                "WORK_STREET"
            );

            $fildsReq = array(
                "LAST_NAME",
                "NAME",
                "EMAIL",
                "PERSONAL_PHONE",
                "UF_USER_TYPE",
            );
            break;
        case "CLIENT":
            $filds = array(
                "LAST_NAME",
                "NAME",
                "EMAIL",
                "PERSONAL_PHONE",
                "UF_USER_TYPE",
            );
			$fildsReq = array(
				"LAST_NAME",
				"NAME",
				"EMAIL",
				"PERSONAL_PHONE",
				"UF_USER_TYPE",
			);
		break;
        case "MASTER":
            $filds = array(
                "LAST_NAME",
                "NAME",
                "EMAIL",
                "PERSONAL_PHONE",
                "UF_CERT_NUM",
                "UF_USER_TYPE",
            );
			$fildsReq = array(
				"LAST_NAME",
				"NAME",
				"EMAIL",
				"PERSONAL_PHONE",
				"UF_CERT_NUM",
				"UF_USER_TYPE",
			);
		break;
    }

    $postList["arParamsReg"]["SHOW_FIELDS"] = $filds;
    $postList["arParamsReg"]["REQUIRED_FIELDS"] = $filds;

    $APPLICATION->IncludeComponent(
        "BKV:main.register_ur",
        "main_old",
        $postList["arParamsReg"]
    );

}