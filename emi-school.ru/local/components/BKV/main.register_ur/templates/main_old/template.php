<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?

use Bitrix\Main,
    Bitrix\Main\Application;

?>
<?
$context = Application::getInstance()->getContext();
$request = $context->getRequest();

if ($request->isAjaxRequest() && !empty($arParams["GET_FORM"])) {


    if (count($arResult["VIVOD"]["CONTACKT"])) {
        ?>
        <div><?= GetMessage("REGISTER_FIELD_TITLE_CONTACKT"); ?></div>
        <?
        foreach ($arResult["VIVOD"]["CONTACKT"] as $item) {
            echo $item;
        }
    }

    if (count($arResult["VIVOD"]["WORK"])) {
        ?>
        <div><?= GetMessage("REGISTER_FIELD_TITLE_WORK"); ?></div>
        <?
        foreach ($arResult["VIVOD"]["WORK"] as $item) {
            echo $item;
        }
    }

    if (count($arResult["VIVOD"]["OTHER"])) {
        ?>
        <div><?= GetMessage("REGISTER_FIELD_TITLE_OTHER"); ?></div>
        <?
        foreach ($arResult["VIVOD"]["OTHER"] as $item) {
            echo $item;
        }
    }

    $arResult["USE_CAPTCHA"] = "Y"; ?>

    <? if ($arResult["USE_CAPTCHA"] == "Y") { ?>
        <div class="form-control bg register-captcha captcha-row clearfix">
            <div class="iblock label_block">
                <label><span><?= GetMessage("REGISTER_CAPTCHA_PROMT") ?>&nbsp;<span
                                class="star">*</span></span></label>
                <div class="captcha_image">
                    <img
                            src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>"
                            border="0"/>
                    <input type="hidden" name="captcha_sid"
                           value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
                    <div class="captcha_reload"><?= GetMessage("RELOAD") ?></div>
                </div>
                <div class="captcha_input">
                    <input type="text" class="inputtext captcha" name="captcha_word" size="30"
                           maxlength="50" value="" required/>
                </div>
            </div>
            <div class="iblock text_block"></div>
        </div>
    <? } ?>
    <div class="but-r">


        <div class="wrap_md">
            <div class="iblock label_block">
                <div class="licence_block filter label_block">
                    <input type="checkbox"
                           id="licenses_register" <?= ($arTheme["SHOW_LICENCE"]["DEPENDENT_PARAMS"]["LICENCE_CHECKED"]["VALUE"] == "Y" ? "checked" : ""); ?>
                           name="licenses_register" required value="Y">
                    <label for="licenses_register">
                        <? $APPLICATION->IncludeFile(SITE_DIR . "include/licenses_text.php", Array(), Array("MODE" => "html", "NAME" => "LICENSES")); ?>
                    </label>
                </div>
            </div>
        </div>


        <button class="btn btn-default short" type="submit" name="register_submit_button1"
                value="<?= GetMessage("AUTH_REGISTER") ?>">
            <?= GetMessage("REGISTER_REGISTER") ?>
        </button>
        <div class="clearboth"></div>
    </div>

<? } else { ?>

    <div class="module-form-block-wr registraion-page">
        <? global $arTheme ?>

        <? if ($USER->IsAuthorized()) { ?>
            <p><? echo GetMessage("MAIN_REGISTER_AUTH") ?></p>
        <? } else { ?>
            <? if (count($arResult["ERRORS"]) > 0) {
                foreach ($arResult["ERRORS"] as $key => $error)
                    if (intval($key) == 0 && $key !== 0)
                        $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);
                ShowError(implode("<br />", $arResult["ERRORS"]));
            }?>
        <? } ?>

        <? if (empty(reset($arResult["ERRORS"])) && !empty($_POST["register_submit_button"]) && $arResult["USE_PHONE_CONFIRMATION"] == "N") {
            LocalRedirect(SITE_DIR . 'personal/');
        } elseif ((empty(reset($arResult["ERRORS"])) && !empty($_POST["register_submit_button"]) && $arResult["USE_PHONE_CONFIRMATION"] == "Y") || ($arResult["VALUES"]["USER_ID"] > 0 && $arResult["VALUES"]["USER_ID"] != "Y")) { ?>
            <? require_once($_SERVER["DOCUMENT_ROOT"] . $this->GetFolder() . "/confirmSms.php"); ?>
        <? } else { ?>

            <div class="form-block border_block">
                <div class="wrap_md">
                    <div class="main_info iblock">
                        <div class="top">
                            <? $APPLICATION->IncludeFile(SITE_DIR . "include/register_description.php", Array(), Array("MODE" => "html", "NAME" => GetMessage("REGISTER_INCLUDE_AREA"),)); ?>
                        </div>
                        <script>
                            $(document).ready(function () {

                            })
                        </script>

                        <form id="registraion-page-form" method="post" action="<?= POST_FORM_ACTION_URI ?>"
                              name="regform"
                              enctype="multipart/form-data">
                            <? if ($arResult["BACKURL"] <> ''): ?>
                                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
                            <? endif; ?>
                            <input type="hidden" name="register_submit_button" value="reg"/>


                            <?
                            if (in_array("UF_USER_TYPE", $arResult["arFields"])) {
                                ?>

                                <div class="form-control bg">
                                    <div class="wrap_md">
                                        <div class="iblock label_block">
                                            <label
                                                    for="input_UF_USER_TYPE"><?= GetMessage("REGISTER_FIELD_UF_USER_TYPE"); ?> <? if ($arResult["REQUIRED_FIELDS_FLAGS"]["UF_USER_TYPE"] == "Y"): ?>
                                                    <span class="star">*</span><? endif; ?>
                                            </label>
                                            <div class="btn-group " data-toggle="buttons" id="input_UF_USER_TYPE">
                                                <label class="btn btn-primary <?=$arResult["VALUES"]["UF_USER_TYPE"] == "SALON" ? " active" : "" ?>">
                                                    <input type="radio" name="REGISTER[UF_USER_TYPE]" id="option3"  value="SALON" autocomplete="off"  <?= $arResult["VALUES"]["UF_USER_TYPE"] == "SALON" ? " checked=\"checked\"" : "" ?>> <?= GetMessage("UF_USER_TYPE_SALON") ?>
                                                </label>
                                                <label class="btn btn-primary <?=$arResult["VALUES"]["UF_USER_TYPE"] == "MASTER" ? " active" : "" ?>">
                                                    <input type="radio" name="REGISTER[UF_USER_TYPE]" id="option2" value="MASTER" autocomplete="off" <?= $arResult["VALUES"]["UF_USER_TYPE"] == "MASTER" ? " checked=\"checked\"" : "" ?>> <?= GetMessage("UF_USER_TYPE_MASTER") ?>
                                                </label>
                                                <label class="btn btn-primary <?=$arResult["VALUES"]["UF_USER_TYPE"] == "CLIENT" ? " active" : "" ?>">
                                                    <input type="radio" name="REGISTER[UF_USER_TYPE]" id="option1" value="CLIENT" autocomplete="off" <?= $arResult["VALUES"]["UF_USER_TYPE"] == "CLIENT" ? " checked=\"checked\"" : "" ?> > <?= GetMessage("UF_USER_TYPE_CLIENT") ?>
                                                </label>

                                            </div>

                                        </div>
                                        <div class="iblock text_block">
                                            <?= GetMessage("UF_USER_TYPE_SALON_TEXT") ?>
                                        </div>
                                    </div>
                                </div>
                            <? } ?>


                        </form>
                    </div>
                    <div class="social_block iblock">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:system.auth.form",
                            "popup",
                            array(
                                "TITLE" => "Авторизация через соц сети",
                                "PROFILE_URL" => $arParams["PATH_TO_PERSONAL"],
                                "SHOW_ERRORS" => "Y",
                                "POPUP_AUTH" => "Y"
                            )
                        ); ?>
                    </div>
                </div>
            </div>
        <? } ?>
    </div>


<? } ?>

<script>
    var arParamsReg = <?=CUtil::PhpToJSObject($arParams)?>;
    var folderTemplateReg = <?=CUtil::PhpToJSObject($this->GetFolder())?>;
    var setValues = <?=CUtil::PhpToJSObject($arResult["VALUES"])?>;
</script>


