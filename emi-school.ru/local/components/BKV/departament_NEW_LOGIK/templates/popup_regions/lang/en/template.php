<?
$MESS["CITY_TITLE"] = "Your city";
$MESS["CITY_PLACEHOLDER"] = "Enter the name of the city";
$MESS["EXAMPLE_CITY"] = "For example:";
$MESS["OKRUG"] = "District";
$MESS["REGION"] = "Region, republic, territory";
$MESS["KRAY"] = "Area";
$MESS["CITY"] = "City";
$MESS["CITY_YES"] = "Yes that's right";
$MESS["CITY_CHANGE"] = "Choose another";
?>