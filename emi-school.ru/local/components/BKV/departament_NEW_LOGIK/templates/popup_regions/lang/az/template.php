<?
$MESS["PRE_CITY_REGION"] = "Перед оплатой уточните наличие товара в регионе #REGION#";
$MESS["CITY_REGION"] = "Обратите внимание, что данный заказ возможен только в текущем местоположении: #CITY#";
$MESS["PRE_CHANGE_CITY"] = "изменить город";
$MESS["CITY_TITLE"] = "Ваш город";
$MESS["CITY_PLACEHOLDER"] = "Введите название города";
$MESS["EXAMPLE_CITY"] = "Например:";
$MESS["OKRUG"] = "Округ";
$MESS["REGION"] = "Область, республика, край";
$MESS["KRAY"] = "Район";
$MESS["CITY"] = "Город";
$MESS["CITY_YES"] = "Да, все верно";
$MESS["CITY_CHANGE"] = "Выбрать другой";
?>