<?php
header('Access-Control-Allow-Origin: *');

define('STOP_STATISTICS', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC','Y');
define('BX_SKIP_POST_UNQUOTE', true);
define('NO_AGENT_CHECK', true);

require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Application,
    Bitrix\Sale;

CModule::IncludeModule("sale");
global $AJAXDEP;
$Instance = Application::getInstance();
$context = $Instance->getContext();
$request = $context->getRequest();


if ($request->isAjaxRequest()) {
    $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
    $basketSmol = base64_encode(gzcompress(serialize($basket)));
    CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());

    echo $basketSmol;
}