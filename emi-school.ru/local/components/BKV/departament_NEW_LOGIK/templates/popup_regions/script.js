BX.ready(function () {
    $('.js_city_change').on('click', function () {
        var _this = $(this);
        _this.closest('.region_wrapper').find('.js_city_chooser').trigger('click');
        _this.closest('.confirm_region').remove();
    });
    $('.js_city_chooser').on('click', function () {
        var _this = $(this);
        _this.closest('.region_wrapper').find('.confirm_region').remove();
    });


    $(document).on("submit", "form.location-go", function (eve) {
        var $this = $(this);

        var BS = $this.find("input[name='BS']").length;
        if (BS < 1) {
            eve.preventDefault();
            $.ajax({
                type: "POST",
                url: dirDEP + "/ajax.php",
                dataType: "text",
                success: function (msg) {
                    $this.append('<input type="hidden" value="' + msg + '" name="BS">')
                    $this.find("button").click();
                }
            });

        }
    })
});