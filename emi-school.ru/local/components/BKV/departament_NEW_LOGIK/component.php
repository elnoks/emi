<?

use  Bitrix\Main\Loader,
     Bitrix\Main\Application,
     Bitrix\Main\Context,
     Bitrix\Main\Server,
     Bitrix\Main\Request,
     Bitrix\Main\Page\Asset;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

Loader::includeModule('highloadblock');
Loader::includeModule("sale");
Loader::includeModule("catalog");

$context = Context::getCurrent();
$server = $context->getServer();
$request = $context->getRequest();
$http_request = $server->get('HTTP_X_REQUESTED_WITH');


require_once(__DIR__ . "/class.php");
$arParams['POPUP'] = (isset($arParams['POPUP']) ? $arParams['POPUP'] : 'N');
$arResult['POPUP'] = ((isset($http_request) && $http_request == 'XMLHttpRequest') || $arParams['POPUP'] == 'Y');
$prot = $arResult['HOST'] = $server->get('REQUEST_SCHEME') . "://";
$dir = $arResult['URI'] = $request->getRequestedPageDirectory();


global $DEPORTAMENT, $DEPORTAMENT_MAIN;

$DEP = new Deportament($arResult, $arParams, $DEPORTAMENT["ID"], $dir);
$item = $DEP->getSessionLocatio();

if ($item) {
    $lang = LANGUAGE_ID;
    if ($lang=="by"||$lang=="az") $lang="ru";
    $DEP->getLocation( // города и деревни
        array('=NAME.LANGUAGE_ID' => $lang, "TYPE_CODE" => array("CITY", "VILLAGE"), "ID" => $item["ID"]),
        array('ID', "CITY_ID", "REGION_ID", "COUNTRY_ID", "PARENT_ID", 'NAME_RU' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE'),
        $item
    );

    $DEP->getDepartament($item);

    $DEP->getLocation( // районы
        array('=NAME.LANGUAGE_ID' => $lang, "TYPE_CODE" => "SUBREGION", "PARENT_ID" => $item["REGION_ID"]),
        array('ID', "CITY_ID", "REGION_ID", "COUNTRY_ID", "PARENT_ID", 'NAME_RU' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE'),
        $item
    );

    $DEP->sopostavlenie($item);

    $arResult = $DEP->getArResult();


    if (!($regions_db = $GLOBALS["memcache"]->get('regions_db_' . LANGUAGE_ID))) {
        if ($USER->IsAdmin()) {
            echo "regions_db memcached";
        }
        // -----------
        $regions_db = $DEP->getLocation( // области - в мемкеш
            array('=NAME.LANGUAGE_ID' => $lang, "TYPE_CODE" => "REGION", "PARENT_ID" => $item["COUNTRY_ID"]),
            array('ID', 'NAME_RU' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE'),
            $item
        );
        // -----------
        $GLOBALS["memcache"]->set('regions_db_' . LANGUAGE_ID, $regions_db, false, 60 * 60 * 24);    // every 24 hour
    } else {
        $arResult["REGION"] = $regions_db;
    }



    $arResult['REGIONS'] = $arResult['CITY'];
    if ($arResult['POPUP'] && !empty($arResult['REGIONS'])) {
        if ($arResult['REGION']) {
            $arResult['SECTION_LEVEL1'] = $arResult['REGION'];
        }

        if ($arResult['SUBREGION']) {
            $arResult['SECTION_LEVEL2'] = $arResult['SUBREGION'];
        }

        foreach ($arResult['REGIONS'] as $id => $arRegionItem) {
            if ($arRegionItem["PARENT_ID"] == $arRegionItem ["REGION_ID"]) {
                $PARENT_ID = $arRegionItem["PARENT_ID"];
            } else {
                $PARENT_ID = $arRegionItem["REGION_ID"];
            }
            $arResult['JS_REGIONS'][] = array(
                'label' => $arRegionItem['NAME'],
                'HREF' => $arRegionItem["DEPORTAMENT"]['URL'],
                'REGION' => (($arSectionsName && ($PARENT_ID && isset($arSectionsName[$PARENT_ID]))) ? $arSectionsName[$PARENT_ID] : ''),
                'ID' => $arRegionItem['ID'],
            );
        }
    }
}

$this->IncludeComponentTemplate();
