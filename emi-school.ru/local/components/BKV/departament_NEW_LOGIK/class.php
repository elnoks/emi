<?

class Deportament
{
    var $hlblock_id;
    var $HL_Infoblock_ID;
    var $arResult;
    var $arParams;
    var $arHL;
    var $MASK;

    function __construct($arResult = "", $arParams = "",$idDep,  $this_dir = "")
    {
        $this->hlblock_id = HL_DEPORTAMENT;
        $this->LANG = LANGUAGE_ID;
        $this->arParams = $arParams;
        $this->arResult = $arResult;
        if (!empty($this_dir)) {
            $_SESSION["DEP_URL_AJAX"] = $this_dir;
        }
        $this->idDep = $idDep;


    }


    function getSessionLocatio()
    {
        global $DEPORTAMENT;

        $item["ID"] = $DEPORTAMENT["ID"];
        $item["CITY_ID"] = $DEPORTAMENT["CITY_ID"];
        $item["COUNTRY_ID"] = $DEPORTAMENT["COUNTRY_ID"];
        $item["REGION_ID"] = $DEPORTAMENT["REGION_ID"];
        $item["PARENT_ID"] = $DEPORTAMENT["PARENT_ID"];
        $item["NAME_LANG"] = $DEPORTAMENT["NAME_LANG"];
        $item["TYPE_CODE"] = $DEPORTAMENT["TYPE_CODE"];

        return $item;

    }

    function getLocation($filter = array(), $select = array(), $item = array(), $ses = "", $order = array("NAME_RU" => "asc"))
    {
        $filter = \Bitrix\Sale\Location\LocationTable::getList(array(
            'filter' => $filter,
            'select' => $select,
            'order' => $order
        ))->fetchAll();

        foreach ($filter as $key => $info) {

            if ($ses == "Y") {
                return $info;
            }

            if ($info["TYPE_CODE"] == "CITY" || $info["TYPE_CODE"] == "VILLAGE") {

                if ($item["ID"] == $info["ID"]) {
                    $this->arResult['CURRENT_REGION'] = $item;
                }
                $info["TYPE_CODE"] = "CITY";
            }
            if ($info["TYPE_CODE"] == "SUBREGION") {
                $locationArr[$info["PARENT_ID"]][$info["ID"]]['ID'] = $info["ID"];
                $locationArr[$info["PARENT_ID"]][$info["ID"]]['NAME'] = $info["NAME_RU"];
            } else {
                $locationArr[$info["ID"]]["ID"] = $info["ID"];
                $locationArr[$info["ID"]]["TYPE_CODE"] = $info["TYPE_CODE"];
                $locationArr[$info["ID"]]["NAME"] = $info["NAME_RU"];
                $locationArr[$info["ID"]]["REGION_ID"] = $info["REGION_ID"];
                $locationArr[$info["ID"]]["COUNTRY_ID"] = $info["COUNTRY_ID"];
                $locationArr[$info["ID"]]["PARENT_ID"] = $info["PARENT_ID"];
            }

            if ($info["TYPE_CODE"] != "REGION") { // если Страна
                $this->arResult[$info["TYPE_CODE"]] = $locationArr;
            }
        }

        if ($locationArr) {
            return $locationArr;
        }
    }

    function getDepartament($item = "")
    {
        $arFilter = array("UF_DEP_LOCATION" => '%"COUNTRY":"' . $item["COUNTRY_ID"] . '"%');

        $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($this->hlblock_id)->fetch();
        $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $rsData = $entity_data_class::getList(array(
            "select" => array('ID',"UF_DEP_LOCATION","UF_DEP_MAIN","UF_DEP_SITE","UF_DEP_TYPE_PRICE"),
            "filter" => $arFilter,
            "order" => array()
        )); 
        while ($arRes = $rsData->Fetch()) {

            if ($arRes["UF_DEP_MAIN"]) {
                $_SESSION["DEPORTAMENT_MAIN"] = $arRes["ID"];
            }

            $formatUF_DEP_LOCATION = json_decode($arRes["UF_DEP_LOCATION"], true);
            $UF_DEP_LOCATION = array_diff($formatUF_DEP_LOCATION, array("", " "));
            $rsSites = Bitrix\Main\SiteTable::getById($arRes["UF_DEP_SITE"]);
            $arSite = $rsSites->Fetch();
            $arRes["URL"] = $arSite["SERVER_NAME"];

            $this->arHL[$arRes["ID"]] = $arRes;
            if (count($UF_DEP_LOCATION)) {
                foreach ($UF_DEP_LOCATION as $vaKey => $arInfo) {

                    if (isset($arInfo["COUNTRY"]) && !empty($arInfo["COUNTRY"])) {
                        if (isset($arInfo["REGION"]) && !empty($arInfo["REGION"])) {

                            if (isset($arInfo["SUBREGION"]) && !empty($arInfo["SUBREGION"])) {
                                if (isset($arInfo["CITY"]) && !empty($arInfo["CITY"])) {
                                    $this->MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["SUBREGION"]][$arInfo["CITY"]]["CITY"] = $arRes["ID"];
                                } else {
                                    $this->MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["SUBREGION"]]["SUBREGION"] = $arRes["ID"];
                                }
                            } else {
                                if (isset($arInfo["CITY"]) && !empty($arInfo["CITY"])) {
                                    $this->MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["REGION"]][$arInfo["CITY"]]["CITY"] = $arRes["ID"];
                                } else {
                                    $this->MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]]["REGION"] = $arRes["ID"];
                                }
                            }
                        } else {
                            $this->MASK[$arInfo["COUNTRY"]]["COUNTRY"] = $arRes["ID"];
                        }
                    }
                }
            }
        }
    }

    function sopostavlenie($item = "")
    {

        if ($this->idDep > 0 && isset($this->idDep)) {
            $idHL = $this->arHL[$this->idDep];
        } else {
            if ($this->MASK[$item["COUNTRY_ID"]][$item["REGION_ID"]][$item["PARENT_ID"]][$item["ID"]]["CITY"]) {
                $idHL = $this->arHL[$this->MASK[$item["COUNTRY_ID"]][$item["REGION_ID"]][$item["PARENT_ID"]][$item["ID"]]["CITY"]];
            } elseif ($this->MASK[$item["COUNTRY_ID"]][$item["REGION_ID"]][$item["PARENT_ID"]]["SUBREGION"]) {
                $idHL = $this->arHL[$this->MASK[$item["COUNTRY_ID"]][$item["REGION_ID"]][$item["PARENT_ID"]]["SUBREGION"]];
            } elseif ($this->MASK[$item["COUNTRY_ID"]][$item["REGION_ID"]]["REGION"]) {
                $idHL = $this->arHL[$this->MASK[$item["COUNTRY_ID"]][$item["REGION_ID"]]["REGION"]];
            } elseif ($this->MASK[$item["COUNTRY_ID"]]["COUNTRY"]) {
                $idHL = $this->arHL[$this->MASK[$item["COUNTRY_ID"]]["COUNTRY"]];
            }
        }

        $this->arResult['CITY'][$item["ID"]]["DEPORTAMENT"] = $idHL;


        if ($item["ID"] == $this->arResult['CURRENT_REGION']["ID"]) {
            $this->arResult['CURRENT_REGION'] = $item;
            $this->arResult['CURRENT_REGION']["DEPORTAMENT"] = $idHL;
            $_SESSION["DEPORTAMENT"] = $idHL;
        }
    }


    function getArResult()
    {
        return $this->arResult;
    }


}