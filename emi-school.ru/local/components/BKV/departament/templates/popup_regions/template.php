<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */

$this->setFrameMode(true);

use \Bitrix\Main\Localization\Loc;
?>
<? if (!$arResult['POPUP']): ?>

    <?
    if ($arResult['CURRENT_REGION']):
        if (!empty($arResult['CURRENT_REGION']["NAME_LANG"])) {
            foreach($_SESSION["DEPORTAMENT"]["UF_DEP_PHONES"] as $item){
                $pos = strpos($item, "8 (800)");
                if ($pos === false) {
                    $arr[] = $item;
                }
            }
            $str =  implode(", ", $arr);
            ?>
            <script>
			BX.ready(function () {
					if (BX("CITY") != null) {
                        let obSelect;

                        obSelect = BX.findChild(BX("CITY"),{ "tag" : "input"});
                        obSelect.setAttribute("id", obSelect.getAttribute("id"));
                        obSelect.value = '<?=$arResult["CURRENT_REGION"]["NAME_LANG"]?>';
                        //obSelect.setAttribute("placeholder", "<?//=$arResult['CURRENT_REGION']["NAME_LANG"]?>//");
                        obSelect.setAttribute("readonly", "true");

						let insert = BX.create('div', {
							props: {
								className: 'tooltip-inner-region'
							},
							text: "<?= Loc::getMessage('CITY_REGION', array('#CITY#' => $arResult['CURRENT_REGION']["NAME_LANG"])); ?>",
						});
						BX.insertAfter(insert, obSelect);
						
						obSelect = BX.findPreviousSibling(BX("CITY"), {
							"tag" : "label",
							"class" : "bx-soa-custom-label",
						} );

						changeInsert = BX.create('div', {
							dataset: {
								"event": "jqm",
								"name": "city_chooser",
							},
							attrs:{
								"data-param-form_id" : "city_chooser",
							},
							style: {
								"display" : "inline",
								"margin-left" : "6px",
							},
							children: [
								BX.create('a', {
									props: {
										className: "tooltip-inner-region"								
									},
									style: {color: '#9a9c9a'},
									text: "<?= Loc::getMessage('PRE_CHANGE_CITY')?>",
									}
								)
							]
						});
						
						BX.insertAfter(changeInsert, obSelect);
					}
					if (BX("prepay-message") != null) {
						let preInsert = BX.create('p', {
							props: {
								className: 'tooltip-inner-region'
							},
							text: "<?= Loc::getMessage('PRE_CITY_REGION', array('#REGION#' => $arResult['REGION'][$arResult['CURRENT_REGION']["REGION_ID"]]["NAME"])); ?>. <?=$str?>",
						});
						
						BX.insertAfter(preInsert, BX("prepay-message"));
					}
				}
			);
            </script>
            <?
        }
        ?>
        <? global $arTheme; ?>
        <div class="region_wrapper">
            <div class="js_city_chooser " data-event="jqm" data-name="city_chooser"
                 data-param-url="<?= urlencode($APPLICATION->GetCurUri()); ?>" data-param-form_id="city_chooser">
                <span>
                    <a class="dark-color colored"
                       href="javascript:void(0)"><?= CNext::showIconSvg("search big", SITE_TEMPLATE_PATH . "/images/svg/map_marker.svg"); ?>
                        <?= $arResult['CURRENT_REGION']['NAME_LANG']; ?></a>
                </span><span class="arrow"><i></i></span>
            </div>
            <? if ($arResult['SHOW_REGION_CONFIRM']): ?>
                <div class="confirm_region">
                    <?
                    $href = 'data-href="' . $arResult['REGIONS'][$arResult['REAL_REGION']['ID']]['URL'] . '"';
                    if ($arTheme['USE_REGIONALITY']['DEPENDENT_PARAMS']['REGIONALITY_TYPE']['VALUE'] == 'SUBDOMAIN' && ($arResult['HOST'] . $_SERVER['HTTP_HOST'] . $arResult['URI'] == $arResult['REGIONS'][$arResult['REAL_REGION']['ID']]['URL']))
                        $href = ''; ?>
                    <div class="title"><?= Loc::getMessage('CITY_TITLE'); ?> <?= $arResult['REAL_REGION']['NAME']; ?>
                        ?
                    </div>
                    <div class="buttons">
                        <span class="btn btn-default aprove"
                              data-id="<?= $arResult['REAL_REGION']['ID']; ?>" <?= $href; ?>><?= Loc::getMessage('CITY_YES'); ?></span>
                        <span class="btn btn-default white js_city_change"><?= Loc::getMessage('CITY_CHANGE'); ?></span>
                    </div>
                </div>
            <? endif; ?>
        </div>
    <? endif; ?>
<? else: ?>
    <div class="popup_regions">
        <div class="h-search autocomplete-block" id="title-search-city">
            <div class="wrapper">
                <input id="search" class="autocomplete text" type="text"
                       placeholder="<?= Loc::getMessage('CITY_PLACEHOLDER'); ?>">
                <div class="search_btn"></div>
            </div>

        </div>
        <div class="items ext_view">
            <? if ($arResult['SECTION_LEVEL1']): ?>
                <div class="block regions old">
                    <div
                            class="title"><?= Loc::getMessage('REGION') ?></div>
                    <div class="items_block">
                        <? foreach ($arResult['SECTION_LEVEL1'] as $key => $arSection): ?>
                            <div class="item dark_link" data-id="<?= $key; ?>">
                                <span><?= $arSection['NAME']; ?></span></div>
                        <? endforeach; ?>
                    </div>
                </div>
            <? endif; ?>
            <? if ($arResult['SECTION_LEVEL2']): ?>
                <div class="block regions subregion">
                    <div class="title"><?= Loc::getMessage('KRAY') ?></div>
                    <div class="items_block">
                        <? foreach ($arResult['SECTION_LEVEL2'] as $key => $arSections): ?>
                            <div class="parent_block current shown" data-id="<?= $key; ?>">
                                <? foreach ($arSections as $key2 => $arSection): ?>
                                    <div class="item dark_link" data-id="<?= $key2; ?>">
                                        <span><?= $arSection['NAME']; ?></span></div>
                                <? endforeach; ?>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            <? endif; ?>
            <? if ($arResult['REGIONS']): ?>
                <div class="block cities">
                    <div class="title"><?= Loc::getMessage('CITY'); ?></div>
                    <div class="items_block">
                        <?
                        foreach ($arResult['REGIONS'] as $key => $arItem): ?>

                            <? $bCurrent = ($arResult['CURRENT_REGION']['ID'] == $arItem['ID']); ?>
                            <?
                            if ($arItem["PARENT_ID"] == $arItem ["REGION_ID"]) {
                                $arItem['IBLOCK_SECTION_ID'] = $arItem["PARENT_ID"];
                            } else {
                                $arItem['IBLOCK_SECTION_ID'] = $arItem["REGION_ID"];
                            }
                            ?>

                            <div
                                    class="item <?= ($bCurrent ? 'current shown' : ''); ?> <?= ((!$arResult['SECTION_LEVEL1'] && !$arResult['SECTION_LEVEL2']) ? 'shown' : ''); ?>"
                                    data-id="<?= $arItem["PARENT_ID"] ?>"
                                    data-id_parent="<?= $arItem["REGION_ID"] ?>"
                            >

                                <form class="location-go" data-id="<?= $arItem['ID']; ?>"
                                      action="<?= $arResult['HOST'] ?><?= $arItem["DEPORTAMENT"]['URL']; ?>"
                                      method="POST">
                                    <input type="hidden" name="LOC_ID" value="<?= $arItem['ID']; ?>">
                                    <button class="name <? if ($bCurrent): ?>select <? endif; ?>"
                                            type="submit"><?= $arItem['NAME']; ?></button>
                                </form>

                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            <? endif; ?>
        </div>
    </div>
<? endif; ?>

<?
$this->addExternalJs($this->GetFolder() . "/js/jquery-ui.js");
?>
<?
$backUrl = $arParams["DEP_URL_AJAX"];
if (empty($arParams["DEP_URL_AJAX"])) {
    $dir = $APPLICATION->GetCurDir();
    $arDir = explode("/", $dir);
    $arDir = array_diff($arDir, array("", " "));
    $backUrl = empty(reset($arDir)) ? "/" : "/" . reset($arDir) . "/";
}
?>
<script>
    var dirDEP = "<?= $this->GetFolder()?>";
    var url_dir_aja = "<?= $backUrl?>";
    var url_ajax_this = "<?= $backUrl?>";
</script>
