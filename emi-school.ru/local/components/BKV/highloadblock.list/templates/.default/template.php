<?
global $USER, $DEPORTAMENT;
if (!empty($arResult['ERROR'])) {
    echo $arResult['ERROR'];
    return false;
}
?>



    <div class="item-views-wrapper <?= $templateName; ?>">



        <? if ($arParams["MAP"] == "Y" && count($arResult['rows'])) { ?>
            <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
            <div id="YMapsID" style="width: 100%; height: 450px;"></div>
        <? } ?>

        <?if(count($arResult['rows'])){?>
        <div class="maxwidth-theme">
            <div class="row">
                <div class="col-md-12">
                    <h4><?= GetMessage("TITLE_LIST")?></h4>
                </div>
                    <table class="contacts-stores no-border shops list">


                        <? foreach ($arResult['rows'] as $i => $arItem): ?>
                            <?if($arItem["UF_DEP_MAIN"]=="нет"): ?>

                                <tr class="item" id="<?= $this->GetEditAreaId($arItem['ID']) ?>">

                                    <td class="hidden-xs">
                                        <div class="title">
                                            <?= $arItem["UF_DEP_NAME"]; ?>
                                        </div>
                                    </td>
                                    <td class=" hidden-xs">
                                        <? if ($arItem['UF_DEP_ADDRESS']!="") { ?>
                                            <div class="muted">
                                                <span class="icon-text schedule grey s25"><span
                                                        class="text"><?=$arItem['UF_DEP_ADDRESS'] ?></span></span>
                                            </div>
                                        <? } ?>
                                        <? if ($arItem['UF_DEP_WORK_TIME']): ?>
                                            <div class="muted">
                                                <span class="icon-text schedule grey s25">
                                                    <i class="fa fa-clock-o"></i>
                                                    <span class="text"><?= $arItem["UF_DEP_WORK_TIME"] ?></span>
                                                </span>
                                            </div>
                                        <? endif; ?>
                                    </td>
                                    <td class="phone hidden-xs">
                                        <? if ($arItem['UF_DEP_PHONES']) {
                                            $arPhone = explode(",", $arItem['UF_DEP_PHONES']);
                                            foreach ($arPhone as $phone):
                                                if($phone!="8 (800) 550-86-95"):
                                                ?>

                                                <a href="tel:+<?= str_replace(array(' ', ',', '-', '+', '(', ')'), '', $phone); ?>"
                                                   class="black"><?= $phone; ?></a>
                                            <?  endif;
                                            endforeach;
                                        } ?>
                                    </td>
                                    <td class="hidden-xs">

                                        <? if ($arItem['UF_DEP_EMAIL']) {
                                            $arEmail = explode(",", $arItem['UF_DEP_EMAIL']);
                                            foreach ($arEmail as $Email): ?>

                                                <div class="muted">
                                                <span class="icon-text schedule grey s25">
                                                    <i class="fa fa-envelope"></i>
                                                    <span class="text">
                                                        <a href="<?= $Email; ?>"
                                                           class="black"><?= $Email; ?>
                                                        </a>
                                                    </span>
                                                </span>
                                                </div>


                                            <? endforeach;
                                        } ?>
                                    </td>
                                    <td class="visible-xs mobile-title-phone" colspan=3>
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <div class="titles-block">

                                                    <span class="title">
                                                        <?= $arItem["UF_DEP_NAME"]; ?>
                                                    </span>

                                                </div>
                                            </div>


                                            <div class="col-sm-6 col-xs-12">

                                                <? if ($arItem['UF_DEP_WORK_TIME']): ?>

                                                    <span class="icon-text schedule grey s25"><i
                                                            class="fa fa-clock-o"></i> <span
                                                            class="text"><?= $arItem["UF_DEP_WORK_TIME"] ?></span></span>

                                                <? endif; ?>

                                            </div>


                                            <div class="col-sm-6 col-xs-12">

                                                <? if ($arItem['UF_DEP_ADDRESS']) { ?>

                                                    <div class="muted">
                                                        <span class="icon-text schedule grey s25"<span
                                                            class="text"><?= $arItem['UF_DEP_ADDRESS'] ?></span></span>
                                                    </div>
                                                <? } ?>

                                            </div>
                                            <div class="col-sm-6 col-xs-12">

                                                <? if ($arItem['UF_DEP_PHONES']) {
                                                    $arPhone = explode(",", $arItem['UF_DEP_PHONES']);
                                                    foreach ($arPhone as $phone): ?>
                                                        <a href="tel:+<?= str_replace(array(' ', ',', '-', '+', '(', ')'), '', $phone); ?>"
                                                           class="black"><?=$phone;?></a>
                                                    <? endforeach;
                                                } ?>

                                            </div>
                                            <div class="col-sm-6 col-xs-12">

                                                <? if ($arItem['UF_DEP_EMAIL']) {
                                                    $arEmail = explode(",", $arItem['UF_DEP_EMAIL']);
                                                    foreach ($arEmail as $Email): ?>
                                                        <a href="<?= $Email; ?>"
                                                           class="black"><?= $Email; ?></a>
                                                    <? endforeach;
                                                } ?>

                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <? endif; ?>
                        <? endforeach; ?>
                    </table>

            </div>
        </div>
    <?}?>
    </div>

<? if ($arParams["MAP"] == "Y") { ?>

    <script type="text/javascript">

        var TCHK = <?= $arResult["Y_MAP"]?>

            ymaps.ready(init);

        function init() {
            var myMap = new ymaps.Map("YMapsID", {
                center: [<?= reset($DEPORTAMENT['DEPORTAMENT']["UF_DEP_COORDINATE"])?>],
                zoom: 11
            }, {
                searchControlProvider: 'yandex#search'
            });
            myMap.behaviors.disable('scrollZoom');

            myMap.controls.remove('rulerControl');
            myMap.controls.remove('searchControl');
            myMap.controls.remove('geolocationControl');
            myMap.controls.remove('trafficControl');
            myMap.controls.remove('typeSelector');

            var geoObjects = [];
            var i = 0;


            $.each(TCHK, function (indexI, valueI) {

                $.each(valueI.CORD, function (indexC, valueC) {

                    var valueNew = valueC.split(",");

                    myMap.geoObjects
                        .add(new ymaps.GeoObject(
                            {
                                geometry: {
                                    type: "Point",
                                    coordinates: [valueNew[0], valueNew[1]]
                                }
                            }, {
                                balloonLayout: "default#imageWithContent",
                                iconLayout: 'default#imageWithContent',
                                iconImageHref: "/local/templates/aspro_next/images/icon_arrow_5.png",
                                iconImageSize: [36, 48],
                                iconImageOffset: [-15, -50],
                                iconContentOffset: [15, 15],

                            }
                        ));
                    i++;
                });

            });

        }


    </script>
<? } ?>