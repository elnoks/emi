<?
global $DEPORTAMENT;

$Ymap = array();
foreach ($arResult["rows"] as $key => $info) {
    
    if (!empty($info["UF_DEP_COORDINATE"]) && $info["UF_DEP_MAIN"] == "нет") {
        $Ymap[$info["ID"]]["ADDRES"]["CITY"] = $info["UF_DEP_CITY"] ;
        $Ymap[$info["ID"]]["ADDRES"]["STRITE"] =  $info["UF_DEP_ADDRESS"];

        $formatCOrd = array();
        $arCord = array_chunk(explode(",", $info["UF_DEP_COORDINATE"]), 2);
        foreach ($arCord as $cord) {
            $Ymap[$info["ID"]]["CORD"][] = trim(implode(",", $cord));
        }
    }
}
$arResult["Y_MAP"] = json_encode($Ymap);
?>

