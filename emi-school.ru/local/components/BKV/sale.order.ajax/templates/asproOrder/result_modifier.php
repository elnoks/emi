<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */


if (count($_SESSION["BASKET"])) {
    $comment = "";
    foreach ($_SESSION["BASKET"] as $id => $info) {
        $comment .= " - ".$info["NAME"] . " - " . $info["QU"] . "шт" . "\n";
    }
    $arResult['JS_DATA']["ORDER_DESCRIPTION"] = "В данный момент отсутствуют:\n" . $comment;
}
/*$arResult['LOCATIONS'][key($arResult['LOCATIONS'])]["output"] = '<input type="text" id="LOCATION_ALT_PROP_DISPLAY_MANUAL[47]" name="LOCATION_ALT_PROP_DISPLAY_MANUAL[47]" value="0" />';*/
/*echo "<pre>";
print_r($arResult['LOCATIONS']);
echo "</pre>";*/


$arResult["PAY_SUCCES"] = "Y";
    if ( CSite::InGroup( array(125) ) ){
        $arResult["PAY_SUCCES"] = "N";
    }


if ( CSite::InGroup( array(103) ) ){
    $arResult["TYPE_USER"] = "MASTER";
}elseif ( CSite::InGroup( array(121) ) ){
    $arResult["TYPE_USER"] = "SALON";
}




$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);

?>