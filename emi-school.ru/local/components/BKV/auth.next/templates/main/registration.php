<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? \Bitrix\Main\Localization\Loc::loadMessages(__FILE__); ?>
<? $APPLICATION->AddChainItem(GetMessage("TITLE")); ?>
<? $APPLICATION->SetTitle(GetMessage("TITLE")); ?>
<? $APPLICATION->SetPageProperty("TITLE_CLASS", "center"); ?>
    <style type="text/css">
        .left-menu-md, body .container.cabinte-page .maxwidth-theme .left-menu-md, .right-menu-md, body .container.cabinte-page .maxwidth-theme .right-menu-md {
            display: none !important;
        }

        .content-md {
            width: 100%;
        }
    </style>
<? global $USER, $APPLICATION;

if (!$USER->IsAuthorized()) {
    ?>

    <? $status = getRus() ?>

    <? if ($status == "SHOP_RU") { ?>


            <? $APPLICATION->IncludeComponent(
                "BKV:main.register_ur",
                "main_old",
                Array(
                    "USER_PROPERTY_NAME" => "",
                    "SHOW_FIELDS" => array("NAME", "LAST_NAME", "SECOND_NAME", "EMAIL", "PERSONAL_PHONE", "UF_USER_TYPE", "WORK_STATE", "UF_INN", "UF_KPP", "UF_OGRN", "WORK_CITY", "WORK_STREET", "WORK_COMPANY"),
                    "REQUIRED_FIELDS" => array("NAME", "PERSONAL_PHONE", "EMAIL", "UF_USER_TYPE"),
                    "AUTH" => "Y",
                    "USE_BACKURL" => "Y",
                    "SUCCESS_PAGE" => "",
                    "SET_TITLE" => "N",
                    "USER_PROPERTY" => array(),
                    "SMS_SUCCES" => $arParams["SMS_SUCCES"]
                )
            ); ?>


    <? } else { ?>


        <? $APPLICATION->IncludeComponent(
            "BKV:main.register",
            "main_old",
            Array(
                "USER_PROPERTY_NAME" => "",
                "SHOW_FIELDS" => array("LOGIN", "LAST_NAME", "NAME", "SECOND_NAME", "EMAIL", "PERSONAL_PHONE"),
                "REQUIRED_FIELDS" => array("LOGIN", "NAME", "PERSONAL_PHONE", "EMAIL"),
                "AUTH" => "Y",
                "USE_BACKURL" => "Y",
                "SUCCESS_PAGE" => "",
                "SET_TITLE" => "N",
                "USER_PROPERTY" => array(),
                "SMS_SUCCES" => $arParams["SMS_SUCCES"]
            )
        ); ?>

    <? } ?>
<? } else {
    LocalRedirect($arParams["SEF_FOLDER"]);
} ?>