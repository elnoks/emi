<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="menu middle mobile_regions">
    <ul>
        <li>
            <a rel="nofollow" href="" class="dark-color parent">
                <i class="svg svg-address black"></i>
                <span><?= $arResult['CITY_NAME'] ?></span>
                <span class="arrow"><i class="svg svg_triangle_right"></i></span>
            </a>

            <ul class="dropdown cities">
                <li class="menu_back">
                    <a href="" class="dark-color" rel="nofollow">
                        <i class="svg svg-arrow-right"></i>Назад
                    </a>
                </li>
                <li class="menu_title ">
                    <div>
                        <input id="searchCityMobile"
                               class="autocomplete text ui-autocomplete-input"
                               type="text"
                               placeholder="Введите название города"
                               autocomplete="off">
                        <div class="wrap_icon_mobile">
                            <button class="top-btn inline-search-show twosmallfont">
                                <i class="svg inline  svg-inline-search big" aria-hidden="true">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 21 21">
                                        <defs>
                                            <style>
                                                .sscls-1 {
                                                    fill: #222;
                                                    fill-rule: evenodd;
                                                }
                                            </style>
                                        </defs>
                                        <path data-name="Rounded Rectangle 106" class="sscls-1"
                                              d="M1590.71,131.709a1,1,0,0,1-1.42,0l-4.68-4.677a9.069,9.069,0,1,1,1.42-1.427l4.68,4.678A1,1,0,0,1,1590.71,131.709ZM1579,113a7,7,0,1,0,7,7A7,7,0,0,0,1579,113Z"
                                              transform="translate(-1570 -111)"></path>
                                    </svg>
                                </i>
                            </button>
                        </div>
                    </div>

                </li>
                <?
                if (count($arResult["ALL_DEPORT"])) {

                    foreach ($arResult["ALL_DEPORT"] as $id => $info) {
                        ?>
                        <li class="dont_remove">
                            <form
                                    class="location-go"
                                    data-id="<?= $info["CODE"] ?>"
                                    action="https://<?= $info["URL"] ?>"
                                    method="POST">
                                <input type="hidden"
                                       name="LOC_ID"
                                       value="<?= $info["CODE"] ?>">
                                <input type="hidden"
                                       name="DEP_ID"
                                       value="<?= $info["ID_DEP"] ?>">
                                <button type="submit"><?= $info["NAME"] ?></button>
                            </form>
                        </li>

                        <?
                    }
                }
                ?>
            </ul>
        </li>
    </ul>


</div>
<?

global $DEPORTAMENT;
$jsDeportament = \Bitrix\Main\Web\Json::encode($DEPORTAMENT);
$dirMobile = \Bitrix\Main\Web\Json::encode($this->GetFolder());
$url_ajax_this = \Bitrix\Main\Web\Json::encode($arResult["THIS_DIR"]);

?>
<script>
    var url_ajax_this = <?= $url_ajax_this?>;
    var dirMobile = <?=$dirMobile?>;
    var DEPORTAMENT = <?= $jsDeportament?>;
</script>