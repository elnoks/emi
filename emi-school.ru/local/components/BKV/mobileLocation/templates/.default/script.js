$(document).ready(function () {
    var xhr1;
    var xhr2;

    $(document).on("input", '#searchCityMobile', function () {
        var thisInput = $(this);
        xhr1 = searchCity(thisInput, xhr1)
    });


    $('#searchCityMobile').focus(function () {
        $(this).data('placeholder', $(this).attr('placeholder'))
        $(this).attr('placeholder', '');
    });

    $('#searchCityMobile').blur(function () {
        $(this).attr('placeholder', $(this).data('placeholder'));
    });

})


function searchCity(thisInput, xhr) {
    if (xhr && xhr.readyState != 2) {
        xhr.abort();
    }


    if (thisInput.prop('selectionEnd') > 3) {
        var string = thisInput.prop('value');

        xhr = $.ajax({
            type: 'POST',
            url: dirMobile + '/ajax.php',
            dataType: "json",
            beforeSend: function () {

                if ($(".search_loader_li").length < 1) {
                    thisInput.closest(".menu_title ").after(' <li class="menu_title search_loader_li"><span class="search_loader"> </span></li>');
                }
            },
            data: {
                "STR": string,
                "DEP": DEPORTAMENT,
                "THIS_URL": window.url_ajax_this
            },
            success: function (msg) {
                if ($(".search_loader_li").length > 0) {
                    $(".search_loader_li").remove()
                }
                thisInput.closest(".dropdown").find(".result_serch ").remove();
                var html = '';
                $.each(msg, function (index, value) {
                    html +=
                        '<li  class="result_serch item">' +
                        '<form  data-id="' + value.ID + '" action="https://' + value.URL + '" method="POST">' +
                        '<input type="hidden" name="LOC_ID" value="' + value.ID + '">' +
                        '<input type="hidden" name="BS" value="' + msg.BUSCKET + '">' +
                        '<button type="submit">' + value.NAME + '<br>('+value.REGION_NAME+')</button>' +
                        '</form>' +
                        '</div>' +
                        '</li>';
                })
                $('#mobilemenu>.scroller').css('height', '1056px')
                thisInput.closest(".dropdown").find(".dont_remove").first().before(html);
            }
        });
    } else {
        $(".search_loader_li").remove()
        thisInput.closest(".dropdown").find(".result_serch ").remove();
    }

    return xhr;
}
