<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

use
    Bitrix\Sale\Location\GeoIp,
    Bitrix\Main\Loader,
    Bitrix\Sale\Location\LocationTable,
    Bitrix\Highloadblock as HL,
    Bitrix\Main\Entity,
    Bitrix\Sale,
    Bitrix\Main\Application;

Loader::includeModule('highloadblock');
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

$context = Application::getInstance()->getContext();
$request = $context->getRequest();

if (!$request->isAjaxRequest()) {
    die("!!Error!!");
}

$hlblock_id = HL_DEPORTAMENT;


if ($request->isAjaxRequest()) {

    $post = $request->getPostList()->toArray();

    if (!empty($post["DEP"])) {

        $filter = \Bitrix\Sale\Location\LocationTable::getList(array(
            'filter' => array(
                '=NAME.LANGUAGE_ID' => LANGUAGE_ID,
                "TYPE_CODE" => array("CITY", "VILLAGE"),
                "NAME_RU" => $post["STR"] . "%"
            ),
            'select' => array("ID",'COUNTRY_ID',"REGION_ID","PARENT_ID","COUNTRY_ID", 'NAME_RU' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE'),
            'order' => array("SORT" => "asc"),
            'limit' => 10
        ))->fetchAll();

        $iter = 0;

        foreach ($filter as $key => $info) {
            if(empty($info["COUNTRY_ID"]) && $info["TYPE_CODE"] == "VILLAGE"){
                $tet= getParent($info["ID"], "COUNTRY_ID");
                if( $tet["COUNTRY_ID"] != $post["DEP"]["COUNTRY_ID"]){
                    continue;
                }
            }
            if($info["COUNTRY_ID"] != $post["DEP"]["COUNTRY_ID"] && !empty($info["COUNTRY_ID"]) ){
                continue;
            }

            if (empty($info["REGION_ID"])) {
                $searchRegion = getParent($info["PARENT_ID"], "REGION_ID");
                $info["REGION_ID"] = $searchRegion["REGION_ID"];
                $info["COUNTRY_ID"] = $searchRegion["COUNTRY_ID"];
            }

            $arFilterRegion[$iter]=$info['REGION_ID'];

            $arResult[$iter]["ID"] = $info["ID"];
            $arResult[$iter]["TYPE_CODE"] = $info["TYPE_CODE"];
            $arResult[$iter]["NAME"] = $info["NAME_RU"];
            $arResult[$iter]["REGION_ID"] = $info['REGION_ID'];
            $arResult[$iter]["COUNTRY_ID"] = $info["COUNTRY_ID"];
            $arResult[$iter]["PARENT_ID"] = $info["PARENT_ID"];
            $iter++;
        }

        if(count($arFilterRegion)) {
            $arFilterRegionClear= array_unique($arFilterRegion);

            $resRegion = Bitrix\Sale\Location\LocationTable::getList(array(
                'filter' => array("ID" => $arFilterRegionClear, "TYPE_CODE" => array("CITY", "REGION"), '=NAME.LANGUAGE_ID' => LANGUAGE_ID),
                'select' => array('ID', 'NAME_RU' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE')
            ));
            while ($itemRegion = $resRegion->fetch()) {
             $arRegion[$itemRegion["ID"]]=$itemRegion["NAME_RU"];
            }
        }

        foreach ($arResult as $key => $arInfo){
            $arResult[$key]["REGION_NAME"] = $arRegion[$arInfo["REGION_ID"]];
        }

        $hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        $entity_table_name = $hlblock['TABLE_NAME'];
        $arFilter = array("UF_DEP_LOCATION" => '%"COUNTRY":"' . $post["DEP"]["COUNTRY_ID"] . '"%'); //задаете фильтр по вашим полям
        $sTableID = 'tbl_' . $entity_table_name;
        $rsData = $entity_data_class::getList(array(
            "select" => array("ID",'UF_DEP_LOCATION',"UF_DEP_SITE"), //выбираем все поля
            "filter" => $arFilter,
            "order" => array() // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
        ));
        $rsData = new CDBResult($rsData, $sTableID);

        while ($arRes = $rsData->Fetch()) {

            $formatUF_DEP_LOCATION = json_decode($arRes["UF_DEP_LOCATION"], true);
            $UF_DEP_LOCATION = array_diff($formatUF_DEP_LOCATION, array("", " "));
            $rsSites = CSite::GetByID($arRes["UF_DEP_SITE"]);
            $arSite = $rsSites->Fetch();

            $arRes["URL"] = $arSite["SERVER_NAME"] . $post["THIS_URL"];

            $arHL[$arRes["ID"]] = $arRes;

            if (count($UF_DEP_LOCATION)) {
                foreach ($UF_DEP_LOCATION as $vaKey => $arInfo) {
                    if (isset($arInfo["COUNTRY"]) && !empty($arInfo["COUNTRY"])) {
                        if (isset($arInfo["REGION"]) && !empty($arInfo["REGION"])) {

                            if (isset($arInfo["SUBREGION"]) && !empty($arInfo["SUBREGION"])) {

                                if (isset($arInfo["VILLAGE"]) && !empty($arInfo["VILLAGE"])) {
                                    $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["SUBREGION"]][$arInfo["VILLAGE"]]["VILLAGE"] = $arRes["ID"];
                                } else {
                                    $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["SUBREGION"]]["SUBREGION"] = $arRes["ID"];
                                }

                                if (isset($arInfo["CITY"]) && !empty($arInfo["CITY"])) {
                                    $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["SUBREGION"]][$arInfo["CITY"]]["CITY"] = $arRes["ID"];
                                } else {
                                    $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["SUBREGION"]]["SUBREGION"] = $arRes["ID"];
                                }
                            } else {
                                if (isset($arInfo["CITY"]) && !empty($arInfo["CITY"])) {
                                    $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["REGION"]][$arInfo["CITY"]]["CITY"] = $arRes["ID"];
                                } else {
                                    $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]]["REGION"] = $arRes["ID"];
                                }
                            }
                        } else {
                            $MASK[$arInfo["COUNTRY"]]["COUNTRY"] = $arRes["ID"];
                        }
                    }
                }
            }
        }


        if (!empty($arResult)) {

            $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

            if ($basket) {
                $compakytbasket = base64_encode(gzcompress(serialize($basket)));
            }

            foreach ($arResult as $id => $arRegionItem) {
                $arr[$id] = $arRegionItem['PARENT_ID'];

                if ($MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]][$arRegionItem["ID"]]["VILLAGE"]) {
                    $idHL = $arHL[$MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]][$arRegionItem["ID"]]["VILLAGE"]];
                } elseif ($MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]][$arRegionItem["ID"]]["CITY"]) {
                    $idHL = $arHL[$MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]][$arRegionItem["ID"]]["CITY"]];
                } elseif ($MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]]["SUBREGION"]) {
                    $idHL = $arHL[$MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]]["SUBREGION"]];
                } elseif ($MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]]["REGION"]) {
                    $idHL = $arHL[$MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]]["REGION"]];
                } elseif ($MASK[$arRegionItem["COUNTRY_ID"]]["COUNTRY"]) {
                    $idHL = $arHL[$MASK[$arRegionItem["COUNTRY_ID"]]["COUNTRY"]];
                }

                $arResult[$id]["URL"] = $idHL['URL'];

                if(!empty($idHL)){
                    $arResult[$id]["BUSCKET"] = $compakytbasket;
                }else{
                    $arResult[$id]["BUSCKET"] = "";
                }

            }
        }


        echo json_encode($arResult, JSON_UNESCAPED_UNICODE);

    }
}


function getParent($ID_SEARCH, $CODE_SEARCH)
{
    for ($shag = 0; $shag <= 10; $shag++) {
        $item = \Bitrix\Sale\Location\LocationTable::getById($ID_SEARCH)->fetch();
        if ($item[$CODE_SEARCH] > 0) {
            return $item;
        }
        $ID_SEARCH = $item["PARENT_ID"];
    }
}
