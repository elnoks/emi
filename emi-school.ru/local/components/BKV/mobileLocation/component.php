<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Highloadblock\HighloadBlockTable as HLBT,
    Bitrix\Sale;

CModule::IncludeModule('highloadblock');
const MY_HL_BLOCK_ID = HL_DEPORTAMENT;


global $DEPORTAMENT;
$arResult['CITY_NAME'] = $DEPORTAMENT['NAME_LANG'];

global $USER;


$entity_data_class = GetEntityDataClass(MY_HL_BLOCK_ID);
$rsData = $entity_data_class::getList(array(
    'select' => array('ID','UF_DEP_MAIN',"UF_DEP_SITE","UF_DEP_LOCATION","UF_DEP_NAME"),
    "order"=> array("UF_DEP_NAME" =>"ASC")
));
$arResult["ALL_DEPORT"] = array();

global $USER;

while ($el = $rsData->fetch()) {
    if ($el["UF_DEP_MAIN"]) continue;
   
    $rsSites = CSite::GetByID($el["UF_DEP_SITE"]);
    $arSite = $rsSites->Fetch();
    $location = str_replace('&quot;', '"', $el["UF_DEP_LOCATION"]);
    $location = json_decode($location, true);
    $arResult["THIS_DIR"] = $APPLICATION->GetCurDir();

    foreach ($location as $key => $infoLocation) {
        $infoLocation = array_diff($infoLocation, array("", " "));
        $keySearch = array_key_exists("CITY", $infoLocation);

        if ($keySearch !== false) {
            $arResult["ALL_DEPORT"][$el["ID"]]["URL"] = $arSite["SERVER_NAME"] .  $arResult["THIS_DIR"];
            $arResult["ALL_DEPORT"][$el["ID"]]["NAME"] = $el["UF_DEP_NAME"];
            $arResult["ALL_DEPORT"][$el["ID"]]["CODE"] = $infoLocation["CITY"];
            $arResult["ALL_DEPORT"][$el["ID"]]["ID_DEP"] = $el["ID"];
        }
    }
}


$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

if($basket){
    $compakytbasket = base64_encode(gzcompress(serialize($basket)));
    $arResult["BUSCKET"] = $compakytbasket;
}

$this->IncludeComponentTemplate();
function GetEntityDataClass($HlBlockId)
{
    if (empty($HlBlockId) || $HlBlockId < 1) {
        return false;
    }
    $hlblock = HLBT::getById($HlBlockId)->fetch();
    $entity = HLBT::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();
    return $entity_data_class;
}