<? require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
set_time_limit(0);


use
    Bitrix\Highloadblock as HL,
    Bitrix\Main\Entity,
    Bitrix\Main\Loader,
    Bitrix\Main\Application;

Loader::includeModule('highloadblock');
$context = Application::getInstance()->getContext();
$request = $context->getRequest();
$hlblock_id = 17;
$HL_Infoblock_ID = 18;
if ($request->isAjaxRequest()) {
    $post = $request->getPostList()->toArray();

    $ipAddress = Bitrix\Main\Service\GeoIp\Manager::getRealIp();
    $locCode = Bitrix\Sale\Location\GeoIp::getLocationCode($ipAddress, LANGUAGE_ID);

    $item = Bitrix\Sale\Location\LocationTable::getList(
        array(
            'filter' => array('=CODE' => $locCode, '=NAME.LANGUAGE_ID' => LANGUAGE_ID),
            'select' => array('ID',"COUNTRY_ID", 'NAME_LANG' => 'NAME.NAME'),
            "order" => array("NAME_LANG" => "asc")
        )
    )->fetch();

    // city
    $hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();

    $rsData = $entity_data_class::getList(array(
        "select" => array("*"),
        "filter" => array("UF_TYPE_LOCATION"=>"CITY","UF_COUNTRY_ID"=>$item["COUNTRY_ID"])
    ));

    while ($arData = $rsData->Fetch()) {
      $arMask[$arData["UF_ID_LOCATION"]]["NAME"] = unserialize($arData["UF_INFO_LOCATION"])["NAME_LANG"];

        $nextfilter[$arData["UF_ID_LOCATION"]] = $arData["UF_ID_LOCATION"];
    }


    // child city
    if(count($nextfilter)){
    /*
        $rsDataCHCH = Bitrix\Sale\Location\LocationTable::getList(
            array(
                'filter' => array("PARENT_ID"=>$nextfilter, '=NAME.LANGUAGE_ID' => LANGUAGE_ID),
                'select' => array('*', 'NAME_LANG' => 'NAME.NAME'),
                "order" => array("NAME_LANG" => "asc")
            )
        );
        while ($arDataCHCH = $rsDataCHCH->Fetch()) {

            $arMask[$arDataCHCH["PARENT_ID"]]["CH"][$arDataCHCH["ID"]] =  $arDataCHCH["NAME_LANG"];
        }

*/

            $rsDataCHCH = $entity_data_class::getList(array(
             "select" => array("*"),
             "filter" => array("UF_PARENT_ID"=>$nextfilter),

         ));

     while ($arDataCHCH = $rsDataCHCH->Fetch()) {
        $arMask[$arDataCHCH["UF_PARENT_ID"]]["CH"][$arDataCHCH["UF_ID_LOCATION"]] =  unserialize($arDataCHCH["UF_INFO_LOCATION"])["NAME_LANG"];
     }



    }

    foreach ($arMask as $key=> $info){
        if(!$info["CH"]){
            unset($arMask[$key]);
        }
    }






    echo "<pre>";
    print_r($arMask);
    echo "</pre>";








    /*
        $filt = array(
            "UF_TYPE_LOCATION" => $post["type"],
            "UF_LANG" => LANGUAGE_ID
        );

        if ($post["type"] == "CITY") {
            $filt["UF_REGION_ID"] = $post["raiyon"];
        } else {
            $filt["UF_PARENT_ID"] = $post["raiyon"];
        }


        if ($post["raiyon"] > 0) {

            ///село
            $hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();

            $rsData = $entity_data_class::getList(array(
                "select" => array("*"),
                "filter" => $filt
            ));

            while ($arData = $rsData->Fetch()) {
                $decode = unserialize($arData["UF_INFO_LOCATION"]);
                $arResult['VILIGE'][$decode["ID"]]["ID"] = $arData["UF_ID_LOCATION"];
                $arResult['VILIGE'][$decode["ID"]]["TYPE_CODE"] = $arData["UF_TYPE_LOCATION"];
                $arResult['VILIGE'][$decode["ID"]]["NAME"] = $decode["NAME_LANG"];
                $arResult['VILIGE'][$decode["ID"]]["REGION_ID"] = $arData["UF_REGION_ID"];
                $arResult['VILIGE'][$decode["ID"]]["COUNTRY_ID"] = $arData["UF_COUNTRY_ID"];
                $arResult['VILIGE'][$decode["ID"]]["PARENT_ID"] = $arData["UF_PARENT_ID"];
                $COUNTRI = $arData["UF_COUNTRY_ID"];
                $volume[$decode["ID"]] = $decode["NAME_LANG"];
            }
            array_multisort($volume, SORT_ASC, $arResult['VILIGE']);


            $arFilter = array("UF_DEP_LOCATION" => '%"COUNTRY":"' . $COUNTRI . '"%'); //задаете фильтр по вашим полям
            ///депортаменты
            $hlblockDEP = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
            $entityDEP = HL\HighloadBlockTable::compileEntity($hlblockDEP);
            $entity_data_classDEP = $entityDEP->getDataClass();
            $rsDataDEP = $entity_data_classDEP::getList(array(
                "select" => array("*"),
                "filter" => $arFilter,
            ));

            while ($arResDEP = $rsDataDEP->Fetch()) {


                $formatUF_DEP_LOCATION = json_decode($arResDEP["UF_DEP_LOCATION"], true);
                $UF_DEP_LOCATION = array_diff($formatUF_DEP_LOCATION, array("", " "));
                $rsSites = CSite::GetByID($arResDEP["UF_DEP_SITE"]);
                $arSite = $rsSites->Fetch();
                $arResDEP["URL"] = $arSite["SERVER_NAME"];
                $rsEnum = CCatalogGroup::GetList(
                    array("NAME" => "ASC"),
                    array("ID" => $arRes["UF_DEP_TYPE_PRICE"]),
                    false,
                    false,
                    array("ID", "NAME")
                );
                while ($arEnum = $rsEnum->Fetch()) {
                    $arResDEP["UF_DEP_TYPE_PRICE_CODE"][$arEnum["ID"]] = $arEnum["NAME"];
                }

                $arHL[$arResDEP["ID"]] = $arResDEP;

                if (count($UF_DEP_LOCATION)) {
                    foreach ($UF_DEP_LOCATION as $vaKey => $arInfo) {
                        if (isset($arInfo["COUNTRY"]) && !empty($arInfo["COUNTRY"])) {
                            if (isset($arInfo["REGION"]) && !empty($arInfo["REGION"])) {

                                if (isset($arInfo["SUBREGION"]) && !empty($arInfo["SUBREGION"])) {
                                    if (isset($arInfo["VILLAGE"]) && !empty($arInfo["VILLAGE"])) {
                                        $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["SUBREGION"]][$arInfo["VILLAGE"]]["VILLAGE"] = $arResDEP["ID"];
                                    } else {
                                        $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["SUBREGION"]]["SUBREGION"] = $arResDEP["ID"];
                                    }

                                    if (isset($arInfo["CITY"]) && !empty($arInfo["CITY"])) {
                                        $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["SUBREGION"]][$arInfo["CITY"]]["CITY"] = $arResDEP["ID"];
                                    } else {
                                        $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["SUBREGION"]]["SUBREGION"] = $arResDEP["ID"];
                                    }
                                } else {
                                    if (isset($arInfo["CITY"]) && !empty($arInfo["CITY"])) {
                                        $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["REGION"]][$arInfo["CITY"]]["CITY"] = $arResDEP["ID"];
                                    } else {
                                        $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]]["REGION"] = $arResDEP["ID"];
                                    }
                                }
                            } else {
                                $MASK[$arInfo["COUNTRY"]]["COUNTRY"] = $arResDEP["ID"];
                            }
                        }
                    }
                }
            }

            foreach ($arResult['VILIGE'] as $id => $arRegionItem) {

                if ($MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]][$arRegionItem["ID"]]["VILLAGE"]) {
                    $idHL = $arHL[$MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]][$arRegionItem["ID"]]["VILLAGE"]];
                } elseif ($MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]][$arRegionItem["ID"]]["CITY"]) {
                    $idHL = $arHL[$MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]][$arRegionItem["ID"]]["CITY"]];
                } elseif ($MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]]["SUBREGION"]) {
                    $idHL = $arHL[$MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]]["SUBREGION"]];
                } elseif ($MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]]["REGION"]) {
                    $idHL = $arHL[$MASK[$arRegionItem["COUNTRY_ID"]][$arRegionItem["REGION_ID"]]["REGION"]];
                } elseif ($MASK[$arRegionItem["COUNTRY_ID"]]["COUNTRY"]) {
                    $idHL = $arHL[$MASK[$arRegionItem["COUNTRY_ID"]]["COUNTRY"]];
                }
                $arResult['VILIGE'][$id]["URL"] = $idHL["URL"];
            }
            echo json_encode($arResult);
        }*/
}