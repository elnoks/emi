<?

define('STOP_STATISTICS', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC', 'Y');
define('BX_SKIP_POST_UNQUOTE', true);
define('NO_AGENT_CHECK', true);

require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

use
    Bitrix\Highloadblock as HL,
    Bitrix\Main\Entity,
    Bitrix\Main\Loader,
    Bitrix\Sale,
    Bitrix\Main\Application;

Loader::includeModule('highloadblock');
Loader::includeModule('sale');


global $APPLICATION;
$context = Application::getInstance()->getContext();
$request = $context->getRequest();
if (!$request->isAjaxRequest()) {
    die("!!Error!!");
}

$hlblock_id = HL_DEPORTAMENT;
if ($request->isAjaxRequest()) {
    $post = $request->getPostList()->toArray();

    if ($post["raiyon"] > 0) {
        $lang = LANGUAGE_ID;
        if ($lang=="by"||$lang=="az") $lang="ru";
        $arResult = array();
        foreach ($post["type"] as $type) {

            if ($type == "SUBREGION") {
                $filter =
                    array(
                        '=NAME.LANGUAGE_ID' => $lang,
                        "TYPE_CODE" => $type,
                        "PARENT_ID" => $post["raiyon"]
                    );
            } elseif ($type == "CITY") {
                $filter =
                    array(
                        '=NAME.LANGUAGE_ID' => $lang,
                        "TYPE_CODE" => $type,
                        array(
                            "LOGIC" => "OR",
                            array(
                                "PARENT_ID" => $post["raiyon"]
                            ),
                            array(
                                "REGION_ID" => $post["raiyon"]
                            )
                        )

                    );
            } elseif ($type == "VILLAGE") {
                $filter =
                    array(
                        '=NAME.LANGUAGE_ID' => $lang,
                        "TYPE_CODE" => $type,
                        "PARENT_ID" => $post["raiyon"],

                    );
            }


            $filter = \Bitrix\Sale\Location\LocationTable::getList(array(
                "filter" => $filter,
                "select" => array('ID',"REGION_ID","COUNTRY_ID","PARENT_ID", 'NAME_RU' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE'),
                'order' => array("SORT" => "asc")
            ))->fetchAll();

            foreach ($filter as $arData) {

                $arResult['LOCATION'][$arData["ID"]]["ID"] = $arData["ID"];
                $arResult['LOCATION'][$arData["ID"]]["TYPE_CODE"] = $arData["TYPE_CODE"];
                $arResult['LOCATION'][$arData["ID"]]["NAME"] = $arData["NAME_RU"];
                $arResult['LOCATION'][$arData["ID"]]["REGION_ID"] = $arData["REGION_ID"];
                $arResult['LOCATION'][$arData["ID"]]["COUNTRY_ID"] = $arData["COUNTRY_ID"];
                $arResult['LOCATION'][$arData["ID"]]["PARENT_ID"] = $arData["PARENT_ID"];
                $COUNTRI = $arData["COUNTRY_ID"];
                $volume[$arData["ID"]] = $arData["NAME_RU"];
            }
        }

        if (empty($COUNTRI)) {
            $id = $post["raiyon"];
            for ($shag = 0; $shag <= 10; $shag++) {
                $item = \Bitrix\Sale\Location\LocationTable::getById($id)->fetch();
                if ($item["COUNTRY_ID"] > 0) {
                    $COUNTRI = $item["COUNTRY_ID"];
                    break;
                }
                $id = $item["PARENT_ID"];
            }
        }

        $arFilter = array("UF_DEP_LOCATION" => '%"COUNTRY":"' . $COUNTRI . '"%');

        ///депортаменты
        $hlblockDEP = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
        $entityDEP = HL\HighloadBlockTable::compileEntity($hlblockDEP);
        $entity_data_classDEP = $entityDEP->getDataClass();
        $rsDataDEP = $entity_data_classDEP::getList(array(
            "select" => array("ID",'UF_DEP_LOCATION',"UF_DEP_SITE"),
            "filter" => $arFilter,
        ));

        while ($arResDEP = $rsDataDEP->Fetch()) {
            $formatUF_DEP_LOCATION = json_decode($arResDEP["UF_DEP_LOCATION"], true);
            $UF_DEP_LOCATION = array_diff($formatUF_DEP_LOCATION, array("", " "));
            $rsSites = CSite::GetByID($arResDEP["UF_DEP_SITE"]);
            $arSite = $rsSites->Fetch();

            $arResDEP["URL"] = $arSite["SERVER_NAME"] . $post["this_url"];

            $arHL[$arResDEP["ID"]] = $arResDEP;
            if (count($UF_DEP_LOCATION)) {
                foreach ($UF_DEP_LOCATION as $vaKey => $arInfo) {
                    if (isset($arInfo["COUNTRY"]) && !empty($arInfo["COUNTRY"])) {
                        if (isset($arInfo["REGION"]) && !empty($arInfo["REGION"])) {

                            if (isset($arInfo["SUBREGION"]) && !empty($arInfo["SUBREGION"])) {
                                if (isset($arInfo["VILLAGE"]) && !empty($arInfo["VILLAGE"])) {
                                    $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["SUBREGION"]][$arInfo["VILLAGE"]]["VILLAGE"] = $arResDEP["ID"];
                                } else {
                                    $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["SUBREGION"]]["SUBREGION"] = $arResDEP["ID"];
                                }

                                if (isset($arInfo["CITY"]) && !empty($arInfo["CITY"])) {
                                    $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["SUBREGION"]][$arInfo["CITY"]]["CITY"] = $arResDEP["ID"];
                                } else {
                                    $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["SUBREGION"]]["SUBREGION"] = $arResDEP["ID"];
                                }
                            } else {
                                if (isset($arInfo["CITY"]) && !empty($arInfo["CITY"])) {
                                    $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]][$arInfo["REGION"]][$arInfo["CITY"]]["CITY"] = $arResDEP["ID"];
                                } else {
                                    $MASK[$arInfo["COUNTRY"]][$arInfo["REGION"]]["REGION"] = $arResDEP["ID"];
                                }
                            }
                        } else {
                            $MASK[$arInfo["COUNTRY"]]["COUNTRY"] = $arResDEP["ID"];
                        }
                    }
                }
            }
        }


        foreach ($arResult['LOCATION'] as $id => $arRegionItem) {

            if (empty($arRegionItem["REGION_ID"])) {
                $searchRegion = getParent($arRegionItem["PARENT_ID"], "REGION_ID");
                $arRegionItem["REGION_ID"] = $searchRegion["REGION_ID"];
            }
            if ($MASK[$COUNTRI][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]][$arRegionItem["ID"]]["VILLAGE"]) {
                $idHL = $arHL[$MASK[$COUNTRI][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]][$arRegionItem["ID"]]["VILLAGE"]];
            } elseif ($MASK[$COUNTRI][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]][$arRegionItem["ID"]]["CITY"]) {
                $idHL = $arHL[$MASK[$COUNTRI][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]][$arRegionItem["ID"]]["CITY"]];
            } elseif ($MASK[$COUNTRI][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]]["SUBREGION"]) {
                $idHL = $arHL[$MASK[$COUNTRI][$arRegionItem["REGION_ID"]][$arRegionItem["PARENT_ID"]]["SUBREGION"]];
            } elseif ($MASK[$COUNTRI][$arRegionItem["REGION_ID"]]["REGION"]) {
                $idHL = $arHL[$MASK[$COUNTRI][$arRegionItem["REGION_ID"]]["REGION"]];
            } elseif ($MASK[$COUNTRI]["COUNTRY"]) {
                $idHL = $arHL[$MASK[$COUNTRI]["COUNTRY"]];
            }
            $arResult['LOCATION'][$id]["URL"] = $idHL["URL"];
            $arResult['LOCATION'][$id]["ID_DEP"] = $idHL["ID"];

        }

        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

        if ($basket) {
            $compakytbasket = base64_encode(gzcompress(serialize($basket)));

            $arResult["BUSCKET"] = $compakytbasket;
        }

        $Fuser = Bitrix\Sale\Fuser::getId();

        $arResult["FUSER"] = $Fuser;

        echo json_encode($arResult);
    }
}


function getParent($ID_SEARCH, $CODE_SEARCH)
{    for ($shag = 0; $shag <= 10; $shag++) {
        $item = \Bitrix\Sale\Location\LocationTable::getById($ID_SEARCH)->fetch();
        if ($item[$CODE_SEARCH] > 0) {

            return $item;
        }
        $ID_SEARCH = $item["PARENT_ID"];
    }
}