<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application,
    Bitrix\Sale;


$contex = Application::getInstance()->getContext();
$request = $contex->getRequest();
if (!$request->isAjaxRequest()) {
    die("!!Error!!");
}


if ($request->isAjaxRequest()) {

    $postList = $request->getPostList()->toArray();

    $locationOBJ = new location($postList);
}


class location
{

    var $POST;

    function __construct($post)
    {
        $this->POST = $post;
        $this->POST["SELECT"] = json_decode($this->POST["SELECT"], true);
        $this->Type = $this->getTypeLocation();
        $thisHtml = $this->getNextSelect($this->POST["TYPE"]);
        if (!empty($thisHtml)) {
            echo $thisHtml;
        }
    }

    function getTypeLocation()
    {
        $res = \Bitrix\Sale\Location\TypeTable::getList(array(
            "order" => array("SORT" => "ASC"),
            'select' => array('ID',"CODE", 'NAME_LANG' => 'NAME.NAME'),
            'filter' => array('=NAME.LANGUAGE_ID' => LANGUAGE_ID)
        ));
        while ($item = $res->fetch()) {

            $arType[$item["CODE"]] = $item;
        }
        return $arType;
    }

    function getNextSelect($type)
    {
        $html = "";
        switch ($type) {
            case "COUNTRY":
                $html = $this->getSelect("REGION", $this->POST["ID"]);
                if ($html) {
                    break;
                }
            case "REGION":
                $html = $this->getSelect("SUBREGION", $this->POST["ID"]);

            case "SUBREGION":
                $html .= $this->getSelect(array("CITY", "VILLAGE"), $this->POST["ID"]);

            case "CITY":
            case "VILLAGE":
                $html .= $this->getSelect("STREET", $this->POST["ID"]);
                if ($html) {
                    break;
                }
            case "STREET":
                break;
            default:
                break;
        }

        return $html;
    }

    function getSelect($typeSelect, $idThisSelect)
    {
        $parmGetList = array(
            "order" => array("NAME_LANG" => "ASC"),
            'filter' => array(
                '=NAME.LANGUAGE_ID' => LANGUAGE_ID,
                "TYPE_CODE" => $typeSelect),
            'select' => array('ID', 'NAME_LANG' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE')
        );

        $parmGetList["filter"]["PARENT_ID"] = $idThisSelect;

        if (in_array("CITY", $typeSelect)) {
            $parmGetList["filter"]["PARENT_ID"] = array($this->POST["SELECT"]["COUNTRY_DISTRICT"], $idThisSelect);
        }


        $resCOUNTRY = \Bitrix\Sale\Location\LocationTable::getList($parmGetList);


        if ($resCOUNTRY->getSelectedRowsCount()) {

            $arType = $this->Type;

            if (in_array("CITY", $typeSelect)) {
                $nameSelect = "Нас. пункт";
                $nameSelectAttr = reset($typeSelect);
            } else {
                $nameSelect = $arType[$typeSelect]["NAME_LANG"];
                $nameSelectAttr = $typeSelect;
            }

            $selectHtml = "<div class='selectLocation {$typeSelect}'><label>{$nameSelect}:</label><select name='{$nameSelectAttr}'><option  value=''>нет</option>";;
            while ($itemLocation = $resCOUNTRY->fetch()) {

                $selectHtml .= "<option value='{$itemLocation["ID"]}'>{$itemLocation["NAME_LANG"]}</option>";
            }
            $selectHtml .= "</select></div>";
        }

        return $selectHtml;
    }
}