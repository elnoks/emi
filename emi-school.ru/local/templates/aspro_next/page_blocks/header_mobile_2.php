<div class="mobileheader-v2">
	<div class="burger col-xs-2">
		<?=CNext::showIconSvg("burger dark", SITE_TEMPLATE_PATH."/images/svg/Burger_big_white.svg");?>
		<?=CNext::showIconSvg("close dark", SITE_TEMPLATE_PATH."/images/svg/Close.svg");?>
	</div>
    <?
    /*
     	<div class="title-block col-sm-6 col-xs-5 pull-left"><?($APPLICATION->GetTitle() ? $APPLICATION->ShowTitle(false) : $APPLICATION->ShowTitle());?></div>
    */
    ?>

    <div class="logo-block text-center col-xs-4">
        <div class="logo<?=$logoClass?>">
            <?= AsproNextCustom::ShowLogoMobile(); ?>
        </div>
    </div>
	<div class="right-icons pull-right col-xs-6">
		<div class="pull-right">
			<div class="wrap_icon">
				<button class="top-btn inline-search-show twosmallfont">
					<?=CNext::showIconSvg("search big", SITE_TEMPLATE_PATH."/images/svg/Search_big_black.svg");?>
				</button>
			</div>
		</div>
		<div class="pull-right">
			<div class="wrap_icon wrap_basket">
				<?=AsproNextCustom::ShowBasketWithCompareLink('', 'big white', false, false, true);?>
			</div>
		</div>
		<div class="pull-right">
			<div class="wrap_icon wrap_cabinet">
				<?=AsproNextCustom::showCabinetLink(true, false, 'big white');?>
			</div>
		</div>
	</div>
</div>