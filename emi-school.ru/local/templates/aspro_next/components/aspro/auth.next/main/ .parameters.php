<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arTemplateParameters = array(
    "SMS_SUCCES" => array(
        "NAME" => GetMessage("SMS_SUCCES_LANG"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "Y",
    ),
);

?>