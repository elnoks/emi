<?
$MESS['FORM_REQUIRED_FIELDS'] = "Required Fields";
$MESS['FORM_APPLY'] = "Apply";
$MESS['FORM_ADD'] = "Add";
$MESS['FORM_ACCESS_DENIED'] = "There is not enough access to the web form.";
$MESS['FORM_DATA_SAVED1'] = "Thank you! <br> <br> Your application number";
$MESS['FORM_DATA_SAVED2'] = "accepted for review.";
$MESS['FORM_MODULE_NOT_INSTALLED'] = "The web forms module is not installed.";
$MESS['FORM_NOT_FOUND'] = "Web form not found.";
$MESS["FORM_SUCCESS"] = "Your message was successfully sent";
$MESS["SEND_FORM"] = "Send";
$MESS["FORM_REQUIRED_FIELDS"] = "Required Fields";
$MESS["FORM_CAPRCHE_TITLE"] = "Enter the text from the image";
$MESS["FORM_RESET"] = "Cancel";



/******************** Fields for forms ****************/


$MESS ["CLIENT_NAME"] = "Your Name";
$MESS ["PHONE"] = "Phone";
$MESS ["EMAIL"] = "E-mail";
$MESS ["POST"] = "Message";
$MESS ["QUESTION"] = "Question";
$MESS ["PRODUCT_NAME"] = "Product";
?>
