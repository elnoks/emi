/*
You can use this file with your scripts.
It will not be overwritten when you upgrade solution.
*/
BX.ready(function(){

    if (BX('showMe')) {
        setTimeout(function() {
            var easing = new BX.easing({
                    duration: 1500,
                    start: {opacity: 0},
                    finish: {opacity: 100},
                    step: function (state) {
                        BX('showMe').style.opacity = state.opacity / 100;
                    },
                }
            );
            easing.animate();
        }, 700);
    }

    $('.to-cart').click(function() {
        //console.log('add_to_cart');
        yaCounter50511952.reachGoal('add_to_cart');
    });
    $('.sale_order_full_table input[type="submit"]').click(function() {
        //console.log('go_to_pay');
        yaCounter50511952.reachGoal('go_to_pay');
    });
    $('#registraion-page-form input[type="submit"]').click(function() {
        //console.log('go_to_register');
        yaCounter50511952.reachGoal('go_to_register');
    });

    // $('.registration-wrapper .btn-pink').click(function() {
    //     yaCounter22189930.reachGoal('zareg');
    //     ga('send', 'pageview', '/zareg');
    //     console.log('zareg');
    // });
});