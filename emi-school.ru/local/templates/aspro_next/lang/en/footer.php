<?
$MESS["COPY"] = "Copyright";
$MESS["PAYMENT"] = "Payment";
$MESS["PHONE"] = "Phone";
$MESS["CREATE"] = "Website Creation";
$MESS["NAME"] = "Name";
$MESS["OK_MSG"] = "Your message has been sent successfully.";
$MESS["FOUND_CHEAPER"] = "Found cheaper?";
$MESS["ITEM_ADDED"] = "Product successfully added to the cart";
$MESS["ITEM_ADDED_ORDER"] = "Go to checkout";
$MESS["ITEM_ADDED_BACK"] = "Continue Shopping";
$MESS["SEND_MSG"] = "Send Message";
$MESS["SEND_RESUME"] = "Send Resume";
$MESS["FIO"] = "Last Name, First Name, Patronymic";
$MESS["ARBITRARY_1"] = "Arbitrary Region 1";
$MESS["ARBITRARY_2"] = "Arbitrary Region 2";
$MESS["ROUBLE"] = "rub.";
$MESS["CALLBACK"] = "Order a call";
?>