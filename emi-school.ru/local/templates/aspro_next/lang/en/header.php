<?
$MESS["PHONE"] = "Phone";
$MESS["FAST_VIEW"] = "Quick View";
$MESS["TABLES_SIZE_TITLE"] = "Fit Size";
$MESS["SOCIAL"] = "Social Networks";
$MESS["DESCRIPTION"] = "Store Description";
$MESS["ITEMS"] = "Products";
$MESS["LOGO"] = "Logo";
$MESS["REGISTER_INCLUDE_AREA"] = "Registration text";
$MESS["AUTH_INCLUDE_AREA"] = "Authorization Text";
$MESS["FRONT_IMG"] = "Company Image";
$MESS["EMPTY_CART"] = "empty";

$MESS["CATALOG_VIEW_MORE"] = "... Show all";
$MESS["CATALOG_VIEW_LESS"] = "... Collapse";
$MESS["JS_REQUIRED"] = 'Fill in this field';
$MESS["JS_FORMAT"] = 'Invalid format';
$MESS["JS_FILE_EXT"] = 'Invalid file extension';
$MESS["JS_PASSWORD_COPY"] = 'Passwords do not match';
$MESS["JS_PASSWORD_LENGTH"] = 'At least 6 characters';
$MESS["JS_ERROR"] = 'The field is incorrectly filled';
$MESS["JS_FILE_SIZE"] = 'Maximum size 5mb';
$MESS["JS_FILE_BUTTON_NAME"] = 'Select a file';
$MESS["JS_FILE_DEFAULT"] = 'File not found';
$MESS["JS_DATE"] = 'Invalid date';
$MESS['JS_DATETIME'] = 'Incorrect date/time';
$MESS["JS_REQUIRED_LICENSES"] = 'Agree to the terms';
$MESS["LICENSE_PROP"] = 'Consent to the processing of personal data';
$MESS["LOGIN_LEN"] = 'Enter the minimum {0} character';
$MESS["FANCY_CLOSE"] = 'Close';
$MESS["FANCY_NEXT"] = 'Next';
$MESS["FANCY_PREV"] = 'Previous';
$MESS["TOP_AUTH_REGISTER"] = 'Registration';
$MESS["CALLBACK"] = 'Order a call';
$MESS["S_CALLBACK"] = 'Order a call';
$MESS["UNTIL_AKC"] = 'Until end of action';
$MESS["TITLE_QUANTITY_BLOCK"] = 'Balance';
$MESS["TITLE_QUANTITY"] = 'pcs.';
$MESS["TOTAL_SUMM_ITEM"] = 'Total Cost';
$MESS["SUBSCRIBE_SUCCESS"] = 'You have successfully subscribed';

$MESS["RECAPTCHA_TEXT"] = 'Confirm that you are not a robot';
$MESS["JS_RECAPTCHA_ERROR"] = 'Pass the test';

$MESS["COUNTDOWN_SEC"] = 'sec.';
$MESS["COUNTDOWN_MIN"] = 'min.';
$MESS["COUNTDOWN_HOUR"] = 'hour.';
$MESS["COUNTDOWN_DAY0"] = 'days';
$MESS["COUNTDOWN_DAY1"] = 'day';
$MESS["COUNTDOWN_DAY2"] = 'day';
$MESS["COUNTDOWN_WEAK0"] = 'Weeks';
$MESS["COUNTDOWN_WEAK1"] = 'Week';
$MESS["COUNTDOWN_WEAK2"] = 'Weeks';
$MESS["COUNTDOWN_MONTH0"] = 'Months';
$MESS["COUNTDOWN_MONTH1"] = 'Month';
$MESS["COUNTDOWN_MONTH2"] = 'Months';
$MESS["COUNTDOWN_YEAR0"] = 'Years';
$MESS["COUNTDOWN_YEAR1"] = 'Year';
$MESS["COUNTDOWN_YEAR2"] = 'Years';

$MESS["CATALOG_PARTIAL_BASKET_PROPERTIES_ERROR"] = "Not all the properties of the added product are filled in";
$MESS["CATALOG_EMPTY_BASKET_PROPERTIES_ERROR"] = "Select the properties of the goods added to the cart in the parameters";
$MESS["CATALOG_ELEMENT_NOT_FOUND"] = "Item not found";
$MESS["ERROR_ADD2BASKET"] = "Error adding goods to the cart";
$MESS["CATALOG_SUCCESSFUL_ADD_TO_BASKET"] = "Successful addition of goods to the cart";
$MESS["ERROR_BASKET_TITLE"] = 'Trash Error';
$MESS["ERROR_BASKET_PROP_TITLE"] = 'Select the properties to add to the basket';
$MESS["ERROR_BASKET_BUTTON"] = 'Select';
$MESS["BASKET_TOP"] = 'Trash in header';
$MESS["ERROR_ADD_DELAY_ITEM"] = 'Delayed Recycle Bin Error';

$MESS["VIEWED_TITLE"] = "You have previously watched";
$MESS["VIEWED_BEFORE"] = "You have previously watched";
$MESS["BEST_TITLE"] = "Best offers";
$MESS["CT_BST_SEARCH_BUTTON"] = "Search";
$MESS["CT_BST_SEARCH2_BUTTON"] = "Find";

$MESS["BASKET_PRINT_BUTTON"] = "Print";
$MESS["BASKET_CLEAR_ALL_BUTTON"] = "Clear";
$MESS["BASKET_QUICK_ORDER_BUTTON"] = "Quick order";
$MESS["BASKET_CONTINUE_BUTTON"] = "Continue shopping";
$MESS["BASKET_ORDER_BUTTON"] = "Checkout";
$MESS["SHARE_BUTTON"] = "Share";
$MESS["BASKET_CHANGE_TITLE"] = "Your order";
$MESS["BASKET_CHANGE_LINK"] = "Edit";
$MESS["MORE_INFO_SKU"] = "Details";

$MESS["FROM"] = "from";
$MESS["TITLE_BLOCK_VIEWED_NAME"] = "You have previously watched";
$MESS["T_BASKET"] = "Recycle Bin";
$MESS["FILTER_EXPAND_VALUES"] = "Show All";
$MESS["FILTER_HIDE_VALUES"] = "Minimize";
$MESS["FULL_ORDER"] = "Full Order";

$MESS['CUSTOM_COLOR_CHOOSE'] = 'Select';
$MESS['CUSTOM_COLOR_CANCEL'] = 'Cancel';
$MESS['S_MOBILE_MENU'] = 'Menu';
$MESS['NEXT_T_MENU_BACK'] = 'Back';
$MESS['NEXT_T_MENU_CALLBACK'] = 'Feedback';
$MESS['NEXT_T_MENU_CONTACTS_TITLE'] = 'Keep in touch';

$MESS['SEARCH_TITLE'] = 'Search';
$MESS['SOCIAL_TITLE'] = 'Stay Connected';
$MESS['HEADER_SCHEDULE'] = 'Work Time';
$MESS['SEO_TEXT'] = 'SEO description';
$MESS['COMPANY_IMG'] = 'Company Picture';
$MESS['COMPANY_TEXT'] = 'Company Description';

$MESS['CONFIG_SAVE_SUCCESS'] = 'Settings saved';
$MESS['CONFIG_SAVE_FAIL'] = 'Error saving settings';
$MESS["ITEM_ECONOMY"] = "Savings";
$MESS["ITEM_ARTICLE"] = "SKU:";
$MESS['JS_FORMAT_ORDER'] = 'has an invalid format';
$MESS['JS_BASKET_COUNT_TITLE'] = 'In the cart for SUMM';
$MESS['POPUP_VIDEO'] = 'Video';
$MESS['POPUP_GIFT_TEXT'] = 'Found something special? Hint friend';
?>