<? CNext::checkRestartBuffer(); ?>
<? IncludeTemplateLangFile(__FILE__); ?>



<? if (!$isIndex): ?>
    <? if ($isBlog): ?>
        </div> <? // class=col-md-9 col-sm-9 col-xs-8 content-md?>
        <div class="col-md-3 col-sm-3 hidden-xs hidden-sm right-menu-md">
            <div class="sidearea">
                <? $APPLICATION->ShowViewContent('under_sidebar_content'); ?>
                <? CNext::get_banners_position('SIDE', 'Y'); ?>
                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "sect", "AREA_FILE_SUFFIX" => "sidebar", "AREA_FILE_RECURSIVE" => "Y"), false); ?>
            </div>
        </div>
        </div><? endif; ?>
    <? if ($isHideLeftBlock): ?>
        </div> <? // .maxwidth-theme?>
    <? endif; ?>
    </div> <? // .container?>
<? else: ?>
    <? AsproNextCustom::ShowPageType('indexblocks'); ?>
<? endif; ?>



<? CNext::get_banners_position('CONTENT_BOTTOM'); ?>
</div> <? // .middle?>



<? //if(!$isHideLeftBlock && !$isBlog):?>
<? if (($isIndex && $isShowIndexLeftBlock) || (!$isIndex && !$isHideLeftBlock) && !$isBlog): ?>
    </div> <? // .right_block?>
    <? if ($APPLICATION->GetProperty("HIDE_LEFT_BLOCK") != "Y" && !defined("ERROR_404")): ?>
        <div class="left_block">
            <? AsproNextCustom::ShowPageType('left_block'); ?>
        </div>
    <? endif; ?>
<? endif; ?>



<? if ($isIndex): ?>
    </div>
<? elseif (!$isWidePage): ?>
    </div> <? // .wrapper_inner?>
<? endif; ?>



</div> <? // #content?>
<? CNext::get_banners_position('FOOTER'); ?>
</div><? // .wrapper?>
<footer id="footer">
    <? if ($APPLICATION->GetProperty("viewed_show") == "Y" || $is404): ?>
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "basket",
            array(
                "COMPONENT_TEMPLATE" => "basket",
                "PATH" => SITE_DIR . "include/footer/comp_viewed.php",
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "",
                "AREA_FILE_RECURSIVE" => "Y",
                "EDIT_TEMPLATE" => "standard.php",
                "PRICE_CODE" => array(
                    0 => "BASE",
                ),
                "STORES" => array(
                    0 => "",
                    1 => "",
                ),
                "BIG_DATA_RCM_TYPE" => "bestsell"
            ),
            false
        ); ?>
    <? endif; ?>
    <? AsproNextCustom::ShowPageType('footer'); ?>
</footer>
<div class="bx_areas">
    <? AsproNextCustom::ShowPageType('bottom_counter'); ?>
</div>
<? AsproNextCustom::ShowPageType('search_title_component'); ?>
<?
CNext::setFooterTitle();
CNext::showFooterBasket();
?>
<?php
global $DEPORTAMENT;
if(!empty($DEPORTAMENT["DEPORTAMENT"]["UF_JIVOSITE_CONNECT"]) && CUser::GetID() != 30469){
?>
    <!-- BEGIN JIVOSITE CODE {literal} -->
    <script type='text/javascript'>
        (function(){ var widget_id = '<?=$DEPORTAMENT["DEPORTAMENT"]["UF_JIVOSITE_CONNECT"]?>';var d=document;var w=window;function l(){var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
    </script>
    <!-- {/literal} END JIVOSITE CODE -->
<?
}
?>
<?
if(!empty($DEPORTAMENT["DEPORTAMENT"]["UF_SENDPULSE_PUSH"])){
    Bitrix\Main\Page\Asset::getInstance()->addJs('//cdn.sendpulse.com/js/push/'.$DEPORTAMENT["DEPORTAMENT"]["UF_SENDPULSE_PUSH"].'.js');
    //echo '<script charset="UTF-8" src="//cdn.sendpulse.com/js/push/'.$DEPORTAMENT["DEPORTAMENT"]["UF_SENDPULSE_PUSH"].'.js" async></script>';
}
?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter50511952 = new Ya.Metrika2({
                    id: 50511952,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/50511952" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>