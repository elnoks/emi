<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if ($GET["debug"] == "y")
    error_reporting(E_ERROR | E_PARSE);
IncludeTemplateLangFile(__FILE__);
global $APPLICATION, $arRegion, $arSite, $arTheme;
$arSite = CSite::GetByID(SITE_ID)->Fetch();
$htmlClass = ($_REQUEST && isset($_REQUEST['print']) ? 'print' : false);
$bIncludedModule = (\Bitrix\Main\Loader::includeModule("aspro.next")); ?>
    <!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="<?= LANGUAGE_ID ?>"
      lang="<?= LANGUAGE_ID ?>" <?= ($htmlClass ? 'class="' . $htmlClass . '"' : '') ?>>
    <head>
        <title><? $APPLICATION->ShowTitle() ?></title>
        <? $APPLICATION->ShowMeta("viewport"); ?>
        <? $APPLICATION->ShowMeta("HandheldFriendly"); ?>
        <? $APPLICATION->ShowMeta("apple-mobile-web-app-capable", "yes"); ?>
        <? $APPLICATION->ShowMeta("apple-mobile-web-app-status-bar-style"); ?>
        <? $APPLICATION->ShowMeta("SKYPE_TOOLBAR"); ?>
        <? $APPLICATION->ShowHead(); ?>
        <? $APPLICATION->AddHeadString('<script>BX.message(' . CUtil::PhpToJSObject($MESS, false) . ')</script>', true); ?>
        <? if ($bIncludedModule)
            AsproNextCustom::Start(SITE_ID); ?>
    </head>


<body
    class="<?= ($bIncludedModule ? "fill_bg_" . strtolower(AsproNextCustom::GetFrontParametrValue("SHOW_BG_BLOCK")) : ""); ?>"
    id="main">

    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>

<? if (!$bIncludedModule): ?>
    <? $APPLICATION->SetTitle(GetMessage("ERROR_INCLUDE_MODULE_ASPRO_NEXT_TITLE")); ?>
    <center><? $APPLICATION->IncludeFile(SITE_DIR . "include/error_include_module.php"); ?></center></body></html><? die(); ?>
<? endif; ?>

<? $arTheme = $APPLICATION->IncludeComponent("emi:theme.next", ".default", array("COMPONENT_TEMPLATE" => ".default"), false, array("HIDE_ICONS" => "Y")); ?>

<? include_once('defines.php'); ?>

<? AsproNextCustom::SetJSOptions(); ?>

<div
    class="wrapper1 <?= ($isIndex && $isShowIndexLeftBlock ? "with_left_block" : ""); ?> <?= CNext::getCurrentPageClass(); ?> <?= CNext::getCurrentThemeClasses(); ?>">
<? AsproNextCustom::get_banners_position('TOP_HEADER'); ?>

    <div
        class="header_wrap visible-lg visible-md title-v<?= $arTheme["PAGE_TITLE"]["VALUE"]; ?><?= ($isIndex ? ' index' : '') ?>">
        <header id="header">
            <?
            if ($arRegion)
            $bPhone = ($arRegion['PHONES'] ? true : false);
            else
            $bPhone = ((int)$arTheme['HEADER_PHONES'] ? true : false);
            $logoClass = ($arTheme['COLORED_LOGO']['VALUE'] !== 'Y' ? '' : ' colored');
            ?>
            <? /* <div class="header-v7 header-wrapper"> */?>
            <div class="header-wrapper">
                <div class="logo_and_menu-row">
                    <div class="logo-row">
                        <div class="maxwidth-theme col-md-12">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="phone-block with_btn">

                                        <div class="inline-block pull-left regions_padding">
                                            <div class="top-description">
                                                <?
                                                    $APPLICATION->IncludeComponent(
                                                        "BKV:departament_NEW_LOGIK",
                                                        "popup_regions",
                                                        Array()
                                                    );
                                                ?>
                                            </div>
                                        </div>
                                        <? if ($bPhone): ?>
                                            <div class="inner-table-block">
                                                <div class="visible-lg">
                                                    <? AsproNextCustom::ShowHeaderPhones(false, false, $DEPORTAMENT); ?>
                                                    <div class="schedule">
                                                        <? $APPLICATION->IncludeFile(SITE_DIR . "include/header-schedule.php", array(), array("MODE" => "html", "NAME" => GetMessage('HEADER_SCHEDULE'), "TEMPLATE" => "include_area.php")); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif ?>
                                        <? /*
                                            <? if ($arTheme['SHOW_CALLBACK']['VALUE'] == 'Y'): ?>
                                                <div class="inner-table-block">
                                                    <span class="callback-block animate-load twosmallfont colored white btn-default btn"
                                                          data-event="jqm" data-param-form_id="CALLBACK"
                                                          data-name="callback"><?= GetMessage("CALLBACK") ?></span>
                                                </div>
                                            <? endif; ?>
                                        */ ?>
                                    </div>
                                </div>

                                <div class="logo-block col-lg-2 col-md-2 text-center">
                                    <div class="logo<?= $logoClass ?>">
                                        <?=AsproNextCustom::ShowLogo(); ?>
                                    </div>
                                </div>
                                <div class="right-icons pull-right">
                                    <div class="pull-right show-fixed">
                                        <div class="wrap_icon">
                                            <button class="top-btn inline-search-show twosmallfont">
                                                <?= CNext::showIconSvg("search big", SITE_TEMPLATE_PATH . "/images/svg/search_emi.svg"); ?>
                                            </button>
                                        </div>
                                    </div>
                                    <?/*
                        <div class="pull-right block-link">
                            <?= CNext::ShowBasketWithCompareLink('', 'big', true, 'wrap_icon wrap_basket baskets',"Y","Y"); ?>
                        </div>
                        */?>
                                    <div class="pull-right">
                                        <div class="wrap_icon wrap_cabinet">
                                            <?= AsproNextCustom::showCabinetLink(true, true, 'big', true); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><? // class=logo-row?>
                </div>

                <div class="header-v2 header-wrapper long">
                    <div class="maxwidth-theme">
                        <div class="logo_and_menu-row">
                            <div class="row">
                                <div class="col-md-12 menu-row">
                                    <div class="right-icons right-icons-menu pull-right">
                                        <div class="pull-right">
                                            <?=AsproNextCustom::ShowBasketWithCompareLink('', 'big', '', 'wrap_icon wrap_basket baskets');?>
                                        </div>
                                    </div>
                                    <div class="menu-only">
                                        <nav class="mega-menu sliced">

                                            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                                                array(
                                                    "COMPONENT_TEMPLATE" => ".default",
                                                    "PATH" => SITE_DIR."include/menu/menu.top.php",
                                                    "AREA_FILE_SHOW" => "file",
                                                    "AREA_FILE_SUFFIX" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y",
                                                    "EDIT_TEMPLATE" => "include_area.php"
                                                ),
                                                false, array("HIDE_ICONS" => "Y")
                                            );?>
                                        </nav>
                                    </div>
                                </div>
                            </div>

                        </div><?// class=logo-row?>
                    </div>
                    <div class="line-row visible-xs"></div>
                </div>

                <? /*
    <div class="menu-row middle-block bg<?= strtolower($arTheme["MENU_COLOR"]["VALUE"]); ?> sliced">
        <div class="maxwidth-theme">
            <div class="row">
                <div class="col-md-12">
                    <div class="right-icons pull-right">
                        <div class="pull-right">
                            <?=CNext::ShowBasketWithCompareLink('', '', false, 'wrap_icon inner-table-block');?>
                        </div>
                    </div>
                    <div class="menu-only">
                        <nav class="mega-menu sliced">
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                                array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "PATH" => SITE_DIR . "include/menu/menu.top.php",
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "",
                                    "AREA_FILE_RECURSIVE" => "Y",
                                    "EDIT_TEMPLATE" => "include_area.php"
                                ),
                                false, array("HIDE_ICONS" => "Y")
                            ); ?>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    */ ?>

                <div class="line-row visible-xs"></div>
            </div>
        </header>
    </div>

<? AsproNextCustom::get_banners_position('TOP_UNDERHEADER'); ?>

    <div id="headerfixed">
        <? @include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/page_blocks_emi/header_fixed_2.php'; ?>
    </div>

    <div id="mobileheader" class="visible-xs visible-sm">

        <? AsproNextCustom::ShowPageType('header_mobile'); ?>
        <div id="mobilemenu"
             class="<?= ($arTheme["HEADER_MOBILE_MENU_OPEN"]["VALUE"] == '1' ? 'leftside' : 'dropdown') ?>">
            <? AsproNextCustom::ShowPageType('header_mobile_menu'); ?>
        </div>
    </div>
<? /*filter for contacts*/


if ($arRegion) {
    if ($arRegion['LIST_STORES'] && !in_array('component', $arRegion['LIST_STORES'])) {
        if ($arTheme['STORES_SOURCE']['VALUE'] != 'IBLOCK')
            $GLOBALS['arRegionality'] = array('ID' => $arRegion['LIST_STORES']);
        else
            $GLOBALS['arRegionality'] = array('PROPERTY_STORE_ID' => $arRegion['LIST_STORES']);
    }
}

if ($isIndex) {
    $GLOBALS['arrPopularSections'] = array('UF_POPULAR' => 1);
    $GLOBALS['arrFrontElements'] = array('PROPERTY_SHOW_ON_INDEX_PAGE_VALUE' => 'Y');
} ?>

<div class="wraps hover_<?= $arTheme["HOVER_TYPE_IMG"]["VALUE"]; ?>" id="content">
<? if (!$is404 && !$isForm && !$isIndex): ?>
    <? $APPLICATION->ShowViewContent('section_bnr_content'); ?>
    <? if ($APPLICATION->GetProperty("HIDETITLE") !== 'Y'): ?>
        <!--title_content-->
        <? AsproNextCustom::ShowPageType('page_title'); ?>
        <!--end-title_content-->
    <? endif; ?>
    <? $APPLICATION->ShowViewContent('top_section_filter_content'); ?>
<? endif; ?>

<? if ($isIndex): ?>
    <div class="wrapper_inner front <?= ($isShowIndexLeftBlock ? "" : "wide_page"); ?>">
    <? elseif (!$isWidePage): ?>
    <div class="wrapper_inner <?= ($isHideLeftBlock ? "wide_page" : ""); ?>">
<? endif; ?>

<? if (($isIndex && $isShowIndexLeftBlock) || (!$isIndex && !$isHideLeftBlock) && !$isBlog): ?>
    <div
        class="right_block <?= (defined("ERROR_404") ? "error_page" : ""); ?> wide_<?=CNext::ShowPageProps("HIDE_LEFT_BLOCK"); ?>">
<? endif; ?>
<div class="middle <?= ($is404 ? 'error-page' : ''); ?>">
<? // AsproNextCustom::get_banners_position('CONTENT_TOP'); ?>
<? if (!$isIndex): ?>
    <div class="container">
    <? //h1?>
    <? if ($isHideLeftBlock && !$isWidePage): ?>
    <div class="maxwidth-theme">
    <? endif; ?>
    <? if ($isBlog): ?>
    <div class="row">
    <div class="col-md-9 col-sm-12 col-xs-12 content-md <?=CNext::ShowPageProps("ERROR_404"); ?>">
<? endif; ?>
<? endif; ?>
<? CNext::checkRestartBuffer(); ?>