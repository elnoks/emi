<?
header("Content-Type: application/x-javascript");
$hash = "bx_random_hash";
$config = array("appmap" =>
	array("main" => "app_test",
		"left" => "/app_test/left.php",
		"right" => "/app_test/right.php",
		"settings" => "/app_test/settings.php",
		"hash" => substr($hash, rand(1, strlen($hash)))
	)
);
echo json_encode($config);
?>