<?
define("STOP_STATISTICS", true);
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/italypayment.txt"); //log

AddMessage2Log("IP: " . $_SERVER['REMOTE_ADDR']);

CModule::IncludeModule("sale");
if(array_key_exists("orderid", $_REQUEST) && array_key_exists("rcode", $_REQUEST)){
	$orderNumber = $_REQUEST["orderid"];
	$rcode = $_REQUEST["rcode"];
	
	if($rcode == 'OK') {
		CSaleOrder::PayOrder($orderNumber, "Y", True, True, 0, array("PAY_VOUCHER_NUM" => "556"));
	}
	
	header('Location: ' . 'https://emischool.it/personal/order/detail', true, 301);
	exit();
}
?>