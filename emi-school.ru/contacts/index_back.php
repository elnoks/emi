<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
if (LANGUAGE_ID != "ru") {
    $APPLICATION->SetPageProperty("description", "Контакты. Школа Екатерины Мирошниченко проводит курсы по ногтевому дизайну. Также предлагаем большой выбор гель лаков и гелевых красок для ногтей. Наш телефон в Москве: +7(985)448-60-58");
}
$APPLICATION->SetTitle(GetMessage("CONTACTS_PAGE_META_TITLE"));
?>		<div class="h1-top">
    <h1><?=GetMessage("CONTACTS_PAGE_META_TITLE")?></h1>
</div>
    <div class="row contacts-block">
        <?if(($GLOBALS["partnerSchool"]->haveSchoolPartner) && ($GLOBALS["partnerSchool"]->getAddress() || $GLOBALS["partnerSchool"]->getPhones())):?>
            <div class="col-md-6">
                <h2><?=GetMessage("CONTACTS_PAGE_BLOCKS_SCHOOL_TITLE")?></h2>
                <div class="border-all">
                    <?if($GLOBALS["partnerSchool"]->getAddress()):?>
                        <span class="bold"><?=GetMessage("CONTACTS_PAGE_BLOCKS_SCHOOL_ADR")?>: </span><span itemprop="streetAddress"><?=$GLOBALS["partnerSchool"]->getAddress();?></span><br>
                    <?endif;?>
                    <?if($GLOBALS["partnerSchool"]->getPhones()):?>
                        <span class="bold"><?=GetMessage("CONTACTS_PAGE_BLOCKS_SCHOOL_TEL")?>: </span><a href="tel:<?=preg_replace("/[^0-9]/","", strip_tags($GLOBALS["partnerSchool"]->getPhones()));?>"><span itemprop="telephone"><?=$GLOBALS["partnerSchool"]->getPhones();?></span></a>
                    <?endif;?>
                </div>
            </div>
        <?endif;?>
        <?if($GLOBALS["partnerShop"]->getAddress() || $GLOBALS["partnerShop"]->getPhones()):?>
            <div class="col-md-6">
                <h2><?=GetMessage("CONTACTS_PAGE_BLOCKS_SHOP_TITLE")?></h2>
                <div class="border-all">
                    <?if($GLOBALS["partnerShop"]->getAddress()):?>
                        <span class="bold"><?=GetMessage("CONTACTS_PAGE_BLOCKS_SHOP_ADR")?>: </span><span itemprop="streetAddress"><?=$GLOBALS["partnerShop"]->getAddress();?></span><br>
                    <?endif;?>
                    <?if($GLOBALS["partnerShop"]->getPhones()):?>
                        <span class="bold"><?=GetMessage("CONTACTS_PAGE_BLOCKS_SHOP_TEL")?>: </span><a href="tel:<?=preg_replace("/[^0-9]/","", strip_tags($GLOBALS["partnerShop"]->getPhones()));?>"><span itemprop="telephone"><?=$GLOBALS["partnerShop"]->getPhones();?></span></a>
                    <?endif;?>
                </div>
            </div>
        <?endif;?>
    </div>

<?if (LANGUAGE_ID != "ru"){
    $APPLICATION->IncludeComponent(
        "ithive:offices",
        "salon-emi",
        array(
            "KEY" => "AJx6J1UBAAAAY0UMSQIA-7-PSRugSDu-fBl9UAlk8zQeUBoAAAAAAAAAAADRFI8DCuQ1FX0n7T1DHV-L-6UQwg==",
            "ICON_FILE" => "/bitrix/components/ithive/offices.list/templates/.default/images/icon_arrow_5.png",
            "ICON_SIZE" => "36, 48",
            "ICON_OFFSET" => "-15,-30",
            "INCLUDE_JQUERY" => "N",
            "DISPLAY_DATE" => "N",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "DISPLAY_AS_RATING" => "rating",
            "TAGS_CLOUD_ELEMENTS" => "150",
            "PERIOD_NEW_TAGS" => "",
            "FONT_MAX" => "50",
            "FONT_MIN" => "10",
            "COLOR_NEW" => "3E74E6",
            "COLOR_OLD" => "C0C0C0",
            "TAGS_CLOUD_WIDTH" => "100%",
            "SEF_MODE" => "N",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => "contacts",
            "IBLOCK_ID" => "23",
            "NEWS_COUNT" => "2000",
            "USE_SEARCH" => "N",
            "USE_RSS" => "N",
            "USE_RATING" => "N",
            "USE_CATEGORIES" => "N",
            "USE_REVIEW" => "N",
            "USE_FILTER" => "Y",
            "SHOW_FILTER" => "N",
            "SORT_BY1" => "SORT",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "CHECK_DATES" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "250",
            "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
            "LIST_FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "LIST_PROPERTY_CODE" => array(
                0 => "NAME_SHORT",
                1 => "ADDRESS",
                2 => "PHONES",
                3 => "",
            ),
            "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
            "DISPLAY_NAME" => "Y",
            "META_KEYWORDS" => "-",
            "META_DESCRIPTION" => "-",
            "BROWSER_TITLE" => "-",
            "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
            "DETAIL_FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "DETAIL_PROPERTY_CODE" => array(
                0 => "",
                1 => "",
            ),
            "DETAIL_DISPLAY_TOP_PAGER" => "N",
            "DETAIL_DISPLAY_BOTTOM_PAGER" => "N",
            "DETAIL_PAGER_TITLE" => "Страница",
            "DETAIL_PAGER_TEMPLATE" => "",
            "DETAIL_PAGER_SHOW_ALL" => "N",
            "DISPLAY_PANEL" => "N",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "USE_PERMISSIONS" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "N",
            "DISPLAY_TOP_PAGER" => "Y",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "FILTER_NAME" => "arrFilter",
            "FILTER_FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "FILTER_PROPERTY_CODE" => array(
                0 => "PRODUCT_TYPE",
                1 => "CITY",
                2 => "",
            ),
            "AJAX_OPTION_SHADOW" => "Y",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "N",
            "AJAX_OPTION_HISTORY" => "N",
            "SEF_FOLDER" => "/offices/",
            "AJAX_OPTION_ADDITIONAL" => "",
            "VARIABLE_ALIASES" => array(
                "SECTION_ID" => "SECTION_ID",
                "ELEMENT_ID" => "ELEMENT_ID",
            )
        ),
        false
    );
}?>


<?
if (LANGUAGE_ID == "ru"){
    echo <<<END
<h3>ОФИЦИАЛЬНЫЕ ПРЕДСТАВИТЕЛЬСТВА ШКОЛЫ НОГТЕВОГО ДИЗАЙНА ЕКАТЕРИНЫ МИРОШНИЧЕНКО И ОФИЦИАЛЬНЫЕ ДИСТРИБЬЮТОРЫ МАРКИ E.Mi в РОССИИ:</h3>
<br><td><a href="https://emi-school.ru/contacts/?new_region=2637">Главный офис в России и СНГ г. Ростов-на-Дону, пер. Халтуринский, 160/102</a>, тел. <a href="tel:+7 (863) 300-26-00">+7 (863) 300-26-00</a>, <a href="tel:+7 (928) 229-28-09">+7 (928) 229-28-09</a></td>
<br><td> </td>

<br><td><a href="https://emi-school.ru/?new_region=2608">г. Брянск, ул. Комарова, д. 59</a>, т. <a href="tel:+7 (900) 694-01-46">+7 (900) 694-01-46</a>, <a href="tel:+7 (4832) 42-02-63">+7 (4832) 42-02-63</a></td>
<br><td> </td>

<br><td><a href="https://emi-school.ru/?new_region=102411">г. Владивосток, Проспект 100-летия Владивостока 117а</a>, т. <a href="tel:270-00-17">270-00-17</a>, <a href="tel:8-914-790-00-17">8-914-790-00-17</a> </td>

<br><td> </td>

<br><td><a href="https://emi-school.ru/?new_region=2609">г. Владимир, ул. Горького, 27</a>, т. <a href="tel:+7 (930) 220-11-22">+7 (930) 220-11-22</a> </td>
<br><td> </td>

<br><td><a href="https://emi-school.ru/?new_region=2610">г. Волгоград, ул. Донецкая, 16А, оф.2</a>, т. <a href="tel:+7 (8442) 43-55-66">+7 (8442) 43-55-66</a>, <a href="tel:+7 (902) 311-91-35">+7 (902) 311-91-35</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2611">г. Вологда, ул. Предтеченская, 72</a>, т. <a href="tel:+7 (900) 558-03-01">+7 (900) 558-03-01</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2612">г. Воронеж, ул. Плехановская, 30, оф. 2</a>, т.  <a href="tel: +7 (473) 300-33-08">+7 (473) 300-33-08</a>, <a href="tel:+7 (4732) 95-07-70">+7 (4732) 95-07-70</a>, <a href="tel:+7 (473) 290-71-71">+7 (473) 290-71-71</a>, <a href="tel:+7 (952) 956-01-71">+7 (952) 956-01-71</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=69491">г. Екатеринбург, ул. Фронтовых Бригад, 7</a>, т. <a href="tel:+7 (343) 201-02-10">+7 (343) 201-02-10</a>, <a href="tel:+7 (952) 730-05-03">+7 (952) 730-05-03</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2642">г. Екатеринбург, ул. Викулова, 32</a>, т. <a href="tel:8-982-65-80-777">8-982-65-80-777</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2613">г. Иваново, ул. Куконковых д. 76</a>, т. <a href="tel:+79023183208">+7 902 318-32-08, </a><a href="tel:+79203585969">+7 920 358-59-69</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2614">г. Иркутск, 6-ая Советская 18А/1, остановка "К/т Баргузин"</a>, т. <a href="tel:+7 (3952) 60-50-59">+7 (3952) 60-50-59</a>, 62-02-70, 8 902 513-02-70, 8 902 510-50-59</td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2592">г. Казань, ул. Чистопольская, 73</a>, т. <a href="tel:+7 (843) 203-48-03">+7 (843) 203-48-03</a>, <a href="tel:+7 (843) 203-96-05">+7 (843) 203-96-05</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2615">г. Калининград, ул. Комсомольская, 85</a>, т. WhatsApp. Viber <a href="tel:+7 (929) 167-00-62">+7 (929) 167-00-62</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2616">г. Калуга, переулок Теренинский, 9</a>, т. <a href="tel:8-965-706-34-34">8-965-706-34-34</a></td>, <a href="tel:8-962-171-47-01">8-962-171-47-01</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=50745">г. Кемерово, ул. 50 лет Октября 32</a>, т. <a href="tel:+7 (903) 068-66-04">+7 (903) 068-66-04</a>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=3211">г. Краснодар, ул. Яна Полуяна, 39, оф. 1</a>, т. <a href="tel:+7 (988) 240-88-33">+7 (988) 240-88-33</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2620">г. Кострома, ул. Советская, 111</a>, т. <a href="tel:+7 (4942) 30-09-40">+7 (4942) 30-09-40</a>, <a href="tel:+7 (920) 641-94-24">+7 (920) 641-94-24</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=60143">г. Красноярск, ул. Ленина, д. 104</a>, т. <a href="tel:+7 (950) 977-67-56">+7 (950) 977-67-56</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2624">г. Липецк, ул. Теперика, д.1</a>, т. <a href="tel:+7 (951) 308-75-68">+7 (951) 308-75-68</a></td> <!--- г. Липецк, ул. Плеханова,50, оф. 404 --->
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2625">г. Магадан, ул. Октябрьская, 17, гост. «УКРАИНА»</a>, т. <a href="tel:+7 (914) 852-88-82">+7 (914) 852-88-82</a>, <a href="tel:+7 (914) 851-18-11">+7 (914) 851-18-11</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=50380">г. Москва (ЮАО), ОФИЦИАЛЬНЫЙ ДИСТРИБЬЮТОР, Варшавское ш., 39, оф. 322, м. Нагатинская,</a> т. +7 (495) 781-72-12, +7 (916) 579-59-57, +7 (915) 149-59-46</a>, <a href="tel: т. +7 (495) 781-72-12, +7 (916) 579-59-57, +7 (915) 149-59-46</a>, <a href="tel: т. +7 (495) 781-72-12, +7 (916) 579-59-57, +7 (915) 149-59-46</a></td> 
<br><td> </td>

<br><td><a href="https://emi-school.ru/?new_region=50380">г. Москва (ЦАО) ОФИЦИАЛЬНЫЙ ДИСТРИБЬЮТОР, м. Цветной бульвар, пер. Средний Каретный, д. 4, оф. 1 </a>, <a href="tel:+74956940012">т. +7(495) 694-00-12</a>, <a href="tel:+74956509439">т. +7(495) 650-94-39</a>, <a href="tel:+79850587163">т. +7(985) 058-71-63</a></td> 
<br><td> </td>

<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=50380">г. Москва, ОФИЦИАЛЬНОЕ ПРЕДСТАВИТЕЛЬСТВО ШКОЛЫ В ЦАО, Бизнес-Центр ТАГАНСКИЙ, ул. Марксистская, д. 3, стр. 5, м. Марксистская</a>,Телефон: <a href="tel:+7 (915) 167-67-63">+7 (915) 167-67-63</a>, <a href="tel:+7 (964) 775-22-20">+7 (964) 775-22-20</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2628">г. Нижний Новгород, ул. Бетанкура, 6</a>, т. <a href="tel:+7 (915) 935-32-30">+7 (915) 935-32-30</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2618">г. Новокузнецк, ул. Кирова 25А</a>, т. <a href="tel:+7 (3843) 91-06-61">+7 (3843) 91-06-61</a>, <a href="tel:+7 (905) 909-66-04">+7(905) 909-66-04</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2630">г. Новосибирск, ул. Фрунзе 80, цокольный этаж</a>, т. <a href="tel:+79139201741">+7 (913) 920-17-41</a>, <a href="tel:+7 (383) 200-40-38">+7 (383) 200-40-38</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2630">г. Новый Уренгой, мкр. Олимпийский. 1, Дворец спорта "Звездный"</a>, т. <a href="tel:89084970434">+7 (908) 497-04-34</a>, 26-04-34</td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2631">г. Омск, ул.Лермонтова, 62</a>, т. <a href="tel:+7 (913) 650-07-56">+7 (913) 650-07-56</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2632">г. Оренбург, ул. Салмышская, 36</a>, т. <a href="tel:+7 (3532) 26-62 06">+7 (3532) 26-62 06</a>, <a href="tel:+7 (903) 366-62-06">+7 (903) 366-62-06</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=48252">г. Орехово-Зуево, ул. Мадонская, 12 б</a>, т. <a href="tel:+7 (915) 065-43-21">+7 (915) 065-43-21</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2634">г. Пенза, ул. Суворова, 184а</a>, т. <a href="tel:+7 (927) 289-42-33">+7 (927) 289-42-33</a>, <a href="tel:+7 (8412) 39-42-33">+7 (8412) 39-42-33</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2635">г. Пермь, ул. Островского, 30</a>, т. <a href="tel:+7 (342) 259-69-45">+7 (342) 259-69-45</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2617">г. Петропавловск-Камчатский, Туристический проезд, 12, офис 4</a>, т. <a href="tel:+7 (902) 464-81-44">+7 (902) 464-81-44</a>, 48-81-44</td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2602">г. Пятигорск, ул. Московская 99</a>, т. <a href="tel:+7 (928) 982-64-38">+7 (928) 982-64-38, +7 (928) 652-86-37</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2638">г. Рязань, ул. Вокзальная, 61</a>, т. <a href="tel: 8(4912)70-09-45">, +79155900600</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2639">г. Самара, ул. Нагорная, 143</a>, т. <a href="tel:+7 (937) 992-52-10">+7 (937) 992-52-10</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2623">г. Санкт-Петербург, ОФИЦИАЛЬНЫЙ ДИСТРИБЬЮТОР, м. Сенная площадь, ул. Казанская, 45</a>, т. <a href="tel:+7 (812) 923-62-09">+7 (812) 923-62-09</a>, <a href="tel:+7 (911) 923-62-09">+7 (911) 923-62-09</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2623">г. Санкт-Петербург, ОФИЦИАЛЬНОЕ ПРЕДСТАВИТЕЛЬСТВО ШКОЛЫ, Каменноостровский пр., 12</a>, т. <a href="tel:+7 (921) 882-07-48">+7 (921) 882-07-48</a>, <a href="tel:+7 (981) 192-97-03">+7 (981) 192-97-03</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2589">г. Саранск, ул. Волгоградская, 60/3</a>, т. <a href="tel:+7 (927) 276-28-20">+7 (927) 276-28-20</a>, <a href="tel:+7 (8342) 30-28-20">+7 (8342) 30-28-20</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2640">г. Саратов, ул. ул. Бахметьевская, д. 34/42</a>, т. <a href="tel:+7 (8452)46-20-12">+7 (8452) 46-20-12</a>, <a href="tel:+7 (927) 226-20-12">+7 (927) 226-20-12</a>, <a href="tel:+7 (927) 223-35-30">+7 (927) 223-35-30</a></td> 
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2660">г. Севастополь, ул. Рыбаков, 5а</a>, т. <a href="tel:+7 (978) 954-08-88">+7 (978) 954-08-88</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=70828">г. Симферополь, ул. Куйбышева, 2</a>, Офис т. <a href="tel:+7 (978) 954-33-90">+7 (978) 954-33-90</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2599">г. Сочи, пр. Курортный, 16, офисный центр «Мелодия», 4-й эт.</a>, т. <a href="tel:+7 (988) 237-96-22">+7 (988) 237-96-22</a></td>
<br><td> </td>

<br><td><a href="https://emi-school.ru/?new_region=2657">г. Сургут, ул. Киртбая, д. 18</a>, т. <a href="tel:+7 (922) 406-56-37">+7 (922) 406-56-37</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2644">г. Тамбов, ул. Орехова 4</a>, т. <a href="tel:+7 (920) 232-51-25">+7 (920) 232-51-25</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2646">г. Томск, ул. Трифонова, 20, офис 100</a>, т. <a href="tel:+7 (3822) 21-30-25">+7 (3822) 21-30-25</a>, <a href="tel:+7 (3822) 21-15-32">+7 (3822) 21-15-32</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2648">г. Тюмень, ул. Пермякова, 65/1</a>, т. <a href="tel:+7 (904) 494-43-40">+7 (904) 494-43-40</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2649">г. Ульяновск, пер. Комсомольский, 6</a>, т. <a href="tel:+7 (8422) 58-85-22">+7 (8422) 58-85-22</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2578">г. Уфа, ул. 50 лет СССР, дом 29 </a>, т. <a href="tel:+7 (987) 595-95-55">+7 (987) 595-95-55</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2603">г. Хабаровск, ул. Войкова, 8</a>, т. <a href="tel:+7 (4212)62-63-39">+7 (4212)62-63-39</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2650">г. Челябинск, Комсомольский проспект, 69</a>, т. <a href="tel:+7 (982) 368-37-32">+7 (982) 368-37-32</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2651">г. Чита, ул. Бутина 115</a>, т. <a href="tel:+7 (3022) 21-00-29">+7 (3022) 21-00-29</a>; т. <a href="tel:+7 (929) 483-03-33">+7 (929) 483-03-33</a>, <a href="tel:+7 (929) 483-33-33">+7 (929) 483-33-33</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=70836">г. Ярославль, ул. Чайковского, 30</a>, т. <a href="tel:+7 (4852) 66-23-86">+7 (4852) 66-23-86</a>, <a href="tel:+7 (965) 726-23-86">+7 (965) 726-23-86</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2577">РЕСПУБЛИКА АДЫГЕЯ г. Майкоп, ул. Пролетарская, 334, 2-й эт., ТЦ «Пирамида», оф. 203</a>, т. <a href="tel:+7 (928) 473-77-47">+7 (928) 473-77-47</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2581">РЕСПУБЛИКА ДАГЕСТАН г. Махачкала, пр-т, Имама Шамиля, 15а</a>, т. <a href="tel:+7 (928) 979-11-00">+7 (928) 979-11-00</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2583">РЕСПУБЛИКА КАБАРДИНО-БАЛКАРИЯ г. Нальчик, ул. Пачева, 13, ТОД «ЕВРОПА», 2-й эт., оф. 2</a>, т. <a href="tel:+7 (928) 702-94-22">+7 (928) 702-94-22</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2660">РЕСПУБЛИКА КРЫМ г. Севастополь, ул. Рыбаков, 5а, офис №0115</a>, т. <a href="tel:+7 (978) 954-08-88">+7 (978) 954-08-88</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=70828">РЕСПУБЛИКА КРЫМ г. Симферополь, ул. Куйбышева, 2, офис 4</a>, т. <a href="tel:+7 (978) 954-33-90">+7 (978) 954-33-90</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2591">РЕСПУБЛИКА СЕВЕРНАЯ ОСЕТИЯ (АЛАНИЯ) г. Владикавказ,  ул. Барбашова 15</a>, т. <a href="tel:+7 (918) 827-55-55">+7 (918) 827-55-55</a>, <a href="tel:+7 (8672) 441061, ">+7 (8672) 441061, </a><a href="tel:+7(8672)29-35-55">+7(8672) 29-35-55</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2590">РЕСПУБЛИКА САХА - ЯКУТИЯ г. Якутск,  ул. Богдана Чижика 2/2</a>, т. <a href="tel:+7 (4112) 26-26-80">+7 (4112) 26-26-80</a>, <a href="tel:+7 (924) 766-26-80">+7 (924) 766-26-80</a></td>
<br><td> </td>
<br><td><a href="https://emi-school.ru/?new_region=2594">РЕСПУБЛИКА УДМУРТИЯ г. Ижевск, ул. Лихвинцева, 50 «А»</a>, т. <a href="tel:+7-912-444-66-44">+7-912-444-66-44, </a><a href="tel:+7 (3412) 970-029">+7 (3412) 970-029</a></td>

<br><h3>ОФИЦИАЛЬНЫЕ ПРЕДСТАВИТЕЛЬСТВА ШКОЛЫ НОГТЕВОГО ДИЗАЙНА ЕКАТЕРИНЫ МИРОШНИЧЕНКО И ОФИЦИАЛЬНЫЕ ДИСТРИБЬЮТОРЫ МАРКИ E.Mi в СНГ:</h3>


<br><td>КЫРГЫЗСКАЯ РЕСПУБЛИКА г. Бишкек, ул. Токтогула, 213, т.<a href="tel:+9 (965) 559-768-05">+9 (965) 559-768-05</a></td>
<br><td> </td>
<br><td><a href="https://emi-shop.by/">РЕСПУБЛИКА БЕЛАРУСЬ г. Минск, ул. Скрыганова, 4д</a>, т. <a href="tel:+37 (529) 334-70-76">+37 (529) 334-70-76</a></td>

<h4>КАЗАХСТАН:</h4>
<td><a href="https://emi-school.kz/">г. Астана, ул. Иманова, 19, Деловой Дом «Алма-Ата», оф. 604D</a>, т. <a href="tel:+7 (7172) 46-69-36">+7 (7172) 46-69-36</a>, <a href="tel:+7 (777) 182-32-14">+7 (777) 182-32-14</a>, <a href="tel:+7 (747) 383-84-34">+7 (747) 383-84-34</a></td>
<br><td><a href="https://emi-school.kz/">г. Алматы, пр. Абая 150/230, помещение 821</a>, т. <a href="tel:+7 (727) 228-20-27">+7 (727) 228-20-27</a>, <a href="tel:+7 (778) 111-56-20">+7 (778) 111-56-20</a></td>
<br><td> </td>

<br><td><a href="https://emischool.md/">Республика Молдова, г.Кишинев, ул. Мирон Костин 19/6 11а</a>, т.<a href="tel:+37368900327">+37368900327, <a href="tel:+37368741417">+37368741417</a></a></td>
<br><td> </td>

<h4>УКРАИНА:</h4>
<td><a href="https://emi.ua/">г. Днепропетровск, ул. Набережная Ленина, оф. 27</a>, т. <a href="tel:+38 (067) 5604101">+38 (067) 5604101</a></td>
<br><td><a href="https://emi.ua/">г. Киев, ул. Кирилловская 160 (корпус Б)</a>, т. <a href="tel:+38 (099) 6210510">+38 (099) 6210510</a>, (098) 1770609</td>
<br><td><a href="https://emi.ua/">г. Кривой Рог, ул. Косиора, 4, оф. 16</a>, т. <a href="tel:+38 (098) 1245454">+38 (098) 1245454</a></td>
<br><td><a href="https://emi.ua/">г. Полтава, ул. Ленина, 124</a>, т. <a href="tel:">+ 38 (099) 7058979</td>
<br><td><a href="https://emi.ua/">г. Ровно, ул. Гурьева, 13</a>, т. <a href="tel:066 50 68715">066 50 68715</a>, <a href="tel:096 196 7730">096 196 7730</a></td>
<br><td><a href="https://emi.ua/">г. Харьков, пл. Защитников Украины 1 (пл. Восстания, 1)</a>, т. <a href="tel:+38 (073) 0003286">+38 (073) 0003286</a></td>
<br><td><a href="https://emi.ua/">г. Черновцы, ул. Русская, 1</a>, т. <a href="tel:+38 (050) 5407868">+38 (050) 5407868</a>, <a href="tel:+38 (068) 2269365">+38 (068) 2269365</a></td><br>

<h4>ДОНЕЦКАЯ НАРОДНАЯ РЕСПУБЛИКА</h4>
<td><a href="https://dnr.emi-school.ru/">г. Донецк, ул. Университетская, 78, оф. 86</a>, т. <a href="tel:+38 (050) 3479784">+38 (050) 3479784</a>, <a href="tel:+38 (098) 5450061">+38 (098) 5450061</a></td>

<br><h3>ОФИЦИАЛЬНЫЕ ПРЕДСТАВИТЕЛЬСТВА ШКОЛЫ НОГТЕВОГО ДИЗАЙНА ЕКАТЕРИНЫ МИРОШНИЧЕНКО И ОФИЦИАЛЬНЫЕ ДИСТРИБЬЮТОРЫ МАРКИ E.Mi В ДРУГИХ СТРАНАХ МИРА:</h3>

<br><td><a href="https://emischool.com/">Главный офис в Европе: Stefanikova 203/23, Praha 5 -Smichov, 15000</a>. Tel. <a href="tel:+420 773 208 276">+420 773 208 276</a>, prague@emischool.com, post@emischool.com</td>
<br><td> </td>
<br><td><a href="https://de.emischool.com/">ГЕРМАНИЯ Kirschblutenstr, 35, 65201 - Wiesbaden</a>. Tel. <a href="tel:+49 17631423222 ">+49 17631423222 </a>, <a href="tel:+49 0611/51051428 ">+49 0611/51051428</a>, germany@emischool.com</td>
<br><td> </td>
<br><td>ГРЕЦИЯ Nikolaou Plastira 97, Aigaleo, shop 1, Athens. Tel. <a href="tel:+30 6942680600">+30 6942680600</a>, greece-school@emischool.com</td>
<br><td> </td>
<br><td>ИЗРАИЛЬ 2 Habonim st., South Industrial Zone, Netanya. Tel. <a href="tel:+972 50 9200011">+972 50 9200011</a>, israel-school@emischool.com</td>
<br><td> </td>
<br><td>ИСПАНИЯ c/Huerta Vieja 8, Paradas, Sevilla. Tel. +34 696 753 883, emischoolandalucia@gmail.com</td>
<br><td> </td>
<br><td><a href="https://emischool.it/">ИТАЛИЯ VIA RESTELLI2/B 21013 GALLARATE (VARESE)</a>. Tel. 0331/701442, italy-school@emischool.com</td>
<br><td> </td>
<br><td><a href="https://emischool.cy/">КИПР 2 EFESSOU street, Amathus Area, Limassol, Emerald coast building. Shop 2</a>. Tel.<a href="tel:+357 964 966 45">+357 964 966 45</a>, cyprus@emischool.com</td>
<br><td> </td>
<br><td>ЛАТВИЯ Kr.Barona 93, Riga, Latvia, LV-1001. Tel. <a href="tel:+37122400008">+37122400008</a></td>
<br><td> </td>
<br><td><a href="https://emischool.lt/">ЛИТВА Taikos pr. 4a, LT-91229 - Klaipeda</a>. Tel. <a href="tel:+370 46 310712">+370 46 310712</a>, lithuania@emischool.com</td>
<br><td> </td>
<br><td><a href="https://emischool.ae/">ОАЭ Prime Business Tower, office 1502A, Jumeirah Village Circle 1, Dubai</a>. Tel. <a href="tel:+971 527 33 32 87">+971527333287</a> uae-sale@emischool.com</td>
<br><td> </td>
<br><td><a href="https://emischool.pt/">ПОРТУГАЛИЯ Av. Defensores de Chaves, 1-B, 1990-612 Lisboa</a>. Tel. <a href="tel:+351 218269255">+351 218269255</a>, <a href="tel:+351 918037777">+351 918037777</a>, portugal-school@emischool.com</td>
<br><td> </td>
<br><td>РУМЫНИЯ Cluj-napoca, str. Trifoiului nr. 16, bl. H4, ap. 5, jud. Cluj, Romania. Tel. <a href="tel:+407 567 92 111">+407 567 92 111</a>, <a href="tel:+40 364 418 056">+40 364 418 056</a>, romania-school@emischool.com</td>
<br><td> </td>
<br><td><a href="https://emischool.fr/">ФРАНЦИЯ 151 rue Petin Gaudet, 42400 St Chamond</a>. Tel. <a href="tel:+33 4 27 77 34 75">+33 4 27 77 34 75</a>, <a href="tel:+33 6 19 57 02 83">+33 6 19 57 02 83</a>, <a href="tel:+33 6 23 73 32 90">+33 6 23 73 32 90</a>, france-school@emischool.fr</td>
<br><td> </td>
<br><td>ШВЕЙЦАРИЯ General Dufour Passage 12, 2502 Biel. Tel. <a href="tel:+41 (0) 323237232">+41 (0) 323237232</a>, switzerland-school@emischool.com</td>
<br><td> </td>
<br><td><a href="https://emischool.ee/">ЭСТОНИЯ Pärnu mnt. 139A/1, Tallinn</a>. Tel. <a href="tel:+372 5800 7154">+372 5800 7154</a>, estonia-school@emischool.com</td>
<br><td> </td>
<br><td><a href="https://emischool.co.za/">ЮАР 23 Loretha street, Van Riebeeck, Park. Kempton, Park. Gauteng</a>. Tel. <a href="tel:+2711 3932 791">+2711 3932 791</a>, southafrica@emischool.com</td>
<br><td> </td>
<br><td><a href="https://kr.emischool.com/">ЮЖНАЯ КОРЕЯ 3F,46-16, Namchang-Dong, Jung-Gu-Seoul</a>. Tel. <a href="tel:+82 10 3686 8631">+82 10 3686 8631</a>, south-korea@emischool.com</td>
<br><td> </td>
<br><td> </td>
END;
}
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

<?
//<br><td><a href="https://emischool.sk/">СЛОВАКИЯ Martincekova 17, 821 01 - Bratislava</a>. Tel. <a href="tel:+421 915 994 994">+421 915 994 994</a> slovakia-sales@emischool.sk</td>
//<br><td> </td>
?>