<?php

// Email address that messages will be sent to;
$address = "it2@emi-school.ru, kaliningrad-sales@emi-school.ru";

if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");

$name       = $_POST['name'];
$phone       = $_POST['phone'];


// Email content
$e_subject = 'Запись на Бизнес семинар';
$msg = "\n ФИО: $name \n Телефон: $phone \n" . PHP_EOL . PHP_EOL;

$headers = "From: kaliningrad-sales@emi-school.ru" . PHP_EOL;
$headers .= "MIME-Version: 1.0" . PHP_EOL;
$headers .= "Content-type: text/plain; charset=utf-8" . PHP_EOL;
$headers .= "Content-Transfer-Encoding: quoted-printable" . PHP_EOL;

if(mail($address, $e_subject, $msg, $headers)) {
    // Email has sent successfully, echo a success page.
    echo "<div id='success_page'>";
    echo "<h4 class='highlight'>Спасибо, <strong>$name</strong>. Ваша заявка принята.</h4>";
    echo "</div>";
} else {
    echo 'ERROR!';
}

