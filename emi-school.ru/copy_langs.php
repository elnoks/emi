<?
// Копирование ланговых файлов EN для новых сайтов
// prgrant 26.06.2017
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
if ($USER->IsAdmin()) {
?>
    <?
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
    $APPLICATION->SetTitle("Создание/изменение ланговых файлов");
    ?>
    <?

        //echo phpinfo();

        echo $_SERVER["DOCUMENT_ROOT"];

        echo "<br>";

        echo "/home/bitrix/ext_www/";

        $LANG_FILES_PATH = array(
            "/bitrix/templates/.default/components/ithive/offices.list/.default/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/main.profile/emi_personal/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/subscribe.edit/subscribe/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/sale.personal.order.list/.default/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/sale.personal.order.list/order_list_courses/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/sale.personal.order.detail/.default/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/sale.personal.order.detail/order_detail_courses/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/sale.personal.order.cancel/.default/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/sale.basket.basket/basket/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/sale.basket.basket/basketSchool/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/sale.order.ajax/order/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/sale.order.ajax/orderSchool/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/sale.basket.basket.line/products_basket/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/sale.basket.basket.line/courses_basket/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/catalog/.default/bitrix/catalog.section/list/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/catalog/.default/bitrix/catalog.element/.default/lang/en/template.php",
            "/local/components/oc/analog.product.slider/templates/.default/lang/en/template.php",
            "/bitrix/templates/.default/components/bitrix/catalog/courses/bitrix/catalog.section/list/lang/en/template.php",
            "/bitrix/components/emi/schedule.calendar/templates/.default/lang/en/template.php",
            "/bitrix/templates/.default/lang/en/header.php",
            "/bitrix/components/emi/news.sections/templates/emi/lang/en/template.php",
            "/bitrix/components/emi/buttons/templates/.default/lang/en/template.php",
        )

    ?>
    <div class="text-center">
        <form action="" method="post" style="margin-top: 15%">
            <input type="text">
            <input type="text">
            <input type="submit" value="Применить">
        </form>
    </div>

    <? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>

<?
} else {
    header("Location: /index.php");
    exit();
}
?>
<?
/*Лэнги шаблонов компонентов

Папка с включаемыми областями
File: /home/bitrix/www/include/lang/en/

Константы:
File: /home/bitrix/www/bitrix/php_interface/include/lang

cy - Кипр Английский

de - Германия Немецкий
ee - Эстония Эстонский
ro - РУМЫНИЯ Румынский
pt - Португалия Португальский

ua - УКРАИНА Русский
by - БЕЛОРУССИЯ Русский
*/
?>