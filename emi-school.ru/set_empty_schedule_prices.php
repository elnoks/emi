<? 
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
?>

<? 
echo "<pre>";
if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog") ) {
	$pid = '15';
	$arSelect = array('ID','IBLOCK_ID', 'PROPERTY_COURSE');
	$arFilter = array('IBLOCK_ID' => 18, 'CATALOG_PRICE_'.$pid => false);
	$dbScheduleResult = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	$arScheduleEmptyPrices = array();
	$arCoursesIds = array();
	while($arScheduleResult = $dbScheduleResult->GetNext()) {
		$arScheduleEmptyPrices[] = $arScheduleResult;
		if(!in_array($arScheduleResult["PROPERTY_COURSE_VALUE"], $arCoursesIds)) {
			$arCoursesIds[] = $arScheduleResult["PROPERTY_COURSE_VALUE"];
		}
	}
	echo "Количество элементов расписания с пустой ценой: ".count($arScheduleEmptyPrices)."<br>";
	if(count($arScheduleEmptyPrices)) {
		$arSelect = array('ID','IBLOCK_ID','CATALOG_PRICE_'.$pid, 'CATALOG_GROUP_'.$pid);
		$arFilter = array('IBLOCK_ID' => 19, 'ID' => $arCoursesIds, '!CATALOG_PRICE_'.$pid => false);
		$dbCourseResult = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		$arCourses = array();
		while($arCourseResult = $dbCourseResult->GetNext()) {
			$arCourses[$arCourseResult["ID"]] = $arCourseResult;
		}
		foreach($arScheduleEmptyPrices as $scheduleEmptyPrices) {
			$price = $arCourses[$scheduleEmptyPrices["PROPERTY_COURSE_VALUE"]]["CATALOG_PRICE_".$pid];
			$currency = $arCourses[$scheduleEmptyPrices["PROPERTY_COURSE_VALUE"]]["CATALOG_CURRENCY_".$pid];
			if($price) {
				CPrice::SetBasePrice($scheduleEmptyPrices["ID"], $price, $currency);
			}
		}
	}
}
echo "</pre>";
?>