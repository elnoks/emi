<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мой профиль");
?>
<?
redirectToLoginIfNotAuthorized($APPLICATION->GetCurPage());
$APPLICATION->IncludeComponent("bitrix:main.profile", "emi_personal", Array(
	"SET_TITLE" => "N",	// Устанавливать заголовок страницы
	),
	false
);
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>