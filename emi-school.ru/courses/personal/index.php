<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
redirectToLoginIfNotAuthorized($APPLICATION->GetCurPage());
?>
<div class="row">
	<div class="col-md-12">
		<p>В личном кабинете Вы можете проверить текущее состояние корзины, ход выполнения Ваших заказов, просмотреть или изменить личную информацию, 
		а также подписаться на новости и другие информационные рассылки. </p>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
