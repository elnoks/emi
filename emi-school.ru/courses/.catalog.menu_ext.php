<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION;
$aMenuLinks = Array(
	Array(
		GetMessage("CATALOG_COURSES_MENU_SHEDULE"),
		"schedule",
		Array(), 
		Array(), 
		"" 
	),

	Array(
		GetMessage("CATALOG_COURSES_MENU_TEACHERS"),
		"teachers",
		Array(), 
		Array(), 
		"" 
	),
);



   $aMenuLinksExt=$APPLICATION->IncludeComponent(
	"bitrix:menu.sections", 
	"", 
	array(
		"IS_SEF" => "N",
		"ID" => $_REQUEST["ID"],
		"IBLOCK_TYPE" => "courses",
		"IBLOCK_ID" => COURSES_IBLOCK_ID,
		"SECTION_URL" => "",
		"DEPTH_LEVEL" => "4",
		"CACHE_TYPE" => "Y",
		"CACHE_TIME" => "36000000"
	),
	false
);
$aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);
?>