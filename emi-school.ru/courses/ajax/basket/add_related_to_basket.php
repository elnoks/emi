<? 
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
?>

<? 
if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog") )
{
	$event_selected_id = $_REQUEST["selected_date_id"];
	$course_selected_id = $_REQUEST["selected_course_id"];
	$event_selected_date = $_REQUEST["selected_date_val"];
	$course_detail_page = $_REQUEST["course_detail_page"];
	
	$PROP=array(
			array("NAME" => "Дата начала", "CODE" => "DATE_START", "VALUE" => $event_selected_date), 
			array("NAME" => "Ссылка на курс", "CODE" => "COURSE_LINK", "VALUE" => $course_detail_page),
			array("NAME" => "Курс", "CODE" => "ID_COURSE", "VALUE" => $course_selected_id),
	);
	if (1){
		Add2BasketByProductID(
			$event_selected_id,
			1,
			array(),
			$PROP
		);
	}

}
?>