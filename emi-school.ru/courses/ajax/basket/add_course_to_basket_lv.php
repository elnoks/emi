<? 
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('sale');
CModule::IncludeModule('catalog');

if($_SERVER["REQUEST_METHOD"] == "POST"){
	if($_REQUEST["course_id"]){
		/*if(!$_REQUEST["schedule_id"])
			$messButton = "В настоящее время в Вашем регионе мероприятие не проводится";
		else{*/
			$PROP=array(
				array(
					"NAME" => "Sākuma datums",
					"CODE" => "DATE_START", 
					"VALUE" => $_REQUEST["datestartBasket"]
				),
				array(
					"NAME" => "Kurss",
					"CODE" => "ID_COURSE", 
					"VALUE" => $_REQUEST["course_id"]
				),
				array(
					"NAME" => "Saite uz kursu",
					"CODE" => "COURSE_LINK", 
					"VALUE" => $_REQUEST["course_link"]
				)
			);
			
			$ob = CSaleBasket::GetList(
				array(),
				array(
					"FUSER_ID" => CSaleBasket::GetBasketUserID(), 
					"LID" => SITE_ID,
					"ORDER_ID" => "NULL"		
				),
				false,
				false, 
				array("ID"/*, "NAME", "QUANTITY", "PRICE"*/)
			);
			
			while ($id = $ob->Fetch())
				$arID[] = $id["ID"];
				


			if($arID){
				
				$obProp = CSaleBasket::GetPropsList(
					array("NAME"),
					array(
						"BASKET_ID" => $arID,
						"VALUE" => $_REQUEST["course_id"],
						"CODE" => "ID_COURSE"
					)
				);
				
				if(!$result = $obProp->Fetch()){
					$baskID = Add2BasketByProductID($_REQUEST["schedule_id"], 1, array(), $PROP);
					
					if($baskID){
						$countBasket = count($arID) + 1;
						$messButton = 'Mans kursi';
					}
				}else{
					$messButton = 'Jūs esat piereģistrēts šim kursam.';
					//echo "<div id='Antosha_bask' style='display:none'><pre>"; print_r($result);echo "</pre></div>";
				}

			}else{
				$baskID = Add2BasketByProductID($_REQUEST["schedule_id"],	1, array(), $PROP);
				//
				$messButton = 'My courses';
				$countBasket = count($arID) + 1;
			}
		
		//}

	}
	//echo "<div id='Antosha_bask' style='display:none'><pre>"; print_r($_REQUEST);echo "</pre></div>";
}

?>
<?if($messButton):?>
	<span id="messButton"><?=$messButton?></span>
<?endif;?>
<?if($countBasket):?>
	<span id="countBasket">(<?=$countBasket?>)</span>
<?endif;?>