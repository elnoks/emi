<? 
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('sale');
CModule::IncludeModule('catalog');

define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/LogBask.txt");

if($_SERVER["REQUEST_METHOD"] == "POST"){
	if($_REQUEST["course_id"]){
		/*if(!$_REQUEST["schedule_id"])
			$messButton = "В настоящее время в Вашем регионе мероприятие не проводится";
		else{*/
			$PROP=array(
				array(
					"NAME" => "Дата начала", 
					"CODE" => "DATE_START", 
					"VALUE" => $_REQUEST["datestartBasket"]
				),
				array(
					"NAME" => "Курс", 
					"CODE" => "ID_COURSE", 
					"VALUE" => $_REQUEST["course_id"]
				),
				array(
					"NAME" => "Ссылка на курс", 
					"CODE" => "COURSE_LINK", 
					"VALUE" => $_REQUEST["course_link"]
				)
			);
			
			$ob = CSaleBasket::GetList(
				array(),
				array(
					"FUSER_ID" => CSaleBasket::GetBasketUserID(), 
					"LID" => SITE_ID,
					"ORDER_ID" => "NULL"		
				),
				false,
				false, 
				array("ID"/*, "NAME", "QUANTITY", "PRICE"*/)
			);

            AddMessage2Log($_REQUEST["schedule_id"] . "\n\n" . var_export($ob, true));

			while ($id = $ob->Fetch()){
				$arID[] = $id["ID"];

            }

			if($arID){

				$obProp = CSaleBasket::GetPropsList(
					array("NAME"),
					array(
						"BASKET_ID" => $arID,
						"VALUE" => $_REQUEST["course_id"],
						"CODE" => "ID_COURSE"
					)
				);

				if(!$result = $obProp->Fetch()){
					$baskID = Add2BasketByProductID($_REQUEST["schedule_id"], 1, array(), $PROP);
					
					if($baskID){
						$countBasket = count($arID) + 1;
						$messButton = 'Мои курсы';
					}
				}else{
					$messButton = 'Вы уже записались на данный курс!';
					//echo "<div id='Antosha_bask' style='display:none'><pre>"; print_r($result);echo "</pre></div>";
				}

			}else{


			    $baskID = Add2BasketByProductID($_REQUEST["schedule_id"],	1, array("PRICE" => 100), $PROP);

				if(LANGUAGE_ID == 'ru'){

				}

				$messButton = 'Мои курсы';
				$countBasket = count($arID) + 1;
			}
		
		//}

	}
	//echo "<div id='Antosha_bask' style='display:none'><pre>"; print_r($_REQUEST);echo "</pre></div>";
}

?>
<?if($messButton):?>
	<span id="messButton"><?=$messButton?></span>
<?endif;?>
<?if($countBasket):?>
	<span id="countBasket">(<?=$countBasket?>)</span>
<?endif;?>