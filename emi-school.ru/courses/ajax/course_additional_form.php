<?
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
?>

<?$APPLICATION->IncludeComponent(
		"bitrix:form.result.new", 
		"course_additional", 
		array(
		 	//"AJAX_MODE" => "Y",  // режим AJAX
		  // 	"AJAX_OPTION_SHADOW" => "N", // затемнять область
           // "AJAX_OPTION_JUMP" => "N", // скроллить страницу до компонента
         //   "AJAX_OPTION_STYLE" => "N", // подключать стили
            //"AJAX_OPTION_HISTORY" => "N",
			"SEF_MODE" => "N",
			"WEB_FORM_ID" => "1",
			"LIST_URL" => "",
			"EDIT_URL" => "",
			"SUCCESS_URL" => "",
			"CHAIN_ITEM_TEXT" => "",
			"CHAIN_ITEM_LINK" => "",
			"IGNORE_CUSTOM_TEMPLATE" => "N",
			"USE_EXTENDED_ERRORS" => "N",
			"CACHE_TYPE" => "N",
			"CACHE_TIME" => "3600",
			"CACHE_NOTES" => "",
			"SEF_FOLDER" => "/test/",
			"VARIABLE_ALIASES" => array(
				"WEB_FORM_ID" => "WEB_FORM_ID",
				"RESULT_ID" => "RESULT_ID",
			)
		),
		false
);
?>