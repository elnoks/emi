<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

//$APPLICATION->SetTitle("");

if (strpos($_SERVER['REQUEST_URI'], '/courses/') !== false) // именно через жесткое сравнение
{
	//    $APPLICATION->SetPageProperty("title", "Courses");
	//    $APPLICATION->SetPageProperty("keywords", "courses design school");
	//    $APPLICATION->SetPageProperty("description", "Courses design school");

    $APPLICATION->SetPageProperty("title",GetMessage("CATALOG_COURSES_SECTION_TITLE"));
    $APPLICATION->SetPageProperty("keywords",GetMessage("CATALOG_COURSES_SECTION_DESCRIPTION"));
    $APPLICATION->SetPageProperty("description",GetMessage("CATALOG_COURSES_SECTION_KEYWORDS"));
    $APPLICATION->SetTitle(GetMessage("CATALOG_COURSES_META_TITLE"));

    if (LANGUAGE_ID == "ru") {
        $get = $_GET["new_region"];
        if ($get) {
            if (CModule::IncludeModule('prgrant.morphos')) {
                $APPLICATION->SetPageProperty("title", "Школа ногтевого дизайна E.Mi в ".morphos\Russian\GeographicalNamesInflection::getCase($GLOBALS["partnerShop"]->getCurrentCityName(), 'Предложный'));
                $APPLICATION->SetPageProperty("keywords", "школа обучение E.Mi курсы маникюра педикюра дизайна ногтей ".morphos\Russian\GeographicalNamesInflection::getCase($GLOBALS["partnerShop"]->getCurrentCityName(), 'Именительный')." ногтевой центр обучение диплом");
                $APPLICATION->SetPageProperty("description", "Школа ногтевого дизайна Екатерины Мирошниченко E.Mi проводит курсы маникюра, педикюра и дизайна ногтей в ".morphos\Russian\GeographicalNamesInflection::getCase($GLOBALS["partnerSchool"]->getCurrentCityName(), 'Предложный').". Все подробности по телефону: ".$GLOBALS["partnerSchool"]->getPhones_keywords().".");
            }
        }
        else{
            $APPLICATION->SetPageProperty("title", "Школа ногтевого дизайна E.Mi");
            $APPLICATION->SetPageProperty("keywords", "школа обучение E.Mi курсы маникюра педикюра дизайна ногтей ногтевой центр обучение диплом");
            $APPLICATION->SetPageProperty("description", "Школа ногтевого дизайна Екатерины Мирошниченко E.Mi проводит курсы маникюра, педикюра и дизайна ногтей. Все подробности по телефону: ".$GLOBALS["partnerSchool"]->getPhones_keywords().".");
        }

    }

}

//else
//{
//    $APPLICATION->SetTitle(GetMessage("CATALOG_COURSES_META_TITLE"));
//    $APPLICATION->SetTitle(GetMessage("CATALOG_COURSES_META_DESCRIPTION"));
//    $APPLICATION->SetTitle(GetMessage("CATALOG_COURSES_META_KEYWORDS"));
//}


$APPLICATION->AddHeadScript("/bitrix/templates/.default/scripts/spin.js");
$APPLICATION->AddHeadScript("/bitrix/templates/.default/scripts/jquery.zoom.js");
?>
<?

if (SITE_ID=="s2" && $GLOBALS["partnerSchool"]->getCurrentRegionId()==70836){
	?>
	<div class="h3" style="; color:#ea516d;">Внимание: Школа ногтевого дизайна E.Mi находится в г. Костроме!</div>
	<div class="h4" style="; color:#ea516d;">
		Перейдите в регион: <a href="?new_region=2620" style="color:#ea516d;" class="h4">Костромская Область (г. Кострома)</a></div>
	<?
}
else
{
	//echo COURSES_IBLOCK_ID;
	if ($partnerPrice = $GLOBALS["partnerSchool"]->getPriceCode())
	{
		$arSParams["PRICE_CODE"][] = $partnerPrice;
		$arSParams["REGION_OWN_PRICE_CODE"] = $partnerPrice;
	}

	$APPLICATION->IncludeComponent(
	"emi-school:catalog", 
	"courses", 
	array(
		"IBLOCK_TYPE" => "courses",
		"IBLOCK_ID" => COURSES_IBLOCK_ID,
		"TEMPLATE_THEME" => "site",
		"HIDE_NOT_AVAILABLE" => "N",
		"BASKET_URL" => "/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/courses/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"SET_TITLE" => "Y",
		"ADD_SECTION_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "Y",
		"SET_STATUS_404" => "Y",
		"DETAIL_DISPLAY_NAME" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_FILTER" => "Y",
		"FILTER_NAME" => "",
		"FILTER_VIEW_MODE" => "VERTICAL",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_PRICE_CODE" => array(
			0 => "RETAIL_AE",
			1 => "RETAIL_AF",
			2 => "RETAIL_AZ",
			3 => "RETAIL_BY",
			4 => "RETAIL_CH",
			5 => "RETAIL_CY",
			6 => "RETAIL_CZ",
			7 => "RETAIL_DE",
			8 => "RETAIL_DN",
			9 => "RETAIL_EE",
			10 => "Укр_Львов_курсы",
			11 => "Укр_Одесса_курсы",
			12 => "ДНР_курсы",
			13 => "Нальчик_курсы",
			14 => "Абакан_курсы",
			15 => "Москва_Цветной_курсы",
			16 => "Владикавказ_курсы",
			17 => "Санкт-Петербург_курсы_Ярославский",
			18 => "RETAIL_ES",
			19 => "Саратов_курсы",
			20 => "Волгоград_курсы",
			21 => "Брянск_курсы",
			22 => "RETAIL_PL",
			23 => "RETAIL_KR",
			24 => "Оптовая",
			25 => "Розничная_1",
			26 => "Краснодар_школа",
			27 => "Цена салона",
			28 => "Красноярск_курсы",
			29 => "RETAIL_GR",
			30 => "Сочи_курсы",
			31 => "RETAIL_HE",
			32 => "BASE",
			33 => "Йошкар-Ола_курсы",
			34 => "RETAIL_EN",
			35 => "RETAIL_FR",
			36 => "RETAIL_IE",
			37 => "RETAIL_IT",
			38 => "RETAIL_KG",
			39 => "RETAIL_KZ",
			40 => "RETAIL_LN",
			41 => "RETAIL_LT",
			42 => "RETAIL_LV",
			43 => "RETAIL_MD",
			44 => "ЗакупочнаяКОРНЕЦКИЙ",
			45 => "RETAIL_PT",
			46 => "RETAIL_RO",
			47 => "RETAIL_TN",
			48 => "RETAIL_UA",
			49 => "RETAIL_UK",
			50 => "Владимир_курсы",
			51 => "Воронеж_курсы",
			52 => "Иваново_курсы",
			53 => "Екатеринбург_курсы",
			54 => "Закупочная",
			55 => "Ижевск_курсы",
			56 => "Иркутск_курсы",
			57 => "Казань_курсы",
			58 => "Калининград_курсы",
			59 => "Камчатка_курсы",
			60 => "Кемерово_курсы",
			61 => "Москва_курсы",
			62 => "Н_Новгород_курсы",
			63 => "Новосибирск_курсы",
			64 => "Оренбург_курсы",
			65 => "Орехово-Зуево_курсы",
			66 => "Пенза_курсы",
			67 => "Плановая себестоимость",
			68 => "Розничная",
			69 => "Рязань_курсы",
			70 => "Санкт-Петербург_курсы",
			71 => "Саранск_курсы",
			72 => "Тамбов_курсы",
			73 => "Тюмень_курсы",
			74 => "Ульяновск_курсы",
			75 => "Укр_Днепр_курсы",
			76 => "Укр_Киев_курсы",
			77 => "Укр_КривойРог_курсы",
			78 => "Укр_Полтава_курсы",
			79 => "Укр_Ровно_курсы",
			80 => "Укр_Харьков_курсы",
			81 => "Укр_Черновцы_курсы",
			82 => "Хабаровск_курсы",
			83 => "Цена мастера",
			84 => "Якутия_курсы",
			85 => "Чебоксары_курсы",
		),
		"FILTER_OFFERS_FIELD_CODE" => array(
			0 => "PREVIEW_PICTURE",
			1 => "DETAIL_PICTURE",
			2 => "",
		),
		"FILTER_OFFERS_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"USE_REVIEW" => "Y",
		"MESSAGES_PER_PAGE" => "10",
		"USE_CAPTCHA" => "Y",
		"REVIEW_AJAX_POST" => "Y",
		"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
		"FORUM_ID" => "1",
		"URL_TEMPLATES_READ" => "",
		"SHOW_LINK_TO_FORUM" => "Y",
		"USE_COMPARE" => "N",
		"PRICE_CODE" => array(
			0 => "RETAIL_AE",
			1 => "RETAIL_AF",
			2 => "RETAIL_AZ",
			3 => "RETAIL_BY",
			4 => "RETAIL_CH",
			5 => "RETAIL_CY",
			6 => "RETAIL_CZ",
			7 => "RETAIL_DE",
			8 => "RETAIL_DN",
			9 => "RETAIL_EE",
			10 => "Укр_Львов_курсы",
			11 => "Укр_Одесса_курсы",
			12 => "ДНР_курсы",
			13 => "Нальчик_курсы",
			14 => "Абакан_курсы",
			15 => "Москва_Цветной_курсы",
			16 => "Владикавказ_курсы",
			17 => "Санкт-Петербург_курсы_Ярославский",
			18 => "RETAIL_ES",
			19 => "Саратов_курсы",
			20 => "Волгоград_курсы",
			21 => "Брянск_курсы",
			22 => "RETAIL_PL",
			23 => "RETAIL_KR",
			24 => "Оптовая",
			25 => "Розничная_1",
			26 => "Краснодар_школа",
			27 => "Цена салона",
			28 => "Красноярск_курсы",
			29 => "RETAIL_GR",
			30 => "Сочи_курсы",
			31 => "RETAIL_HE",
			32 => "BASE",
			33 => "Йошкар-Ола_курсы",
			34 => "RETAIL_EN",
			35 => "RETAIL_FR",
			36 => "RETAIL_IE",
			37 => "RETAIL_IT",
			38 => "RETAIL_KG",
			39 => "RETAIL_KZ",
			40 => "RETAIL_LN",
			41 => "RETAIL_LT",
			42 => "RETAIL_LV",
			43 => "RETAIL_MD",
			44 => "ЗакупочнаяКОРНЕЦКИЙ",
			45 => "RETAIL_PT",
			46 => "RETAIL_RO",
			47 => "RETAIL_TN",
			48 => "RETAIL_UA",
			49 => "RETAIL_UK",
			50 => "Владимир_курсы",
			51 => "Воронеж_курсы",
			52 => "Иваново_курсы",
			53 => "Екатеринбург_курсы",
			54 => "Закупочная",
			55 => "Ижевск_курсы",
			56 => "Иркутск_курсы",
			57 => "Казань_курсы",
			58 => "Калининград_курсы",
			59 => "Камчатка_курсы",
			60 => "Кемерово_курсы",
			61 => "Москва_курсы",
			62 => "Н_Новгород_курсы",
			63 => "Новосибирск_курсы",
			64 => "Оренбург_курсы",
			65 => "Орехово-Зуево_курсы",
			66 => "Пенза_курсы",
			67 => "Плановая себестоимость",
			68 => "Розничная",
			69 => "Рязань_курсы",
			70 => "Санкт-Петербург_курсы",
			71 => "Саранск_курсы",
			72 => "Тамбов_курсы",
			73 => "Тюмень_курсы",
			74 => "Ульяновск_курсы",
			75 => "Укр_Днепр_курсы",
			76 => "Укр_Киев_курсы",
			77 => "Укр_КривойРог_курсы",
			78 => "Укр_Полтава_курсы",
			79 => "Укр_Ровно_курсы",
			80 => "Укр_Харьков_курсы",
			81 => "Укр_Черновцы_курсы",
			82 => "Хабаровск_курсы",
			83 => "Цена мастера",
			84 => "Якутия_курсы",
			85 => "Чебоксары_курсы",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"USE_PRODUCT_QUANTITY" => "Y",
		"CONVERT_CURRENCY" => "N",
		"CURRENCY_ID" => "RUB",
		"QUANTITY_FLOAT" => "N",
		"OFFERS_CART_PROPERTIES" => array(
			0 => "COLOR_REF",
			1 => "SIZES_SHOES",
			2 => "SIZES_CLOTHES",
		),
		"SHOW_TOP_ELEMENTS" => "N",
		"SECTION_COUNT_ELEMENTS" => "Y",
		"SECTION_TOP_DEPTH" => "1",
		"SECTIONS_VIEW_MODE" => "TILE",
		"SECTIONS_SHOW_PARENT_NAME" => "Y",
		"PAGE_ELEMENT_COUNT" => "3000",
		"LINE_ELEMENT_COUNT" => "3",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"LIST_PROPERTY_CODE" => array(
			0 => "",
			1 => "NEWPRODUCT",
			2 => "SALELEADER",
			3 => "SPECIALOFFER",
			4 => "",
		),
		"INCLUDE_SUBSECTIONS" => "Y",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_BROWSER_TITLE" => "-",
		"LIST_OFFERS_FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "DETAIL_PICTURE",
			3 => "",
			4 => "",
		),
		"LIST_OFFERS_PROPERTY_CODE" => array(
			0 => "ARTNUMBER",
			1 => "COLOR_REF",
			2 => "SIZES_SHOES",
			3 => "SIZES_CLOTHES",
			4 => "MORE_PHOTO",
			5 => "",
		),
		"LIST_OFFERS_LIMIT" => "0",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "COURSE_LENGTH",
			2 => "NEWPRODUCT",
			3 => "MANUFACTURER",
			4 => "MATERIAL",
			5 => "TIPS",
			6 => "RECOMMENDED_COURSES",
			7 => "TRAINING_LITERATURE",
			8 => "SAMPLE_SOLUTIONS",
			9 => "DIFF_LEVEL",
		),
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_BROWSER_TITLE" => "-",
		"DETAIL_OFFERS_FIELD_CODE" => array(
			0 => "NAME",
			1 => "",
		),
		"DETAIL_OFFERS_PROPERTY_CODE" => array(
			0 => "ARTNUMBER",
			1 => "COLOR_REF",
			2 => "SIZES_SHOES",
			3 => "SIZES_CLOTHES",
			4 => "MORE_PHOTO",
			5 => "",
		),
		"LINK_IBLOCK_TYPE" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_PROPERTY_SID" => "",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"USE_ALSO_BUY" => "Y",
		"ALSO_BUY_ELEMENT_COUNT" => "4",
		"ALSO_BUY_MIN_BUYES" => "1",
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"PAGER_TEMPLATE" => "arrows",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
		"PAGER_SHOW_ALL" => "N",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_TREE_PROPS" => array(
			0 => "COLOR_REF",
			1 => "SIZES_SHOES",
			2 => "SIZES_CLOTHES",
		),
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_OLD_PRICE" => "Y",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"DETAIL_USE_VOTE_RATING" => "Y",
		"DETAIL_VOTE_DISPLAY_AS_RATING" => "rating",
		"DETAIL_USE_COMMENTS" => "Y",
		"DETAIL_BLOG_USE" => "Y",
		"DETAIL_VK_USE" => "N",
		"DETAIL_FB_USE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"USE_STORE" => "Y",
		"USE_STORE_PHONE" => "Y",
		"USE_STORE_SCHEDULE" => "Y",
		"USE_MIN_AMOUNT" => "N",
		"STORE_PATH" => "/store/#store_id#",
		"MAIN_TITLE" => "Наличие на складах",
		"MIN_AMOUNT" => "10",
		"DETAIL_BRAND_USE" => "Y",
		"DETAIL_BRAND_PROP_CODE" => "",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"SECTIONS_HIDE_SECTION_NAME" => "N",
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"DETAIL_SHOW_MAX_QUANTITY" => "N",
		"DETAIL_BLOG_URL" => "catalog_comments",
		"DETAIL_FB_APP_ID" => "",
		"COMPONENT_TEMPLATE" => "courses",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"DETAIL_STRICT_SECTION_CHECK" => "N",
		"SET_LAST_MODIFIED" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"SECTION_BACKGROUND_IMAGE" => "-",
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"DETAIL_BACKGROUND_IMAGE" => "-",
		"SHOW_DEACTIVATED" => "N",
		"USE_GIFTS_DETAIL" => "Y",
		"USE_GIFTS_SECTION" => "Y",
		"USE_GIFTS_MAIN_PR_SECTION_LIST" => "Y",
		"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_SECTION_LIST_BLOCK_TITLE" => "Подарки к товарам этого раздела",
		"GIFTS_SECTION_LIST_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
		"STORES" => array(
			0 => "",
			1 => "",
		),
		"USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SHOW_EMPTY_STORE" => "Y",
		"SHOW_GENERAL_STORE_INFORMATION" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"COMPATIBLE_MODE" => "Y",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DETAIL_SET_VIEWED_IN_COMPONENT" => "N",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE#/",
			"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
			"compare" => "compare/",
			"smart_filter" => "#SECTION_CODE#/filter/#SMART_FILTER_PATH#/apply/",
		)
	),
	false
);
}
?>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => CURLANG_INCLUDE_PATH . "courses_text_block.php",
				"EDIT_TEMPLATE" => ""
			),
			false
		);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>