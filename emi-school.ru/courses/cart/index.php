<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?$APPLICATION->SetTitle(GetMessage("CART_PAGE_META_TITLE"));?>

	<div class="wrapper-top">
		<div class="bredcrumbs">
			<?$APPLICATION->IncludeComponent(
				"bitrix:breadcrumb", 
				"emi", 
				array(
					"START_FROM" => "0",
					"PATH" => "",
					"SITE_ID" => "-"
				),
				false
			);?>
		</div>
	</div>
        
	 <?
	 $APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket", 
	"basketSchool", 
	array(
		"COLUMNS_LIST" => array(
			0 => "NAME",
			1 => "DATE",
			2 => "PROPS",
			3 => "DELETE",
			4 => "DELAY",
			5 => "DISCOUNT",
			6 => "QUANTITY",
			7 => "PRICE",
		),
		"OFFERS_PROPS" => array(
		),
		"PATH_TO_ORDER" => "/courses/cart/order/make/",
		"HIDE_COUPON" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "Y",
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"USE_PREPAYMENT" => "N",
		"QUANTITY_FLOAT" => "N",
		"SET_TITLE" => "Y",
		"SHOW_WEIGHT_PROP" => "N",
		"ACTION_VARIABLE" => "action"
	),
	false
);
?>
	 
	 <?$APPLICATION->IncludeComponent(
	"emi:sale.basket.related_courses", 
	"", 
	array(
		"COLUMNS_LIST" => array(
			0 => "NAME",
			1 => "DATE",
			2 => "PROPS",
			3 => "DELETE",
			4 => "DELAY",
			5 => "DISCOUNT",
			6 => "QUANTITY",
			7 => "PRICE",
			8 => "ADD_TO_CART",
		),
	),
	false
);?>
	 	 
	 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>