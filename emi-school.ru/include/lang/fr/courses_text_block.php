﻿<?if ($_SERVER['REQUEST_URI']=='/courses/') {?>
<div>
	<p>L'«École de Nail Design by Ekaterina Miroshnichenko» vous propose également les Formations de Nail-art. Les cours sont données par les formateurs diplômées E.Mi. Vous avez une occasion unique d'améliorer vos compétences ou d'apprendre un nouveau métier. Les programmes d'études sont divisés en trois degrés de complexité. Choisissez les cours de difficulté appropriée et commencez vos leçons prochainement.</p>
<h2>The main advantages of nail design courses are:</h2>
<ul>
<li>Ekaterina Miroshnichenko propose régulièrement de nouvelles techniques et méthodes de Nail Art  dont elle fait part aux étudiants de l'école dans le cadre de l'apprentissage de la conception des ongles.</li>
<li>Les principaux avantages de cours de Nail Design</li>
<li>L'apprentissage des tendances actuelles de Nail Art sous la direction de professionnels.</li>
<li>L’étude des principes de travail sur les produits de haute qualité  de la marque populaire E.Mi.</li>
<li>Les programmes universels élaborés pour les professionnels qui ne disposent pas d'une éducation artistique.</li>

</ul>
<h2>Que comprend la formation?</h2>
<p>En formation,  vous apprendrez les techniques de base du Nail Art, vous vous entraînerez en performance, vous obtiendrez les conseils pratiques des enseignants.</p>
<p>A l’issue de la formation sur la conception des ongles, vous pourrez réaliser vos idées créatives les plus sophistiquées, offrir à vos clients de nouveaux services et même participer à des compétitions professionnelles d’un niveau plus élevé.</p>
</div><br>
<?}?>