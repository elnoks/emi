<h2 class="heading">A favorite of celebrities<br>
 </h2>
<div>
	<span class="subheading"><span style="font-size: 18px;"> </span>
	<div>
		<span style="font-size: 18px;">E.Mi manicure is favored not only by Russian fashion icons, such as singer Glukoza or actress Polina Maksimova – it is also the first choice of many Hollywood superstars like Noomi Rapace, Doutzen Kroes or Rihanna.</span>
	</div>
 </span>
</div>
<br>