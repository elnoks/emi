
<h2 class="heading">E.Mi brendas
  <br />
 <span class="subheading">Sėkmės istorija</span></h2>
 
<div><span class="subheading"><font size="4"> </font> 
    <div><font size="4">Auštos kokybės, Jekaterinos Mirošničenko sukurti nagų dizaino produktai. Nepriekaištingos kokybės priemonės buvo kuriamos tam, kad patenkintų ir palengvintų nagų dizaino specialisčių darbą -juk jas kūrė žmogus, žinantis visus šio darbo niuansus.</font></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div><font size="4">Geliniai dažai ir teptukai, pagal patentuotas formules gaminami Vokietijoje  ir įvairūs aksesuarai bei pagalbinės priemonės nagų dizainui kurti. Geliniai dažai UV lempoje polimerizuojasi  per 1 min (kai kurios spalvos per keliolika sekundžių), jų konsistencija tokia, kad galite piešti ant visų nagų ir tik tada džiovinti. Su ,,E.Mi” priemonėm lengva būti piešiančiu ir kuriančiu meistru!</font></div>
   
 
   </span></div>
