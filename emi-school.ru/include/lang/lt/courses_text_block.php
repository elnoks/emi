﻿<?if ($_SERVER['REQUEST_URI']=='/courses/') {?>
<div>
	<p>Jekaterinos Mirošničenko nagų dizaino mokykla siūlo įvairaus sudėtingumo lygio populiarius mokymus. Visų seminarų pagrindas yra pačios autorės kurtos mokymų metodikos. Jūs turite unikalią galimybę pagerinti savo įgūdžius arba įgyti naują profesiją.  Kursai suskirstyti į trijų sudėtingumo lygių seminarus.  Pasirinkite kursus pagal Jums tinkantį sudėtingumo lygį ir pradėti savo mokymus jau artimiausiu metu.</p>
<p>Jekaterinos Mirošničenko  nuolat kuria naujas mokymų technikas ir naujus nagų dizaino būdus, kuriais dalinasi mokymų metu su savo mokyniais.</p>
<h2>Pagrindiniai nagų dizaino mokymų privalumai:</h2>
<ul>
<li>Išmoksite modrniausių nagų dailės tendencijų  pagal specialistų rekomendacijas.</li>
<li>Išmoksite pagrindinių darbo principų su populiariais  ir aukštos kokybės E.Mi produktais.</li>
<li>Mokymosi programos sukurtos specialistams, kurie neturi specialaus meninio ugdymo išsilavinimo.</li>
</ul>
<h2>Kas sudaro mokymų programas?</h2>
<p>Šiuo metu mokymai vyksta visoje Lietuvoje. Jūs galite pasirinkti mokymus, kurie vyksta Jūsų mieste arba arčiau Jūsų esančiame didmiestyje.</p>
<p>Mokymų metu Jūs išmoksite pagrindinių nagų dizaino technikų bei jų panaudojimo galimybių, gausite praktinių patarimų ir instrukcijų.</p>
<p>Po šių mokymų  Jūs turėsite naujų įdomių kūrybinių idėjų, savo klientams galėsite pasiūlyti naujas paslaugas ir net dalyvauti  aukšto lygio profesionaliuose konkursuose.</p>
</div><br>
<?}?>