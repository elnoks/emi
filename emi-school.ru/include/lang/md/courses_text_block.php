﻿<?if ($_SERVER['REQUEST_URI']=='/courses/') {?>
<div>
	<p>Școală de Nail Design by Ekaterina Miroshnichenko oferă cursuri profesionale de nail design. Instuctori  predau după  tehnicile  Ekaterinei Miroshnichenko. Dumnevostră aveți o ocazie unică de a se perfecţiona pe plan profesional sau să învățaţi o nouă meserie. Programele de studiu sunt împărțite în trei niveluri de dificultate.  Selectați cursuri de complexitate corespunzătoare și înceapeţi studiile la data disponibilă.</p>
<p>Ekaterina Miroshnichenko oferă în mod regulat tehnici și metode noi de nail art, pe care le împarte cu studenții săi în cadrul cursurilor.</p>
<h2>Principalele avantaje ale cursurilor de Nail Design:</h2>
<ul>
<li>Mastering  tendințelor Nail Art sub îndrumarea  specialiştilor licenţiaţi.</li>
<li>Studiul principiilor de lucru pe bază  produselor E.Mi de înaltă calitate.</li>
<li>Programe universale destinate specialiştilor  fără studii în artă.</li>
</ul>
<h2>Ce se include în formarea profesională?</h2>
<p>Oficii reprezentantive ale”Școlii de Nail Design by Ekaterina Miroshnichenko” pînă în prezent au fost deschise în Rusia, CSI și alte țări străine.  Cursuri privind nail design în Republica Moldova puteţi să le faceţi la Chişinău  în conformitate cu programul prevăzut. </p>
<p>La cursuri veți învăța tehnicile de bază ale tehnologiei Nail Art,  veţi practica acestea tehnici şi veţi obține sfaturi practice şi îndrumări din partea profesioniştilor.</p>
<p>La finele instruirei Nail Design veţi putea realiza idei creative interesante,  o să aveţi posibilitate să oferiţi  clienţilor sai noi servicii și chiar să participaţi la concursuri profesionale la cel mai înalt nivel.</p>
<p>Cursanţii se vor putea baza pe sprijinul nostru şi după încheierea cursurilor.</p>
</div><br>
<?}?>