
<h2 class="heading">E.Mi
    <br />
</h2>

<div><span class="subheading"><font size="4"> </font>
    <div><font size="4">E.Mi is an international brand of stylish studio manicure, the favorite of superstars, nail industry leaders, and fashionistas in 28 countries all over the world.</font></div>

        <br />

    <div><font size="4">E.Mi is well-loved for its high-quality products, a never-ending stream of product innovation, unique designs, and comprehensive business solutions for beauty studios.</font></div>
</div>

<h2 class="heading">A global brand
    <span class="subheading">
</span></h2>

<div><span class="subheading"><font size="4"> </font>
    <div><font size="4">E.Mi is a global brand in every sense of the word, working with the best suppliers of raw materials from Europe, South Korea, and the USA to create the best products.</font></div>
</div>
