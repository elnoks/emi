
<h2 class="heading">Vera Miroshnichenko
 </h2>

<div>
    <div><font size="4">Vera is the brand’s co-founder, head product engineer of E.Mi Gel system, and a business trainer with 15 years of experience in the beauty salon industry. She is also the author of E.Mi Business School’s programs for the beauty studio owners – “How to attract and keep your customers”, “How to attract and keep your personnel”, “Efficient beauty salon marketing”, and “Beauty salon budgeting and planning”.

        </font></div>

    <div>
        <br />
    </div>

    <div>
        <br />
    </div>
</div>
