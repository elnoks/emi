﻿<?if ($_SERVER['REQUEST_URI']=='/courses/') {?>
<div>
	<p>Școala Ekaterinei Miroshnichenko oferă cursuri unice  de nail design. Cursurile sunt susținute de către experți cu experiență în tehnici originale. Aveți oportunitatea  unică de a vă îmbunătăți abilitățile sau de a începe o nouă profesie. Cursurile de formare sunt împărțite în 3 niveluri de complexitate. Alegeți un curs de o complexitate corespunzătoare pregatirii dumneavoastra și începeți instruirea cât mai repede.</p>
<p>Ekaterina Miroshnichenko creează în mod regulat noi tehnici și modele de nail design, pe care le împărtășeste cu studenții săi în cadrul cursurilor.</p>
<h2>Principalele avantaje ale cursurilor de nail design sunt:</h2>
<ul>
<li>învățarea tendințelor în nail art sub îndrumarea profesioniștilor.</li>
<li>învățarea principiilor de bază și a caracteristicilor  produsele E.Mi de înaltă calitate.</li>
<li>programe generale dezvoltate pentru specialiști, fără cunoștinte in domeniul artei.</li>
</ul>
<h2>Ce cuprinde cursul de specializare?</h2>
<p>În prezent, avem reprezentanți ai școalii în  Rusia, în CSI, Europa și alte țări.  Aveti posibilittaea de a  urma cursuri de modelare și corectie  a unghiilor la  School of nail design by Ekaterina Miroshnichenko  în  Cluj-Napoca, București  și în alte orașe,  în conformitate cu programul reprezentantei din România. </p>
<p>În timpul cursului veți învăța principalele tehnici de modelare a  unghiilor, veti lucra asupra perfomantei dumneavoastră si veți primi sfaturi practice și feedback de la instructorii Scolii.</p>
<p>După încheierea cursului de modelarea  unghiilor veti fi capabil/a să vă puneți în practică ideile creative,  să oferiți clienților dumneavoastră servicii noi și chiar să participati la concursuri profesionale la un nivel înalt.</p>
</div><br>
<?}?>