
<h2 class="heading">E.Mi brand
  <br />
 <span class="subheading">The history of success.</span></h2>
 
<div><span class="subheading"><font size="4"> </font> 
    <div><font size="4">The E.Mi brand presents products of high quality for nail designs by Ekaterina Miroshnichenko. All peculiarities and nuances of nail design are taken into account.</font></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div><font size="4">E.Mi is a range of gel paints and brushes specially produced in Germany, as well as nail design decorations and brand accessories. The E.Mi product is so handy that you can realize all your wild ideas.
Gel paints polymerization takes a minute and some paints are polymerized in few seconds that significantly reduces the operating time. </font></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div><font size="4">Design can be applied to all the nails. Special texture of gel paint doesn’t allow them to flow and you will have the desired result. It’s easy to be a first-class master with the E.Mi products!</font></div>
   </span></div>
