<a href="https://www.facebook.com/e.mi.schoolsouthafrica/" target="_blank" title="E.MI School South Africa. Distribution and training">
	<i class="icon-social fb"></i>
</a>
<a href="https://www.instagram.com/e.mi_southafrica/?hl=en" target="_blank" title="Instagram E.MI School South Africa">
	<i class="icon-social in"></i>
</a>
<a href="http://www.youtube.com/user/emischoolcom/featured" target="_blank" title="Channel of the School of Ekatherina Miroshnichenko on YouTube YouTube">
	<i class="icon-social yt"></i>
</a>