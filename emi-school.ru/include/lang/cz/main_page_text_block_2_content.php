
<h2 class="heading">E.Mi
    <br />
</h2>

<div><span class="subheading"><font size="4"> </font>
    <div><font size="4">E.Mi je mezinárodní značka módní salónní manikúry. E.Mi volí hvězdy, lídři nehtového průmyslu a ženy, co mají styl, ve 28 zemích světa.</font></div>

        <br />

    <div><font size="4">E.Mi je oblíbená díky evropské kvalitě, neustálým inovacím při výrobě materiálů, unikátnímu přístupu k designu nehtů a komplexním byznys řešením pro salóny krásy.</font></div>
</div>

<h2 class="heading">Globální značka
    <span class="subheading">
</span></h2>

<div><span class="subheading"><font size="4"> </font>
    <div><font size="4">E.Mi je globální značka. K výrobě svých produktů využívá suroviny dodávané předními světovými dodavateli z Evropy, Jižní Koreje a USA.</font></div>
</div>
