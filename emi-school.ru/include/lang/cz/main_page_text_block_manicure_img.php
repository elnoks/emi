
    <div id="table-tent-slider" class="owl-carousel owl-theme">
        <div class="item">
            <a id="table_tent_1" href="#">
                <img src="include/img/tabletent/table_tent_1.png" alt="">
            </a>
            <span class="slide-text">
							Sbírka E.Mi-manikúra jaro-léto 2018
						</span>
        </div>
        <div class="item">
            <a id="table_tent_2" hreg">
                <img src="include/img/tabletent/table_tent_2.png" alt="">
            </a>
            <span class="slide-text">
							Sbírka E.Mi-manikúra jaro-léto 2018
						</span>
        </div>
        <div class="item">
            <a id="table_tent_3" hreg">
                <img src="include/img/tabletent/table_tent_3.png" alt="">
            </a>
            <span class="slide-text">
							Sbírka E.Mi-manikúra jaro-léto 2018
						</span>
        </div>
        <div class="item">
            <a id="table_tent_4" hreg">
                <img src="include/img/tabletent/table_tent_4.png" alt="">
            </a>
            <span class="slide-text">
							Sbírka E.Mi-manikúra jaro-léto 2018
						</span>
        </div>
        <div class="item">
            <a id="table_tent_5" hreg">
                <img src="include/img/tabletent/table_tent_5.png" alt="">
            </a>
            <span class="slide-text">
							Sbírka E.Mi-manikúra jaro-léto 2018
						</span>
        </div>
    </div>


<script>
    $(document).ready(function() {
        $("#table-tent-slider").owlCarousel({
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            },
            loop: true,
            autoHeight: true,
            stagePadding: 0,
            autoPlay: true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            lazyLoad : true,
            margin: 30,
            navigation: false,
            pagination: false,
            dots: false
        });

    });
</script>