<h2 class="heading">Ekaterina Miroshnichenko
  <br />
 <span class="subheading">Od manikúry až po světovou šampionku</span></h2>
 
<div> 
  <div><font size="4">Zakladatelka a hlavní technoložka značky E.Mi, autor a tvůrce technologií „Smaltované zlato“ (2009), „Imitace hadí kůže“ (2009), „Efekt rozbitého skla a etnické vzory“ (2010), „Sametový písek a tekuté kameny“ (2012), „3D Vintage“ (2013), TEXTONE&Combiture (2014) a unikátních materiálů, sloužících k jejich vytváření: „Černý tulipán“, „Sametový písek“, GLOSSEMI, EMPSATA, TEXTONE, PRINCOTE; světová šampionka v oblastí nehtového designu (Paříž 2010), dvojnásobná šampionka Evropy (Atény, Paříž 2009), porotkyně mezinárodních soutěží.</font></div>
 
  <div> 
    <br />
   </div>
 
  <div> 
    <br />
   </div>
 </div>
