﻿<?if ($_SERVER['REQUEST_URI']=='/courses/') {?>
<div>
	<p>Škola nehtového designu Ekateriny Miroshnichenko nabízí trendové kurzy nehtového designu. Lekce probíhají pod vedením zkušených manikérek podle autorských metod. Máte jedinečnou možnost zvýšit svou kvalifikaci nebo získat nové povolání. Vzdělávací programy jsou rozdělené do 3 úrovni. Vyberte si pro sebe kurzy, které odpovídají Vaši úrovni a začněte se vzdělávat v co nejbližším termínu.</p>
<p>Ekaterina Miroshnichenko pravidelně nabízí nové techniky a možnosti stylistické úpravy nehtů, které předává svým posluchačům v rámci vzdělávání v oblasti nehtového designu.</p>
<h2>Hlavní výhody kurzů nehtového designu:</h2>
<ul>
<li>Osvojení aktuálních trendů pod vedením profesionálů</li>
<li>Pochopení principů práce s kvalitními výrobky E.Mi</li>
<li>Univerzální programy, určené specialistům, které nemají výtvarné vzdělání.</li>
</ul>
<h2>Co obsahuje příprava?</h2>
<p>Filiálky školy dneska existují v Rusku, sousedících státech a dalších zemích světa. Absolvovat kurzy nehtového designu můžete na základě rozvrhu nejbližší k Vám filiálky.</p>
<p>Na lekcích se seznámíte se základními technikami vybraného stylu nehtového designu, natrénujete jejich provedení, získáte praktické rady a pomoc od lektorů. </p>
<p>Po ukončení kurzů nehtového designu budete moct realizovat zajímavé tvůrčí nápady, samostatně navrhnout klientům nové služby a zúčastnit se profesionálních soutěží na vysoké úrovni.</p>
</div><br>
<?}?>