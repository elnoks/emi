<h2 class="heading">Ekaterina Mirošnitšenko
  <br />
 <span class="subheading">Maniküüri meistrist maailmameistrini</span></h2>
 
<div> 
  <div><font size="4">E.Mi brändi asutaja ja juhtiv tehnoloog, tehnoloogiate „Kuldvalamine“ (2008), „Reptiiliate naha imitatsioon“ (2009), „Krakelüüri efekt ja etnilised mustrid“ (2010), „Sametliiv ja vedelad kivid“ (2012), „Ruumiline vinatage“ (2013), TEXTONE&Combiture (2014) ning nende loomiseks vajalike materjalide „Must tulp“, „Sametliiv“, GLOSSEMI, EMPASTA, TEXTONE, PRINCOT autor ja väljatöötaja. Küünedisaini maailmameister (Pariis, 2010), kahekordne Euroopa meister (Ateena, Pariis 2009), rahvusvahelise tasandi kohtunik.</font></div>
 
  <div> 
    <br />
   </div>
 
  <div> 
    <br />
   </div>
 </div>
