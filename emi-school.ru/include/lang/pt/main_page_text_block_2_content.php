
<h2 class="heading">Marca E.Mi
  <br />
 <span class="subheading">A história de sucesso.</span></h2>
 
<div><span class="subheading"><font size="4"> </font> 
    <div><font size="4">A marca E.Mi apresenta produtos de alta qualidade para designs de unhas por Ekaterina Miroshnichenko. Todas as peculiaridades e nuances de design de unhas são tidos em conta.</font></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div><font size="4">E.Mi é uma gama de géis de cor e pincéis especialmente produzidos na Alemanha, bem como decorações de design de unhas e acessórios da marca. O produto E.Mi é tão manuseável que você pode realizar todas as suas idéias extravagantes. A catalisação dos géis de cor demora um minuto e algumas cores são catalisadas em poucos segundos, o que reduz significativamente o tempo de execução.</font></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div><font size="4">Os designs podem ser aplicados a todas as unhas. A textura especial dos géis de cor faz com que não escorram e você obterá o resultado desejado. É fácil ser técnica de primeira classe com os produtos E.Mi!</font></div>
   </span></div>
