﻿<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

?>
<div>
	<form method="GET" action="">
		<b>Код сайта:</b><br>
		<input name="siteCode" type="text" size="40" value="<?= isset($_REQUEST["siteCode"]) ? $_REQUEST["siteCode"] : ""?>"><br>
		<b>Код партнера:</b><br>
		<input name="partnerCode" type="text" size="40" value="<?= isset($_REQUEST["partnerCode"]) ? $_REQUEST["partnerCode"] : ""?>"><br>
		<input type="submit">
	</form>
</div>

<?
if(!isset($_REQUEST["siteCode"]) || !isset($_REQUEST["partnerCode"])){
	echo "Не задан Код сайта и/или код партнера";
	return;
}
$siteCode = $_REQUEST["siteCode"];
$partnerCode = $_REQUEST["partnerCode"];
echo "<hr>";
if (CModule::IncludeModule("sale")){
	$arFilter = Array(
		"LID" => $siteCode,
		">=DATE_INSERT" => date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), mktime(0, 0, 0, date("n"), date("d")-20, date("Y")))
	);
	$rsSales = CSaleOrder::GetList(
		array("DATE_INSERT" => "DESC"),
		$arFilter,
		false,
		false,
		array("ID", "ACCOUNT_NUMBER", "PAYED", "DATE_INSERT", "USER_NAME", "USER_LAST_NAME", "USER_EMAIL", "CANCELED")
	);
}

echo "<table align='center' width='70%' rules='rows'><thead><tr>";
echo "<td>№ заказа</td><td>Оплачен</td><td>Дата</td><td>Имя клиента</td><td>E-Mail</td><td>Отменен</td>";
echo "</tr></thead>";
echo "<tbody>";
while($arSales = $rsSales -> Fetch()){
	$orderProps = CSaleOrderPropsValue::GetOrderProps($arSales['ID']);
	while($prop = $orderProps -> Fetch()){
		if($prop['CODE'] == 'PARTNER_CODE'){
			if($prop['VALUE'] == $partnerCode){
				echo "<tr>";
				echo "<td>" . $arSales['ACCOUNT_NUMBER'] . "</td>";
				echo "<td><div style='color:" . ($arSales['PAYED'] == 'Y' ? "green'><b>Да</b>" : "red'><b>Нет</b>") . "</div></td>";
				echo "<td>" . $arSales['DATE_INSERT'] . "</td>";
				echo "<td>" . $arSales['USER_NAME'] . " " . $arSales['USER_LAST_NAME'] . "</td>";
				echo "<td>" . $arSales['USER_EMAIL'] . "</td>";
				echo "<td><div style='color:" . ($arSales['CANCELED'] == 'Y' ? "red'><b>Да</b>" : "green'><b>Нет</b>") . "</div></td>";
				echo "</tr>";
			}
		break;
		}
	}
}
echo "</tbody></table>";
?>