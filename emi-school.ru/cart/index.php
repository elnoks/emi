<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(GetMessage("CART_PAGE_META_TITLE"));?>
<div class="wrapper-top">
	<div class="bredcrumbs">
		<?$APPLICATION->IncludeComponent(
			"bitrix:breadcrumb",
			"emi",
			Array(
				"START_FROM" => "0",
				"PATH" => "",
				"SITE_ID" => "-"
			)
		);?>
	</div>
</div>

<?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket", 
	"basket", 
	array(
		"COLUMNS_LIST" => array(
			0 => "NAME",
			1 => "DISCOUNT",
			2 => "PROPS",
			3 => "DELETE",
			4 => "DELAY",
			5 => "PRICE",
			6 => "QUANTITY",
		),
		"OFFERS_PROPS" => "",
		"PATH_TO_ORDER" => "/cart/order/make/",
		"HIDE_COUPON" => "N",
		"PRICE_VAT_SHOW_VALUE" => "Y",
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"USE_PREPAYMENT" => "N",
		"QUANTITY_FLOAT" => "N",
		"SET_TITLE" => "Y",
		"SHOW_WEIGHT_PROP" => "N",
		"ACTION_VARIABLE" => "action"
	),
	false
);
?>

<br />
<br />

<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => CURLANG_INCLUDE_PATH . "cart_have_question.php",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>

 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>