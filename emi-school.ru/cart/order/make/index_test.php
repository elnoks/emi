<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказы");
?>
    <div class="wrapper-top">
        <div class="bredcrumbs">
            <?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "emi",
                array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "-"
                ),
                false
            );?>
        </div>
    </div>
<?
$template = "order";
if (LANGUAGE_ID=="kz"){
    $template = "orderKZ";
}
// prgrant - подключение нового шаблона для Украины
if (LANGUAGE_ID=="ua") {
    if ($USER->IsAdmin()) {
        $template = "orderUA";
    }
}
?>

<?$APPLICATION->IncludeComponent(
    "BKV_SCHOOL:sale.order.ajax",
    ".default",
    array(
        "PAY_FROM_ACCOUNT" => "N",
        "COUNT_DELIVERY_TAX" => "N",
        "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
        "ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
        "ALLOW_AUTO_REGISTER" => "N",
        "SEND_NEW_USER_NOTIFY" => "Y",
        "DELIVERY_NO_AJAX" => "N",
        "TEMPLATE_LOCATION" => ".default",
        "PROP_1" => array(
        ),
        "PATH_TO_BASKET" => "/cart/",
        "PATH_TO_PERSONAL" => "/personal/order/",
        "PATH_TO_PAYMENT" => "/personal/order/payment/",
        "PATH_TO_ORDER" => "/personal/order/make/",
        "SET_TITLE" => "Y",
        "DELIVERY2PAY_SYSTEM" => "",
        "SHOW_ACCOUNT_NUMBER" => "Y",
        "DELIVERY_NO_SESSION" => "N",
        "DELIVERY_TO_PAYSYSTEM" => "d2p",
        "USE_PREPAYMENT" => "N",
        "PROP_3" => array(
        ),
        "PROP_4" => array(
        ),
        "ALLOW_NEW_PROFILE" => "Y",
        "SHOW_PAYMENT_SERVICES_NAMES" => "Y",
        "SHOW_STORES_IMAGES" => "N",
        "PATH_TO_AUTH" => "/login/",
        "DISABLE_BASKET_REDIRECT" => "N",
        "PRODUCT_COLUMNS" => array(
        ),
        "COMPONENT_TEMPLATE" => "order",
        "PROP_5" => array(
        )
    ),
    false
);?>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>