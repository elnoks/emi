<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказы");
?>
    <div class="wrapper-top">
        <div class="bredcrumbs">
            <?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "emi",
                array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "-"
                ),
                false
            );?>
        </div>
    </div>
<?
$template = "order";
$component = "emi-school:sale.order.ajax";
if (LANGUAGE_ID=="kz"){
    $template = "orderKZ";
	$component = "emi-school:sale.order.ajax";
}
// prgrant - подключение нового шаблона для Украины
if (LANGUAGE_ID=="ua") {
    if ($USER->IsAdmin()) {
        $template = "orderUA";
		$component = "emi-school:sale.order.ajax";
    }
}
?>

<?
/*
if (LANGUAGE_ID == "cz") {
	$APPLICATION->IncludeComponent(
	"emi-school:sale.order.ajax", 
	"order", 
	array(
        "PAY_FROM_ACCOUNT" => "N",
        "COUNT_DELIVERY_TAX" => "N",
        "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
        "ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
        "ALLOW_AUTO_REGISTER" => "N",
        "SEND_NEW_USER_NOTIFY" => "Y",
        "DELIVERY_NO_AJAX" => "N",
        "TEMPLATE_LOCATION" => ".default",
        "PATH_TO_BASKET" => "/cart/",
        "PATH_TO_PERSONAL" => "/personal/order/",
        "PATH_TO_PAYMENT" => "/personal/order/payment/",
        "PATH_TO_ORDER" => "/personal/order/make/",
        "SET_TITLE" => "Y",
        "DELIVERY2PAY_SYSTEM" => "",
        "SHOW_ACCOUNT_NUMBER" => "Y",
        "DELIVERY_NO_SESSION" => "N",
        "DELIVERY_TO_PAYSYSTEM" => "d2p",
        "USE_PREPAYMENT" => "N",
        "ALLOW_NEW_PROFILE" => "N",
        "SHOW_PAYMENT_SERVICES_NAMES" => "Y",
        "SHOW_STORES_IMAGES" => "N",
        "PATH_TO_AUTH" => "/login/",
        "DISABLE_BASKET_REDIRECT" => "N",
        "PRODUCT_COLUMNS" => array(
        ),
		"USE_CUSTOM_ADDITIONAL_MESSAGES" => "N",
        "USE_CUSTOM_ERROR_MESSAGES" => "Y",
        "USE_CUSTOM_MAIN_MESSAGES" => "N",
        "USE_PREPAYMENT" => "N",
        "USE_YM_GOALS" => "N",
        "COMPONENT_TEMPLATE" => "order",
		"USER_CONSENT" => "N",
        "USER_CONSENT_ID" => "1",
        "USER_CONSENT_IS_CHECKED" => "Y",
        "USER_CONSENT_IS_LOADED" => "N"
	),
	false
);
}else{
	*/
$APPLICATION->IncludeComponent(
	$component,
    $template,
    array(
        "PAY_FROM_ACCOUNT" => "N",
        "COUNT_DELIVERY_TAX" => "N",
        "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
        "ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
        "ALLOW_AUTO_REGISTER" => "N",
        "SEND_NEW_USER_NOTIFY" => "Y",
        "DELIVERY_NO_AJAX" => "N",
        "TEMPLATE_LOCATION" => ".default",
        "PATH_TO_BASKET" => "/cart/",
        "PATH_TO_PERSONAL" => "/personal/order/",
        "PATH_TO_PAYMENT" => "/personal/order/payment/",
        "PATH_TO_ORDER" => "/personal/order/make/",
        "SET_TITLE" => "Y",
        "DELIVERY2PAY_SYSTEM" => "",
        "SHOW_ACCOUNT_NUMBER" => "Y",
        "DELIVERY_NO_SESSION" => "N",
        "DELIVERY_TO_PAYSYSTEM" => "d2p",
        "USE_PREPAYMENT" => "N",
        "ALLOW_NEW_PROFILE" => "N",
        "SHOW_PAYMENT_SERVICES_NAMES" => "Y",
        "SHOW_STORES_IMAGES" => "N",
        "PATH_TO_AUTH" => "/login/",
        "DISABLE_BASKET_REDIRECT" => "N",
        "PRODUCT_COLUMNS" => array(
        ),
		"USE_CUSTOM_ADDITIONAL_MESSAGES" => "N",
        "USE_CUSTOM_ERROR_MESSAGES" => "Y",
        "USE_CUSTOM_MAIN_MESSAGES" => "N",
        "USE_PREPAYMENT" => "N",
        "USE_YM_GOALS" => "N",
        "COMPONENT_TEMPLATE" => "order",
		"USER_CONSENT" => "N",
        "USER_CONSENT_ID" => "1",
        "USER_CONSENT_IS_CHECKED" => "Y",
        "USER_CONSENT_IS_LOADED" => "N"
    ),
    false
);
// }
?>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>