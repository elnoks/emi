<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
$APPLICATION->SetPageProperty("TITLE", GetMessage("GALLERY_PAGE_META_TITLE"));
ini_set('error_reporting', E_ERROR );

?>
<div class="social-container">
    <div class="h1-top social">
        <h1 id="top"><?=GetMessage("GALLERY_PAGE_META_TITLE")?></h1>
    </div>

<div class="video-container shadow">

	<h2 id="video"><?=GetMessage("MAIN_PAGE_4BLOCKS_VIDEO");?></h2>
    <?

    $APPLICATION->IncludeComponent("prgrant:video.gallery", "gallery",
        Array(
            "COMPONENT_TEMPLATE" => ".default",
            "PLAYLIST_SKIN" => "right-thumb",
            "VG_PLAYER_ID" => "1",
            "VG_AUTOPLAY" => "N",	// Автоматическое воспроизведение
            "VG_PLAYLIST_SKIN" => "right-thumb",	// Вид плейлиста
            "VG_DISABLE_PANEL_TIMEOUT" => "700",	// Время скрытого плейлиста при загрузке (мс)
            "VG_VIDEO_VALUE" => array(
            ),
            "VG_VIDEO_PLAYLIST" => array(
                0 => YOUTUBE_GALLERY_SECOND_PLAYLIST,
            ),
            "VG_YOUTUBE_KEY" => "AIzaSyD7ho3783ibUiT3yMBZhLrJYgbjTmXfVt4",	// YouTube KEY (Data API v3)
        ),
        false
    );
    ?>
</div>

<div class="video-container shadow">
	<h2 id="masterclas"><?=GetMessage("MAIN_PAGE_4BLOCKS_MASTER_CLASS");?></h2>
    <?
    $APPLICATION->IncludeComponent("prgrant:video.gallery", "gallery",
        Array(
            "COMPONENT_TEMPLATE" => ".default",
            "PLAYLIST_SKIN" => "right-thumb",
            "VG_PLAYER_ID" => "2",
            "VG_AUTOPLAY" => "N",	// Автоматическое воспроизведение
            "VG_PLAYLIST_SKIN" => "right-thumb",	// Вид плейлиста
            "VG_DISABLE_PANEL_TIMEOUT" => "700",	// Время скрытого плейлиста при загрузке (мс)
            "VG_VIDEO_VALUE" => array(
            ),
            "VG_VIDEO_PLAYLIST" => array(
                0 => YOUTUBE_VIDEO,
            ),
            "VG_YOUTUBE_KEY" => "AIzaSyD7ho3783ibUiT3yMBZhLrJYgbjTmXfVt4",	// YouTube KEY (Data API v3)
        ),
        false
    );
    ?>
</div>

<?

if(file_exists('..'.CURLANG_INCLUDE_PATH.'flipbook')){
    $dir='..'.CURLANG_INCLUDE_PATH.'flipbook';
    $files=array_slice(scandir($dir),2);

    sort($files,SORT_NATURAL);
    foreach ($files as $key=>$file) {
        $files[$key]=$dir.'/'.$file;
    }
    //print_r($files);
    $filesj=json_encode($files);
?>
<div class="youtube">
    <h2 id="flipbook"><?=GetMessage("MAIN_PAGE_4BLOCKS_FLIPBOOK");?></h2>
    <?
    $APPLICATION->SetAdditionalCSS("/bitrix/templates/.default/plugins/dflip/css/dflip.css");
    $APPLICATION->SetAdditionalCSS("/bitrix/templates/.default/plugins/dflip/css/themify-icons.css");
    ?>
    <? /*
    <div class="_df_thumb" id="_df_thumb_flip"
         tags="2018"
         source="<?=GetMessage("MAIN_PAGE_4BLOCKS_FLIPBOOK_SRC");?>"
         thumb="<?=GetMessage("MAIN_PAGE_4BLOCKS_FLIPBOOK_IMG");?>" >
        <?=GetMessage("MAIN_PAGE_4BLOCKS_FLIPBOOK");?>
    </div>
    */ ?>
    <div id="flipbookContainer" class="_df_book"></div>



    <script src="/bitrix/templates/.default/plugins/dflip/js/dflip.min.js" type="text/javascript"></script>
    <script>


        var dFlipLocation = "/bitrix/templates/.default/plugins/dflip/";
        jQuery(document).ready(function () {
            var source = '<?=GetMessage("MAIN_PAGE_4BLOCKS_FLIPBOOK_SRC");?>';
            var imgs = eval('<?=$filesj?>');
            console.log(imgs);
            var options = {
                webgl: true,
                //webglShadow: true,
                soundEnable: false,
                height: 700,
                <? if (LANGUAGE_ID=="ru"){?>
                // TRANSLATION ru
                text: {
                    toggleSound: "Включить/Выключить Звуки",
                    toggleThumbnails: "Включить Эскизы",
                    toggleOutline: "Включить Контур/Закладка",
                    previousPage: "Предыдущая страница",
                    nextPage: "Следующая страница",
                    toggleFullscreen: "Полноэкранный режим",
                    zoomIn: "Приблизить",
                    zoomOut: "Отдалить",
                    toggleHelp: "Включить Помощь",

                    singlePageMode: "Одностраничный Режим",
                    doublePageMode: "Двустраничный Режим",
                    downloadPDFFile: "Скачать PDF файл",
                    gotoFirstPage: "Перейти на первую страницу",
                    gotoLastPage: "Перейти на последнюю страницу",
                    play: "Запуск автовоспроизведения",
                    pause: "Пауза автовоспроизведения",

                    share: "Поделиться"
                },
                <?}?>
                // allControls: "altPrev,pageNumber,altNext,play,outline,thumbnail,zoomIn,zoomOut,fullScreen,share,download,more,pageMode,startPage,endPage,sound",
                // moreControls: "download,pageMode,startPage,endPage,sound",
                // hideControls: "",
                scrollWheel: false,
                pdfRenderQuality: 0.90,
                backgroundColor: "#fff",
                duration: 600
            };

            var flipBook = $("#flipbookContainer").flipBook(imgs, options);

        });

    </script>
</div>
<? } ?>

	
<!-- publications-->
<?
$arrFilterPublications = array("SECTION_CODE" => "publications");
$arrFilterPhoto = array("SECTION_CODE" => "photo");
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"publications", 
	array(
		"TITLE" => GetMessage("GALLERY_PAGE_BLOCKS_PUBLIC"),
		"IBLOCK_ID" => GALLERY_IBLOCK_ID,
		"NEWS_COUNT" => "100", //BOGDAN 19.12.16 было 15, сделал 100
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "",
		"SORT_ORDER2" => "",
		"FILTER_NAME" => "arrFilterPublications",
		"FIELD_CODE" => array(
			0 => "DETAIL_PICTURE",
			1 => "",
			2 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "PROPERTY_PUB_PAGES",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Галерея",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"IBLOCK_TYPE" => "news",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>
<!-- /.publications -->	
<?
//ajax photo gallery
$APPLICATION->IncludeFile("/include/ajax/photo_gallery.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>