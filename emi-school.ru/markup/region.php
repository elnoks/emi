<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<link rel="stylesheet" href="css/region.css">
<link rel="stylesheet" href="reveal/reveal.css">
<script type="text/javascript" src="reveal/jquery.reveal.js"></script>

<a href="#" data-reveal-id="myModal">Click Me For A Modal</a>

<div id="myModal" class="reveal-modal region-modal">
     <h1>Выберите свой регион</h1>
     <div class="choose-region-container clearfix">
		<form action="post">
			<div class="first-two-columns">
				<ul>
					<li><a href="#">Алтайский край</a></a></li>
					<li><a href="#">Амурская область</a></li>
					<li><a href="#">Архангельская область</a></li>
					<li><a href="#">Астраханская область</a></li>
					<li><a href="#">Байконур</a></li>
					<li><a href="#">Белгородская область</a></li>
					<li><a href="#">Брянская область</a></li>
					<li><a href="#">Владимирская область</a></li>
					<li><a href="#">Волгоградская область</a></li>
					<li><a href="#">Вологодская область</a></li>
					<li><a href="#">Воронежская область</a></li>
					<li><a href="#">Еврейская автономная область</a></li>
					<li><a href="#">Забайкальский край</a></li>
					<li><a href="#">Ивановская область</a></li>
					<li><a href="#">Иркутская область</a></li>
					<li><a href="#">Кабардино-Балкарская Республика</a></li>
					<li><a href="#">Калининградская область</a></li>
					<li><a href="#">Калужская область</a></li>
					<li><a href="#">Камчатский край</a></li>
					<li><a href="#">Карачаево-Черкесская Республика</a></li>
					<li><a href="#">Кемеровская область</a></li>
				</ul>
				<ul>
					<li><a href="#">Кировская область</a></li>
					<li><a href="#">Костромская область</a></li>
					<li><a href="#">Краснодарский край</a></li>
					<li><a href="#">Красноярский край</a></li>
					<li><a href="#">Курганская область</a></li>
					<li><a href="#">Курская область</a></li>
					<li><a href="#">Ленинградская область</a></li>
					<li><a href="#">Липецкая область</a></li>
					<li><a href="#">Магаданская область</a></li>
					<li><a href="#">Москва - 1-й округ</a></li>
					<li><a href="#">Москва - 2-й округ</a></li>
					<li><a href="#">Московская область</a></li>
					<li><a href="#">Мурманская область</a></li>
					<li><a href="#">Ненецкий автономный округ</a></li>
					<li><a href="#">Нижегородская область</a></li>
					<li><a href="#">Новгородская область</a></li>
					<li><a href="#">Новосибирская область</a></li>
					<li><a href="#">Омская область</a></li>
					<li><a href="#">Оренбургская область</a></li>
					<li><a href="#">Орловская область</a></li>
					<li><a href="#">Пензенская область</a></li>
				</ul>
			</div>
			<div class="second-two-columns">
				<ul>
					<li><a href="#">Пермский край</a></li>
					<li><a href="#">Приморский край</a></li>
					<li><a href="#">Псковская область</a></li>
					<li><a href="#">Республика Адыгея</a></li>
					<li><a href="#">Республика Алтай</a></li>
					<li><a href="#">Республика Башкортостан</a></li>
					<li><a href="#">Республика Бурятия</a></li>
					<li><a href="#">Республика Дагестан</a></li>
					<li><a href="#">Республика Ингушетия</a></li>
					<li><a href="#">Республика Калмыкия</a></li>
					<li><a href="#">Республика Карелия</a></li>
					<li><a href="#">Республика Коми</a></li>
					<li><a href="#">Республика Крым</a></li>
					<li><a href="#">Республика Марий Эл</a></li>
					<li><a href="#">Республика Мордовия</a></li>
					<li><a href="#">Республика Саха (Якутия)</a></li>
					<li><a href="#">Республика Северная Осетия - Алания</a></li>
					<li><a href="#">Республика Татарстан</a></li>
					<li><a href="#">Республика Тыва</a></li>
					<li><a href="#">Республика Хакасия</a></li>
					<li><a href="#">Ростовская область</a></li>
				</ul>
				<ul>
					<li><a href="#">Рязанская область</a></li>
					<li><a href="#">Самарская область</a></li>
					<li><a href="#">Санкт-Петербург</a></li>
					<li><a href="#">Севастополь</a></li>
					<li><a href="#">Саратовская область</a></li>
					<li><a href="#">Сахалинская область</a></li>
					<li><a href="#">Свердловская область</a></li>
					<li><a href="#">Смоленская область</a></li>
					<li><a href="#">Ставропольский край</a></li>
					<li><a href="#">Тамбовская область</a></li>
					<li><a href="#">Тверская область</a></li>
					<li><a href="#">Томская область</a></li>
					<li><a href="#">Тульская область</a></li>
					<li><a href="#">Тюменская область</a></li>
					<li><a href="#">Ульяновская область</a></li>
					<li><a href="#">Челябинская область</a></li>
					<li><a href="#">Хабаровский край</a></li>
					<li><a href="#">Ханты-Мансийский автономный округ - Югра</a></li>
					<li><a href="#">Чукотский автономный округ</a></li>
					<li><a href="#">Ямало-Ненецкий автономный округ</a></li>
					<li><a href="#">Ярославская область</a></li>
				</ul>
			</div>
		</form>
	</div>
     <a class="close-reveal-modal">&#215;</a> 
</div>




<div class="choose-region-modal" id="region-modal">

    	
  	
</div>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('a[data-reveal-id]').on('click', function(e) {
		e.preventDefault();
		var modalLocation = $(this).attr('data-reveal-id');
		$('#'+modalLocation).reveal($(this).data());
	});
	$('#myModal ul>li>a').on('click', function(){
		$('#myModal').trigger('reveal:close');
	});
});

//-->
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>