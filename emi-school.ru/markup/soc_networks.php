<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<link rel="stylesheet" href="css/soc_networks.css">
<div>
	<div class="youtube"></div>
	<div class="other-social-networks">
		<div class="vk">
			<div class="vk-school">
				<script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>
				<!-- VK Widget -->
				<div id="vk_groups"></div>
				<script type="text/javascript">
					VK.Widgets.Group("vk_groups", {mode: 2, width: "210", height: "400"}, 14869746);
				</script>
			</div>
			<div class="vk-shop"></div>
		</div>
		<div class="fb">
			<div class="fb-school">
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				<div class="fb-like-box" data-app-id="469636786396768" data-href="http://www.facebook.com/emiofficialworld" data-width="210" data-height="400" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="true" data-show-border="true"></div>
			</div>
			<div class="fb-shop"></div>
		</div>
		<div class="ok">
			<div class="ok-school">
				<div id="ok_group_widget"></div>
				<script>
				!function (d, id, did, st) {
				  var js = d.createElement("script");
				  js.src = "https://connect.ok.ru/connect.js";
				  js.onload = js.onreadystatechange = function () {
				  if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
				    if (!this.executed) {
				      this.executed = true;
				      setTimeout(function () {
				        OK.CONNECT.insertGroupWidget(id,did,st);
				      }, 0);
				    }
				  }}
				  d.documentElement.appendChild(js);
				}(document,"ok_group_widget","50582132228315","{width:210,height:400}");
				</script>
				
			</div>
			<div class="ok-shop"></div>
		</div>
		<div class="instagram">
			<div class="instagram-school"></div>
			<div class="instagram-shop"></div>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>