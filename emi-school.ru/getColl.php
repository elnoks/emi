<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 29.06.2018
 * Time: 10:07
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;
use Bitrix\Main\Mail\Event;


require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/php_interface/amo_crm_emi.php');
require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/php_interface/amo_functions.php');

$account = "https://emiofficial.amocrm.ru/";
$login = "brand@emi-school.ru";
$hash = "ea7d659ea1ed06aac97b16c084d2708e";
$coning['account']=$account;
$coning['login']=$login;
$coning['hash']=$hash;
$amoemi = new AmoEmi($account, $login, $hash);
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/log_amo_ru_mainColl.txt");


$ffcol=explode(',', $_COOKIE['ffcoll']);
foreach ($ffcol as $key => $val){
    list($vlname, $vlvalue) = explode(':', $val);
    $ffcol[$key]=$vlvalue;
}


$deals['add'] = array(
    array(
        'name'                => 'Получить коллекцию в '.$_POST['salon_name'],
        'created_at'          => time(),
        'status_id'           => 19853572,
        'responsible_user_id' => 2374885, #Пользователь Шатова
        'tags'                => $_SERVER["HTTP_HOST"] . ' коллекция в салон', #Теги
        'custom_fields'       => array(
            array(
                'id'     => 116555, #Регион
                'values' => array(
                    array(
                        'value' => $ffcol[0]
                    )
                )
            ),
            array(
                'id'     => 524033, #Салон
                'values' => array(
                    array(
                        'value' => $_POST['salon_name']
                    )
                )
            ),
            array(
                'id'     => 524035, #Адрес доставки
                'values' => array(
                    array(
                        'value' => $_POST['salon_adress']
                    )
                )
            ),
            array(
                'id'     => 107493, #тип сделки
                'values' => array(
                    array(
                        'value' => 476435
                    )
                )
            )
        )
    )
);


$resdeals = json_decode( $amoemi->set_deals($deals,$login));
$iddeals=$resdeals->_embedded->items['0']->id;

// -----------------


$nphones=array(
    $ffcol[2],
    substr($ffcol[2],2),
    preg_replace('![^0-9]+!','',$ffcol[2]),
    preg_replace('![^0-9]+!', '', substr($ffcol[2],2)),
    '8'.preg_replace('![^0-9]+!', '', substr($ffcol[2],2)),
    '+7'.preg_replace('![^0-9]+!', '', substr($ffcol[2],2))
);


$idcont=amo_search_contact($nphones, $coning)->_embedded->items['0']->id;
$resdeals=$idcont->_embedded->items['0']->leads->id;
$resdeals[]= $iddeals;

if($idcont) {

    $contacts['update'] = array(
        array(
            'id' => $idcont,
            'updated_at' => time(),
            'leads_id' => $resdeals,
        )
    );
}
else {
    $contacts['add'] = array(
        array(
            'name'                => $ffcol[1],
            'responsible_user_id' => 2374885, #Пользователь Шатова
            'tags'                => $_SERVER["HTTP_HOST"], #Теги
            'leads_id'            => array( $iddeals ),
            'custom_fields'       => array(
                array(
                    'id'     => 68239, #Примечание
                    'values' => array(
                        array(
                            'value' => $ffcol['3'],
                            'enum'  => 142091
                        )
                    )
                ),
                array(
                    'id'     => 68237,
                    'values' => array(
                        array(
                            'value' => $ffcol['2'],
                            'enum'  => 142081
                        )
                    )
                ),
                array(
                    'id'     => 134157, #Регион
                    'values' => array(
                        array(
                            'value' => $ffcol['0']
                        )
                    )
                ),
                array(
                    'id'     => 134155, #Источник
                    'values' => array(
                        array(
                            'value' => 271555
                        )
                    )
                )
            )
        )

    );
}
$rescont=$amoemi->set_contacts($contacts,$login);
AddMessage2Log('---//---$deals---//---');
AddMessage2Log(var_export($deals, true));
AddMessage2Log('---//---$contacts---//---');
AddMessage2Log(var_export($contacts, true));
AddMessage2Log('---//---$rescont---//---');
AddMessage2Log(var_export($rescont, true));
AddMessage2Log('---//---$resdeals---//---');
AddMessage2Log(var_export($resdeals, true));

?>
