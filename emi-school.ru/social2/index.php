<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<div class="social-container">
	<div class="h1-top social">
		<h1><?=GetMessage("SOCIAL_PAGE_META_TITLE")?></h1>
    </div>
	<div class="youtube">
      		<?$APPLICATION->IncludeComponent(
	"emi:youtube.feed", 
	".default", 
	array(
		"FEED_TYPE" => "PLAYLIST",
		"FEED_ID" => "PLw2vuUxscijuMJhHbcFFFU45T_g-0h0Yl"
	),
	false
);?>
	</div>
	<div class="other-social-networks">
		<div class="vk-school">
			<script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>
			<!-- VK Widget -->
			<div id="vk_groups"></div>
			<script type="text/javascript">
				VK.Widgets.Group("vk_groups", {mode: 2, width: "278", height: "800"}, 14869746);
			</script>
		</div>		
		<div class="ok-school">
			<div id="ok_group_widget"></div>
			<script>
			!function (d, id, did, st) {
			  var js = d.createElement("script");
			  js.src = "https://connect.ok.ru/connect.js";
			  js.onload = js.onreadystatechange = function () {
			  if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
			    if (!this.executed) {
			      this.executed = true;
			      setTimeout(function () {
			        OK.CONNECT.insertGroupWidget(id,did,st);
			      }, 0);
			    }
			  }}
			  d.documentElement.appendChild(js);
			}(document,"ok_group_widget","43303601242252","{width:280,height:396}");
			</script>
			
		</div>
		<div class="fb-school"> 
			<div id="fb-root"></div>
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			<div class="fb-like-box" data-app-id="469636786396768" data-href="http://www.facebook.com/emiofficialworld" data-width="280" data-height="396" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="true" data-show-border="true"></div>
		</div>
		 
		<div class="instagram-school">
			<iframe src='/inwidget/index_1.php?toolbar=false&LOGIN=emiroshnichenko' scrolling='no' frameborder='no' style='border:none;width:280px;height:396px;overflow:hidden;'></iframe>
		</div>

		<div class="instagram-shop">
			<iframe src='/inwidget/index.php?toolbar=false&LOGIN=emiroshnichenko' scrolling='no' frameborder='no' style='border:none;width:280px;height:396px;overflow:hidden;'></iframe>
		</div>
		
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>