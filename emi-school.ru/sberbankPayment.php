<?
define("STOP_STATISTICS", true);
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");
/* if(file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/ru/payment/SberbankTEST/pay.php")){
	include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/ru/payment/SberbankTEST/pay.php"); */
header("Content-type: text/plain; charset=utf-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/SBERBANK.txt"); //log
CModule::IncludeModule("sale");

$order_bitrix_id = $_POST["ORDER_ID"];

$partner_shop_id = $_POST["PARTNER_SHOP_ID"];
$ORDER_arr = CSaleOrder::GetByID($order_bitrix_id);
//AddMessage2Log(print_r($ORDER_arr, true)); // в лог
$registerUrl = 'https://securepayments.sberbank.ru/payment/rest/register.do';


//подключаем файл с логинами от площадок
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/sale/ru/payment/sberbankTEST/logins.php");

if(!array_key_exists($partner_shop_id, $logins)){ //если так вышло, что сюда попали с Партнера, у которого не указан логин и пароль для площадки Сбербанка, то возвращаем ошибку
	echo json_encode(array("errorMessage" => "Данный регион не поддерживается для оплаты картой:" . $partner_shop_id));
	return;
}

$result;

if(!empty($ORDER_arr['PS_STATUS_CODE'])){//есть ли уже какой-то код
	if($ORDER_arr['PS_STATUS_CODE'] == '1'){//1 - заказ уже зарегистрирован в платежной системе, 2 - оплачен
		$getOrderId = $ORDER_arr['PS_STATUS_DESCRIPTION']; //берем ID в ПС из PS_STATUS_DESCRIPTION, 
		$getOrderUrl = $ORDER_arr['PS_STATUS_MESSAGE']; //берем урл из PS_STATUS_MESSAGE
		$result = json_encode(array('orderId' => $getOrderId, 'formUrl' => $getOrderUrl)); //и возвращаем
	}else{
		$result = "{\"a\":\":(\"}";
	}
	//AddMessage2Log(print_r($result, true));
	echo $result; //возвращаем на страницу магазина, где юзер ткнул "Оплатить"
	
}else{//иначе регаем
	$params = array(
		'userName' => $logins[$partner_shop_id]["USERNAME"], 
		'password' => $logins[$partner_shop_id]["PASSWORD"], 
		'orderNumber' => $ORDER_arr['ACCOUNT_NUMBER'], //ACCOUNT_NUMBER это номер заказа, но ORDER_ID Это id заказа в битриксе. НЕ ПУТАТЬ
		'amount' => ($ORDER_arr['PRICE'] * 100), 
		'returnUrl' => 'http://' . $_POST['CURR_SITE_URL'] . '/sberbankResult.php' . '?orderNumber=' . $order_bitrix_id . '&'. 'partnerCode=' . $_POST['PARTNER_SHOP_ID'], 
		'failUrl' => 'http://' . $_POST['CURR_SITE_URL'] . '/sberbankResult.php' . '?orderNumber=' . $order_bitrix_id . '&'. 'partnerCode=' . $_POST['PARTNER_SHOP_ID'],
		'sessionTimeoutSecs' => '86400' //срок жизни заказа в ПС (24 часа)
	);
	
	$result = file_get_contents(
		$registerUrl,
		false,
		stream_context_create(
			array(
				'http' => array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => http_build_query($params)
				)
			)
		)
	);

	$resultToArray = json_decode($result, true);

	if(array_key_exists('errorCode', $resultToArray)){
		//AddMessage2Log(print_r("Ошибка", true));
	}else{
		//заносим инфу в заказ
		//AddMessage2Log(print_r("Пробуем внести данные в заказ N " . $order_bitrix_id, true));
		if(array_key_exists('orderId', $resultToArray)){
			//AddMessage2Log(var_dump($resultToArray));
			
			CSaleOrder::Update(
				$order_bitrix_id,
				array(
					"PS_STATUS_DESCRIPTION" => $resultToArray['orderId'],
					"PS_STATUS_MESSAGE" => $resultToArray['formUrl'],
					"PS_STATUS_CODE" => '1'
				),
				true
			);
			
			//AddMessage2Log(print_r("Вроде внесли", true));
		}
	}
	
	//AddMessage2Log(print_r($result, true));
	echo $result; //возвращаем на страницу магазина, где юзер ткнул "Оплатить"
	 
};


?>
