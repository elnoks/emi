<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->IncludeComponent(
	'bitrix:landing.pub',
	'',
	array(
		'HTTP_HOST' => $_SERVER['HTTP_HOST']
	),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);

//\Bitrix\Main\Page\Asset::addString(
//    $str="
//
//    ";
//);
?>

    <script data-skip-moving="true">
        function contactForm() {
            $('#contactform').submit(function () {
                console.log($('#basicRb'));
                var action = '/lp/php/contact-form.php';
                $("#message-info").slideUp(250, function () {
                    $('#message-info').hide();
                    $('#submit')
                        .after('<div class="loader"><div></div></div>')
                        .attr('disabled', 'disabled');
                    $.post(action, {
                            name: $('#name').val(),
                            phone: $('#phone').val(),
                            basic: $('input[id=basicRb]:checked').val(),
                            vip: $('input[id=vipRb]:checked').val(),
                                                   },
                        function (data) {
                            document.getElementById('message-info').innerHTML = data;
                            $('#message-info').slideDown(250);
                            $('#contactform .loader div').fadeOut('slow', function() {
                                $(this).remove();
                            });
                            $('#submit').removeAttr('disabled');
                            if (data.match('success') !== null) {
                                $('#contactform').slideUp(850, 'easeInOutExpo');
                            }
                        });

                });
                return false;
            });
        }

        $(document).ready(function () {
            contactForm();
        });
    </script>

    <style type="text/css">
        .bitrix-footer, .landing-block-node-subtitle, landing-block-node-card-text, .fa {
            display: none
        }
        .g-bg-img-hero {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: left top;
        }
        .g-theme-event-bg-blue-dark-v2 {
            background-color: #ffffff;
        }
        .g-bg-darkblue-opacity-0_7 {
            background-color: rgb(176, 177, 179)!important;
        }
        .landing-block-node-container {
            margin-left: 32%;
        }
        .landing-block-card-container {
            min-height: 550px;
        }
        #ticketrb {
            display:none
        }
        #buyh1 {
            color: #f50000;
            font-size: 40px;
            font-weight: 700;
        }
        .g-pa-10 {
            padding: 0 !important;
        }
        .g-mr-20 {
            margin-right: 0!important;
        }
        .navbar.navbar-expand-lg.g-py-0{
            padding: 0 15px;
        }
        #OrderSec {
            background-color: #f7f7f7
        }
        #LastBtn {
            height: 100%;
            font-size: 2.28571rem!important;
        }

        input{
            border-color: #ff2722;
            box-shadow: none;
            color: #444349;
            outline: none;
            background: #fff;
            font-size: 16px;
            box-shadow: none;
            line-height: 20px;
            padding: 14px 20px;
            margin: 15px 0;
            border-radius: 0px;
            display: inline-block;
            vertical-align: middle;
            border: 1px solid #dfdfdf;
            text-transform: none;
            box-sizing: border-box;
        }

        @media screen and (max-width: 1199px) {
            .landing-block-node-container .landing-block-node-button{
                text-align: center;
                display: inline-block;
                width: 260px;
                font-size: 28px!important;
            }
            .g-max-width-750 {
                max-width: 520px;
            }
            h2.landing-block-node-title  > span {
                font-size: 38px!important;
            }
            #MainBtn {
                text-align: center;
            }
        }
        @media screen and (min-width: 992px) and (max-width: 1199px) {
            .landing-block-node-container {
                margin-left: 48%;
            }
        }
        @media screen and (max-width: 1050px){
            .g-bg-img-hero {
                background-size: cover;
                background-repeat: no-repeat;
                background-position: left top;
                background-position-x: 15%;
            }

        }

        @media screen and (max-width: 768px){
            .landing-block-node-container{
                margin: 0 auto;
            }
            h2.landing-block-node-title  > span {
                font-size: 30px!important;
            }
            .landing-block-node-container h2{
                font-size: 23.7708px !important;
            }

            .landing-block-node-container .text-md-right{
                text-align: center;
            }
            .g-pt-150 {
                padding-top: 50px !important;
            }
            .landing-block-node-bgimg {
                background-image: none !important;
            }

            h2.landing-block-node-block-title > p > span {
                font-size: 1.71429rem!important;
            }
            .g-font-size-36 {
                font-size: 1.57143rem!important;
            }
            .g-mb-40{
                margin-bottom: 0!important;
            }
            .landing-block .g-font-size-30 {
                font-size: 1.14286rem!important;
            }
            .landing-block-node-img {
                display: none;
            }
        }
    </style>
<?

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');