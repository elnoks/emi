<?
require "./signature.php";
?>

<html>

<body>
<div align="center">

<?php
//data z getu
$data = $_GET['data'];
$sign = new CSignature("test_key.pem","changeit","test_cert.pem");
$signature = $sign->sign($data);

$f = fopen("signature.sign", "w");
fwrite($f, $signature);
fclose($f);

$f = fopen("signatureEnc.sign", "w");
fwrite($f, urlencode($signature));
fclose($f);

?>
<table border="0" cellspacing="0" cellpadding="0">
<form method="get" action="verifySign.php">
<tr>
  <th>Data:&nbsp;</th>
  <td>
    <INPUT type="text" size="100"  value="<?=$data?>" name="data"/>
  </td>
</tr>
<tr>
  <th valign="top">Podpis:&nbsp;</th>
  <td>
    <TEXTAREA name="signature" cols="80" rows="6"><?=$signature?></TEXTAREA>
  </td>
</tr>
<tr>
  <th valign="top">Podpis URLEncoded:&nbsp;</th>
  <td>
    <TEXTAREA cols="80" rows="6"><?=urlencode($signature)?></TEXTAREA>
  </td>
</tr>
<tr>
  <td colspan="2" align="right"><input type="submit" value="Over podpis"/></td>
</tr>
</form>
</table>
</div>
<body>
</html>

