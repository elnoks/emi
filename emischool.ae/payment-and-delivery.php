<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оплата и доставка");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Payment and delivery</h1>
   </div>
	</br>
	<p><b>Prices.</b></p>
	<p>If you order а ready-made package you get a discount. The package includes a course and all the necessary products for emimanicure. Call us for more details.</p>
	<br/>
	
	<p><b>Payment.</b></p>
	<p>All payments are effected on the basis of issued invoice.</p>
	<br/>

	<p><b>Shipping conditions</b></p>
	<p>• Products are shipped in Abu Dhabi, Dubai, Umm al-Quwain, Ajman, Sharjah.</p>
	<p>Are you from another region? <a href="/contacts">Contact us by phone or email</a>, we come up with a solution for you.</p>
	<p>• Free Shipping on orders over AED 1000.</p>

  <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>