<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Consent to personal data processing");
?>
<style>
    .lang {
        padding: 0 0 20px 0;
    }
    .lang a {
        display: inline-block;
        background-color: #ea516d;
        width: 30px;
        height: 30px;
        padding: 15px 7px 0px 0px;
        text-decoration: none;
        color: #fff;
        line-height: 0;
        cursor: pointer;
    }
    .lang a:hover, .lang a:focus, .lang a.active{
        background-color: #ff9daf;
    }
    .text-lang > div{
        display: none;
        -webkit-transition: all ease-out 0.2s;
        -moz-transition: all ease-out 0.2s;
        transition: all ease-out 0.2s;
    }
    .text-lang > div.active{
        display: block;
        -webkit-transition: all ease-out 0.2s;
        -moz-transition: all ease-out 0.2s;
        transition: all ease-out 0.2s;
    }
</style>
<div class="row">
    <div class="lang col-md-12 col-sm-12">
        <div class="col-md-6 col-sm-6">

        </div>
        <div class="col-md-6 col-sm-6 text-right">
            <a id="en" class="active">EN</a>
            <a id="de" >DE</a>
            <a id="it" >IT</a>
            <a id="fr" >FR</a>
            <a id="es" >ES</a>
            <a id="cz" >CZ</a>
        </div>
    </div>
</div>
<div class="text-lang">
    <div id="text_en" class="active">
        <p>
            In the case that you grant us your consent to the processing of your personal data, we will be able to contact you in the future with an offer of our products and services which correspond better to your needs. Provision of our products and services is, however, not conditioned by this consent. Of course, you can change your decision at any time and to withdraw your consent. Here we would like to state some information on the processing of your personal data.
        </p>
        <h4>1. TO WHOM DO YOU GRANT YOUR CONSENT TO THE PROCESSING OF YOUR PERSONAL DATA, OR WHO ARE WE?</h4>
        <p>You grant your consent to the processing of your personal data to the company:</p>
        <p>E.Mi – International s.r.o.</p>
        <p>Company Reg. No. 24214647</p>
        <p>Registered office: U božích bojovníků 89/1, Žižkov, 130 00 Prague 3</p>
        <p>Registered in the Commercial Register administered by the Municipal Court in Prague under the file ref. no. C 189332</p>

        <h4>2. FOR WHAT PURPOSE DO WE PROCESS YOUR PERSONAL DATA?</h4>
        <p>We process your personal data for marketing purposes which include the following activities:</p>
        <ul>
            <li>Participation in the loyalty programme,</li>
            <li>Informing about new products and special offers, as well as sending invitations to corporate events (exhibitions, E.MiDay); we can send you offers on the basis of your consent electronically, especially by way of e-mail messages or messages sent to a mobile device through a phone number, through the web client zone, in a written form or by way of phone calls,</li>
            <li>Automated processing of personal data with a view to adapting a commercial offer to your individual needs,</li>
            <li>Organising various surveys of satisfaction with our products and services.</li>
        </ul>

        <h4>3. WHAT PERSONAL DATA WILL WE PROCESS ON THE BASIS OF THIS CONSENT PROVIDED BY YOU?</h4>
        <p>On the basis of this consent we will process the following personal data:</p>
        <ul>
            <li>First name(s), surname, title,</li>
            <li>Place of residence/business,</li>
            <li>Date of birth,</li>
            <li>Data from the documents submitted,</li>
            <li>Payment data and payment behaviour data,</li>
            <li>Telephone and e-mail connection data,</li>
            <li>Information identifying the equipment or the person connected with the use of the equipment, namely the IP address.</li>
        </ul>


        <h4>4. WHY DO PROFILING AND AUTOMATED DECISION MAKING TAKE PLACE?</h4>
        <p>We try to provide you with the most suitable personalised offers of products and services and not to bother you with the offer of all our products. For this reason we profile your personal data on the basis of the consent granted by you, to which we use automatic information systems or web applications. In practice this means that still before offering a product or service to you, we at first assess, according to your personal data, what needs you could have and according to the results we will send you personalised offers of products and services.</p>
        <p>The automated assessment (profiling) of personal data helps us to get better knowledge of you and your needs and according to this fact also to adapt our products and services so that they can be tailored to you.</p>

        <h4>5. FOR HOW LONG WILL WE PROCESS YOUR PERSONAL DATA?</h4>
        <p>We will process your personal data for up to 10 years from the day when you provide this consent to the processing of your data to us.</p>

        <h4>6. CAN ANYBODY ELSE PROCESS YOUR PERSONAL DATA FOR US?</h4>
        <p>Your personal data are processed by us, however, in order to be able to fulfil the above mentioned purpose of the processing of your personal data, your personal data are processed for us also by the following processors – providers of web sites, e-mail, SMS correspondence, on-line chat, statistical and monitoring systems.</p>

        <h4>7. CAN YOU WITHDRAW YOUR CONSENT TO THE PROCESSING OF PERSONAL DATA?</h4>
        <p>Yes, you can withdraw your consent to the processing of your personal data at any time, by clicking on the corresponding field in your account.</p>
        <p>At the same time we state that the non-granting of this content or its withdrawal does not have any impact on provision of our products or services.</p>

        <h4>8. WHAT ARE YOUR RIGHTS IN RELATION TO THE PERSONAL DATA PROCESSED?</h4>
        <p>You have the right to:</p>
        <ul>
            <li>Access to your personal data – or you have the right to ask us for the information whether we process your personal data or not, and if yes, what of your personal data we process and for what purpose, to what other entities we make your personal data accessible, for how long your personal data will be stored at our company, whether automated decision making, including profiling, is carried out or not</li>
            <li>Explanation, correction or completion of your personal data – if you think that your personal data which we process are inexact, you have the right to require explanation and removal of the state which has arisen, especially through blocking, correction, completion or erasure of personal data</li>
            <li>Erasure of your personal data,</li>
            <li>Limitation of the processing of your personal data,</li>
            <li>Portability of your personal data – you have the right to obtain the personal data concerning you and which you have provided to us, in a structured, commonly used and machine-readable format, for the purpose of the transferring thereof to another data controller</li>
            <li>Contact the Office for Personal Data Protection – in the case that you have doubts about compliance with the obligations associated with the processing of your personal data, you have the right to contact the Office for Personal Data Protection (www.uoou.cz)</li>
        </ul>
        <p>If you think that your personal data are processed by us or by a processor authorised by us in contradiction with the purpose for which the data were provided to us, you have the right to raise an objection against the processing of personal data for the purpose of direct marketing, including profiling, as far as the direct marketing is concerned.</p>


        <h4>9. DECLARATION</h4>
        <p>You declare that if you are younger than 16, you have asked your legal guardian for the consent to the processing of your personal data.</p>
        <p>You declare that the personal data provided are true, were provided freely, knowingly, voluntarily and on the basis of your own decision.</p>

        <p>Additional information concerning the processing of personal data is provided for in the document <strong>Information Note on Processing of Personal Data</strong>. The current information concerning the processing of personal data, including the information about personal data recipients, is always stated <a target="_blank" href="https://emischool.com/legal/processing_personal_data.php">here</a></p>
    </div>
    <div id="text_de">
        <p>
            Wenn Sie uns Ihre Einwilligung in die Verarbeitung Ihrer personenbezogenen Daten erteilen, werden wir in der Lage sein, Sie in Zukunft mit dem Angebot unserer Produkte und Dienstleistungen anzusprechen, das Ihren Bedürfnissen am besten entspricht. Die Bereitstellung unserer Produkte und Dienstleistungen ist durch diese Einwilligung jedoch nicht bedingt. Natürlich können Sie Ihre Entscheidung jederzeit ändern und Ihre Einwilligung widerrufen. Wir erlauben uns, hier einige Informationen zur Verarbeitung Ihrer personenbezogenen Daten anzuführen.        </p>
        <h4>1. WEM GEBEN SIE IHRE EINWILLIGUNG IN DIE VERARBEITUNG IHRER PERSONENBEZOGENEN DATEN ODER WER SIND WIR?</h4>
        <p>Die Einwilligung in die Verarbeitung Ihrer personenbezogenen Daten geben Sie der Gesellschaft:</p>
        <p>E.Mi – International s.r.o.</p>
        <p>Ident.-Nr. 24214647</p>
        <p>mit Sitz U božích bojovníků 89/1, Žižkov, 130 00 Praha 3</p>
        <p>eingetragen im Handelsregister beim Stadtgericht in Prag unter dem Az. C 189332</p>

        <h4>2. ZU WELCHEM ZWECK VERARBEITEN WIR IHRE PERSONENBEZOGENE DATEN?</h4>
        <p>Ihre personenbezogenen Daten verarbeiten wir für Marketingzwecke. Hierher gehören folgende Tätigkeiten:</p>
        <ul>
            <li>Teilnahme am Treueprogramm,</li>
            <li>Informationen zu neuen Produkten und Sonderangeboten sowie die Versendung von Einladungen zu Firmenveranstaltungen (Ausstellungen, E.MiDay); aufgrund Ihrer Einwilligung können wir Ihnen die Angebote sowohl elektronisch, insbesondere per E-Mail oder mittelst Nachrichten an ein mobiles Gerät über eine Telefonnummer, als auch über die Webkunden-Zone, schriftlich oder telefonisch zustellen,</li>
            <li>Automatisierte Verarbeitung der personenbezogenen Daten mit dem Ziel, das Geschäftsangebot an Ihre individuelle Bedürfnisse anzupassen,</li>
            <li>Zufriedenheitsumfragen bezüglich unserer Produkte und Dienstleistungen.</li>
        </ul>

        <h4>3. WELCHE PERSONENBEZOGENE DATEN WERDEN WIR AUF GRUNDLAGE DIESER IHRER EINWILLIGUNG VERARBEITEN?</h4>
        <p>Auf Grundlage dieser Einwilligung werden wir die folgenden personenbezogenen Daten verarbeiten:</p>
        <ul>
            <li>Vorname, Nachname, Titel,</li>
            <li>Wohnort/Geschäftssitz,</li>
            <li>Geburtsdatum,</li>
            <li>Angaben aus vorgelegten Dokumenten,</li>
            <li>Angaben zu Zahlungen und zur Zahlungsmoral,</li>
            <li>Telefonnummer und E-Mail-Adresse,</li>
            <li>Informationen, die das Gerät oder die mit der Verwendung des Geräts verbundene Person identifizieren, nämlich die IP-Adresse.</li>
        </ul>


        <h4>4. WARUM FINDET DAS PROFILING UND DIE AUTOMATISIERTE ENTSCHEIDUNGSFINDUNG STATT?</h4>
        <p>Wir bemühen uns, Ihnen die möglichst geeignetsten personalisierten Angebote an Produkten und Dienstleistungen anzubieten und Sie nicht mit allen unseren Produkten zu belasten. Aus diesem Grunde profilieren wir auf Grundlage Ihrer Einwilligung Ihre personenbezogenen Daten und nutzen hierbei automatische Informationssysteme oder Webanwendungen. In der Praxis bedeutet dies, dass noch bevor wir Ihnen ein Produkt oder eine Dienstleistung anbieten, wir zuerst gemäß Ihren personenbezogenen Daten auswerten, welche Bedürfnisse Sie haben könnten, und auf Grundlage dessen versenden wir Ihnen personalisierte Angebote an Produkten und Dienstleistungen.</p>
        <p>Die automatisierten Auswertungen (Profiling) personenbezogener Daten helfen dabei, Sie und Ihre Bedürfnisse näher kennenzulernen und dementsprechend unsere Produkte und Dienstleistungen individuell anzupassen.</p>

        <h4>5. WIE LANGE WERDEN WIR IHRE PERSONENBEZOGENEN DATEN VERARBEITEN?</h4>
        <p>Ihre personenbezogenen Daten werden wir für einen Zeitraum von 10 Jahren ab dem Tag, an dem Sie uns diese Einwilligung in die Verarbeitung Ihrer Daten geben, verarbeiten.</p>

        <h4>6. KANN IHRE PERSONENBEZOGENEN DATEN JEMAND ANDERS FÜR UNS VERARBEITEN?</h4>
        <p>Ihre personenbezogenen Daten werden von uns verarbeitet. Damit wir jedoch den oben beschriebenen Zweck der Verarbeitung Ihrer personenbezogenen Daten erfüllen können, verarbeiten für uns die personenbezogenen Daten auch die folgenden Auftragsverarbeiter – Dienstleister von Internetseiten, E-Mail, SMS-Korrespondenz, Online-Chats, statistischen und Monitoring-Systemen.</p>

        <h4>7. KÖNNEN SIE IHRE EINWILLIGUNG IN DIE VERARBEITUNG IHRER PERSONENBEZOGENEN DATEN WIEDERRUFEN?</h4>
        <p>Ja, Ihre Einwilligung in die Verarbeitung Ihrer personenbezogenen Daten können Sie jederzeit durch Anklicken des entsprechenden Felds in Ihrem Konto widerrufen.</p>
        <p>Gleichzeitig weisen wir darauf hin, dass eine Nichterteilung oder der Widerruf dieser Einwilligung keinen Einfluss auf die Bereitstellung unserer Produkte oder Dienstleistungen hat.</p>

        <h4>8. WELCHE RECHTE HABEN SIE IN BEZUG AUF DIE VERARBEITUNG DER PERSONENBEZOGENEN DATEN?</h4>
        <p>Sie haben das Recht auf:</p>
        <ul>
            <li>Auskunft über Ihre personenbezogenen Daten – das bedeutet, Sie haben das Recht, von uns die Information darüber zu verlangen, ob wir Ihre personenbezogenen Daten verarbeiten oder nicht, und falls ja, welche personenbezogene Daten wir von Ihnen verarbeiten, für welchen Zweck, an welche Subjekte wir diese Ihre personenbezogenen Daten offenlegen, für welchen Zeitraum werden Ihre personenbezogenen Daten gespeichert und ob eine automatisierte Entscheidungsfindung, einschließlich des Profilings erfolgt</li>
            <li>Recht auf Erklärung, Berichtigung, bzw. Ergänzung Ihrer personenbezogenen Daten – wenn Sie vermuten, dass Ihre personenbezogenen Daten, die von uns verarbeitet werden, unvollständig sind, haben Sie das Recht, eine Erklärung und Beseitigung des entstandenen Zustands zu verlangen, insbesondere durch Sperre, Berichtigung, Ergänzung oder Löschung der personenbezogenen Daten</li>
            <li>Recht auf Löschung Ihrer personenbezogenen Daten,</li>
            <li>Recht auf Einschränkung Ihrer personenbezogenen Daten,</li>
            <li>Recht auf Datenübertragbarkeit Ihrer personenbezogenen Daten – Sie haben das Recht, die personenbezogenen Daten, die Sie betreffen und die Sie uns bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten, damit diese an einen anderen Verantwortlichen übermittelt werden könne</li>
            <li>Recht, sich an die Behörde für den Datenschutz zu wenden – falls Sie Zweifel an der Einhaltung von Pflichten in Zusammenhang mit der Verarbeitung Ihrer personenbezogenen Daten haben, haben Sie das Recht, sich an die Behörde für den Datenschutz zu wenden (www.uoou.cz)</li>
        </ul>
        <p>Falls Sie vermuten, dass wir oder ein von uns beauftragter Auftragsverarbeiter Ihre personenbezogenen Daten im Widerspruch zu dem Zweck, für den uns die Daten bereitgestellt wurden, verarbeitet, haben Sie das Recht, Widerspruch gegen die Verarbeitung der personenbezogenen Daten für Zwecke des direkten Marketing einschließlich des das direkte Marketing betreffende Profilings einzulegen.</p>


        <h4>9. ERKLÄRUNG</h4>
        <p>Sie erklären, dass Sie Ihren gesetzlichen Vertreter um die Einwilligung in die Verarbeitung Ihrer personenbezogenen Daten ersucht haben, falls Sie unter 16 Jahre alt sind.</p>
        <p>Sie erklären, dass die bereitgestellten personenbezogenen Daten richtig sind, dass sie frei, bewusst, freiwillig und auf Grundlage Ihrer eigenen Entscheidung bereitgestellt wurden.</p>

        <p>Weitere Informationen zur Verarbeitung personenbezogener Daten sind im Dokument <strong>Information Note on Processing of Personal Data</strong> angeführt. Aktuelle Informationen zur Verarbeitung personenbezogener Daten sowie Informationen über die Empfänger der personenbezogenen Daten werden stets auf der <a target="_blank" href="https://emischool.com/legal/processing_personal_data.php">Website</a> angeführt</p>
    </div>
    <div id="text_it">
        <p>
            Nel caso in cui ci concediate il Vostro consenso all’elaborazione dei Vostri dati personali saremo in grado di rivolgerci a Voi nel prossimo futuro con l’offerta dei nostri prodotti e servizi che corrisponderà meglio ai Vostri fabbisogni. La concessione dei nostri prodotti e servizi non è naturalmente condizionata da tale scelta. E’ ovvio che Voi siete liberissimi di cambiare la Vostra opinione e revocare la Vostra decisione in qualsiasi momento. A questo punto ci permettiamo di riportare alcune informazioni relative all’elaborazione dei Vostri dati personali.
        </p>
        <h4>1. A CHI DATE IL CONSENSO ALL’ELABORAZIONE DEI VOSTRI DATI PERSONALI;  DETTO IN ALTRE PAROLE -  CHI SIAMO NOI?</h4>
        <p>Il consenso all’elaborazione dei Vostri dati personali viene dato alla società:</p>
        <p>E.Mi – International s.r.o.</p>
        <p>IČO. 24214647</p>
        <p>con sede in via U božích bojovníků 89/1, Žižkov, CAP 130 00, Praha 3 </p>
        <p>iscritta al registro commerciale gestito presso il Tribunale civico di Praga, n° rep. C 189332</p>

        <h4>2. A QUALE SCOPO NOI ELABORIAMO I VOSTRI DATI PERSONALI?</h4>
        <p>I Vostri dati personali vengono elaborati per le finalità del marketing che racchiude in sé le seguenti attività:</p>
        <ul>
            <li>partecipazione al programma fedeltà</li>
            <li>informazioni su nuovi prodotti e sulle offerte speciali nonché l’invio di inviti agli incontri e eventi vari delle società (esposizioni, E.MiDay); le offerte Vi potranno essere inviate in base al Vostro consenso sia in via elettronica e, soprattutto sotto forma di messaggi e-mail sui supporti mobili per mezzo del numero telefonico, per mezzo della zona web del cliente e inoltre in forma scitta e sotto forma di chiamate telefoniche, </li>
            <li>elaborazione automatizzzata dei dati personali con l’obiettivo di adattare l’offerta commerciale ai Vostri fabbisogni  individuali,</li>
            <li>organizzazioni di ricerche per conoscere la soddisfazione di nostri prodotti e servizi.</li>
        </ul>

        <h4>3. QUALI DATI PERSONALI VERRANNO ELABORATI IN BASE A QUESTO VOSTRO CONSENSO?</h4>
        <p>In base a questa approvazione si pensa al trattamento di seguenti dati personali:</p>
        <ul>
            <li>nome, cognome, titolo,</li>
            <li>luogo di residenza/luogo esercizio attività, </li>
            <li>data di nascita, </li>
            <li>dati rilevati dalla documentazione presentata, </li>
            <li>dati sui pagamenti e sulla morale di pagamento, </li>
            <li>indirizzi telefonici e l’ e-mail, </li>
            <li>informazioni che servono a identificare l’impianto oppure la persona incaricata di utilizzare l’impianto compreso l’indirizzo IP</li>
        </ul>


        <h4>4. PERCHÈ VENGONO EFFETTUATE LA PROFILAZIONE E LA DECISIONE AUTOMATIZZATA?</h4>
        <p>Stiamo cercando di presentarVi le offerte più vantaggiose individualizzate di prodotti e servizi e non farvi sostenere l’onere riguardante l’offerta di tutti i nostri prodotti. Per tale motivo i Vostri dati personali vengono profilati, ovviamente in base al Vostro consenso; per tale scopo utilizziamo i sistemi informatici automatici oppure le applicazioni web. Ciò significa in pratica che ancor prima di offrirVi un prodotto o un servizio, noi valutiamo in primo luogo ed in base ai Vostri dati personali, quali potrebbero essere i Vostri fabbisogni e, in base a questi dati Vi inviamo le offerte individualizzate di prodotti e di servizi.</p>
        <p>La valutazione automatizzata (perizia) dei dati personali ci permette di conoscere più da vicino Voi, i Vostri fabbisogni e, stando a quanto sopra, di essere in grado di adattare i nostri prodotti e servizi per Voi per così dire a pennello.</p>

        <h4>5. PER QUANTO TEMPO DURERA’ IL TRATTAMENTO DEI  VOSTRI DATI PERSONALI?</h4>
        <p>I Vostri dati personali verranno elaborati per un periodo fino a 10 anni dal giorno in cui ci darete il Vostro consenso al trattamento dei Vostri dati.</p>

        <h4>6. C’È QUALCUN ALTRO CHE PUÒ ELABORARE I VOSTRI DATI PERSONALI?</h4>
        <p>Siamo noi che elaboriamo i Vostri dati personali ma per essere in grado di raggiungere lo scopo per il quale si elaborano i Vostri dati personali, ci sono oltre a noi, per conto nostro, anche altri elaboratori – provider delle pagine  internet, e-mail, corrispondenti SMS, del on-line, dei sistemi statistici e di monitoraggio.</p>

        <h4>7. POTETE REVOCARE IL VOSTRO CONSENSO ALL’ELABORAZIONE DEI VOSTRI DATI PERSONALI?</h4>
        <p>Si, il consenso all’elaborazione dei Vostri dati personali può essere revocato in qualsiasi momento, cliccando sull’apposito campo del Vostro account.</p>
        <p>Al tempo stesso teniamo a precisare che la mancata concessione di tale approvazione o la sua revoca non hanno alcuna influenza alla concessione dei nostri prodotti oppure servizi.</p>

        <h4>8. QUALI SONO I VOSTRI DIRITTI IN RELAZIONE AI DATI PERSONALI ELABORATI?</h4>
        <p>Avete il diritto:</p>
        <ul>
            <li>all’accesso ai Vostri dati personali – altrimenti detto avete il diritto di chiederci l’informazione sul fatto se stiamo elaborando i Vostri dati personali o no; se è così, quali sono i Vostri dati personali che elaboriamo e per quale finalità, a quali altri soggetti concediamo l’accesso ai suddetti dati Vostri personali, per quanto tempo i Vostri dati personali saranno da noi depositati e se viene attuato il processo della decisione  automatizzzata, compresa la profilazione</li>
            <li>alla spiegazione, correzione, risp. integrazione dei Vostri dati personali – se ritenete che i Vostri dati personali che elaboriamo non siano esatti: in tal caso avete il diritto di chiedere le spiegazione e la rimozione di tale situazione, soprattutto chiedendo il blocco, la dovuta correzione o cancellazione dei dati personali</li>
            <li>diritto alla cancellazione dei Vostri dati personali, </li>
            <li>diritto alla restrizione dell’elaborazione dei Vostri dati personali,</li>
            <li>diritto alla possibilità di trasferimento dei Vostri dati personali – avete il diritto di acquisire i dati personali che Vi riguardano e che ci avete concesso; quanto sopra nel formato strutturato, correntemente utilizzato e leggibile in modo che i dati possano essere trasferiti ad un altro gestore</li>
            <li>diritto di rivolgersi all’Ufficio per la protezione dei dati personali – qualora aveste un dubbio circa l’osservanza degli obblighi riguardanti l’elaborazione dei Vostri dati personali siete liberi di rivolgerVi all’Ufficio per la protezione dei dati personali (www.uoou.cz)</li>
        </ul>
        <p>Se invece ritenete che noi oppure l’elaboratore da noi incaricato elalabora i Vostri dati personali contrariamente alle finalità per le quali questi dati ci erano stati concessi, avete il diritto di sollevare l’obiezione contro l’elaborazione dati personali per le finalità del marketing diretto compresa la profilazione, per quanto riguarda il marketing diretto.</p>


        <h4>9. DICHIARAZIONE</h4>
        <p>Dichiarate che, se avete meno di 16 anni, avete chiesto al Vostro rappresentante legale di concederVi il consenso al trattamento dei Vostri dati personali.</p>
        <p>Dichiarate che i dati personali concessi corrispondono alla verità, sono stati concessi liberamente, spontaneamente, senza costrizione, volontariamente ed in base alla vostra libera volontà.</p>

        <p>Altre informazioni riguardanti il trattamento dei dati personali sono riportati nel documento <strong>Information Note on Processing of Personal Data</strong>.  Le informazioni aggiornate concernenti il trattamento dei dati personali, comprese le informazioni sui destinatari dei dati personali, vengono sempre riportate sulle pagine <a target="_blank" href="https://emischool.com/legal/processing_personal_data.php">here</a></p>
    </div>
    <div id="text_fr"><p>
            Dans le cas où vous acceptez le traitement de vos données personnelles, nous pourrons nous adresser à vous à l'avenir avec l'offre de nos produits et services qui répondront le mieux à vos besoins. Cependant, la fourniture de nos produits et services n'est pas soumise à ce consentement. Bien sûr, vous pouvez changer votre décision à tout moment et retirer votre consentement. Voici quelques informations sur la façon de traiter vos données personnelles</p>
        <h4>1. À QUI VOUS DONNEZ LE CONSENTEMENT AU TRAITEMENT DE VOS DONNÉES PERSONNELLES OU QUI SOMMES-NOUS?</h4>
        <p>Vous consentez au traitement de vos données personnelles par la société:</p>
        <p>E.Mi – International s.r.o.</p>
        <p>N. d´identification. 24214647</p>
        <p>ayant son siège social à U božích bojovníků 89/1, Žižkov, 130 00 Praha 3</p>
        <p>inscrit au registre du commerce tenu à la Cour municipale de Prague sous le no. C 189332</p>

        <h4>2. DANS QUEL BUT NOUS TRAITONS VOS DONNÉES PERSONNELLES?</h4>
        <p>Nous traitons vos données personnelles à des fins de marketing, ce qui inclut les activités suivantes:</p>
        <ul>
            <li>participation au programme de fidélité, </li>
            <li>des informations sur les nouveaux produits et les offres spéciales, ainsi que des invitations à des événements d'entreprise (expositions, E.MiDay); nous pouvons vous envoyer nos offres sur la base de votre consentement par voie électronique, notamment par courrier électronique ou message envoyé à l'appareil mobile via un numéro de téléphone, via la zone client Web, par écrit ou au moyen d'un appel téléphonique,</li>
            <li>traitement automatisé des données personnelles afin d'adapter l'offre commerciale à vos besoins individuels,</li>
            <li>des enquêtes de satisfaction avec nos produits et services.</li>
        </ul>

        <h4>3. QUELLES DONNÉES PERSONNELLES ALLONS-NOUS TRAITER SUR LA BASE DE VOTRE CONSENTEMENT?</h4>
        <p>Sur la base de ce consentement, nous traiterons les données personnelles suivantes:</p>
        <ul>
            <li>nom, prénom. titre, </li>
            <li>résidence permanente/lieu de l´entreprise, </li>
            <li>date de naissance, </li>
            <li>les données des documents soumis, </li>
            <li>des données sur les paiements et la morale de paiement,</li>
            <li>contact téléphonique et e-mail, </li>
            <li>informations identifiant l'appareil ou la personne associée à l'utilisation de l'appareil, à savoir l'adresse IP.</li>
        </ul>

        <h4>4. POURQUOI LE PROFILAGE ET LA PRISE DE DÉCISION AUTOMATISÉE ONT-ILS LIEU?</h4>
        <p>Nous nous efforçons de vous offrir les produits et services personnalisés les mieux adaptés et de ne pas vous imposer tous nos produits. Pour cette raison, vos données personnelles sont profilées en fonction de votre consentement, pour lequel nous utilisons des systèmes d'information automatisés ou des applications Web. En pratique, cela signifie qu'avant de vous proposer un produit ou un service, nous évaluerons d'abord vos informations personnelles selon vos besoins, et nous vous enverrons ensuite des offres personnalisées de produits et de services.</p>
        <p>L´ évaluation automatisée (profilage) des données nous aide à mieux vous connaître, à répondre à vos besoins et à adapter nos produits et services en conséquence.</p>

        <h4>5. COMBIEN DE TEMPS ALLONS-NOUS TRAITER VOS DONNÉES PERSONNELLES?</h4>
        <p>Nous traiterons vos données personnelles jusqu'à 10 ans à compter de la date à laquelle vous nous avez donné votre consentement pour traiter vos données.</p>

        <h4>6. EST-CE QUE QUELQU´UN D´AUTRE PEUT TRAITER VOS DONNÉES PERSONNELLES POUR NOUS?</h4>
        <p>Vos données personnelles sont traitées par nous, mais afin que nous puissions remplir l'objectif susmentionné de traitement de vos données personnelles, vos données personnelles sont également traitées par ces sous-traitants - les fournisseurs de sites Internet, e-mail, correspondance SMS, chat en ligne, des systèmes statistiques et de surveillance.</p>

        <h4>7. POUVEZ-VOUS RETIRER VOTRE CONSENTEMENT AU TRAITEMENT DE VOS DONNÉES PERSONNELLES?</h4>
        <p>Oui, vous pouvez retirer votre consentement au traitement de vos informations personnelles à tout moment en cliquant sur la case appropriée dans votre compte.</p>
        <p>Nous déclarons également que le défaut d'accorder ce consentement ou sa révocation n'affecte pas la fourniture de nos produits ou services.</p>

        <h4>8. QUELS SONT VOS DROITS CONCERNANT LES DONNÉES PERSONNELLES TRAITÉES?</h4>
        <p>Vous avez le droit de:</p>
        <ul>
            <li>d´accéder à vos données personnelles – ou bien vous avez le droit de nous demander des informations sur le traitement ou non de vos données personnelles et, le cas échéant, quelles sont vos données personnelles que nous traitons, dans quel but, quelles sont les autres entités auxquelles nous donnons accès à ces informations personnelles, pour combien de temps vos données personnelles seront stockées chez nous, si la prise de décision automatisée, y compris le profilage, a lieu</li>
            <li>droit à explication, correction ou complément de vos données personnelles – si vous estimez que vos données personnelles que nous traitons sont inexactes, vous avez le droit d'exiger une explication et demander de rectifier l´état qui en résulte, notamment en bloquant, corrigeant, ajoutant ou supprimant des données personnelles</li>
            <li>droit de supprimer vos données personnelles, </li>
            <li>droit de restreindre le traitement de vos données personnelles,</li>
            <li>droit à la portabilité de vos données personnelles – vous avez le droit d'obtenir des données personnelles qui vous concernent et que vous nous avez fournies dans un format structuré, couramment utilisé et lisible par machine dans le but de les transmettre à un autre responsable de traitement</li>
            <li>droit de contacter l´Office pour la protection des données personnelles – si vous avez des doutes quant à l'accomplissement des tâches liées au traitement de vos données personnelles, vous avez le droit de contacter l'Office pour la protection des données (www.uoou.cz)</li>
        </ul>
        <p>Si vous estimez que nous ou notre sous-traitant traite vos données personnelles en violation de l'objectif pour lequel les données nous ont été fournies, vous avez le droit de vous opposer au traitement des données personnelles à des fins de marketing direct, y compris de profilace, en ce qui concerne le marketing direct.</p>

        <h4>9. DÉCLARATION</h4>
        <p>Vous déclarez que si vous avez moins de 16 ans, vous avez demandé à votre représentant légal la permission de traiter vos données personnelles.</p>
        <p>Vous déclarez que les données personnelles fournies sont vraies, fournies librement, sciemment, volontairement et sur la base de ma propre décision.</p>

        <p>Pour plus d'informations concernant le traitement des données personnelles, veuillez consulter le document <strong>Information Note on Processing of Personal Data</strong>. Des informations à jour sur le traitement des données personnelles, y compris des informations sur les destinataires de données à caractère personnel, sont toujours disponibles <a target="_blank" href="https://emischool.com/legal/processing_personal_data.php">sur</a></p>
    </div>
    <div id="text_es"><p>
            IEn caso de que acepte el procesamiento de sus datos personales, podremos enviarle en el futuro ofertas de nuestros productos y servicios que mejor se adapten a sus necesidades. Sin embargo, la provisión de nuestros productos y servicios no está sujeta a este consentimiento. Por supuesto, puede cambiar su decisión en cualquier momento y retirar su consentimiento. Nos gustaría proporcionarle información de cómo procesaremos sus datos personales</p>
        <h4>1. ¿A QUIÉN OTORGA SU CONSENTIMIENTO PARA EL PROCESAMIENTO DE SUS DATOS PERSONALES O QUIÉNES SOMOS?</h4>
        <p>Usted concede su consentimiento para el tratamiento de sus datos personales a:</p>
        <p>E.Mi – International s.r.o.</p>
        <p>NIF 24214647</p>
        <p>Con sede en Praga 3, U božích bojovníků 89/1, Žižkov, 130 00</p>
        <p>Inscrita en el Registro Mercantil mantenido en el Juzgado Municipal de Praga con la ref. C 189332</p>

        <h4>2. ¿CON QUÉ PROPÓSITO PROCESAMOS SUS DATOS PERSONALES?</h4>
        <p>Procesamos sus datos personales con fines de marketing que incluye las siguientes actividades:</p>
        <ul>
            <li>participación en el programa de fidelización,</li>
            <li>información sobre nuevos productos y ofertas especiales, así como invitaciones a eventos corporativos (exposiciones, E.MiDay); En base a su consentimiento podemos enviarle las ofertas electrónicamente, en particular por correo electrónico o mensajes enviados al dispositivo móvil al número de teléfono, a través de la zona del cliente web, por escrito o por medio de una llamada telefónica.</li>
            <li>procesamiento automatizado de datos personales para adaptar la oferta comercial a sus necesidades individuales,</li>
            <li>realización de encuestas de satisfacción con nuestros productos y servicios</li>
        </ul>

        <h4>3. ¿QUÉ DATOS PERSONALES PROCESAREMOS CON SU CONSENTIMIENTO?</h4>
        <p>En base a este consentimiento, procesaremos los siguientes datos personales:</p>
        <ul>
            <li>nombre, apellido, título,</li>
            <li>residencia permanente / lugar de trabajo,</li>
            <li>fecha de nacimiento,</li>
            <li>datos de los documentos presentados</li>
            <li>información sobre su historia de pagos y cultura de pago de deudas,</li>
            <li>contacto telefónico y electrónico,</li>
            <li>información que identifica el dispositivo o la persona asociada con el uso del dispositivo, a saber, la dirección IP</li>
        </ul>


        <h4>4. ¿POR QUÉ SE ESTÁN LLEVANDO A CABO EL PROCESAMIENTO Y LA TOMA DE DECISIONES AUTOMATIZADA?</h4>
        <p>Nos esforzamos por ofrecerle las ofertas de productos y servicios personalizadas y más adaptadas y no molestarle con todos nuestros productos. Por este motivo, se perfilan sus datos personales, basados en su consentimiento, para lo cual utilizamos los sistemas de información automatizados o aplicaciones web. En la práctica, esto significa que, antes de ofrecerle un producto o servicio, primero evaluaremos según sus datos personales cuales podrían ser sus necesidades y le enviaremos ofertas personalizadas de productos y servicios.</p>
        <p>La evaluación automática (perfiles) de datos personales nos ayuda a conocerle más de cerca a Usted, sus necesidades y en consecuencia adaptar nuestros productos y servicios a su medida.</p>

        <h4>5. ¿CUÁNTO TIEMPO PROCESAREMOS SUS DATOS PERSONALES?</h4>
        <p>Procesaremos sus datos personales durante hasta 10 años a partir de la fecha en que nos da su consentimiento para procesar sus datos.</p>

        <h4>6. ¿PUEDE PROCESAR SU INFORMACIÓN PERSONAL OTRA PERSONA?</h4>
        <p>Somos nosotros quienes procesamos sus datos personales, sin embargo para cumplir con el objetivo mencionado anteriormente, también procesan sus datos personales los siguientes procesadores: proveedores de sitios web, correo electrónico, correspondencia por SMS, chat en línea, sistemas estadísticos y de monitoreo.</p>

        <h4>7. ¿PUEDE RETIRAR SU CONSENTIMIENTO PARA EL PROCESAMIENTO DE SUS DATOS PERSONALES?</h4>
        <p>Sí, puede revocar su permiso para procesar su información personal en cualquier momento haciendo clic en el campo correspondiente en su cuenta.</p>
        <p>También declaramos que el hecho de no otorgar este consentimiento o su revocación no afecta la prestación de nuestros productos o servicios.</p>

        <h4>8. ¿CUÁLES SON SUS DERECHOS EN RELACIÓN CON LOS DATOS PERSONALES PROCESADOS?</h4>
        <p>Tiene derecho a:</p>
        <ul>
            <li>acceso a su información personal – usted tiene derecho a solicitarnos información sobre si procesamos o no sus datos personales y, en caso positivo, cómo los procesamos, el propósito por el cual otras entidades acceden a su información personal, cuánto tiempo se almacenarán sus datos personales, si la toma de decisiones es automatizada, incluyendo la creación de perfiles, </li>
            <li>aclarar, corregir o complementar su información personal – Si considera que sus datos personales que procesamos son inexactos, tiene derecho a solicitar aclaraciones y eliminar la condición resultante, en particular mediante el bloqueo, la corrección, la complementación o la eliminación de información personal.</li>
            <li>derecho de eliminar su información personal, </li>
            <li>derecho a limitar el procesamiento de sus datos personales,</li>
            <li>derecho a la portabilidad de sus datos personales –tiene derecho a obtener información personal relacionada con usted que nos haya proporcionado, en un formato estructurado, comúnmente utilizado y legible por máquina con el propósito de reenviarla a otro administrador. </li>
            <li>derecho a contactar la Oficina de Protección de Datos Personales – En caso de que tenga dudas sobre el cumplimiento de las obligaciones relacionadas con el procesamiento de sus datos personales, tiene derecho a contactarse con la Oficina de Protección de Datos Personales (www.uoou.cz)</li>
        </ul>
        <p>Si usted cree que nosotros o nuestro procesador autorizado procesa sus datos personales en contra de la finalidad para la cual se nos proporcionaron dichos datos, tiene derecho a objetar el procesamiento de datos personales con fines de marketing directo, incluidos los perfiles, si relacionado con el marketing directo.</p>

        <h4>9. DECLARACIÓN</h4>
        <p>Usted declara que si tiene menos de 16 años de edad, ha solicitado permiso a su representante legal para procesar sus datos personales.</p>
        <p>Usted declara que los datos personales proporcionados son verídicos, se proporcionan de manera gratuita, a sabiendas, de forma voluntaria y se basan en su propia decisión.</p>

        <p>Para obtener más información sobre el procesamiento de datos personales, consulte la <strong>Information Note on Processing of Personal Data</strong>. En el <a target="_blank" href="https://emischool.com/legal/processing_personal_data.php">web</a> se proporciona información actualizada sobre el procesamiento de los datos personales, incluida la información sobre los destinatarios de los datos personales</p>
    </div>
    <div id="text_cz">
        <p>
            V případě, že nám udělíte souhlas se zpracováváním Vašich osobních údajů, budeme Vás moci v budoucnu oslovit s nabídkou našich výrobků a služeb, která bude lépe odpovídat Vašim potřebám. Poskytování našich výrobků a služeb však není tímto souhlasem podmíněno. Samozřejmě můžete své rozhodnutí kdykoliv změnit a souhlas odvolat. Zde si dovolujeme uvést několik informací ke zpracovávání Vašich osobních údajů
        </p>
        <h4>1. KOMU UDĚLUJETE SOUHLAS SE ZPRACOVÁNÍM VAŠICH OSOBNÍCH ÚDAJŮ NEBOLI KDO JSME?</h4>
        <p>Souhlas se zpracováním Vašich osobních údajů udělujete společnosti:</p>
        <p>E.Mi – International s.r.o.</p>
        <p>IČO 24214647</p>
        <p>se sídlem U božích bojovníků 89/1, Žižkov, 130 00 Praha 3</p>
        <p>zapsané v obchodním rejstříku vedeném u Městského soudu v Praze pod sp.zn. C 189332</p>

        <h4>2. ZA JAKÝM ÚČELEM ZPRACOVÁVÁME VAŠE OSOBNÍ ÚDAJE?</h4>
        <p>Vaše osobní údaje zpracováváme pro účely marketingu, který zahrnuje následující činnosti:</p>
        <ul>
            <li>účast ve věrnostním programu, </li>
            <li>informování o nových produktech a speciálních nabídkách, jakož i zasílání pozvánek na firemní akce (výstavy, E.MiDay); nabídky Vám na základě Vašeho souhlasu můžeme zasílat jak elektronicky, zejména formou e-mailových zpráv nebo zpráv zasílaných na mobilní zařízení prostřednictvím telefonního čísla, prostřednictvím webové klientské zóny, písemnou formou či formou telefonického volání, </li>
            <li>automatizované zpracování osobních údajů s cílem přizpůsobit obchodní nabídku Vašim individuálním potřebám, </li>
            <li>provádění průzkumů spokojenosti s našimi produkty a službami</li>
        </ul>

        <h4>3. JAKÉ OSOBNÍ ÚDAJE BUDEME NA ZÁKLADĚ TOHOTO VAŠEHO SOUHLASU ZPRACOVÁVAT?</h4>
        <p>Na základě tohoto souhlasu budeme zpracovávat následující osobní údaje:</p>
        <ul>
            <li>jméno, příjmení, titul</li>
            <li>trvalý pobyt/místo podnikání</li>
            <li>datum narození</li>
            <li>číslo bankovního účtu</li>
            <li>údaje z předložených dokladů</li>
            <li>údaje o platbách a platební morálce</li>
            <li>telefonní a e-mailové spojení</li>
        </ul>


        <h4>4. PROČ DOCHÁZÍ K PROFILOVÁNÍ A AUTOMATIZOVANÉMU ROZHODOVÁNÍ?</h4>
        <p>Snažíme se poskytnout Vám nejvhodnější individualizované nabídky produktů a služeb a nezatěžovat Vás nabídkou všech našich produktů. Z tohoto důvodu Vaše osobní údaje, na základě Vámi uděleného souhlasu profilujeme, k čemuž používáme automatické informační systémy nebo webové aplikace. V praxi to znamená, že ještě předtím, než Vám nabídneme produkt či službu, nejprve vyhodnotíme podle Vašich osobních údajů, jaké byste mohli mít potřeby a dle toho Vám zasíláme individualizované nabídky produktů a služeb.</p>
        <p>Automatizované vyhodnocování (profilování) osobních údajů nám pomáhá poznat blíže Vás, Vaše potřeby a podle toho i přizpůsobit naše produkty a služby Vám na míru.</p>

        <h4>5. JAK DLOUHO BUDEME VAŠE OSOBNÍ ÚDAJE ZPRACOVÁVAT?</h4>
        <p>Vaše osobní údaje budeme zpracovávat po dobu do 10 let ode dne, kdy nám tento souhlas se zpracováním Vašich údajů poskytnete.</p>

        <h4>6. MŮŽE PRO NÁS ZPRACOVÁVAT VAŠE OSOBNÍ ÚDAJE NĚKDO JINÝ?</h4>
        <p>Vaše osobní údaje zpracováváme my, však abychom mohli naplnit shora uvedený účel zpracovávání Vašich osobních údajů, zpracovávají pro nás osobní údaje rovněž tito zpracovatelé – poskytovatelé internetových stránek, e-mailu, SMS korespondenci, on-line chatu, statistických a monitorovacích systémů.</p>

        <h4>7. MŮŽETE SVŮJ SOUHLAS SE ZPRACOVÁNÍM VAŠICH OSOBNÍCH ÚDAJŮ ODVOLAT?</h4>
        <p>Ano, svůj souhlas se zpracováním Vašich osobních údajů můžete kdykoliv odvolat, kliknutím na příslušné pole ve vašem účtu.</p>
        <p>Zároveň uvádíme, že neudělení tohoto souhlasu či jeho odvolání nemá vliv na poskytování našich výrobků či služeb.</p>

        <h4>8. JAKÁ JSOU VAŠE PRÁVA VE VZTAHU KE ZPRACOVÁVANÝM OSOBNÍM ÚDAJŮM?</h4>
        <p>Máte právo na:</p>
        <ul>
            <li>přístup k Vašim osobním údajům – neboli máte právo od nás požadovat informaci o tom, zda Vaše osobní údaje zpracováváme či nikoliv, a pokud ano, jaké Vaše osobní údaje zpracováváme, za jakým účelem, jakým dalším subjektům zpřístupňujeme tyto Vaše osobní údaje, po jakou dobu u nás budou Vaše osobní údaje uloženy, zda dochází k automatizovanému rozhodování, včetně profilování</li>
            <li>právo na vysvětlení, opravu, resp. doplnění, Vašich osobních údajů – pokud se domníváte, že Vaše osobní údaje, které zpracováváme, jsou nepřesné, máte právo požadovat vysvětlení a odstranění vzniklého stavu, zejména prostřednictvím blokování, provedení opravy, doplnění nebo výmazu osobních údajů</li>
            <li>právo na výmaz Vašich osobních údajů, </li>
            <li>právo na omezení zpracování Vašich osobních údajů,</li>
            <li>právo na přenositelnost údajů Vašich osobních údajů – máte právo získat osobní údaje, které se Vás týkají a které jste nám poskytl, ve strukturovaném, běžně používaném a strojově čitelném formátu, pro účely jejich předání jinému správci </li>
            <li>právo obrátit se na Úřad pro ochranu osobních údajů – v případě, že máte pochybnosti o dodržování povinností souvisejících se zpracováním Vašich osobních údajů, máte právo se obrátit na Úřad pro ochranu osobních údajů (www.uoou.cz)</li>
        </ul>
        <p>Pokud se domníváte, že my nebo námi pověřený zpracovatel zpracovává Vaše osobní údaje v rozporu s účelem, pro který nám byla data poskytnuta, máte právo vznést námitku proti zpracování osobních údajů na účely přímého marketingu včetně profilování, pokud se týká přímého marketingu.</p>

        <h4>9. PROHLÁŠENÍ</h4>
        <p>Prohlašujete, že pokud je Vám méně než 16 let, požádal jste svého zákonného zástupce o souhlas se zpracováním Vašich osobních údajů.</p>
        <p>Prohlašujete, že poskytnuté osobní údaje jsou pravdivé, byly poskytnuty svobodně, vědomě, dobrovolně a na základě mého vlastního rozhodnutí.</p>

        <p>Další informace týkající se zpracovávání osobních údajů jsou uvedeny v dokumentu <strong>Information Note on Processing of Personal Data</strong>. Aktuální informace týkající se zpracování osobních údajů, včetně informace o příjemcích osobních údajů, jsou vždy uvedeny na <a target="_blank" href="https://emischool.com/legal/processing_personal_data.php">stránkách</a></p>

    </div>
</div>
<script>
    $(".lang a").on("click", function(e){
        e.preventDefault();
        $(".lang a").removeClass("active");
        $(this).addClass("active");

        $(".text-lang div").removeClass("active");
        $("#text_"+$(this).attr('id')).addClass("active");
    })
</script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>