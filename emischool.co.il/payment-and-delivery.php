<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Payment and Delivery");
?><div class="bx_page">
	<div class="h1-top">
		<h1>Payment and Delivery</h1>
	</div>
</div>
 <br>
<h2>
1. Prices. </h2>
<p>
	 1.1 All the e-shop prices include 23% VAT.
</p>
<p>
	 1.2 Products are supplied without 23% VAT to non-EU countries.
</p>
<p>
	 1.3 All taxes and other possible expenses in accordance with Client’s country laws pays Client.
</p>
<p>
</p>
<h2>
2. Payment </h2>
<p>
	 2.1 All payments are effected on the basis of issued invoice.
</p>
<p>
	 2.2 Before paying the invoice please check if products and products amounts in the invoice are correct.
</p>
<p>
</p>
<h2>
3. Shipping conditions. </h2>
<p>
	 3.1 Products are shipped internationally by TNT Transporting Company.
</p>
<p>
	 3.2 Shipping rates are not fixed and depend on package weight and shipping destination.
</p>
<p>
	 3.3 Packaging and shipping are processed within 3 business days after receiving of the payment.
</p>
<p>
	 3.4 All shipping terms are estimated and depend on shipping destination.
</p>
<p>
     3.5 When order is shipped, TNT Transporting Company sends Client an email with a web link for tracking of the order and tracking number.<br>
</p>

 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>