<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("About Company");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>About Company</h1>
   </div>
 
  <div class="bx_page" style="text-align: center;"><img src="/upload/medialibrary/f7c/f7cb52963256ebe4b40f9af5c03a36d2.jpg" title="56.jpg" border="0" alt="56.jpg" width="1140" height="300"  /> 
    <br />
   </div>
 
  <br />
 
  <p>The E.Mi Company presents thousands of latest and exclusive ready-made solutions, developed by Ekaterina Miroshnichenko, a world champion nail design, as well as unique materials and accessories for nail-art created with her participation.</p>
 
  <p>The E.Mi mission is to help every manicurist to elicit her potential, feel an artist and become a real fashion-expert! To learn how to create the E.Mi amazing designs is possible in the School of nail design by Ekaterina Miroshnichenko.</p>
 
  <p>The E.Mi Company is quickly developing and successful business and you can be a part of it! Currently, E.Mi includes fifty official representatives of the School of nail design by Ekaterina Miroshnichenko in Russia, Europe and Asia, official E.Mi brand distributors and hundreds sales points all over the world.</p>
 
  <h2>READY-MADE SOLUTIONS&amp;NAIL&FASHION </h2>
 
  <p><img src="/upload/medialibrary/cd4/cd48dcf71294554d8839eab0a34a4986.jpg" title="98.jpg" border="0" alt="54.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Haute couture is a real art. It dazzles, encourages on experiments and makes you look forward to a new season which always amazes and surprises.</p>
 
  <p>The E.Mi ready-made solutions are haute couture nail-art.  During the year we present some exclusive collections developed by Ekaterina Miroshnichenko, a world champion nail design in the category Fantasy according to MLA (Paris, 2010), a two-time European champion (Athens, Paris, 2009), an international referee and the founder of the original school of nail design.</p>
 
  <p>Each Ekaterina’s collection is an original interpretation of the latest tendencies, embodied in nail design and adjusted to beauty salons version: incredibly beautiful and unique on the first sight, they are easy to fulfill after training course or master-class video.</p>
 
  <p>Such bestsellers as Zhostovo Painting, One Stroke Painting, Imitation of reptile skin, Crackled effect, Ethnic Prints, Velvet sand and liquid stones and others are among Ekaterina Miroshnichenko’s collection. Feel inspired by the full range of design collection by Ekaterina Miroshnichenko right now!</p>
 
  <h2>PRODUCTS FOR NAIL DESIGN</h2>
 
  <p><img src="/upload/medialibrary/6e3/6e3b48a3e4405516b24de034f7ca5b60.jpg" title="6542.jpg" border="0" alt="6542.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Every nail master, having used the E.Mi products, can say with safe: “This products is made for me!”</p>
 
  <p>The secret of this love from the first sight is simple: Ekaterina Miroshnichenko takes part personally in creation of each product, new colour and accessory. A nail designer, a world champion, a respected expert in nail-art makes the E.Mi products which she wants to work with. That’s why the E.Mi products meet all requirements of nail-designers, take into consideration all peculiarities of work, allow saving the operating time and giving great opportunities for creativity.</p>
 
  <p>Such unique products as EMPASTA, GLOSSEMI, TEXTONE, PRINCOT as well as gel paints collections, designer foil of the most top colours, a great variety of décor materials and accessories are presented in E.Mi. Find the necessary product for you right now!</p>
 
  <h2>SCHOOL OF NAIL DESIGN BY EKATERINA MIROSHNICHENKO</h2>
 
  <p><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" border="0" alt="765.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>If you dream about interesting and creative profession, if you want your clients leave you with a thankful smile and come back to you, get the training courses at the School of nail design by Ekaterina Miroshnichenko.</p>
 
  <p>School of nail design by Ekaterina Miroshnichenko offers original educational programs for nail-designers, which are elaborated especially for nail masters without art education. Ekaterina Miroshnichenko’s unique methods are the basis of all nail design courses.</p>
 
  <p>Design courses are divided into 3 levels of complexity and additional courses. Those who aspires to conquer the Olympus and participate in nail-art competitions, the school of nail design offers special training courses. Students of the nail design school by Ekaterina Miroshnichenko are multiple winners of regional and federal nail design competitions.</p>
 
  <p>At the present day, there are 50 official representatives of Nail design school by Ekaterina Miroshnichenko and all work at united high standards. Let your dream come true and become a real expert in nail-art – choose your own educational program right now!</p>
 
  <p>Find the nearest school on our website and achieve your dream!</p>
 
  <h2>OFFICIAL REPRESENTATIVES</h2>
 
  <p><img src="/upload/medialibrary/060/060926d856ef3ef2bcc0eef18fa948cc.jpg" title="7645.jpg" border="0" alt="7645.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>The E.Mi brand and the School of nail design by Ekaterina Miroshnichenko are the latest business lines which are popular and of great demand. Currently, the official representatives of the nail design school by Ekaterina Miroshnichenko are successfully operating in Russia, Ukraine, Kazakhstan, Belarus, Italy, Portugal, Romania, Cyprus, Germany, France, Lithuania, Slovakia, South Korea and the United Arab Emirates. The E.Mi products can be purchased all over the world.</p>
 
  <p>You also can be a part of a successful international brand and get a ready-made business solutions – become an official representative of the Nail design School by Ekaterina Miroshnichenko or an official distributor of the E.Mi brand in your city, region or country. Official representatives of the Nail design school by Ekaterina Miroshnichenko gets the following advantages:
<ul> 
	<li>
the possibility to represent exclusively the brand of the Nail design school by Ekaterina Miroshnichenko in your region;
	</li>
	<li>
the right to tutor original courses on Ekaterina Miroshnichenko’s nail design;
	</li>
	<li>
full business processes for opening and developing of your business;
	</li>
	<li>
perfective design maintenance and personnel training;
	</li>
	<li>
federal promotional support;
	</li>
	<li>
profit making decisions on all E.Mi product range purchase.
	</li>
</ul></p>
 
  <p>To become an official representative of the Nail design school by Ekaterina Miroshnichenko or an official distributor of the E.Mi brand, you can fill in an application form on our website or call +420 722935746 Korbut Marina, korbut@emischool.com</p>

  <p>
    <br />
  </p>

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>