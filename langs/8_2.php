﻿<?
$MESS["TSB1_PERSONAL"] = "Προσωπική περιοχή";
$MESS["TSB1_EXPAND"] = "Επέκταση";
$MESS["TSB1_COLLAPSE"] = "Απόκρυψη";
$MESS["TSB1_CART"] = "Kαλάθι";
$MESS["TSB1_TOTAL_PRICE"] = "Συνολική αξία";
$MESS["TSB1_YOUR_CART"] = "Το Kαλάθι σας";
$MESS["TSB1_READY"] = "Σε απόθεμα";
$MESS["TSB1_DELAY"] = "Στη λίστα επιθυμιών";
$MESS["TSB1_NOTAVAIL"] = "Μη διαθέσιμο";
$MESS["TSB1_SUBSCRIBE"] = "Παρακολούθηση λίστας";
$MESS["TSB1_SUM"] = "Σύνολο";
$MESS["TSB1_DELETE"] = "Διαγραφή";
$MESS["TSB1_2ORDER"] = "Ολοκλήρωση αγοράς";
?>