﻿<?
$MESS["CATALOG_QUANTITY"] = "Ποσότητα";
$MESS["CATALOG_QUANTITY_FROM_TO"] = "από #FROM# μέχρι #TO#";
$MESS["CATALOG_QUANTITY_FROM"] = "#FROM# και περισσότερο";
$MESS["CATALOG_QUANTITY_TO"] = "μέχρι και #TO#";
$MESS["CATALOG_PRICE_VAT"] = "συμπεριλαμβανομένου του φόρου";
$MESS["CATALOG_PRICE_NOVAT"] = "εξαιρούνται του φόρου";
$MESS["CATALOG_VAT"] = "Φορολογικός συντελεστής";
$MESS["CATALOG_NO_VAT"] = "χωρίς φορολόγηση";
$MESS["CATALOG_VAT_INCLUDED"] = "Οι φόροι συμπεριλαμβάνονται στην τιμή";
$MESS["CATALOG_VAT_NOT_INCLUDED"] = "Οι φόροι δεν συμπεριλαμβάνονται στην τιμή";
$MESS["CT_BCE_QUANTITY"] = "Ποσότητα";
$MESS["CT_BCE_CATALOG_BUY"] = "Αγορά";
$MESS["CT_BCE_CATALOG_ADD"] = "Kαλάθι";
$MESS["CT_BCE_CATALOG_COMPARE"] = "Σύγκριση";
$MESS["CT_BCE_CATALOG_NOT_AVAILABLE"] = "Μη διαθέσιμο σε απόθεμα";
$MESS["OSTATOK"] = "Ποσότητα σε απόθεμα";
$MESS["COMMENTARY"] = "Σχόλια";
$MESS["ECONOMY_INFO"] = "(Savings - #ECONOMY#)";
$MESS["FULL_DESCRIPTION"] = "Πλήρης περιγραφή";
$MESS["CT_BCE_CATALOG_TITLE_ERROR"] = "Σφάλμα";
$MESS["CT_BCE_CATALOG_TITLE_BASKET_PROPS"] = "Στοιχεία στοιχείου για μετάβαση στο Kαλάθι";
$MESS["CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR"] = "Προέκυψε αγνωστο σφάλμα κατά την προσθήκη ενός στοιχείου στο Kαλάθι";
$MESS["CT_BCE_CATALOG_BTN_SEND_PROPS"] = "Επιλογή";
$MESS["CT_BCE_CATALOG_BTN_MESSAGE_CLOSE"] = "Κλείσιμο";

$MESS["NO_PRICE"] = "Χωρίς τιμή";
$MESS["STORE_QUANTITY"] = "Ποσότητα σε απόθεμα:";
$MESS["ADD_TO_CART"] = "Kαλάθι";
$MESS["PAYMENT_AND_DELIV"] = "Πληρωμή και παράδοση";
$MESS["IN_CART"] = "Στο Kαλάθι";
$MESS["ADD_PROCESS"] = "Προσθήκη στο/στα...";
$MESS["CANT_ADD_WITHOUT_PRICE"] = "Δεν μπορείτε να προσθέσετε προϊόντα χωρίς τιμή";
?>