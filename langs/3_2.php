﻿<?
$MESS ['P_PAY_SYS'] = "Τρόπος πληρωμής: ";
$MESS ['P_DELIVERY'] = "Τρόπος αποστολής:";
$MESS ['STPOL_CUR_ORDERS'] = "Πρόσφατες παραγγελίες";
$MESS ['STPOL_ORDERS_HISTORY'] = "Ιστορικό παραγγελιών";
$MESS ['STPOL_STATUS'] = "Παραγγελίες σε εξέλιξη";
$MESS ['STPOL_ORDER_NO'] = "Αριθμός παραγγελίας";
$MESS ['STPOL_DETAIL_ALT'] = "Πληροφορίες παραγγελίας";
$MESS ['STPOL_FROM'] = "από";
$MESS ['STPOL_CANCELED'] = "� ακυρώθηκε";
$MESS ['STPOL_SUM'] = "Ποσό:";
$MESS ['STPOL_STATUS_T'] = "Κατάσταση:";
$MESS ['STPOL_STATUS_FROM'] = "Κατάσταση της/του";
$MESS ['STPOL_CONTENT'] = "Περιεχόμενο παραγγελίας:";
$MESS ['STPOL_SHT'] = "τμχ.";
$MESS ['STPOL_DETAILS'] = "Λεπτομέρειες";
$MESS ['STPOL_REORDER'] = "Επανάληψη παραγγελίας (παραγγελία ίδιων προϊόντων)";
$MESS ['STPOL_REORDER1'] = "Επανάληψη παραγγελίας";
$MESS ['STPOL_CANCEL'] = "Ακύρωση παραγγελίας";
$MESS ['STPOL_NO_ORDERS'] = "Δεν βρέθηκαν παραγγελίες";
$MESS ['STPOL_HINT'] = "Πατήστε τον αριθμό της παραγγελίας για να δείτε το περιεχόμενό της.";
$MESS ['STPOL_HINT1'] = "Αν βρήκατε λάθη στην λίστα, παρακαλώ επικοινωνήστε με το τμήμα πωλήσεών μας.";
$MESS ['STPOL_PAYED_Y'] = "(πληρώθηκε)";
$MESS ['STPOL_PAYED_N'] = "(δεν έχει πληρωθεί)";


$MESS ['STPOL_DETAIL'] = "Πληροφορίες παραγγελίας";
$MESS ['STPOL_SUM'] = "Ποσό πληρωμής:";
$MESS ['STPOL_PAYED'] = "Πληρώθηκε:";
$MESS ['STPOL_Y'] = "Ναι";
$MESS ['STPOL_N'] = "Όχι";

$MESS ['STPOL_F_NAME'] = "Παραγγελίες:";
$MESS ['STPOL_F_ACTIVE'] = "Σε ισχύ";
$MESS ['STPOL_F_ALL'] = "Ιστορικό παραγγελιών";
$MESS ['STPOL_F_COMPLETED'] = "ολοκληρώθηκε";
$MESS ['STPOL_F_CANCELED'] = "ακυρώθηκε";
$MESS ['STPOL_NO_ORDERS_NEW'] = "Δεν υπάρχουν παραγγελίες σε αυτήν την κατηγορία";
?>