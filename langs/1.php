﻿<?
$MESS ['ITHIVE_OFFICES_LIST_TITLE'] = "Επαφές";
$MESS ['ITHIVE_OFFICES_NOTHINGS_FOUND'] = "Δεν βρέθηκε!";
$MESS ['ITHIVE_OFFICES_MY_LOCATION'] = "Η τοποθεσία μου";
$MESS ['ITHIVE_OFFICES_SHOW_YOUR_LOCATION'] = "Εμφάνιση τοποθεσίας";
$MESS ['ITHIVE_OFFICES_YOUR_LOCATION'] = "Η τοποθεσία_σου:<br/>";
$MESS ['ITHIVE_OFFICES_ENTIRE_MAP'] = "Όλος ο χάρτης";
$MESS ['ITHIVE_OFFICES_SHOW_ALL_SHOPS'] = "Εμφάνιση όλων των καταστήματων";
$MESS ['ITHIVE_OFFICES_NAME_SHORT'] = "Όνομα";
$MESS ['ITHIVE_OFFICES_ADDRESS'] = "Διεύθυνση";
$MESS ['ITHIVE_OFFICES_PHONES'] = "Τηλέφωνα";
$MESS ['ITHIVE_OFFICES_WWW'] = "WWW";
$MESS ['ITHIVE_OFFICES_DETAIL'] = "Πληροφορία";
?>