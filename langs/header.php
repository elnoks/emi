﻿<?
$MESS["DEF_TEMPLATE_NF"]="Το πρότυπο δεν βρέθηκε. Ορίστε πρότυπο για αυτόν τον ιστότοπο.";
$MESS["DEF_TEMPLATE_NF_SET"]="Ορίστε πρότυπο";


//Header
	//Выбор страны
$MESS["HEADER_CHOOSE_COUNTRY_ru"] = "Ρωσία
";
$MESS["HEADER_CHOOSE_COUNTRY_en"] = "Ευρώπη";

	//Меню
$MESS["HEADER_TOP_MENU_P_COURSES"] = "Σεμινάρια";
$MESS["HEADER_TOP_MENU_P_CATALOG"] = "Κατάλογος E.Mi";
$MESS["HEADER_TOP_MENU_P_EVENTS"] = "Εκδηλώσεις";
$MESS["HEADER_TOP_MENU_P_GALLERY"] = "Φωτογραφίες";
$MESS["HEADER_TOP_MENU_P_SOCIAL"] = "Κοινότητα";
$MESS["HEADER_TOP_MENU_P_CONTACTS"] = "Επικοινωνία";
$MESS["HEADER_TOP_MENU_P_ABOUT"] = "Σχετικά με την εταιρεία";


//Главная страница
	//Section title
$MESS["MAIN_PAGE_SECTION_TITLE"] = "Αρχική σελίδα";

	//Title, Description
$MESS["MAIN_PAGE_META_TITLE"] = "E.Mi";
$MESS["MAIN_PAGE_META_DESCRIPTION"] = "E.Mi";

	//4 блока
$MESS["MAIN_PAGE_4BLOCKS_VIDEO"] = "Βίντεο";
$MESS["MAIN_PAGE_4BLOCKS_MASTER_CLASS"] = "Οδηγοί step-by-step";
$MESS["MAIN_PAGE_4BLOCKS_WHERE_BUY"] = "Σημείο πώλησης?";
$MESS["MAIN_PAGE_4BLOCKS_SOCIAL"] = "Κοινότητα";

	//Слайдеры
$MESS["MAIN_PAGE_SLIDER_COURSES"] = "Τα σεμινάριά μας";
$MESS["MAIN_PAGE_SLIDER_PRODUCTS"] = "Τμήματα σχετικά με τα προϊόντα";

//Каталог (продукция)
	//Section title
$MESS["CATALOG_PRODUCTS_SECTION_TITLE"] = "ΠροΪόν";
	//Title
$MESS["CATALOG_PRODUCTS_META_TITLE"] = "Προϊόν";

//Деталка каталога, слайдеры
$MESS["SLIDER_CATALOG_PROD_PT"] = "Παρόμοια προϊόντα";
$MESS["SLIDER_CATALOG_PROD_ST"] = "Σχετικά προϊόντα";
$MESS["SLIDER_CATALOG_PROD_INSET"] = "Σε σετ";;
$MESS["SLIDER_CATALOG_PROD_COURSES"] = "Σεμινάρια";

//Корзина
	//Section title
$MESS["CART_PAGE_SECTION_TITLE"] = "Το Kαλάθι μου";
	//Title
$MESS["CART_PAGE_META_TITLE"] = "Το Kαλάθι μου";

//Офромление заказа
	//Section title
$MESS["ORDER_MAKE_SECTION_TITLE"] = "Ολοκλλήρωση παραγγελίας";

//Сообщества
	//Title
$MESS["SOCIAL_PAGE_META_TITLE"] = "Κοινότητες";
	//Меню
$MESS["SOCIAL_PAGE_MENU_SOCIAL"] = "Κοινότητες";
$MESS["SOCIAL_PAGE_MENU_SALONS"] = "Σαλόνια ομορφιάς E.Mi ";
$MESS["SOCIAL_PAGE_MENU_MASTERS"] = "E.Mi masters";
$MESS["SOCIAL_PAGE_MENU_HOWOPEN"] = "Πώς να ανοίξετε μια επίσημη αντιπροσωπεία της σχολής";

//События
	//Section title
$MESS["EVENTS_PAGE_SECTION_TITLE"] = "Εκδηλώσεις";
	//Title
$MESS["EVENTS_PAGE_META_TITLE"] = "Εκδηλώσεις";
$MESS["EVENTS_PAGE_ARCHIVE_META_TITLE"] = "Archive";

//Галерея
	//Title
$MESS["GALLERY_PAGE_META_TITLE"] = "Φωτογραφίες";
	//Меню
$MESS["GALLERY_PAGE_MENU_MASTER_C"] = "Οδηγοί step-by-step";
$MESS["GALLERY_PAGE_MENU_PUBLIC"] = "Δημοσιεύσεις";
$MESS["GALLERY_PAGE_MENU_PHOTO_DESIGN"] = "Φωτογραφίες Nail art";
	//Blocks
$MESS["GALLERY_PAGE_BLOCKS_PUBLIC"] = "Δημοσιεύσεις";
$MESS["GALLERY_PAGE_BLOCKS_BACKSTAGES"] = "Παρασκήνια";
$MESS["GALLERY_PAGE_BLOCKS_PHOTO_DESIGN"] = "Φωτογραφίες Nail art";

//Контакты
	//Title, description, keywords
$MESS["CONTACTS_PAGE_META_TITLE"] = "Επικοινωνία";
	//Contact blocks
$MESS["CONTACTS_PAGE_BLOCKS_SCHOOL_TITLE"] = "Σχολή nail design";
$MESS["CONTACTS_PAGE_BLOCKS_SCHOOL_ADR"] = "Γραφεία σχολής";
$MESS["CONTACTS_PAGE_BLOCKS_SCHOOL_TEL"] = "Τηλέφωνα";
$MESS["CONTACTS_PAGE_BLOCKS_SHOP_TITLE"] = "Διεθνές Γραφείο στην Τσεχία";
$MESS["CONTACTS_PAGE_BLOCKS_SHOP_ADR"] = "Διεθνές Γραφείο στην Τσεχία";
$MESS["CONTACTS_PAGE_BLOCKS_SHOP_TEL"] = "Τηλέφωνα";

//Личный кабинет
	//Главная страница личного кабинета
$MESS["PERSONAL_CABINET_PAGE_INDEX_TITLE"] = "Ο λογαριασμός μου";
$MESS["PERSONAL_CABINET_PAGE_INDEX_P"] = "Στην περιοχή ο Λογαριασμός μου μπορείτε να ελέγξετε τις τρέχουσες παραγγελίες, τις παραγγελίες σε εξέλιξη, 
		να δείτε και να αλλάξετε τις προσωπικές σας πληροφορίες και επίσης να εγγραφείτε σε newsletters και άλλες ενημερωτικές υπηρεσίες";
	//Меню
$MESS["PERSONAL_CABINET_PAGE_MENU_CABINET"] = "Ο λογαριασμός μου";
$MESS["PERSONAL_CABINET_PAGE_MENU_COURSES"] = "Τα σεμινάριά μου";
$MESS["PERSONAL_CABINET_PAGE_MENU_ORDER"] = "Οι παραγγελίες μου";
$MESS["PERSONAL_CABINET_PAGE_MENU_SUBSCRIBE"] = "Οι εγγραφές μου";
$MESS["PERSONAL_CABINET_PAGE_MENU_PROFILE"] = "Το προφίλ μου";

	//Список заказов
$MESS["PERSONAL_CABINET_PAGE_ORDER_LIST_TITLE"] = "Οι παραγγελίες μου";

//Каталог курсов
	//Расписание
$MESS["CATALOG_COURSES_DAYS_C_DAY"] = "ημέρα";
$MESS["CATALOG_COURSES_DAYS_C_DAYS_YA"] = "ημέρα";
$MESS["CATALOG_COURSES_DAYS_C_DAYS"] = "ημέρες";
$MESS["CATALOG_COURSES_SEATS_C_SEAT"] = "τοποθεσία";
$MESS["CATALOG_COURSES_SEATS_C_SEATS_A"] = "τοποθεσία";
$MESS["CATALOG_COURSES_SEATS_C_SEATS"] = "τοποθεσιών";
	//Section title
$MESS["CATALOG_COURSES_SECTION_TITLE"] = "Σεμινάρια";
	//Деталка курса, слайдеры
$MESS["SLIDER_CATALOG_COURSES_RECOMMENDED_C"] = "Recommended courses";
$MESS["SLIDER_CATALOG_COURSES_RECOMMENDED_P"] = "Προτεινώμενα προϊόντα";
$MESS["SLIDER_CATALOG_COURSES_OL"] = "Εκπαιδευτική λογοτεχνία";
$MESS["SLIDER_CATALOG_COURSES_READY_S"] = "Έτοιμες λύσεις για τους πελάτες";
	//Title
$MESS["CATALOG_COURSES_META_TITLE"] = "Κατάλογος σεμιναρίων";

	//Меню
$MESS["CATALOG_COURSES_MENU_SHEDULE"] = "Πρόγραμμα σεμιναρίων";
$MESS["CATALOG_COURSES_MENU_TEACHERS"] = "Καθηγητές";

//Footer
	//адрес, телефон, email
$MESS["FOOTER_ADRESS"] = "Διεύθυνση";
$MESS["FOOTER_PHONE"] = "Τηλέφωνα";
$MESS["FOOTER_EMAIL"] = "e-mail";



?>