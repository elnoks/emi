﻿<?
$MESS["SALE_REFRESH"] = "Ανανέωση";
$MESS["SALE_ORDER"] = "Έλεγχος";
$MESS["SALE_NAME"] = "Όνομα";
$MESS["SALE_PROPS"] = "Ιδιότητες";
$MESS["SALE_PRICE"] = "Τιμή";
$MESS["SALE_TYPE"] = "Τύπος πληρωμής";
$MESS["SALE_QUANTITY"] = "Ποσότητα";
$MESS["SALE_DELETE"] = "Διαγραφή";
$MESS["SALE_DELAY"] = "Αναβολή";
$MESS["SALE_WEIGHT"] = "Βάρος";
$MESS["SALE_WEIGHT_G"] = "g";
$MESS["SALE_ORDER_DESCR"] = "Κάντε κλικ σε αυτό το κουμπί για να δείτε και παραγγείλετε τα προϊόντα που βρίσκονται στο καλάθι σας";
$MESS["SALE_DELAYED_TITLE"] = "Αναβολή";
$MESS["SALE_UNAVAIL_TITLE"] = "Προσωρινά μη διαθέσιμο";
$MESS["STB_ORDER_PROMT"] = "Click \"Έλεγχος\" Για να ολοκληρώσετε την παραγγελία σας";
$MESS["STB_COUPON_PROMT"] = "Εάν έχετε ειδικό κωδικό κουπονιού έκπτωσης, παρακαλώ εισάγετέ το εδώ:";
$MESS["SALE_VAT"] = "Φόρος:";
$MESS["SALE_VAT_INCLUDED"] = "Ο φόρος συμπεριλαμβάνεται:";
$MESS["SALE_TOTAL"] = "Σύνολο:";
$MESS["SALE_CONTENT_DISCOUNT"] = "Έκπτωση";
$MESS["SALE_DISCOUNT"] = "Έκπτωση";
$MESS["SALE_NOTIFY_TITLE"] = "Σε λίστα αναμονής";
$MESS["SALE_REFRESH_NOTIFY_DESCR"] = "Κάντε κλικ σε αυτό το κουμπί για να καταργήσετε προϊόντα.";
$MESS["SALE_OR"] = "ή";
$MESS["SALE_ADD_TO_BASKET"] = "Προσθέστε το καλάθι";
$MESS["SALE_TOTAL_WEIGHT"] = "Συνολικό βάρος:";
$MESS["SALE_VAT_EXCLUDED"] = "Συνολική τιμή:";
$MESS["SALE_ITEMS"] = "Kαλάθι:";
$MESS["SALE_BASKET_ITEMS"] = "Διαθέσιμο";
$MESS["SALE_BASKET_ITEMS_DELAYED"] = "Αποθηκεύτηκε για αργότερα";
$MESS["SALE_BASKET_ITEMS_SUBSCRIBED"] = "Σύντομα σε απόθεμα";
$MESS["SALE_BASKET_ITEMS_NOT_AVAILABLE"] = "Μη διαθέσιμο";
$MESS["SALE_NO_ITEMS"] = "Ακόμη δεν υπάρχουν προϊόντα στο καλάθι σας.";



$MESS["COUPON_APPLY"] = "Εφαρμογή";

$MESS["BASKET_ROW_SALE"] = "Έκπτωσης";
$MESS["BASKET_ROW_ITEMS_COUNT"] = "Ποσότητα";
$MESS["BASKET_ROW_PRICE"] = "Κόστος";


$MESS["BASKET_LIMIT_ERROR"] = "Το κόστος της παραγγελίας σας είναι μικρότερο των 40 ευρώ";

$MESS["BASKET_LIMIT_DESCRIPTION"] = "Η ελάχιστη παραγγελία είναι 40 ευρώ.
					<br>
					Για όλες τις παραγγελίες άνω των 500 ευρώ ισχύει έκπτωση 5%.
					<br>
					<br>
					<br>
					<br>";

$MESS["CART_EMPTY"] = "Το καλάθι είναι άδειο";
?>