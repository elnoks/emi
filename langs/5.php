﻿<?
$MESS ['SALE_RECORDS_LIST'] = "Λίστα παραγγελιών";
$MESS ['SALE_CANCEL_ORDER1'] = "Είστε σίγουρος πως θέλετε να ακυρώσετε";
$MESS ['SALE_CANCEL_ORDER2'] = "την παραγγελία";
$MESS ['SALE_CANCEL_ORDER3'] = "Δεν μπορείτε να επαναφέρεται την παραγγελία μετά την ακύρωσή της.";
$MESS ['SALE_CANCEL_ORDER4'] = "Παρακαλώ πείτε μας τον λόγο της ακύρωσης της παραγγελίας";
$MESS ['SALE_CANCEL_ORDER_BTN'] = "Ακύρωση παραγγελίας";
?>