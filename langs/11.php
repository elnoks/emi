﻿<?
$MESS["CATALOG_BUY"] = "Αγορά";
$MESS["CATALOG_ADD"] = "Kαλάθι";
$MESS["CATALOG_ADDING"] = "Γίνεται προσθήκη...";
$MESS["CATALOG_ADDED"] = "Προστέθηκε";
$MESS["CATALOG_NOT_AVAILABLE"] = "(μη διαθέσιμο σε απόθεμα)";
$MESS["CATALOG_TITLE"] = "Τίτλος";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "Όλες οι πληροφορίες που σχετίζονται με αυτήν την εγγραφή θα διαγραφούν. Συνέχεια;";
$MESS["CATALOG_NO_QUANTITY"] = "Μη διαθέσιμο σε απόθεμα";
$MESS["NO_PRICE"] = "Kαμία τιμή";
?>