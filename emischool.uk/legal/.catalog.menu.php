<?
$aMenuLinks = Array(
	Array(
		"Details of the Seller", 
		"/legal/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"General Retail Terms and Conditions", 
		"vop_maloobchod.php", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"General Wholesale Terms and Conditions", 
		"general_wholesale_terms_and_conditions.php", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Web Interface usage terms and conditions", 
		"web_interface_usage_terms_and_conditions.php", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Complaint Guidelines", 
		"complaint_guidelines.php", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Contract termination form", 
		"contract_termination_form.php", 
		Array(), 
		Array(), 
		"" 
	),
    Array(
        "Consent to personal data processing",
        "consent_personal_data.php",
        Array(),
        Array(),
        ""
    )
);
?>