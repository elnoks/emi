<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Payment and delivery");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Payment and delivery</h1>
   </div>


    <h3>1. Prices</h3>

    <p>1.1 All the e-shop prices include 20% VAT.</p>
    <p>1.2 Products are supplied without 20% VAT to non-EU countries.</p>
    <p>1.3 For all orders over £ 500 is valid 5% discount (discount is not fundable and is valid only for one order).</p>
    <p>1.4 Prices do not include delivery charges.</p>
    <p>1.5 All taxes and other possible expenses in accordance with Client’s country laws, pays Client.</p>

    <h3>2. Payment</h3>


    <p>2.1 All payments are effected on the basis of issued invoice.</p>
    <p>2.2 Before paying the invoice please check if products and products amounts in the invoice are correct.</p>
    <p>2.3 We accept payments via:</p>
    <p>- Bank transfer to our account.</p>

    <h3>3. Shipping conditions</h3>

    <p>3.1 Products are shipped by DPD transporting Company.</p>
    <p>3.2 Shipping rates are not fixed and depend on package weight and shipping destination.</p>
    <p>Transport expenses are counted and shown in the issued invoice (POST).</p>
    <p>3.3 Packaging and shipping are processed within 3 business days after receiving of the payment.</p>
    <p>3.4 All shipping terms are estimated and depend on shipping destination.</p>

    <p><br /><br />When order is shipped, DPD transporting Company sends Client an email with a web link for tracking of the order and tracking number.</p>

  <br />


</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>