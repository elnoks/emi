<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Despre compania");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Despre compania</h1>
   </div>
 
  <div class="bx_page" style="text-align: center;"><img src="/upload/medialibrary/f7c/f7cb52963256ebe4b40f9af5c03a36d2.jpg" title="56.jpg" border="0" alt="56.jpg" width="1140" height="300"  /> 
    <br />
   </div>
 
  <br />
 
  <p>Compania E.Mi - este  brandul cu mii de soluții de-a gata unice și exclusive. E.MI este brandul campioanei  în design de unghii EKATERINA MIROSHNICHENKO.  Va punem la dispoziţie produse profesionale pentru designul unghiilor de cea mai buna calitate, recunoscute pe plan mondial. </p>
 
  <p>Misiunea noastră - de a ajuta pe fiecare maestrul să-şi dezvolte creativitatea,  să se simtă ca un pictor adevărat  și să  devină  un real fashion - expert.</p>
 
  <p>Compania E.Mi - este afacere de succes cu o creștere rapidă,  oricine poate sa se alăture acestei companii!  În momentul de față, sunt  cincizeci reprezentanţe autorizate ale “Școlii de Nail Design by Ekaterina Miroshnichenko” în Rusia, Europa și Asia, distribuitorii oficiali ale brandului E.Mi și sute de puncte de vînzări din întreaga lume.</p>
 
  <h2>Soluţii de-a gata&Unghii&Moda</h2>
 
  <p><img src="/upload/medialibrary/cd4/cd48dcf71294554d8839eab0a34a4986.jpg" title="98.jpg" border="0" alt="54.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Haute couture - o adevărata arta.  Ea fascinează, te inspiră la experimente noi şi te impune cu nerăbdare să aştepţi sezonul nou,   care întotdeauna e plin de surprize.</p>
 
  <p>Soluţie gata E.Mi – este însăşi  Nail Design de la  haute couture. Pe parcursul anului, noi prezentăm cîteva colecţii exclusive elaborate de Campioana mondiala în Nail Design la categoria fantasy (Paris 2010), de doua ori Campioana Europeana (Athena, Paris -2009), judecătoarea  de categorie internațională şi fondatoarea  Școlii de Nail Design by Ekaterina Miroshnichenko.</p>
 
  <p>Fiecare dintre colecțiile Ekaterinei Miroshnichenko – este o interpretarea personală  a  tendințelor cele mai actuale în modă, pe care ea le traduce  în limbajul Nail Design și le adaptează pentru a fi utilizate în saloane de frumuseţe , incredibil de frumoase şi unice la  primă vedere ele sunt uşor de creat, oricare maestrul chiar şi ne avînd educație artistică, după trecerea cursului de formare sau video master-class poate uşor sa creeze aceste tehnici</p>
 
  <p>Printre colecțiile bestseller-uri,  se enumeră aşa  "Pictură Zhostovo", "Pictura Chinezească," "Imitație de Piele Reptile," "Efectul Craquelure", "Printuri Etnice", " Pietre Lichide şi Nisip Catifelat" și altele. Fii inspirat de setul complet a colecțiilor chiar acum!</p>
 
  <h2>Produse</h2>
 
  <p><img src="/upload/medialibrary/6e3/6e3b48a3e4405516b24de034f7ca5b60.jpg" title="6542.jpg" border="0" alt="6542.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>La crearea fiecărui produs nou, culori noi sau accesoriilor pentru lucru Ekaterina Miroshnichenko participă personal. Produsele respectă toate cerințele prevăzute de designeri - ia în considerare toate subtilităţile de lucru în Nail Design, ceea ce permite să reducă timpul pentru a crea o imagine
și oferă oportunități nelimitate pentru creativitate.
</p>
 
  <p>La noi sunt prezente aşa produse unice ca EMPASTA, GLOSSEMI, TEXTONE, PRINCOT, cât și colecții de vopsele gel, folie de design de diverse  culori, o varietate de materiale pentru a decora unghiile si accesorii.  Găsiţi produsul de care aveţi nevoie chiar acum!</p>
 
  <h2>Școală de Nail Design by Ekaterina Miroshnichenko </h2>
 
  <p><img src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" title="765.jpg" border="0" alt="765.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Dacă visaţi despre o profesie interesantă și creativă, doriți  ca clienții dumneavoastră  să plece de la voi cu un zîmbet recunoscător și întotdeauna  să revină, să deveniți proprietarul unei măiestrii exclusive la Școală de Nail Design by Ekaterina Miroshnichenko.  </p>
 
  <p>Şcoala prezintă programe educaționale unice de nail design, special concepute pentru specialişti care nu au o educație de artă.</p>
 
  <p>Cursurile sunt împărțite în 3 nivele de dificultate și cursuri suplimentare.  Cei care caută să cucerească Olympus profesional și să participe la  concursuri  în Nail Design,noi  oferim cursuri speciale de formare competitive. Elevii şcolii – sunt cîştigătorii de concursuri regionale și naționale de Nail Design.</p>
 
  <p>Alege programul de formare, chiar acum pe site-ul nostru!</p>
 
  <h2>Reprezentanți Oficiali</h2>
 
  <p><img src="/upload/medialibrary/060/060926d856ef3ef2bcc0eef18fa948cc.jpg" title="7645.jpg" border="0" alt="7645.jpg" width="1140" height="150"  /> 
    <br />
   </p>
 
  <p>Marca E.Mi și Școală de Nail Design by Ekaterina Miroshnichenko  este o tendinţe modernă a  businessului , care devine din ce în ce mai populară și pe plan internațional.  În momentul de față, reprezentanțe oficiale operează cu succes în Rusia, Ucraina, Kazahstan, Belarus, Italia, Portugalia, România, Cipru, Germania, Franța, Lituania, Slovacia, Coreea de Sud și Emiratele Arabe Unite iar produselor E.Mi pot fi achiziționate la nivel mondial.</p>
 
  <p>Puteți deveni, de asemenea, parte dintr-un brand internațional de succes și să  obţineţi o decizie gata de afacere - a deveni reprezentant oficial sau distribuitor al brăndului în orașul său, regiune sau țară. Reprezentanți oficiali  au următoarele avantaje: 
<ul> 
	<li>
posibilitatea de a reprezenta exclusiv școlile de brand din regiunea lor;
	</li>
	<li>
dreptul la  cursuri de Nail Design,  procesele de afaceri complexe pentru pornirea si dezvoltarea afacerilor personale;
	</li>
	<li>
suport complet pentru selecția și formarea personalului;
	</li>
	<li>
suport complet pentru selecția și formarea personalului;
	</li>
	<li>
soluții eficiente pentru achiziționarea de întreaga gamă de produse E.Mi.
	</li>
</ul></p>
 
  <p>Pentru a deveni un reprezentant oficial sau un distribuitor de brand, puteți completa  formularul  pe site-ul sau prin telefon ……………., e-mail ………………………., ………………………………,<!--To become an official representative of the Nail design school by Ekaterina Miroshnichenko or an official distributor of the E.Mi brand, you can fill in an application form on our website or call +420 722935746 Korbut Marina, korbut@emischool.com--></p>

  <p>
    <br />
  </p>

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>