<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Cum să devii un reprezentant oficial al brandului E.Mi şi al Școlii de Nail Design by Ekaterina Miroshnichenko?");
?><div class="bx_page">
	<p style="text-align: center;">
 <img width="800" alt="765.jpg" src="/upload/medialibrary/883/88396b336e705a892d5a505f75478813.jpg" height="105" title="765.jpg" border="0"><b> <br>
 </b>
	</p>
	<p>
 <b> <br>
 </b>
	</p>
	<p>
 <b>Cooperarea cu Şcoala de Nail Design by Ekaterina Miroshnichenko este o oportunitate de a avea un bussines stabil, care va aduce venituri mereu. Cooperând cu noi, vi se va pune la dispoziție o gama completă de instrumente si resurse, de la nail design, la management si promovare. Modelele de nail design de la Scoala Ekaterinei Miroshnichenko sunt întotdeauna in voga, populare printre clienți, elegante și ușor de notorietate internaționala!</b>
	</p>
	<p>
		 Deveniţi reprezentanţi regionali şi va vom învăța cum sa aveți succes si sa fiți creativa!
	</p>
	<p>
		La momentul de fata, Școala de Nail Design by Ekaterina Miroshnichenko este cunoscuta peste tot în lume, obținând premii prestigioase la concursuri internaționale, realizând seminariilor in Germania și Italia si deschizând un birou internațional în Europa.&nbsp;<br>
		 Puteți obține dreptul exclusiv de a dezvolta in regiunea Dvst-a o afacere interesantă si extrem de profitabilă in domeniul de instruire și al formării avansate a maeștrilor de design de unghii de către Şcoala de Nail Design by Ekaterina Miroshnichenko.
	</p>
	<p>
		 Alăturați-vă celor care sunt mereu in trend!&nbsp;Școala de Nail Design by Ekaterina Miroshnichenko a dezvoltat atât o tehnologie unică de nail design recunoscut la nivel International, cat si tehnici unice de învățare a acestor design-uri.&nbsp;<br>
		 Noi punem la dispoziția tuturor bucuria creației și a frumuseții!&nbsp;<br>
		 Avantajele pe care le oferă Școala de Nail Design by Ekaterina Miroshnichenko:
	</p>
	<p>
	</p>
	<ul>
		<li>
		<p>
			 Având in vedere ca in centrul de formare din Rostov -pe- Don se instruiesc, anual, mai mult de 500 de persoane din toate regiunile din Rusia si de peste hotare, se confirma faptul ca exista o cerere pentru serviciile educaționale in materie de nail design.
		</p>
 </li>
		<li>
		<p>
			 Activitățile formare se desfășoară intr-un cadru unic pentru designul unghiilor având marca&nbsp;<a href="https://emischool.ro/catalog/">E.Mi</a>
		</p>
 </li>
		<li>
		<p>
			 La nivel internațional , compania deține un birou în Europa (Praga).
		</p>
 </li>
		<li>
		<p>
			 Cursurile se bazează pe predarea tehnicilor unice dezvoltate și patentate de către Ekaterina Miroshnichenko, campioana mondiala la categoria Nail Design (Paris, 2011).
		</p>
 </li>
	</ul>
	<p>
	</p>
	<p>
 <b>Oferim distribuitorilor suportul necesar pentru Școlii de Nail Design by E. Miroshnichenko.&nbsp;</b><br>
 <b>
		La deschiderea unei reprezentante oficiale a Școlii de Nail Design beneficiați de următoarele:</b>
	</p>
	<p>
	</p>
	<ol>
		<li>
		<p>
			Dreptul exclusiv de a opera sub numele brand-ului Școala de Nail Design by Ekaterina Miroshnichenko si de a furniza servicii educaționale pe teritoriul specificat în contract.
		</p>
		</li>
		<li>
		<p>
			Programe de instruire&nbsp;de-a gata elaborate de Ekaterina Miroshnichenko Centrul educațional economisește timp și finanțe în ceea ce privește dezvoltarea curriculum-ului, metodele de instruire și materialele educaționale și manualele. Toate acestea permit concentrarea asupra căutării cursanţilor și compensarea cheltuielilor de deschidere în cel mai scurt timp posibil.
		</p>
		</li>
		<li>
		<p>
			Includerea într-un sistem&nbsp;unic de certificare. Reprezentanta are dreptul de a elibera cursanților si clienților certificate ce au pe ele holograma Școlii de Nail Design by Ekaterina Miroshnichenko.
		</p>
		</li>
		<li>
		<p>
			Dreptul la reduceri exclusive la&nbsp;<a href="https://emischool.ro/catalog/">E.Mi</a>&nbsp;produsele.
		</p>
		</li>
		<li>
		<p>
			Suport promoţional. Școala răspândește toate informațiile despre reprezentanții si distribuitorii lor în reviste de specialitate de vârf , precum și în propria sa revista si pe site-ul oficial. Aceasta ajuta la organizarea expozițiilor regionale și a conferințelor despre unghii, oferind șabloane gata de a fi folosite.
		</p>
		</li>
	</ol>
	<p>
	</p>
	<p>
		 Cooperarea în calitate de reprezentant oficial al Școlii de Nail Design by Ekaterina Miroshnichenko și un distribuitor oficial al brandului E.Mi.
	</p>
	<p>
		 Reprezentantul oficial al Scolii de Nail Design by Ekaterina Miroshnichenko are dreptul sa încheie un acord privind distribuția produselor ce apartin de marca E.Mi pe teritoriul stabilit în contractul de reprezentare oficială a Scoala de Nail Design. În acest caz, cerințele suplimentare și termenii de cooperare a distribuitorilor, precum și obligațiile privind prețul de cumpărare, cu majorarea corespunzătoare a reducerii produselor de brand E.Mi, se adaugă la cerințele reprezentanților oficiali ai Școlii de Nail Design by Ekaterina Miroshnichenko.
	</p>
	<p>
		 &nbsp;
	</p>
	<p>
 <b>Cerințe pentru deschiderea unei reprezentanțe a Scolii de Nail Design by Ekaterina Miroshnichenko:</b>
	</p>
	<p>
	</p>
	<ol>
		<li>
		<p>
			Instructor talentat, capabil sa reproducă întocmai tehnicile Ekaterinei Miroshnichenko. In prima faza, candidatul participa la cursuri in calitate de cursant, iar in cazul in care se potrivește standardelor profesionale, aprobate de Ekaterina Miroshnichenko, urmează cursurile de specializare pentru a deveni instructor.
		</p>
		</li>
		<li>
		<p>
			Disponibilitatea unui spațiu de cel puțin 16 metri pătrați, pentru centrul de instruire. Plasarea de panou de marcă în conformitate cu Şcoală de Nail Design by Ekaterina Miroshnichenko.
		</p>
		</li>
		<li>
		<p>
			Existenta unui cadru juridic ce permite furnizarea serviciilor educaționale.
		</p>
		</li>
		<li>
		<p>
			Resurse financiare suficiente pentru achiziționarea pachetului de produse de început, dotarea centrului de formare cu echipamentele necesare și formarea instructorilor.
		</p>
		</li>
		<li>
		<p>
			Angajamentul reprezentativ pentru a atrage noi cursanţi pe baza tehnologiilor dezvoltate.
		</p>
		</li>
	</ol>
	<p>
	</p>
	<p>
 <b>&nbsp;Dacă v-ați decis să deschideți un birou de reprezentant al Școlii în regiunea dvs., va recomandam sa urmați următorii pași:</b>
	</p>
	<p>
	</p>
	<ol>
		<li>
		<p>
			Verificați dacă există un birou în zona dumneavoastră. Acesta poate fi găsit în secțiunea "Contact" sau apelați 7 906 427-25-86. În cazul în care in regiunea dvs. exista deja o Școala, vom fi nevoiți să vă refuzam colaborarea.
		</p>
		</li>
		<li>
		<p>
			Trimiteți la <a href="mailto:sviridova@emi-school.ru">sviridova@emi-school.ru</a> fotografii cu tipsuri reproducând cat mai bine design-ul modelelor create de Ekaterina Miroshnichenko (de ex. flori sofisticate, fructe, pliuri etc.). Aceasta etapa este necesară pentru aprobarea candidaturii ca instructor. Capacitatea de a reproduce întocmai stilul si tehnica Ekaterinei Miroshnichenko este esențiala.
		</p>
		</li>
		<li>
		<p>
			Informați-va asupra costului de urmare a cursurilor la nr. de telefon + 7-906-427-25-86. Procesul de specializare se desfășoară în două etape: în calitate de cursant și de instructor. Cursurile se desfășoară numai la centrul de instruire în Rostov-pe-Don! Costul variază in funcție de numărul de cursuri.
		</p>
		</li>
		<li>
		<p>
			Informați-va asupra sumei totale de achiziții pe trimestru la tel. + 7-906-427-25-86. Volumul achizițiilor este fixat în scris în contract.
		</p>
		</li>
		<li>
		<p>
			Fiți pregătit/a pentru antrenament regulat. Contractul de instructor este valabil timp de 1 an. În fiecare an, instructorul trebuie sa &nbsp;&nbsp;&nbsp;&nbsp;urmeze un curs de recalificare în Rostov-pe-Don și sa încheie un nou contract pentru următorul an.
		</p>
		</li>
	</ol>
	<p>
	</p>
	<p>
		 Pentru mai multe informații despre deschiderea unei reprezentante a Scolii de Nail Design by Ekaterina Miroshnichenko, puteți contacta sediul central:&nbsp;<br>
		 Rostov-pe-Don, T. + 7-906-427-25-86, <a href="mailto:trade@emi-school.ru">trade@emi-school.ru</a>
	</p>
	<p>
	</p>
	<p>
		 &nbsp;
	</p>
 <br>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>