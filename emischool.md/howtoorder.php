<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Cum plăsezi o comanda");
?> 
<div class="bx_page"> 
  <div class="h1-top"> 		 
    <h1>Cum plăsezi o comanda.</h1>
   </div>
 </div>
 

<div class="bx_page"> 
  <p>Procuraţi produsele E.Mi chiar acum - profitați de comanda on-line</p>
  <ul>
	<li><p>1. Înregistraţi-vă pe site.</p></li>
	<li><p>2. Apăsaţi "Produse" și adaugaţi  produsele preferate în coşul de cumpărături , apasînd clic pe butonul de culoare roz "Adaugă în coş", din dreptul fotografiei produsului ales. În cazul în care produsul a fost adăugat cu succes în coş,  buton schimbă culoarea în verde și pe el va apărea inscripţie "în coș." Dupa adaugarea produsului, poţi să-ţi continui cumpărăturile  sau ai posibilitatea de a finaliza comanda accesînd butonul „ Coşul meu”.</p></li>
	<li><p>3. După ce ați terminat procesul de achiziție, verificaţi starea comenzii prin realizarea unui click în colţul din dreapta paginii pe butonul “Cosul meu”. În urma acestei acţiuni se va deschide coşul tău de cumpărături unde se găsesc informaţiile despre produsele selectate, poţi actualiza cantitatea sau calcula costul total al comenzii.</p>
	
	<p>Dacă aţi modificat numărul de produse, aţi şters sau aţi adăugat denumiri noi, faceți clic pe "Convert" pentru a vedea valoarea actuală de cumpărături.</p>

	<p>Atenție! Comanda minima E.Mi  este în valoare de  ............</p>
	<p>La comandarea produselor beneficiezi de  următoarele reduceri:</p>
	<p>- Suma comenzii de ............... - 5% reducere,</p>
	<p>- Suma comenzii de la ................ - 10% reducere.</p>

<li><p>4. Pentru a finaliza comanda, faceți clic pe butonul de culoare roz "Checkout", care este situat pe partea dreapta a ecranului. Alege  modalitatea de plată şi de livrare.</p></li>
</ul>

<p>Numărul de telefon  al managerului magazinului online: ………………………….</p>
<p>E-mail: ……………………</p>


 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>