<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Modalităţi de plată şi livrare");
$APPLICATION->SetTitle("Modalităţi de plată şi livrare");
?><div class="bx_page">
	<div class="h1-top">
		<h1>Modalităţi de plată şi livrare</h1>
	</div>
 <br>
 <span style="font-weight:bold;">Atenţie! Comanda minimă E.Mi - .....................</span>
	<p>
		<span style="color: #666666; font-family: 'Pt Sans', Arial, Helvetica; font-size: 13px; line-height: 18.5714302062988px; background-color: #f9f9f9;">Plata se efectuează prin utilizarea cardurilor bancare după sistemele de plată ale băncii&nbsp;</span><span style="box-sizing: border-box; font-weight: 700; color: #666666; font-family: 'Pt Sans', Arial, Helvetica; font-size: 13px; line-height: 18.5714302062988px; background-color: #f9f9f9;">..............</span>
	</p>
	<ul style="list-style-type:none;">
		<li> <img src="/visa.png" style="float: left; position: relative; top: -20px; left: -10px;"> <span style="color: #666666; font-family: 'Pt Sans', Arial, Helvetica; font-size: 13px; line-height: 18.5714302062988px; background-color: #f9f9f9;">Visa,&nbsp;</span> </li>
		<li style="margin-top: 44px;"> <img src="/mastercard.png" style="float: left; margin-top: -37px; position: relative; left: -12px;"> <span style="color: #666666; font-family: 'Pt Sans', Arial, Helvetica; position: relative; top: -15px;font-size: 13px; line-height: 18.5714302062988px; background-color: #f9f9f9;">MasterCard</span>
		<p>
		</p>
 </li>
	</ul>
	<p>
		Procesarea comenzilor se efectuează de luni pînă vineri, de la 9:00 pînă la 18:00. Dacă aveţi întrebări puteți apela la tel. +373 068207966 sau scrieţi pe email ............................
	</p>
	<p>
		Citiți cu atenție regulile de plată și de livrare! Cumpararea produselor de pe site-ul, implica în mod automat, acceptarea regulilor de plată și de livrare.
	</p>
	<h2> Condiții suplimentare: </h2>
	<p>
		 Comanda minimă este de .... La comandarea produselor au loc următoarele reduceri: <br>
		 Suma comenzii de la .................. – 5% reducere <br>
		 Suma comenzii de la .................. — 10% reducere&nbsp;
	</p>
	<p>
		<span style="font-family: 'Times New Roman', serif; font-size: 12pt; line-height: 115%;">Expediere comenzi se efectuază în termen de 3 zile lucrătoare de la confirmarea plății.</span>&nbsp;
	</p>
	<div style="clear:both;">
	</div>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>