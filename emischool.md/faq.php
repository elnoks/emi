<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Întrebări-răspunsuri");
?><div class="bx_page">
	<div class="h1-top">
		<h1>Întrebări-răspunsuri</h1>
	</div>
 <br>
	<p>
 <b>Cum să devii un reprezentant oficial al brandului E.Mi şi al Școlii de Nail Design by Ekaterina Miroshnichenko?</b> <br>
		 Companie E.Mi oferă soluţii complete pentru crearea unei afaceri rapide, emergente şi interesante. Puteți deschide un birou oficial reprezentativ al Școlii de Nail Design by Ekaterina Miroshnichenko în orașul dumneavoastră, sa deveniţi distribuitor oficial al E.Mi sau reprezentantul en-gros. Informații detaliate pot fi găsite în rubrica <a href="/social/open_school.php">"Cum se deschide un birou reprezentativ al Școlii"</a>
	</p>
	<p>
 <b>Cum puteţi afla dacă există o “Școală de Nail Design by Ekaterina Miroshnichenko” în orașul dumneavoastră?</b> <br>
		 Găsiţi Școală de Nail Design by Ekaterina Miroshnichenko din orașul dumneavoastră în secțiunea <a href="/contacts/">"Contacte"</a>.
	</p>
	<p>
 <b>Unde puteţi cumpăra produsele E.Mi?</b> <br>
		 Puteţi procura produse E.Mi de pe site-ul nostru la rubrica "Produse" (instrucțiuni detaliate despre cum să comandați, puteţi să le găsiţi în rubrica "Cum să face comandă") sau la punctul de vînzare E.Mi. Puteţi afla daca există “Şcoala de Design de unghi Ekaterina Miroshnichenko” în orașul dumneavoastră în secțiunea <a href="/contacts/">"Contacte”</a>.
	</p>
	<p>
 <b>Este oare posibil a învăţa specialităţi de bază (manichiură, pedichiură, stilist-protezist de unghi ) la Şcoala de Design de unghi Ekaterina Miroshnichenko?</b> <br>
		 Şcoala de Design de unghi Ekaterina Miroshnichenko este specializata exclusiv în nail design.
	</p>
	<p>
 <b>Este oare posibil a studia la Școală de Nail Design by Ekaterina Miroshnichenko, neavând experiență unui maestru?</b> <br>
		 Cursuri de nail design sunt organizate ca stagieri de perfecţionare în domeniu, prin urmare, vă recomandăm mai întâi să fiţi instruiți în specialitate de bază (maestrul în manichiură, pedichiură şi stilist-protezist de unghi). În aşa mod, va fi mai ușor să învățaţi nail design.
	</p>
	<p>
 <b>Cu ce curs ar trebui să înceapă instruire?</b> <br>
		 Vă recomandăm să începeți instruire cu cursuri de bază "Pictură Artistică. Curs de bază" și "Tehnici E.Mi Design». La cursurile date veţi învăţa elementele de bază a lucrului cu geluri colorate şi veţi invăţa a “poziţiona” corect mîina. După ceea va fi mult mai ușor să treceţi cursuri de formare profesională cu nivelul doi şi trei de complexitate, cum ar fi "Pictura Zhostovo", "Pictura Chinezeazscă", "Floristica Complexă", "Abstracții Complexe" și altele.
	</p>
	<p>
 <b>Unde pot găsi un calendar al cursurilor de la “Școala Ekaterina Miroshnichenko” în orașul meu?</b> <br>
		 Un program detaliat al cursurilor în zona dumneavoastra poate fi găsit în rubrica <a href="/courses/#schedule">"Cursuri"</a>.
	</p>
	<p>
 <b>La ce vîrsta pot frecventa cursuri de la “Școală de Nail Design by Ekaterina Miroshnichenko”?</b> <br>
		 “Şcoala de Nail Design by Ekaterina Miroshnichenko” acceptă cursanţi de la 15 ani.
	</p>
	<p>
 <b>Se poate de aplicat vopsele gel E.Mi pe lac gel de alt brand?</b> <br>
		 Vopsele gel E.Mi pot fi aplicate pe alte lacuri gel, dar este important de reținut că majoritatea lacurilor gel se îndepărtează cu ajutorul unui lichid de înmuiere specială, iar vopsele gel E.Mi se pilesc cu buffer. De aceea, vă recomandăm sa aplicaţi design sau ornament pe o suprafaţa nu prea mare a unghiei, acoperită cu lac gel. De exemplu, design "Pietre Lichide şi Nisip Catifelat" sau "3D Vintage”.
	</p>
	<p>
 <b>Pot aplica vopsele gel E.Mi pe unghia naturala?</b> <br>
		 Înainte de a aplica vopsea gel E.Mi, placa unghiei trebuie să fie acoperită cu un material artificial: gel, acril sau lac gel. Aplicarea vopselelor gel E.Mi pe unghia naturală este interzis.
	</p>
	<p>
 <b>Prin ce diferă vopsea gel E.Mi de EMPASTA?</b> <br>
		 Diferența principală constă în consistență și prezența (sau absența) stratului lipicios. EMPASTA E.Mi e mai gros, ceea ce face un frotiu sau o linie să rămîne mai clară pe o perioadă mai lungă, iar consistenţă EMPASTA pentru "tehnica Baroque " permite crearea unui design tridimensional. In plus EMPASTA nu are un strat lipicios, astfel se poate de aplicat asupra Finish Gel sau sub el. Uscarea intermediară pentru EMPASTA E.Mi One Stroke durează numai 2-5 secunde, aceasta permite a efectua design mult mai uşor, cu un număr mare de elemente. Vopsele gel E.Mi au o consistență mai lichidă şi de aceea sunt ideale pentru crearea desenelor.
	</p>
	<p>
 <b>Prin ce diferă polimer "Piatră lichidă" de gel "Piatră lichidă"?</b> <br>
		 Gel "Piatră lichidă" - este un material suplimentar pentru crearea designului folosind tehnica "Piatră lichidă". Acest gel permite simularea cît mai reală a pietrelor prețioase de orice dimensiune și de o înălțime de un strat pe unghii și el nu necesită gel protectiv, astfel se reduce timpul creării unui design. Gel "Piatra lichidă" este transparent şi nu poate fi amestecat cu pigmenți. Polimer "Piatră Lichidă" vă permite simularea pietrelor prețioase, deoarece el poate fi amestecat cu pigmenți şi necesită suprapunere cu gel protector cu strat lipicios.
	</p>
	<p>
 <b>Cum se utilizează vopsea gel EMPASTA “Openwork Silver " la tehnica Baroque?</b> <br>
		 EMPASTA “Openwork Silver” cuprinde o fracțiune transparentă, de aceea e necesar să stoarceţi o cantitate mică de vopsea într-o paletă și s-o amestecaţi bine, apoi lăsați-o să stea câteva zile, pentru a deveni mai deasă, potrivită pentru modelare. A lucra cu EMPASTA proaspătă, tocmai stoarsă dintr-un tub, e imposibil. Cu cît mai mult e păstrată EMPASTA, cu atît e mai bine. EMPASTA nu se usucă pe paletă şi își păstrează consistenţa groasă și este ideală pentru a continua lucru.
	</p>
	<p>
 <b>Ce fel de pensule sunt necesare cînd se lucrează cu materiale E.Mi?</b> <br>
		 Pentru a lucra cu materiale E.Mi se recomandă trei seturi de pensuli "Set de pensule pentru Pictura artistică", "Set de pensule pentru Pictura Chinezească" și "Set universal de pensule."
	</p>
	<p>
 <b>Cum să ai grijă de pensule în mod corespunzător?</b> <br>
		 Pentru ca pensula cît mai mult timp să mențină proprietățile ei, pentru curățarea nu utilizați lichidul care conține acetonă, pur și simplu ștergeți-o cu o cîrpă umedă.
	</p>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>