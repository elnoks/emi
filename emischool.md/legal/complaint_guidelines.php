<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Instrucțiuni privind reclamaţii");
?>
<p>
	 Aceste Instrucțiuni privind reclamațiile prevăd metoda și condițiile reclamațiilor privind defectele de bunuri, achiziționate prin magazinul on-line emischool.com, operat de Compania noastră
</p>
<p>
	 E.Mi - ……………………………………………………………………………………………..
</p>
<p>
</p>
<p>
	 Pentru procesarea reclamațiilor, vă recomandăm să utilizați în principal adresa de corespondență:
</p>
<p>
	 …………………………………….
</p>
<p>
	 Numar de telefon:&nbsp; ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
</p>
<p>
	 Emailul de contact:&nbsp; ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
</p>
<p>
</p>
<p>
	 1. Pentru care defecte suntem responsabili?
</p>
<p>
	 În calitate de vânzător, suntem responsabili pentru bunurile fără defecte. Înseamnă că bunurile:
</p>
<p>
	 - au proprietăți, care au fost aprobate de către noi, pe care le-am descris sau pe care v-ați așteptat în ceea ce privește natura bunurilor bazate pe publicitate;
</p>
<p>
	 - sunt în cantitate, măsură şi greutate corespunzătoare;
</p>
<p>
	 - satisfac cerințele legislației.
</p>
<p>
	 Diferența de nuanțe de culoare în realitate și pe echipamente de imagistica electronice nu este considerat un defect al mărfurilor. În cazul în care mărfurile nu corespund așteptărilor dvs., dacă sunteți consumatorul, aveți dreptul să vă retrageți din contract în termen de 14 zile de la primirea mărfurilor în conformitate cu articolul 5 din Termenii și condițiile generale.
</p>
<p>
	 În cazul în care defectul apare în termen de șase luni de la primirea mărfurilor, se consideră că bunurile erau defectate, la primirea acestora.
</p>
<p>
	 Garantează consumatorilor că defectele nu se vor demonstra în perioada de garanție. Dacă nu sunteți consumator, nu vi se oferă garanția calității. Articolul 2 se aplică numai consumatorului.
</p>
<p>
	 2. Cât timp este perioada de garanție?
</p>
<p>
	 Garanția pentru bunurile neutilizate este de douăzeci și patru de luni de la primirea acesteia, cu excepția cazului în care se specifică o perioadă mai lungă de garanție pe portalul web sau în documentele atașate la bunuri.
</p>
<p>
	 În cazul în care mărfurile au o durată de valabilitate minimă, perioada de garanție durează până la această dată.
</p>
<p>
	 3. Care sunt drepturile tale în cazul depistării defectelor?
</p>
<p>
	 În cazul în care un defect al bunurilor se demonstrează în timpul perioadei de garanție, ceea ce împiedică utilizarea corectă a produsului și defectul nu poate fi înlăturat, aveți dreptul la reparația acestuia gratuit.
</p>
<p>
	 În cazul unui defect detașabil al unui produs neutilizat, puteți să înlocuiți produsul defect sau produsul perfect sau o reducere adecvată a prețului de achiziție.
</p>
<p>
	 În cazul unui defect care nu poate fi înlăturat și care împiedică utilizarea corectă a mărfurilor ca mărfuri perfecte, aveți dreptul la o înlocuire a bunurilor, la o reducere adecvată a prețului de cumpărare sau aveți dreptul să vă retrageți din contractul de cumpărare .
</p>
<p>
	 De asemenea, aveți dreptul la o reducere rezonabilă în cazul în care nu reușim să livrăm un produs nou fără defecte, să-l înlocuiască pentru a repara un produs sau în cazul în care nu ar trebui să asigurăm remedierea într-o perioadă adecvată sau dacă remedierea vă provoacă dificultăți semnificative.
</p>
<p>
	 Nu aveți dreptul să vă retrageți din contract sau să solicitați livrarea unui produs nou dacă nu puteți să returnați bunurile într-o stare în care ați primit-o (cu excepția cazurilor specificate în dispozițiile articolului 2110 CC).
</p>
<p>
</p>
<p>
	 4. Când dreptul de performanță defectuoasă nu se aplică?
</p>
<p>
	 Drepturile de performanță defectuoasă nu se aplică dacă:
</p>
<p>
</p>
<p>
	 - ați fost conștient de defectul anterior primirii produsului;
</p>
<p>
</p>
<p>
	 - ați înlăturat defectul;
</p>
<p>
</p>
<p>
	 - perioada de garanție a expirat.
</p>
<p>
</p>
<p>
	 De asemenea, garanția și drepturile de răspundere pentru defecte nu se aplică:
</p>
<p>
</p>
<p>
	 - uzura cauzată de utilizarea acestuia;
</p>
<p>
</p>
<p>
	 - defectele cauzate de utilizarea incorectă a mărfurilor, nerespectarea manualului, întreținerea incorectă sau depozitarea incorectă.
</p>
<p>
	 În cazul unor lucruri vândute la prețuri reduse, nu suntem responsabili pentru defecte, pentru care prețul a fost redus.
</p>
<p>
	 Noi nu suntem responsabili pentru vătămarea fizică a persoanelor sau pentru daune de proprietate și de bunuri, care au fost cauzate de manipularea neautorizată sau utilizarea necorespunzătoare a mărfurilor, sau prin neglijență în cele din urmă.
</p>
<p>
	 5. Cum să procedați la plângere?
</p>
<p>
	 Aplicați reclamația la compania noastră (sau la o entitate care se află pe portalul web desemnat ca persoană autorizată pentru reparații) fără întârziere inutilă de la detectarea defectului.
</p>
<p>
	 Plângerea poate fi aplicată după cum urmează:
</p>
<p>
	 - pentru o procesare mai rapidă, ne puteți informa în prealabil despre plângere printr-un telefon, prin e-mail, în scris.
</p>
<p>
	 - pentru o prelucrare cât mai rapidă a unei reclamații, vă recomandăm să trimiteți mărfurile revendicate (altele decât COD, pe care nu le acceptăm) la adresa noastră de e-mail (sau la adresa unei persoane autorizate pentru reparații). De asemenea, puteți trimite mărfurile la adresa sediului social sau a oricărui sediu comercial.
</p>
<p>
	 Împachetați bunurile pentru a le expedia într-un ambalaj adecvat pentru a preveni orice deteriorare sau distrugere.
</p>
<p>
	 Documentul de cumpărare sau factură, dacă a fost emis, sau un alt document de achiziție ar trebui să fie atașat la mărfurile, împreună cu o descriere a defectului și o sugestie a unei metode de soluționare a cererii. Imposibilitatea de a prezenta oricare dintre documentele menționate mai sus nu împiedică prelucrarea pozitivă a cererii, în conformitate cu termenii legali.
</p>
<p>
</p>
<p>
	 Momentul aplicării unei revendicări este momentul în care am fost informați despre defect și dreptul de răspundere pentru defectele unui articol vândut a fost executat.
</p>
<p>
</p>
<p>
	 Solicităm reclamațiile livrate fără întârzieri inutile, în termen de 30 de zile de la data depunerii unei plângeri, cu excepția cazului în care se convine altfel. Vom emite o confirmare scrisă cu privire la aplicarea și soluționarea plângerii dvs.
</p>
<p>
</p>
<p>
	 În cazul unei plângeri contestabile, vom decide asupra acceptării acesteia în termen de trei zile lucrătoare de la data depunerii cererii.
</p>
<p>
</p>
<p>
</p>
<p>
	 Aceste Instrucțiuni privind reclamațiile sunt valabile și începând cu data de 23.07.2015.
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>