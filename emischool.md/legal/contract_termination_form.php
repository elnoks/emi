<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Formularul de reziliere a contractului");
?><p>
	 Formularul de reziliere a contractului
</p>
<p>
	 (completați acest formular și trimiteți-l înapoi numai în cazul în care doriți să reziliați contractul)
</p>
<p>
</p>
<p>
	 Anunț de reziliere
</p>
<p>
</p>
<p>
	 - Destinatar E.Mi -………………………………
</p>
<p>
</p>
<p>
	 - Declar / declaram (*) prin prezenta că vom rezilia contractul de achiziție a următoarelor produse: …
</p>
<p>
</p>
<p>
	 &nbsp; - Data comenzii (*) / data primirii (*)
</p>
<p>
</p>
<p>
	 - Numele și prenumele destinatarului / destinatarilor
</p>
<p>
</p>
<p>
	 - adresa destinatarului / destinatarilor
</p>
<p>
</p>
<p>
	 - semnătura destinatarului / destinatarilor (numai atunci când este trimisă pe suport de hârtie)
</p>
<p>
</p>
<p>
	 - Data
</p>
<p>
</p>
<p>
</p>
<p>
	 (*) Înlăturați inaplicabilul
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>